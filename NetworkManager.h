//
//  NetworkManager.h
//  pd2
//
//  Created by User on 17.06.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "Singleton.h"
#import "Utils.h"
#import "AFHTTPRequestOperationManager.h"

@class WaitingChildrenModel, AuthorisedPersonModel, ParentalAccessChildrenModel, ParentalAccessParentModel, DiaryDuplicationFullModel, AdministratorModel;

NSString * const NetworkManagerVideoServerHostInfoSuccessNotification;
NSString * const NetworkManagerVideoServerHostInfoFailNotification;



//заменено на PhotoGalleryType.h
//enum PhotoGalleryType{kPhotoGalleryTypeParental=1, kPhotoGalleryTypeDiary=2, kPhotoGalleryTypeProgress=3,kPhotoGalleryTypeGeneral=4};

typedef void (^NetworkManagerCompletionHandler)(id response, NSError *error);
typedef void(^imageCacheDownloadCompletionHandler)(UIImage* image);

@interface NetworkManager : Singleton

@property (nonatomic, strong) NSString *subdomain;
@property (nonatomic,retain) NSString *baseUrl;
@property (nonatomic, strong) NSString *videoServerHost;
- (NSString *)urlForSubdomain:(NSString *)subdomain;
@property (nonatomic,retain) NSString *token;
@property (nonatomic, assign) BOOL isScottish;

@property (nonatomic, strong) NSString *serverApiVersion;

@property (nonatomic, strong) void (^requestInfoLog)(AFHTTPRequestOperation * );

//Переход в регистер для парентсайда
//@property (nonatomic, strong) NSArray *registerLoginArray;


@property (nonatomic, assign) BOOL isWhiteLabel;
@property (nonatomic, strong) NSString *whiteLabelDomain;
@property (nonatomic, strong) NSArray *whiteLabelSubdomains;

@property (nonatomic, assign) BOOL isBeta;
@property (nonatomic, assign) BOOL isJ3;


- (void)storeVideoServerHost;

- (NSMutableData *)generatePostDataForDictionary:(NSDictionary *)dict withBoundary:(NSString *)boundary close:(BOOL)isClose;
- (void)updateSessionTimer;

//white label
-(void)getDomainsListFor:(int)index withCompletion:(void (^)(id response, NSError *error))completion;

//admins
-(void)getAdminListAndShowProgressHUD:(BOOL)showProgressHUD withCompletion:(void (^)(id response, NSError *error))completion;
-(void)registerAdminWithFirstName:(NSString *)fname lastName:(NSString *)lName email:(NSString *)emailStr username:(NSString *)userName password:(NSString *)password accessLevel:(int)accLevel position:(int)position room:(int)room image:(UIImage *)image completion:(void (^)(id, NSError *))completion;
-(void)updateAdminWithId:(int)adminId firstName:(NSString *)fname lastName:(NSString *)lName email:(NSString *)emailStr username:(NSString *)userName password:(NSString *)password accessLevel:(int)accLevel position:(int)position room:(int)room image:(UIImage *)image completion:(void (^)(id response, NSError *error))completion;
-(void)deleteAdminWithId:(int)adminId completion:(void (^)(id response, NSError *error))completion;
-(void)getAdminWithId:(int)adminId completion:(void (^)(id response, NSError *error))completion;


//positions
-(void)getPositionListWithCompletion:(void (^)(id response, NSError *error))completion;
-(void)createPositionWithTitle:(NSString *)title completion:(void (^)(id response, NSError *error))completion;
-(void)renamePositionWithOldName:(NSString *)oldName andId:(int)positionId toNewName:(NSString *)newName completion:(void (^)(id response, NSError *error))completion;
-(void)deletePositionWithId:(int)position_id completion:(void (^)(id response, NSError *error))completion;
-(void)reassignAdminsFromPosition:(int)room1Id toPosition:(int)room2Id completion:(void (^)(id response, NSError *error))completion;


//rooms
-(void)getRoomListWithCompletion:(void (^)(id response, NSError *error))completion;
-(void)updateRoomWithId:(int)roomId title:(NSString *)title from:(int)from to:(int)to ratio:(int)ratio capacity:(int)capacity image:(UIImage *)image completion:(void (^)(id response, NSError *error))completion;
-(void)createRoomWithTitle:(NSString *)title from:(int)from to:(int)to ratio:(int)ratio capacity:(int)capacity image:(UIImage *)image completion:(void (^)(id response, NSError *error))completion;
-(void)deleteRoomWithId:(int)roomId completion:(void (^)(id response, NSError *error))completion;
-(void)reassignChildrenFromRoom:(int)room1Id toRoom:(int)room2Id completion:(void (^)(id response, NSError *error))completion;


//children
-(void)getChildrenListWithCompletion:(void (^)(id response, NSError *error))completion;
-(void)createChildWithOptions:(NSDictionary*)options completion:(void (^)(id response, NSError *error))completion;
-(void)updateChildWithId:(int)childId options:(NSDictionary*)options completion:(void (^)(id response, NSError *error))completion;
-(void)deleteChildWithId:(int)childId completion:(void (^)(id response, NSError *error))completion;
-(void)archiveChildWithId:(int)childId completion:(void (^)(id response, NSError *error))completion;
-(void)unarchiveChildWithId:(int)childId completion:(void (^)(id response, NSError *error))completion;
-(void)getChildInfo:(int)childId completion:(void (^)(id response, NSError *error))completion;
-(void)getChildrenBirthdaysWithCompletion:(void (^)(id response, NSArray *birthdays, NSError *error))completion;
- (void)getChildrenStatisticsWithCompletion:(void (^)(id response, NSError *error))completion;


//diary helpers
-(void)getDiaryHelpersListAndShowProgressHUD:(BOOL)progressHUD withCompletion:(void (^)(id response, NSError *error))completion;
-(void)createOrUpdateDiaryHelperWithOptions:(NSDictionary*)options completion:(void (^)(id response, NSError *error))completion;
-(void)editDiaryWithOptions:(NSDictionary*)options completion:(void (^)(id response, NSError *error))completion;
-(void)deleteDiaryHelperWithId:(int)helperId completion:(void (^)(id response, NSError *error))completion;
-(void)updatePhotoInformationForDiaryWithOptions:(NSDictionary*)options completion:(void (^)(id response, NSError *error))completion;


//child profile sections
-(void)getChildProfileListWithCompletion:(void (^)(id response, NSError *error))completion;
-(void)createOrUpdateChildProfileWithOptions:(NSDictionary*)options completion:(void (^)(id response, NSError *error))completion;
-(void)deleteChildProfileWithId:(int)helperId completion:(void (^)(id response, NSError *error))completion;
-(void)changeOrderingForHelpersWithParams:(NSDictionary*)dict isDiaryHelper:(BOOL)isDiaryHelper completion:(void (^)(id response, NSError *error))completion;


//login section
-(void)loginWithUsername:(NSString*)username password:(NSString*)password subdomain:(NSString *)subdomain completion:(void (^)(id response, NSError * error))completion;
-(void)loginWithUserId:(int)userId password:(NSString*)password subdomain:(NSString *)subdomain completion:(void (^)(id response, NSError * error))completion;
-(void)logoutAndDeviceToken:(BOOL)isWithToken completion:(void (^)(id response, NSError * error))completion;
- (void)checkIfPinExistsForUser:(int)uid subdomain:(NSString *)subdomain withCompletion:(void (^)(id response, NSError *error))completion;
- (void)createPin:(NSString *)pin forUser:(int)uid withCompletion:(void (^)(id response, NSError *error))completion;
- (void)loginWithPin:(NSString *)pin forUser:(int)uid subdomain:(NSString *)subdomain withCompletion:(void (^)(id response, NSError *error))completion;
- (void)loginWithTouchIdString:(NSString *)touchIdString forUser:(int)uid subdomain:(NSString *)subdomain withCompletion:(void (^)(id response, NSError *error))completion;
- (void)resetPinForUserID:(int)uid password:(NSString *)password subdomain:(NSString *)subdomain withCompletion:(void (^)(id response, NSError *error))completion;
- (void)createTouchIdToken:(NSString *)touchIdToken forUser:(int)uid isParent:(BOOL)isParent withCompletion:(void (^)(id response, NSError *error))completion;
- (void)createTouchIdToken:(NSString *)touchIdToken forUserName:(NSString *)userName password:(NSString *)password subdomain:(NSString *)subdomain isParent:(BOOL)isParent withCompletion:(void (^)(id response, NSError *error))completion;


@end

