//
//  NSDate+MonthAge.m
//  pd2
//
//  Created by i11 on 19/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "NSDate+MonthAge.h"

@implementation NSDate (MonthAge)


- (NSInteger)monthsToToday {
    
    return [self monthsToDate:[NSDate date]];
};


- (NSInteger)monthsToDate:(NSDate *)date {
    
    return [[[NSCalendar currentCalendar] components: NSCalendarUnitMonth
                                                       fromDate: self
                                                         toDate: date
                                                        options: 0] month];

    
}

@end
