//
//  NSDate+DayOfMonthOnServer.m
//  pd2
//
//  Created by i11 on 19/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "NSDate+DayOfMonthOnServer.h"
#import "NSDate+Utilities.h"

@implementation NSDate (DayOfMonthOnServer)


- (int)deltaDayOfMonthOnServer {
    
    //вычисляем старт месяца
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    NSDateComponents *edgeCase = [[NSDateComponents alloc] init];
    [edgeCase setDay:1];
    [edgeCase setMonth:[self month]];
    [edgeCase setYear:[self year]];
    
    NSDate *edgeCaseDate = [calendar dateFromComponents:edgeCase];
    
    //Индекс дня старта недели
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    //[gregorian setFirstWeekday:2]; //monday
    
    NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:edgeCaseDate];
    
    //[sun, mon, tue, wed,...]
    int weekday = (int)[comps weekday];
    NSArray *replacements = @[@5,@-1,@0,@1,@2,@3,@4];
    int delta =  [[replacements objectAtIndex:weekday - 1] intValue];
    
    return delta;
    
}


- (int)dayOfMonthOnServer {

    return (int)[self day] + [self deltaDayOfMonthOnServer];
    
}


@end
