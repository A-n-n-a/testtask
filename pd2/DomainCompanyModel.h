//
//  DomainCompanyModel.h
//  pd2
//
//  Created by Andrey on 19/01/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXBaseModel.h"

@interface DomainCompanyModel : AXBaseModel

@property (nonatomic, strong) NSString *subdomain;
@property (nonatomic, strong) NSString *domain;
@property (nonatomic, strong) NSString *companyName;

- (instancetype)initWithDomain:(NSString *)domain subdomain:(NSString *)subdomain companyName:(NSString *)companyName;

@end
