//
//  SplashViewController.m
//  pd2
//
//  Created by i11 on 25/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "SplashViewController.h"
#import "UserManager.h"
#import "DomainCompanyModel.h"

#import "WL.h"

@interface SplashViewController ()


@property (assign, nonatomic) BOOL screenPerformed;

@property (strong, nonatomic) NSTimer *responceTimeOutTimer;


@property (strong, nonatomic) NSNumber *whiteLabelIndex;

@property (assign, nonatomic) BOOL domainWait;
@property (assign, nonatomic) BOOL usersWait;


@end

@implementation SplashViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.whiteLabelIndex = [self getCustomWhiteLabelNumber];
    self.whiteLabelIndex = [WL index];

    self.logoImageView.image = [UIImage imageNamed:[WL logoSplashFileName]];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    
    self.screenPerformed = NO;
    
    //Именно в will appear. Иначе в окрашивается в другой цвет
    self.view.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
    
    if ([[MyColorTheme sharedInstance] activityIndicatorLightContent]) {
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    }
    else {
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    }
    
    self.logoTop.constant = [WL logoSplashRect].origin.y;
    self.logoWidth.constant = [WL logoSplashRect].size.width;
    self.logoHeight.constant = [WL logoSplashRect].size.height;

    
}

- (void)viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];
    
    // Preloads keyboard so there's no lag on initial keyboard appearance.
    UITextField *lagFreeField = [[UITextField alloc] init];
    [self.view addSubview:lagFreeField];
    [lagFreeField becomeFirstResponder];
    [lagFreeField resignFirstResponder];
    [lagFreeField removeFromSuperview];
    
    
    if (self.whiteLabelIndex) {
        [NetworkManager sharedInstance].isWhiteLabel = YES;
        [self domainsRequest];
    }
    
    
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self
           selector:@selector(usersIsChecked:)
               name:AllUsersHasBeenRecivePinAvailabilityNotification
             object:nil];
    
    //иницализируем и стартуем проверку всех юзеров в userDefaults
    [[UserManager sharedManager] readUsersAndRecievePin]; //Запускаем чтение и проверку
    self.usersWait = YES;
    
#warning проверить на возможность удаления нижней проверки
    //Если время на запросы не требуется, то сразу "готовность". Иначе ждем notification
    NSLog(@"users = %@", [[UserManager sharedManager] users]);
    if  ([[UserManager sharedManager] isAllUsersRecivePinAvailability]) {
        [self usersIsChecked:nil];
    }
    
    
    
    
    //Если latency большое - показываем логин экран
    self.responceTimeOutTimer = [NSTimer scheduledTimerWithTimeInterval:20 target:self selector:@selector(responceTimeOut) userInfo:nil repeats:NO];

}

/*
-(void)viewWillLayoutSubviews{
    
    [super viewWillLayoutSubviews];
    
    //[self.view setFrame:CGRectMake(100, 100, 100, 100)];
    //[self.logoImageView setFrame:CGRectMake(100, 100, 100, 100)];

}
*/

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [self.responceTimeOutTimer invalidate];
}


- (void)domainsRequest {
    
    
    __weak __typeof(self)weakSelf = self;
    
    self.domainWait = YES;
    
    [[NetworkManager sharedInstance] getDomainsListFor:[self.whiteLabelIndex intValue] withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            /*
            {
                "config": {
                    "x": 0,
                    "y": 1,
                    "z": 2,
                    "b": 3
                },
                "domains": {
                    "browzero.com": {
                        "dom0": true,
                        "dom1": true,
                        "dom2": true,
                        "dom3": true
                    }
                },
                "domains_company": {
                    "sdnapp.co.uk": {
                         "starsdogsthorpe": "starsdogsthorpe",
                         "brightstarspeterborough": "brightstarspeterborough",
                         "littlestarspeterborough": "littlestarspeterborough"
                    },
                    "some.co.uk": {
                         "sdnapp4": "sdnapp4",
                         "sdnapp5": "sdnapp5",
                         "sdnapp6": "sdnapp6",
                         "shootingstarspeterborough": "shootingstarspeterborough"
                    }
                }
            }
            */
            
            NSDictionary *domains = [response valueForKeyPath:@"domains_company"];
            if (domains && [domains isKindOfClass:[NSDictionary class]]) {
                
                NSMutableArray *models = [[NSMutableArray alloc] init];
                for (NSString *key in domains.allKeys) {
                    
                    NSString *baseDomain = key;
                    NSDictionary *subDict = [domains objectForKey:key];
                    if (subDict && [subDict isKindOfClass:[NSDictionary class]]) {
                        for (NSString *subKey in subDict.allKeys) {
                            NSString *subDomain = subKey;
                            NSString *value = [subDict objectForKey:subKey];
                            DomainCompanyModel *model = [[DomainCompanyModel alloc] initWithDomain:baseDomain subdomain:subDomain companyName:value];
                            if (model && [model isExist]) {
                                [models addObject:model];
                            }
                        }
                    }
                    
                    //[subDomains sortUsingSelector:@selector(compare:)];
                    /*
                    [NetworkManager sharedInstance].whiteLabelDomain = baseDomain;
                    [NetworkManager sharedInstance].whiteLabelSubdomains = subDomains;
                    NSLog(@"base domain: %@", baseDomain);
                    NSLog(@"Custom subdomains: %@", subDomains);
                    break;
                    */
                }
                
                NSSortDescriptor *companyNameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"companyName"
                                                                                       ascending:YES
                                                                                        selector:@selector(localizedCaseInsensitiveCompare:)];
                
                NSArray *sortDescriptors = [NSArray arrayWithObjects:companyNameSortDescriptor, nil];
                
                [NetworkManager sharedInstance].whiteLabelSubdomains = [models sortedArrayUsingDescriptors:sortDescriptors];

            }
            
            strongSelf.domainWait = NO;
            [strongSelf completionRequests];
            
            
            
            
                    }
        else if(error)
        {
            NSLog(@"Request Error");
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }

            [NetworkManager sharedInstance].whiteLabelDomain = nil;
            [NetworkManager sharedInstance].whiteLabelSubdomains = nil;
            
            strongSelf.domainWait = NO;
            [strongSelf completionRequests];
        }
    }];
    
    
    
}

-(void)completionRequests {
    
    
    if (self.usersWait || self.domainWait) {
        return;
    }
    
    self.screenPerformed = YES;
    
    NSLog(@"users is checked");
    NSLog(@"%@", ([[UserManager sharedManager] usersWithPin]));
    
    if ([[UserManager sharedManager] isUsersWithPin]) {
        //domain set for WL!
        [self performSegueWithIdentifier:@"enterPinSegue" sender:self];
        NSLog(@"have users with pin");
    } else {
        [self performSegueWithIdentifier:@"loginSegue" sender:self];
        NSLog(@"NO have users with pin");
    }
    
}

- (void)usersIsChecked:(NSNotification *)userInfo {
    
    
    
    self.usersWait = NO;
    
    [self completionRequests];
    
    /*
     
    self.screenPerformed = YES;
     
    NSLog(@"users is checked");
    NSLog(@"%@", ([[UserManager sharedManager] usersWithPin]));

    if ([[UserManager sharedManager] isUsersWithPin]) {
        [self performSegueWithIdentifier:@"enterPinSegue" sender:self];
        NSLog(@"have users with pin");
    } else {
        [self performSegueWithIdentifier:@"loginSegue" sender:self];
        NSLog(@"NO have users with pin");
    }
    */
}

- (void)responceTimeOut {

#warning оттестировать
    //Если ответ от сервера при опросе пинов занимает слишком долгое время - переходить на следующий экран
    
    NSLog(@"timer!");
    
    if (!self.screenPerformed) {
        self.screenPerformed = YES;
        NSLog(@"Timer perform screen");
        [self performSegueWithIdentifier:@"loginSegue" sender:self];
    }
    
}


- (void)setColorTheme {
    [super setColorTheme];
}


- (NSNumber *)getCustomWhiteLabelNumber {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"wlNum" ofType:@"txt"];
    NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    
    if (content && [content length]) {
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        return [f numberFromString:content];
    }
    
    return nil;
    
}

@end
