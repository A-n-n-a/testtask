//
//  NSString+VersionNumbers.m
//  pd2
//
//  Created by i11 on 21/11/15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import "NSString+VersionNumbers.h"

@implementation NSString (VersionNumbers)


- (NSString *)shortenedVersionNumberString {
    static NSString *const unnecessaryVersionSuffix = @".0";
    NSString *shortenedVersionNumber = self;
    
    while ([shortenedVersionNumber hasSuffix:unnecessaryVersionSuffix]) {
        shortenedVersionNumber = [shortenedVersionNumber substringToIndex:shortenedVersionNumber.length - unnecessaryVersionSuffix.length];
    }
    
    return shortenedVersionNumber;
}

@end
