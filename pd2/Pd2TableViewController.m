//
//  Pd2TableViewController.m
//  pd2
//
//  Created by admin on 6/22/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "Pd2TableViewController.h"
#import "TimerUIApplication.h"
#import "DataStateManager.h"

#import "MainMenuVC.h"

#import "WL.h"


//#define UPLOAD_IMAGE_CELL_ACTION_SHEET 17500

@interface Pd2TableViewController ()

@property (strong, nonatomic) NSTimer *timeOutCheatTimer;

@property (weak, nonatomic) UIButton *leftButton;
@property (weak, nonatomic) UIButton *rightButton;

//UploadImageCell
@property (nonatomic, copy) void (^selectUploadImageCompletion)(UIImage *image);
@property (nonatomic, weak) UIImageView *uploadImageView;

@end

@implementation Pd2TableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar.layer setBorderWidth: 0];
    NSUInteger ind = [self.navigationController.viewControllers indexOfObject:self];
    NSLog(@"index of this view is %lu",(unsigned long)ind);
    if (ind!=NSNotFound&&ind>1)
    {
        UIButton *leftButton=[UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *backImage=[[UIImage imageNamed:@"back-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIImageView *iv=[[UIImageView alloc]initWithImage:backImage];
        iv.frame=CGRectMake(5, 5, 20, 20);
        [leftButton addSubview:iv];
        leftButton.frame=CGRectMake(0, 0, 30, 30);
        [leftButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        leftButton.tintColor=[UIColor whiteColor];
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
        self.leftButton = leftButton;
        
        UIButton *rightButton=[UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *homeImage=[[UIImage imageNamed:@"home-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIImageView *ivHome=[[UIImageView alloc]initWithImage:homeImage];
        ivHome.frame=CGRectMake(5, 5, 20, 20);
        [rightButton addSubview:ivHome];
        rightButton.frame=CGRectMake(0, 0, 30, 30);
        [rightButton addTarget:self action:@selector(popToMenuAction:) forControlEvents:UIControlEventTouchUpInside];
        rightButton.tintColor=[UIColor whiteColor];
        self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:rightButton];
        self.rightButton = rightButton;
        
        [self setTopTitle:_topTitle];
        /*
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
         */
    }
    [self setColorTheme];
    
    
    
}



-(void)setTopTitle:(NSString *)topTitle
{
    _topTitle=topTitle;
 
    CGFloat barButtonWidth = 60;
    UIView * view = [[UIView alloc] initWithFrame:(CGRect){
        .origin = {barButtonWidth,0},
        .size   = {[UIScreen mainScreen].bounds.size.width - barButtonWidth*2, self.navigationController.navigationBar.frame.size.height}
    }];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:view.bounds];
    titleLabel.text          = _topTitle.length?_topTitle:[WL name];
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.minimumScaleFactor = 0.5;
    titleLabel.font          = [UIFont boldSystemFontOfSize:23];
    titleLabel.textColor     = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [view addSubview:titleLabel];
    self.navigationItem.titleView = view;

}


//до того часу як не вирішиться питання що робити з довгими title
-(void)setTopTitle:(NSString *)topTitle withFontSize:(CGFloat)fontSize
{
    _topTitle=topTitle;
    
    CGFloat barButtonWidth = 60;
    UIView * view = [[UIView alloc] initWithFrame:(CGRect){
        .origin = {barButtonWidth,0},
        .size   = {[UIScreen mainScreen].bounds.size.width - barButtonWidth*2, self.navigationController.navigationBar.frame.size.height}
    }];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:view.bounds];
    titleLabel.text         = _topTitle.length?_topTitle:[WL name];
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    titleLabel.font          = [UIFont boldSystemFontOfSize:fontSize];
    titleLabel.textColor     = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [view addSubview:titleLabel];
    self.navigationItem.titleView = view;
    
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //Из-за существующей проблемы некорректного скроллинга клавиатуры к textFields - подписываемся на получение уведомлений и в самих классах контроллеров реализуем куда прыгать. Пример CreateAccidentVC
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    /*
    self.view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    self.navigationController.navigationBar.barTintColor = [[MyColorTheme sharedInstance] backgroundColor];
    self.rightButton.tintColor=[[MyColorTheme sharedInstance]navigaionButtonsColor];
    self.leftButton.tintColor=[[MyColorTheme sharedInstance]navigaionButtonsColor];
    */
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: animated];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    
    if (self.timeOutCheatTimer) {
        [self.timeOutCheatTimer invalidate];
    }
}


-(void)setColorTheme
{
    [[MyColorTheme sharedInstance]setColorThemeForViewController:self];
}

-(void)keyboardWillShow:(id)sender
{
    
}

-(void)keyboardWillHide:(id)sender
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - InactiveTimer

- (void)disableInactiveTimer {
    
    //Сбрасываем таймер приложения
    int timeout = kApplicationTimeoutInMinutes * 60 - 1;
    //self.timeOutCheatTimer = [NSTimer scheduledTimerWithTimeInterval:timeout target:self selector:@selector(timerAction) userInfo:nil repeats:NO];
}

- (void)timerAction {
    //[(TimerUIApplication *)[UIApplication sharedApplication] resetIdleTimer];
}

#pragma mark - Border and Lines for View

- (void)addBorder:(PDBorderType)borderType toView:(UIView *)view forToolbar:(BOOL)toolbar
{
    if (borderType == PDBorderTop) {
        [self addTopLineToView:view forToolbar:toolbar];
        
    } else if (borderType == PDBorderBottom) {
        [self addBottomLineToView:view dynamic:NO lightColor:NO shortLine:NO forToolbar:toolbar];

    } else if (borderType == PDBorderBottomLight) {
        [self addBottomLineToView:view dynamic:NO lightColor:YES shortLine:NO forToolbar:toolbar];
        
    } else if (borderType == PDBorderBottomDynamic) {
        [self addBottomLineToView:view dynamic:YES lightColor:NO shortLine:NO forToolbar:toolbar];
    
    } else if (borderType == PDBorderBottomDynamicShort) {
        [self addBottomLineToView:view dynamic:YES lightColor:NO shortLine:YES forToolbar:toolbar];
        
    } else if (borderType == PDBorderBottomDynamicLight) {
        [self addBottomLineToView:view dynamic:YES lightColor:YES shortLine:NO forToolbar:toolbar];
    
    } else if (borderType == PDBorderBottomDynamicLightShort) {
        [self addBottomLineToView:view dynamic:YES lightColor:YES shortLine:YES forToolbar:toolbar];
    
    } else if (borderType == PDBorderTopAndBottom) {
        [self addTopLineToView:view forToolbar:toolbar];
        [self addBottomLineToView:view dynamic:NO lightColor:NO shortLine:NO forToolbar:toolbar];
        
    } else if (borderType == PDBorderTopAndBottomDynamic) {
        [self addTopLineToView:view forToolbar:toolbar];
        [self addBottomLineToView:view dynamic:YES lightColor:NO shortLine:NO forToolbar:toolbar];
    
    } else if (borderType == PDBorderLeft) {
        [self addLeftLineToView:view forToolbar:toolbar];
    
    } else if (borderType == PDBorderRightDynamic) {
        [self addRightLineToView:view dynamic:YES forToolbar:toolbar];

    } else if (borderType == PDBorderRight) {
        [self addRightLineToView:view dynamic:NO forToolbar:toolbar];
    }
}

- (void)addTopLineToView:(UIView *)view forToolbar:(BOOL)toolbar {
    
    CALayer *existLineLayer = [view.layer valueForKey:TOP_LINE_LAYER_NAME];
    
    if (!existLineLayer) {
        
        CALayer *lineLayer = [CALayer layer];
        
        lineLayer.frame = CGRectMake(0.0f, 0.0f, CGRectGetWidth(view.frame), LINE_WIDTH);
        lineLayer.backgroundColor = toolbar ? LINE_COLOR.CGColor : [MyColorTheme sharedInstance].onePxBorderColor.CGColor;
        lineLayer.name = TOP_LINE_LAYER_NAME;
        
        [view.layer addSublayer:lineLayer];
        [view.layer setValue:lineLayer forKey:TOP_LINE_LAYER_NAME];

        //NSLog(@"Top line added");
    } else {
        //NSLog(@"Top line exists");
    }
}

- (void)addBottomLineToView:(UIView *)view dynamic:(BOOL)dynamic lightColor:(BOOL)light shortLine:(BOOL)shortLine forToolbar:(BOOL)toolbar {
    
    //Для не статичных view привязываем через constraints к нижней части (медленно)
    if (dynamic) {
        
        UIView *existLineView = [view viewWithTag:BOTTOM_LINE_VIEW_TAG];
        
        if (!existLineView) {
            
            UIView *lineView = [[UIView alloc] init];
            lineView.translatesAutoresizingMaskIntoConstraints = NO;
            
            if (toolbar) {
                lineView.backgroundColor = light ? LINE_LIGHT_COLOR : LINE_COLOR;
            } else {
                lineView.backgroundColor = [MyColorTheme sharedInstance].onePxBorderColor;
            }
            
            
            lineView.tag = BOTTOM_LINE_VIEW_TAG;
            
            [view addSubview:lineView];
            
            if (shortLine) {
                [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                                 attribute:NSLayoutAttributeLeading
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:view
                                                                 attribute:NSLayoutAttributeLeading
                                                                multiplier:1
                                                                  constant:LINE_MARGIN]];
                
                [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                                 attribute:NSLayoutAttributeWidth
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:view
                                                                 attribute:NSLayoutAttributeWidth
                                                                multiplier:1
                                                                  constant:-LINE_MARGIN*2]];
                
            }
            else {
                [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                                 attribute:NSLayoutAttributeWidth
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:view
                                                                 attribute:NSLayoutAttributeWidth
                                                                multiplier:1
                                                                  constant:0]];
                
            }
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1
                                                              constant:LINE_WIDTH]];

            [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeBottom
                                                            multiplier:1
                                                              constant:0]];
        }
        
        //[view setNeedsUpdateConstraints];
        //[view updateConstraints];
        
    } else {
        //Для статичных view рисуем через CALayer (быстро)
        
        CALayer *existLineLayer = [view.layer valueForKey:BOTTOM_LINE_LAYER_NAME];
        
        if (!existLineLayer) {
            CGFloat height = CGRectGetHeight(view.frame);
            
            CALayer *lineLayer = [CALayer layer];
            if (shortLine) {
                 lineLayer.frame = CGRectMake(LINE_MARGIN, height - LINE_WIDTH, CGRectGetWidth(view.frame) - LINE_MARGIN * 2, LINE_WIDTH);
            }
            else {
                lineLayer.frame = CGRectMake(0.0f, height - LINE_WIDTH, CGRectGetWidth(view.frame), LINE_WIDTH);
            }
            
            if (toolbar) {
                lineLayer.backgroundColor = light ? LINE_LIGHT_COLOR.CGColor : LINE_COLOR.CGColor;
            } else {
                lineLayer.backgroundColor = [MyColorTheme sharedInstance].onePxBorderColor.CGColor;
            }
            
            lineLayer.name = BOTTOM_LINE_LAYER_NAME;
            
            [view.layer addSublayer:lineLayer];
            [view.layer setValue:lineLayer forKey:BOTTOM_LINE_LAYER_NAME];
        }
    }
    
}

- (void)addLeftLineToView:(UIView *)view forToolbar:(BOOL)toolbar {
    
    CALayer *existLineLayer = [view.layer valueForKey:LEFT_LINE_LAYER_NAME];
    
    if (!existLineLayer) {
        
        CALayer *lineLayer = [CALayer layer];
        
        lineLayer.frame = CGRectMake(0.0f, 0.0f, LINE_WIDTH, CGRectGetHeight(view.frame));
        lineLayer.backgroundColor = toolbar ? LINE_COLOR.CGColor : [MyColorTheme sharedInstance].onePxBorderColor.CGColor;
        lineLayer.name = LEFT_LINE_LAYER_NAME;
        
        [view.layer addSublayer:lineLayer];
        [view.layer setValue:lineLayer forKey:LEFT_LINE_LAYER_NAME];
        
        //NSLog(@"Top line added");
    } else {
        //NSLog(@"Top line exists");
    }
}

- (void)addRightLineToView:(UIView *)view dynamic:(BOOL)dynamic forToolbar:(BOOL)toolbar {
    
    //Для не статичных view привязываем через constraints к нижней части (медленно)
    if (dynamic) {
        
        UIView *existLineView = [view viewWithTag:RIGHT_LINE_VIEW_TAG];
        
        if (!existLineView) {
            
            UIView *lineView = [[UIView alloc] init];
            lineView.translatesAutoresizingMaskIntoConstraints = NO;
            lineView.backgroundColor = toolbar ? LINE_COLOR : [MyColorTheme sharedInstance].onePxBorderColor;
            lineView.tag = RIGHT_LINE_VIEW_TAG;
            
            [view addSubview:lineView];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1
                                                              constant:LINE_WIDTH]];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0]];

            [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeBottom
                                                            multiplier:1
                                                              constant:0]];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                             attribute:NSLayoutAttributeRight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeRight
                                                            multiplier:1
                                                              constant:0]];
        
        }
        
        //[view setNeedsUpdateConstraints];
        //[view updateConstraints];
        
    } else {
    
        CALayer *existLineLayer = [view.layer valueForKey:RIGHT_LINE_LAYER_NAME];
        
        if (!existLineLayer) {
            
            CALayer *lineLayer = [CALayer layer];
            
            lineLayer.frame = CGRectMake(CGRectGetWidth(view.frame) - LINE_WIDTH, 0.0f, LINE_WIDTH, CGRectGetHeight(view.frame));
            lineLayer.backgroundColor = toolbar ? LINE_COLOR.CGColor : [MyColorTheme sharedInstance].onePxBorderColor.CGColor;
            lineLayer.name = RIGHT_LINE_LAYER_NAME;
            
            [view.layer addSublayer:lineLayer];
            [view.layer setValue:lineLayer forKey:RIGHT_LINE_LAYER_NAME];
            
            //NSLog(@"Top line added");
        } else {
            //NSLog(@"Top line exists");
        }
    }
}

/*
- (void)addBottomLineToView:(UIView *)view dynamic:(BOOL)dynamic {
 
    CALayer *existLineLayer = [view.layer valueForKey:BOTTOM_LINE_LAYER_NAME];
 
    if (dynamic) {
        [existLineLayer removeFromSuperlayer];
        [view.layer setValue:nil forKey:BOTTOM_LINE_LAYER_NAME];
        existLineLayer = nil;
 
        __weak UIView *weakView = view;
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

            //Переодически вылетает при обращении к памяти
            if (weakView) {
                CGFloat height = [weakView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    CALayer *lineLayer = [CALayer layer];
                    lineLayer.frame = CGRectMake(0.0f, height - LINE_WIDTH, CGRectGetWidth(weakView.frame), LINE_WIDTH);
                    lineLayer.backgroundColor = LINE_CGCOLOR;
                    lineLayer.name = BOTTOM_LINE_LAYER_NAME;
                    
                    [weakView.layer addSublayer:lineLayer];
                    [weakView.layer setValue:lineLayer forKey:BOTTOM_LINE_LAYER_NAME];
                });

            }
        });

    } else {

        CGFloat height = CGRectGetHeight(view.frame);

        CALayer *lineLayer = [CALayer layer];
        lineLayer.frame = CGRectMake(0.0f, height - LINE_WIDTH, CGRectGetWidth(view.frame), LINE_WIDTH);
        lineLayer.backgroundColor = LINE_CGCOLOR;
        lineLayer.name = BOTTOM_LINE_LAYER_NAME;
        
        [view.layer addSublayer:lineLayer];
        [view.layer setValue:lineLayer forKey:BOTTOM_LINE_LAYER_NAME];

    }

}
*/

/*
- (void)addBottomLineToView:(UIView *)view dynamic:(BOOL)dynamic {
    
    CALayer *existLineLayer = [view.layer valueForKey:BOTTOM_LINE_LAYER_NAME];
    
    if (dynamic && existLineLayer) {
        [existLineLayer removeFromSuperlayer];
        existLineLayer = nil;
    }
    
    if (!existLineLayer) {
    
        CGFloat height = dynamic ? [view systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height : CGRectGetHeight(view.frame);

        CALayer *lineLayer = [CALayer layer];
        lineLayer.frame = CGRectMake(0.0f, height - LINE_WIDTH, CGRectGetWidth(view.frame), LINE_WIDTH);
        lineLayer.backgroundColor = LINE_CGCOLOR;
        lineLayer.name = BOTTOM_LINE_LAYER_NAME;
        
        [view.layer addSublayer:lineLayer];
        [view.layer setValue:lineLayer forKey:BOTTOM_LINE_LAYER_NAME];
    
       
        //NSLog(@"Bottom line added");
    } else {
        //NSLog(@"Bottom line exists");
    }
    
}
*/

#pragma mark - Table view data source

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}
*/
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void) pushViewController:(NSString *)viewControllerIdentifier
                 storyboard:(NSString *)storyboardName
{
    @try {
        UIStoryboard * nextStepsStoryboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
        UIViewController * viewController = [nextStepsStoryboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    @catch (NSException *exception) {}
}

- (UIView *) viewFromCell:(NSString *) cellIdentifier
{
    UIView * view = nil;
    @try {
        view = [[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier] valueForKey:@"contentView"];
    }
    @catch (NSException *exception) {}
    return view;
}


- (void)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)popToMenuAction:(id)sender {
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        if ([controller isKindOfClass:[MainMenuVC class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            return;
        }
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

#pragma mark - UploadImageCell
//Andrey

- (void)uploadImageActionForImageView:(UIImageView *)imageView withCompletion:(void(^)(UIImage *image))completion {
    
    self.selectUploadImageCompletion = completion;
    self.uploadImageView = imageView;
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Please select photo source"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:@"Take photo from camera",@"Choose photo from library", nil];
    sheet.tag = UPLOAD_IMAGE_CELL_ACTION_SHEET;
    [sheet showInView:self.view];
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.tag == UPLOAD_IMAGE_CELL_ACTION_SHEET) {
        
        if (buttonIndex==0)//camera
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            
            //На симуляторах камера не доступна
            @try {
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.delegate = self;
                [self.navigationController presentViewController:picker animated:YES completion:nil];
            }
            @catch (NSException *exception) {
                NSLog(@"Exception. Reason: %@", exception.reason);
            }
        }
        else if(buttonIndex==1)//library
        {
            UIImagePickerController *picker=[[UIImagePickerController alloc]init];
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.delegate = self;
            [self.navigationController presentViewController:picker animated:YES completion:nil];
        }
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    image = [[Utils sharedInstance] imageScaledToRegularVisitorImage:image];
    
    
    self.uploadImageView.image = image;
    if (self.selectUploadImageCompletion) {
        self.isImageChanged = YES;
        self.selectUploadImageCompletion(image);
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}




#pragma mark - Pd2TableViewCellDelegate -

- (void) pd2TableViewCell:(Pd2TableViewCell *)cell beginEditingField:(id)field
{
    UIView * _field = field;
    CGPoint p = [_field.superview convertPoint:_field.frame.origin toView:self.view];
    [self.tableView setContentOffset:(CGPoint){0, (p.y+_field.frame.size.height-200)} animated:YES];
}



#pragma mark - device orientations

//для видео секций
- (void)doPortrait {
    
    if ([AppDelegate isAVFullScreenViewController]) {
        return;
    }
    
    if ([AppDelegate isPortrait]) {
        return;
    }
    
    NSLog(@"rotate!");
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    
}

#pragma mark -


- (BOOL)isScottish {
    
    return [[NetworkManager sharedInstance] isScottish];
    
    
}


#pragma mark - Push Failure alert
//Алерт о некорректном переходе по пушу. Когда не удалось что то подгрузить
- (void)showPushFailureAlert {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Something went wrong" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    
}


#pragma mark - localize

- (void)localize:(NSString *)string tag:(int)tag view:(UIView *)view {
    
    
    id desiredView = [view viewWithTag:tag];
    if (!desiredView) {
        NSLog(@"LOCALIZE: view with tag: %d for: %@ NOT FOUND", tag, string);
        return;
    }
    
    if ([desiredView isKindOfClass:[UILabel class]]) {
        UILabel *label = desiredView;
        label.text = NSLocalizedString(string, nil);
        return;
    }
    
    
    NSLog(@"LOCALIZE: view with tag: %d for: %@ NOT FOUND", tag, string);
    
}


#pragma mark - AgeInMonth

- (NSString*)getAgeInMonthFromDateString:(NSString*)dateString
{
    NSString *ageInMonth = [NSString new];
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd.MM.yyyy"];
    NSDate *childDate = [formatter dateFromString:dateString];
    NSDateComponents *componentsAgeMonths = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:childDate toDate:[NSDate date] options:0];
    ageInMonth = [NSString stringWithFormat:@"%d %@", (int)componentsAgeMonths.month, componentsAgeMonths.month==1?@"Month":@"Months"];
    return ageInMonth;
}


@end
