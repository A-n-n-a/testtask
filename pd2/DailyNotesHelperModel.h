//
//  DailyNotesHelperModel.h
//  pd2
//
//  Created by Andrey on 08/03/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXBaseModel.h"


@interface DailyNotesHelperModel : AXBaseModel


@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSArray *values;

- (instancetype)initWithDict:(NSDictionary *)dict;


@end
