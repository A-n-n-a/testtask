//
//  AXSections.m
//  pd2
//
//  Created by i11 on 10/02/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AXSections.h"
#import "AXSection.h"
#import "AXHeader.h"
#import "AXRow.h"


typedef enum {
    AXSectionsRow,
    AXSectionsHeader,
    AXSectionSection
} AXSectionsObject;

@interface AXSections()

@property (strong, nonatomic) NSMutableDictionary *remembered; //rows, headers (for gen)

@end


@implementation AXSections

//TODO:
//В section и row как то хранить (получать) для них текущий indexPath, index

- (instancetype)init {
    
    if (self = [super init])
    {
        self.sections = [[NSMutableArray alloc] init];
        self.remembered = [[NSMutableDictionary alloc] init];
    }
    return self;
}

#pragma mark - AXSectionDelegate

- (NSInteger)indexForSection:(AXSection *)section {
    return [self.sections indexOfObject:section];
}


#pragma mark - standart add / get

- (void)addSection:(AXSection *)section {
    section.delegate = self;
    [self.sections addObject:section];
}

- (void)addSections:(NSArray *)sections {

    for (AXSection *section in sections) {
        [self addSection:section];
    }
    
}

- (void)removeSections {
    self.sections = [[NSMutableArray alloc] init];
}

- (BOOL)removeSectionsByIndexSet:(NSIndexSet *)indexSet {

    if ([indexSet count] == 0) {
        return NO;
    } else {
        [self.sections removeObjectsAtIndexes:indexSet];
        return YES;
    }
}

- (AXSection *)sectionAtIndex:(NSUInteger)index {
    
    return [self.sections objectAtIndex:index];
}

- (NSArray *)sectionsAtIndexSet:(NSIndexSet *)indexSet {
    
    return [self.sections objectsAtIndexes:indexSet];
    
}

- (AXSection *)sectionAtIndexPath:(NSIndexPath *)indexPath {
    
    return [self.sections objectAtIndex:indexPath.section];
}

- (AXSection *)sectionWithSectionType:(NSString *)sectionType {
    
    __block AXSection *desiredSection = nil;
    
    [self.sections enumerateObjectsUsingBlock:^(AXSection *section, NSUInteger idx, BOOL *stop) {
        if ([section.type isEqualToString:sectionType]) {
            desiredSection = section;
            *stop = YES;
        }
    }];
    
    return desiredSection;
    
}

- (AXRow *)rowAtIndexPath:(NSIndexPath *)indexPath {
    
    AXSection *section = [self.sections objectAtIndex:indexPath.section];
    
    if ([section.rows count] > indexPath.row) {
        return [section.rows objectAtIndex:indexPath.row];
    }
    else {
        return nil;
    }
    
    
}

- (BOOL)isRowAvailableAtIndexPath:(NSIndexPath *)indexPath {
    AXRow *row = [self rowAtIndexPath:indexPath];
    AXSection *section = [self sectionAtIndexPath:indexPath];
    
    if (row && !section.isHide) {
        return YES;
    }
    
    return NO;
}

- (NSArray *)rowsAtIndexPaths:(NSArray *)indexPaths {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (NSIndexPath *indexPath in indexPaths) {
        AXRow *row = [self rowAtIndexPath:indexPath];
        if (row) {
            [array addObject:row];
        }
    }
    
    return (NSArray *)array;
}

- (AXRow *)rowWithRowType:(NSString *)rowType {
    
    NSArray *indexPaths = [self getIndexPathsForRowType:rowType];
    if ([indexPaths count] > 0) {
        return [self rowAtIndexPath:[indexPaths firstObject]];
    } else {
        return nil;
    }
    
}


- (BOOL)isRowWithRowType:(NSString *)rowType {
    
    AXRow *row = [self rowWithRowType:rowType];
    if (row) {
        return YES;
    }
    else {
        return NO;
    }
    
}


- (AXHeader *)headerAtSectionIndex:(NSInteger)index {
    AXSection * section = [self.sections objectAtIndex:index];
    return section.header;
}

#pragma mark -

- (NSUInteger)countOfSections {
    return [self.sections count];
}

- (NSUInteger)countOfRowsInSection:(NSUInteger)index {
    AXSection *section = [self sectionAtIndex:index];
    
    if (section.isHide) {
        return 0;
    } else {
        return [section.rows count];
    }
}

#pragma mark -


- (CGFloat)heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    AXRow *row = [self rowAtIndexPath:indexPath];
    return [row height];
}

- (CGFloat)heightForHeaderInSection:(NSUInteger)index {
    AXHeader *header = [self headerAtSectionIndex:index];
    return header.height;
}

- (void)needUpdateHeightForRowType:(NSString *)rowType {
    
    if (!rowType) {
        return;
    }
    
    NSArray *rowsIndexPath = [self getIndexPathsForRowType:rowType];
    
    if (![rowsIndexPath count]) {
        return;
    }
    
    for (NSIndexPath *indexPath in rowsIndexPath) {
        AXRow *row = [self rowAtIndexPath:indexPath];
        [row needUpdateHeight];
    }
    
}

- (void)needUpdateHeightForRowTypes:(NSArray *)rowTypes {
    
    if (![rowTypes isKindOfClass:[NSArray class]]) {
        return;
    }
    
    for (NSString *rowType in rowTypes) {
        [self needUpdateHeightForRowType:rowType];
    }

}



#pragma mark -

- (NSString *)rowTypeAtIndexPath:(NSIndexPath *)indexPath {
    AXRow *row = [self rowAtIndexPath:indexPath];
    return row.type;
}

- (NSUInteger)getSectionIndexForView:(UIView *)view {
    for (NSUInteger index = 0; index < [self countOfSections]; index++) {
        AXSection *section = [self sectionAtIndex:index];
        AXHeader *header = section.header;
        if (header && [view isEqual:header.view]) {
            return index;
        }
    }
    return NSUIntegerMax;
}

#pragma mark -

- (NSArray *)getIndexPathsForRowWithModel:(id)model {
    
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    
    for (NSUInteger sectionIndex = 0; sectionIndex < [self.sections count]; sectionIndex++) {
        
        AXSection *section = [self.sections objectAtIndex:sectionIndex];
        for (NSUInteger rowIndex = 0; rowIndex < [section.rows count]; rowIndex ++) {
            
            AXRow *row = [section.rows objectAtIndex:rowIndex];
            if (row.model && [row.model isEqual:model]) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowIndex inSection:sectionIndex];
                [indexPaths addObject:indexPath];
            }
            
        }
        
    }
    
    return (NSArray *)indexPaths;
}


- (NSArray *)getIndexPathsForRowType:(NSString *)type {
    return [self getIndexPathsForRowType:type atIndex:AXRowAll withCondition:nil];
}

- (NSArray *)getIndexPathsNotHiddenForRowType:(NSString *)type {
    return [self getIndexPathsForRowType:type atIndex:AXRowAll withCondition:^BOOL(AXSection *section, AXRow *row) {
        return !section.isHide;
    }];
    
}


- (NSArray *)getIndexPathsForRowTypes:(NSArray *)rowTypes {
    
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    
    for (NSString *type in rowTypes) {
        
        NSArray *partArray = [self getIndexPathsForRowType:type];
        
        [indexPaths addObjectsFromArray:partArray];
    }
    
    return indexPaths;
}

- (NSArray *)getIndexPathsForRows:(NSArray *)rows {
    
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    
    if (!rows) {
        return indexPaths;
    }
    
    [rows enumerateObjectsUsingBlock:^(AXRow *row, NSUInteger idx, BOOL *stop) {
//TODO: для hide sections index path у таблицы будет не корректный
        [indexPaths addObject:row.indexPath];
    }];
    
    return indexPaths;
}

- (NSArray *)getIndexPathsForRowType:(NSString *)type atIndex:(id)index withCondition:(BOOL(^)(AXSection *section, AXRow *row))condition {
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    
    for (NSUInteger sectionIndex = 0; sectionIndex < [self.sections count]; sectionIndex++) {
        
        AXSection *section = [self.sections objectAtIndex:sectionIndex];
        for (NSUInteger rowIndex = 0; rowIndex < [section.rows count]; rowIndex ++) {
            
            AXRow *row = [section.rows objectAtIndex:rowIndex];
            if ([row.type isEqualToString:type]) {
                
                BOOL add = NO;
                
                if (condition) {
                    if (condition(section, row)) {
                        add = YES;
                    }
                } else {
                    add = YES;
                }
                
                if (add) {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowIndex inSection:sectionIndex];
                    [indexPaths addObject:indexPath];
                }
            }
            
        }
        
    }
    
    if ([indexPaths count] == 0) {
        return (NSArray *)indexPaths;
    }
    
    if ([index isKindOfClass:[NSString class]]) {
        if ([index isEqualToString:AXRowFirst]) {
            return  [NSArray arrayWithObject:[indexPaths firstObject]];
        }else if ([index isEqualToString:AXRowLast]) {
            return  [NSArray arrayWithObject:[indexPaths lastObject]];
        }
        else if ([index isEqualToString:AXRowAll]) {
            return (NSArray *)indexPaths;
        }
    } else {
        NSUInteger idx = (NSUInteger)index;
        if (idx <= [indexPaths count] - 1) {
            return  [NSArray arrayWithObject:[indexPaths objectAtIndex:idx]];
        }
    }
    
    return (NSArray *)indexPaths;
    
}


- (NSUInteger)getIndexForRowType:(NSString *)rowType atIndexPath:(NSIndexPath *)indexPath {
    NSArray *indexPaths = [self getIndexPathsForRowType:rowType];
    
    __block NSUInteger index = 0;
    
    [indexPaths enumerateObjectsUsingBlock:^(NSIndexPath *obj, NSUInteger idx, BOOL *stop) {
        
        if (obj.section == indexPath.section && obj.row == indexPath.row) {
            index = idx;
        }
    }];
    
    return index;
}

- (NSArray *)getIndexPathsForRowWithTag:(NSString *)tag {
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    
    for (NSUInteger sectionIndex = 0; sectionIndex < [self.sections count]; sectionIndex++) {
        
        AXSection *section = [self.sections objectAtIndex:sectionIndex];
        for (NSUInteger rowIndex = 0; rowIndex < [section.rows count]; rowIndex ++) {
            
            AXRow *row = [section.rows objectAtIndex:rowIndex];
            if ([row.tags containsObject:tag]) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowIndex inSection:sectionIndex];
                [indexPaths addObject:indexPath];
            }
            
        }
        
    }
    
    return (NSArray *)indexPaths;
    
}




- (void)removeArrayRowsByIndexPaths:(NSArray *)indexPaths {
    NSLog(@"Before:");
    [self debugPrint];
    [self debugPrintIndexPaths:indexPaths];
    
    //Помечаем к неиспользованию затроннутые в genIndexSetForSection indexPath
    NSMutableArray *usedIndexPath = [[NSMutableArray alloc] init];
    
    //Сортировать indexPath по section
    //TODO: Проверить (и мб сделать) для многосекционных!
    for (NSIndexPath *indexPath in indexPaths) {
        
        if ([usedIndexPath containsObject:indexPath]) {
            continue;
        }
        
        AXSection *section = [self.sections objectAtIndex:indexPath.section];
        NSIndexSet *removeIndexSet = [self genIndexSetForSection:indexPath.section fromIndexPaths:indexPaths withUsedIndexPaths:&usedIndexPath];
        [self debugPrintIndexSet:removeIndexSet];
        [section.rows removeObjectsAtIndexes:removeIndexSet];
    }
    NSLog(@"After");
    [self debugPrint];
}

//Разобраться что же я сделал
- (NSIndexSet *)genIndexSetForSection:(NSUInteger)section fromIndexPaths:(NSArray *)indexPaths withUsedIndexPaths:(NSMutableArray **)usedIndexPath {
    
    NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
    
    //TODO: переделать через предикат?
    
    for (NSIndexPath *indexPath in indexPaths) {
        if (indexPath.section == section) {
            [indexSet addIndex:indexPath.row];
            [*usedIndexPath addObject:indexPath];
        }
    }
    
    return indexSet;
}

- (NSIndexSet *)getIndexSetForSections:(NSArray *)sections {
    
    NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
    
    for (AXSection *section in sections) {
        [indexSet addIndex:section.sectionIndex];
    }
    
    return indexSet;
    
}



- (NSIndexSet *)getIndexSetForSectionsWithType:(NSString *)type {
//TODO: проверка на isHide?

    NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
    
    for (NSUInteger i = 0; i < [self.sections count]; i++) {
        AXSection *section = [self.sections objectAtIndex:i];
        if ([section.type isEqualToString:type]) {
            [indexSet addIndex:i];
        }
    }
    
    return (NSIndexSet *)indexSet;
    
}

- (NSIndexSet *)getIndexSetForSectionsWithTypes:(NSArray *)types {
    
    NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
    
    for (NSString *type in types) {
        NSIndexSet *requiredIndexSet = [self getIndexSetForSectionsWithType:type];
        [indexSet addIndexes:requiredIndexSet];
    }

    return indexSet;
    
}



- (NSIndexSet *)getIndexSetForSectionsWithTag:(NSString *)tag {

    return [self getIndexSetForSectionsWithTag:tag andExcludeTag:nil];
    
}

- (NSIndexSet *)getIndexSetForSectionsWithTag:(NSString *)tag andExcludeTag:(NSString *)excludeTag {
    
    //TODO: проверка на isHide?
    
    NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
    
    for (NSUInteger i = 0; i < [self.sections count]; i++) {
        AXSection *section = [self.sections objectAtIndex:i];
        if ([section.tags containsObject:tag]) {
            if (!excludeTag || (excludeTag && ![section.tags containsObject:excludeTag])) {
                [indexSet addIndex:i];
            }
        }
    }
    
    return (NSIndexSet *)indexSet;
}


- (NSIndexSet *)getIndexSetForSectionsWithTags:(NSSet *)tags {
    
    
    //TODO: проверка на isHide?
    
    NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
    
    for (NSUInteger i = 0; i < [self.sections count]; i++) {
        AXSection *section = [self.sections objectAtIndex:i];
        if ([section.tags isSubsetOfSet:tags]) {
            [indexSet addIndex:i];
        }
    }
    
    return (NSIndexSet *)indexSet;

}


#pragma mark - Range

- (NSArray *)getRangeIndexPathsFromRowType:(NSString *)type1 atIndex:(id)index1 toRowType:(NSString *)type2 atIndex:(id)index2 {
    
    NSMutableArray *rangeArray = [[NSMutableArray alloc] init];
    
    NSArray *indexPaths1 = [self getIndexPathsForRowType:type1 atIndex:index1 withCondition:nil];
    NSArray *indexPaths2 = [self getIndexPathsForRowType:type2 atIndex:index2 withCondition:nil];
    
    if ([indexPaths1 count] == 0 || [indexPaths2 count] == 0) {
        return nil;
    }
    
    NSIndexPath *indexPath1 = [indexPaths1 firstObject];
    NSIndexPath *indexPath2 = [indexPaths2 firstObject];
    
    
    for (NSUInteger sectionIndex = indexPath1.section; sectionIndex <= indexPath2.section; sectionIndex ++) {
        AXSection *section = [self sectionAtIndex:sectionIndex];

        NSUInteger rowStartIndex;
        if(sectionIndex == indexPath1.section) {
            rowStartIndex = indexPath1.row + 1;
        } else {
            rowStartIndex = 0;
        }
        
        for (NSUInteger rowIndex = rowStartIndex; rowIndex < [section.rows count]; rowIndex ++) {
            
            if ((sectionIndex < indexPath2.section ) ||
                (sectionIndex == indexPath2.section && rowIndex < indexPath2.row)) {
        
                [rangeArray addObject: [NSIndexPath indexPathForRow:rowIndex inSection:sectionIndex]];
            }
        }
    }

    return (NSArray *)rangeArray;
}


#pragma mark - insertSections

- (NSIndexSet *)insertSections:(NSArray *)sections afterSectionType:(NSString *)sectionType {
    //TODO: first, last, index
    
    NSIndexSet *desiredIndexSet = [self getIndexSetForSectionsWithType:sectionType]; //+ at index
    
    if ([desiredIndexSet count] == 0) {
        return [[NSIndexSet alloc] init]; //пустой
    }
    
    NSMutableArray *indexes = [[NSMutableArray alloc] init];
    
    [desiredIndexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [indexes addObject:[NSNumber numberWithUnsignedInteger:idx]];
    }];
    
    //Delegate
    for (AXSection *section in sections) {
        section.delegate = self;
    }
    
    //вынести выбор первого/второго последнего getIndexSetForSectionsWithType
    //для последнего
    NSUInteger startIndex = [[indexes lastObject] unsignedIntegerValue] + 1; //ставим на следующий
    //TODO проверить если в массиве всего 2, а ставим на 3-е место
    

    NSIndexSet *addedSectionsIndexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(startIndex, [sections count])];
    
    [self.sections insertObjects:sections atIndexes:addedSectionsIndexSet];
    
    return addedSectionsIndexSet;
    
}


- (NSIndexSet *)insertSections:(NSArray *)sections afterSectionIndex:(NSUInteger)sectionIndex {
    
    if (sectionIndex >= [self.sections count]) {
        return [[NSIndexSet alloc] init]; //пустой
    }
    
    //Delegate
    for (AXSection *section in sections) {
        section.delegate = self;
    }
    
    NSUInteger startIndex = sectionIndex + 1; //ставим на следующий

    
    
    NSIndexSet *addedSectionsIndexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(startIndex, [sections count])];
    
    [self.sections insertObjects:sections atIndexes:addedSectionsIndexSet];
    
    return addedSectionsIndexSet;
    
}


#pragma mark - insertRows: after

- (NSArray *)insertRows:(NSArray *)rows afterIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Before %@", [[self.sections objectAtIndex:indexPath.section] rows]);
    
    NSMutableArray *addedIndexPaths = [[NSMutableArray alloc] init];
    
    AXSection *section = [self.sections objectAtIndex:indexPath.section];
    
    //Delegate
    for (AXRow *row in rows) {
        row.delegate = section;
    }
    
    NSUInteger startIndex = indexPath.row + 1;
    NSIndexSet *addedRowsIndexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(startIndex, [rows count])];
    
    [section.rows insertObjects:rows atIndexes:addedRowsIndexSet];
    
    
    [addedRowsIndexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        NSLog(@"Section = %ld, Row = %lu", (long)indexPath.section, (unsigned long)idx);
        [addedIndexPaths addObject: [NSIndexPath indexPathForRow:idx inSection:indexPath.section]];
    }];

    NSLog(@"After %@", [[self.sections objectAtIndex:0] rows]);
    return (NSArray *)addedIndexPaths;
}



- (NSArray *)insertRows:(NSArray *)rows afterRowType:(NSString *)rowType atIndex:(id)index {
    NSLog(@"Before %@", [[self.sections objectAtIndex:0] rows]);
    NSMutableArray *addedIndexPaths = [[NSMutableArray alloc] init];
    
    NSArray *desiredIndexPaths = [self getIndexPathsForRowType:rowType atIndex:index withCondition:nil];
    
    //TODO:убрать условие по примеру выше
    if ([desiredIndexPaths count] > 0) {
        
        //
        NSIndexPath *desiredIndexPath = [desiredIndexPaths firstObject];
        
        AXSection *section = [self.sections objectAtIndex:desiredIndexPath.section];
        
        
        NSUInteger startIndex = desiredIndexPath.row + 1;
        NSIndexSet *addedRowsIndexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(startIndex, [rows count])];
        
        //Delegate
        for (AXRow *row in rows) {
            row.delegate = section;
        }
        
        [section.rows insertObjects:rows atIndexes:addedRowsIndexSet];
        
        [addedRowsIndexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            NSLog(@"Section = %ld, Row = %lu", (long)desiredIndexPath.section, (unsigned long)idx);
            [addedIndexPaths addObject: [NSIndexPath indexPathForRow:idx inSection:desiredIndexPath.section]];
        }];
    }
    NSLog(@"After %@", [[self.sections objectAtIndex:0] rows]);
    return (NSArray *)addedIndexPaths;
}

- (NSArray *)insertRows:(NSArray *)rows afterFirstRowType:(NSString *)rowType {
    return [self insertRows:rows afterRowType:rowType atIndex:AXRowFirst];
}

- (NSArray *)insertRows:(NSArray *)rows afterLastRowType:(NSString *)rowType {
    return [self insertRows:rows afterRowType:rowType atIndex:AXRowLast];
}


- (NSArray *)insertRows:(NSArray *)rows fromIndex:(NSInteger)index inSection:(AXSection *)section {
    
    NSLog(@"Before %@", [section rows]);
    NSMutableArray *addedIndexPaths = [[NSMutableArray alloc] init];
    
    
    for (AXRow *row in rows) {
        row.delegate = section;
    }
    
    NSIndexSet *addedRowsIndexSet = nil;
    
    if ([section.rows count] > index) {
        addedRowsIndexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(index, [rows count])];
    }
    else {
        NSLog(@"В секции всего %lu rows, требуемый старт %ld добавляется в конец", (unsigned long)[section.rows count], (long)index);
        addedRowsIndexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange([section.rows count], [rows count])];
    }

    [section.rows insertObjects:rows atIndexes:addedRowsIndexSet];
    
    [addedRowsIndexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        NSLog(@"Section = %ld, Row = %lu", section.sectionIndex, (unsigned long)idx);
        [addedIndexPaths addObject: [NSIndexPath indexPathForRow:idx inSection:section.sectionIndex]];
    }];

    
       NSLog(@"After %@", [section rows]);
    return (NSArray *)addedIndexPaths;
}



#pragma mark - DeleteRows: after

//TODO: Переименовать во что то типа getIndexPathsForRowsAfterRowType
- (NSArray *)deleteRowsAfterRowType:(NSString *)rowType {
    
    //TODO: сейчас удаляет только первый встреченный и только в одной секции. При необходимости - дописать
    
    NSMutableArray *deletingIndexPaths = [[NSMutableArray alloc] init];
    
    NSArray *desiredIndexPaths = [self getIndexPathsForRowType:rowType];
    
    if ([desiredIndexPaths count] > 0) {
        NSIndexPath *desiredIndexPath= [desiredIndexPaths firstObject];
        AXSection *section = [self.sections objectAtIndex:desiredIndexPath.section];
        NSUInteger startIndex = desiredIndexPath.row;
        
        for (NSUInteger index = startIndex + 1; index < [section.rows count]; index++) {
            [deletingIndexPaths addObject:[NSIndexPath indexPathForRow:index inSection:desiredIndexPath.section]];
        }
        
        //Вызываем отдельно по получении... Сделать все в одном?
        //[self removeArrayRowsByIndexPaths:deletingIndexPaths];
    }
    
    return (NSArray *)deletingIndexPaths;
    
}


#pragma mark - Replace Row

- (NSArray *)replaceOldRow:(AXRow *)oldRow toNewRow:(AXRow *)newRow {
    
    if (!oldRow || !newRow) {
        return [NSArray array];
    }
    
    AXSection *section = oldRow.section;
    NSUInteger index = [section.rows indexOfObject:oldRow];
    [section.rows replaceObjectAtIndex:index withObject:newRow];
    newRow.delegate = section;
    
    
    return @[newRow.indexPath];
    
}


#pragma mark - Remember

//Сохранение identifier
- (void)rememberType:(NSString *)type withHeight:(CGFloat)height identifier:(NSString *)identifier {
    
    NSDictionary *obj = [NSDictionary dictionaryWithObjectsAndKeys: [
                         NSNumber numberWithFloat:height], @"height",
                         identifier, @"identifier",
                         nil];
    
    [self.remembered setObject:obj forKey:type];
    
}

- (void)rememberType:(NSString *)type withEstimatedHeight:(CGFloat)estimatedHeight identifier:(NSString *)identifier {
    
    NSDictionary *obj = [NSDictionary dictionaryWithObjectsAndKeys: [
                         NSNumber numberWithFloat:estimatedHeight], @"estimatedHeight",
                         identifier, @"identifier",
                         nil];
    
    [self.remembered setObject:obj forKey:type];
    
}


//Шаблон для генерации
- (id)gen:(AXSectionsObject)genType withType:(NSString *)type {
    
    NSDictionary *remember = [self.remembered objectForKey:type];
    
    if (remember) {
        
        if (genType == AXSectionsRow) {
            AXRow *obj = [[AXRow alloc] initWithType:type];
            obj.identifier = [remember objectForKey:@"identifier"];
            obj.heightNumber = [remember objectForKey:@"height"];
            obj.estimatedHeightNumber = [remember objectForKey:@"estimatedHeight"];
            return obj;
        } else {
            AXHeader *obj = [[AXHeader alloc] initWithType:type];
            obj.identifier = [remember objectForKey:@"identifier"];
            obj.height = [[remember objectForKey:@"height"] floatValue];
            return obj;
        }
    
        /* //Выдает 0 высоту!
         CGFloat height = [[rememberRow objectForKey:@"height"] floatValue];
         if (abs(height - MAXFLOAT) < 0.05) {
         row.isDynamicHeight = YES;
         } else {
         row.height = height;
         }
         */
        
    } else {
        NSLog(@"error! %@ не известный gen", type);
    }
    
    return nil;
}


- (AXRow *)genRowWithType:(NSString *)rowType {
  
    return (AXRow *)[self gen:AXSectionsRow withType:rowType];
}

- (AXHeader *)genHeaderWithType:(NSString *)headerType {
    
    return (AXHeader *)[self gen:AXSectionsHeader withType:headerType];
    
}


- (void)debugPrint {

    int i = 0;
    for (AXSection *section in self.sections) {
        
        NSLog(@"Section %i %@", i, section.type);
        i++;
        
        int j = 0;
        for (AXRow *row in section.rows) {
            NSLog(@"| Row %i %@", j, row.type);
            j++;
        }
        
    }
}

- (void)debugPrintIndexSet:(NSIndexSet *)indexSet {
    NSLog(@"IndexSet: %@", indexSet);
}

- (void)debugPrintIndexPaths:(NSArray *)indexPaths {
    
    for (NSIndexPath *indexPath in indexPaths) {
        
        if ([indexPath isKindOfClass:[NSIndexPath class]]) {
            NSLog(@"IndexPath: {%ld, %ld}", (long)indexPath.section, (long)indexPath.row);
        }
        else {
            NSLog(@"Error! Not indexPath!");
        }
        
        
    }
}

@end
