//
//  DailyNotesChildrenNoteEditViewController.h
//  pd2
//
//  Created by Andrey on 17/01/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXTableViewController.h"

@class DailyNotesChildrenModel;

@interface DailyNotesChildrenNoteEditViewController : AXTableViewController

@property (nonatomic, strong) DailyNotesChildrenModel *childrenModel;

@property (nonatomic, assign) int noteId;
@property (nonatomic, strong) NSString *nameOrigString;
@property (nonatomic, strong) NSString *nameString;
@property (nonatomic, strong) NSString *noteString;

- (IBAction)sendAction:(id)sender;

@end
