//
//  PushNotificationModel.m
//  pd2
//
//  Created by Andrey on 04/10/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "PushNotificationModel.h"
#import "PushEYOModel.h"

@implementation PushNotificationModel


- (instancetype)initWithDict:(NSDictionary *)dict {
    self = [super initWithDict:dict];
    if (self) {
        
        self.host = [self safeString:dict[@"h"]];
        self.message = [self safeString:dict[@"txt"]];
        
        self.aols = [NSMutableArray array];
        
        NSString *action = [self safeString:dict[@"a"]];
        if (!action) {
            self.pushAction = PushNotificationAlert;
        }
        else if ([action isEqualToString:@"viewDiary"]) {
            self.pushAction = PushNotificationViewDiary;
        }
        else if ([action isEqualToString:@"viewDiaryComment"]) {
            self.pushAction = PushNotificationViewDiaryComment;
        }
        else if ([action isEqualToString:@"registerIn"]) {
            self.pushAction = PushNotificationRegisterIn;
        }
        else if ([action isEqualToString:@"registerOut"]) {
            self.pushAction = PushNotificationRegisterOut;
        }
        else if ([action isEqualToString:@"viewPermissions"]) {
            self.pushAction = PushNotificationPermissions;
        }
        else if ([action isEqualToString:@"viewPermissionsSign"]) {
            self.pushAction = PushNotificationPermissionsViewSign;
        }
        else if ([action isEqualToString:@"viewPolicies"]) {
            self.pushAction = PushNotificationPolicies;
        }
        else if ([action isEqualToString:@"viewPoliciesSign"]) {
            self.pushAction = PushNotificationPoliciesViewSign;
        }
        else if ([action isEqualToString:@"viewAccidentParent"] || [action isEqualToString:@"viewAccidentAdmin"]) {
            self.pushAction = PushNotificationAccidentView;
        }
        else if ([action isEqualToString:@"viewAccidentSign"]) {
            self.pushAction = PushNotificationAccidentViewSign;
        }
        else if ([action isEqualToString:@"adminAddShortNoSig"] || [action isEqualToString:@"parentAddShort"]) {
            self.pushAction = PushNotificationShortTermAdded;
        }
        else if ([action isEqualToString:@"adminAddShortSig"] || [action isEqualToString:@"parentSigShort"]) {
            self.pushAction = PushNotificationShortTermSign;
        }
        else if ([action isEqualToString:@"adminAddNewDoseShort"]) {
            self.pushAction = PushNotificationShortTermDose;
        }
        else if ([action isEqualToString:@"adminCompleteShort"] || [action isEqualToString:@"parentSigShortComplete"]) {
            self.pushAction = PushNotificationShortTermComplete;
        }
        else if ([action isEqualToString:@"adminAddLongNoSig"] || [action isEqualToString:@"parentAddLong"]) {
            self.pushAction = PushNotificationLongTermAdded;
        }
        else if ([action isEqualToString:@"adminAddLongtSig"] || [action isEqualToString:@"parentSigLong"]) {
            self.pushAction = PushNotificationLongTermSign;
        }
        else if ([action isEqualToString:@"adminAddNewDoseLong"]) {
            self.pushAction = PushNotificationLongTermDose;
        }
        else if ([action isEqualToString:@"adminCompleteLong"] || [action isEqualToString:@"parentSigLongComplete"]) {
            self.pushAction = PushNotificationLongTermComplete;
        }
        else if ([action isEqualToString:@"adminAddPhotoInGallery"] || [action isEqualToString:@"parentAddPhotoInGallery"]) {
            self.pushAction = PushNotificationPhotoGallery;
        }
        else if ([action isEqualToString:@"adminAddVideoInGallery"] || [action isEqualToString:@"parentAddVideoInGallery"]) {
            self.pushAction = PushNotificationVideoGallery;
        }
        else if ([action isEqualToString:@"adminAddNS"]) {
            self.pushAction = PushNotificationNextStepAddedByAdmin;
        }
        else if ([action isEqualToString:@"parentAddNS"]) {
            self.pushAction = PushNotificationNextStepAddedByParent;
        }
        else if ([action isEqualToString:@"adminCompleteNS"]) {
            self.pushAction = PushNotificationNextStepCompleted;
        }
        else if ([action isEqualToString:@"adminAddObservation"]) {
            self.pushAction = PushNotificationObservationAddedByAdmin;
        }
        else if ([action isEqualToString:@"parentAddObservation"]) {
            self.pushAction = PushNotificationObservationAddedByParent;
        }
        else if ([action isEqualToString:@"adminAddDateProgress"]) {
            self.pushAction = PushNotificationProgressAchievementDate;
        }
        else if ([action isEqualToString:@"adminAddPhotoProgress"]) {
            self.pushAction = PushNotificationProgressPhoto;
        }
        else if ([action isEqualToString:@"adminAddVideoProgress"]) {
            self.pushAction = PushNotificationProgressVideo;
        }
        
        else if ([action isEqualToString:@"adminAddSigAuthorised"]) {
            self.pushAction = PushNotificationAuthorisedSignatureRequired;
        }
        else if ([action isEqualToString:@"adminAddPhotoAuthorised"]) {
            self.pushAction = PushNotificationAuthorisedPhotoRequired;
        }
        else if ([action isEqualToString:@"adminAddSigPhotoAuthorised"]) {
            self.pushAction = PushNotificationAuthorisedSignatureAndPhotoRequired;
        }
        else if ([action isEqualToString:@"parentAddSigAuthorised"]) {
            self.pushAction = PushNotificationAuthorisedSignatureAddedByParent;
        }
        else if ([action isEqualToString:@"parentAddPhotoAuthorised"]) {
            self.pushAction = PushNotificationAuthorisedPhotoAddedByParent;
        }
        else if ([action isEqualToString:@"parentAddSigPhotoAuthorised"]) {
            self.pushAction = PushNotificationAuthorisedSignatureAndPhotoAddedByParent;
        }
        
        else if ([action isEqualToString:@"parentNappiesWet"]) {
            self.pushAction = PushNotificationNappiesAddWetByParent;
        }
        else if ([action isEqualToString:@"parentNappiesSoiled"]) {
            self.pushAction = PushNotificationNappiesAddSoiledByParent;
        }
        else if ([action isEqualToString:@"adminNappiesWet"]) {
            self.pushAction = PushNotificationNappiesAddWetByAdmin;
        }
        else if ([action isEqualToString:@"adminNappiesSoiled"]) {
            self.pushAction = PushNotificationNappiesAddSoiledByAdmin;
        }
        else if ([action isEqualToString:@"admin2parentSendPrivateMessage"]) {
            self.pushAction = PushNotificationPrivateMessageToParent;
        }
        else if ([action isEqualToString:@"admin2adminSendPrivateMessage"] || [action isEqualToString:@"parent2adminSendPrivateMessage"]) {
            self.pushAction = PushNotificationPrivateMessageToAdmin;
        }
        
        [self updateWithDict:dict[@"opt"]];
        
    }
    return self;
}


//Заполняем opt
- (void)updateWithDict:(NSDictionary *)dict {
    
    
    if (!dict || ![dict isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    self.userId = [self safeNumber:dict[@"uid"]];
    
    self.chid = [self safeNumber:dict[@"chid"]];
    
    
    //diary
    self.diaryId = [self safeNumber:dict[@"did"]];
    self.diaryDate = [self safeString:dict[@"date"]];

    //register
    //self.registerId = [self safeNumber:dict[@"register_id"]];
    //self.day = [self safeNumber:dict[@"day"]];
    //self.month = [self safeNumber:dict[@"month"]];
    //self.year = [self safeNumber:dict[@"year"]];
    
    //Permissions
    self.permissionsId = [self safeNumber:dict[@"permission_id"]];
    
    //Policies
    self.policiesId = [self safeNumber:dict[@"policies_id"]];
    
    //Accidents
    self.accidentId = [self safeNumber:dict[@"accident_id"]];
    
    //ShortTerm/LongTerm
    self.medicationId = [self safeNumber:dict[@"medical_id"]];

    //Photo/Video
    self.mediaIds = [self safeArray:dict[@"id_list"]];
    
    //NextStep
    self.observationId = [self safeNumber:dict[@"observationId"]];

    /*
     eyo =         (
     {
     rowId = CLT97;
     tab = tab1;
     type = cl;
     }
    */
    
    //Obs
    self.observationId = [self safeNumber:dict[@"nextStepId"]];
    
    /*
    eyo =         (
                   {
                       date = "02/11/2016";
                       item = CLT1;
                       obtype = obs;
                       tab = tab1;
                       type = cl;
                   }
                   );
    */
    
    /*
    ids =         (
                   {
                       id = 210;
                       type = obs;
                   }
                   );
    */
    
    //Progress
    self.type = [self safeString:dict[@"type"]];
    self.tab = [self safeString:dict[@"tab"]];
 
    
    NSArray *aols;
    
    if (self.pushAction == PushNotificationNextStepCompleted || self.pushAction == PushNotificationObservationAddedByAdmin) {
        aols = [self safeArray:dict[@"eyo"]];
    }
    else if (self.pushAction == PushNotificationObservationAddedByParent) {
        aols = [self safeArray:dict[@"ids"]];
    }
    
    if (aols && [aols count]) {
        for (NSDictionary *aolDict in aols) {
            PushEYOModel *model = [[PushEYOModel alloc] initWithDict:aolDict andPushTypeAction:self.pushAction];
            if (model && [model isExist]) {
                [self.aols addObject:model];
            }
            
        }
    }
    
    //Authorised
    self.personId = [self safeNumber:dict[@"pid"]];
    
    //Nappies
    self.year = [self safeString:dict[@"year"]];
    self.month = [self safeString:dict[@"month"]];
    self.day = [self safeString:dict[@"day"]];
    
    //Private messages
    self.dialogId = [self safeNumber:dict[@"did"]];
    
}


- (BOOL)isExist {
    
    if (!self.message) {
        return NO;
    }
    
    
    PushEYOModel *eyoModel = [self.aols firstObject];
    
    switch (self.pushAction) {
        case PushNotificationAlert:
            return YES;
            break;
        
        case PushNotificationViewDiary:
            if (self.chid && self.diaryId && self.diaryDate && [self isBaseExist]) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationViewDiaryComment:
            if (self.chid && self.diaryId && self.diaryDate && [self isBaseExist] ) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationRegisterIn:
            if ([self isBaseExist]) {
                return YES;
            }
            else {
                return NO;
            }
            break;
    
        case PushNotificationRegisterOut:
            if ([self isBaseExist]) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationPermissions:
            if ([self isBaseExist] && self.permissionsId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationPermissionsViewSign:
            if ([self isBaseExist] && self.permissionsId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
            
        case PushNotificationPolicies:
            if ([self isBaseExist] && self.policiesId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
            
        case PushNotificationPoliciesViewSign:
            if ([self isBaseExist] && self.policiesId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;

        case PushNotificationAccidentView:
            if ([self isBaseExist] && self.accidentId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationAccidentViewSign:
            if ([self isBaseExist] && self.accidentId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationShortTermAdded:
            if ([self isBaseExist] && self.medicationId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationShortTermSign:
            if ([self isBaseExist] && self.medicationId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationShortTermDose:
            if ([self isBaseExist] && self.medicationId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationShortTermComplete:
            if ([self isBaseExist] && self.medicationId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationLongTermAdded:
            if ([self isBaseExist] && self.medicationId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
            
        case PushNotificationLongTermSign:
            if ([self isBaseExist] && self.medicationId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
            
        case PushNotificationLongTermDose:
            if ([self isBaseExist] && self.medicationId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
            
        case PushNotificationLongTermComplete:
            if ([self isBaseExist] && self.medicationId && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
         
        case PushNotificationPhotoGallery:
            if ([self isBaseExist] && self.mediaIds && [self.mediaIds count] && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationVideoGallery:
            if ([self isBaseExist] && self.mediaIds && [self.mediaIds count] && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationNextStepAddedByAdmin:
            if ([self isBaseExist] && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;

        case PushNotificationNextStepAddedByParent:
            if ([self isBaseExist] && self.chid) { //&& self.nextStepId
                return YES;
            }
            else {
                return NO;
            }
            break;

        case PushNotificationNextStepCompleted:
            if ([self isBaseExist] && self.chid && eyoModel) {
                return YES;
            }
            else {
                return NO;
            }
            break;

        case PushNotificationObservationAddedByAdmin:
            if ([self isBaseExist] && eyoModel && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationObservationAddedByParent:
            if ([self isBaseExist] && eyoModel && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationProgressAchievementDate:
            if ([self isBaseExist] && self.type && self.tab && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationProgressPhoto:
            if ([self isBaseExist] && self.mediaIds && [self.mediaIds count] && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
        
        case PushNotificationProgressVideo:
            if ([self isBaseExist] && self.mediaIds && [self.mediaIds count] && self.chid) {
                return YES;
            }
            else {
                return NO;
            }
            break;
            
        case PushNotificationAuthorisedSignatureRequired:
            return ([self isBaseExist] && self.chid && self.personId);
            break;
        case PushNotificationAuthorisedPhotoRequired:
            return ([self isBaseExist] && self.chid && self.personId);
            break;
        case PushNotificationAuthorisedSignatureAndPhotoAddedByParent:
            return ([self isBaseExist] && self.chid && self.personId);
            break;
        case PushNotificationAuthorisedSignatureAndPhotoRequired:
            return ([self isBaseExist] && self.chid && self.personId);
            break;
        case PushNotificationAuthorisedSignatureAddedByParent:
            return ([self isBaseExist] && self.chid && self.personId);
            break;
        case PushNotificationAuthorisedPhotoAddedByParent:
            return ([self isBaseExist] && self.chid && self.personId);
            break;

        case PushNotificationNappiesAddWetByAdmin:
            return ([self isBaseExist] && self.year && self.month && self.day);
            break;
        case PushNotificationNappiesAddSoiledByAdmin:
            return ([self isBaseExist] && self.year && self.month && self.day);
            break;
        case PushNotificationNappiesAddWetByParent:
            return ([self isBaseExist] && self.year && self.month && self.day);
            break;
        case PushNotificationNappiesAddSoiledByParent:
            return ([self isBaseExist] && self.year && self.month && self.day);
            break;
            
        case PushNotificationPrivateMessageToAdmin:
            return ([self isBaseExist] && self.dialogId);
            break;
        case PushNotificationPrivateMessageToParent:
            return ([self isBaseExist] && self.dialogId);
            break;
            
        default:
            return NO;
    }
    
    
}


- (BOOL)isBaseExist {
    
    if (self.userId && self.host) {
        return YES;
    }
    
    return NO;
    
    
}


@end
