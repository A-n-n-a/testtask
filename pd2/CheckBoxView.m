//
//  CheckBoxView.m
//  pd2
//
//  Created by User on 15.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "CheckBoxView.h"
#import "CheckSwitch.h"

@implementation CheckBoxView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    self.backgroundColor=[[MyColorTheme sharedInstance]checkboxFieldBackGroundColor];
    
    for (UIView *v in self.subviews)
    {
        if ([v isKindOfClass:[UILabel class]])
        {
            [(UILabel*)v setTextColor:[[MyColorTheme sharedInstance]checkboxFieldTextColor]];
        }
        if ([v isKindOfClass:[CheckSwitch class]])
        {
            [(CheckSwitch*)v setColor:[[MyColorTheme sharedInstance]checkboxFieldIconColor]];
        }
    }
}


@end
