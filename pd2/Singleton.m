//
//  Singleton.m
//  ValterTaxiClient
//
//  Created by Evgeniy Tka4enko on 19.02.14.
//  Copyright (c) 2014 vexadev. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

static NSMutableDictionary *instances = nil;

+ (void)initialize
{
    if(!instances)
    {
        instances = [NSMutableDictionary dictionary];
    }
}

+ (instancetype)sharedInstance
{
    id instance = nil;
    
	@synchronized(self)
    {
        NSString *selfClassString = NSStringFromClass(self);
        
		if (instances[selfClassString] == nil)
        {
			instances[selfClassString] = [[self alloc] init];
		}
        instance = instances[selfClassString];
	}
    
	return instance;
}

+ (void)destroyInstance
{
    @synchronized(self)
    {
        [instances removeObjectForKey:NSStringFromClass(self)];
    }
}

@end
