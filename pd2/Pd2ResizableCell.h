#import <UIKit/UIKit.h>

@interface Pd2ResizableCell : UITableViewCell
@property (nonatomic, readonly) CGFloat height;
@end
