//
//  IconButton.h
//  pd2
//
//  Created by i11 on 03/04/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IconButton : UIButton

@property (assign, nonatomic) BOOL isActivityIndicatorActive;

- (void)setNewDefaultImage:(UIImage *)image;

- (void)setActivityIndicatorStatus:(BOOL)status;

- (void)showActivityIndicator;
- (void)hideActivityIndicator;



@end
