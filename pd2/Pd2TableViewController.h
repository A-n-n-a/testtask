//
//  Pd2TableViewController.h
//  pd2
//
//  Created by admin on 6/22/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadImageCellDelegate.h"
#import "Pd2TableViewCell.h"

#import "NSDate+Printable.h"

#import "AlertMessageHandler.h"


#define UPLOAD_IMAGE_CELL_ACTION_SHEET 17500


#define LINE_COLOR [[MyColorTheme sharedInstance] toolbarLineColor]
#define LINE_LIGHT_COLOR [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1.0]

#define LINE_WIDTH 0.5f
#define LINE_MARGIN 16.f
#define TOP_LINE_LAYER_NAME @"topLineLayer"
#define BOTTOM_LINE_LAYER_NAME @"bottomLineLayer"
#define BOTTOM_LINE_VIEW_TAG 50505
#define LEFT_LINE_LAYER_NAME @"leftLineLayer"
#define RIGHT_LINE_LAYER_NAME @"rightLineLayer"
#define RIGHT_LINE_VIEW_TAG 40404


@interface Pd2TableViewController : UITableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UploadImageCellDelegate, Pd2TableViewCellDelegate>

//line
typedef enum {
    PDBorderNone,
    PDBorderTop,
    PDBorderBottom,
    PDBorderBottomLight,
    PDBorderBottomDynamic,
    PDBorderBottomDynamicShort,
    PDBorderBottomDynamicLight,
    PDBorderBottomDynamicLightShort,
    PDBorderLeft,
    PDBorderRight,
    PDBorderRightDynamic,
    PDBorderTopAndBottom,
    PDBorderTopAndBottomDynamic,
} PDBorderType;

@property (nonatomic,retain)NSString *topTitle;
@property BOOL keyboardShown;

//Отслеживаем изменено ли изображение (чтобы не аплоадить просто скаченную картинку)
@property (assign, nonatomic) BOOL isImageChanged;

@property (nonatomic, assign) BOOL isScottish; //dynamic

-(void)setColorTheme;

-(void)keyboardWillShow:(id)sender;
-(void)keyboardWillHide:(id)sender;

- (void)disableInactiveTimer;

- (void)addBorder:(PDBorderType)borderType toView:(UIView *)view forToolbar:(BOOL)toolbar;

- (void) pushViewController:(NSString *)viewControllerIdentifier
                 storyboard:(NSString *)storyboardName;
- (UIView *) viewFromCell:(NSString *) cellIdentifier;

- (void)uploadImageActionForImageView:(UIImageView *)imageView withCompletion:(void(^)(UIImage *image))completion;

- (void)setTopTitle:(NSString *)topTitle withFontSize:(CGFloat)fontSize;

- (void)backAction:(id)sender;

- (void)doPortrait;

- (void)showPushFailureAlert;

//Localize
- (void)localize:(NSString *)string tag:(int)tag view:(UIView *)view;


// AgeInMonth
- (NSString*)getAgeInMonthFromDateString:(NSString*)dateString;

@end
