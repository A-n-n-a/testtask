//
//  EditChildViewController.m
//  pd2
//
//  Created by User on 05.08.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "EditChildViewController.h"
#import "ImageCache.h"

#import "ChildContentCell.h"
#import "ChildContent2Cell.h"
#import "ChildBackDateProgressCell.h"
#import "ChildProgressDatesCell.h"
#import "ChildMoveDateCell.h"
#import "ChildProfileSectionTextfieldCell.h"
#import "ChildProfileSectionTextviewCell.h"
#import "PECropViewController.h"

#import "NSDate+Utilities.h"

#import "NSString+RemoveEmoji.h"

@interface EditChildViewController () <PECropViewControllerDelegate>

@end

@implementation EditChildViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    //
    
    
    if (self.infoDict) {
        
        self.infoDict = [NSMutableDictionary dictionaryWithDictionary:self.infoDict];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.infoDict[@"child"]];
        
        NSNumber *inNappies = [dict objectForKey:@"in_nappies"];
        NSNumber *funded = [dict objectForKey:@"funded"];
        NSNumber *eal = [dict objectForKey:@"english_as_addition"];
        NSNumber *premium = [dict objectForKey:@"premium"];
        
        if (inNappies && [inNappies intValue] == 1) {
            [dict setObject:@"on" forKey:@"in_nappies"];
        }
        else {
            [dict setObject:@"" forKey:@"in_nappies"];
        }
        
        if (funded && [funded intValue] == 1) {
            [dict setObject:@"on" forKey:@"funded"];
        }
        else {
            [dict setObject:@"" forKey:@"funded"];
        }
        
        if (eal && [eal intValue] == 1) {
            [dict setObject:@"on" forKey:@"english_as_addition"];
        }
        else {
            [dict setObject:@"" forKey:@"english_as_addition"];
        }
        
        if (premium && [premium intValue] == 1) {
            [dict setObject:@"on" forKey:@"premium"];
        }
        else {
            [dict setObject:@"" forKey:@"premium"];
        }
        
        
        self.infoDict[@"child"] = dict;
        
    }
    
    
    if (_editMode) {
        _showSection0 = false;
        _showSection1 = false;
        _showSection2 = false;
        _showSection3 = false;
        _showSection4 = false;
        _showSection5 = false;
        _showSection6 = false;
    }else{
        _showSection0 = true;
        _showSection1 = true;
        _showSection2 = true;
        _showSection3 = false;
        _showSection4 = false;
        _showSection5 = false;
        _showSection6 = false;
    }

    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 300)];
    titleLabel.text =_editMode?NSLocalizedString(@"as.child.addEdit.editTitle", nil):NSLocalizedString(@"as.child.addEdit.addTitle", nil);
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont boldSystemFontOfSize:23];
    self.navigationItem.titleView=titleLabel;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    _textfields=[NSMutableDictionary new];
    _checkSwitches =[NSMutableDictionary new];
    _childSectionCells=[NSMutableArray arrayWithArray:@[
                                                        @{@"cellType" : @"section1_content"},
                                                        ]];
    for (int i=0; i<_profileSections.count; i++)
    {
        NSDictionary *dict=[_profileSections objectAtIndex:i];
        NSMutableDictionary *mutDict=[NSMutableDictionary dictionaryWithDictionary:dict];
        [mutDict setValue:@"profileSection" forKey:@"cellType"];
        [_childSectionCells addObject:mutDict];
    }
    [_childSectionCells addObject: @{@"cellType" : @"section1_content2"}];

    NSArray *parents = [self.infoDict objectForKey:@"parents"];
   
    if (_isCopyMode)
    {
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        formatter.dateFormat=@"dd.MM.yyyy";
        
        NSString *datestr=[formatter stringFromDate:[NSDate date]];
        [self.infoDict setValue:datestr forKey:@"birth"];
        self.year=[NSNumber numberWithInteger:[[NSDate date] year]];
        self.month=[NSNumber numberWithInteger:[[NSDate date] month]];
        self.day=[NSNumber numberWithInteger:[[NSDate date] day]];
    }

    if (_editMode || _isCopyMode)
    {
        _parent2added = parents.count > 1 ? true : false;
        
        if ([[_infoDict valueForKeyPath:@"child.waiting_1_date"]isKindOfClass:[NSString class]])
        {
            [self setShowRoomMoveDate:true reload:NO];
        }
        if ([[_infoDict valueForKeyPath:@"child.published"]intValue]==0)
        {
            [self setShowProgressDates:true reload:NO];
        }
    }
    else
    {
        _infoDict=[NSMutableDictionary dictionaryWithObjectsAndKeys:                          
                   //@"on", @"p1_diaries",
                   @"on", @"p1_progress",
                   @"on", @"p1_observations",
                   @"on", @"p1_nextsteps",
                   @"on", @"p1_policies",
                   @"on", @"p1_permissions",
                   @"on", @"p1_medical",
                   @"on", @"p1_gallery",
                   @"on", @"p1_register",
                   @"on", @"p1_bookkeeping",
                   @"on", @"p1_communicate",
                   @"on", @"p1_meal_planning",
                   @"on", @"p1_questionnaires",
                   @"on", @"p1_contracts",
                   @"on", @"p1_risk_assessments",
                   @"on", @"p1_nappies",
                   @"on", @"p1_short_term_planning",

                   //@"on", @"p2_diaries",
                   @"on", @"p2_progress",
                   @"on", @"p2_observations",
                   @"on", @"p2_nextsteps",
                   @"on", @"p2_policies",
                   @"on", @"p2_permissions",
                   @"on", @"p2_medical",
                   @"on", @"p2_gallery",
                   @"on", @"p2_register",
                   @"on", @"p2_bookkeeping",
                   @"on", @"p2_communicate",
                   @"on", @"p2_meal_planning",
                   @"on", @"p2_questionnaires",
                   @"on", @"p2_contracts",
                   @"on", @"p2_risk_assessments",
                   @"on", @"p2_nappies",
                   @"on", @"p2_short_term_planning",

                   @1, @"published", nil];
        
    }
    
    [_infoDict setObject:@"true" forKey:@"displ_alert"];
    [_infoDict setObject:@(1) forKey:@"parent_show_alert"];
    
    
    if ([NetworkManager sharedInstance].isJ3) {
        [_infoDict setObject:@"on" forKey:@"p1_calendar"];
        [_infoDict setObject:@"on" forKey:@"p1_notes"];
        [_infoDict setObject:@"on" forKey:@"p1_sleeps"];
        [_infoDict setObject:@"on" forKey:@"p1_bottles"];
        
        [_infoDict setObject:@"on" forKey:@"p2_calendar"];
        [_infoDict setObject:@"on" forKey:@"p2_notes"];
        [_infoDict setObject:@"on" forKey:@"p2_sleeps"];
        [_infoDict setObject:@"on" forKey:@"p2_bottles"];
    }
    else {
        [_infoDict setObject:@"on" forKey:@"p1_diaries"];
        [_infoDict setObject:@"on" forKey:@"p2_diaries"];
    }
    
    
    
    NSLog(@"editChildDidLoad");
}


-(void)setShowRoomMoveDate:(bool)showRoomMoveDate
{
    [self setShowRoomMoveDate:showRoomMoveDate reload:YES];
}


-(void)setShowRoomMoveDate:(bool)showRoomMoveDate reload:(BOOL)reload
{
    if (_showRoomMoveDate!=showRoomMoveDate)
    {
        _showRoomMoveDate=showRoomMoveDate;
        
        if (_showRoomMoveDate)
        {
            if (![self cellExistsWithIdentifier:@"section1_moveRoomDate"])
            {
                for (int i=0;i<_childSectionCells.count; i++)
                {
                    NSDictionary *dict = [_childSectionCells objectAtIndex:i];
                    if ([dict[@"cellType"]isEqualToString:@"section1_content2"])
                    {
                        [_childSectionCells insertObject:@{@"cellType" : @"section1_moveRoomDate"} atIndex:i+1];
                        //[self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i+1 inSection:1]] withRowAnimation:UITableViewRowAnimationLeft];
                        break;
                    }
                }
            }
        }
        else
        {
            [self deleteCellWithIdentifier:@"section1_moveRoomDate"];
        }
        //[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
        if (reload)
        {
            [self.tableView reloadData];
        }
    }
}


-(void)setShowProgressDates:(bool)showProgressDates
{
    [self setShowProgressDates:showProgressDates reload:YES];
}


-(void)setShowProgressDates:(bool)showProgressDates reload:(BOOL)reload
{
    if (_showProgressDates!=showProgressDates)
    {
        _showProgressDates=showProgressDates;
        if (_showProgressDates)
        {
            if (![self cellExistsWithIdentifier:@"section1_progressDates"])
            {
                for (int i=0;i<_childSectionCells.count; i++)
                {
                    NSDictionary *dict = [_childSectionCells objectAtIndex:i];
                    if ([dict[@"cellType"]isEqualToString:@"section1_backDateProgress"])
                    {
                        [_childSectionCells insertObject:@{@"cellType" : @"section1_progressDates"} atIndex:i+1];
                        //[self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i+1 inSection:1]] withRowAnimation:UITableViewRowAnimationLeft];
                        break;
                    }
                }
            }
        }
        else
        {
            [self deleteCellWithIdentifier:@"section1_progressDates"];
        }
        //[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
        if (reload)
        {
            [self.tableView reloadData];
        }
    }
}


-(BOOL)cellExistsWithIdentifier:(NSString*)identifier
{
    for (int i=0;i<_childSectionCells.count; i++)
    {
        NSDictionary *dict = [_childSectionCells objectAtIndex:i];
        if ([dict[@"cellType"] isEqualToString:identifier])
        {
            return YES;
        }
    }
    return NO;
}

-(void)deleteCellWithIdentifier:(NSString*)identifier
{
    for (int i=0;i<_childSectionCells.count; i++)
    {
        NSDictionary *dict = [_childSectionCells objectAtIndex:i];
        if ([dict[@"cellType"] isEqualToString:identifier])
        {
            [_childSectionCells removeObjectAtIndex:i];
            //[self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
            return;
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [_listPicker hide];
    [_datePicker hide];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSDictionary *)getChildParameters
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"dd.MM.yyyy";
    NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"com_sted_mobile",@"option",
                               _editMode?@"updateChild":@"createChild",@"task",
                               @"sted_child_manage",@"controller",
                               
                               //child
                               [_childFirstNameTextField.text stringByRemovingEmoji],@"first_name",
                               [_childLastNameTextField.text stringByRemovingEmoji],@"last_name",
                               _gender, @"gender",
                               _roomId, @"room",
                               _childSpecialEducationCheckSwitch.status,@"senco",
                               _childFundedCheckSwitch.status,@"funded",
                               _childEALCheckSwitch.status,@"english_as_addition",
                               _childPremiumCheckSwitch.status,@"premium",
                               _childBackDateProgressCheckSwitch.on?@0:@1,@"published",
                               [_childHouseStreetTextField.text stringByRemovingEmoji],@"ch_street_1",
                               [_childPostCodeTextField.text stringByRemovingEmoji],@"ch_postcode",
                               _childStartingDateTextField.text,@"starting",
                               _childFinishedDateTextField.text,@"finished",
                               self.infoDict[@"displ_alert"],@"displ_alert",

                               _day,@"day",
                               _month,@"month",
                               _year,@"year",
                               
                               //parent
                               [_parent1FirstNameTextField.text stringByRemovingEmoji],@"p1_first_name",
                               [_parent1LastNameTextField.text stringByRemovingEmoji],@"p1_last_name",
                               [_parent1PostCodeTextField.text stringByRemovingEmoji],@"p1_postcode",
                               
                               /*_parent1HouseStreetTextField.text,@"p1_street_1",
                               _parent1AreaTextField.text,@"p1_area",
                               _parent1TownTextField.text,@"p1_town",
                               _parent1CountryTextField.text,@"p1_county",
                               _parent1EmailTextField.text,@"p1_email",
                               _parent1TelephoneTextField.text,@"p1_phone",
                               _parent1MobileTextField.text,@"p1_mobile",
                               _parent1PlaceOfWorkTextField.text,@"p1_workplace",
                               _parent1WorkTelephoneTextField.text,@"p1_work_telephone",*/
                               
                               [_parent2FirstNameTextField.text stringByRemovingEmoji],@"p2_first_name",
                               [_parent2LastNameTextField.text stringByRemovingEmoji],@"p2_last_name",
                               [_parent2PostCodeTextField.text stringByRemovingEmoji],@"p2_postcode",
                               
                               /*_parent2HouseStreetTextField.text,@"p2_street_1",
                               _parent2AreaTextField.text,@"p2_area",
                               _parent2TownTextField.text,@"p2_town",
                               _parent2CountryTextField.text,@"p2_county",
                               _parent2EmailTextField.text,@"p2_email",
                               _parent2TelephoneTextField.text,@"p2_phone",
                               _parent2MobileTextField.text,@"p2_mobile",
                               _parent2PlaceOfWorkTextField.text,@"p2_workplace",
                               _parent2WorkTelephoneTextField.text,@"p2_work_telephone",*/
                               
                               _sendEmailToParent1CheckSwitch.status,@"p1_email_notify",
                               _sendEmailToParent2CheckSwitch.status,@"p2_email_notify",
                               
                               //parent/carer access
                               _enableParent1AccessCheckSwitch.status,@"p1_status",
                               _enableParent2AccessCheckSwitch.status,@"p2_status",
                               
                               //_parent1AccessToDiariesCheckSwitch.status, @"p1_diaries",
                               _parent1AccessToProgressCheckSwitch.status, @"p1_progress",
                               _parent1AccessToObservationsCheckSwitch.status, @"p1_observations",
                               _parent1AccessToNextstepsCheckSwitch.status, @"p1_nextsteps",
                               _parent1AccessToPoliciesCheckSwitch.status,@"p1_policies",
                               _parent1AccessToPermissionsCheckSwitch.status,@"p1_permissions",
                               _parent1AccessToMedicalCheckSwitch.status,@"p1_medical",
                               _parent1AccessToGalleriesCheckSwitch.status,@"p1_gallery",
                               _parent1AccessToRegisterCheckSwitch.status,@"p1_register",
                               _parent1AccessToBookKeepingCheckSwitch.status,@"p1_bookkeeping",
                               _parent1AccessToCommunicateCheckSwitch.status,@"p1_communicate",
                               _parent1AccessToMenusCheckSwitch.status,@"p1_meal_planning",
                               _parent1AccessToQuestionsCheckSwitch.status, @"p1_questionnaires",
                               _parent1AccessToContactsCheckSwitch.status, @"p1_contracts",
                               _parent1AccessToRiskAssessmentsCheckSwitch.status, @"p1_risk_assessments",
                               _parent1NappiesSwitch.status, @"p1_nappies",
                               _parent1PlanningSwitch.status, @"p1_short_term_planning",

                               
                               //_parent2AccessToDiariesCheckSwitch.status,@"p2_diaries",
                               _parent2AccessToProgressCheckSwitch.status,@"p2_progress",
                               _parent2AccessToObservationsCheckSwitch.status, @"p2_observations",
                               _parent2AccessToNextstepsCheckSwitch.status, @"p2_nextsteps",
                               _parent2AccessToPoliciesCheckSwitch.status,@"p2_policies",
                               _parent2AccessToPermissionsCheckSwitch.status,@"p2_permissions",
                               _parent2AccessToMedicalCheckSwitch.status,@"p2_medical",
                               _parent2AccessToGalleriesCheckSwitch.status,@"p2_gallery",
                               _parent2AccessToRegisterCheckSwitch.status,@"p2_register",
                               _parent2AccessToBookKeepingCheckSwitch.status,@"p2_bookkeeping",
                               _parent2AccessToCommunicateCheckSwitch.status,@"p2_communicate",
                               _parent2AccessToMenusCheckSwitch.status,@"p2_meal_planning",
                               _parent2AccessToQuestionsCheckSwitch.status, @"p2_questionnaires",
                               _parent2AccessToContactsCheckSwitch.status, @"p2_contracts",
                               _parent2AccessToRiskAssessmentsCheckSwitch.status, @"p2_risk_assessments",
                               _parent2NappiesSwitch.status, @"p2_nappies",
                               _parent2PlanningSwitch.status, @"p2_short_term_planning",

                               //emergency
                               [_emergencyFirstNameTextField.text stringByRemovingEmoji],@"ec_first_name",
                               [_emergencyLastNameTextField.text stringByRemovingEmoji],@"ec_last_name",
                               [_emergencyPostCodeTextField.text stringByRemovingEmoji],@"ec_postcode",
                               
                               [_emergencyHouseStreetTextField.text stringByRemovingEmoji],@"ec_street_1",
                               /* _emergencyAreaTextField.text,@"ec_area",
                               _emergencyTownTextField.text,@"ec_town",
                               _emergencyCountryTextField.text,@"ec_county",
                               _emergencyTelephoneTextField.text,@"ec_telephone",
                               _emergencyMobileTextField.text,@"ec_mobile",
                               _emergencyPlaceOfWorkTextField.text,@"ec_workplace",
                               _emergencyWorkTelephoneTextField.text,@"ec_work_telephone",
                               _doctorsNameTextField.text,@"ec_doctor_name",
                               _doctorsPhoneTextField.text,@"ec_doctor_tel",*/
                               
//                               _diphteriaCheckSwitch.status,@"dip",
//                               _measlesCheckSwitch.status,@"mea",
//                               _mumpsCheckSwitch.status,@"mum",
//                               _meningitisCheckSwitch.status,@"hib",
//                               _polioCheckSwitch.status,@"pol",
//                               _rubellaCheckSwitch.status,@"rub",
//                               _tetanusCheckSwitch.status,@"tet",
//                               _whoopingCoughCheckSwitch.status,@"who",
                               nil];
    
    if ([NetworkManager sharedInstance].isJ3) {
        [dict setObject:_parent1CalendarSwitch.status forKey:@"p1_calendar"];
        [dict setObject:_parent1DailyNotesSwitch.status forKey:@"p1_notes"];
        [dict setObject:_parent1SleepingSwitch.status forKey:@"p1_sleeps"];
        [dict setObject:_parent1BottlesSwitch.status forKey:@"p1_bottles"];
        
        [dict setObject:_parent2CalendarSwitch.status forKey:@"p2_calendar"];
        [dict setObject:_parent2DailyNotesSwitch.status forKey:@"p2_notes"];
        [dict setObject:_parent2SleepingSwitch.status forKey:@"p2_sleeps"];
        [dict setObject:_parent2BottlesSwitch.status forKey:@"p2_bottles"];
    }
    else {
        [dict setObject:_parent1AccessToDiariesCheckSwitch.status forKey:@"p1_diaries"];
        [dict setObject:_parent2AccessToDiariesCheckSwitch.status forKey:@"p2_diaries"];
    }
    
    
    
    
    if (![NetworkManager sharedInstance].isJ3) {
        [dict setObject:_diphteriaCheckSwitch.status forKey:@"dip"];
        [dict setObject:_measlesCheckSwitch.status forKey:@"mea"];
        [dict setObject:_mumpsCheckSwitch.status forKey:@"mum"];
        [dict setObject:_meningitisCheckSwitch.status forKey:@"hib"];
        [dict setObject:_polioCheckSwitch.status forKey:@"pol"];
        [dict setObject:_rubellaCheckSwitch.status forKey:@"rub"];
        [dict setObject:_tetanusCheckSwitch.status forKey:@"tet"];
        [dict setObject:_whoopingCoughCheckSwitch.status forKey:@"who"];
    }
    
    
    if (![NetworkManager sharedInstance].isJ3) {
        [dict setValue:_childInNappiesCheckSwitch.status forKey:@"in_nappies"];
    }
    
    
    if(_editMode)
    {
        [dict setValue:_infoDict[@"id"] forKey:@"id"];
    }
    
    if (_moveDate)
    {
        [dict setValue:[[[Utils sharedInstance]dotsDateFormatter] stringFromDate:_moveDate] forKey:@"waiting_1_date"];
    }
    
    if (_startingDate)
    {
        [dict setValue:[formatter stringFromDate:_startingDate] forKey:@"starting"];
    }
    if (_finishedDate)
    {
        [dict setValue:[formatter stringFromDate:_finishedDate] forKey:@"finished"];
    }
    if (_childRoomMoveTextField.text.length)
    {
        [dict setValue:_roomMoveId forKey:@"waiting_1_room"];
    }
    
    
    for (NSString *key in _textfields.allKeys)
    {
        //UITextField *tf =[_textfields valueForKey:key];
        NSString *value=_infoDict[key];
        if (value.length)
        //if (tf.text.length)
        {
            [dict setValue:value forKey:key];
        }
    }

    //custom fields from profile sections
    for (NSDictionary *dct in _profileSections)
    {
        NSString *key=dct[@"idname"];
        NSString *value=_infoDict[key];//[dct[@"textfield"]text];
        if (value) {
            [dict setValue:value forKey:key];
        }
    }
    //NSLog(@"childImage:%@",_image);
    if (_image && self.isImageChanged)
    {
        
        UIImage *image = [[Utils sharedInstance]imageScaledToRegularVisitorImage:self.image];
        
        NSString *image64=[[Utils sharedInstance]base64StringFromImage:image];
        [dict setValue:image64 forKey:@"file"];
    }
    
    if (self.childDateOfBirthTextField.text.length){
        [dict setValue:self.childDateOfBirthTextField.text forKey:@"birth"];
    }else{
        if (_isCopyMode)
        {
            NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
            formatter.dateFormat=@"dd.MM.yyyy";
            
            NSString *datestr=[formatter stringFromDate:[NSDate date]];
            [dict setValue:datestr forKey:@"birth"];
            self.year=[NSNumber numberWithInteger:[[NSDate date] year]];
            self.month=[NSNumber numberWithInteger:[[NSDate date] month]];
            self.day=[NSNumber numberWithInteger:[[NSDate date] day]];
        }

    }
    
    NSLog(@"child dict:%@",dict);
    return dict;
}


-(void)setInfoForIndexPath:(NSIndexPath *)indexPath
{
    [self setInfoForIndexPath:indexPath cellType:nil];
}


-(void)setInfoForIndexPath:(NSIndexPath *)indexPath cellType:(NSString* )cellType
{
    if (!self.infoDict)
    {
        return;
    }
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"dd.MM.yyyy";
    _infoDict=[NSMutableDictionary dictionaryWithDictionary:_infoDict];
    
    if (!_initialized)
    {
        NSDictionary *childDict=_infoDict[@"child"];
        
        
        
        
        
        for (NSString *key in childDict.allKeys)
        {
            [_infoDict setValue:childDict[key] forKey:key];
        }
        NSArray *parents=_infoDict[@"parents"];
        
        
        if (parents && [parents isKindOfClass:[NSArray class]] && [parents count]) {
            NSDictionary *p1dict=parents[0];
            if (p1dict && [p1dict isKindOfClass:[NSDictionary class]]) {
                for (NSString *key in p1dict)
                {
                    [_infoDict setValue:p1dict[key] forKey:[NSString stringWithFormat:@"p1_%@",key]];
                }
            }
            
            
            //
            if ([parents count] > 1) {
                NSDictionary *p2dict=parents[1];
                for (NSString *key in p2dict)
                {
                    [_infoDict setValue:p2dict[key] forKey:[NSString stringWithFormat:@"p2_%@",key]];
                }
            }
        }
        
        
        
        NSArray *profileArray=_infoDict[@"profile"];
        for (NSDictionary *profileDict in profileArray)
        {
            [_infoDict setValue:profileDict[@"title"] forKey:profileDict[@"idname"]];
        }
        
        _initialized = true;
    }
    NSLog(@"child.setInfoFor IP [%d, %d],withDict",(int)indexPath.section,(int)indexPath.row);
    
    NSDictionary *dict =_infoDict;
    
    if (indexPath.section==1)
    {
        if ([cellType isEqualToString:@"section1_content"])
        {
            _childFirstNameTextField.text=dict[@"first_name"];
            _childLastNameTextField.text=dict[@"last_name"];
            _childPostCodeTextField.text=dict[@"ch_postcode"];
            _childHouseStreetTextField.text=dict[@"ch_street_1"];
            _childAreaTextField.text=dict[@"ch_area"];
            _childTownTextField.text=dict[@"ch_town"];
            _childCountryTextField.text=dict[@"ch_county"];
            _childStartingDateTextField.text = [dict[@"starting"] isEqualToString:@"01.01.1970"] || [dict[@"starting"] isEqualToString:@"00.00.0000"] ? @"" : dict[@"starting"];
            _childFinishedDateTextField.text = [dict[@"finished"] isEqualToString:@"01.01.1970"] || [dict[@"finished"] isEqualToString:@"00.00.0000"] ? @"" : dict[@"finished"];
            //_parentAlertsSwitch.on = [dict[@"parent_show_alert"] integerValue] == 1;
            
            if (dict[@"gender"] && !_isCopyMode)
            {
                _childBoyOrGirlTextField.text=[(NSNumber*)dict[@"gender"] intValue]== 0 ? NSLocalizedString(@"Boy", nil):NSLocalizedString(@"Girl", nil);
                _gender = dict[@"gender"];
            }
            NSString *dob=dict[@"birth"];
            _childDateOfBirthTextField.text=dob;
            if (dob.length)
            {
                NSArray *dobcomponents=[dob componentsSeparatedByString:@"."];
                _year=dobcomponents[2];
                _month=dobcomponents[1];
                _day=dobcomponents[0];
            }
            
            _childSpecialEducationCheckSwitch.on=([dict[@"senco"] isEqualToString:@"on"]);
            _childInNappiesCheckSwitch.on=([dict[@"in_nappies"] isEqualToString:@"on"]);
            _childFundedCheckSwitch.on=([dict[@"funded"] isEqualToString:@"on"]);
            _childEALCheckSwitch.on=([dict[@"english_as_addition"] isEqualToString:@"on"]);
            _childPremiumCheckSwitch.on=([dict[@"premium"] isEqualToString:@"on"]);
            

            
            _roomId=dict[@"room"];
            _childRoomTextField.text=[[DataManager sharedInstance]getRoomTitleWithId:[_roomId intValue]];
            
            _roomMoveId=dict[@"waiting_1_room"];
            if (![_roomMoveId isKindOfClass:[NSNull class]])
            {
                NSString *roomMove=[[DataManager sharedInstance]getRoomTitleWithId:_roomMoveId.intValue];
                _childRoomMoveTextField.text=roomMove;
            }
        }
        if ([cellType isEqualToString:@"section1_content2"])
        {
            if (dict[@"gender"] && !_isCopyMode)
            {
                _childBoyOrGirlTextField.text=[(NSNumber*)dict[@"gender"] intValue]==0?NSLocalizedString(@"Boy", nil):NSLocalizedString(@"Girl", nil);
                _gender = dict[@"gender"];
            }
            NSString *dob=dict[@"birth"];
            
            if (!dob.length) {
                _childDateOfBirthTextField.text=@"";
            }
            _childDateOfBirthTextField.text=dob;
            if (dob.length)
            {
                NSArray *dobcomponents=[dob componentsSeparatedByString:@"."];
                _year=dobcomponents[2];
                _month=dobcomponents[1];
                _day=dobcomponents[0];
            }
            
        
            _childSpecialEducationCheckSwitch.on=([dict[@"senco"] isEqualToString:@"on"]);
            if (![NetworkManager sharedInstance].isJ3) {
                _childInNappiesCheckSwitch.on=([dict[@"in_nappies"] isEqualToString:@"on"]);
            }
            _childFundedCheckSwitch.on=([dict[@"funded"] isEqualToString:@"on"]);
            _childEALCheckSwitch.on=([dict[@"english_as_addition"] isEqualToString:@"on"]);
            _childPremiumCheckSwitch.on=([dict[@"premium"] isEqualToString:@"on"]);

            _parentAlertsSwitch.on = [dict[@"parent_show_alert"] integerValue] == 1;
            
            _roomId=dict[@"room"];
            _childRoomTextField.text=[[DataManager sharedInstance]getRoomTitleWithId:[_roomId intValue]];
            
            _roomMoveId=dict[@"waiting_1_room"];
            if (![_roomMoveId isKindOfClass:[NSNull class]])
            {
                NSString *roomMove=[[DataManager sharedInstance]getRoomTitleWithId:_roomMoveId.intValue];
                _childRoomMoveTextField.text=roomMove;
            }
        }

        else if([cellType isEqualToString:@"section1_moveRoomDate"])
        {
            NSString *waiting_date=dict[@"waiting_1_date"];
            if ([waiting_date isKindOfClass:[NSString class]])
            {
                [self setShowRoomMoveDate:true reload:NO];
                _childMoveDateTextField.text=dict[@"waiting_1_date"];
            }
        }
        else if ([cellType isEqualToString:@"section1_backDateProgress"])
        {
            _childBackDateProgressCheckSwitch.on = [dict[@"published"]intValue]==0;
            [self setShowProgressDates:_childBackDateProgressCheckSwitch.on reload:NO];
        }
        else if ([cellType isEqualToString:@"section1_progressDates"])
        {
            _childStartingDateTextField.text = dict[@"starting"];
            _startingDate = [formatter dateFromString:dict[@"starting"]];
            
            _childFinishedDateTextField.text = dict[@"finished"];
            _finishedDate = [formatter dateFromString: dict[@"finished"]];
        }
        else if([cellType isEqualToString:@"profileSection"])
        {
            //custom sections
            NSDictionary *sectionDict=[_profileSections objectAtIndex:indexPath.row-1];
            UITextField * tf=sectionDict[@"textfield"];
            NSString *key=sectionDict[@"idname"];
            NSString *value=dict[key];
            [tf setText:value];
            /*
            for (NSDictionary *sectionDict in _profileSections)
            {
                NSString *key=sectionDict[@"idname"];
                NSString *value=dict[key];
                [sectionDict[@"textfield"]setText:value];
            }*/
        }
    }
    else if(indexPath.section==2)
    {
        if (indexPath.row==0)
        {
            //parent1
            _parent1FirstNameTextField.text=dict[@"p1_first_name"];
            _parent1LastNameTextField.text=dict[@"p1_last_name"];
            _parent1PostCodeTextField.text=dict[@"p1_postcode"];
            _parent1HouseStreetTextField.text=dict[@"p1_street_1"];
            _parent1AreaTextField.text=dict[@"p1_area"];
            _parent1TownTextField.text=dict[@"p1_town"];
            _parent1CountryTextField.text=dict[@"p1_county"];
            _parent1EmailTextField.text=dict[@"p1_email"];
            _parent1TelephoneTextField.text=dict[@"p1_phone"];
            _parent1MobileTextField.text=dict[@"p1_mobile"];
            _parent1PlaceOfWorkTextField.text=dict[@"p1_workplace"];
            _parent1WorkTelephoneTextField.text=dict[@"p1_work_telephone"];
        }
        else if(indexPath.row==2)
        {
            //parent2
            _parent2FirstNameTextField.text=dict[@"p2_first_name"];
            _parent2LastNameTextField.text=dict[@"p2_last_name"];
            _parent2PostCodeTextField.text=dict[@"p2_postcode"];
            _parent2HouseStreetTextField.text=dict[@"p2_street_1"];
            _parent2AreaTextField.text=dict[@"p2_area"];
            _parent2TownTextField.text=dict[@"p2_town"];
            _parent2CountryTextField.text=dict[@"p2_county"];
            _parent2EmailTextField.text=dict[@"p2_email"];
            _parent2TelephoneTextField.text=dict[@"p2_phone"];
            _parent2MobileTextField.text=dict[@"p2_mobile"];
            _parent2PlaceOfWorkTextField.text=dict[@"p2_workplace"];
            _parent2WorkTelephoneTextField.text=dict[@"p2_work_telephone"];

        }
    }
    else if(indexPath.section==3)
    {
        if (indexPath.row==0)
        {
            _sendEmailToParent1CheckSwitch.on=[dict[@"p1_email_notify"]isEqualToString:@"on"];
            _sendEmailToParent2CheckSwitch.on=[dict[@"p2_email_notify"]isEqualToString:@"on"];
        }
    }
    else if(indexPath.section==4)
    {
        if (indexPath.row==0)
        {
            //parent/carer access 1
            _parent1AccessToProgressCheckSwitch.on=[dict[@"p1_progress"]isEqualToString:@"on"];
            _parent1AccessToObservationsCheckSwitch.on=[dict[@"p1_observations"]isEqualToString:@"on"];
            _parent1AccessToNextstepsCheckSwitch.on=[dict[@"p1_nextsteps"]isEqualToString:@"on"];
            _parent1AccessToPoliciesCheckSwitch.on=[dict[@"p1_policies"]isEqualToString:@"on"];
            _parent1AccessToPermissionsCheckSwitch.on=[dict[@"p1_permissions"]isEqualToString:@"on"];
            _parent1AccessToMedicalCheckSwitch.on=[dict[@"p1_medical"]isEqualToString:@"on"];
            _parent1AccessToGalleriesCheckSwitch.on=[dict[@"p1_gallery"]isEqualToString:@"on"];
            _parent1AccessToRegisterCheckSwitch.on=[dict[@"p1_register"]isEqualToString:@"on"];
            _parent1AccessToBookKeepingCheckSwitch.on=[dict[@"p1_bookkeeping"]isEqualToString:@"on"];
            _parent1AccessToCommunicateCheckSwitch.on=[dict[@"p1_communicate"]isEqualToString:@"on"];
            _parent1AccessToMenusCheckSwitch.on=[dict[@"p1_meal_planning"]isEqualToString:@"on"];
            _parent1AccessToQuestionsCheckSwitch.on=[dict[ @"p1_questionnaires"]isEqualToString:@"on"];
            _parent1AccessToContactsCheckSwitch.on=[dict[ @"p1_contracts"]isEqualToString:@"on"];
            _parent1AccessToRiskAssessmentsCheckSwitch.on=[dict[ @"p1_risk_assessments"]isEqualToString:@"on"];
            _parent1NappiesSwitch.on=[dict[ @"p1_nappies"]isEqualToString:@"on"];
            _parent1PlanningSwitch.on=[dict[ @"p1_short_term_planning"]isEqualToString:@"on"];

            
            //parent/carer access 2
            _parent2AccessToProgressCheckSwitch.on=[dict[@"p2_progress"]isEqualToString:@"on"];
            _parent2AccessToObservationsCheckSwitch.on=[dict[@"p2_observations"]isEqualToString:@"on"];
            _parent2AccessToNextstepsCheckSwitch.on=[dict[@"p2_nextsteps"]isEqualToString:@"on"];
            _parent2AccessToPoliciesCheckSwitch.on=[dict[@"p2_policies"]isEqualToString:@"on"];
            _parent2AccessToPermissionsCheckSwitch.on=[dict[@"p2_permissions"]isEqualToString:@"on"];
            _parent2AccessToMedicalCheckSwitch.on=[dict[@"p2_medical"]isEqualToString:@"on"];
            _parent2AccessToGalleriesCheckSwitch.on=[dict[@"p2_gallery"]isEqualToString:@"on"];
            _parent2AccessToRegisterCheckSwitch.on=[dict[@"p2_register"]isEqualToString:@"on"];
            _parent2AccessToBookKeepingCheckSwitch.on=[dict[@"p2_bookkeeping"]isEqualToString:@"on"];
            _parent2AccessToCommunicateCheckSwitch.on=[dict[@"p2_communicate"]isEqualToString:@"on"];
            _parent2AccessToMenusCheckSwitch.on=[dict[@"p2_meal_planning"]isEqualToString:@"on"];
            _parent2AccessToQuestionsCheckSwitch.on=[dict[ @"p2_questionnaires"]isEqualToString:@"on"];
            _parent2AccessToContactsCheckSwitch.on=[dict[ @"p2_contracts"]isEqualToString:@"on"];
            _parent2AccessToRiskAssessmentsCheckSwitch.on=[dict[ @"p2_risk_assessments"]isEqualToString:@"on"];
            _parent2NappiesSwitch.on=[dict[ @"p2_nappies"]isEqualToString:@"on"];
            _parent2PlanningSwitch.on=[dict[ @"p2_short_term_planning"]isEqualToString:@"on"];


            if ([NetworkManager sharedInstance].isJ3) {
                _parent1CalendarSwitch.on=[dict[@"p1_calendar"]isEqualToString:@"on"];
                _parent1DailyNotesSwitch.on=[dict[@"p1_notes"]isEqualToString:@"on"];
                _parent1SleepingSwitch.on=[dict[@"p1_sleeps"]isEqualToString:@"on"];
                _parent1BottlesSwitch.on=[dict[@"p1_bottles"]isEqualToString:@"on"];
            
                _parent2CalendarSwitch.on=[dict[@"p2_calendar"]isEqualToString:@"on"];
                _parent2DailyNotesSwitch.on=[dict[@"p2_notes"]isEqualToString:@"on"];
                _parent2SleepingSwitch.on=[dict[@"p2_sleeps"]isEqualToString:@"on"];
                _parent2BottlesSwitch.on=[dict[@"p2_bottles"]isEqualToString:@"on"];
            }
            else {
                _parent1AccessToDiariesCheckSwitch.on=[dict[@"p1_diaries"]isEqualToString:@"on"];
                _parent2AccessToDiariesCheckSwitch.on=[dict[@"p2_diaries"]isEqualToString:@"on"];
            }
            
            
            //_enableParent1AccessCheckSwitch.on = [dict[ @""]isEqualToString:@"on"];
            //_enableParent2AccessCheckSwitch.on = [dict[ @"p2_questionnaires"]isEqualToString:@"on"];
        }
    }
    else if(indexPath.section==5)
    {
        if (indexPath.row==0)
        {
            //emergency
            _emergencyFirstNameTextField.text=dict[@"ec_first_name"];
            _emergencyLastNameTextField.text=dict[@"ec_last_name"];
            _emergencyPostCodeTextField.text=dict[@"ec_postcode"];
            _emergencyHouseStreetTextField.text=dict[@"ec_street_1"];
            _emergencyAreaTextField.text=dict[@"ec_area"];
            _emergencyTownTextField.text=dict[@"ec_town"];
            _emergencyCountryTextField.text=dict[@"ec_county"];
            _emergencyTelephoneTextField.text=dict[@"ec_telephone"];
            _emergencyMobileTextField.text=dict[@"ec_mobile"];
            _emergencyPlaceOfWorkTextField.text=dict[@"ec_workplace"];
            _emergencyWorkTelephoneTextField.text=dict[@"ec_work_telephone"];
            _doctorsNameTextField.text=dict[@"ec_doctor_name"];
            _doctorsPhoneTextField.text=dict[@"ec_doctor_tel"];
            
            //vaccinations
            if (![NetworkManager sharedInstance].isJ3) {

                _diphteriaCheckSwitch.on = [dict[@"dip"] isEqualToString:@"on"];
                _measlesCheckSwitch.on = [dict[@"mea"] isEqualToString:@"on"];
                _mumpsCheckSwitch.on = [dict[@"mum"] isEqualToString:@"on"];
                _meningitisCheckSwitch.on = [dict[@"hib"] isEqualToString:@"on"];
                _polioCheckSwitch.on = [dict[@"pol"] isEqualToString:@"on"];
                _rubellaCheckSwitch.on = [dict[@"rub"] isEqualToString:@"on"];
                _tetanusCheckSwitch.on = [dict[@"tet"] isEqualToString:@"on"];
                _whoopingCoughCheckSwitch.on = [dict[@"who"] isEqualToString:@"on"];
            }
        }
    }
    else if(indexPath.section==6)
    {
        if (indexPath.row==0)
        {
            if (self.image)
            {
                [self.addPhotoButton setBackgroundImage:self.image forState:UIControlStateNormal];
            }
            else
            {
                NSString *imagePath=dict[@"image"];
                [[Utils sharedInstance]downloadImageAtRelativePath:imagePath completionHandler:^(UIImage *image) {
                    if (image)
                    {
                        _image=image;
                        [self.addPhotoButton setBackgroundImage:image forState:UIControlStateNormal];
                    }
                }];
            }
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextFieldDelegate methods

-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [_listPicker hide];
    [_datePicker hide];
    
    CGPoint p =[self.tableView convertPoint:textField.frame.origin fromView:textField.superview];
    [self.tableView setContentOffset:CGPointMake(0, p.y-100) animated:YES];
    __weak typeof(self) weakSelf=self;
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"dd.MM.yyyy";
    
    if (textField.tag>1500&&textField.tag<1600)
    //for (int i=0; i<_profileSections.count; i++)
    {
        NSDictionary *dict=[_profileSections objectAtIndex:textField.tag-1500-1];
        if (dict[@"textfield"]==textField)
        {
            if ([dict[@"type"]intValue]==2)
            {
                if ([dict[@"content"] isKindOfClass:[NSString class]])
                {
                    NSString *content=dict[@"content"];
                    NSArray *list=[content componentsSeparatedByString:@";"];
                    
                    [self.view endEditing:YES];
                    _listPicker=[[ListPicker alloc]init];
                    _listPicker.dataArray=list;
                    [_listPicker setHandler:^(NSString *string, int index) {
                        textField.text = string;
                        [weakSelf.infoDict setValue:textField.text forKey:dict[@"idname"]];
                    }];
                    [_listPicker show];
                    return NO;
                }
            }
        }
    }
    
    if (textField.keyboardType==UIKeyboardTypePhonePad)
    {
        NSLog(@"phonePad!");
        UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        numberToolbar.barStyle = UIBarStyleBlackTranslucent;
        numberToolbar.items = [NSArray arrayWithObjects:
                               [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                               [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                               nil];
        [numberToolbar sizeToFit];
        textField.inputAccessoryView = numberToolbar;
    }
    
    if(textField==_childBoyOrGirlTextField)
    {
        [self.view endEditing:YES];
        _listPicker=[[ListPicker alloc]init];
        _listPicker.dataArray=@[NSLocalizedString(@"Boy", nil), NSLocalizedString(@"Girl", nil)];
        [_listPicker setHandler:^(NSString *string, int index) {
            weakSelf.childBoyOrGirlTextField.text = string;
            weakSelf.gender=[NSNumber numberWithInt:index];
            [weakSelf.infoDict setValue:[weakSelf.gender stringValue] forKey:@"gender"];
#warning localizedPicker
        }];
        [_listPicker show];

        return NO;
    }
    else if(textField==_childDateOfBirthTextField)
    {
        [self.view endEditing:YES];
        _datePicker=[[DOBPicker alloc]init];
        
        NSDate *initialDate = [NSDate date];
        
        _datePicker.initialDate = initialDate;
        [_datePicker setDateHandler:^(NSDate *newDate)
        {
            NSString *datestr=[formatter stringFromDate:newDate];
            weakSelf.childDateOfBirthTextField.text = datestr;
            [weakSelf.infoDict setValue:datestr forKey:@"birth"];
            /*
            NSArray *dobcomponents=[datestr componentsSeparatedByString:@"-"];
            weakSelf.year=[NSNumber numberWithInt:[dobcomponents[0] intValue]];
            weakSelf.month=[NSNumber numberWithInt:[dobcomponents[1]intValue]];
            weakSelf.day=[NSNumber numberWithInt:[dobcomponents[2]intValue]];
            */
            weakSelf.year=[NSNumber numberWithInteger:[newDate year]];
            weakSelf.month=[NSNumber numberWithInteger:[newDate month]];
            weakSelf.day=[NSNumber numberWithInteger:[newDate day]];
        }];
        [_datePicker show];

        return NO;
    }
    else if(textField==_childRoomTextField)
    {
        [self.view endEditing:YES];
        _listPicker=[[ListPicker alloc]init];
        _listPicker.dataArray=[DataManager sharedInstance].roomList;
        [_listPicker setHandler:^(NSString *string, int index) {
            weakSelf.childRoomTextField.text = string;
            weakSelf.roomId = [NSNumber numberWithInt:[[DataManager sharedInstance]roomIdWithTitle:string]];
            [weakSelf.infoDict setValue:weakSelf.roomId forKey:@"room"];

            NSLog(@"newRoomId:%@",weakSelf.roomId);
        }];
        [_listPicker show];

        return NO;
    }
    else if(textField==_childRoomMoveTextField)
    {
        [self.view endEditing:YES];
        if ([DataManager sharedInstance].roomList.count==1)
        {
            ErrorAlert(NSLocalizedString(@"as.child.addEdit.alert.oneRoom", nil));
        }
        _listPicker=[[ListPicker alloc]init];
        NSMutableArray *roomsArray = [NSMutableArray arrayWithArray:[[DataManager sharedInstance] roomListWithoutRoom:_childRoomTextField.text]];
        [roomsArray insertObject:NSLocalizedString(@"None", nil) atIndex:0];
        _listPicker.dataArray=roomsArray;
        [_listPicker setHandler:^(NSString *string, int index) {
            if (index==0) {
                weakSelf.roomMoveId = nil;
                [weakSelf.infoDict setValue:weakSelf.roomMoveId forKey:@"waiting_1_room"];
                if (weakSelf.showRoomMoveDate)
                {
                    weakSelf.showRoomMoveDate=false;
                    [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
                }
                return;
            }
            string=[string stringByReplacingOccurrencesOfString:@"-" withString:@"."];
            weakSelf.childRoomMoveTextField.text = string;
            weakSelf.roomMoveId = [NSNumber numberWithInt:[[DataManager sharedInstance]roomIdWithTitle:string]];
            [weakSelf.infoDict setValue:weakSelf.roomMoveId forKey:@"waiting_1_room"];
            weakSelf.showRoomMoveDate=true;
            NSLog(@"showRoomMoveDate");
            [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
        }];
        [_listPicker show];
        return NO;
    }
    else if(textField==_childMoveDateTextField)
    {
        [self.view endEditing:YES];
        _datePicker=[[DOBPicker alloc]init];
        _datePicker.initialDate = _moveDate;
        [_datePicker setDateHandler:^(NSDate *newDate) {
            weakSelf.childMoveDateTextField.text = [formatter stringFromDate:newDate];
            weakSelf.moveDate=newDate;
            [weakSelf.infoDict setValue:[formatter stringFromDate:newDate] forKey:@"waiting_1_date"];
        }];
        [_datePicker show];
        
        return NO;
    }
    else if(textField==_childStartingDateTextField)
    {
        [self.view endEditing:YES];
        _datePicker=[[DOBPicker alloc]init];
        _datePicker.initialDate = _startingDate;
        [_datePicker setDateHandler:^(NSDate *newDate) {
            weakSelf.childStartingDateTextField.text = [formatter stringFromDate:newDate];
            weakSelf.startingDate=newDate;
            [weakSelf.infoDict setValue:[formatter stringFromDate:newDate] forKey:@"starting"];
        }];
        [_datePicker show];

        return NO;
    }
    else if(textField==_childFinishedDateTextField)
    {
        [self.view endEditing:YES];
        _datePicker=[[DOBPicker alloc]init];
        _datePicker.initialDate = _finishedDate;
        [_datePicker setDateHandler:^(NSDate *newDate) {
            weakSelf.childFinishedDateTextField.text = [formatter stringFromDate:newDate];
            weakSelf.finishedDate=newDate;
            [weakSelf.infoDict setValue:[formatter stringFromDate:newDate] forKey:@"finished"];
        }];
        [_datePicker show];
        return NO;
    }
    else
    {
        
    }
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"textfield did begin editing:%@",textField);
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text=[textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField.tag>=1500&&textField.tag<1600) {
        int index=(int)textField.tag-1500-1;
        NSDictionary *dict=[_profileSections objectAtIndex:index];
        if (dict[@"textfield"]==textField)
        {
            NSLog(@"textfield found");
            NSString *key=dict[@"idname"];
            [_infoDict setValue:textField.text forKey:key];
        }
    }
    
    for (NSString *key in _textfields.allKeys)
    {
        UITextField *tf=_textfields[key];
        if (tf==textField)
        {
            [_infoDict setValue:text forKey:key];
        }
    }
    
    /*
    for (int i=0; i<_profileSections.count; i++)
    {
        NSDictionary *dict=[_profileSections objectAtIndex:i];
        if (dict[@"textfield"]==textField)
        {
            NSLog(@"textfield found");
            NSString *key=dict[@"idname"];
            [_infoDict setValue:text forKey:key];
        }
    }
     */
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"textfieldDidEndEditing:%@",textField);
    if (textField.tag>=1500&&textField.tag<1600) {
        int index=(int)textField.tag-1500-1;
        NSDictionary *dict=[_profileSections objectAtIndex:index];
        if (dict[@"textfield"]==textField)
        {
            NSLog(@"textfield found");
            NSString *key=dict[@"idname"];
            [_infoDict setValue:textField.text forKey:key];
        }
    }
    for (NSString *key in _textfields.allKeys)
    {
        UITextField *tf=_textfields[key];
        if (tf==textField)
        {
            [_infoDict setValue:tf.text forKey:key];
        }
    }
    /*
    for (int i=0; i<_profileSections.count; i++)
    {
        NSDictionary *dict=[_profileSections objectAtIndex:i];
        if (dict[@"textfield"]==textField)
        {
            NSLog(@"textfield found");
            NSString *key=dict[@"idname"];
            [_infoDict setValue:textField.text forKey:key];
        }
    }*/
    NSLog(@"infodict:%@",_infoDict);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITextViewDelegate methods

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [_listPicker hide];
    [_datePicker hide];
    
    CGPoint p = [textView.superview convertPoint:textView.frame.origin toView:self.view];
    [self.tableView setContentOffset:CGPointMake(0, p.y-100) animated:YES];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
    }
    else
    {
        NSString *newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
        for (int i=0; i<_profileSections.count; i++)
        {
            NSDictionary *dict = [_profileSections objectAtIndex:i];
            if (dict[@"textfield"] == textView)
            {
                NSLog(@"textView found");
                NSString *key = dict[@"idname"];
                [_infoDict setValue:newText forKey:key];
            }
        }
    }
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    for (int i=0; i<_profileSections.count; i++)
    {
        NSDictionary *dict=[_profileSections objectAtIndex:i];
        if (dict[@"textfield"]==textView)
        {
            NSLog(@"textView found");
            NSString *key=dict[@"idname"];
            [_infoDict setValue:textView.text forKey:key];
        }
    }
    NSLog(@"infodict:%@",_infoDict);
}

#pragma mark - UITableView Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 8;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return _childSectionCells.count;
            break;
        case 2:
            return 3;
            break;
        case 3:
            return 1;
            break;
        case 4:
            return 1;
            break;
        case 5:
            return 1;
            break;
        case 6:
            return 1;
            break;
        case 7:
            return 1;
            break;
        default:
            break;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row)
            {
                case 0:
                {
                    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section0_textview"];
                    if (cell)
                    {
                        NSLog(@"cell for row %ld found!",(long)indexPath.row);
                        cell.contentView.layer.borderWidth=0.5;
                        cell.contentView.layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];
                        _topTextView=(UITextView*)[cell viewWithTag:1];
                        UIImageView *imageChild = [UIImageView new];
                        if (_editMode) {
                            imageChild=(UIImageView*)[cell viewWithTag:767];
                        }
                        [(NoteView*)_topTextView.superview setTopView:true];
                        if (_editMode)
                        {
                            NSString *chFirstName=[self.infoDict valueForKeyPath:@"child.first_name"];
                            NSString *chLastName=[self.infoDict valueForKeyPath:@"child.last_name"];
                            NSString *chBirth=[self.infoDict valueForKeyPath:@"child.birth"];
                           
                            NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
                            formatter.dateFormat=@"dd.MM.yyyy";
                            NSDate *birthDate=[formatter dateFromString:chBirth];
                            if (!birthDate){
                                birthDate = [NSDate date];
                            }
                            NSInteger month = [[[NSCalendar currentCalendar] components: NSCalendarUnitMonth
                                                                               fromDate: birthDate
                                                                                 toDate: [NSDate date]
                                                                                options: 0] month];
                            
                            NSString *monthString = month == 1 ? NSLocalizedString(@"month", nil) : NSLocalizedString(@"months", nil);
                            
                           NSString *chAge = [NSString stringWithFormat:@"%d %@", (int)month, monthString];

                            NSString *chRoom=[self.infoDict valueForKeyPath:@"child.room_title"];

                            NSString *firstString;
                            NSRange foundRange = [[NSString stringWithFormat:NSLocalizedString(@"as.child.addEdit.editInfo", nil),chFirstName,chLastName, chAge, chBirth, chRoom] rangeOfString:@"\n"];
                            if (foundRange.location != NSNotFound) {
                                foundRange.length = foundRange.location + 1;
                                foundRange.location = 0;
                                firstString = [[NSString alloc] initWithString:[[NSString stringWithFormat:NSLocalizedString(@"as.child.addEdit.editInfo", nil),chFirstName,chLastName, chAge, chBirth, chRoom] substringWithRange:foundRange]];
                            }
                            
                            if (_editMode) {
                                NSString *imagePath=[NSString stringWithFormat:@"%@/%@",[NetworkManager sharedInstance].baseUrl, self.infoDict[@"child"][@"image"]];
                                
                                [[Utils sharedInstance]downloadImageAtPath:imagePath completionHandler:^(UIImage *image) {
                                    imageChild.image = image;
                                    imageChild.layer.borderWidth = 0.5;
                                    imageChild.layer.borderColor = [[UIColor lightGrayColor]CGColor];
                                    imageChild.layer.cornerRadius = imageChild.frame.size.height/2;
                                    imageChild.layer.masksToBounds = YES;
                                }];
                                
                                UIBezierPath * imgRect = [UIBezierPath bezierPathWithRect:CGRectMake(imageChild.frame.origin.x-5, imageChild.frame.origin.y-5, imageChild.frame.size.width, imageChild.frame.size.height)];
                                _topTextView.textContainer.exclusionPaths = @[imgRect];
                                
                                _topTextView.attributedText =  [self highlightString:firstString inString:[NSString stringWithFormat:NSLocalizedString(@"as.child.addEdit.editInfoDoubleN", nil),chFirstName,chLastName, chAge, chBirth, chRoom]];
                                _topTextView.textColor = [MyColorTheme sharedInstance].textViewTextColor;
                            }
                        }
                        
                        return cell;
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            NSDictionary *dict=[_childSectionCells objectAtIndex:indexPath.row];
            NSString *cellType=dict[@"cellType"];
            if ([cellType isEqualToString:@"section1_content"])
            {
                ChildContentCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section1_content"];
                if (cell)
                {
                    NSLog(@"cell for row %ld found!",(long)indexPath.row);
                    
                    _childFirstNameTextField=cell.childFirstNameTextField;
                    _childLastNameTextField=cell.childLastNameTextField;
                    _childPostCodeTextField=cell.childPostCodeTextField;
                    _childHouseStreetTextField=cell.childHouseStreetTextField;
                    _childAreaTextField=cell.childAreaTextField;
                    _childTownTextField=cell.childTownTextField;
                    _childCountryTextField=cell.childCountryTextField;
//                    _parentAlertsSwitch=(CheckSwitch*)[cell viewWithTag:6666];
//                    _parentAlertsSwitch.delegate = self;
                    
                    _childStartingDateTextField=cell.childStartingDateTF;
                    _childFinishedDateTextField=cell.childFinishDateTF;
                    
                    [_textfields setValue:_childStartingDateTextField forKey:@"starting"];
                    [_textfields setValue:_childFinishedDateTextField forKey:@"finished"];
                    [_textfields setValue:_childFirstNameTextField forKey:@"first_name"];
                    [_textfields setValue:_childLastNameTextField forKey:@"last_name"];
                    [_textfields setValue:_childPostCodeTextField forKey:@"ch_postcode"];
                    [_textfields setValue:_childHouseStreetTextField forKey:@"ch_street_1"];
                    [_textfields setValue:_childAreaTextField forKey:@"ch_area"];
                    [_textfields setValue:_childTownTextField forKey:@"ch_town"];
                    [_textfields setValue:_childCountryTextField forKey:@"ch_county"];
                    
                    [self setInfoForIndexPath:indexPath cellType:cellType];
                    
                    for (int tag=201; tag<=204; tag++)
                    {
                        [cell viewWithTag:tag].layer.borderWidth=0.5;
                        [cell viewWithTag:tag].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];
                    }
                    return cell;
                }
            }
            if ([cellType isEqualToString:@"section1_content2"])
            {
                ChildContent2Cell *cell=[tableView dequeueReusableCellWithIdentifier:[NetworkManager sharedInstance].isJ3 ? @"section1_content2" : @"section1_content2j15"];
                if (cell)
                {
                    NSLog(@"cell for row %ld found!",(long)indexPath.row);

                    _childBoyOrGirlTextField=cell.childBoyOrGirlTextField;
                    _childDateOfBirthTextField=cell.childDateOfBirthTextField;
                    _childSpecialEducationCheckSwitch=cell.childSpecialEducationCheckSwitch;
                    _childInNappiesCheckSwitch=cell.childInNappiesCheckSwitch;
                    _childFundedCheckSwitch=cell.childFundedCheckSwitch;
                    _childEALCheckSwitch=cell.childEALCheckSwitch;
                    _childPremiumCheckSwitch=cell.childPremiumCheckSwitch;
                    _parentAlertsSwitch=cell.childAlertsCheckSwitch;
                    
                    _childRoomTextField=cell.childRoomTextField;
                    _childRoomMoveTextField=cell.childRoomMoveTextField;

                    _childSpecialEducationCheckSwitch.delegate=self;
                    _childInNappiesCheckSwitch.delegate=self;
                    _childFundedCheckSwitch.delegate=self;
                    _childEALCheckSwitch.delegate=self;
                    _childPremiumCheckSwitch.delegate=self;
                    _parentAlertsSwitch.delegate = self;

                    [_checkSwitches addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:_childSpecialEducationCheckSwitch,@"senco",nil]];
                    if (![NetworkManager sharedInstance].isJ3) {
                        [_checkSwitches addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:_childInNappiesCheckSwitch,@"in_nappies",nil]];
                    }
                    
                    [_checkSwitches addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:_childFundedCheckSwitch,@"funded",nil]];
                    [_checkSwitches addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:_childEALCheckSwitch,@"english_as_addition",nil]];
                    [_checkSwitches addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:_childPremiumCheckSwitch,@"premium",nil]];
                    
                    [_textfields setValue:_childBoyOrGirlTextField forKey:@"gender"];
                    [_textfields setValue:_childDateOfBirthTextField forKey:@"birth"];
                    [_textfields setValue:_childPostCodeTextField forKey:@"ch_postcode"];
                    [_textfields setValue:_childHouseStreetTextField forKey:@"ch_street_1"];
                    
                    [self setInfoForIndexPath:indexPath cellType:cellType];
                    
                    for (int tag=201; tag<=204; tag++)
                    {
                        [cell viewWithTag:tag].layer.borderWidth=0.5;
                        [cell viewWithTag:tag].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];
                    }
                    
//                    if (!self.editMode) {
//                        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
//                        formatter.dateFormat=@"dd.MM.yyyy";
//                        cell.childDateOfBirthTextField.text = [formatter stringFromDate:[NSDate date]];
//                    }
                    
                    return cell;
                }
            }
            
            if ([cellType isEqualToString:@"section1_moveRoomDate"])
            {
                ChildMoveDateCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section1_moveRoomDate"];
                if (cell)
                {
                    NSLog(@"cell for row %ld found!",(long)indexPath.row);
                    
                    UITextField *textField=cell.childMoveDateTextField;//(UITextField*)[cell viewWithTag:112];
                    _childMoveDateTextField=textField;
                    _childMoveDateTextField.text=@"";
                    if ([_infoDict[@"waiting_1_date"] isKindOfClass:[NSString class]])
                    {
                        _childMoveDateTextField.text= [_infoDict[@"waiting_1_date"] isEqualToString:@"00.00.0000"] ? @"" : _infoDict[@"waiting_1_date"];
                    }
                    return cell;
                }
            }
            if ([cellType isEqualToString:@"section1_backDateProgress"])
            {
                ChildBackDateProgressCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section1_backDateProgress"];
                if (cell)
                {
                    NSLog(@"cell for row %ld found!",(long)indexPath.row);
                    
                    _childBackDateProgressCheckSwitch=cell.childBackDateProgressCheckSwitch;
                    _childBackDateProgressCheckSwitch.delegate=self;
                    
                    [_checkSwitches addEntriesFromDictionary: @{@"published" : _childBackDateProgressCheckSwitch}];
                    
                    [self setInfoForIndexPath:indexPath cellType:cellType];
                    
                    for (int tag=201; tag<=204; tag++)
                    {
                        [cell viewWithTag:tag].layer.borderWidth=0.5;
                        [cell viewWithTag:tag].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];
                    }
                    return cell;
                }
            }
            if ([cellType isEqualToString:@"section1_progressDates"])
            {
                ChildProgressDatesCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section1_progressDates"];
                if (cell)
                {
                    NSLog(@"cell for row %ld found!",(long)indexPath.row);
                    
                    _childStartingDateTextField=cell.childStartingDateTextField;
                    _childFinishedDateTextField=cell.childFinishedDateTextField;
                    
                    [_textfields setValue:_childStartingDateTextField forKey:@"starting"];
                    [_textfields setValue:_childFinishedDateTextField forKey:@"finished"];
                    
                    [self setInfoForIndexPath:indexPath cellType:cellType];
                    
                    return cell;
                }
            }
            else if ([cellType isEqualToString:@"profileSection"])
            {
                int index=(int)indexPath.row-1;
                NSDictionary *profileDict=[_profileSections objectAtIndex:index];
                NSMutableDictionary *dict=[[NSMutableDictionary alloc]initWithDictionary:profileDict];
                [_profileSections replaceObjectAtIndex:index withObject:dict];

                NSString *helperTitle = dict[@"helperTitle"];

                if (!self.editMode && !self.isCopyMode) {
                    helperTitle = dict[@"title"];
                }
                
                int type=[dict[@"type"]intValue];
                NSString *title=[NSString stringWithFormat:@"%@:*",(NSString*)helperTitle];
                
                NSString *idName=(NSString*)dict[@"idname"];
                if (type==0)
                {
                    //text
                    ChildProfileSectionTextviewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section1_profileSectionText"];
                    if (cell)
                    {
                        NSLog(@"cell for row %ld found!",(long)indexPath.row);
                        
                        UILabel *titleLabel=cell.profileSectionTitleLabel;//(UILabel*)[cell viewWithTag:111];
                        UITextView *textView=cell.profileSectionTextView;//(UITextView*)[cell viewWithTag:112];
                        [dict setValue:textView forKey:@"textfield"];
                        titleLabel.text=[NSString stringWithFormat:@"%@:", helperTitle];
                        if (!self.editMode && !self.isCopyMode) {
                            textView.text = @"";
                        }else{
                            textView.text = dict[@"title"];
                        }
                        textView.layer.cornerRadius=5.0;
                        textView.tag=1500+indexPath.row;
                        return cell;
                    }
                }
                else if(type==1)
                {
                    //single line
                    ChildProfileSectionTextfieldCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section1_profileSectionTextField"];
                    if (cell)
                    {
                        NSLog(@"cell for row %ld found!",(long)indexPath.row);
                        
                        UILabel *titleLabel=cell.profileSectionTitleLabel;//(UILabel *)[cell viewWithTag:111];
                        UITextField *textField=cell.profileSectionTextField;//(UITextField *)[cell viewWithTag:112];
                        [dict setValue:textField forKey:@"textfield"];
                        titleLabel.text=[NSString stringWithFormat:@"%@:", helperTitle];;
                        if (!self.editMode && !self.isCopyMode) {
                            textField.text = @"";
                        }else{
                            textField.text = dict[@"title"];
                        }
                        textField.tag=1500+indexPath.row;
                        return cell;
                    }
                }
                else if(type==2)
                {
                    //multiple list
                    ChildProfileSectionTextfieldCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section1_profileSectionTextField"];
                    if (cell)
                    {
                        NSLog(@"cell for row %ld found!",(long)indexPath.row);
                        
                        UILabel *titleLabel=cell.profileSectionTitleLabel;//(UILabel*)[cell viewWithTag:111];
                        UITextField *textField=cell.profileSectionTextField;//(UITextField*)[cell viewWithTag:112];
                        [dict setValue:textField forKey:@"textfield"];
                        titleLabel.text=title;
                        textField.text=[_infoDict objectForKey:idName];
                        textField.tag=1500+indexPath.row;
                        return cell;
                    }
                }
            }
        }
            break;
        case 2:
        {
            switch (indexPath.row)
            {
                case 0:
                {
                    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section2_parent1"];
                    if (cell)
                    {
                        NSLog(@"cell for row %ld found!",(long)indexPath.row);
                        [cell viewWithTag:101].layer.borderWidth=0.5;
                        [cell viewWithTag:101].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];
                        
                        _parent1FirstNameTextField=(UITextField*)[cell viewWithTag:2101];
                        _parent1LastNameTextField=(UITextField*)[cell viewWithTag:2102];
                        _parent1PostCodeTextField=(UITextField*)[cell viewWithTag:2103];
                        _parent1HouseStreetTextField=(UITextField*)[cell viewWithTag:2104];
                        _parent1AreaTextField=(UITextField*)[cell viewWithTag:2105];
                        _parent1TownTextField=(UITextField*)[cell viewWithTag:2106];
                        _parent1CountryTextField=(UITextField*)[cell viewWithTag:2107];
                        _parent1EmailTextField=(UITextField*)[cell viewWithTag:2108];
                        _parent1TelephoneTextField=(UITextField*)[cell viewWithTag:2109];
                        _parent1MobileTextField=(UITextField*)[cell viewWithTag:2110];
                        _parent1PlaceOfWorkTextField=(UITextField*)[cell viewWithTag:2111];
                        _parent1WorkTelephoneTextField=(UITextField*)[cell viewWithTag:2112];
                        
                        [_textfields setValue:_parent1FirstNameTextField forKey:@"p1_first_name"];
                        [_textfields setValue:_parent1LastNameTextField forKey:@"p1_last_name"];
                        [_textfields setValue:_parent1PostCodeTextField forKey:@"p1_postcode"];
                        [_textfields setValue:_parent1HouseStreetTextField forKey:@"p1_street_1"];
                        [_textfields setValue:_parent1AreaTextField forKey:@"p1_area"];
                        [_textfields setValue:_parent1TownTextField forKey:@"p1_town"];
                        [_textfields setValue:_parent1CountryTextField forKey:@"p1_county"];
                        [_textfields setValue:_parent1EmailTextField forKey:@"p1_email"];
                        [_textfields setValue:_parent1TelephoneTextField forKey:@"p1_phone"];
                        [_textfields setValue:_parent1MobileTextField forKey:@"p1_mobile"];
                        [_textfields setValue:_parent1PlaceOfWorkTextField forKey:@"p1_workplace"];
                        [_textfields setValue:_parent1WorkTelephoneTextField forKey:@"p1_work_telephone"];
                        
                        [self setInfoForIndexPath:indexPath];
                        return cell;
                    }
                }
                    break;
                case 1:
                {
                    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section2_addButton"];
                    if (cell)
                    {
                        NSLog(@"cell for row %ld found!",(long)indexPath.row);
                        return cell;
                    }
                }
                    break;
                case 2:
                {
                    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section2_parent2"];
                    if (cell)
                    {
                        NSLog(@"cell for row %ld found!",(long)indexPath.row);
                        
                        _parent2FirstNameTextField=(UITextField*)[cell viewWithTag:2301];
                        _parent2LastNameTextField=(UITextField*)[cell viewWithTag:2302];
                        _parent2PostCodeTextField=(UITextField*)[cell viewWithTag:2303];
                        _parent2HouseStreetTextField=(UITextField*)[cell viewWithTag:2304];
                        _parent2AreaTextField=(UITextField*)[cell viewWithTag:2305];
                        _parent2TownTextField=(UITextField*)[cell viewWithTag:2306];
                        _parent2CountryTextField=(UITextField*)[cell viewWithTag:2307];
                        _parent2EmailTextField=(UITextField*)[cell viewWithTag:2308];
                        _parent2TelephoneTextField=(UITextField*)[cell viewWithTag:2309];
                        _parent2MobileTextField=(UITextField*)[cell viewWithTag:2310];
                        _parent2PlaceOfWorkTextField=(UITextField*)[cell viewWithTag:2311];
                        _parent2WorkTelephoneTextField=(UITextField*)[cell viewWithTag:2312];
                        [self setInfoForIndexPath:indexPath];
                        [cell viewWithTag:101].layer.borderWidth=0.5;
                        [cell viewWithTag:101].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];
                        
                        [_textfields setValue:_parent2FirstNameTextField forKey:@"p2_first_name"];
                        [_textfields setValue:_parent2LastNameTextField forKey:@"p2_last_name"];
                        [_textfields setValue:_parent2PostCodeTextField forKey:@"p2_postcode"];
                        [_textfields setValue:_parent2HouseStreetTextField forKey:@"p2_street_1"];
                        [_textfields setValue:_parent2AreaTextField forKey:@"p2_area"];
                        [_textfields setValue:_parent2TownTextField forKey:@"p2_town"];
                        [_textfields setValue:_parent2CountryTextField forKey:@"p2_county"];
                        [_textfields setValue:_parent2EmailTextField forKey:@"p2_email"];
                        [_textfields setValue:_parent2TelephoneTextField forKey:@"p2_phone"];
                        [_textfields setValue:_parent2MobileTextField forKey:@"p2_mobile"];
                        [_textfields setValue:_parent2PlaceOfWorkTextField forKey:@"p2_workplace"];
                        [_textfields setValue:_parent2WorkTelephoneTextField forKey:@"p2_work_telephone"];

                        return cell;
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 3:
        {
            switch (indexPath.row)
            {
                case 0:
                {
                    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section3_content"];
                    if (cell)
                    {
                        NSLog(@"cell for row %ld found!",(long)indexPath.row);
                        
                        _sendEmailToParent1CheckSwitch=(CheckSwitch*)[cell viewWithTag:313];
                        _sendEmailToParent2CheckSwitch=(CheckSwitch*)[cell viewWithTag:314];
                        
                        _sendEmailToParent1CheckSwitch.delegate=self;
                        _sendEmailToParent2CheckSwitch.delegate=self;
                        
                        [_checkSwitches addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:                                                              _sendEmailToParent1CheckSwitch,@"p1_email_notify",
                            _sendEmailToParent2CheckSwitch,@"p2_email_notify",
                                                                  nil]];
                        
                        
                        [self setInfoForIndexPath:indexPath];
                        
                        [cell viewWithTag:101].layer.borderWidth=0.5;
                        [cell viewWithTag:101].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];

                        [cell viewWithTag:102].layer.borderWidth=0.5;
                        [cell viewWithTag:102].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];
                        
                        return cell;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case 4:
        {
            switch (indexPath.row)
            {                case 0:
                {
                    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:[NetworkManager sharedInstance].isJ3 ? @"section4_content" : @"section4_contentJ15"];
                    if (cell)
                    {
                        NSLog(@"cell for row %ld found!",(long)indexPath.row);
                        
                        _enableParent1AccessCheckSwitch=(CheckSwitch*)[cell viewWithTag:4103];
                        _enableParent2AccessCheckSwitch=(CheckSwitch*)[cell viewWithTag:4104];
                        
                        _selectAllSwitch = (CheckSwitch*)[cell viewWithTag:4777];
                        _selectAllSwitch.color = [UIColor whiteColor];
                        _selectAllSwitch.button.center = CGPointMake(10, 15);

                        
                        _deselectAllSwitch = (CheckSwitch*)[cell viewWithTag:4888];
                        _deselectAllSwitch.color = [UIColor whiteColor];
                        _deselectAllSwitch.button.center = CGPointMake(20, 15);

                        if ([NetworkManager sharedInstance].isJ3) {
                            _parent1CalendarSwitch = (CheckSwitch*)[cell viewWithTag:4150];
                            _parent1DailyNotesSwitch = (CheckSwitch*)[cell viewWithTag:4152];
                            _parent1SleepingSwitch = (CheckSwitch*)[cell viewWithTag:4154];
                            _parent1BottlesSwitch = (CheckSwitch*)[cell viewWithTag:4156];
                            
                            _parent2CalendarSwitch = (CheckSwitch*)[cell viewWithTag:4151];
                            _parent2DailyNotesSwitch = (CheckSwitch*)[cell viewWithTag:4153];
                            _parent2SleepingSwitch = (CheckSwitch*)[cell viewWithTag:4155];
                            _parent2BottlesSwitch = (CheckSwitch*)[cell viewWithTag:4157];
                        }
                        else {
                            _parent1AccessToDiariesCheckSwitch=(CheckSwitch*)[cell viewWithTag:4105];
                            _parent2AccessToDiariesCheckSwitch=(CheckSwitch*)[cell viewWithTag:4122];

                        }
                        _parent1AccessToProgressCheckSwitch=(CheckSwitch*)[cell viewWithTag:4106];
                        _parent1AccessToPoliciesCheckSwitch=(CheckSwitch*)[cell viewWithTag:4109];
                        _parent1AccessToPermissionsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4110];
                        _parent1AccessToMedicalCheckSwitch=(CheckSwitch*)[cell viewWithTag:4111];
                        _parent1AccessToGalleriesCheckSwitch=(CheckSwitch*)[cell viewWithTag:4112];
                        _parent1AccessToRegisterCheckSwitch=(CheckSwitch*)[cell viewWithTag:4113];
                        _parent1AccessToBookKeepingCheckSwitch=(CheckSwitch*)[cell viewWithTag:4114];
                        _parent1AccessToCommunicateCheckSwitch=(CheckSwitch*)[cell viewWithTag:4115];
                        _parent1AccessToMenusCheckSwitch=(CheckSwitch*)[cell viewWithTag:4116];
                        _parent1AccessToQuestionsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4118];
                        _parent1AccessToContactsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4117];
                        _parent1AccessToRiskAssessmentsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4119];
                        _parent1NappiesSwitch=(CheckSwitch*)[cell viewWithTag:4120];
                        _parent1PlanningSwitch=(CheckSwitch*)[cell viewWithTag:4121];


                        _parent2AccessToProgressCheckSwitch=(CheckSwitch*)[cell viewWithTag:4123];
                        _parent2AccessToPoliciesCheckSwitch=(CheckSwitch*)[cell viewWithTag:4126];
                        _parent2AccessToPermissionsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4127];
                        _parent2AccessToMedicalCheckSwitch=(CheckSwitch*)[cell viewWithTag:4128];
                        _parent2AccessToGalleriesCheckSwitch=(CheckSwitch*)[cell viewWithTag:4129];
                        _parent2AccessToRegisterCheckSwitch=(CheckSwitch*)[cell viewWithTag:4130];
                        _parent2AccessToBookKeepingCheckSwitch=(CheckSwitch*)[cell viewWithTag:4131];
                        _parent2AccessToCommunicateCheckSwitch=(CheckSwitch*)[cell viewWithTag:4132];
                        _parent2AccessToMenusCheckSwitch=(CheckSwitch*)[cell viewWithTag:4133];
                        _parent2AccessToQuestionsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4135];
                        _parent2AccessToContactsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4134];
                        _parent2AccessToRiskAssessmentsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4136];
                        _parent2NappiesSwitch=(CheckSwitch*)[cell viewWithTag:4137];
                        _parent2PlanningSwitch=(CheckSwitch*)[cell viewWithTag:4138];

                        //_parent1AccessToObservationsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4109];
                        //_parent1AccessToNextstepsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4110];
                        _parent1AccessToObservationsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4107];
                        _parent1AccessToNextstepsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4108];
                        
                        _parent2AccessToObservationsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4124];
                        _parent2AccessToNextstepsCheckSwitch=(CheckSwitch*)[cell viewWithTag:4125];
                    
                        
                        
                        
                        _parent2AccessToContactsCheckSwitch.delegate = self;
                        _parent2AccessToRiskAssessmentsCheckSwitch.delegate = self;
                        
                        _parent2NappiesSwitch.delegate = self;
                        _parent2PlanningSwitch.delegate = self;
                        
                        _parent1AccessToContactsCheckSwitch.delegate = self;
                        _parent1AccessToRiskAssessmentsCheckSwitch.delegate = self;
                        _parent1NappiesSwitch.delegate = self;
                        _parent1PlanningSwitch.delegate = self;

                        _enableParent1AccessCheckSwitch.delegate=self;
                        _enableParent2AccessCheckSwitch.delegate=self;
                        
                        _selectAllSwitch.delegate = self;
                        _deselectAllSwitch.delegate = self;

                        _parent1AccessToDiariesCheckSwitch.delegate=self;
                        _parent1AccessToProgressCheckSwitch.delegate=self;
                        _parent1AccessToPoliciesCheckSwitch.delegate=self;
                        _parent1AccessToPermissionsCheckSwitch.delegate=self;
                        _parent1AccessToMedicalCheckSwitch.delegate=self;
                        _parent1AccessToGalleriesCheckSwitch.delegate=self;
                        _parent1AccessToRegisterCheckSwitch.delegate=self;
                        _parent1AccessToBookKeepingCheckSwitch.delegate=self;
                        _parent1AccessToCommunicateCheckSwitch.delegate=self;
                        _parent1AccessToMenusCheckSwitch.delegate=self;
                        _parent1AccessToQuestionsCheckSwitch.delegate=self;
                        
                        _parent2AccessToDiariesCheckSwitch.delegate=self;
                        _parent2AccessToProgressCheckSwitch.delegate=self;
                        _parent2AccessToPoliciesCheckSwitch.delegate=self;
                        _parent2AccessToPermissionsCheckSwitch.delegate=self;
                        _parent2AccessToMedicalCheckSwitch.delegate=self;
                        _parent2AccessToGalleriesCheckSwitch.delegate=self;
                        _parent2AccessToRegisterCheckSwitch.delegate=self;
                        _parent2AccessToBookKeepingCheckSwitch.delegate=self;
                        _parent2AccessToCommunicateCheckSwitch.delegate=self;
                        _parent2AccessToMenusCheckSwitch.delegate=self;
                        _parent2AccessToQuestionsCheckSwitch.delegate=self;
                        
                        _parent1AccessToObservationsCheckSwitch.delegate=self;
                        _parent1AccessToNextstepsCheckSwitch.delegate=self;
                        
                        _parent2AccessToObservationsCheckSwitch.delegate=self;
                        _parent2AccessToNextstepsCheckSwitch.delegate=self;
                        
                        
                        _parent1CalendarSwitch.delegate = self;
                        _parent1DailyNotesSwitch.delegate = self;
                        _parent1SleepingSwitch.delegate = self;
                        _parent1BottlesSwitch.delegate = self;
                        
                        _parent2CalendarSwitch.delegate = self;
                        _parent2DailyNotesSwitch.delegate = self;
                        _parent2SleepingSwitch.delegate = self;
                        _parent2BottlesSwitch.delegate = self;
                        
                        
                        //////////////////
                        [_checkSwitches addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:
                        _sendEmailToParent1CheckSwitch,@"p1_email_notify",
                        _sendEmailToParent2CheckSwitch,@"p2_email_notify",
                        
                        //parent/carer access
                        _parent1AccessToProgressCheckSwitch, @"p1_progress",
                        _parent1AccessToObservationsCheckSwitch, @"p1_observations",
                        _parent1AccessToNextstepsCheckSwitch, @"p1_nextsteps",
                        _parent1AccessToPoliciesCheckSwitch,@"p1_policies",
                        _parent1AccessToPermissionsCheckSwitch,@"p1_permissions",
                        _parent1AccessToMedicalCheckSwitch,@"p1_medical",
                        _parent1AccessToGalleriesCheckSwitch,@"p1_gallery",
                        _parent1AccessToRegisterCheckSwitch,@"p1_register",
                        _parent1AccessToBookKeepingCheckSwitch,@"p1_bookkeeping",
                        _parent1AccessToCommunicateCheckSwitch,@"p1_communicate",
                        _parent1AccessToMenusCheckSwitch,@"p1_meal_planning",
                        _parent1AccessToQuestionsCheckSwitch, @"p1_questionnaires",
                        _parent1AccessToContactsCheckSwitch, @"p1_contracts",
                        _parent1AccessToRiskAssessmentsCheckSwitch, @"p1_risk_assessments",
                        _parent1NappiesSwitch, @"p1_nappies",
                        _parent1PlanningSwitch, @"p1_short_term_planning",

                        _parent2AccessToProgressCheckSwitch,@"p2_progress",
                        _parent2AccessToObservationsCheckSwitch, @"p2_observations",
                        _parent2AccessToNextstepsCheckSwitch, @"p2_nextsteps",
                        _parent2AccessToPoliciesCheckSwitch,@"p2_policies",
                        _parent2AccessToPermissionsCheckSwitch,@"p2_permissions",
                        _parent2AccessToMedicalCheckSwitch,@"p2_medical",
                        _parent2AccessToGalleriesCheckSwitch,@"p2_gallery",
                        _parent2AccessToRegisterCheckSwitch,@"p2_register",
                        _parent2AccessToBookKeepingCheckSwitch,@"p2_bookkeeping",
                        _parent2AccessToCommunicateCheckSwitch,@"p2_communicate",
                        _parent2AccessToMenusCheckSwitch,@"p2_meal_planning",
                        _parent2AccessToQuestionsCheckSwitch, @"p2_questionnaires",
                        _parent2AccessToContactsCheckSwitch, @"p2_contracts",
                        _parent2AccessToRiskAssessmentsCheckSwitch, @"p2_risk_assessments",
                        _parent2NappiesSwitch, @"p2_nappies",
                        _parent2PlanningSwitch, @"p2_short_term_planning",

                                                                  nil]];
                        
                        if ([NetworkManager sharedInstance].isJ3) {
                            [_checkSwitches setObject:_parent1CalendarSwitch forKey:@"p1_calendar"];
                            [_checkSwitches setObject:_parent1DailyNotesSwitch forKey:@"p1_notes"];
                            [_checkSwitches setObject:_parent1SleepingSwitch forKey:@"p1_sleeps"];
                            [_checkSwitches setObject:_parent1BottlesSwitch forKey:@"p1_bottles"];
                            
                            [_checkSwitches setObject:_parent2CalendarSwitch forKey:@"p2_calendar"];
                            [_checkSwitches setObject:_parent2DailyNotesSwitch forKey:@"p2_notes"];
                            [_checkSwitches setObject:_parent2SleepingSwitch forKey:@"p2_sleeps"];
                            [_checkSwitches setObject:_parent2BottlesSwitch forKey:@"p2_bottles"];
                        }
                        else {
                            [_checkSwitches setObject:_parent1AccessToDiariesCheckSwitch forKey:@"p1_diaries"];
                            [_checkSwitches setObject:_parent2AccessToDiariesCheckSwitch forKey:@"p2_diaries"];
                        }
                        
                        
                        [self setInfoForIndexPath:indexPath];
                        
                        [cell viewWithTag:101].layer.borderWidth=0.5;
                        [cell viewWithTag:101].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];

                        [cell viewWithTag:102].layer.borderWidth=0.5;
                        [cell viewWithTag:102].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];

                        [cell viewWithTag:103].layer.borderWidth=0.5;
                        [cell viewWithTag:103].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];

                        return cell;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case 5:
        {
            switch (indexPath.row)
            {
                case 0:
                {
                    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:[NetworkManager sharedInstance].isJ3 ? @"section5_content" : @"section5_contentJ15"];
                    if (cell)
                    {
                        NSLog(@"cell for row %ld found!",(long)indexPath.row);
                        
                        _emergencyFirstNameTextField=(UITextField*)[cell viewWithTag:5101];
                        _emergencyLastNameTextField=(UITextField*)[cell viewWithTag:5102];
                        _emergencyPostCodeTextField=(UITextField*)[cell viewWithTag:5103];
                        _emergencyHouseStreetTextField=(UITextField*)[cell viewWithTag:5104];
                        _emergencyAreaTextField=(UITextField*)[cell viewWithTag:5105];
                        _emergencyTownTextField=(UITextField*)[cell viewWithTag:5106];
                        _emergencyCountryTextField=(UITextField*)[cell viewWithTag:5107];
                        _emergencyTelephoneTextField=(UITextField*)[cell viewWithTag:5108];
                        _emergencyMobileTextField=(UITextField*)[cell viewWithTag:5109];
                        _emergencyPlaceOfWorkTextField=(UITextField*)[cell viewWithTag:5110];
                        _emergencyWorkTelephoneTextField=(UITextField*)[cell viewWithTag:5111];
                        
                        _doctorsNameTextField=(UITextField*)[cell viewWithTag:5112];
                        _doctorsPhoneTextField=(UITextField*)[cell viewWithTag:5113];
                        
                        _diphteriaCheckSwitch=(CheckSwitch*)[cell viewWithTag:5114];
                        _measlesCheckSwitch=(CheckSwitch*)[cell viewWithTag:5115];
                        _polioCheckSwitch=(CheckSwitch*)[cell viewWithTag:5116];
                        _tetanusCheckSwitch=(CheckSwitch*)[cell viewWithTag:5117];
                        _meningitisCheckSwitch=(CheckSwitch*)[cell viewWithTag:5118];
                        _mumpsCheckSwitch=(CheckSwitch*)[cell viewWithTag:5119];
                        _rubellaCheckSwitch=(CheckSwitch*)[cell viewWithTag:5120];
                        _whoopingCoughCheckSwitch=(CheckSwitch*)[cell viewWithTag:5121];
                        
                        _diphteriaCheckSwitch.delegate=self;
                        _measlesCheckSwitch.delegate=self;
                        _polioCheckSwitch.delegate=self;
                        _tetanusCheckSwitch.delegate=self;
                        _meningitisCheckSwitch.delegate=self;
                        _mumpsCheckSwitch.delegate=self;
                        _rubellaCheckSwitch.delegate=self;
                        _whoopingCoughCheckSwitch.delegate=self;
                        
                        if (![NetworkManager sharedInstance].isJ3) {
                            [_checkSwitches addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                      _diphteriaCheckSwitch,@"dip",
                                                                      _measlesCheckSwitch,@"mea",
                                                                      _mumpsCheckSwitch,@"mum",
                                                                      _meningitisCheckSwitch,@"hib",
                                                                      _polioCheckSwitch,@"pol",
                                                                      _rubellaCheckSwitch,@"rub",
                                                                      _tetanusCheckSwitch,@"tet",
                                                                      _whoopingCoughCheckSwitch,@"who",
                                                                      nil]];
                        }
                        
                        [self setInfoForIndexPath:indexPath];
                        
                        [cell viewWithTag:101].layer.borderWidth=0.5;
                        [cell viewWithTag:101].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];

                        [cell viewWithTag:102].layer.borderWidth=0.5;
                        [cell viewWithTag:102].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];
                        
                        [_textfields setValue:_emergencyFirstNameTextField forKey:@"ec_first_name"];
                        [_textfields setValue:_emergencyLastNameTextField forKey:@"ec_last_name"];
                        [_textfields setValue:_emergencyPostCodeTextField forKey:@"ec_postcode"];
                        [_textfields setValue:_emergencyHouseStreetTextField forKey:@"ec_street_1"];
                        [_textfields setValue:_emergencyAreaTextField forKey:@"ec_area"];
                        [_textfields setValue:_emergencyTownTextField forKey:@"ec_town"];
                        [_textfields setValue:_emergencyCountryTextField forKey:@"ec_county"];
                        [_textfields setValue:_emergencyTelephoneTextField forKey:@"ec_telephone"];
                        [_textfields setValue:_emergencyMobileTextField forKey:@"ec_mobile"];
                        [_textfields setValue:_emergencyPlaceOfWorkTextField forKey:@"ec_workplace"];
                        [_textfields setValue:_emergencyWorkTelephoneTextField forKey:@"ec_work_telephone"];
                        [_textfields setValue:_doctorsNameTextField forKey:@"ec_doctor_name"];
                        [_textfields setValue:_doctorsPhoneTextField forKey:@"ec_doctor_tel"];
                        
                        return cell;
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 6:
        {
            switch (indexPath.row)
            {                case 0:
                {
                    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section6_content"];
                    if (cell)
                    {
                        NSLog(@"cell for row %ld found!",(long)indexPath.row);
                        self.addPhotoButton = (UIButton*)[cell viewWithTag:601];
                        [self setInfoForIndexPath:indexPath];

                        _addPhotoButton.layer.borderWidth=0.5;
                        _addPhotoButton.layer.borderColor=[[UIColor lightGrayColor]CGColor];
                        _addPhotoButton.layer.cornerRadius=38;
                        _addPhotoButton.layer.masksToBounds=YES;
                        
                        return cell;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case 7:
        {
            switch (indexPath.row)
            {
                case 0:
                {
                    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"section7_saveButton"];
                    if (cell)
                    {
                        NSLog(@"cell for row %ld found!",(long)indexPath.row);
                        _saveButton=(UIButton*)[cell viewWithTag:701];
                        if (_editMode)
                        {
                            [_saveButton setTitle:@"Save Data" forState:UIControlStateNormal];
                        }
                        return cell;
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;

        default:
            break;
    }
    return [[UITableViewCell alloc]init];
}



- (NSAttributedString *)highlightString:(NSString*)subString inString:(NSString *)mainString
{
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString:[[NSAttributedString alloc] initWithString:mainString]];
    if (!subString) {
        subString = mainString;
    }
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font
                                                                forKey:NSFontAttributeName];
    text = [[NSMutableAttributedString alloc] initWithString:text.string attributes:attrsDictionary];

    UIFont *boldFont= [UIFont fontWithName:@"HelveticaNeue-Medium" size:11.f];

    NSRange titleRange = [[mainString lowercaseString] rangeOfString:[subString lowercaseString]];
    [text addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(titleRange.location, subString.length)];
    
    if ([mainString containsString:NSLocalizedString(@"as.child.addEdit.highlights.age", nil)]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:NSLocalizedString(@"as.child.addEdit.highlights.age", nil)];
        [text addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(rangeOfLeftBracketString.location, 4)];
    }
    if ([mainString containsString:NSLocalizedString(@"as.child.addEdit.highlights.dob", nil)]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:NSLocalizedString(@"as.child.addEdit.highlights.dob", nil)];
        [text addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(rangeOfLeftBracketString.location, 14)];
    }
    if ([mainString containsString:NSLocalizedString(@"as.child.addEdit.highlights.assignedRoom", nil)]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:NSLocalizedString(@"as.child.addEdit.highlights.assignedRoom", nil)];
        [text addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(rangeOfLeftBracketString.location, 14)];
    }

    return text;
}

-(void)drawArrowAtSection:(int)section
{
    NSLog(@"section:%d",section);
    switch (section) {
        case 1:
            _arrow1.image=[[UIImage imageNamed:(_showSection1?@"arrowUp":@"arrowDown")]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            break;
        case 2:
            _arrow2.image=[[UIImage imageNamed:(_showSection2?@"arrowUp":@"arrowDown")]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            break;
        case 3:
            _arrow3.image=[[UIImage imageNamed:(_showSection3?@"arrowUp":@"arrowDown")]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            break;
        case 4:
            _arrow4.image=[[UIImage imageNamed:(_showSection4?@"arrowUp":@"arrowDown")]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            break;
        case 5:
            _arrow5.image=[[UIImage imageNamed:(_showSection5?@"arrowUp":@"arrowDown")]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            break;
        case 6:
            _arrow6.image=[[UIImage imageNamed:(_showSection6?@"arrowUp":@"arrowDown")]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            break;
            
        default:
            break;
    }

}

-(void)scrollToSection:(int)section
{
    CGRect rect=[self.tableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    NSLog(@"rectForSection %d is :%f %f",section,rect.origin.x,rect.origin.y);
    NSLog(@"scrollToSection:%d offset:%f",section,rect.origin.y-56);
    if (section==6)
    {
        CGRect rect2=[self.tableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:7]];
        int offset=rect2.origin.y+rect2.size.height-self.tableView.frame.size.height;
        [self.tableView setContentOffset:CGPointMake(0, offset) animated:YES];
    }
    else
    {
        [self.tableView setContentOffset:CGPointMake(0, rect.origin.y-56) animated:YES];
    }
}

-(void)headerTapped:(id)sender
{
    
    //Иначе баг при открытой клавиатуре
    [self.view endEditing:YES];
    
    UITapGestureRecognizer *tap=sender;
    int section=(int)[tap.view tag];
    NSLog(@"did select header at section:%d",section);
    
    switch (section)
    {
        case 1: {
            _showSection1=!_showSection1;
            if (_showSection1 && _editMode) {
                _showSection2 = false;
                _showSection3 = false;
                _showSection4 = false;
                _showSection5 = false;
                _showSection6 = false;
            }
        }
            break;
            
        case 2:  {
            _showSection2=!_showSection2;
            if (_showSection2 && _editMode) {
                _showSection1 = false;
                _showSection3 = false;
                _showSection4 = false;
                _showSection5 = false;
                _showSection6 = false;
            }
        }
            break;
            
        case 3: {
            _showSection3=!_showSection3;
            if (_showSection3 && _editMode) {
                _showSection2 = false;
                _showSection1 = false;
                _showSection4 = false;
                _showSection5 = false;
                _showSection6 = false;
            }
        }
            break;
            
        case 4: {
            _showSection4=!_showSection4;
            if (_showSection4 && _editMode) {
                _showSection2 = false;
                _showSection3 = false;
                _showSection1 = false;
                _showSection5 = false;
                _showSection6 = false;
            }
        }
            break;
            
        case 5: {
            _showSection5=!_showSection5;
            if (_showSection5 && _editMode) {
                _showSection2 = false;
                _showSection3 = false;
                _showSection4 = false;
                _showSection1 = false;
                _showSection6 = false;
            }
        }
            break;
            
        case 6: {
            _showSection6=!_showSection6;
            if (_showSection6 && _editMode) {
                _showSection2 = false;
                _showSection3 = false;
                _showSection4 = false;
                _showSection5 = false;
                _showSection1 = false;
            }
        }
            break;
    }
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
   
    bool sections[7];
    sections[0]=_showSection0;
    sections[1]=_showSection1;
    sections[2]=_showSection2;
    sections[3]=_showSection3;
    sections[4]=_showSection4;
    sections[5]=_showSection5;
    sections[6]=_showSection6;
    
    if (sections[section])
    {
        [self scrollToSection:section];
    }
    
    [self drawArrowAtSection:section];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section)
    {
        case 0:
            return _editMode ? 118 : 66;
            break;
        case 1:
        {
            if (!_showSection1) {
                return 0;
            }
            NSDictionary *dict=[_childSectionCells objectAtIndex:indexPath.row];
            NSString *cellType=dict[@"cellType"];
            int height;
            if ([cellType isEqualToString:@"section1_content"])
            {
                height=682;
            }
            if ([cellType isEqualToString:@"section1_content2"])
            {
                float h = [NetworkManager sharedInstance].isJ3 ? 632.f : 689.f;
                
                height = h-(_showRoomMoveDate?28:0);
            }
            else if ([cellType isEqualToString:@"section1_moveRoomDate"])
            {
                height = 70;
            }
            else if ([cellType isEqualToString:@"section1_backDateProgress"])
            {
                height = 171+(_showProgressDates?0:13);
            }
            else if ([cellType isEqualToString:@"section1_progressDates"])
            {
                height = 150;
            }
            else if([cellType isEqualToString:@"profileSection"])
            {
                dict=[_profileSections objectAtIndex:indexPath.row-1];
                int type=[dict[@"type"]intValue];
                if (type>0)
                {
                    height =70;
                }
                else
                {
                    height = 130;
                }
            }
            NSLog(@"height for cell %@ is %d", cellType,height);
            return height;
        }
            break;
            
        case 2:
            switch (indexPath.row)
        {
            case 0:
                return _showSection2?992:0;
                break;
            case 1:
                return _showSection2&&!_parent2added?68:0;
                break;
            case 2:
                return _showSection2&&_parent2added?992:0;
            default:
                break;
        }
            break;
        case 3:
            switch (indexPath.row)
        {
            case 0:
                return (!_editMode)&&_showSection3?220:0;
            default:
                break;
        }
            break;
        case 4:
            switch (indexPath.row)
        {
            case 0: {
                
                float h = [NetworkManager sharedInstance].isJ3 ? 1762.f : 1465.f;
                
                return (!_editMode)&&_showSection4?h:0;
            }
                
            default:
                break;
        }
            break;
        case 5:
            switch (indexPath.row)
        {
            case 0: {
                float height = [NetworkManager sharedInstance].isJ3 ? 1032.f : 1458.f;
                
            return _showSection5?height:0; }
            default:
                break;
        }
            break;
        case 6:
            switch (indexPath.row)
        {
            case 0:
                return _showSection6?167:0;
            default:
                break;
        }
            break;
        case 7:
            return 68;
            break;
        default:
            break;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (section>0&&section<7)
    {
        if (_editMode)
        {
            if (section==3||section==4)
            {
                return 0;
            }
        }
        return 44;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section>0&&section<7)
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"section%d_spoiler",(int)section]];
        if (cell)
        {
            switch (section) {
                case 1:
                    _arrow1=(UIImageView*)[cell viewWithTag:55];
                    break;
                case 2:
                    _arrow2=(UIImageView*)[cell viewWithTag:55];
                    break;
                case 3:
                    _arrow3=(UIImageView*)[cell viewWithTag:55];
                    break;
                case 4:
                    _arrow4=(UIImageView*)[cell viewWithTag:55];
                    break;
                case 5:
                    _arrow5=(UIImageView*)[cell viewWithTag:55];
                    break;
                case 6:
                    _arrow6=(UIImageView*)[cell viewWithTag:55];
                    break;
                default:
                    break;
            }
            [cell viewWithTag:101].layer.borderWidth=0.5;
            [cell viewWithTag:101].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];
            cell.contentView.tag=section;
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headerTapped:)];
            [cell.contentView addGestureRecognizer:tap];
            [self drawArrowAtSection:(int)section];
            return cell.contentView;
        }
    }
    return nil;
}

-(void)checkSwitch:(CheckSwitch *)chSwitch statusChanged:(BOOL)status
{
    if (chSwitch.tag == 4777 ) {
        for (NSString *key in _checkSwitches.allKeys){
            CheckSwitch *sw=[_checkSwitches objectForKey:key];
            if (status && ((sw.tag<=4121 && sw.tag>=4105) || sw.tag==4150 || sw.tag==4152 || sw.tag==4154 || sw.tag==4156)) {
                [sw setOn:YES];
                [self.infoDict setValue:@"on" forKey:key];
            }else if (!status && ((sw.tag<=4121 && sw.tag>=4105) || sw.tag==4150 || sw.tag==4152 || sw.tag==4154 || sw.tag==4156)){
                [sw setOn:NO];
                [self.infoDict setValue:@"" forKey:key];
            }
        }
    }
    if (chSwitch.tag == 4888 ) {
        for (NSString *key in _checkSwitches.allKeys){
            CheckSwitch *sw=[_checkSwitches objectForKey:key];
            if (status && ((sw.tag<=4138 && sw.tag>=4122) || sw.tag==4151 || sw.tag==4153 || sw.tag==4155 || sw.tag==4157)) {
                [sw setOn:YES];
                [self.infoDict setValue:@"on" forKey:key];
            }else if (!status && ((sw.tag<=4138 && sw.tag>=4122) || sw.tag==4151 || sw.tag==4153 || sw.tag==4155 || sw.tag==4157)){
                [sw setOn:NO];
                [self.infoDict setValue:@"" forKey:key];
            }
        }
    }
    

    if (chSwitch.tag == 6666 ) {

        [self.infoDict setValue:(status?@"true":@"false") forKey:@"displ_alert"];
        [self.infoDict setValue:(status?@"1":@"") forKey:@"parent_show_alert"];

    }
    
    for (NSString *key in _checkSwitches.allKeys)
    {
        CheckSwitch *sw=[_checkSwitches objectForKey:key];
        if (chSwitch==sw)
        {
            if (chSwitch==_childBackDateProgressCheckSwitch)
            {
                [self.infoDict setValue:(status?@"0":@"1") forKey:key];
                self.showProgressDates=status;
            }
            else
            {
                [self.infoDict setValue:(status?@"on":@"") forKey:key];
            }
            NSLog(@"%@ switched to %d",key,status);
        }
    }
}


#pragma mark - PECropViewControllerDelegate methods

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    [self processForCroppedImage:croppedImage];
    
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller image:(UIImage *)image
{
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    [self processForCroppedImage:image];
}


- (void)processForCroppedImage:(UIImage *)image {
    
    //self.adminImage = [[Utils sharedInstance]imageScaledToAvatar:self.adminImage]
    //NSLog(@"orientation:%ld",_adminImage.imageOrientation);
    
    //[[Utils sharedInstance]base64StringFromImage:_adminImage];
    
    self.image = image;
    
    [self.addPhotoButton setBackgroundImage:image forState:UIControlStateNormal];
    
}


#pragma mark - IBActions

-(BOOL)allFieldsFilled
{
    NSLog(@"profile_sections:%@",_profileSections);
    NSMutableArray *tFields = [NSMutableArray arrayWithArray:@[_childFirstNameTextField,_childLastNameTextField,_childBoyOrGirlTextField, _childDateOfBirthTextField,_childRoomTextField,_parent1FirstNameTextField, _parent1LastNameTextField]];
    NSMutableArray *tfNames = [NSMutableArray arrayWithArray:@[
                                                               NSLocalizedString(@"as.child.addEdit.checkFields.chFName", nil),
                                                               NSLocalizedString(@"as.child.addEdit.checkFields.chLName", nil),
                                                               NSLocalizedString(@"as.child.addEdit.checkFields.boyOrGirl", nil),
                                                               NSLocalizedString(@"as.child.addEdit.checkFields.chDOB", nil),
                                                               NSLocalizedString(@"as.child.addEdit.checkFields.chRoom", nil),
                                                               NSLocalizedString(@"as.child.addEdit.checkFields.p1FName", nil),
                                                               NSLocalizedString(@"as.child.addEdit.checkFields.p1LName", nil)]];
    
    
    NSMutableArray *emptyFields=[NSMutableArray new];
    
    if (_childRoomMoveTextField.text.length>1 && _childMoveDateTextField.text.length<1) {
        [tFields addObject:_childMoveDateTextField];
        [tfNames addObject:NSLocalizedString(@"as.child.addEdit.checkFields.chRoomMoveDate", nil)];
    }
    
    if (_parent2added)
    {
        [tFields addObjectsFromArray:@[_parent2FirstNameTextField,_parent2LastNameTextField]];
        [tfNames addObjectsFromArray:@[NSLocalizedString(@"as.child.addEdit.checkFields.p2FName", nil),
                                       NSLocalizedString(@"as.child.addEdit.checkFields.p2LName", nil)]];
    }

    //custom fields from profile sections
    for (NSDictionary *dct in _profileSections)
    {
        UITextField *tf=dct[@"textfield"];
        if ([dct[@"type"]intValue]==2)
        {
            [tFields addObject:tf];
            id titleSource = dct[@"title"];
            NSString *title;
            if (titleSource) {
                if ([titleSource isKindOfClass:[NSString class]] || [titleSource length]) {
                    title = titleSource;
                }
                else if ([titleSource isKindOfClass:[NSNumber class]]) {
                    title = [titleSource stringValue];
                }
            }
            if (!title) {
                title = dct[@"helperTitle"];
            }
            
            //NSLog(title);
            //safe
            //[tfNames addObject:dct[@"title"]];
            [tfNames addObject:title];
        }
    }
    
    //check
    for (int i=0; i<tFields.count; i++)
    {
        UITextField *tf=[tFields objectAtIndex:i];
        if (tf.text.length==0)
        {
            [emptyFields addObject:tfNames[i]];
        }
    }
    
    if (emptyFields.count)
    {
        int index=(int)[tfNames indexOfObject:emptyFields[0]];
        UITextField *tf=[tFields objectAtIndex:index];
        CGPoint p=[self.tableView convertPoint:tf.frame.origin fromView:tf.superview];
        [self.tableView setContentOffset:CGPointMake(0, p.y-100)];
        NSString *alert=[NSString stringWithFormat:NSLocalizedString(@"as.child.addEdit.alert.notAllFields", nil),[emptyFields componentsJoinedByString:@", "]];
        ErrorAlert(alert);
        return NO;
    }
    return YES;
}

-(IBAction)findAddressForChild:(id)sender
{
    //not working for now
}

-(IBAction)findAddressForParent1:(id)sender
{
            //not working for now
}

-(IBAction)copyChildsAddressForParent1:(id)sender
{
    _parent1HouseStreetTextField.text=_childHouseStreetTextField.text;
    _parent1AreaTextField.text=_childAreaTextField.text;
    _parent1TownTextField.text=_childTownTextField.text;
    _parent1CountryTextField.text=_childCountryTextField.text;
    _parent1PostCodeTextField.text=_childPostCodeTextField.text;

    [self textFieldDidEndEditing:_parent1HouseStreetTextField];
    [self textFieldDidEndEditing:_parent1AreaTextField];
    [self textFieldDidEndEditing:_parent1TownTextField];
    [self textFieldDidEndEditing:_parent1CountryTextField];
    [self textFieldDidEndEditing:_parent1PostCodeTextField];

}

-(IBAction)addParentOrCarer:(id)sender
{
    _parent2added=true;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

-(IBAction)findAddressForParent2:(id)sender
{
        //not working for now
}

-(IBAction)copyChildsAddressForParent2:(id)sender
{
    _parent2HouseStreetTextField.text=_childHouseStreetTextField.text;
    _parent2AreaTextField.text=_childAreaTextField.text;
    _parent2TownTextField.text=_childTownTextField.text;
    _parent2CountryTextField.text=_childCountryTextField.text;
    _parent2PostCodeTextField.text=_childPostCodeTextField.text;

    [self textFieldDidEndEditing:_parent2HouseStreetTextField];
    [self textFieldDidEndEditing:_parent2AreaTextField];
    [self textFieldDidEndEditing:_parent2TownTextField];
    [self textFieldDidEndEditing:_parent2CountryTextField];
    [self textFieldDidEndEditing:_parent2PostCodeTextField];

}

-(IBAction)findAddressForEmergencyContact:(id)sender
{
        //not working for now
}

-(IBAction)uploadPhoto:(id)sender
{
    UIActionSheet *sheet=[[UIActionSheet alloc]initWithTitle:NSLocalizedString(@"Please select photo source", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take photo from camera", nil),NSLocalizedString(@"Choose photo from library", nil), nil];
    [sheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"actionSheet %d",(int)buttonIndex);
    
    if (buttonIndex==0)//camera
    {
        UIImagePickerController *picker=[[UIImagePickerController alloc]init];
        picker.sourceType=UIImagePickerControllerSourceTypeCamera;
        picker.delegate=self;
        [self.navigationController presentViewController:picker animated:YES completion:nil];
    }
    else if(buttonIndex==1)//library
    {
        UIImagePickerController *picker=[[UIImagePickerController alloc]init];
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate=self;
        [self.navigationController presentViewController:picker animated:YES completion:nil];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"info:%@",info);
    //self.image = [info valueForKey:UIImagePickerControllerOriginalImage];
    //self.image=[[Utils sharedInstance]imageScaledToRegularVisitorImage:self.image];
    //[[Utils sharedInstance]base64StringFromImage:self.image];
    
    //[self.addPhotoButton setBackgroundImage:self.image forState:UIControlStateNormal];
    //[picker dismissViewControllerAnimated:YES completion:nil];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        self.isImageChanged = YES;
        [self openEditorForImage:[info valueForKey:UIImagePickerControllerOriginalImage]];
    }];
}


- (void)openEditorForImage:(UIImage *)image {
    
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = image;
    
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat length = MIN(width, height);
    controller.keepingCropAspectRatio = YES;
    controller.toolbarHidden = YES;
    /*
     controller.imageCropRect = CGRectMake((width - length) / 2,
     (height - length) / 2,
     length,
     length);
     */
    controller.imageCropRect = CGRectMake((width - length*0.66) / 2,
                                          (height - length) / 2,
                                          length*0.66,
                                          length);
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    [self presentViewController:navigationController animated:YES completion:NULL];
    
    //[navigationController presentViewController:controller animated:YES completion:NULL];
}


-(IBAction)saveButtonPressed:(id)sender
{
    CGPoint p =[self.tableView convertPoint:_childFirstNameTextField.frame.origin fromView:_childFirstNameTextField.superview];
    NSLog(@"point:%f %f",p.x, p.y);
    if (![self allFieldsFilled])
    {
        return;
    }
    /*
    if (_editMode) 
    {
        [[NetworkManager sharedInstance] updateChildWithId:[_infoDict[@"id"] intValue] options:[self getChildParameters] completion:^(id response, NSError *error) {
            if (response)
            {
                NSLog(@"update child success:%@",response);
                [self.navigationController popViewControllerAnimated:YES];
            }
            else if(error)
            {
                NSLog(@"update child fail:%@",error.localizedDescription);
                ErrorAlert(error.localizedDescription);
            }
        }];
    }else{*/
    
        [[NetworkManager sharedInstance]createChildWithOptions:[self getChildParameters] completion:^(id response, NSError *error)
         {
             if (response)
             {
                 NSLog(@"create/update child success:%@",response);
                 if (_image&&_infoDict[@"image"])
                 {
                     [[ImageCache sharedInstance]deleteImageForLink:_infoDict[@"image"]];
                 }

                 if ([response[@"result"][@"complete"] integerValue] == 0) {
                     ErrorAlert(response[@"result"][@"msg"]);
                 } else {
                     [AppDelegate showCompletedHUDWithCompletionBlock:^{
                         [self.navigationController popViewControllerAnimated:YES];
                     }];
                 }
             }
             else if(error)
             {
                 NSLog(@"create/update child fail:%@",error.localizedDescription);
                 ErrorAlert(error.localizedDescription);
             }
         }];
   // }
}

#pragma mark - HideKeyboard

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

@end
