#import <UIKit/UIKit.h>

@interface UIImage(Utils)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
