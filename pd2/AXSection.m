//
//  AXSection.m
//  pd2
//
//  Created by i11 on 29/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AXSection.h"
#import "AXSections.h"
#import "AXRow.h"

NSString * const AXSectionFirst = @"AXSectionFirst";
NSString * const AXSectionLast = @"AXSectionLast";
NSString * const AXSectionAll = @"AXSectionAll";

@implementation AXSection

- (instancetype)initWithType:(NSString *)type;
{
    self = [super init];
    if (self) {
        
        self.type = type;
        self.tags = [[NSMutableSet alloc] init];
        self.rows = [[NSMutableArray alloc] init];
        self.userInfo = [[NSMutableDictionary alloc] init];
    }
    return self;
}


#pragma mark - Getters & setters

- (NSInteger)sectionIndex {
    return [self.delegate indexForSection:self];
}


#pragma mark - AXRowDelegate 

- (NSIndexPath *)indexPathForRow:(AXRow *)row {
    
    NSInteger sectionIndex = [self.delegate indexForSection:self];
    NSInteger rowIndex = [self.rows indexOfObject:row];

    return [NSIndexPath indexPathForRow:rowIndex inSection:sectionIndex];
}

#pragma mark -

- (BOOL)addRow:(AXRow *)row {
    
    if (![self.rows containsObject:row]) {
        row.delegate = self;
        [self.rows addObject:row];
        return YES;
    }
    else {
        NSLog(@"ERROR row dublicate");
        //в таком то type, по такому то index добавляет дублирующий row по такому то индексу
        return NO;
    }
}

- (void)addRows:(NSArray *)rows {
    
    for (AXRow *row in rows) {
        [self addRow:row];
    }
}

- (BOOL)addRowWithType:(NSString *)type andHeight:(CGFloat)height {
    
    AXRow *row = [[AXRow alloc] init];
    row.type = type;
    row.height = height;
    
    return ([self addRow:row]);
}


- (void)addHeader:(AXHeader *)header {
    self.header = header;
}


- (CGFloat)heightForRowAtIndex:(NSUInteger)index {
    AXRow *row = [self.rows objectAtIndex:index];
    return row.height;
}


#pragma mark - hide/unhide multi sections

- (NSIndexSet *)hideSectionsWithTags:(NSSet *)tags {

    self.isHide = YES;
    
    NSMutableIndexSet *indexSet = [[self.delegate getIndexSetForSectionsWithTags:tags] mutableCopy];
    
    if ([indexSet containsIndex:self.sectionIndex]) {
        [indexSet removeIndex:self.sectionIndex]; //удаляем себя
    }
    
    if (![indexSet count]) {
        return [NSIndexSet indexSet];
    }
    
    self.storedSections = [self.delegate sectionsAtIndexSet:indexSet];
    [self.delegate removeSectionsByIndexSet:indexSet];
    
    
    return indexSet; //sections к delete, reload саму секцию

}

- (NSIndexSet *)showHiddenSections {
    
    self.isHide = NO;
    
    if (!self.storedSections || ![self.storedSections count]) {
        return [NSIndexSet indexSet];
    }
    
    /*
    NSRange range = NSMakeRange(self.sectionIndex + 1, [self.storedSections count]);
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:range];
    */

    
    NSIndexSet *indexSet = [self.delegate insertSections:self.storedSections afterSectionIndex:self.sectionIndex];
    self.storedSections = [NSArray array];
    
    return indexSet; //sections к insert, reload саму секцию
    
}


@end
