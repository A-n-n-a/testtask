//
//  ChildFilterChildCell.h
//  pd2
//
//  Created by Andrey on 10/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@class ChildFilterModel;

@interface ChildFilterChildCell : AXTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *firstNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateOfBirthLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;

@property (weak, nonatomic) IBOutlet UIImageView *picImageView;

- (void)updateWithModel:(ChildFilterModel *)model;
- (void)updateImageWithModel:(ChildFilterModel *)model;


@end
