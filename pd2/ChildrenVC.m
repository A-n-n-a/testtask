//
//  ChildrenVC.m
//  pd2
//
//  Created by Eugene Fozekosh on 02.07.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ChildrenVC.h"
#import "AXSections.h"
#import "AXHeader.h"
#import "ManageChildModel.h"
#import "ChildCell.h"
#import "UIView+superCell.h"
#import "ListPicker.h"
#import "InfoCell.h"
#import "EditChildViewController.h"
#import "PSTAlertController.h"
#import <CoreText/CoreText.h>
#import "ShowChildVC.h"
#import "ArchivedChildCell.h"

#define CONTAINER_VIEW_TAG 250
#define HEADER_ARROW_IMAGE_VIEW_TAG 105
#define DELETE_ALERT_TAG 10
#define ACTIVITY_INDICATOR_VIEW_TAG 260
#define BOTTOM_LINE_VIEW_TAG 50505


@interface ChildrenVC ()
{
    NSString *PDInfoCell;
    NSString *PDInfo2Cell;
    NSString *PDInfo3Cell;
    
    NSString *PDSpoilerCell;
    NSString *PDSpoiler2Cell;
    NSString *PDSpoiler3Cell;
 
    NSString *PDPickerCell;
    NSString *PDChildCell;
    NSString *PDLoadingCell;
    NSString *PDButtonCell;
    NSString *PDSpacerCell;
    
    NSString *PDTopSection;
    NSString *PDActiveChildrenSection;
    NSString *PDArchivedChildrenSection;
    NSString *PDButtonSection;
    NSString *PDSpacerSection;
}

@property (strong, nonatomic) AXSections *sections;
@property (strong, nonatomic) NSIndexPath *confirmationDeletingIndexPath;
@property (strong, nonatomic) NSMutableArray *activeChildren;
@property (strong, nonatomic) NSMutableArray *archivedChildren;
@property (strong, nonatomic) InfoCell *autolayoutInfoCell;

@property (strong, nonatomic) NSMutableDictionary *sortedActiveDict;
@property (strong, nonatomic) NSMutableDictionary *sortedArchiveDict;

@property (assign, nonatomic) BOOL canDelete;


@end


@implementation ChildrenVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self getAdminAccessLevel];
    self.topTitle = @"Existing Children";
    [self initializeCellTypes];
    [self configureSections];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(freeTapAction:)];
    [self.view addGestureRecognizer:tap];
}


-(void)getAdminAccessLevel
{
    NSString *roomLevel = [[DataManager sharedInstance] getAdminLevelTitleById:[[DataManager sharedInstance].adminInfo[@"acl"] intValue]];
    
    self.canDelete = [roomLevel isEqualToString:@"Room"] ? NO : YES;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self reloadSections];
    [self getAllChildren];
}


-(void) setupSearchByRoom
{
    [self reloadSections];
    [self sortChildren:self.archivedChildren byRoomInDictionary:self.sortedArchiveDict];
    [self sortChildren:self.activeChildren byRoomInDictionary:self.sortedActiveDict];
    [self setupChildrenSections];
}


- (void)freeTapAction:(id)sender
{
    [self.view endEditing:YES];
}


-(NSArray *) childrenNames
{
    NSMutableArray *names = [NSMutableArray new];
    NSMutableArray *children = [[NSMutableArray alloc] initWithArray:self.activeChildren];
    [children addObjectsFromArray:self.archivedChildren];
    
    for (ManageChildModel *child in children) {
        [names addObject:[NSString stringWithFormat:@"%@ %@", child.childFirstName, child.childLastName]];
    }
    return names;
}


- (void)initializeCellTypes
{
    PDInfoCell = @"PDInfoCell";
    PDInfo2Cell = @"PDInfo2Cell";
    PDInfo3Cell = @"PDInfo3Cell";
    
    PDSpoilerCell = @"PDSpoilerCell";
    PDSpoiler2Cell = @"PDSpoiler2Cell";
    PDSpoiler3Cell = @"PDSpoiler3Cell";
    
    PDPickerCell = @"PDPickerCell";
    PDChildCell = @"PDChildCell";
    PDLoadingCell = @"PDLoadingCell";
    PDButtonCell = @"PDButtonCell";
    PDSpacerCell = @"PDSpacerCell";
    
    PDTopSection = @"PDTopSection";
    PDActiveChildrenSection = @"PDActiveChildrenSection";
    PDArchivedChildrenSection = @"PDArchivedChildrenSection";
    PDButtonSection = @"PDButtonSection";
    PDSpacerSection = @"PDSpacerSection";
}


-(void) reloadSections
{
    self.sortedActiveDict = [NSMutableDictionary new];
    self.sortedArchiveDict = [NSMutableDictionary new];

    [self configureSections];
    [self setupTopSection];
    [self setupButtonSection];
}


- (void)configureSections
{
    self.sections = [[AXSections alloc] init];
    
    [self.sections rememberType:PDInfoCell withHeight:52.f identifier:@"infoCell"];
    [self.sections rememberType:PDInfo2Cell withHeight:76.f identifier:@"info2Cell"];
    [self.sections rememberType:PDInfo3Cell withHeight:76.f identifier:@"info3Cell"];
    [self.sections rememberType:PDSpoilerCell withHeight:43.f identifier:@"spoilerCell"];
    [self.sections rememberType:PDSpoiler2Cell withHeight:43.f identifier:@"spoiler2Cell"];
    [self.sections rememberType:PDSpoiler3Cell withHeight:43.f identifier:@"spoiler3Cell"];
    [self.sections rememberType:PDChildCell withHeight:146.f identifier:@"childCell"];
    [self.sections rememberType:PDPickerCell withHeight:43.f identifier:@"pickerCell"];
    [self.sections rememberType:PDButtonCell withHeight:68.f identifier:@"buttonCell"];
    [self.sections rememberType:PDLoadingCell withEstimatedHeight:76.f identifier:@"loadingCell"];
    [self.sections rememberType:PDSpacerCell withEstimatedHeight:13.f identifier:@"spacerCell"];
}


- (void)setupTopSection
{
    AXSection *topSection = [[AXSection alloc] initWithType:PDTopSection];
    [topSection addRow:[self.sections genRowWithType:PDInfoCell]];
    [topSection addRow:[self.sections genRowWithType:PDButtonCell]];
    [self.sections addSection:topSection];
    
    [self.tableView reloadData];
}


-(AXSection *)sectionsForSpacerWithTag:(NSArray *)tags {
    
    AXSection *spacerSection = [[AXSection alloc] initWithType:PDSpacerSection];
    [spacerSection.tags addObjectsFromArray:tags];
    [spacerSection.tags addObject:@"spacer"];
    AXRow *spacerRow = [self.sections genRowWithType:PDSpacerCell];
    [spacerSection addRow:spacerRow];
    
    return spacerSection;
}


-(void)setupButtonSection
{
    AXSection *buttonSection = [[AXSection alloc] initWithType:PDButtonSection];
    [buttonSection addRow:[self.sections genRowWithType:PDButtonCell]];
    [self.sections addSection:buttonSection];
    
    [self.tableView reloadData];
}


-(void) setupChildrenSections
{
    BOOL isEmpty = YES;
    NSMutableArray *sections = [[NSMutableArray alloc] init];
    
    NSArray *activeChildrensSections = [self sectionsForChildrenTypeWithStatusName:PDActiveChildrenSection childrenDict:self.sortedActiveDict isHide:NO isEmpty:&isEmpty];
   
    [sections addObjectsFromArray:activeChildrensSections];

    if (self.sortedArchiveDict.allValues.count) {
        NSArray *archivedChildrensSections = [self sectionsForChildrenTypeWithStatusName:PDArchivedChildrenSection childrenDict:self.sortedArchiveDict isHide:YES isEmpty:&isEmpty];
        [sections addObjectsFromArray:archivedChildrensSections];
    }

    NSIndexSet *indexSet = [self.sections insertSections:sections afterSectionType:PDTopSection];
    NSArray *infoCellIndexPaths = [self.sections getIndexPathsForRowType:PDInfoCell];
    
    [self.tableView beginUpdates];
    
    if ([indexSet count] > 0) {
        [self.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }
    
    if ([infoCellIndexPaths count] > 0) {
        [self.tableView reloadRowsAtIndexPaths:infoCellIndexPaths withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.tableView endUpdates];
}


-(NSArray *)sectionsForChildrenTypeWithStatusName:(NSString *)statusName childrenDict:(NSMutableDictionary *)dict isHide:(BOOL)isHide isEmpty:(BOOL *)isEmpty
{
    NSMutableArray *sections = [[NSMutableArray alloc] init];
    
    //here goes two sections (status name can be PDActiveChildrenSection or PDArchivedChildrenSection)
    AXSection *section = [[AXSection alloc] initWithType:statusName];
    NSString *infoCell = [NSString new];
    infoCell = [statusName isEqualToString:PDActiveChildrenSection] ? PDInfo2Cell : PDInfo3Cell;
    [section addRow:[self.sections genRowWithType:infoCell]];
    if ([statusName isEqualToString:PDActiveChildrenSection]) {
        [section addRow:[self.sections genRowWithType:PDPickerCell]];
    }
    section.isHide = isHide;
    [section.tags addObject:statusName];
    [section.tags addObject:@"main"];
    [section.tags addObject:@"top"];
    
    NSString *headerType = [NSString new];
    headerType = [statusName isEqualToString:PDActiveChildrenSection] ? PDSpoilerCell : PDSpoiler2Cell;
    AXHeader *header = [self.sections genHeaderWithType:headerType];
    [header.userInfo setObject:statusName forKey:@"title"];
    [header.userInfo setObject:statusName forKey:@"tag"];
    header.object = dict;
    section.header = header;
    
    [sections addObject:section];
  
     if(dict) {
        if (!isHide) {
            //sections for rooms
            NSArray *childrensSections = [self sectionsForRooms:dict isHide:YES statusTag:statusName];
            [sections addObjectsFromArray:childrensSections];
            //spacer after PDArchivedChildrenSection
            [sections addObject:[self sectionsForSpacerWithTag:[NSArray arrayWithObjects:statusName, nil]]];
        }
        
        *isEmpty = NO;
        
    }
    return  (NSArray *)sections;
    
}


-(NSArray *)sectionsForRooms:(NSMutableDictionary *)dict isHide:(BOOL)isHide statusTag:(NSString *)statusTag
{
    NSMutableArray *sections = [[NSMutableArray alloc] init];

    NSArray *sortedByRoom = [dict.allKeys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
    id lastHeader = [sortedByRoom lastObject];
  
    for (int i = 0; i < sortedByRoom.count; i++) {
       
        NSString *room = [sortedByRoom objectAtIndex:i];
       

        if (![room isKindOfClass:[NSString class]]) {
            room = [NSString stringWithFormat:@"%@", room];
        }

        if (![room isEqualToString:@"key"]) {
            AXSection *childSection = [[AXSection alloc] initWithType:statusTag];
            childSection.isHide = statusTag == PDActiveChildrenSection ? NO : YES;
            [childSection.tags addObject:statusTag];
            [childSection.tags addObject:@"main"];
            [childSection.tags addObject:@"childrenTop"];
            [childSection.tags addObject:room];
            
            AXHeader *childHeader = [self.sections genHeaderWithType:PDSpoiler3Cell];
            childHeader.object = [dict objectForKey:room];
            [childHeader.userInfo setObject:statusTag forKey:@"statusTag"];
            [childHeader.userInfo setObject:room forKey:@"title"];
            [childHeader.userInfo setObject:statusTag forKey:@"tag"];
            [childHeader.userInfo setObject:[[dict objectForKey:room] valueForKey:@"childRoomID"] forKey:@"roomID"];

            //if (lastHeader == room && statusTag == PDActiveChildrenSection) {
            if (lastHeader == room && (statusTag == PDActiveChildrenSection || statusTag == PDArchivedChildrenSection)) {
                [childHeader.userInfo setObject:@"last" forKey:@"lastHeader"];
            }
            if ([[dict objectForKey:room] count]) {
                childSection.header = childHeader;
            }
            
            NSArray *sortedChildsByFirstName =[dict objectForKey:room];
            [self sortArrayByFirstName:sortedChildsByFirstName];
            
            ManageChildModel *lastChild = [sortedChildsByFirstName lastObject];
            
            for (ManageChildModel *child in sortedChildsByFirstName) {
                if(child) {
                    AXRow *childRow = [self.sections genRowWithType:PDChildCell];
                    childRow.object = child;
                    [childSection addRow:[self.sections genRowWithType:PDSpacerCell]];
                    [childSection addRow:childRow];
                    
                    if (self.archivedChildren.count > 0) {
                        //if (child == lastChild && !child.childArchived && statusTag != PDArchivedChildrenSection) {
                        if (child == lastChild) {
                            [childSection addRow:[self.sections genRowWithType:PDSpacerCell]];
                            [childSection addRow:[self.sections genRowWithType:PDSpacerCell]];
                            [childSection addRow:[self.sections genRowWithType:PDSpacerCell]];
                        }
                    } else {
                        if (child == lastChild && lastHeader != room) {
                            [childSection addRow:[self.sections genRowWithType:PDSpacerCell]];
                            [childSection addRow:[self.sections genRowWithType:PDSpacerCell]];
                            [childSection addRow:[self.sections genRowWithType:PDSpacerCell]];
                        }
                    }
                }
            }
            [sections addObject:childSection];
        }
    }
    return  (NSArray *)sections;
}


-(void) getAllChildren
{
    self.activeChildren = [[NSMutableArray alloc] init];
    self.archivedChildren = [[NSMutableArray alloc] init];
    self.sortedArchiveDict = [NSMutableDictionary new];
    self.sortedActiveDict = [NSMutableDictionary new];
    
    [self insertLoadingCell];
    [[NetworkManager sharedInstance] getChildrenListWithCompletion:^(id response, NSError *error){
         if (response){
             if ([[response valueForKey:@"result_active" ] isKindOfClass:[NSDictionary class]]){
                 NSArray *active = [response valueForKeyPath:@"result_active.child_list"];
                
                 if (![active isKindOfClass:[NSNull class]]) {
                     for (int i = 0; i < active.count; i++) {
                         ManageChildModel *child = [ManageChildModel new];
                         [child updateFromDictionary:active[i]];
                         child.childArchived = false;
                         [self.activeChildren addObject:child];
                     }
                 }else{
                     _activeChildren = [NSMutableArray new];
                 }
             }else{
                 _activeChildren = [NSMutableArray new];
             }
             [self updateTableWithResponse:self.activeChildren afterRowType:PDInfo2Cell];

             if ([[response valueForKey:@"result_archive"] isKindOfClass:[NSDictionary class]]){
                 NSArray *archived = [response valueForKeyPath:@"result_archive.child_list"];
                 if (![archived isKindOfClass:[NSNull class]]) {
                     for (int i = 0; i < archived.count; i++) {
                         ManageChildModel *child = [ManageChildModel new];
                         [child updateFromDictionary:archived[i]];
                         child.childArchived = true;
                         [self.archivedChildren addObject:child];
                     }
                 }else{
                     _archivedChildren = [NSMutableArray new];
                 }
             }else{
                 _archivedChildren = [NSMutableArray new];
             }
             [self updateTableWithResponse:self.archivedChildren afterRowType:PDInfo3Cell];

             [self setupSearchByRoom];
         }else if(error){
             ErrorAlert(error.localizedDescription);
         }
     }];
}


-(void) sortChildren:(NSMutableArray*)children byRoomInDictionary:(NSMutableDictionary*)dictionary
{
    NSMutableSet *roomNames = [NSMutableSet new];
    
    for (ManageChildModel *child in children) {
        [roomNames addObject:child.childRoomName];
    }
    for (NSString *room in roomNames) {
        NSMutableArray *roomChildren = [NSMutableArray new];
        for (ManageChildModel *child1 in children) {
            if ([child1.childRoomName isEqualToString:room]) {
                [roomChildren addObject:child1];
            }
        }
        [dictionary setObject:roomChildren forKey:room];
    }
}


-(void) configureSectionsForSearchByName:(NSString *)keyWord  withArrayOfKeyWords:(NSArray*)arrayOfNames
{
    NSArray *arrayOfChildren =  [self childrenByNameWithString:keyWord];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(childFirstName BEGINSWITH[cd] %@) OR (childLastName BEGINSWITH[cd] %@)", keyWord, keyWord];

    NSArray *results = [arrayOfChildren filteredArrayUsingPredicate:predicate];

    if ([keyWord isEqualToString:@""]) {
        results = arrayOfChildren;
    }

    NSMutableArray *active = [NSMutableArray new];
    NSMutableArray *archived = [NSMutableArray new];

    if (arrayOfChildren.count) {
        for (ManageChildModel *child in results) {
            child.childArchived ? [archived addObject:child] :  [active addObject:child];
        }
    }
    
    [self reloadSections];
    [self sortChildren:archived byRoomInDictionary:self.sortedArchiveDict];
    [self sortChildren:active byRoomInDictionary:self.sortedActiveDict];
    [self setupChildrenSections];
}


#pragma mark - TableView Header

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [self.sections heightForHeaderInSection:section];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    AXSection *sect = [self.sections sectionAtIndex:section];
    AXHeader *header = [self.sections headerAtSectionIndex:section];
   
    if (header) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:header.identifier];
        UIView *view = cell.contentView;
        //стрелка
        UIImageView *arrowImageView = (UIImageView *)[view viewWithTag:HEADER_ARROW_IMAGE_VIEW_TAG];
        [self updateHeaderArrowImage:arrowImageView withState:sect.isHide];
        
        UILabel *titleLabel = (UILabel *)[view viewWithTag:200];
        
        if ([header.type isEqualToString:PDSpoiler3Cell]) {

            // room title can be the number, that's why:
            if ([[header.userInfo objectForKey:@"title"] isKindOfClass:[NSString class]]) {
                titleLabel.text = [NSString stringWithFormat:@"%@", [header.userInfo objectForKey:@"title"]];
            }else{
                titleLabel.text = [NSString stringWithFormat:@"%@", [[header.userInfo objectForKey:@"title"] stringValue]]  ;
            }
            titleLabel.textColor = [[MyColorTheme sharedInstance]spoilerRoomTextColor];
            
            UIImageView *roomImageView = (UIImageView *)[view viewWithTag:66];

            NSDictionary *dict = [[DataManager sharedInstance] roomIDsWithImagePaths];
            NSString *roomImagePath  = [dict objectForKey:[[header.userInfo objectForKey:@"roomID"] lastObject]];
            
            NSString *imagePath=[NSString stringWithFormat:@"%@/%@",[NetworkManager sharedInstance].baseUrl, roomImagePath];
            [[Utils sharedInstance]downloadImageAtPath:imagePath completionHandler:^(UIImage *image) {
                roomImageView.image = image;
                roomImageView.layer.cornerRadius = 20;
                roomImageView.layer.masksToBounds = YES;
            }];
        }
        //бордеры
//        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        
//        [cell viewWithTag:101].layer.borderWidth=0.5;
//        [cell viewWithTag:101].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];

//        [self addBorder:PDBorderBottomDynamic toView:containerView];
        
        
        //Обязательное удаление gestureRecognizers, иначе при longPress crash
        while (cell.contentView.gestureRecognizers.count) {
            [cell.contentView removeGestureRecognizer:[cell.contentView.gestureRecognizers objectAtIndex:0]];
        }
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headerTap:)];
        [view addGestureRecognizer:tap];
        
        view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        header.view = view;
    }
    return header.view;
}


#pragma mark - Handle Header Tap

- (void)headerTap:(UIGestureRecognizer *)gestureRecognizer
{
    UIView *view = gestureRecognizer.view;
    
    NSUInteger index = [self.sections getSectionIndexForView:view];
    
    if (index == NSUIntegerMax) {
        return;
    }
    
    NSIndexSet *reloadIndexSet = [NSIndexSet indexSetWithIndex:index];
    
    AXHeader *header = [self.sections headerAtSectionIndex:index];
    
    AXSection *section = [self.sections sectionAtIndex:index];
    section.isHide = !section.isHide;
    [self updateHeaderArrowImage:header.arrowImageView withState:section.isHide];
    
    NSMutableIndexSet *removeIndexSet = [[NSMutableIndexSet alloc] init];
    NSIndexSet *insertIndexSet = [[NSIndexSet alloc] init];
    
    NSString *tag = [header.userInfo objectForKey:@"tag"];
    
    if ([header.type isEqualToString:PDSpoilerCell] || [header.type isEqualToString:PDSpoiler2Cell]) {
        if (section.isHide) {
            
            [removeIndexSet addIndexes:[self.sections getIndexSetForSectionsWithTag:tag andExcludeTag:@"top"]];
            if (tag != PDArchivedChildrenSection) {
                [removeIndexSet addIndexes:[self.sections getIndexSetForSectionsWithTag:@"spacer"]];
            }
            [self.sections removeSectionsByIndexSet:removeIndexSet];

        } else {
            NSMutableArray *sections = [[NSMutableArray alloc] init];
            
            NSArray *childrensSections = [self sectionsForRooms:header.object isHide:YES statusTag:tag];
            AXSection *spacerSection = [self sectionsForSpacerWithTag:[NSArray arrayWithObject:@"spacer"]];
            [sections addObjectsFromArray:[NSArray arrayWithArray:childrensSections]];
            [sections addObject:spacerSection];
            insertIndexSet = [self.sections insertSections:sections afterSectionIndex:section.sectionIndex];
        }
    }else if ([header.type isEqualToString:PDSpoiler3Cell]){
        //here we need detect last header (subsection); after this header we should set spacer
        NSString *last = [header.userInfo objectForKey:@"lastHeader"];
        if ([last isEqualToString:@"last"]) {
            if (!section.isHide) {
                [removeIndexSet addIndexes:[self.sections getIndexSetForSectionsWithTag:@"spacer" andExcludeTag:@"top"]];
                [self.sections removeSectionsByIndexSet:removeIndexSet];
            }else{
                AXSection *spacerSection = [self sectionsForSpacerWithTag:[NSArray arrayWithObject:@"spacer"]];
                insertIndexSet = [self.sections insertSections:[NSArray arrayWithObject:spacerSection] afterSectionIndex:section.sectionIndex];
            }
        }
    }
    
    [self.tableView beginUpdates];
    
    //сама кликнутая секция
    [self.tableView reloadSections:reloadIndexSet withRowAnimation:UITableViewRowAnimationNone];
    
    if ([insertIndexSet count] > 0) {
        [self.tableView insertSections:insertIndexSet withRowAnimation:UITableViewRowAnimationNone];
    }
    
    if ([removeIndexSet count] > 0) {
        [self.tableView deleteSections:removeIndexSet withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.tableView endUpdates];
    
}


- (void)updateHeaderArrowImage:(UIImageView *)imageView withState:(BOOL)state
{
    imageView.image = [[UIImage imageNamed:(state ? @"arrowDown" : @"arrowUp")] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}


#pragma mark - UITableView stuff

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sections countOfSections];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sections countOfRowsInSection:section];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    return row.height;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    NSString *rowType = row.type;
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    if (rowType == PDInfoCell || rowType == PDInfo2Cell || rowType == PDInfo3Cell) {
        cell = [self configureInfoCellForTableView:tableView indexPath:indexPath cellID:row.identifier];
    }else if ([rowType isEqualToString:PDPickerCell]) {
        cell = [self configurePickerCellForTableView:tableView indexPath:indexPath];
    }else if ([rowType isEqualToString:PDChildCell]) {
        cell = [self configureChildCellForTableView:tableView indexPath:indexPath];
    }else if ([rowType isEqualToString:PDLoadingCell]) {
        cell = [self configureDefaultCellForTableView:tableView indexPath:indexPath withBorder:PDBorderTopAndBottom];
    }else if ([rowType isEqualToString:PDButtonCell]) {
        cell = [self configureButtonCellForTableView:tableView indexPath:indexPath];
    }else if ([rowType isEqualToString:PDSpacerCell]) {
        cell = [self configureSpacerCellForTableView:tableView indexPath:indexPath];
    }
    row.cell = cell;
    
    return cell;
}


- (UITableViewCell*) configureButtonCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"buttonCell"];
    
    return cell;
}

- (UITableViewCell*) configureSpacerCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"spacerCell"];
    
    return cell;
}

- (UITableViewCell *)configureDefaultCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath withBorder:(PDBorderType)borderType
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadingCell"];
    if (cell) {
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        if (borderType != PDBorderNone) {
            [self addBorder:borderType toView:containerView forToolbar:NO];
        }
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    }
    
    return cell;
}


- (UITableViewCell *)configureInfoCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath cellID:(NSString*)cellID
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    return cell;
}


- (UITableViewCell *)configurePickerCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    //picker, because at the begining it was real picker with a lot of sorting types
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pickerCell"];
    return cell;
}



- (UITableViewCell*) configureChildCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    ManageChildModel *child = row.object;
    ChildCell *cell = [ChildCell new];
    
    if (child.childArchived) {
       ArchivedChildCell  *cell1= (ArchivedChildCell*)[tableView dequeueReusableCellWithIdentifier:@"archivedChildCell"];
        cell = cell1;
    }else{
         cell = (ChildCell*)[tableView dequeueReusableCellWithIdentifier:@"childCell"];
    }
    
    if (cell) {
        cell.archived = child.childArchived;
        [cell initWithModel:child];
        UIView *containerView = [cell.contentView viewWithTag:CONTAINER_VIEW_TAG];
        [self addBorder:PDBorderTopAndBottomDynamic toView:containerView forToolbar:NO];
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        if (!cell.archived) {
            cell.deleteButton.enabled = self.canDelete;
        }
    }
    
    return cell;
}


- (void)updateTableWithResponse:(NSArray*)response afterRowType:(NSString*)rowType
{
    NSMutableArray *rows = [[NSMutableArray alloc] init];
    for (ManageChildModel *child in response) {
        AXRow *row = [self.sections genRowWithType:PDChildCell];
        row.object = child;
        if (child && ![child isEqual:[NSNull null]]) {
            [rows addObject:row];
        }
    }
    [self deleteLoadingCell];
    if ([rows count] != 0) {
        [self insertRows:rows afterRowType:rowType andWithRowAnimation:UITableViewRowAnimationNone];
    }
}


- (void)insertRows:(id)rows afterRowType:(NSString *)rowType andWithRowAnimation:(UITableViewRowAnimation)rowAnimation
{
    NSArray *addRows = [[NSArray alloc] init];
    if ([rows isKindOfClass:[AXRow class]]) {
        addRows = [NSArray arrayWithObject:rows];
    } else if ([rows isKindOfClass:[NSArray class]]) {
        addRows = rows;
    }
    
    NSArray *addedIndexPaths = [self.sections insertRows:addRows afterRowType:rowType atIndex:AXRowFirst];
    if ([addedIndexPaths count] > 0) {
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:addedIndexPaths  withRowAnimation:rowAnimation];
        [self.tableView endUpdates];
    }
    [self.tableView reloadData];
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *keyWord = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.sortedActiveDict = [NSMutableDictionary new];
    self.sortedArchiveDict = [NSMutableDictionary new];
    [self configureSectionsForSearchByName:keyWord withArrayOfKeyWords:[self childrenNames]];
    [textField becomeFirstResponder];
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

//gets array of child models according to the names
-(NSArray *) childrenByNameWithString:(NSString *)string
{
    NSMutableArray *children = [[NSMutableArray alloc] initWithArray:self.activeChildren];
    [children addObjectsFromArray:self.archivedChildren];

    NSMutableArray *finalChildrenArray = [NSMutableArray new];
    for (ManageChildModel *child in children) {
        if ([child nameContain:string]) {
            [finalChildrenArray addObject:child];
        }
    }
    //sorting A-Z
    [self sortArray:finalChildrenArray];

    //first name sorting firstly
    [self sortArrayByFirstName:finalChildrenArray];

    return finalChildrenArray;
}


- (void)sortArray:(id)array
{
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"childFirstName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"childLastName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    if([array isKindOfClass:[NSMutableArray class]]){
        NSMutableArray *a = (NSMutableArray*)array;
        [a sortUsingDescriptors:@[sortDescriptor1, sortDescriptor2]];
    }else if([array isKindOfClass:[NSArray class]]){
        NSArray *a = (NSArray*)array;
        a = [a sortedArrayUsingDescriptors:@[sortDescriptor1, sortDescriptor2]];
    }
}


- (void)sortArrayByFirstName:(id)array
{
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"childFirstName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    if([array isKindOfClass:[NSMutableArray class]]){
        NSMutableArray *a = (NSMutableArray*)array;
        [a sortUsingDescriptors:@[sortDescriptor1]];
    }else if([array isKindOfClass:[NSArray class]]){
        NSArray *a = (NSArray*)array;
        a = [a sortedArrayUsingDescriptors:@[sortDescriptor1]];
    }
}


#pragma mark - Loading Cell

- (void)insertLoadingCell
{
    AXRow *row = [self.sections genRowWithType:PDLoadingCell];
    [self insertRows:row afterRowType:PDSpoilerCell andWithRowAnimation:UITableViewRowAnimationNone];
}


- (void)deleteLoadingCell
{
    NSArray *deletingIndexPaths = [self.sections getIndexPathsForRowType:PDLoadingCell];
    [self.sections removeArrayRowsByIndexPaths:deletingIndexPaths];
    if ([deletingIndexPaths count] > 0) {
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:deletingIndexPaths withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
}


- (void)deleteRowsAtIndexPath:(NSArray *)deletingIndexPaths
{
    [self.sections removeArrayRowsByIndexPaths:[NSArray arrayWithArray:deletingIndexPaths]];
    [self.tableView beginUpdates];
    
    [self.tableView deleteRowsAtIndexPaths:deletingIndexPaths withRowAnimation:UITableViewRowAnimationRight];
    [self.tableView endUpdates];
}



- (IBAction)showChildButtonTapped:(id)sender
{
    UITableViewCell *cell = [sender superCell];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    ManageChildModel *child = row.object;
    
    [[NetworkManager sharedInstance] getChildInfo:[child.childID intValue] completion:^(id response, NSError *error) {
        if (response){
            
            [self performSegueWithIdentifier:@"showChild" sender:response];
        }
    }];

}


- (IBAction)copyChildButtonTapped:(id)sender
{
    UITableViewCell *cell = [sender superCell];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    ManageChildModel *child = row.object;
    
    [[NetworkManager sharedInstance] getChildrenStatisticsWithCompletion:^(id response, NSError *error) {
        if (response) {
            if ([response[@"data"][@"available"] integerValue] > 0) {
                
                [[NetworkManager sharedInstance] getChildInfo:[child.childID intValue] completion:^(id response, NSError *error) {
                    if (response){
                        
                        NSMutableDictionary *childDictionary = [NSMutableDictionary dictionaryWithDictionary:[response mutableCopy]];
                        NSMutableDictionary *childPart = [NSMutableDictionary dictionaryWithDictionary:[response objectForKey:@"child"]];
                        
                        [childPart setObject:@"" forKey:@"birth"];
                        [childPart setObject:@"" forKey:@"first_name"];
                        [childPart setObject:@"" forKey:@"gender"];
                        [childPart setObject:@"" forKey:@"id"];
                        
                        [childDictionary setObject:childPart forKey:@"child"];
                        [childDictionary setObject:childDictionary[@"profile"] forKey:@"profile"];
                        [self performSegueWithIdentifier:@"copyChildSegue" sender:childDictionary];
                    }
                }];
            } else {
                NSString *limitCount = [NSString stringWithFormat:@"You cannot add any more children as your system is limited to %@ children.\nYou will need to upgrade your subscription plan to add additional children to your system.", response[@"data"][@"limit"] ];
                ErrorAlert(limitCount);
            }
        } else {
            ErrorAlert(error.localizedDescription);
        }
    }];
}


- (IBAction)editChildTapped:(id)sender
{
    UITableViewCell *cell = [sender superCell];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    ManageChildModel *child = row.object;
    
    [[NetworkManager sharedInstance] getChildInfo:[child.childID intValue] completion:^(id response, NSError *error) {
        if (response){
            NSDictionary *child = response;//[@"child"];
            NSMutableDictionary *dict = [NSMutableDictionary new];
            [dict setObject:child[@"profile"] forKey:@"profile"];
            [dict setObject:child forKey:@"child"];
            
            [self performSegueWithIdentifier:@"editChildSegue" sender:dict];
        }
    }];
}


- (IBAction)folderButtonTapped:(id)sender
{
    UITableViewCell *cell = [sender superCell];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    ManageChildModel *child = row.object;
    if (!child.childArchived) {
        PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning" message:@"Are you sure you want to archive this child?" preferredStyle:PSTAlertControllerStyleAlert];
        
        [alert addAction:[PSTAlertAction actionWithTitle:@"Ok" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
            
            [[NetworkManager sharedInstance] archiveChildWithId:child.childID.intValue completion:^(id response, NSError *error) {
                if (response){
                    child.childArchived = YES;
                    
                    //child transfer from active to archive
                    [self.activeChildren removeObject:child];
                    [self.archivedChildren addObject:child];
                    
                    //child transfer from sortedActive dict to sortedArchive dict
                    
                    [[self.sortedActiveDict objectForKey:child.childRoomName] removeObject:child];
                    [[self.sortedArchiveDict objectForKey:child.childRoomName] addObject:child];
                    
                    //if in sortedArchive dictionary we haven't room with childRoomName - we create it
                    if (![self.sortedArchiveDict objectForKey:child.childRoomName]) {
                        [self.sortedArchiveDict setObject:[NSMutableArray arrayWithObject:child] forKey:child.childRoomName];
                    }
                    if (![[self.sortedActiveDict objectForKey:child.childRoomName] count]) {
                        [self.sortedActiveDict removeObjectForKey:child.childRoomName];
                    }
                    [self configureSections];
                    [self setupTopSection];
                    [self setupButtonSection];
                    [self setupChildrenSections];

                } else if(error){
                    ErrorAlert(error.localizedDescription);
                }
            }];
        }]];
        
        [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:^(PSTAlertAction *action) {
        }]];
        
        [alert showWithSender:self controller:self animated:YES
                   completion:nil];

    }else{
        
        
        PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning" message:@"Are you sure you want to unarchive this child?" preferredStyle:PSTAlertControllerStyleAlert];
        
        [alert addAction:[PSTAlertAction actionWithTitle:@"Ok" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {

        
        
                [[NetworkManager sharedInstance] unarchiveChildWithId:child.childID.intValue completion:^(id response, NSError *error) {
                    if (response){
                        child.childArchived = NO;
                        [self.activeChildren addObject:child];
                        [self.archivedChildren removeObject:child];
                        [[self.sortedArchiveDict objectForKey:child.childRoomName] removeObject:child];

                        if (![self.sortedActiveDict objectForKey:child.childRoomName]) {
                            [self.sortedActiveDict setObject:[NSMutableArray arrayWithObject:child] forKey:child.childRoomName];
                        }else{
                            [[self.sortedActiveDict objectForKey:child.childRoomName] addObject:child];
                        }
                        
                        if (![[self.sortedArchiveDict objectForKey:child.childRoomName] count]) {
                            [self.sortedArchiveDict removeObjectForKey:child.childRoomName];
                        }
                        
                        [self configureSections];
                        [self setupTopSection];
                        [self setupButtonSection];
                        [self setupChildrenSections];
                    } else if(error){
                        ErrorAlert(error.localizedDescription);
                    }
                }];
        }]];
        
        [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:^(PSTAlertAction *action) {
        }]];
        
        [alert showWithSender:self controller:self animated:YES
                   completion:nil];
    }
}


- (IBAction)deleteChildTapped:(id)sender
{
    UITableViewCell *cell = [sender superCell];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSLog(@"taped on %ld, %ld", (long)indexPath.section, (long)indexPath.row);
    
    self.confirmationDeletingIndexPath = indexPath;
    
    if (self.canDelete) {
        NSString *message = @"Are you sure you want to delete the child from the system, deleting the child from the system removes all their personal data including diaries and progress etc and can NOT be reversed.\n Click DELETE below to delete the child from the system or Cancel to return to the previous page.";
        
        
        UIAlertView *deleteActionAlert = [[UIAlertView alloc] initWithTitle:@"WARNING"
                                                                    message:message
                                                                   delegate:self
                                                          cancelButtonTitle:@"Cancel"
                                                          otherButtonTitles:@"Delete", nil];
        deleteActionAlert.tag = DELETE_ALERT_TAG;
        [deleteActionAlert show];
    }else{
        NSString *warningMessage = @"Access denied";
        
        
        UIAlertView *warningDeleteActionAlert = [[UIAlertView alloc] initWithTitle:@"ERROR"
                                                                           message:warningMessage
                                                                          delegate:self
                                                                 cancelButtonTitle:@"Ok"
                                                                 otherButtonTitles: nil];
        [warningDeleteActionAlert show];
    }
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == DELETE_ALERT_TAG) {
        
        if (buttonIndex == 0) { //cancel
            
        } else if (buttonIndex == 1) { //OK
            
            if (self.confirmationDeletingIndexPath) {
                [self deleteActionAtIndexPath:self.confirmationDeletingIndexPath];
            }
            
        }
        self.confirmationDeletingIndexPath = nil;
    }
}


- (void)deleteActionAtIndexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    row.isForRemoval = YES;
    
    UIView *view =[row.cell.contentView viewWithTag:CONTAINER_VIEW_TAG];
    [self addActivityIndicatorToView:view withAnimation:YES];
    
    [self remoteRemoveActionForIndexPath:indexPath];
}


- (void)remoteRemoveActionForIndexPath:(NSIndexPath *)indexPath
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    ManageChildModel *child = row.object;
    
    [[NetworkManager sharedInstance] deleteChildWithId:child.childID.intValue completion:^(id response, NSError *error){
         if (response) {
             UIView *view =[row.cell.contentView viewWithTag:CONTAINER_VIEW_TAG];
             [self removeActivityIndicatorFromView:view];
             
             NSIndexPath* spacerIndexPath = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];
             UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:spacerIndexPath];
             if (cell) {
                 [self deleteRowsAtIndexPath:@[indexPath, spacerIndexPath]];
             } else {
                 [self deleteRowsAtIndexPath:@[indexPath]];
             }

             [self.activeChildren removeObject:child];
             [[self.sortedActiveDict objectForKey:child.childRoomName] removeObject:child];
         } else if(error) {
             ErrorAlert(error.localizedDescription);
         }
     }];
}


- (void)addActivityIndicatorToView:(UIView *)destinationView withAnimation:(BOOL)animation{
    
    CGRect destFrame = destinationView.frame;
    destFrame.origin.x = 0;
    destFrame.origin.y = 0;
    UIView *view = [[UIView alloc] initWithFrame:destFrame];
    view.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5f];
    view.tag = ACTIVITY_INDICATOR_VIEW_TAG;
    
    [destinationView addSubview:view];
    
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc]
                                                      initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    CGRect frame = activityIndicatorView.frame;
    frame.origin.x = view.frame.size.width / 2 - frame.size.width / 2;
    frame.origin.y = view.frame.size.height / 2 - frame.size.height / 2;
    activityIndicatorView.frame = frame;
    
    [activityIndicatorView startAnimating];
    
    if (animation) {
        [UIView transitionWithView:view duration:1.5
                           options:UIViewAnimationOptionTransitionCrossDissolve //change to whatever animation you like
                        animations:^ { [view addSubview:activityIndicatorView]; }
                        completion:nil];
    } else {
        [view addSubview:activityIndicatorView];
    }
}


- (void)removeActivityIndicatorFromView:(UIView *)destinationView
{
    UIView *removeView;
    
    while((removeView = [destinationView viewWithTag:ACTIVITY_INDICATOR_VIEW_TAG]) != nil) {
        [removeView removeFromSuperview];
    }
}


- (IBAction)addNewChildTapped:(id)sender
{
    [[NetworkManager sharedInstance] getChildrenStatisticsWithCompletion:^(id response, NSError *error) {
        if (response) {
            if ([response[@"data"][@"available"] integerValue] > 0) {
                [[NetworkManager sharedInstance] getChildProfileListWithCompletion:^(id response, NSError *error)
                 {
                     [self performSegueWithIdentifier:@"createNewChildSegue" sender:response];
                 }];
            } else {
                NSString *limitCount = [NSString stringWithFormat:@"You cannot add any more children as your system is limited to %@ children.\nYou will need to upgrade your subscription plan to add additional children to your system.", response[@"data"][@"limit"] ];
                ErrorAlert(limitCount);
            }
        } else {
            ErrorAlert(error.localizedDescription);
        }
    }];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"editChildSegue"]){
        EditChildViewController *editChild = [segue destinationViewController];
        editChild.editMode = true;
        editChild.infoDict = sender[@"child"];
        editChild.profileSections = [[NSMutableArray alloc] initWithArray:sender[@"profile"]];

    }else if ([segue.identifier isEqualToString:@"copyChildSegue"]){
        EditChildViewController *editChild = [segue destinationViewController];
        editChild.infoDict = sender;
        editChild.profileSections = [[NSMutableArray alloc] initWithArray:sender[@"profile"]];
        editChild.isCopyMode = true;
    }else if([segue.identifier isEqualToString:@"showChild"]){
        ShowChildVC *shovVC = [segue destinationViewController];
        shovVC.childDict = sender;
        shovVC.profileSections = sender[@"profile"];
    }else if ([segue.identifier isEqualToString:@"createNewChildSegue"]){
        EditChildViewController *editChild = [segue destinationViewController];
        editChild.profileSections = [[NSMutableArray alloc] initWithArray:sender[@"list_child_profile"]];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
