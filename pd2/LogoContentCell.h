//
//  LogoContentCell.h
//  pd2
//
//  Created by Eugene Fozekosh on 09.06.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubtextButton.h"
#import "SubtextLabel.h"

@interface LogoContentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SubtextButton *logoButton;
@property (weak, nonatomic) IBOutlet SubtextLabel *bottomLabel;

@end
