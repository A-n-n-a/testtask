//
//  ListPicker2ComponentsDict.h
//  pd2
//
//  Created by i11 on 26/08/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ListPicker2ComponentsDict : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UIGestureRecognizerDelegate>

@property (copy) void (^handler)(NSString *string0Comp, NSString *string1Comp);
@property (copy) void (^finishHandler)();



@property NSString *selectedStringIn0Component;
@property NSString *selectedStringIn1Component;

@property (nonatomic,retain) NSDictionary *dataDict;


@property (nonatomic,retain)IBOutlet UIPickerView *picker;
@property (nonatomic,retain)IBOutlet UIBarButtonItem *doneButton, *cancelButton;

-(void)show;
-(IBAction)hide;




@end
