//
//  AdminCell.m
//  pd2
//
//  Created by Admin on 04.04.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "AdminCell.h"

@implementation AdminCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setInfoDict:(NSDictionary *)infoDict
{
    _infoDict=infoDict;
    _usernameLabel.text=infoDict[@"username"];
    _fNameLabel.text=[infoDict[@"first_name"] capitalizedString];
    _lNameLabel.text=[infoDict[@"last_name"] capitalizedString];
    NSNumber *accLev=infoDict[@"acl"];
    
    if (accLev.intValue == 1) {
        _adminLevelLabel.text = @"Master";
    }
    else if (accLev.intValue == 2) {
        _adminLevelLabel.text = @"Super";
    }
    else if (accLev.intValue == 3) {
        _adminLevelLabel.text = @"Room";
    }
    else if (accLev.intValue == 5) {
        _adminLevelLabel.text = @"Bookkeeping";
    }
    else {
        _adminLevelLabel.text = @"N/A";
    }
    

    
    if (accLev.intValue ==1 )
    {
        _deleteButton.enabled=NO;
        _deleteButtonImage.image=[UIImage imageNamed:@"trash-icon-pressed"];
    }
    else
    {
        _deleteButton.enabled=YES;
        _deleteButtonImage.image=[UIImage imageNamed:@"trash-icon"];
    }
    //NSNumber *positionNum=infoDict[@"position"];
    if (accLev.intValue==1||accLev.intValue==2)
    {
        _roomLabel.text=@"All rooms";
    }
    else
    {
        NSString *roomStr;
        if ([infoDict[@"title"]isKindOfClass:[NSString class]])
        {
            roomStr=infoDict[@"title"];
        }
        else
        {
            roomStr=@"None";
        }
        _roomLabel.text=roomStr;
    }
    int myAcl=[[DataManager sharedInstance]getAclForCurrentUser];
    if (myAcl>=accLev.intValue)
    {
        _deleteButton.enabled=NO;
        _deleteButtonImage.image=[UIImage imageNamed:@"trash-icon-pressed"];
    }
    
    NSString *imagePath=[NSString stringWithFormat:@"%@/%@",[NetworkManager sharedInstance].baseUrl,infoDict[@"image"]];
    
    [[Utils sharedInstance]downloadImageAtPath:imagePath completionHandler:^(UIImage *image) {
        _userImageView.image=image;
        _userImageView.layer.borderWidth=0.5;
        _userImageView.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        _userImageView.layer.cornerRadius=38;
        _userImageView.layer.masksToBounds=YES;
    }];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)toolButtonPressed:(id)sender
{
    if (sender==_renameButton)
    {
        _editButtonImage.image=[UIImage imageNamed:@"compose-icon-pressed"];
    }
    else
    {
        _deleteButtonImage.image=[UIImage imageNamed:@"trash-icon-pressed"];
    }
}

-(IBAction)toolButtonReleased:(id)sender
{
    if (sender==_renameButton)
    {
        _editButtonImage.image=[UIImage imageNamed:@"compose-icon"];
    }
    else
    {
        _deleteButtonImage.image=[UIImage imageNamed:@"trash-icon"];
    }

}




@end
