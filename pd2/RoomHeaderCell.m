//
//  RoomHeaderCell.m
//  pd2
//
//  Created by i11 on 25/10/15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import "RoomHeaderCell.h"

@implementation RoomHeaderCell

- (void)updateWithRoomName:(NSString *)roomName {
    
    self.roomNameLabel.text = notAvailStringIfNil(roomName);
    
    self.roomNameLabel.textColor = [[MyColorTheme sharedInstance]spoilerRoomTextColor];
}


- (void)updateWithRoomImagePath:(NSString *)roomImagePath {
    
    [self updateProfileImageView:self.roomImageView withImagePath:roomImagePath andDefaultImagePath:nil downloadImageCompletition:^(UIImage *downloadedImage) {
        
    }];
    
    [self decorateProfileView:self.roomImageView];
    
}

@end
