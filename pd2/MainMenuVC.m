//
//  MainMenuVC.m
//  pd2
//
//  Created by User on 09.07.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "MainMenuVC.h"
#import "UserManager.h"
#import "AccessTimeManager.h"
#import "PushNotificationManager.h"
#import "SplashViewController.h"



#import "PSTAlertController.h"
#import "AOLPlistBritish.h"
#import "UIImage+Tint.h"

#import "WL.h"

#define COLLECTION_BUTTON_TAG 100
#define COLLECTION_LABEL_TAG 200
#define QUICK_MENU_COLLECTION_VIEW_TAG 600


@interface MainMenuVC ()

@property NSMutableArray *buttonsArray;
@property NSMutableArray *quickButtonsArray;


@end

@implementation MainMenuVC
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/

#warning При изменении UI в MainMenuParentSideVC и MainMenuVC держать их синхронизированными
//TODO: обновление меню до side bar menu

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"main menu did load");
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:[UIView new]];
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 300)];
    titleLabel.text = [WL name];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont boldSystemFontOfSize:23];
    //self.navigationItem.titleView=titleLabel;
    
    
    /*
    NSLog(@"self.scrollview.contentSize:%f,%f",self.scrollView.contentSize.width,self.scrollView.contentSize.height);
    NSLog(@"self.scrollview.frame:%f,%f",self.scrollView.frame.size.width,self.scrollView.frame.size.height);
    NSLog(@"self.view.frame:%f,%f",self.view.frame.size.width,self.view.frame.size.height);
    NSLog(@"self.contentView.size:%f,%f",self.contentView.frame.size.width,self.contentView.frame.size.height);
    */
    /*
    self.pageControl.currentPage=0;
    
    UISwipeGestureRecognizer *swipe=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(scrollToPage2:)];
    swipe.direction=UISwipeGestureRecognizerDirectionLeft;
    swipe.numberOfTouchesRequired=1;
    
    swipe.delegate=self;
    */
    
    [[DataManager sharedInstance]refresh];
    //[self.view addGestureRecognizer:swipe];
    
    UIBarButtonItem *leftItem=[[UIBarButtonItem alloc]initWithTitle:@"            " style:UIBarButtonItemStylePlain target:self action:nil];
    self.navigationItem.leftBarButtonItem=leftItem;
    
    UIButton *logoutButton=[UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backImage=[UIImage imageNamed:@"logout_icon"];
    UIImageView *logoutIv=[[UIImageView alloc]initWithImage:backImage];
    logoutIv.frame=CGRectMake(5, 5, 20, 20);
    [logoutButton addSubview:logoutIv];
    logoutButton.frame=CGRectMake(0, 0, 30, 30);
    [logoutButton addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:logoutButton];
    
    
    
    UIButton *appSettingsButton=[UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *settingsImage=[UIImage imageNamed:@"settings_menu_top"];
    UIImageView *settingsIv=[[UIImageView alloc]initWithImage:settingsImage];
    settingsIv.frame=CGRectMake(5, 5, 20, 20);
    [appSettingsButton addSubview:settingsIv];
    appSettingsButton.frame=CGRectMake(0, 0, 30, 30);
    [appSettingsButton addTarget:self action:@selector(showSettings:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:appSettingsButton];
    
    
    
    UIView *supportView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 149*0.95, 43*0.95)];
    UIImage *logo=[UIImage imageNamed:[WL logoMenuFileName]];
    UIImageView *iv=[[UIImageView alloc]initWithImage:logo];
    
    iv.contentMode=UIViewContentModeScaleAspectFit;
    iv.frame=CGRectMake(0, 0, supportView.frame.size.width, supportView.frame.size.height);
    [supportView addSubview:iv];
    self.navigationItem.titleView=supportView;
    supportView.center=CGPointMake(160, self.navigationController.navigationBar.frame.size.height/2);
    /*
    UIImage *logo=[UIImage imageNamed:[WL logoMenuFileName]];
    UIImageView *iv=[[UIImageView alloc]initWithImage:logo];
    iv.contentMode=UIViewContentModeScaleAspectFit;
    iv.frame=CGRectMake(0, 0, 149*0.75, 43*0.75);
    self.navigationItem.titleView=iv;
    
    [self.navigationController.navigationBar addSubview:iv];
    iv.center=CGPointMake(160, self.navigationController.navigationBar.frame.size.height/2);
    */
    
    
    
    self.navigationController.navigationBar.clipsToBounds=NO;
    UIView *onePxLine=[[UIView alloc]initWithFrame:CGRectMake(0, 44, 320, 1)];
    onePxLine.backgroundColor = [[MyColorTheme sharedInstance]loginNavigationHeaderBackgroudColor];
    [self.navigationController.navigationBar addSubview:onePxLine];
    

    
    
    

    
    
    
    
        
        self.quickButtonsArray = [NSMutableArray array];
        
        self.buttonsArray = [@[@"manageCell",@"childrenCell"] mutableCopy];
    
    
    
    
    
    [[UserManager sharedManager] clearNewUserFlag];
    [[UserManager sharedManager] saveLocalUsers];
    
    [[UserManager sharedManager] debugPrintUsers];
    
    [[UserManager sharedManager] updateUserImage];
    
    [AppDelegate appLaunchedAfterTouchIdUpdate];

}


- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];

    [[PushNotificationManager sharedInstance] jump];

    
}


- (void)viewWillAppear:(BOOL)animated {
    
    
    [super viewWillAppear:animated];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    [self.navigationController.navigationBar setHidden:NO];
    
    
}

/*
- (void)showPushSettings:(id)sender {
    
    [self performSegueWithIdentifier:@"settingsViewController@Push_iPhone" sender:sender];
    
    
}
*/

- (void)showSettings:(id)sender {
    
    [self performSegueWithIdentifier:@"appSettingsSegue" sender:sender];
    
    
}

-(IBAction)logout:(id)sender
{
    
    
            [self doLogout];
    
    
    
}


- (void)doLogout {
    
    [[AccessTimeManager sharedInstance] disableTimer];

    
    //Нет смысла отслеживать какой получен ответ. Отвязка от токена в приложении. На сервер не передаем никаких идентификаторов
    [[NetworkManager sharedInstance] logoutAndDeviceToken:YES completion:^(id response, NSError *error){}];
    
    [DataManager sharedInstance].adminId = 0;
    [NetworkManager sharedInstance].token = nil;
   
    //Нужно ресетнуть чтобы не крашилось если перелогиниваться. Так переинитим AOLPlist
    [AOLPlistBritish destroyInstance];
    
    /*if ([[UserManager sharedManager] isAllUsersRecivePinAvailability]) {
     if ([[UserManager sharedManager] isUsersWithPin]) {
     [self performSegueWithIdentifier:@"enterPinSegue" sender:self];
     NSLog(@"have users with pin");
     } else {
     [self performSegueWithIdentifier:@"loginSegue" sender:self];
     NSLog(@"NO have users with pin");
     }
     }*/
    [[UserManager sharedManager] recieveAndFillPinAvailabilityForAllUsers];
    [[UserManager sharedManager] successfullyLogout]; //запустить принудительную проверку пинов
    
    [[PushNotificationManager sharedInstance] clearQueue];
    
    SplashViewController *vc = [self.navigationController.viewControllers firstObject];
    [vc setColorTheme]; //обновляем цвет
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    
    //[self.navigationController popViewControllerAnimated:YES];
    
}


-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    NSLog(@"gesture regognizer should begin:%@",gestureRecognizer);
    return YES;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)scrollToPage2:(id)sender
{
    NSLog(@"scroll to page 2");
    if (self.pageControl.currentPage==1){return;}
    else
    {
        NSLog(@"must scroll");
        [self.scrollView setContentOffset:CGPointMake(320, 0) animated:YES];
        self.pageControl.currentPage=1;
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x==320)
    {
        self.pageControl.currentPage=1;
    }
    else if(scrollView.contentOffset.x==0)
    {
        self.pageControl.currentPage=0;
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView.tag == QUICK_MENU_COLLECTION_VIEW_TAG) {
        return [self.quickButtonsArray count];
    }
    return _buttonsArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *array = collectionView.tag == QUICK_MENU_COLLECTION_VIEW_TAG ? self.quickButtonsArray : self.buttonsArray;
    
    NSString *cellIdentifier=[array objectAtIndex:indexPath.row];
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];

    
    
    if (cell) {

        UIButton *button = (UIButton *)[cell.contentView viewWithTag:COLLECTION_BUTTON_TAG];

        /*
        
        if ([cellIdentifier isEqualToString:@"manageCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_mainscreen_manage_.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"Manage.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                
            }
        }
        
        
        
        if ([cellIdentifier isEqualToString:@"childrenCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_mainscreen_children_.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"Children.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                
            }
        }
        
         */
        
        //Quick Menu
        if ([cellIdentifier isEqualToString:@"quickObservationCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_qa_observations.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"Observation.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                
            }
        }
        
        if ([cellIdentifier isEqualToString:@"quickRegisterCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_qa_register.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"Register.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
        }
        if ([cellIdentifier isEqualToString:@"quickNappiesCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_qa_nappy_.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"square_button_nappies.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
        }
        if ([cellIdentifier isEqualToString:@"quickAccidentCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_qa_accidents.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"menu_medical_incident_icon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
        }
        if ([cellIdentifier isEqualToString:@"quickLongTermCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_qa_longterm_.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"menu_medical_long_term_icon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
        }
        if ([cellIdentifier isEqualToString:@"quickShortTermCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_qa_shortterm_.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"menu_medical_short_term_icon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
        }
        
        

        
        
        
        
        if ([cellIdentifier isEqualToString:@"manageCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManage]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"childrenCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageChildren]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"settingsCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASSettings]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"diariesCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASDiaryOverview]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"progressCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASProgress]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"progressJ15Cell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASProgress]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"nextStepsCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASNextStep]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"permissionsCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASPermissions]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"policiesCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASPolicies]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"medicalCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASMedical]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"registerCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASRegister]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"bookkeepingCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASBookkeeping]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"accountsCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASBookkeeping]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"galleriesCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASGalleries]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"privateMessageCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASPrivateMessages]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"dailyCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASDaily]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"parentsCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASParents]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"staffCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASStaff]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"legalCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASLegal]) {
            [self buttonAsDisabled:button];
        }
        
        //Quick
        else if ([cellIdentifier isEqualToString:@"quickObservationCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASProgressAddObservation]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"quickRegisterCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASRegisterToday]) {
            [self buttonAsDisabled:button];
        }

        else if ([cellIdentifier isEqualToString:@"quickNappiesCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageChildrenNappiesNappyOverview]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"quickAccidentCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASMedicalAccidentsAccidentsAdd]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"quickLongTermCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASMedicalLongTermAdd]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"quickShortTermCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASMedicalShortTermAdd]) {
            [self buttonAsDisabled:button];
        }
    }
    
    return cell;
}




- (void) configureCollectionCell:(UICollectionViewCell*)cell
{
    UIButton * button = (UIButton *)[cell.contentView viewWithTag:COLLECTION_BUTTON_TAG];
    UILabel * label = (UILabel *)[cell.contentView viewWithTag:COLLECTION_LABEL_TAG];
    
    if ([NetworkManager sharedInstance].isJ3) {
        if ([[NetworkManager sharedInstance] isScottish]) {
            [button setBackgroundImage:[[UIImage imageNamed:@"j3_mainscreen_wellbeing"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            label.text = @"Wellbeing";
        } else {
            [button setBackgroundImage:[[UIImage imageNamed:@"j3_mainscreen_progress"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            label.text = @"Progress";
        }
    }
    else {
        if ([[NetworkManager sharedInstance] isScottish]) {
            [button setBackgroundImage:[[UIImage imageNamed:@"Wellbeing"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            label.text = @"Wellbeing";
        } else {
            [button setBackgroundImage:[[UIImage imageNamed:@"Progress.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            label.text = @"Progress";
        }
    }
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.

    /*
    NSLog(@"self.scrollview.contentSize:%f,%f",self.scrollView.contentSize.width,self.scrollView.contentSize.height);
    NSLog(@"self.scrollview.frame:%f,%f",self.scrollView.frame.size.width,self.scrollView.frame.size.height);
    NSLog(@"self.view.frame:%f,%f",self.view.frame.size.width,self.view.frame.size.height);
    NSLog(@"self.contentView.size:%f,%f",self.contentView.frame.size.width,self.contentView.frame.size.height);
    */

}


- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    
    NSLog(@"Identifier: %@", identifier);
    
    if ([identifier isEqualToString:@"manage"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManage]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"childrenSegue"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageChildren]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"settings"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASSettings]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"diaryOverview@Diaries_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASDiaryOverview]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"Overview@Progress_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASProgress]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"progressSegue"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASProgress]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"LPChildren@NextSteps_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASNextStep]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"permissions"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASPermissions]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"policies"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASPolicies]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"medical"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASMedical]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"register"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASRegister]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"bookkeeping"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASBookkeeping]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"galleries"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASGalleries]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"listDef@PrivateMessages_Phone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASPrivateMessages]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"dailySegue"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASDaily]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"parentsSegue"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASParents]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"staffSegue"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASStaff]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"legalSegue"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASLegal]) {
        [self showUnavailableAlert];
        return NO;
    }
    
    
    //quick:
    else if ([identifier isEqualToString:@"thisDayRegisters@Register_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASRegisterToday]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"nappyOverview@Nappies_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageChildrenNappiesNappyOverview]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"CreateAccidentVC@Medical_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASMedicalAccidentsAccidentsAdd]) {
        [self showUnavailableAlert];
        return NO;
    }
    
    else {
        return YES;
    }

    
}



- (IBAction)addLongTerm:(id)sender {
    
    if ([[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASMedicalLongTermAdd]) {
        [self showUnavailableAlert];
        return;
    }

    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Medical_iPhone" bundle:nil];
    
    
    
}
- (IBAction)addShortTerm:(id)sender {
    
    
    if ([[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASMedicalShortTermAdd]) {
        [self showUnavailableAlert];
        return;
    }
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Medical_iPhone" bundle:nil];
   
}

    
@end
