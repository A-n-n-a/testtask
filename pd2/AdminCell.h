//
//  AdminCell.h
//  pd2
//
//  Created by Admin on 04.04.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdminCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;

@property (nonatomic,retain)IBOutlet UIButton *renameButton;
@property (nonatomic,retain)IBOutlet UIButton *deleteButton;

@property (nonatomic,retain)IBOutlet UIImageView *editButtonImage;
@property (nonatomic,retain)IBOutlet UIImageView *deleteButtonImage;

@property (nonatomic,retain)IBOutlet UILabel *usernameLabel;
@property (nonatomic,retain)IBOutlet UILabel *fNameLabel;
@property (nonatomic,retain)IBOutlet UILabel *lNameLabel;
@property (nonatomic,retain)IBOutlet UILabel *adminLevelLabel;
@property (nonatomic,retain)IBOutlet UILabel *roomLabel;

@property (nonatomic,retain)IBOutlet UIImageView *userImageView;

@property (nonatomic,retain) NSDictionary *infoDict;

-(IBAction)toolButtonPressed:(id)sender;
-(IBAction)toolButtonReleased:(id)sender;


@end
