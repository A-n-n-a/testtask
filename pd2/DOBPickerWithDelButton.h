//
//  DOBPickerWithDelButton.h
//  pd2
//
//  Created by Vasyl Balazh on 17.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DOBPickerWithDelButton : UIViewController

@property bool validationMode;
@property NSString *validString, *invalidString;

@property (copy) void (^validationHandler)(NSDate *newDate);
@property (copy) void (^dateHandler)(NSDate *newDate);
@property (copy) void (^finishHandler)();
@property (copy) void (^deletingHandler)();

@property (nonatomic,retain)NSDate *minDate;
@property (nonatomic,retain)NSDate *maxDate;

@property(nonatomic,retain)IBOutlet UIDatePicker *picker;
@property(nonatomic,retain)IBOutlet UIToolbar *toolBar;
@property(nonatomic,retain)IBOutlet UIBarButtonItem *doneButton;
@property(nonatomic,retain)IBOutlet UIBarButtonItem *deleteButton;

@property(nonatomic,retain)IBOutlet UILabel *validationLabel;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *activityInficator;

@property (nonatomic,retain)NSDate *initialDate;
@property (nonatomic,assign)BOOL isTimeMode;
@property (nonatomic,assign)BOOL is24hFormat;

@property (nonatomic,assign)BOOL hideDeleteButton;


-(IBAction)close:(id)sender;
-(IBAction)deleting:(id)sender;
-(IBAction)datePickerValueChanged;
-(void)show;
-(IBAction)hide;


@end
