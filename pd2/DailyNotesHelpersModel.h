//
//  DailyNotesHelpersModel.h
//  pd2
//
//  Created by Andrey on 08/03/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXBaseModel.h"

@interface DailyNotesHelpersModel : AXBaseModel

@property (nonatomic, strong) NSMutableArray *breakfast;
@property (nonatomic, strong) NSMutableArray *morningSnack;
@property (nonatomic, strong) NSMutableArray *lunch;
@property (nonatomic, strong) NSMutableArray *afternoonSnack;

@property (nonatomic, strong) NSMutableArray *helpersTitle;
@property (nonatomic, strong) NSMutableArray *helpers;


- (instancetype)initWithDict:(NSDictionary *)dict;


@end
