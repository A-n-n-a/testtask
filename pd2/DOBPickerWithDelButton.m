//
//  DOBPickerWithDelButton.m
//  pd2
//
//  Created by Vasyl Balazh on 17.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DOBPickerWithDelButton.h"

@interface DOBPickerWithDelButton ()

@end

@implementation DOBPickerWithDelButton


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.frame=[UIScreen mainScreen].bounds;
    self.picker.backgroundColor=[UIColor whiteColor];
    
    if (self.isTimeMode) {
        self.picker.datePickerMode = UIDatePickerModeTime;
        
        if (self.is24hFormat) {
            self.picker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
        }
    }
    
    self.picker.maximumDate=self.maxDate;
    self.picker.minimumDate=self.minDate;

    
    if (self.hideDeleteButton) {
        self.deleteButton.title = nil;
        self.deleteButton.enabled = NO;
    } else {
        self.deleteButton.title = @"Delete";
        self.deleteButton.enabled = YES;
        self.deleteButton.action = @selector(deleting:);
    }
    
    
    if (self.initialDate) {
        self.picker.date=self.initialDate;
    }
    
    
    self.doneButton.action=@selector(close:);
    
    
    if (self.validationMode) {
        _validationLabel.hidden=NO;
    }
    
    [self datePickerValueChanged];
}


- (IBAction)close:(id)sender
{
    NSLog(@"datepicker.close with block:%@",self.finishHandler);
    NSLog(@"dateBlock:%@",self.dateHandler);
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        if (self.dateHandler)
        {
            self.dateHandler(self.picker.date);
        }
        
        if (self.finishHandler)
        {
            NSLog(@"handler must be activated");
            self.finishHandler();
        }
        self.dateHandler=nil;
        self.finishHandler=nil;
    }];
}


- (IBAction)deleting:(id)sender
{
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = frame;
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
        if (self.deletingHandler)
        {
            self.deletingHandler();
        }
        self.dateHandler = nil;
        self.finishHandler = nil;
        self.deletingHandler = nil;
    }];
}


-(IBAction)hide
{
    NSLog(@"datepicker.hide");
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
        if (self.finishHandler)
        {
            self.finishHandler();
        }
        self.dateHandler=nil;
        self.finishHandler=nil;
    }];
}


-(void)show
{
    CGRect frame = CGRectMake(0,0, 320, self.view.frame.size.height);
    CGRect startRect = frame;
    startRect.origin.y += self.view.frame.size.height;
    
    self.view.frame = startRect;
    
    AppDelegate *delegate=[AppDelegate delegate];
    [delegate.window addSubview:self.view];
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    }];
}


-(IBAction)datePickerValueChanged
{
    NSLog(@"%@",self.dateHandler);
    if (_validationMode)
    {
        if (self.validationHandler)
        {
            self.validationHandler(self.picker.date);
        }
    }
    return;
}

@end
