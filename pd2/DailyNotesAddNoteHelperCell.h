//
//  DailyNotesAddNoteHelperCell.h
//  pd2
//
//  Created by Andrey on 28/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@class DailyNotesNoteModel;

@interface DailyNotesAddNoteHelperCell : AXTableViewCell

@property (weak, nonatomic) IBOutlet UITextField *helperTextField;
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;

- (void)updateWithModel:(DailyNotesNoteModel *)model;

@end
