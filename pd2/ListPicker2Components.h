//
//  ListPicker2Components.h
//  Exchange
//
//  Created by Andrey on 02.04.15.
//  Copyright (c) 2014 Vexadev LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListPicker2Components : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UIGestureRecognizerDelegate>

@property (copy) void (^handler)(NSString *string0Comp, int index0Comp, NSString *string1Comp, int index1Comp);
@property (copy) void (^finishHandler)();

@property int selectedIndexIn0Component;
@property int selectedIndexIn1Component;

@property NSString *selectedStringIn0Component;
@property NSString *selectedStringIn1Component;

@property (nonatomic,retain) NSArray *dataArray0Component;
@property (nonatomic,retain) NSArray *dataArray1Component;

@property (nonatomic,retain)IBOutlet UIPickerView *picker;
@property (nonatomic,retain)IBOutlet UIBarButtonItem *doneButton, *cancelButton;

-(void)show;
-(IBAction)hide;


@end
