//
//  TextFieldForCash.m
//  pd2
//
//  Created by Vasyl Balazh on 29.05.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//


#define POUND_SYMBOL @"£ "


#import "TextFieldForCash.h"

@implementation TextFieldForCash

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.delegate = self;
        self.keyboardType = UIKeyboardTypeDecimalPad;
    }
    return self;
}

- (id)init
{
    if (self = [super init]) {
        self.delegate = self;
        self.keyboardType = UIKeyboardTypeDecimalPad;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.delegate = self;
        self.keyboardType = UIKeyboardTypeDecimalPad;
    }
    return self;
}


#pragma mark - TextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField setSpellCheckingType:UITextSpellCheckingTypeNo];
    [self setShouldUpdateAttributes:YES];
    if (textField.text.length == 0) {
        [textField setText:POUND_SYMBOL];
    }
}


- (void)textFieldDidChangeText:(UITextField *)sender
{
    if (self.shouldUpdateAttributes) {
        [self setShouldUpdateAttributes:NO];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:sender.text];
        
        [attributedString setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]} range:NSMakeRange(0, POUND_SYMBOL.length)];
        [attributedString setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]} range:NSMakeRange(POUND_SYMBOL.length, sender.text.length - POUND_SYMBOL.length)];
        
        [sender setAttributedText:attributedString];
        [sender resignFirstResponder];
        [sender becomeFirstResponder];
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self setShouldUpdateAttributes:YES];
    NSRange substringRange = [textField.text rangeOfString:POUND_SYMBOL];
    if (range.location >= substringRange.location && range.location < substringRange.location + substringRange.length) {
        return NO;
    }
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self endEditing:YES];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text isEqualToString:POUND_SYMBOL]) {
        textField.text = [NSString stringWithFormat:@"%@00", POUND_SYMBOL];
    }
    
    NSString *suffix = [textField.text containsString:@"."] ? @"00" : @".00";
    textField.text = [NSString stringWithFormat:@"%@%@", textField.text, suffix];
    
    NSRange range = [textField.text rangeOfString:@"."];
    NSString *newString = [textField.text substringWithRange:NSMakeRange(0, range.location+3)];
    textField.text = newString;
}


@end
