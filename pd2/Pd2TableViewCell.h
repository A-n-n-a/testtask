#import <UIKit/UIKit.h>

@class Pd2TableViewCell;
@protocol Pd2TableViewCellDelegate <NSObject>
@required
/**
 * @param UITextView|UITextField field
 */
- (void) pd2TableViewCell:(Pd2TableViewCell *)cell beginEditingField:(id)field;
@end

@interface Pd2TableViewCell : UITableViewCell<UITextViewDelegate, UITextFieldDelegate>
@property (nonatomic, strong) NSMutableArray * array_textFields;
@property (nonatomic, strong) NSMutableArray * array_textViews;
@property (nonatomic, weak) id<Pd2TableViewCellDelegate>scrollKeyboardDelegate;

@end
