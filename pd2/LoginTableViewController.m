//
//  LoginTableViewController.m
//  pd2
//
//  Created by i11 on 21/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "LoginTableViewController.h"
#import "AppDelegate.h"
#import "UserManager.h"
#import "DataManager.h"
#import "AccessTimeManager.h"
#import "RegisterForceManager.h"
#import "User.h"
#import "DomainCompanyModel.h"
#import "LoginPickerControl.h"
#import "UnavailableSectionManager.h"
#import "IconButton.h"
#import "UIImage+ImageByApplyingAlpha.h"
#import "LoginLogoCell.h"

#import "PSTAlertController.h"

#import "NSString+VersionNumbers.h"

#import "MyColorTheme.h"
#import "ListPicker.h"

#import "VPBiometricAuthenticationFacade.h"

#import <LocalAuthentication/LocalAuthentication.h>
#import "WL.h"

#define LOGO_IMAGE_VIEW_TAG 100
#define ALERT_DOMAIN_TEXT_FIELD 103

//#import "MyColorTheme.h"

NSString * const kPDLogo = @"kPDLogo";
NSString * const kPDLogin = @"kPDLogin";
NSString * const kPDPicker = @"kPDPicker";
NSString * const kPDPassword = @"kPDPassword";
NSString * const kPDDomain = @"kPDDomain";
NSString * const kPDSubmit = @"kPDSubmit";
NSString * const kPDSpacer = @"kPDSpacer";
NSString * const kPDVersion = @"kPDVersion";
NSString * const kPDOptions = @"kPDOptions";

@interface LoginTableViewController () <UIGestureRecognizerDelegate>

@property (strong, nonatomic) NSMutableArray *rows;
@property (strong, nonatomic) NSArray *visitedUsers;
@property (assign, nonatomic) BOOL isPickerShow;

@property (strong, nonatomic) LoginPickerControl *loginPickerControl;

@property (assign, nonatomic) BOOL isShowPassword;
@property (assign, nonatomic) BOOL isEngineeringMode;

@property (strong, nonatomic) NSString *whiteLabelFullDomain;
@property (strong, nonatomic) NSString *whiteLabelCompanyName;


@property (strong, nonatomic) ListPicker *listPicker;


@end

@implementation LoginTableViewController

- (void)loadView {
    [super loadView];
    
    self.rows = [NSMutableArray arrayWithObjects:
                 kPDLogo, //На время тестирования убираем, так как selenium не видит domain по isVisible
                 kPDDomain,
                 kPDLogin,
                 kPDPassword,
                 kPDSubmit,
                 kPDSpacer,
                 kPDVersion,
                 //kPDOptions,
                 nil];
    
    self.isPickerShow = NO;
    self.isShowPassword = NO;
    self.isEngineeringMode = NO;
    
    //Может передаваться из других viewController'ов. default user для ввода
    if (!self.activeUser && !self.addNewUser) {
        self.activeUser = [[UserManager sharedManager] lastLoggedUser];
    }
    
    [self disableInactiveTimer];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(freeTapAction:)];
    [self.view addGestureRecognizer:tap];
    
    
    self.yellowb.layer.cornerRadius = self.blueb.layer.cornerRadius = 25;
    self.yellowb.layer.borderWidth = self.blueb.layer.borderWidth = 1;
    self.yellowb.layer.borderColor = self.blueb.layer.borderColor = [[UIColor blackColor]CGColor];
    
    self.view.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
    
    [[UserManager sharedManager] debugPrintUsers];
    
}


- (void)updateTextFieldsWithUser:(User *)user {
    self.loginTextField.text = user.userName;
    if ([NetworkManager sharedInstance].isWhiteLabel) {
        self.whiteLabelFullDomain = user.subDomain;
        self.whiteLabelCompanyName = user.companyName;
        if (user.companyName) {
            self.domainTextField.text = user.companyName;
        }
        else {
            self.domainTextField.text = [self replaceInDomainString:user.subDomain];
        }
    }
    else {
        self.domainTextField.text = [self replaceInDomainString:user.subDomain];
    }
}


- (NSString *)replaceInDomainString:(NSString *)origString {
    
    //Удаляем .mybabysdays.com из домена
    
    
    if ([NetworkManager sharedInstance].isWhiteLabel) {
        NSArray *components = [origString componentsSeparatedByString:@"."];
        if ([components count]) {
            return [components objectAtIndex:0];
        }
        else {
            return @"";
        }
    }
    
    
    NSRange containRange = [origString rangeOfString:@".mybabysdays.com" options:NSCaseInsensitiveSearch];
    if (containRange.location != NSNotFound) {
        return [origString stringByReplacingCharactersInRange:containRange withString:@""];
    }
    
    return @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    if ([NetworkManager sharedInstance].isWhiteLabel) {
        
        BOOL showAlert = NO;
        
        if (![NetworkManager sharedInstance].whiteLabelSubdomains || ![[NetworkManager sharedInstance].whiteLabelSubdomains isKindOfClass:[NSArray class]]) {
            showAlert = YES;
        }
        
        if (![[NetworkManager sharedInstance].whiteLabelSubdomains count]) {
            showAlert = YES;
        }
        
        if (showAlert) {
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:@"Error"
                                                message:@"Try again later"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *action =
            [UIAlertAction actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       [AppDelegate logoutAndSendDeviceTokenIs:NO];
                                   }];
            
            [alert addAction:action];
            
            [self presentViewController:alert animated:YES completion:nil];
         
            return;
        }
        
        
    }
    
    
    VPBiometricAuthenticationFacade *biometricFacede = [[VPBiometricAuthenticationFacade alloc] init];

    
    if (self.activeUser) {
        [self updateTextFieldsWithUser:self.activeUser];
        User *user = [[UserManager sharedManager] getUserWithTouchId];
        if ([user isEqual:self.activeUser] && [biometricFacede isAuthenticationAvailable]) {
            [self updateTextFieldsWithUser:user];
            [self loginByTouchId];
        }
    
    } else {
        if ([NetworkManager sharedInstance].isWhiteLabel) {
            
            //Именно такой порядок:
            //был ранее залогиненный,
            //пришел всего 1 сервер,
            //то, что осталось
            if (![[[UserManager sharedManager] defaultCompanyName] isEqualToString:@""]) {
                self.domainTextField.text = [[UserManager sharedManager] defaultCompanyName];
                self.whiteLabelCompanyName = [[UserManager sharedManager] defaultCompanyName];
                self.whiteLabelFullDomain = [[UserManager sharedManager] defaultSubDomain];
            }
            else if ([[NetworkManager sharedInstance].whiteLabelSubdomains count] == 1){
                DomainCompanyModel *selectedModel = [[NetworkManager sharedInstance].whiteLabelSubdomains firstObject];
                [NetworkManager sharedInstance].whiteLabelDomain = selectedModel.domain;
                self.domainTextField.text = selectedModel.companyName;
                self.whiteLabelCompanyName = selectedModel.companyName;
                self.whiteLabelFullDomain = [NSString stringWithFormat:@"%@.%@", selectedModel.subdomain, selectedModel.domain];
            }
            else {
                self.domainTextField.text = [[UserManager sharedManager] defaultCompanyName];
                self.whiteLabelCompanyName = [[UserManager sharedManager] defaultCompanyName];
                self.whiteLabelFullDomain = [[UserManager sharedManager] defaultSubDomain];
            }
        }
        else {
            self.domainTextField.text = [self replaceInDomainString:[[UserManager sharedManager] defaultSubDomain]];
        }
    
        User *user = [[UserManager sharedManager] getUserWithTouchId];
        if ([biometricFacede isAuthenticationAvailable] && user && [[UserManager sharedManager].sortedUsers count] == 1) {
            [self updateTextFieldsWithUser:user];
            [self loginByTouchId];
        }
    
    }
    
    
    
    
//    BOOL isCanLoginWithTouchId = [biometricFacede isAuthenticationAvailable] && [[UserManager sharedManager].sortedUsers count] == 1 && [UserManager sharedManager].isUsersWithTouchId;
//    User *user = [[UserManager sharedManager].sortedUsers firstObject];
//    if (isCanLoginWithTouchId && user) {
//        [self updateTextFieldsWithUser:user];
//        [self loginByTouchId];
//    }
    
    //[self updateDomainColor];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setColorTheme
{
    [super setColorTheme];
    
    
    NSArray *cells = [NSArray arrayWithObjects:
                      self.logoCell, self.loginCell, self.passwordCell,
                      self.domainCell, self.submitCell, self.optionsCell,
                      self.pickerCell, nil]; //pickerCell должен идти последним (мб nil)

    for (UITableViewCell *cell in cells) {
        
         if ([cell respondsToSelector:@selector(setBackgroundColor:)]) {
            [cell performSelector:@selector(setBackgroundColor:) withObject:[[MyColorTheme sharedInstance]backgroundColor] afterDelay:0];
        }
        
    }
   
}


#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *row = [self.rows objectAtIndex:indexPath.row];
    
    if ([row isEqualToString:kPDLogo]) {
        return 153.f;
    }
    else if ([row isEqualToString:kPDLogin]) {
        return 43.f;
    }
    else if ([row isEqualToString:kPDPicker]) {
        return 175.f;
    }
    else if ([row isEqualToString:kPDPassword]) {
        return 43.f;
    }
    else if ([row isEqualToString:kPDDomain]) {
        return 43.f;
    }
    else if ([row isEqualToString:kPDSubmit]) {
        return 48.f;
    }
    else if ([row isEqualToString:kPDSpacer]) {
        
        return [[UIScreen mainScreen] bounds].size.height - 153.f - 43.f - 43.f - 48.f - 47.f - 70.f;
    }
    else if ([row isEqualToString:kPDVersion]) {
        return 47.f;
    }
    else if ([row isEqualToString:kPDOptions]) {
        return 140.f;
    }
    else {
        return 0.f;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.rows count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *row = [self.rows objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    if ([row isEqualToString:kPDLogo])
    {
        LoginLogoCell *cell=(LoginLogoCell *)[tableView dequeueReusableCellWithIdentifier:@"logoCell"];
        if (cell)
        {
            if (!self.logoCell) self.logoCell = cell;
            
            self.logoCell.logoTop.constant = [WL logoPasswordLoginRect].origin.y;
            self.logoCell.logoWidth.constant = [WL logoPasswordLoginRect].size.width;
            self.logoCell.logoHeight.constant = [WL logoPasswordLoginRect].size.height;
            
            if (!self.logoImageView) {
                self.logoImageView = self.logoCell.logoImageView;
                self.logoImageView.image = [UIImage imageNamed:[WL logoPasswordLoginFileName]];
                //self.logoImageView.frame = [WL logoPasswordLoginRect];
                /*
                self.logoImageView.gestureRecognizers = nil;
                UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(engineeringModeGestureRecognizer:)];
                longPressGesture.minimumPressDuration = 3; //seconds
                longPressGesture.numberOfTapsRequired = 0;
                //longPressGesture.numberOfTouchesRequired = 1;

                longPressGesture.delegate = self;
                [self.logoImageView addGestureRecognizer:longPressGesture];
                */
            }
            
            cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
            return cell;
        }
    }
    
    else if ([row isEqualToString:kPDLogin])
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"loginCell"];
        if (cell)
        {
            if (!self.loginCell) self.loginCell = cell;
            if (!self.loginTextField) {
                self.loginTextField = (UITextField *)[cell viewWithTag:100];
                self.loginTextField.delegate = self;
            }
            cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
            [cell viewWithTag:50].layer.cornerRadius=5;
            
            //[cell viewWithTag:50].layer.borderColor = [[MyColorTheme sharedInstance] loginTextColor].CGColor;
            //[cell viewWithTag:50].layer.borderWidth = 0.5f;
            return cell;
        }
    }
    else if ([row isEqualToString:kPDPicker])
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"pickerCell"];
        if (cell)
        {
            self.pickerCell = cell;
            
            self.picker = (UIPickerView *)[cell viewWithTag:100];
            self.picker.dataSource = self;
            self.picker.delegate = self;
            
            [cell viewWithTag:50].layer.cornerRadius = 5;
            [cell viewWithTag:50].layer.masksToBounds = YES;
            
            //[cell viewWithTag:50].layer.borderColor = [[MyColorTheme sharedInstance] loginTextColor].CGColor;
            //[cell viewWithTag:50].layer.borderWidth = 0.5f;
            
            
            cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
            
            //if ([[[UserManager sharedManager] users] count] == 1) {
                
                //Если всего 1 user, то подтверждаем его по тапу на row пикера
                
                //UITapGestureRecognizer* gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewTapGestureRecognized:)];
                
                //gestureRecognizer.cancelsTouchesInView = NO;
                //[[cell viewWithTag:50] addGestureRecognizer:gestureRecognizer];
                
               
                /*
                CGRect pickerFrame = self.picker.frame;
                
                CGRect loginPickerControlFrame = CGRectMake(pickerFrame.origin.x, pickerFrame.origin.y, pickerFrame.size.width, pickerFrame.size.height);
                self.loginPickerControl = [[LoginPickerControl alloc] initWithFrame:loginPickerControlFrame];
                self.loginPickerControl.backgroundColor = [UIColor clearColor];
                
                [self.loginPickerControl addTarget: self
                                action: @selector(pickerControlTapped)
                      forControlEvents: UIControlEventTouchUpInside];
                
                [[cell viewWithTag:50] addSubview:self.loginPickerControl];
                 */
            //}
            

            
            
            
            
            /*
            //offscreen rendering
             
            UIImageView *imageView = (UIImageView *)[cell viewWithTag:51];
            
            UIImage *image = [UIImage imageNamed:@"whiteTest"];
            
            // Begin a new image that will be the new image with the rounded corners
            // (here with the size of an UIImageView)
            UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, NO, 1.0);
            
            // Add a clip before drawing anything, in the shape of an rounded rect
            [[UIBezierPath bezierPathWithRoundedRect:imageView.bounds
                                        cornerRadius:5.0] addClip];
            // Draw your image
            [image drawInRect:imageView.bounds];
            
            // Get the image, here setting the UIImageView image
            imageView.image = UIGraphicsGetImageFromCurrentImageContext();
            
            // Lets forget about that we were drawing
            UIGraphicsEndImageContext();
            */
            
            return cell;
        }
    }
    else if ([row isEqualToString:kPDPassword])
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"passwordCell"];
        if (cell)
        {
            if (!self.passwordCell) self.passwordCell = cell;
            if (!self.passwordTextField) {
                self.passwordTextField = (UITextField *)[cell viewWithTag:100];
                self.passwordTextField.delegate = self;
            }
            if (!self.eyeButton) {
                self.eyeButton = (IconButton *)[cell viewWithTag:200];
            }
            
            UIImage *eyeImage = [[UIImage imageNamed:@"eye"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            if (self.isShowPassword) {
                self.passwordTextField.secureTextEntry = NO;
                [self.eyeButton setNewDefaultImage:[eyeImage imageByApplyingAlpha:0.9f]];
            }
            else {
                self.passwordTextField.secureTextEntry = YES;
                [self.eyeButton setNewDefaultImage:[eyeImage imageByApplyingAlpha:0.5f]];
            }
            
            
            cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
            [cell viewWithTag:50].layer.cornerRadius=5;
            
            //[cell viewWithTag:50].layer.borderColor = [[MyColorTheme sharedInstance] loginTextColor].CGColor;
            //[cell viewWithTag:50].layer.borderWidth = 0.5f;
            
            return cell;
        }
    }
    else if ([row isEqualToString:kPDDomain])
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"domainCell"];
        if (cell)
        {
            if (!self.domainCell) self.domainCell = cell;
            if (!self.domainTextField) {
                self.domainTextField = (UITextField *)[cell viewWithTag:100];
                self.domainTextField.delegate = self;
                /*
                [self.domainTextField addTarget:self
                              action:@selector(domainTextFieldDidChange:)
                    forControlEvents:UIControlEventEditingChanged];
                */
                
                
                self.domainTextField.gestureRecognizers = nil;
                UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(engineeringModeGestureRecognizer:)];
                longPressGesture.minimumPressDuration = 3; //seconds
                longPressGesture.numberOfTapsRequired = 0;
                //longPressGesture.numberOfTouchesRequired = 1;
                
                longPressGesture.delegate = self;
                [self.domainTextField addGestureRecognizer:longPressGesture];
                
            }
            
            if (!self.domainView) {
                self.domainView = (UIView *)[cell viewWithTag:50];
                
            }
            
            //[self updateDomainColor];
            
            cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
            [cell viewWithTag:50].layer.cornerRadius=5;
            
            //[cell viewWithTag:50].layer.borderColor = [[MyColorTheme sharedInstance] loginTextColor].CGColor;
            //[cell viewWithTag:50].layer.borderWidth = 0.5f;
            
            return cell;
        }
    }
    else if ([row isEqualToString:kPDSubmit])
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"submitCell"];
        if (cell)
        {
            if (!self.submitCell) self.submitCell = cell;
            if (!self.submitButton) {
                self.submitButton = (UIButton *)[cell viewWithTag:100];
                [self.submitButton addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
            return cell;
        }
    }
    else if ([row isEqualToString:kPDSpacer])
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"spacerCell"];
        if (cell)
        {
            cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
            return cell;
        }
    }
    else if ([row isEqualToString:kPDVersion])
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"versionCell"];
        if (cell)
        {
            UILabel *versionLabel = (UILabel *)[cell viewWithTag:504];
            
            NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
            NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
            versionLabel.text = [NSString stringWithFormat:@"Version: %@ (%@)", appVersionString, appBuildString];
            versionLabel.textColor = [[MyColorTheme sharedInstance] menuButtonsColor];
            
            
            
            cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
            return cell;
        }
    }
    
    else if ([row isEqualToString:kPDOptions])
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"optionsCell"];
        if (cell)
        {
            if (!self.optionsCell) self.optionsCell = cell;
            if (!self.blueb) {
                self.blueb = (UIButton *)[cell viewWithTag:101];
                [self.blueb addTarget:self action:@selector(blueAction:) forControlEvents:UIControlEventTouchUpInside];
                self.blueb.layer.cornerRadius = 25;
                self.blueb.layer.borderWidth = 1;
                self.blueb.layer.borderColor = [[UIColor blackColor]CGColor];
            }
            if (!self.yellowb) {
                self.yellowb = (UIButton *)[cell viewWithTag:102];
                [self.yellowb addTarget:self action:@selector(yellowAction:) forControlEvents:UIControlEventTouchUpInside];
                self.yellowb.layer.cornerRadius = 25;
                self.yellowb.layer.borderWidth = 1;
                self.yellowb.layer.borderColor = [[UIColor blackColor]CGColor];
            }
            
            //TODO: добавить для forgot password, и тд
            UIButton *forgot = (UIButton *)[cell viewWithTag:30];
            [self setButton:forgot withAlignment:NSTextAlignmentLeft];

            UIButton *sign = (UIButton *)[cell viewWithTag:40];
            [self setButton:sign withAlignment:NSTextAlignmentRight];
            
            
            UILabel *versionLabel = (UILabel *)[cell viewWithTag:504];
            
            NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
            NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
            versionLabel.text = [NSString stringWithFormat:@"Version: %@ (%@)", appVersionString, appBuildString];
            versionLabel.textColor = [[MyColorTheme sharedInstance] menuButtonsColor];

            
            
            cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
            return cell;
        }
    }


    return cell;
}

#pragma mark - Actions

- (void)submitAction:(id)sender{
    
    self.submitButton.highlighted = YES;
    self.submitButton.enabled = NO;
    NSString *login = self.loginTextField.text;
    NSString *password = self.passwordTextField.text;
    
    
    NSString *subDomain = self.domainTextField.text;
    if ([NetworkManager sharedInstance].isWhiteLabel && !self.isEngineeringMode) {
        subDomain = self.whiteLabelFullDomain;
    }
    NSString *completeSubDomain;

    NSString *domainAdd = @"mybabysdays.com";
    if ([NetworkManager sharedInstance].isWhiteLabel) {
        //domainAdd = [NetworkManager sharedInstance].whiteLabelDomain;
        //Иначе получается дублирование при возврате с сохраненных
        domainAdd = @"";
    }
    
    if (self.isEngineeringMode) {
        if  (![subDomain hasPrefix:@"http://"] || ![subDomain hasPrefix:@"https://"]) {
            //Без протокола NSURL host возвращает nil
            subDomain = [NSString stringWithFormat:@"https://%@", subDomain];
        }
        
        NSURL *url = [NSURL URLWithString:subDomain];
        if (url) {
            completeSubDomain = [url host];
        }
        else {
        
            subDomain = [subDomain stringByReplacingOccurrencesOfString:@"http://" withString:@""];
            subDomain = [subDomain stringByReplacingOccurrencesOfString:@"https://" withString:@""];
            
            if ([subDomain containsString:@"."]) {
                completeSubDomain = subDomain;
            }
            else {

                completeSubDomain = [NSString stringWithFormat:@"%@.%@", self.domainTextField.text, domainAdd];
            }
        }
        
    }
    else {
        if ([NetworkManager sharedInstance].isWhiteLabel) {
            completeSubDomain = self.whiteLabelFullDomain;
        }
        else {
            completeSubDomain = [NSString stringWithFormat:@"%@.%@", subDomain, domainAdd];
        }
        
    }
    
    
    [[NetworkManager sharedInstance]loginWithUsername:login password:password subdomain:completeSubDomain completion:^(id response, NSError *error) {
        
        if (response)
        {
            if([[response valueForKeyPath:@"result.complete"] intValue]==1)
            {
                
                NSString *iosString;
                
                @try {
                    iosString = [response valueForKeyPath:@"result.ios"];
                }
                @catch (NSException *exception) {
                    NSLog(@"Exception. Reason: %@", exception.reason);
                    NSLog(@"Для parent'а неверный response result");
                }
                
                NSNumber *iosUpdate = iosString ? [NSNumber numberWithInt:[iosString intValue]] : nil;

                //iosUpdate = @1;
                
                //Сервер рекоммендует обновиться
                if (iosUpdate && [iosUpdate isEqualToNumber:@1]) {
                    //Alert(@"Notice", @"Please update your application to the latest version");
                    
                    PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning" message:[response valueForKeyPath:@"result.msg"] preferredStyle:PSTAlertControllerStyleAlert];
                    
                    [alert addAction:[PSTAlertAction actionWithTitle:@"Update" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                        
                        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[WL appStoreLink]]];
                        [self loginProcedureWithResponse:response subdomain:completeSubDomain login:login];
                    }]];
                    
                    [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:^(PSTAlertAction *action) {
                        [self loginProcedureWithResponse:response subdomain:completeSubDomain login:login];
                        
                        
                    }]];
                    
                    
                    [alert showWithSender:self controller:self animated:YES completion:nil];
                    
                }
                else {
                    [self loginProcedureWithResponse:response subdomain:completeSubDomain login:login];
                }

            }
            else
            {
                
                NSString *iosString;
                
                @try {
                    iosString = [response valueForKeyPath:@"result.ios"];
                }
                @catch (NSException *exception) {
                    NSLog(@"Exception. Reason: %@", exception.reason);
                    NSLog(@"Неверный response result");
                }
                
                    NSNumber *iosUpdate = iosString ? [NSNumber numberWithInt:[iosString intValue]] : nil;
                    
                    if (iosUpdate && [iosUpdate isEqualToNumber:@2]) {
                        //Сервер настоятельно рекоммендует обновиться
                        //Alert(@"Error", @"Please update your application to the latest version");
                        //Alert(@"Error",[response valueForKeyPath:@"result.msg"] );
                        
                        
                        PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning" message:[response valueForKeyPath:@"result.msg"] preferredStyle:PSTAlertControllerStyleAlert];
                        
                        [alert addAction:[PSTAlertAction actionWithTitle:@"Update" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                            
                            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[WL appStoreLink]]];
                            
                        }]];
                        
                        [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:nil]];
                        
                        [alert showWithSender:self controller:self animated:YES completion:nil];
                        
                        self.submitButton.enabled=YES;
                        
                        return;
                        
                    }
                    else {
                        
                        
                        //для блокированных систем выводим msg в заголовок, text в тело сообщения. +tel по кнопке звонка
                        NSString *msgString = [response valueForKeyPath:@"result.msg"];
                        NSString *textString = [response valueForKeyPath:@"result.text"];
                        NSString *telString = [response valueForKeyPath:@"result.tel"];
                        
                        if (msgString && textString && telString) {
                            
                            PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:msgString message:textString preferredStyle:PSTAlertControllerStyleAlert];
                            
                            [alert addAction:[PSTAlertAction actionWithTitle:@"OK" style:PSTAlertActionStyleDefault handler:nil]];
                            
                            [alert addAction:[PSTAlertAction actionWithTitle:@"Call" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                
                                NSString *tel = [NSString stringWithFormat:@"tel:%@", telString];
                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
                                
                            }]];
                            
                            [alert showWithSender:self controller:self animated:YES completion:nil];
                            
                        }
                        else {
                            Alert(@"Error",[response valueForKeyPath:@"result.msg"]);
                        }
                        
                        self.submitButton.enabled=YES;
                    }
                    
                    
                        
                
                

            }
        }
        else if(error)
        {
            
            NSError *underlyingError = error.userInfo[NSUnderlyingErrorKey];
            if ([[[underlyingError userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode] == 404) {
                Alert(@"Error", @"Non-existent domain name. Please check the used data.");
            }
            else {
                Alert(@"Error", error.localizedDescription);
            }
            
            self.submitButton.enabled=YES;
        }
        
    }];
}

- (void)blueAction:(UIButton *)sender {
    [[MyColorTheme sharedInstance]setBlueTheme];
    [self setColorTheme];
}

- (void)yellowAction:(UIButton *)sender {
    [[MyColorTheme sharedInstance]setYellowTheme];
    [self setColorTheme];
}

- (IBAction)forgotPasswordAction:(id)sender {

//http://client`s/index.php?option=com_sted&controller=user&task=reset
    
    NSString *domain = self.domainTextField.text;
    
    //TODO: инженерное меню
    
    if ([domain length] >= 1) {
        NSString *link = [NSString stringWithFormat:@"http://%@/index.php?option=com_sted&controller=user&task=reset", domain];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
    }
    else {
        
        PSTAlertController *forgotPasswordController = [PSTAlertController alertWithTitle:@"Enter Subdomain" message:nil];
        [forgotPasswordController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.keyboardType = UIKeyboardTypeURL;
            //textField.text = @".mybabysdays.com";
            textField.tag = ALERT_DOMAIN_TEXT_FIELD;
            textField.delegate = self;
        }];
        [forgotPasswordController addCancelActionWithHandler:NULL];
        
        [forgotPasswordController addAction:[PSTAlertAction actionWithTitle:@"Next" handler:^(PSTAlertAction *action) {
            NSString *domain = action.alertController.textField.text;
            NSString *link = [NSString stringWithFormat:@"http://%@/index.php?option=com_sted&controller=user&task=reset", domain];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
        }]];
        
        [forgotPasswordController showWithSender:nil controller:self animated:YES completion:NULL];
       
    }

}

- (IBAction)signUpAction:(id)sender {

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.babysdays.com/index.php?option=com_billing&view=billing"]];

}

- (IBAction)selectThemeAction:(UIButton *)sender {
    
    self.listPicker = [[ListPicker alloc] init];
    self.listPicker.dataArray = [MyColorTheme sharedInstance].themesList;
    
    [self.listPicker setHandler:^(NSString *value, int index) {
        [[MyColorTheme sharedInstance] setThemeName:value];
    }];
    [self.listPicker show];
}



- (IBAction)showPasswordAction:(id)sender {
    
    self.isShowPassword = !self.isShowPassword;
    
    UIImage *eyeImage = [[UIImage imageNamed:@"eye"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    if (self.isShowPassword) {
        self.passwordTextField.secureTextEntry = NO;
        [self.eyeButton setNewDefaultImage:[eyeImage imageByApplyingAlpha:0.9f]];
    }
    else {
        self.passwordTextField.secureTextEntry = YES;
        [self.eyeButton setNewDefaultImage:[eyeImage imageByApplyingAlpha:0.5f]];
    }
    self.passwordTextField.font = nil;
    self.passwordTextField.font = [UIFont systemFontOfSize:14.f];

    
    
}


- (void)freeTapAction:(id)sender{
    /*
    CGPoint location = [sender locationInView:self.loginPickerControl];
    
    NSLog(@"point %@, frame %@", NSStringFromCGPoint(location), NSStringFromCGRect(self.loginPickerControl.frame));
    
    if (CGRectContainsPoint(self.loginPickerControl.frame, location)) {
        return;
    }*/
    /*
     NSLog(@"freetap");
    
     CGPoint location = [sender locationInView:self.picker];
     
     NSLog(@"point %@, frame %@", NSStringFromCGPoint(location), NSStringFromCGRect(self.picker.frame));
     
     if (CGRectContainsPoint(self.picker.frame, location)) {
     return;
     }
    */
    
    [self.view endEditing:YES];
    [self hidePicker];
}



- (void)engineeringModeGestureRecognizer:(UILongPressGestureRecognizer *)gesture {
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        
        if (self.isEngineeringMode) {
            return;
        }
        
        //[AppDelegate showCompletedHUDWithMessage:@"Engineer Mode"];
        [AppDelegate showSettingsHUDWithMessage:@"Engineer Mode"];
        
        self.isEngineeringMode = YES;
        self.domainView.backgroundColor = UIColorFromRGB(0xe5e6e2);
        if (self.listPicker) {
            [self.listPicker hide];
            self.listPicker = nil;
        }
        
        //Показываем при инженерном меню выбор тем
        [self.view endEditing:YES];
        [self selectThemeAction:nil];
    }
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;

}

- (void)loginProcedureWithResponse:(NSDictionary *)response subdomain:(NSString *)subDomain login:(NSString *)login{
    
    
    NSString *uidString;
    NSString *aclString;
    NSString *vString;
    NSString *appString;
    NSString *apiVersionString;
    NSString *betaString;
    NSString *j3String;
    
    @try {
        uidString = [response valueForKeyPath:@"result.aid"];
        aclString = [response valueForKeyPath:@"result.acl"];
        vString = [response valueForKeyPath:@"result.v"];
        appString = [response valueForKeyPath:@"result.app"];
        apiVersionString = [response valueForKeyPath:@"result.api"];
        betaString = [response valueForKeyPath:@"result.beta"];
        j3String = [response valueForKeyPath:@"result.version_system"];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception. Reason: %@", exception.reason);
        NSLog(@"Для parent'а неверный response result");
    }
    
    if (![uidString isKindOfClass:[NSString class]] || ![aclString isKindOfClass:[NSString class]]) {
        Alert(@"Error", @"Wrong user");
        self.submitButton.enabled=YES;
        return;
    }
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    
    int uid = [uidString intValue];
    int acl = [aclString intValue];
    BOOL v = [vString boolValue];
    BOOL appDenied = appString && [appString intValue] == 0 ? YES : NO;
    NSArray *unavailableSections = [response valueForKeyPath:@"result.section_block"];
    NSString *unavailableAlert = [response valueForKeyPath:@"result.section_block_message"];
    
    [NetworkManager sharedInstance].serverApiVersion = apiVersionString;
    
    //Доступ на данном сервере специально запрещен
    if (appDenied) {
        Alert(@"Error", @"Sorry, your subscription plan does not support the use of the mobile app");
        self.submitButton.enabled=YES;
        return;
    }
    
    //Приложение не поддерживает версию апи на сервере. Версия должна быть ниже
    if (apiVersionString) {
        if (!([[API_VERSION_THRESHOLD shortenedVersionNumberString] compare:[apiVersionString shortenedVersionNumberString] options:NSNumericSearch] == NSOrderedDescending)) {
            //!(apiVersionString < API_VERSION_THRESHOLD)
            Alert(@"Error", @"Please update your application to the latest version");
            self.submitButton.enabled=YES;
            return;
            
        }
    }
    
    
    //Доступ для буккипинга запрещен
    if (acl == 5) {
        Alert(@"Error", @"No Access");
        self.submitButton.enabled=YES;
        return;
    }
    
    if (betaString && [betaString isKindOfClass:[NSNumber class]] && [betaString intValue] == 1) {
        [NetworkManager sharedInstance].isBeta = YES;
    }
    else {
        [NetworkManager sharedInstance].isBeta = NO;
    }
    
    if (j3String && [j3String isKindOfClass:[NSString class]] && [j3String isEqualToString:@"j3"]) {
        [NetworkManager sharedInstance].isJ3 = YES;
    }
    else {
        [NetworkManager sharedInstance].isJ3 = NO;
    }
    
    
    [[NetworkManager sharedInstance] updateSessionTimer];
    
    [NetworkManager sharedInstance].baseUrl=subDomain;
    
    [NetworkManager sharedInstance].token=[response valueForKeyPath:@"result.access_token"];
    
    //to Igor. Актуализация листа
    //****
    [DataManager sharedInstance].adminId = uid;
    [DataManager sharedInstance].adminAccessLevel = acl;
    [NetworkManager sharedInstance].token=[response valueForKeyPath:@"result.access_token"];
    [NetworkManager sharedInstance].isScottish = v;
    [[NetworkManager sharedInstance]getAdminWithId:[DataManager sharedInstance].adminId completion:^(id responseAdmin, NSError *error) {
        [DataManager sharedInstance].adminInfo= [NSMutableDictionary dictionaryWithDictionary:[responseAdmin valueForKey:@"admin_info"]];
    }];
    
    [NetworkManager sharedInstance].videoServerHost = nil;
    NSLog(@"admin id :%d",[DataManager sharedInstance].adminId);
    
    //****
    
    User *user = [[UserManager sharedManager] addUserWithUserName:login uid:uid subDomain:subDomain acl:acl store:YES isSuccessfullyLogged:YES];
    //Для парента заполняем доступные секции
    if (user.userType == UserTypeParent) {
        NSDictionary *options = nil;
        NSString *firstName;
        NSString *lastName;
        NSNumber *parentNum;
        @try {
            options = [response valueForKeyPath:@"result.additional_info.options"];
            firstName = [response valueForKeyPath:@"result.additional_info.first_name"];
            lastName = [response valueForKeyPath:@"result.additional_info.last_name"];
            parentNum = [response valueForKeyPath:@"result.parent_num"];
            self.submitButton.enabled=YES;
        }
        @catch (NSException *exception) {
            NSLog(@"Exception. Reason: %@", exception.reason);
            NSLog(@"Для parent'а неверный response result");
        }
        [user fillUserOptionsWithDict:options]; //проверка внутри
        user.firstName = firstName;
        user.lastName = lastName;
        
        if (parentNum && [parentNum isKindOfClass:[NSNumber class]]) {
            user.parentNum = @([parentNum intValue] + 1);
        }
        
        
        
        [[UserManager sharedManager] saveLocalUsers];
        /*
         NSDictionary *additionalInfo = [response objectForKey:@"additional_info"];
         if (additionalInfo && [additionalInfo isKindOfClass:[NSDictionary class]]) {
         NSDictionary *options = [additionalInfo objectForKey:@"options"];
         [user fillUserOptionsWithDict:options]; //проверка внутри
         }
         */
    }
    
    if (user.userType == UserTypeAdmin && [NetworkManager sharedInstance].isJ3) {
        NSDictionary *options = nil;
        @try {
            options = [response valueForKeyPath:@"result.options"];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception. Reason: %@", exception.reason);
            NSLog(@"Для admin неверный response result");
        }
        [user updateAccessLevelWithDict:options]; //проверка внутри
    }
    
    
    if ([NetworkManager sharedInstance].isWhiteLabel) {
        user.companyName = self.whiteLabelCompanyName;
        [[UserManager sharedManager] saveLocalUsers];
    }
    
    [[AccessTimeManager sharedInstance] disableTimer];
    if (user.userType == UserTypeAdmin && user.acl != 1) {
        //time access
        NSNumber *timeLeft = [response valueForKeyPath:@"result.left_time"];
        if (timeLeft && [timeLeft isKindOfClass:[NSNumber class]]) {
            [[AccessTimeManager sharedInstance] startWithSeconds:[timeLeft longLongValue]];
        }
    }
    
    [[UnavailableSectionManager sharedInstance] updateWithArray:unavailableSections andMessage:unavailableAlert];
    
    
    //Переход в register для парентсайда при наличии неподписанных часов
    //[NetworkManager sharedInstance].registerLoginArray = [response valueForKeyPath:@"result.register.ActiveChildrenList"];
    [[RegisterForceManager sharedManager] updateWithRegisterArray:[response valueForKeyPath:@"result.register.ActiveChildrenList"]];
    //Старт менеджера видео загрузок
    
    
    //Для добавленного юзера запускаем проверку пина (если он уже не в проверенном списке)
    //TODO: передавать ссылкой 'new' добавлен или нет, чтобы сразу знать проверены ли пины
    if ([[UserManager sharedManager] isAllUsersRecivePinAvailability]) {
        [self userPinCheked];
    }
    
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self
           selector:@selector(userPinChekedGetNotification:)
               name:AllUsersHasBeenRecivePinAvailabilityNotification
             object:nil];
    self.submitButton.enabled=YES;
        
}

#pragma mark - Methods


- (void)showPicker {
    
    if (self.isPickerShow) {
        return;
    }
    
    NSIndexPath *insertIndexPath = [NSIndexPath indexPathForRow:3 inSection:0];
    
    [self.rows insertObject:kPDPicker atIndex:3];
    
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[insertIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    
    if (self.activeUser) {
        [self.picker selectRow:[[[UserManager sharedManager] users] indexOfObject:self.activeUser] inComponent:0 animated:NO];
    }
    
    self.isPickerShow = YES;

}

- (void)hidePicker {
    
    if (!self.isPickerShow) {
        return;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
    
    [self.rows removeObjectAtIndex:3];
    
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];

     self.isPickerShow = NO;
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {

    
    
   /*
   if (textField.tag == ALERT_DOMAIN_TEXT_FIELD || ([textField isEqual:self.domainTextField] && [textField.text isEqual:@".mybabysdays.com"])) {
       
       UITextPosition *beginning = [textField beginningOfDocument];
       [textField setSelectedTextRange:[textField textRangeFromPosition:beginning
                                                             toPosition:beginning]];
   }
   */
    
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if ([textField isEqual:self.loginTextField]) {
        
        if ([[[UserManager sharedManager] users] count] <= 1) { //не из чего выбирать
            return YES;
        }
        
        /*
        if ([[[UserManager sharedManager] users] count] == 1) {
            User *user = (User *)[[[UserManager sharedManager] users] firstObject];
            if ([textField.text isEqualToString:user.userName]) {
                return YES;
            }
        }
        */
        
        if (!self.isPickerShow) {
            [self showPicker];
            [self.view endEditing:YES]; //убираем клавитуру от других textFields
            return NO; //убираем клавиатуру для выбранного textField
        } else {
            [self hidePicker];
        }
    }
    
    
    if ([textField isEqual:self.domainTextField]) {
        if ([NetworkManager sharedInstance].isWhiteLabel && self.isEngineeringMode == NO) {
            
            //Если домен всего 1 и он уже выбран
            DomainCompanyModel *model = [[NetworkManager sharedInstance].whiteLabelSubdomains firstObject];
            if ([[NetworkManager sharedInstance].whiteLabelSubdomains count] == 1 && [self.domainTextField.text isEqualToString:model.companyName]) {
                return NO;
            }
            
            [self.view endEditing:YES];
            
            NSMutableArray *companyNames = [NSMutableArray array];
            NSMutableArray *domains = [NSMutableArray array];
            for (DomainCompanyModel *model in [NetworkManager sharedInstance].whiteLabelSubdomains) {
                [companyNames addObject:model.companyName];
                [domains addObject:[NSString stringWithFormat:@"%@.%@", model.subdomain, model.domain]];
            }
            
            NSInteger selectedIndex = 0;
            NSInteger selectedIndexCompany = [companyNames indexOfObject:self.domainTextField.text];
            NSInteger selectedIndexSubdomain = [domains indexOfObject:self.whiteLabelFullDomain];
            
            if (selectedIndexCompany != NSNotFound) {
                selectedIndex = selectedIndexCompany;
            }
            else if (selectedIndexSubdomain != NSNotFound) {
                selectedIndex = selectedIndexSubdomain;
            }
            
            
            self.listPicker = [[ListPicker alloc] init];
            self.listPicker.dataArray = companyNames;
            self.listPicker.selectedIndex = (int)selectedIndex;
            
            [self.listPicker setHandler:^(NSString *value, int index) {
                
                DomainCompanyModel *selectedModel = [[NetworkManager sharedInstance].whiteLabelSubdomains objectAtIndex:index];
                [NetworkManager sharedInstance].whiteLabelDomain = selectedModel.domain;
                self.domainTextField.text = selectedModel.companyName;
                self.whiteLabelCompanyName = selectedModel.companyName;
                self.whiteLabelFullDomain = [NSString stringWithFormat:@"%@.%@", selectedModel.subdomain, selectedModel.domain];
                
            }];
            [self.listPicker show];
        return NO;
        }
        
        
    }
    
    [self hidePicker]; //при вводе в пароля/домена - picker убирается
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    if  ([textField isEqual:self.domainTextField]) {
        [self.loginTextField becomeFirstResponder];
    }else if ([textField isEqual:self.loginTextField]) {
        [self.passwordTextField becomeFirstResponder];
    } else if ([textField isEqual:self.passwordTextField]) {
        [textField resignFirstResponder];
        [self submitAction:nil];
    }
    
    return YES;
}

/*
- (NSString *)extractSubdomainFromString:(NSString *)inputString {
    
    //NSString *searchedString = @"domain-name.tld.tld2";
    NSRange   searchedRange = NSMakeRange(0, [inputString length]);
    NSString *pattern = @"(?:www\\.)?((?!-)[a-zA-Z0-9-]{2,63}(?<!-))\\.?((?:[a-zA-Z0-9]{2,})?(?:\\.[a-zA-Z0-9]{2,})?)";
    NSError  *error = nil;
    
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
    NSArray* matches = [regex matchesInString:inputString options:0 range: searchedRange];
    for (NSTextCheckingResult* match in matches) {
        NSString* matchText = [inputString substringWithRange:[match range]];
        //NSLog(@"match: %@", matchText);
        NSRange group1 = [match rangeAtIndex:1];
        NSRange group2 = [match rangeAtIndex:2];
        //return [inputString substringWithRange:group1];
        NSLog(@"group1: %@", [inputString substringWithRange:group1]);
        NSLog(@"group2: %@", [inputString substringWithRange:group2]);
    }
    
    return @"";
    
}
*/


- (NSString *)extractSubdomainFromString:(NSString *)inputString {
    
    NSURL *url = [NSURL URLWithString:inputString];
    if (url) {
        NSArray *hostArray = [[url host] componentsSeparatedByString:@"."];
        if ([hostArray count] >= 3) {
            return [hostArray firstObject];
        }
    }
    
    
    return @"";
    
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {


    if ([textField isEqual:self.domainTextField]) {
        
        if (self.isEngineeringMode) {
            return YES;
        }
        
        
        if ([string isEqualToString:[UIPasteboard generalPasteboard].string]) {
            textField.text = [self extractSubdomainFromString:string];
            return NO;
        }
        else {
        
        
        
        
        
            //backspace
            if ([string length] == 0) {
                return YES;
            }
            
            
            NSLog(@"string: %@", string);
            
            NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_-"];
            
            NSRange range = [string rangeOfCharacterFromSet:characterSet];
            if (range.location != NSNotFound) {
                return YES;
            }
            else {
                NSLog(@"the string contains illegal characters");
                Alert(@"Warning", @"Enter your personal domain name ONLY, this is the domain name you requested when you registered or the domain name your child care provider has provided you with.");
                return NO;
            }
            
            
            
    //        [textField setText:[textField.text stringByReplacingCharactersInRange:range withString:string]];
            
            //[self updateDomainColor];
            
            
            
            return YES;
        }
    }
    
    return YES;
    
    
    
}

#pragma mark - Utils

- (void)updateDomainColor {
    
    
    
    
    NSString *domainText = self.domainTextField.text;
    
    if (!domainText) {
        return;
    }
    
    
    
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:domainText];
    
    NSRange fullStringRange = NSMakeRange(0, string.length);
    
    
    if (fullStringRange.location == NSNotFound) {
        return;
    }
    
    [string addAttribute:NSForegroundColorAttributeName
                   value:[UIColor blackColor]
                   range:fullStringRange];
    
    
    NSRange range = [domainText rangeOfString:@".mybabysdays.com"];
    
    
    [string addAttribute:NSForegroundColorAttributeName
                   value:[UIColor grayColor]
                   range:range];
    
    self.domainTextField.attributedText = string;
    
    
    
}


//Не используется
- (void)domainTextFieldDidChange:(id)sender {
    
    if (!self.domainTextField) {
        return;
    }
    
    UITextPosition *beginning = self.domainTextField.selectedTextRange.start;
    UITextPosition *finishing = self.domainTextField.selectedTextRange.end;
    
    [self updateDomainColor];
    
    [self.domainTextField setSelectedTextRange:[self.domainTextField textRangeFromPosition:beginning
                                                                                toPosition:finishing]];
    
    
}


#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [[[UserManager sharedManager] users] count];
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    User *user = [[[UserManager sharedManager] users] objectAtIndex:row];
    return user.userName;
}
#warning TODO: если 1 row, то по клику на это row подтверждать

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    User *user = [[[UserManager sharedManager] users] objectAtIndex:row];
    
    [self updateTextFieldsWithUser:user];
    self.activeUser = user;
    
}

/*
- (void)pickerViewTapGestureRecognized:(UITapGestureRecognizer*)gestureRecognizer
{
    NSLog(@"tap in picker");

    
    CGPoint touchPoint = [gestureRecognizer locationInView:gestureRecognizer.view.superview];
    NSLog(@"touch point %@", NSStringFromCGPoint(touchPoint));
    CGRect frame = self.picker.frame;
    CGRect selectorFrame = CGRectInset( frame, 0.0, self.picker.bounds.size.height * 0.85 / 2.0 );
    
    if( CGRectContainsPoint( selectorFrame, touchPoint) )
    {
        NSLog( @"Selected Row: %li", (long)[self.picker selectedRowInComponent:0] );
    }
}
*/

#pragma mark - NSNotifications

- (void)userPinChekedGetNotification:(NSNotification *)userInfo {
    [self userPinCheked];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -

- (void)userPinCheked {
    NSLog(@"пины проверены");

    if ([[UserManager sharedManager] loggedUser].havePin) {
        
        [self setupTouchIdOrShowMenu];
        
    
    } else {
        [self performSegueWithIdentifier:@"setupPinSegue" sender:self];
    }
}


- (void)setupTouchIdOrShowMenu {
    
    VPBiometricAuthenticationFacade *biometricFacede = [[VPBiometricAuthenticationFacade alloc] init];
    
    BOOL isNeedSetupTouchId = [biometricFacede isAuthenticationAvailable] && ![UserManager sharedManager].isUsersWithTouchId && [[UserManager sharedManager] loggedUser].isNewUser;
    BOOL isFirstLaunchAfterUpdate = [biometricFacede isAuthenticationAvailable] && ![UserManager sharedManager].isUsersWithTouchId && [AppDelegate isFirstLaunchAfterTouchIdUpdate];
    //isNeedSetupTouchId = NO;
    //isFirstLaunchAfterUpdate = NO;
    if (isNeedSetupTouchId || isFirstLaunchAfterUpdate) {
        
        PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:[Utils isFaceIdSupported] ? @"Face ID" : @"Touch ID" message:[Utils isFaceIdSupported] ? @"Do you want to enable Face ID?" : @"Do you want to enable Touch ID?" preferredStyle:PSTAlertControllerStyleAlert];
          
        [alert addAction:[PSTAlertAction actionWithTitle:@"Yes" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
            
            [self setupTouchId];
            
            
        }]];
        
        [alert addAction:[PSTAlertAction actionWithTitle:@"Skip" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
            //User.skip
            [self showMenu];
            
            
        }]];
        
        [alert showWithSender:self controller:self animated:YES
                   completion:nil];
        [AppDelegate appLaunchedAfterTouchIdUpdate];
        
    }
    else {
        [self showMenu];
    }
}


- (void)setupTouchId {
    
    LAContext *context = [[LAContext alloc] init];
    context.localizedFallbackTitle = @"";
    
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:[Utils isFaceIdSupported] ? @"Unlock using Face ID" : @"Touch the Home button to login"
                          reply:^(BOOL success, NSError *error) {
                              
                              if (error) {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{

                                      PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Error" message:[Utils isFaceIdSupported] ? @"Face ID authentication failed" : @"Touch ID authentication failed" preferredStyle:PSTAlertControllerStyleAlert];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Retry" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          
                                          [self setupTouchId];
                                          
                                      }]];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Skip" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          [self showMenu];
                                          
                                      }]];
                                      
                                      [alert showWithSender:nil controller:self animated:YES
                                                 completion:nil];
                                  });
                                  return;
                                  
                              }
                              
                              if (success) {
                                  
                                  User *user = [[UserManager sharedManager] lastLoggedUser];
                                  user.touchIdToken = [AppDelegate randomString];
                                  
                                  
                                  [[NetworkManager sharedInstance] createTouchIdToken:user.touchIdToken forUser:user.uid isParent:user.userType == UserTypeParent ? YES : NO withCompletion:^(id response, NSError *error) {
                                      
                                      if (response)
                                      {
                                          if([[response valueForKeyPath:user.userType == UserTypeAdmin ? @"result.complete" : @"data.result.complete"]intValue]==1)
                                          {
                                              user.isTouchId = YES;
                                              [self showMenu];
                                          }
                                          else
                                          {
                                              Alert(@"Error",[response valueForKeyPath:user.userType == UserTypeAdmin ? @"result.msg" : @"data.result.msg"] );
                                              [self showMenu];
                                              
                                          }
                                      }
                                      else if(error)
                                      {
                                          Alert(@"Error", @"Error happened while auth");
                                          [self showMenu];
                                      }
                                  }];
                                  
                              } else {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                  
                                      PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Error" message:@"You are not the device owner." preferredStyle:PSTAlertControllerStyleAlert];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Retry" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          
                                          [self setupTouchId];
                                          
                                      }]];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Skip" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          [self showMenu];
                                          
                                      }]];
                                      
                                      [alert showWithSender:nil controller:self animated:YES
                                                 completion:nil];
                                
                                      });
                              }
                              
                          }];
        
    }/* else {
      
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
      message:@"Your device cannot authenticate using TouchID."
      delegate:nil
      cancelButtonTitle:@"Ok"
      otherButtonTitles:nil];
      [alert show];
      
      }
      */
}


- (void)loginByTouchId {
    
    LAContext *context = [[LAContext alloc] init];
    context.localizedFallbackTitle = @"";
    
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:[Utils isFaceIdSupported] ? @"Unlock using Face ID" : @"Touch the Home button to login"
                          reply:^(BOOL success, NSError *error) {
                              
                              if (error) {
                                  /*
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      
                                      PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Error" message:@"Touch ID authentication failed" preferredStyle:PSTAlertControllerStyleAlert];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Retry" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          
                                          [self loginByTouchId];
                                          
                                      }]];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          
                                          
                                      }]];
                                      
                                      [alert showWithSender:nil controller:self animated:YES
                                                 completion:nil];
                                  });
                                  */
                                  return;
                                  
                              }
                              
                              if (success) {
                                  
                                //User *user = [[UserManager sharedManager].sortedUsers firstObject];
                                  User *user = [[UserManager sharedManager] getUserWithTouchId];
                                  if (!user) {
                                      NSString *msg = [Utils isFaceIdSupported] ? @"Face ID Failure" : @"Touch ID Failure";
                                      Alert(@"Error", msg);
                                      return;
                                  }
                                  [[NetworkManager sharedInstance] loginWithTouchIdString:user.touchIdToken forUser:user.uid subdomain:user.subDomain withCompletion:^(id response, NSError *error) {
                                      
                                      if (response)
                                      {
                                          if([[response valueForKeyPath:@"result.complete"] intValue]==1)
                                          {
                                              
                                              NSString *iosString;
                                              
                                              @try {
                                                  iosString = [response valueForKeyPath:@"result.ios"];
                                              }
                                              @catch (NSException *exception) {
                                                  NSLog(@"Exception. Reason: %@", exception.reason);
                                                  NSLog(@"Для parent'а неверный response result");
                                              }
                                              
                                              NSNumber *iosUpdate = iosString ? [NSNumber numberWithInt:[iosString intValue]] : nil;
                                              
                                              //iosUpdate = @1;
                                              
                                              //Сервер рекоммендует обновиться
                                              if (iosUpdate && [iosUpdate isEqualToNumber:@1]) {
                                                  //Alert(@"Notice", @"Please update your application to the latest version");
                                                  
                                                  PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning" message:[response valueForKeyPath:@"result.msg"] preferredStyle:PSTAlertControllerStyleAlert];
                                                  
                                                  [alert addAction:[PSTAlertAction actionWithTitle:@"Update" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                                      
                                                      [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[WL appStoreLink]]];
                                                      [self loginProcedureWithResponse:response subdomain:user.subDomain login:user.userName];
                                                  }]];
                                                  
                                                  [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:^(PSTAlertAction *action) {
                                                      [self loginProcedureWithResponse:response subdomain:user.subDomain login:user.userName];
                                                      
                                                      
                                                  }]];
                                                  
                                                  
                                                  [alert showWithSender:self controller:self animated:YES completion:nil];
                                                  
                                              }
                                              else {
                                                  [self loginProcedureWithResponse:response subdomain:user.subDomain login:user.userName];
                                              }
                                              
                                          }
                                          else
                                          {
                                              
                                              NSString *iosString;
                                              
                                              @try {
                                                  iosString = [response valueForKeyPath:@"result.ios"];
                                              }
                                              @catch (NSException *exception) {
                                                  NSLog(@"Exception. Reason: %@", exception.reason);
                                                  NSLog(@"Неверный response result");
                                              }
                                              
                                              NSNumber *iosUpdate = iosString ? [NSNumber numberWithInt:[iosString intValue]] : nil;
                                              
                                              if (iosUpdate && [iosUpdate isEqualToNumber:@2]) {
                                                  //Сервер настоятельно рекоммендует обновиться
                                                  //Alert(@"Error", @"Please update your application to the latest version");
                                                  //Alert(@"Error",[response valueForKeyPath:@"result.msg"] );
                                                  
                                                  
                                                  PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning" message:[response valueForKeyPath:@"result.msg"] preferredStyle:PSTAlertControllerStyleAlert];
                                                  
                                                  [alert addAction:[PSTAlertAction actionWithTitle:@"Update" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                                      
                                                      [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[WL appStoreLink]]];
                                                      
                                                  }]];
                                                  
                                                  [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:nil]];
                                                  
                                                  [alert showWithSender:self controller:self animated:YES completion:nil];
                                                  
                                                  //self.submitButton.enabled=YES;
                                                  
                                                  return;
                                                  
                                              }
                                              else {
                                                  
                                                  
                                                  //для блокированных систем выводим msg в заголовок, text в тело сообщения. +tel по кнопке звонка
                                                  NSString *msgString = [response valueForKeyPath:@"result.msg"];
                                                  NSString *textString = [response valueForKeyPath:@"result.text"];
                                                  NSString *telString = [response valueForKeyPath:@"result.tel"];
                                                  
                                                  if (msgString && textString && telString) {
                                                      
                                                      PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:msgString message:textString preferredStyle:PSTAlertControllerStyleAlert];
                                                      
                                                      [alert addAction:[PSTAlertAction actionWithTitle:@"OK" style:PSTAlertActionStyleDefault handler:nil]];
                                                      
                                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Call" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                                          
                                                          NSString *tel = [NSString stringWithFormat:@"tel:%@", telString];
                                                          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
                                                          
                                                      }]];
                                                      
                                                      [alert showWithSender:self controller:self animated:YES completion:nil];
                                                      
                                                  }
                                                  else {
                                                      Alert(@"Error",[response valueForKeyPath:@"result.msg"]);
                                                  }
                                                  
                                                  //self.submitButton.enabled=YES;
                                              }
                                              
                                              
                                              
                                              
                                              
                                              
                                          }
                                      }
                                      else if(error)
                                      {
                                          
                                          NSError *underlyingError = error.userInfo[NSUnderlyingErrorKey];
                                          if ([[[underlyingError userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode] == 404) {
                                              Alert(@"Error", @"Non-existent domain name. Please check the used data.");
                                          }
                                          else {
                                              Alert(@"Error", error.localizedDescription);
                                          }
                                          
                                          self.submitButton.enabled=YES;
                                      }
                                  }];
                                  
                              } else {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      
                                      PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Error" message:@"You are not the device owner." preferredStyle:PSTAlertControllerStyleAlert];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Retry" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          
                                          [self loginByTouchId];
                                          
                                      }]];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          //[self showMenu];
                                          
                                      }]];
                                      
                                      [alert showWithSender:nil controller:self animated:YES
                                                 completion:nil];
                                   
                                  });
                                  
                              }
                              
                          }];
        
    }/* else {
      
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
      message:@"Your device cannot authenticate using TouchID."
      delegate:nil
      cancelButtonTitle:@"Ok"
      otherButtonTitles:nil];
      [alert show];
      
      }
      */
}



- (void)showMenu {
    
    if ([[UserManager sharedManager] loggedUser].userType == UserTypeAdmin) {
        [self performSegueWithIdentifier:@"@Main_iPhone" sender:self];
    } else {
        [self performSegueWithIdentifier:@"@Main-ParentSide_iPhone" sender:self];
    }
    
}


- (void)setButton:(UIButton *)button withAlignment:(NSTextAlignment)alignmentStyle {
    
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:alignmentStyle];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    
    //UIFont *font = [UIFont systemFontOfSize:12.5f];
    UIFont *font = [UIFont systemFontOfSize:12.6f];
    
    NSDictionary *dict = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
                            NSFontAttributeName:font,
                            NSParagraphStyleAttributeName:style,
                            NSForegroundColorAttributeName:[UIColor whiteColor]};
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:button.titleLabel.text    attributes:dict]];

    [button setAttributedTitle:attString forState:UIControlStateNormal];
    [[button titleLabel] setNumberOfLines:0];
    [[button titleLabel] setLineBreakMode:NSLineBreakByWordWrapping];

    
}

/*
- (void)pickerControlTapped
{
    NSLog(@"тапед");
    [self.picker selectRow: rand()% 1
              inComponent: 0
                 animated: YES];
}
*/
@end
