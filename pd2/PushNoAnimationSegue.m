//
//  PushNoAnimationSegue.m
//  pd2
//
//  Created by i11 on 08/02/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "PushNoAnimationSegue.h"

@implementation PushNoAnimationSegue

-(void) perform{
    
    [[[self sourceViewController] navigationController] pushViewController:[self destinationViewController] animated:NO];

}

@end
