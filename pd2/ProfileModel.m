//
//  ProfileModel.m
//  pd2
//
//  Created by Andrey on 05/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "ProfileModel.h"

@implementation ProfileModel



//From child
/*
 {
 "id": "269",
 "chid": "63",
 "idname": "id17",
 "helperTitle": "Ethnic Origin",
 "content": "WHB - White - British;WHR - White - Irish;WHA - Any Other White Background;MWB - White and Black Caribbean;MBA - White and Black African;MWA - White and Asian;MOT - Any Other Mixed Background;AIN - Indian;APK - Pakistani;ABA - Bangladeshi;AAO - Any Other Asian Background ;BLB - Caribbean;BLF - African;BLG - Any Other Black Background;CHE - Chinese;OEO - Any Other Ethnic Group",
 "title": "WHA - Any Other White Background",
 "type": "2",
 "ordering": "5"
 }
 */

//separated
/*
{
content = "WHB - White - British;WHR - White - Irish;WHA - Any Other White Background;MWB - White and Black Caribbean;MBA - White and Black African;MWA - White and Asian;MOT - Any Other Mixed Background;AIN - Indian;APK - Pakistani;ABA - Bangladeshi;AAO - Any Other Asian Background ;BLB - Caribbean;BLF - African;BLG - Any Other Black Background;CHE - Chinese;OEO - Any Other Ethnic Group";
id = 17;
idname = id17;
ordering = 5;
published = 0;
    title = "Ethnic Origin";
type = 2;
}
*/

- (instancetype)initWithDict:(NSDictionary *)dict isFromChild:(BOOL)isFromChild {
    
    self = [super init];
    if (self) {
        
        
        NSNumber *type = [self safeNumber:dict[@"type"]];
        if (type) {
            if ([type intValue] == 0) {
                self.profileType = ProfileModelTypeMultiLine;
            }
            else if ([type intValue] == 1) {
                self.profileType = ProfileModelTypeSingleLine;
            }
            else if ([type intValue] == 2) {
                self.profileType = ProfileModelTypeList;
            }
        }
        else {
            self.profileType = ProfileModelTypeSingleLine;
        }
        
        self.profileId = [self safeString:dict[@"idname"]];
        
        if (isFromChild) {
            self.helperTitle = [self safeStringAndDecodingHTML:dict[@"helperTitle"]];
            self.value = [self safeString:dict[@"title"]];
        }
        else {
            self.helperTitle = [self safeStringAndDecodingHTML:dict[@"title"]];
        }
        
        self.order = [self safeInt:dict[@"order"]];
        
        

        if (self.profileType == ProfileModelTypeList) {
            NSString *contentString = [self safeString:dict[@"content"]];
            self.items = [contentString componentsSeparatedByString:@";"];
        }
        
        

        
        
    }
    return self;
    
}


- (BOOL)isExist {
    
    
    if (self.profileType == ProfileModelTypeMultiLine || self.profileType == ProfileModelTypeSingleLine) {
        if (self.profileId && self.helperTitle) {
            return YES;
        }
    }
    else if (self.profileType == ProfileModelTypeList) {
        if (self.profileId && self.helperTitle && [self.items count]) {
            return YES;
        }
    }
    
    
    return NO;
}




@end
