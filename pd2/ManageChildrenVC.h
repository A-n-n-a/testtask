//
//  ManageChildrenVC.h
//  pd2
//
//  Created by User on 09.07.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "Pd2MenuViewController.h"

@interface ManageChildrenVC : Pd2MenuViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@end
