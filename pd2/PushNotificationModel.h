//
//  PushNotificationModel.h
//  pd2
//
//  Created by Andrey on 04/10/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXBaseModel.h"


typedef NS_ENUM(NSUInteger, PushNotificationAction) {
    PushNotificationViewDiary,
    PushNotificationViewDiaryComment,
    PushNotificationRegisterIn,
    PushNotificationRegisterOut,
    PushNotificationPermissions,
    PushNotificationPermissionsViewSign,
    PushNotificationPolicies,
    PushNotificationPoliciesViewSign,
    PushNotificationAccidentView,
    PushNotificationAccidentViewSign,
    PushNotificationAlert,
    PushNotificationShortTermAdded,
    PushNotificationShortTermSign,
    PushNotificationShortTermDose,
    PushNotificationShortTermComplete,
    PushNotificationLongTermAdded,
    PushNotificationLongTermSign,
    PushNotificationLongTermDose,
    PushNotificationLongTermComplete,
    PushNotificationPhotoGallery,
    PushNotificationVideoGallery,
    PushNotificationNextStepAddedByAdmin,
    PushNotificationNextStepAddedByParent,
    PushNotificationNextStepCompleted,
    PushNotificationObservationAddedByAdmin,
    PushNotificationObservationAddedByParent,
    PushNotificationProgressAchievementDate,
    PushNotificationProgressPhoto,
    PushNotificationProgressVideo,
    PushNotificationAuthorisedSignatureRequired,
    PushNotificationAuthorisedPhotoRequired,
    PushNotificationAuthorisedSignatureAndPhotoRequired,
    PushNotificationAuthorisedSignatureAddedByParent,
    PushNotificationAuthorisedPhotoAddedByParent,
    PushNotificationAuthorisedSignatureAndPhotoAddedByParent,
    PushNotificationNappiesAddWetByAdmin,
    PushNotificationNappiesAddSoiledByAdmin,
    PushNotificationNappiesAddWetByParent,
    PushNotificationNappiesAddSoiledByParent,
    PushNotificationPrivateMessageToAdmin,
    PushNotificationPrivateMessageToParent
};

@interface PushNotificationModel : AXBaseModel


@property (nonatomic, strong) NSString *host;
@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) PushNotificationAction pushAction;
@property (nonatomic, strong) NSNumber *chid;


@property (nonatomic, assign) BOOL isBackground;



//diary
@property (nonatomic, strong) NSNumber *diaryId;
@property (nonatomic, strong) NSString *diaryDate;

//register
//Просто алерт
//@property (nonatomic, strong) NSNumber *day;
//@property (nonatomic, strong) NSNumber *month;
//@property (nonatomic, strong) NSNumber *year;
//@property (nonatomic, strong) NSNumber *registerId;


//Permissions
@property (nonatomic, strong) NSNumber *permissionsId;

//Policies
@property (nonatomic, strong) NSNumber *policiesId;

//Accidents
@property (nonatomic, strong) NSNumber *accidentId;

//Long/Short Term
@property (nonatomic, strong) NSNumber *medicationId;

//Photo/Video Galleries
//@property (nonatomic, strong) NSNumber *month;
//@property (nonatomic, strong) NSNumber *year;
@property (nonatomic, strong) NSArray *mediaIds;


//Progress
@property (nonatomic, strong) NSNumber *nextStepId;
@property (nonatomic, strong) NSNumber *observationId;
@property (nonatomic, strong) NSString *observationType; //obs/note
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *tab;
@property (nonatomic, strong) NSString *rowId;
@property (nonatomic, strong) NSMutableArray *aols;

//Authorised
@property (nonatomic, strong) NSNumber *personId;

//Nappies
@property (nonatomic, strong) NSString *year;
@property (nonatomic, strong) NSString *month;
@property (nonatomic, strong) NSString *day;

//Private Messages
@property (nonatomic, strong) NSNumber *dialogId;

- (instancetype)initWithDict:(NSDictionary *)dict;
- (BOOL)isExist;


@end

