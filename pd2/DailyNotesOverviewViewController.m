//
//  DailyNotesOverviewViewController.m
//  pd2
//
//  Created by Andrey on 24/01/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesOverviewViewController.h"
#import "DailyNotesAddNoteViewController.h"
#import "DailyNotesChildrenNotesListViewController.h"

#import "DailyNotesOverviewNoteCell.h"
#import "DailyNotesOverviewChildrenCell.h"

#import "DailyNotesPendingNoteModel.h"
#import "DailyNotesChildrenModel.h"


#define CHILDREN_SECTIONS_TAG @"childrenSectionTag"

@interface DailyNotesOverviewViewController () <UITextFieldDelegate>

{
    NSString *PDNotesSection;
    NSString *PDChildrenSection;
    NSString *PDSubChildrenSection;

    
    NSString *PDNotesHeader;
    NSString *PDChildrenHeader;
    
    NSString *PDAddButtonCell;
    NSString *PDNoteCell;
    NSString *PDChildrenCell;
    
    NSString *PDSpacer35Cell;
    NSString *PDSpacer45Cell;
    
}

@property (nonatomic, strong) NSMutableDictionary *roomsAndChildDictionary;
@property (nonatomic, strong) NSMutableDictionary *roomsAndChildFilteredDictionary;
@property (nonatomic, strong) NSString *filterString;
@property (nonatomic, weak) UITextField *filterTextField;


@end

@implementation DailyNotesOverviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initRows];
    [self initLabels];
    AXSection *topSection = [[AXSection alloc] initWithType:PDTopSection];
    [topSection addRow:[self.sections genRowWithType:PDInfoCell]];
    [topSection addRow:[self.sections genRowWithType:PDAddButtonCell]];
    //[topSection addRow:[self.sections genRowWithType:PDSpoilerCell]];
    //[topSection addRow:[self.sections genRowWithType:PDFilterCell]];
    
    [self.sections addSection:topSection];
    
    [self refreshData];
    
    
    AXSection *bottomSection = [[AXSection alloc] initWithType:PDBottomSection];
    [bottomSection addRow:[self.sections genRowWithType:PDAddButtonCell]];
    
    [self.sections addSection:bottomSection];
    
    self.topTitle = @"Daily Notes";
    self.view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];

    
}


- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if ([[DataStateManager sharedManager] isChanged:DataStateDailyNotes forViewController:self]) {
        
        [self refreshData];
        
    }
    
    
}



- (void)initRows {
    [super initRows];
    
    PDNotesSection = @"PDNotesSection";
    PDChildrenSection = @"PDChildrenSection";
    PDSubChildrenSection = @"PDSubChildrenSection";
    
    PDNotesHeader = @"PDNotesHeader";
    PDChildrenHeader = @"PDChildrenHeader";
    
    PDAddButtonCell = @"PDAddButtonCell";
    PDNoteCell = @"PDNoteCell";
    PDChildrenCell = @"PDChildrenCell";
    
    PDSpacer35Cell = @"PDSpacer35Cell";
    PDSpacer45Cell = @"PDSpacer45Cell";
    
    
    [self.sections rememberType:PDInfoCell withEstimatedHeight:180.f identifier:@"infoCell"];
    
    [self.sections rememberType:PDAddButtonCell withHeight:68.f identifier:@"addButtonCell"];
    
    [self.sections rememberType:PDNotesHeader withHeight:44.f identifier:@"pendingHeaderCell"];
    [self.sections rememberType:PDNoteCell withEstimatedHeight:173.f identifier:@"noteCell"];

    [self.sections rememberType:PDChildrenHeader withHeight:44.f identifier:@"childrenHeaderCell"];
    [self.sections rememberType:PDChildrenCell withEstimatedHeight:173.f identifier:@"childrenCell"];
    
    [self.sections rememberType:PDSpacer35Cell withEstimatedHeight:35.f identifier:@"spacerCell"];
    [self.sections rememberType:PDSpacer45Cell withEstimatedHeight:45.f identifier:@"spacerCell"];
    
    [self.infoCells addObject:PDInfoCell];
    [self.contentFitCells addObject:PDChildrenCell];
    [self.contentFitCells addObject:PDNoteCell];

}


#pragma mark - Configure cells

- (UIView *)viewForPDNotesHeader:(AXHeader *)header inSection:(AXSection *)section {
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:header.identifier];
    UIView *view = cell.contentView;
    
    UIImageView *arrowImageView = (UIImageView *)[view viewWithTag:HEADER_ARROW_IMAGE_VIEW_TAG];
    [self updateHeaderArrowImage:arrowImageView withState:section.isHide];
    
    
    UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
    
    
    [self addBorder:PDBorderTopAndBottom toView:containerView forToolbar:NO];
    
    
    return view;
    
}

- (UIView *)viewForPDChildrenHeader:(AXHeader *)header inSection:(AXSection *)section {
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:header.identifier];
    UIView *view = cell.contentView;
    
    UIImageView *arrowImageView = (UIImageView *)[view viewWithTag:HEADER_ARROW_IMAGE_VIEW_TAG];
    [self updateHeaderArrowImage:arrowImageView withState:section.isHide];
    
    
    UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
    
    
    [self addBorder:PDBorderTopAndBottom toView:containerView forToolbar:NO];
    
    
    return view;
    
}


- (UIView *)viewForPDRoomHeader:(AXHeader *)header inSection:(AXSection *)section {
    
    RoomHeaderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:header.identifier];
    UIView *view = cell.contentView;

    
    [self updateHeaderArrowImage:cell.arrowImageView withState:section.isHide];
    
    if (header.title) {
        [cell updateWithRoomName:header.title];
        [cell updateWithRoomImagePath:[[DataManager sharedInstance] roomImagePathWithTitle:header.title]];
    }
    
    
    UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
    [self addBorder:PDBorderBottom toView:containerView forToolbar:NO];
    
    
    return view;
    
}

- (UITableViewCell *)configurePDFilterCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    FilterCell *cell = (FilterCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        self.filterTextField = cell.filterTextField;
        
        cell.filterTextField.delegate = self;
        cell.filterTextField.text = [self.filterString copy];
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}


- (UITableViewCell *)configurePDNoteCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    DailyNotesOverviewNoteCell *cell = (DailyNotesOverviewNoteCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        DailyNotesPendingNoteModel *model = row.model;
        
        cell = [self offscreenRenderingCell:cell withRow:row];
        
        [cell activityIndicatorSetStatus:row.isForRemoval];
        
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        
        [self addBorder:PDBorderTop toView:cell.toolbarView forToolbar:NO];
        [self addBorder:PDBorderRight toView:cell.toolbar1View forToolbar:YES];
        [self addBorder:PDBorderRight toView:cell.toolbar2View forToolbar:YES];
        [self addBorder:PDBorderTopAndBottomDynamic toView:containerView forToolbar:NO];
        
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}



- (UITableViewCell *)configurePDChildrenCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    DailyNotesOverviewChildrenCell *cell = (DailyNotesOverviewChildrenCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        DailyNotesChildrenModel *model = row.model;
        
        cell = [self offscreenRenderingCell:cell withRow:row];
        [cell updateImageWithModel:model];
        
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        
        [self addBorder:PDBorderTop toView:cell.toolbarView forToolbar:NO];
        [self addBorder:PDBorderTopAndBottomDynamic toView:containerView forToolbar:NO];

        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}




#pragma mark - OffScreen rendering


- (id)offscreenRenderingPDNoteCell:(DailyNotesOverviewNoteCell *)cell withRow:(AXRow *)row {
    
    DailyNotesPendingNoteModel *model = row.model;
    [cell updateWithModel:model];
    
    return cell;
}


- (id)offscreenRenderingPDChildrenCell:(DailyNotesOverviewChildrenCell *)cell withRow:(AXRow *)row {
    
    DailyNotesChildrenModel *model = row.model;
    [cell updateWithModel:model];
    
    return cell;
}



#pragma mark - header Tap

- (void)headerTap:(UIGestureRecognizer *)gestureRecognizer {
    
    NSLog (@"tapped");
    
    //UITableViewCell *viewCell = gestureRecognizer.view;
    //UIView *view = viewCell.contentView;
    
    
    UIView *view = gestureRecognizer.view;
    
    NSUInteger index = [self.sections getSectionIndexForView:view];
    
    if (index == NSUIntegerMax) {
        return;
    }
    
    NSIndexSet *reloadIndexSet = [NSIndexSet indexSetWithIndex:index];
    
    AXHeader *header = [self.sections headerAtSectionIndex:index];
    
    AXSection *section = [self.sections sectionAtIndex:index];
    section.isHide = !section.isHide;
    [self updateHeaderArrowImage:header.arrowImageView withState:section.isHide];
    
    NSIndexSet *removeIndexSet = [[NSIndexSet alloc] init];
    NSIndexSet *insertIndexSet = [[NSIndexSet alloc] init];
    
    if ([header.type isEqualToString:PDChildrenHeader]) {
        
        if (section.isHide) {
    
            removeIndexSet = [self.sections getIndexSetForSectionsWithTag:CHILDREN_SECTIONS_TAG];
            [self.sections removeSectionsByIndexSet:removeIndexSet];
            
        } else {
            NSArray *sections = [self genFiltredChildrenSections];
            insertIndexSet = [self.sections insertSections:sections afterSectionIndex:section.sectionIndex];
        }
    }
    
    
    [self.tableView beginUpdates];
    
    //сама кликнутая секция
    [self.tableView reloadSections:reloadIndexSet withRowAnimation:UITableViewRowAnimationFade];
    
    if ([insertIndexSet count] > 0) {
        [self.tableView insertSections:insertIndexSet withRowAnimation:UITableViewRowAnimationFade];
    }
    
    if ([removeIndexSet count] > 0) {
        [self.tableView deleteSections:removeIndexSet withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.tableView endUpdates];
    
    //
}


#pragma mark - Remote

- (void)refreshData {
    
    [[DataStateManager sharedManager] updated:DataStateDailyNotes forViewController:self];

    
    [self cleanForRefresh];
    
    __weak __typeof(self)weakSelf = self;
    
    [[NetworkManager sharedInstance] dailyNotesOverviewAndShowProgressHUD:YES withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            if([[response valueForKeyPath:@"result.success"] intValue] == 1)
            {
                NSArray *childrenAndRooms = [response valueForKeyPath:@"data.notes"];
                NSArray *notes = [response valueForKeyPath:@"data.panding"];
                [strongSelf updateTableWithChildrenAndRooms:childrenAndRooms notes:notes];
            }
            else
            {
                Alert(@"Error",[response valueForKeyPath:@"result.message"] );
            }
        }
        else if(error)
        {
            Alert(@"Error", @"Error happened while operation");
        }
    }];
    
}

- (void)remoteDeleteItemForRow:(AXRow *)row {
    
    DailyNotesPendingNoteModel *model = row.model;
    row.isForRemoval = YES;
    [self updateRowCellWithRemovalActiovityIndicator:row];
    
    __weak __typeof(self)weakSelf = self;
    __block __weak __typeof(row)weakRow = row;
    
    
    
    [[NetworkManager sharedInstance] dailyNotesDeletePendingNoteWithId:[model.noteId intValue] showProgressHUD:NO withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            __strong __typeof(weakRow)strongRow = weakRow;
            if (!strongSelf || !strongRow) {
                return;
            }
            
            
            if([[response valueForKeyPath:@"result.success"] intValue] == 1)
            {
                //                if ([self.notesArray containsObject:model]) {
                //                    [self.notesArray removeObject:model];
                //                }
                [strongSelf deleteRows:@[strongRow]];
                [self updateNotesAfterDelete];
            }
            else
            {
                Alert(@"Error",[response valueForKeyPath:@"result.message"] );
                
                strongRow.isForRemoval = NO;
                [strongSelf updateRowCellWithRemovalActiovityIndicator:strongRow];
                
                
            }
        }
        else if(error)
        {
            
            Alert(@"Error", @"Error happened while operation");
            
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            __strong __typeof(weakRow)strongRow = weakRow;
            if (!strongSelf || !strongRow) {
                return;
            }
            strongRow.isForRemoval = NO;
            [strongSelf updateRowCellWithRemovalActiovityIndicator:strongRow];
            
            
        }
    }];
    
    
}

- (void)updateNotesAfterDelete {
    
    
    AXRow *row = [self.sections rowWithRowType:PDNoteCell];
    if (row) {
        return;
    }
    
    AXSection *section = [self.sections sectionWithSectionType:PDNotesSection];
    NSIndexSet *removeIndexSet = [NSIndexSet indexSetWithIndex:section.sectionIndex];
    if ([self.sections removeSectionsByIndexSet:removeIndexSet]) {
        [self.tableView beginUpdates];
        [self.tableView deleteSections:removeIndexSet withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
    }
    
    
}



- (void)cleanForRefresh {
    
    //Очищаем mainSection
    NSIndexSet *indexSet = [self.sections getIndexSetForSectionsWithType:PDMainSection];
    [self.sections removeSectionsByIndexSet:indexSet];
    
    indexSet = [self.sections getIndexSetForSectionsWithType:PDChildrenSection];
    [self.sections removeSectionsByIndexSet:indexSet];
    
    indexSet = [self.sections getIndexSetForSectionsWithType:PDSubChildrenSection];
    [self.sections removeSectionsByIndexSet:indexSet];
    
    
    indexSet = [self.sections getIndexSetForSectionsWithType:PDNotesSection];
    [self.sections removeSectionsByIndexSet:indexSet];
    
    [self.tableView reloadData];
}


#pragma mark - Remote processing

- (void)updateTableWithChildrenAndRooms:(NSArray *)childenAndRooms notes:(NSArray *)notes {
    
    
    //обрабатываем notes
    NSMutableArray *notesPrepared = [[NSMutableArray alloc] init];
    if ([notes isKindOfClass:[NSArray class]]) {
        for (NSDictionary *noteDict in notes) {
            
            DailyNotesPendingNoteModel *model = [[DailyNotesPendingNoteModel alloc] initWithDict:noteDict];
            if (model && [model isExist]) {
                [notesPrepared addObject:model];
            }
        }
    }
    
    
    NSMutableArray *sections = [NSMutableArray array];
    
    if ([notesPrepared count]) {
        AXSection *notesSection = [[AXSection alloc] initWithType:PDNotesSection];
        notesSection.isHide = NO;
        AXHeader *header = [self.sections genHeaderWithType:PDNotesHeader];
        notesSection.header = header;
        
        for (DailyNotesPendingNoteModel *model in notesPrepared) {
            AXRow *row = [self.sections genRowWithType:PDNoteCell];
            row.model = model;
            [notesSection addRow:row];
        }
        [notesSection addRow:[self.sections genRowWithType:PDSpacer35Cell]];
        
        [sections addObject:notesSection];
    }
    
    
    
    
    
    //обрабатываем children
    NSMutableArray *childrensPrepared = [[NSMutableArray alloc] init];
    
    if ([childenAndRooms isKindOfClass:[NSArray class]]) {
        for (NSDictionary *roomsAndChildsDict in childenAndRooms) {
            NSArray *childs = [roomsAndChildsDict objectForKey:@"children"];
            NSDictionary *roomDict = [roomsAndChildsDict objectForKey:@"room"];
            if (childs && [childs isKindOfClass:[NSArray class]]) {
                for (NSDictionary *child in childs) {
                    DailyNotesChildrenModel *model = [[DailyNotesChildrenModel alloc] initWithChildDict:child roomDict:roomDict];
                    if (model) {
                        [childrensPrepared addObject:model];
                    }
                }
            }
            
        }
    }
    
    //Заполняем список комнат
    self.roomsAndChildDictionary = [NSMutableDictionary dictionary];
    for (DailyNotesChildrenModel *child in childrensPrepared) {
        [self addChild:child toRoom:child.roomName dictType:FilterDictTypeBase];
    }
    
    
    AXSection *childrenSection = [[AXSection alloc] initWithType:PDChildrenSection];
    childrenSection.isHide = NO;
    AXHeader *childrenHeader = [self.sections genHeaderWithType:PDChildrenHeader];
    childrenSection.header = childrenHeader;
    [childrenSection addRow:[self.sections genRowWithType:PDFilterCell]];

    [sections addObject:childrenSection];
    
    [self.sections insertSections:sections afterSectionType:PDTopSection];

    
    
    
    /*
    if (![childrensPrepared count]) {
        [self cleanForRefreshWithAnimation];
        [self insertMessageCell]; //Empty default
        return;
    }
    */
    
    
    
    
    [self updateTableFiltered];
    
    
}


- (void)cleanForRefreshWithAnimation {
    
    //Очищаем mainSection
    NSIndexSet *indexSet = [self.sections getIndexSetForSectionsWithType:PDMainSection];
    [self.sections removeSectionsByIndexSet:indexSet];
    
    
    [self.tableView beginUpdates];
    
    if ([indexSet count] > 0) {
        [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.tableView endUpdates];
    
    //[self.tableView reloadData];
}


- (void)updateTableFiltered {
    
    
    //TODO:
    //Сделать очистку старых
    
     //NSIndexSet *removeIndexSet = [self.sections getIndexSetForSectionsWithType:PDMainSection];
    NSIndexSet *removeIndexSet = [self.sections getIndexSetForSectionsWithTag:CHILDREN_SECTIONS_TAG];
     [self.sections removeSectionsByIndexSet:removeIndexSet];
     if ([removeIndexSet count] > 0) {
         //[self.tableView deleteSections:removeIndexSet withRowAnimation:UITableViewRowAnimationNone];
         //[self.tableView reloadData];
     }
    
    
    
    
    NSArray *sections = [self genFiltredChildrenSections];
    
    //[UIView setAnimationsEnabled:NO];
    //[CATransaction setDisableActions:YES];
    //[self.tableView beginUpdates];
    //[self cleanForRefreshWithAnimation];
    
    //[self.sections insertSections:sections afterSectionType:PDTopSection];
    
    
    
    
    
    
    NSIndexSet *indexSet = [self.sections insertSections:sections afterSectionType:PDChildrenSection];
    
    
    if ([indexSet count] > 0) {
        //[self.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView reloadData];
    }
    //    [self.tableView endUpdates];
    //    [UIView setAnimationsEnabled:YES];
    //[CATransaction setDisableActions:NO];
    
    
    //[self.tableView reloadData];
    
    
}


- (NSArray *)genFiltredChildrenSections {
    
    
    [self sortedRoomAndChildsWithFilter:self.filterString];
    
    NSArray *sortedRoomNames = [[self.roomsAndChildFilteredDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
    if (![sortedRoomNames count]) {
        AXSection *childrenSubSection = [[AXSection alloc] initWithType:PDSubChildrenSection];
        [childrenSubSection.tags addObject:CHILDREN_SECTIONS_TAG];
        [childrenSubSection addRow:[self.sections genRowWithType:PDMessageCell]];
        return @[childrenSubSection];
    }

    
    NSMutableArray *sections = [NSMutableArray array];
    
    AXSection *sectionSpacer = [[AXSection alloc] initWithType:PDSubChildrenSection];
    [sectionSpacer addRow:[self.sections genRowWithType:PDSpacerCell]];
    [sectionSpacer.tags addObject:CHILDREN_SECTIONS_TAG];
    [sections addObject:sectionSpacer];
    
    
    for (NSString *roomName in sortedRoomNames) {
        
        AXSection *section = [[AXSection alloc] initWithType:PDMainSection];
        section.isHide = NO;
        [section.tags addObject:CHILDREN_SECTIONS_TAG];
        
        AXHeader *header = [self.sections genHeaderWithType:PDRoomHeader];
        header.title = roomName;
        //[header.userInfo setObject:roomName forKey:@"roomName"];
        //[header.userInfo setObject:statusName forKey:@"photo"];
        section.header = header;
        
        NSArray *childs = [self.roomsAndChildFilteredDictionary objectForKey:roomName];
        for (DailyNotesChildrenModel *child in childs) {
            AXRow *row = [self.sections genRowWithType:PDChildrenCell];
            row.model = child;
            [section addRow:row];
            
            //header save child
            header.object = child;
        }
        
        if ([roomName isEqualToString:[sortedRoomNames lastObject]]) {
            [section addRow:[self.sections genRowWithType:PDSpacerCell]];
        }
        else {
            [section addRow:[self.sections genRowWithType:PDSpacer45Cell]];
        }
        
        
        [sections addObject:section];
        
    }
    
    return sections;
    
}

/*
- (void)insertChidlrenMessageCell {
    
    AXSection *childrenSubSection = [[AXSection alloc] initWithType:PDSubChildrenSection];
    
    
    [childrenSubSection addRow:[self.sections genRowWithType:PDMessageCell]];
    NSIndexSet *insertIndexSet = [self.sections insertSections:@[childrenSubSection] afterSectionType:PDChildrenSection];
    
    if ([insertIndexSet count] == 0) {
        return;
    }
    
    
    [self.tableView beginUpdates];
    
    [self.tableView insertSections:insertIndexSet withRowAnimation:UITableViewRowAnimationNone];
    
    [self.tableView endUpdates];
    
}
*/

- (void)addChild:(id)child toRoom:(NSString *)roomName dictType:(FilterDictType)dictType{
    
    if (!roomName) {
        roomName = @"N/A";
    }
    
    NSMutableDictionary *dict;
    
    if (dictType == FilterDictTypeBase) {
        dict = self.roomsAndChildDictionary;
    }
    else {
        dict = self.roomsAndChildFilteredDictionary;
    }
    
    
    NSMutableArray *childs = [dict objectForKey:roomName];
    if ([childs isKindOfClass:[NSMutableArray class]]) {
        [childs addObject:child];
    }
    else {
        childs = [[NSMutableArray alloc] init];
        [childs addObject:child];
        [dict setObject:childs forKey:roomName];
    }
    
    
}


- (void)sortedRoomAndChildsWithFilter:(NSString *)filterString {
    
    //Создаем фильтрованный список
    self.roomsAndChildFilteredDictionary = [[NSMutableDictionary alloc] init];
    
    for (NSString *roomName in self.roomsAndChildDictionary) {
        
        NSArray *childs = [self.roomsAndChildDictionary objectForKey:roomName];
        
        for (DailyNotesChildrenModel *child in childs) {
            if ([child nameContain:filterString]) {
                [self addChild:child toRoom:child.roomName dictType:FilterDictTypeFiltered];
            }
        }
    }
    
    //Создаем сортированный список детей
    NSMutableDictionary *filteredDictionary = [[NSMutableDictionary alloc] init];
    for (NSString *roomName in [self.roomsAndChildFilteredDictionary allKeys]) {
        
        NSArray *childs = [self.roomsAndChildFilteredDictionary objectForKey:roomName];
        
        
        NSSortDescriptor *lastNameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName"
                                                                               ascending:YES
                                                                                selector:@selector(localizedCaseInsensitiveCompare:)];
        
        NSSortDescriptor *firstNameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firstName"
                                                                                ascending:YES
                                                                                 selector:@selector(localizedCaseInsensitiveCompare:)];
        
        NSArray *sortDescriptors = [NSArray arrayWithObjects:firstNameSortDescriptor, lastNameSortDescriptor, nil];
        
        [filteredDictionary setObject:[childs sortedArrayUsingDescriptors:sortDescriptors] forKey:roomName];
        
    }
    
    self.roomsAndChildFilteredDictionary = filteredDictionary;
    
}


/*
- (AXRow *)rowForChildren:(BottlesOverviewChildrenModel *)children {
    
    NSArray *childrenIndexPaths = [self.sections getIndexPathsForRowType:PDChildrenCell atIndex:AXRowAll withCondition:^BOOL(AXSection *section, AXRow *row) {
        if ([row.model isEqual:children]) {
            return YES;
        }
        return  NO;
 
    }];
    
    
    if (![childrenIndexPaths count]) {
        return nil;
    }
    
    
    AXRow *childrenRow = [self.sections rowAtIndexPath:[childrenIndexPaths firstObject]];
    
    return childrenRow;
    
    
    
    
}
*/



#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == FILTER_TEXT_FIELD_TAG) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        self.filterString = newString;
        
        
        NSIndexPath *indexPath = [self indexPathForView:textField];
        [self updateTableFiltered];
        
        FilterCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [cell.filterTextField becomeFirstResponder];
        //[textField becomeFirstResponder];
        return NO;
    }
    
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    
    if (textField.tag == FILTER_TEXT_FIELD_TAG) {
        [textField resignFirstResponder];
    }
    
    
    return YES;
}


#pragma mark - Actions


- (IBAction)editNoteAction:(id)sender {

    [self performSegueWithIdentifier:@"editSegue" sender:sender];
}


- (IBAction)deleteNoteAction:(id)sender {
    
    AXRow *row = [self rowForView:sender];
    
    PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning!" message:@"Deleting this information from the system can NOT be reversed!" preferredStyle:PSTAlertControllerStyleAlert];
    
    [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:nil]];
    
    [alert addAction:[PSTAlertAction actionWithTitle:@"Delete" style:PSTAlertActionStyleDestructive handler:^(PSTAlertAction *action) {
        
        [self remoteDeleteItemForRow:row];
    }]];
    
    [alert showWithSender:self controller:self animated:YES
               completion:nil];

    
}


- (IBAction)viewChildAction:(id)sender {
    
    [self performSegueWithIdentifier:@"viewSegue" sender:sender];
    
}









#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"editSegue"]) {
        
        AXRow *row = [self rowForView:sender];
        
         DailyNotesPendingNoteModel *noteModel = row.model;
        
        if (!noteModel) {
            return;
        }
        
        DailyNotesAddNoteViewController *viewController = segue.destinationViewController;
        viewController.isEdit = YES;
        viewController.noteId = [noteModel.noteId intValue];
    }
    else if ([segue.identifier isEqualToString:@"viewSegue"]) {
        
        AXRow *row = [self rowForView:sender];
        
        DailyNotesChildrenModel *model = row.model;
        
        if (!model) {
            return;
        }
        
        DailyNotesChildrenNotesListViewController *viewController = segue.destinationViewController;
        viewController.childrenModel = model;
    }
    
    
}



@end
