//
//  NavigationButton.m
//  pd2
//
//  Created by User on 15.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "NavigationButton.h"

@implementation NavigationButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    UIImage *image=[self backgroundImageForState:UIControlStateNormal];
    image=[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self setBackgroundImage:image forState:UIControlStateNormal];
    self.tintColor=[[MyColorTheme sharedInstance]menuButtonsColor];
}

@end
