//
//  UnavailableSectionManager.m
//  pd2
//
//  Created by i11 on 02/03/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "UnavailableSectionManager.h"

NSString * const UnavailableSectionManagerMessageTitle = @"Warning";


#define DEFAULT_ALERT_MESSAGE @"Sorry, this section is temporary unavailable"

@interface UnavailableSectionManager ()


@property (nonatomic, strong) NSMutableSet *sections;

@end

@implementation UnavailableSectionManager

- (id)init
{
    self = [super init];

    if (self) {
        
        [self clear];
        
        //tmp
        
        
    }
    
    return self;
}


- (void)clear {
    
    self.sections = [[NSMutableSet alloc] init];
    
}

- (void)updateWithArray:(NSArray *)array andMessage:(NSString *)message {
    
    [self updateMessage:message];
    
    
    [self clear];

    //tmp
    //[self.sections addObject:[NSNumber numberWithUnsignedInteger:UnavailableSectionASGalleriesPhoto]];
    
    if (!(array && [array isKindOfClass:[NSArray class]])) {
        return;
    }
    
    
    for (NSString *key in array) {
        [self checkAndAddSourceKey:key];
    }
    
    
}

- (void)updateMessage:(NSString *)message {
    
    if (message && [message isKindOfClass:[NSString class]] && [message length] > 0) {
        self.alertMessage = message;
    }
    else {
        self.alertMessage = DEFAULT_ALERT_MESSAGE;
    }
    
}

- (void)checkAndAddSourceKey:(NSString *)key {
    
    
    if (!key) {
        return;
    }
    
    if (![key isKindOfClass:[NSString class]]) {
        return;
    }
    
    [self checkKey1:key key2:@"ASAppSettingsPush" andAddUnavailableSection:UnavailableSectionASAppSettingsPush];
    [self checkKey1:key key2:@"ASAppSettingsDeleteAdmins" andAddUnavailableSection:UnavailableSectionASAppSettingsDeleteAdmins];
    [self checkKey1:key key2:@"ASAppSettingsTouchId" andAddUnavailableSection:UnavailableSectionASAppSettingsTouchId];
    
    
    [self checkKey1:key key2:@"ASManage" andAddUnavailableSection:UnavailableSectionASManage];
    [self checkKey1:key key2:@"ASManageAdministrators" andAddUnavailableSection:UnavailableSectionASManageAdministrators];
    [self checkKey1:key key2:@"ASManageMyProfile" andAddUnavailableSection:UnavailableSectionASManageMyProfile];
    [self checkKey1:key key2:@"ASManageChildren" andAddUnavailableSection:UnavailableSectionASManageChildren];
    [self checkKey1:key key2:@"ASManageChildrenExisting" andAddUnavailableSection:UnavailableSectionASManageChildrenExisting];
    [self checkKey1:key key2:@"ASManageChildrenDeparted" andAddUnavailableSection:UnavailableSectionASManageChildrenDeparted];
    [self checkKey1:key key2:@"ASManageChildrenWaitingList" andAddUnavailableSection:UnavailableSectionASManageChildrenWaitingList];
    [self checkKey1:key key2:@"ASManageChildrenMoving" andAddUnavailableSection:UnavailableSectionASManageChildrenMoving];
    [self checkKey1:key key2:@"ASManageChildrenAuthorised" andAddUnavailableSection:UnavailableSectionASManageChildrenAuthorised];
    [self checkKey1:key key2:@"ASManageChildrenNappies" andAddUnavailableSection:UnavailableSectionASManageChildrenNappies];
    [self checkKey1:key key2:@"ASManageChildrenNappiesChildrenInNappies" andAddUnavailableSection:UnavailableSectionASManageChildrenNappiesChildrenInNappies];
    [self checkKey1:key key2:@"ASManageChildrenNappiesNappyOverview" andAddUnavailableSection:UnavailableSectionASManageChildrenNappiesNappyOverview];
    [self checkKey1:key key2:@"ASManageChildrenSleeping" andAddUnavailableSection:UnavailableSectionASManageChildrenSleeping];
    [self checkKey1:key key2:@"ASManageChildrenBottles" andAddUnavailableSection:UnavailableSectionASManageChildrenBottles];
    [self checkKey1:key key2:@"ASManageParentalAccess" andAddUnavailableSection:UnavailableSectionASManageParentalAccess];
    [self checkKey1:key key2:@"ASManagePositions" andAddUnavailableSection:UnavailableSectionASManagePositions];
    [self checkKey1:key key2:@"ASManageRooms" andAddUnavailableSection:UnavailableSectionASManageRooms];
    [self checkKey1:key key2:@"ASManageDiaryHelpers" andAddUnavailableSection:UnavailableSectionASManageDiaryHelpers];
    [self checkKey1:key key2:@"ASManageProfileSections" andAddUnavailableSection:UnavailableSectionASManageProfileSections];
    [self checkKey1:key key2:@"ASManageBirthdays" andAddUnavailableSection:UnavailableSectionASManageBirthdays];
    
    [self checkKey1:key key2:@"ASSettings" andAddUnavailableSection:UnavailableSectionASSettings];
    
    [self checkKey1:key key2:@"ASDailyChecklist" andAddUnavailableSection:UnavailableSectionASDailyChecklist];
    [self checkKey1:key key2:@"ASDailyChecklistThisWeek" andAddUnavailableSection:UnavailableSectionASDailyChecklistThisWeek];
    [self checkKey1:key key2:@"ASDailyChecklistSelectWeek" andAddUnavailableSection:UnavailableSectionASDailyChecklistSelectWeek];
    [self checkKey1:key key2:@"ASDailyChecklistAddRiskArea" andAddUnavailableSection:UnavailableSectionASDailyChecklistAddRiskArea];
    [self checkKey1:key key2:@"ASDailyChecklistViewRiskArea" andAddUnavailableSection:UnavailableSectionASDailyChecklistViewRiskArea];
    [self checkKey1:key key2:@"ASDailyChecklistAddAction" andAddUnavailableSection:UnavailableSectionASDailyChecklistAddAction];
    [self checkKey1:key key2:@"ASDailyChecklistViewAction" andAddUnavailableSection:UnavailableSectionASDailyChecklistViewAction];
    
    
    [self checkKey1:key key2:@"ASVisitors" andAddUnavailableSection:UnavailableSectionASVisitors];
    [self checkKey1:key key2:@"ASVisitorsOverview" andAddUnavailableSection:UnavailableSectionASVisitorsOverview];
    [self checkKey1:key key2:@"ASVisitorsAddVisitor" andAddUnavailableSection:UnavailableSectionASVisitorsAddVisitor];
    [self checkKey1:key key2:@"ASVisitorsRegularVisitors" andAddUnavailableSection:UnavailableSectionASVisitorsRegularVisitors];
    [self checkKey1:key key2:@"ASVisitorsAddRegularVisitor" andAddUnavailableSection:UnavailableSectionASVisitorsAddRegularVisitor];
    
    [self checkKey1:key key2:@"ASGeneralNotes" andAddUnavailableSection:UnavailableSectionASGeneralNotes];

    [self checkKey1:key key2:@"ASDaily" andAddUnavailableSection:UnavailableSectionASDaily];
    [self checkKey1:key key2:@"ASDailyNotes" andAddUnavailableSection:UnavailableSectionASDailyNotes];
    [self checkKey1:key key2:@"ASFoodMenu" andAddUnavailableSection:UnavailableSectionASFoodMenu];
    [self checkKey1:key key2:@"ASParents" andAddUnavailableSection:UnavailableSectionASParents];
    [self checkKey1:key key2:@"ASStaff" andAddUnavailableSection:UnavailableSectionASStaff];
    [self checkKey1:key key2:@"ASLegal" andAddUnavailableSection:UnavailableSectionASLegal];


    
    
    [self checkKey1:key key2:@"ASDiary" andAddUnavailableSection:UnavailableSectionASDiary];
    [self checkKey1:key key2:@"ASDiaryOverview" andAddUnavailableSection:UnavailableSectionASDiaryOverview];
    [self checkKey1:key key2:@"ASDiaryDuplication" andAddUnavailableSection:UnavailableSectionASDiaryDuplication];

    [self checkKey1:key key2:@"ASProgress" andAddUnavailableSection:UnavailableSectionASProgress];
    [self checkKey1:key key2:@"ASProgressAddObservation" andAddUnavailableSection:UnavailableSectionASProgressAddObservation];
    
    [self checkKey1:key key2:@"ASNextStep" andAddUnavailableSection:UnavailableSectionASNextStep];
    
    [self checkKey1:key key2:@"ASPermissions" andAddUnavailableSection:UnavailableSectionASPermissions];
    [self checkKey1:key key2:@"ASPermissionsAssign" andAddUnavailableSection:UnavailableSectionASPermissionsAssign];
    [self checkKey1:key key2:@"ASPermissionsAddPermission" andAddUnavailableSection:UnavailableSectionASPermissionsAddPermission];
    [self checkKey1:key key2:@"ASPermissionsViewPermissions" andAddUnavailableSection:UnavailableSectionASPermissionsViewPermissions];
    [self checkKey1:key key2:@"ASPermissionsAddCategory" andAddUnavailableSection:UnavailableSectionASPermissionsAddCategory];
    [self checkKey1:key key2:@"ASPermissionsViewCategories" andAddUnavailableSection:UnavailableSectionASPermissionsViewCategories];
    [self checkKey1:key key2:@"ASPermissionsAddGroup" andAddUnavailableSection:UnavailableSectionASPermissionsAddGroup];
    [self checkKey1:key key2:@"ASPermissionsViewGroups" andAddUnavailableSection:UnavailableSectionASPermissionsViewGroups];
    
    [self checkKey1:key key2:@"ASPolicies" andAddUnavailableSection:UnavailableSectionASPolicies];
    [self checkKey1:key key2:@"ASPoliciesAssign" andAddUnavailableSection:UnavailableSectionASPoliciesAssign];
    [self checkKey1:key key2:@"ASPoliciesAddPolicy" andAddUnavailableSection:UnavailableSectionASPoliciesAddPolicy];
    [self checkKey1:key key2:@"ASPoliciesViewPolicies" andAddUnavailableSection:UnavailableSectionASPoliciesViewPolicies];
    [self checkKey1:key key2:@"ASPoliciesAddCategory" andAddUnavailableSection:UnavailableSectionASPoliciesAddCategory];
    [self checkKey1:key key2:@"ASPoliciesViewCategories" andAddUnavailableSection:UnavailableSectionASPoliciesViewCategories];
    [self checkKey1:key key2:@"ASPoliciesAddGroup" andAddUnavailableSection:UnavailableSectionASPoliciesAddGroup];
    [self checkKey1:key key2:@"ASPoliciesViewGroups" andAddUnavailableSection:UnavailableSectionASPoliciesViewGroups];
    
    [self checkKey1:key key2:@"ASMedical" andAddUnavailableSection:UnavailableSectionASMedical];
    [self checkKey1:key key2:@"ASMedicalOverview" andAddUnavailableSection:UnavailableSectionASMedicalOverview];
    [self checkKey1:key key2:@"ASMedicalAccidents" andAddUnavailableSection:UnavailableSectionASMedicalAccidents];
    [self checkKey1:key key2:@"ASMedicalAccidentsAccidents" andAddUnavailableSection:UnavailableSectionASMedicalAccidentsAccidents];
    [self checkKey1:key key2:@"ASMedicalAccidentsAccidentsAdd" andAddUnavailableSection:UnavailableSectionASMedicalAccidentsAccidentsAdd];
    [self checkKey1:key key2:@"ASMedicalAccidentsLocations" andAddUnavailableSection:UnavailableSectionASMedicalAccidentsLocations];
    [self checkKey1:key key2:@"ASMedicalAccidentsAddLocation" andAddUnavailableSection:UnavailableSectionASMedicalAccidentsAddLocation];
    [self checkKey1:key key2:@"ASMedicalShortTerm" andAddUnavailableSection:UnavailableSectionASMedicalShortTerm];
    [self checkKey1:key key2:@"ASMedicalShortTermAdd" andAddUnavailableSection:UnavailableSectionASMedicalShortTermAdd];
    [self checkKey1:key key2:@"ASMedicalLongTerm" andAddUnavailableSection:UnavailableSectionASMedicalLongTerm];
    [self checkKey1:key key2:@"ASMedicalLongTermAdd" andAddUnavailableSection:UnavailableSectionASMedicalLongTermAdd];
    [self checkKey1:key key2:@"ASMedicalConcerns" andAddUnavailableSection:UnavailableSectionASMedicalConcerns];
    [self checkKey1:key key2:@"ASMedicalMedicines" andAddUnavailableSection:UnavailableSectionASMedicalMedicines];
    [self checkKey1:key key2:@"ASMedicalVaccination" andAddUnavailableSection:UnavailableSectionASMedicalVaccination];

    
    
    [self checkKey1:key key2:@"ASRegister" andAddUnavailableSection:UnavailableSectionASRegister];
    [self checkKey1:key key2:@"ASRegisterOverview" andAddUnavailableSection:UnavailableSectionASRegisterOverview];
    [self checkKey1:key key2:@"ASRegisterSelectWeek" andAddUnavailableSection:UnavailableSectionASRegisterSelectWeek];
    [self checkKey1:key key2:@"ASRegisterThisWeek" andAddUnavailableSection:UnavailableSectionASRegisterThisWeek];
    [self checkKey1:key key2:@"ASRegisterNextWeek" andAddUnavailableSection:UnavailableSectionASRegisterNextWeek];
    [self checkKey1:key key2:@"ASRegisterToday" andAddUnavailableSection:UnavailableSectionASRegisterToday];
    
    [self checkKey1:key key2:@"ASBookkeeping" andAddUnavailableSection:UnavailableSectionASBookkeeping];
    [self checkKey1:key key2:@"ASBookkeepingGeneralSetup" andAddUnavailableSection:UnavailableSectionASBookkeepingGeneralSetup];
    
    [self checkKey1:key key2:@"ASGalleries" andAddUnavailableSection:UnavailableSectionASGalleries];
    [self checkKey1:key key2:@"ASGalleriesPhoto" andAddUnavailableSection:UnavailableSectionASGalleriesPhoto];
    [self checkKey1:key key2:@"ASGalleriesVideo" andAddUnavailableSection:UnavailableSectionASGalleriesVideo];
    [self checkKey1:key key2:@"ASGalleriesUploadManager" andAddUnavailableSection:UnavailableSectionASGalleriesUploadManager];
    [self checkKey1:key key2:@"ASPrivateMessages" andAddUnavailableSection:UnavailableSectionASPrivateMessages];

    
    
    //--PARENT SIDE
    [self checkKey1:key key2:@"PSAppSettingsPush" andAddUnavailableSection:UnavailableSectionPSAppSettingsPush];
    [self checkKey1:key key2:@"PSAppSettingsDeleteAdmins" andAddUnavailableSection:UnavailableSectionPSAppSettingsDeleteAdmins];
    [self checkKey1:key key2:@"PSAppSettingsTouchId" andAddUnavailableSection:UnavailableSectionPSAppSettingsTouchId];
    
    [self checkKey1:key key2:@"PSChildren" andAddUnavailableSection:UnavailableSectionPSChildren];
    [self checkKey1:key key2:@"PSChildrenExisting" andAddUnavailableSection:UnavailableSectionPSChildrenExisting];
    [self checkKey1:key key2:@"PSChildrenDeparted" andAddUnavailableSection:UnavailableSectionPSChildrenDeparted];
    [self checkKey1:key key2:@"PSChildrenAuthorised" andAddUnavailableSection:UnavailableSectionPSChildrenAuthorised];
    [self checkKey1:key key2:@"PSChildrenNappies" andAddUnavailableSection:UnavailableSectionPSChildrenNappies];
    [self checkKey1:key key2:@"PSChildrenSleeping" andAddUnavailableSection:UnavailableSectionPSChildrenSleeping];
    [self checkKey1:key key2:@"PSChildrenBottles" andAddUnavailableSection:UnavailableSectionPSChildrenBottles];
    [self checkKey1:key key2:@"PSChildrenDailyNotes" andAddUnavailableSection:UnavailableSectionPSChildrenDailyNotes];

    
    
    [self checkKey1:key key2:@"PSDiaries" andAddUnavailableSection:UnavailableSectionPSDiaries];
    [self checkKey1:key key2:@"PSProgress" andAddUnavailableSection:UnavailableSectionPSProgress];
    [self checkKey1:key key2:@"PSObservation" andAddUnavailableSection:UnavailableSectionPSObservation];
    [self checkKey1:key key2:@"PSNextStep" andAddUnavailableSection:UnavailableSectionPSNextStep];
    [self checkKey1:key key2:@"PSPolicies" andAddUnavailableSection:UnavailableSectionPSPolicies];
    [self checkKey1:key key2:@"PSPermissions" andAddUnavailableSection:UnavailableSectionPSPermissions];
    [self checkKey1:key key2:@"PSMedical" andAddUnavailableSection:UnavailableSectionPSMedical];
    [self checkKey1:key key2:@"PSRegister" andAddUnavailableSection:UnavailableSectionPSRegister];
    [self checkKey1:key key2:@"PSGalleries" andAddUnavailableSection:UnavailableSectionPSGalleries];
    [self checkKey1:key key2:@"PSGalleriesPhoto" andAddUnavailableSection:UnavailableSectionPSGalleriesPhoto];
    [self checkKey1:key key2:@"PSGalleriesVideo" andAddUnavailableSection:UnavailableSectionPSGalleriesVideo];
    [self checkKey1:key key2:@"PSPrivateMessages" andAddUnavailableSection:UnavailableSectionPSPrivateMessages];
    [self checkKey1:key key2:@"PSPush" andAddUnavailableSection:UnavailableSectionPSPush];
    
}




- (BOOL)checkKey1:(NSString *)key1 key2:(NSString *)key2 andAddUnavailableSection:(UnavailableSection)unavailableSection {
    
    if([key1 caseInsensitiveCompare:key2] == NSOrderedSame) {
        [self addKey:unavailableSection];
        return YES;
    }
    else {
        return NO;
    }
    
}


- (void)addKey:(UnavailableSection)key {
    
    [self.sections addObject:[NSNumber numberWithUnsignedInteger:key]];
    
    
}



- (BOOL)isUnavialable:(UnavailableSection)unavailableSection {
    
    NSNumber *sectionNumber = [NSNumber numberWithUnsignedInteger:unavailableSection];
    
    if (!sectionNumber) {
        return NO;
    }
    
    if ([self.sections containsObject:sectionNumber]) {
        return YES; //секция не доступна
    }
    else {
        return NO; //секция доступна
    }
    
}


@end

