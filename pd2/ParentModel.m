//
//  ParentModel.m
//  pd2
//
//  Created by Andrey on 05/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "ParentModel.h"

@implementation ParentModel



/*
 {
 "first_name": "sdf",
 "last_name": "sdf",
 "image": "images/sted/parents/default.png",
 "ec_first_name": "",
 "ec_last_name": "",
 "ec_house": "",
 "ec_street_1": "",
 "ec_street_2": "",
 "ec_area": "",
 "ec_town": "",
 "ec_county": "",
 "ec_postcode": "",
 "ec_telephone": "",
 "ec_mobile": "",
 "ec_workplace": "",
 "ec_work_telephone": "",
 "ec_doctor_name": "",
 "ec_doctor_tel": "",
 "dip": "",
 "hib": "",
 "mea": "",
 "mum": "",
 "pol": "",
 "rub": "",
 "tet": "",
 "who": "",
 "ch_street_1": "",
 "ch_area": "",
 "ch_town": "",
 "ch_postcode": "",
 "ch_county": "",
 "parent_show_alert": true,
 "house": "",
 "street_1": "",
 "street_2": "",
 "area": "",
 "town": "",
 "county": "",
 "postcode": "",
 "phone": "",
 "mobile": "",
 "workplace": "",
 "work_telephone": "",
 "status": "on",
 "diaries": "on",
 "progress": "on",
 "gallery": "on",
 "policies": "on",
 "permissions": "on",
 "register": "on",
 "bookkeeping": "on",
 "communicate": "on",
 "meal_planning": "on",
 "medical": "on",
 "questionnaires": "on",
 "nextsteps": "on",
 "observations": "on",
 "contracts": "on",
 "risk_assessments": "on",
 "short_term_planning": "",
 "nappies": "on",
 "register_force": "on",
 "email": "ABC_ABC_mum@mybabysdays.com"
 }
 */

- (instancetype)initWithDict:(NSDictionary *)dict {
    
    self = [super init];
    if (self) {
        
        
        self.firstName = [self safeStringAndDecodingHTML:dict[@"first_name"]];
        self.lastName = [self safeStringAndDecodingHTML:dict[@"last_name"]];
        
        self.photoPath = [self safeString:dict[@"image"]];
    

        self.street1 = [self safeStringAndDecodingHTML:dict[@"street_1"]];
        self.street2 = [self safeStringAndDecodingHTML:dict[@"street_2"]];
        self.area = [self safeStringAndDecodingHTML:dict[@"area"]];
        self.town = [self safeStringAndDecodingHTML:dict[@"town"]];
        self.postCode = [self safeStringAndDecodingHTML:dict[@"postcode"]];
        self.country = [self safeStringAndDecodingHTML:dict[@"county"]];
        self.tel = [self safeStringAndDecodingHTML:dict[@"phone"]];
        self.mobile = [self safeStringAndDecodingHTML:dict[@"mobile"]];
        self.email = [self safeStringAndDecodingHTML:dict[@"email"]];
        self.workPlace = [self safeStringAndDecodingHTML:dict[@"workplace"]];
        self.workTel = [self safeStringAndDecodingHTML:dict[@"work_telephone"]];
        
        
    }
    return self;
    
}


- (NSString *)addressParent {
    
    
    NSMutableArray *array = [NSMutableArray array];
    
    if (self.street1) {
        [array addObject:self.street1];
    }
    if (self.street2) {
        [array addObject:self.street2];
    }
    if (self.area) {
        [array addObject:self.area];
    }
    if (self.town) {
        [array addObject:self.town];
    }
    if (self.country) {
        [array addObject:self.country];
    }
    if (self.postCode) {
        [array addObject:self.postCode];
    }
    
    if ([array count]) {
        return [array componentsJoinedByString:@", "];
    }
    else {
        return nil;
    }
}




@end
