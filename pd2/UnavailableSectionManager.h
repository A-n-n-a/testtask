//
//  UnavailableSectionManager.h
//  pd2
//
//  Created by i11 on 02/03/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Singleton.h"

NSString * const UnavailableSectionManagerMessageTitle;


@interface UnavailableSectionManager : Singleton

typedef NS_ENUM(NSUInteger, UnavailableSection) {
    //--App Settings
    UnavailableSectionASAppSettingsPush,
    UnavailableSectionASAppSettingsDeleteAdmins,
    UnavailableSectionASAppSettingsTouchId,


    
    //--Manage
    UnavailableSectionASManage, //вся секция manage
    UnavailableSectionASManageAdministrators, //определенный экран (administrators)
    UnavailableSectionASManageMyProfile,
    UnavailableSectionASManageChildren,
    UnavailableSectionASManageChildrenExisting,
    UnavailableSectionASManageChildrenDeparted,
    UnavailableSectionASManageChildrenWaitingList,
    UnavailableSectionASManageChildrenMoving,
    UnavailableSectionASManageChildrenAuthorised,
    UnavailableSectionASManageChildrenNappies,
    UnavailableSectionASManageChildrenNappiesChildrenInNappies,
    UnavailableSectionASManageChildrenNappiesNappyOverview,
    UnavailableSectionASManageChildrenSleeping,
    UnavailableSectionASManageChildrenBottles,
    UnavailableSectionASManageParentalAccess,
    UnavailableSectionASManagePositions,
    UnavailableSectionASManageRooms,
    UnavailableSectionASManageDiaryHelpers,
    UnavailableSectionASManageProfileSections,
    UnavailableSectionASManageBirthdays,

    
    //--Settings
    UnavailableSectionASSettings,
    //--Settings-DailyChecklist
    UnavailableSectionASDailyChecklist, //весь поздраздел Daily Cheklist
    UnavailableSectionASDailyChecklistThisWeek,
    UnavailableSectionASDailyChecklistSelectWeek,
    UnavailableSectionASDailyChecklistAddRiskArea,
    UnavailableSectionASDailyChecklistViewRiskArea,
    UnavailableSectionASDailyChecklistAddAction,
    UnavailableSectionASDailyChecklistViewAction,
    //--Settings-Visitors
    UnavailableSectionASVisitors,
    UnavailableSectionASVisitorsOverview,
    UnavailableSectionASVisitorsAddVisitor,
    UnavailableSectionASVisitorsRegularVisitors,
    UnavailableSectionASVisitorsAddRegularVisitor,
    //--Settings-General Notes
    UnavailableSectionASGeneralNotes,
    
    UnavailableSectionASDaily,
    UnavailableSectionASDailyNotes,
    
    UnavailableSectionASFoodMenu,

    UnavailableSectionASParents,
    UnavailableSectionASStaff,
    UnavailableSectionASLegal,

    
    //--Diary
    UnavailableSectionASDiary,
    UnavailableSectionASDiaryOverview,
    UnavailableSectionASDiaryDuplication,
    //--Progress
    UnavailableSectionASProgress,
    UnavailableSectionASProgressAddObservation,

    //--NextStep
    UnavailableSectionASNextStep,
    //Permissions
    UnavailableSectionASPermissions,
    UnavailableSectionASPermissionsAssign,
    UnavailableSectionASPermissionsAddPermission,
    UnavailableSectionASPermissionsViewPermissions,
    UnavailableSectionASPermissionsAddCategory,
    UnavailableSectionASPermissionsViewCategories,
    UnavailableSectionASPermissionsAddGroup,
    UnavailableSectionASPermissionsViewGroups,
    //Policies
    UnavailableSectionASPolicies,
    UnavailableSectionASPoliciesAssign,
    UnavailableSectionASPoliciesAddPolicy,
    UnavailableSectionASPoliciesViewPolicies,
    UnavailableSectionASPoliciesAddCategory,
    UnavailableSectionASPoliciesViewCategories,
    UnavailableSectionASPoliciesAddGroup,
    UnavailableSectionASPoliciesViewGroups,
    //--Medical
    UnavailableSectionASMedical,
    UnavailableSectionASMedicalOverview,
    UnavailableSectionASMedicalAccidents,
    UnavailableSectionASMedicalAccidentsAccidents,
    UnavailableSectionASMedicalAccidentsAccidentsAdd,
    UnavailableSectionASMedicalAccidentsLocations,
    UnavailableSectionASMedicalAccidentsAddLocation,
    UnavailableSectionASMedicalShortTerm,
    UnavailableSectionASMedicalShortTermAdd,
    UnavailableSectionASMedicalLongTerm,
    UnavailableSectionASMedicalLongTermAdd,
    UnavailableSectionASMedicalConcerns,
    UnavailableSectionASMedicalMedicines,
    UnavailableSectionASMedicalVaccination,
    
    //--Register
    UnavailableSectionASRegister,
    UnavailableSectionASRegisterOverview,
    UnavailableSectionASRegisterSelectWeek,
    UnavailableSectionASRegisterThisWeek,
    UnavailableSectionASRegisterNextWeek,
    UnavailableSectionASRegisterToday,
    
    //--Bookkeeping
    UnavailableSectionASBookkeeping,
    UnavailableSectionASBookkeepingGeneralSetup,

    //--Galleries
    UnavailableSectionASGalleries,
    UnavailableSectionASGalleriesPhoto,
    UnavailableSectionASGalleriesVideo,
    UnavailableSectionASGalleriesUploadManager,

    //--Private Messages
    UnavailableSectionASPrivateMessages,
    
    
    //--PARENT SIDE
    UnavailableSectionPSAppSettingsPush,
    UnavailableSectionPSAppSettingsDeleteAdmins,
    UnavailableSectionPSAppSettingsTouchId,

    UnavailableSectionPSChildren,
    UnavailableSectionPSChildrenExisting,
    UnavailableSectionPSChildrenDeparted,
    UnavailableSectionPSChildrenAuthorised,
    UnavailableSectionPSChildrenDailyNotes,
    UnavailableSectionPSChildrenSleeping,
    UnavailableSectionPSChildrenBottles,
    UnavailableSectionPSChildrenNappies,
    UnavailableSectionPSDiaries,
    UnavailableSectionPSProgress,
    UnavailableSectionPSObservation,
    UnavailableSectionPSNextStep,
    UnavailableSectionPSPolicies,
    UnavailableSectionPSPermissions,
    UnavailableSectionPSMedical,
    UnavailableSectionPSRegister,
    UnavailableSectionPSGalleries,
    UnavailableSectionPSGalleriesPhoto,
    UnavailableSectionPSGalleriesVideo,
    UnavailableSectionPSPrivateMessages,
    UnavailableSectionPSPush

    
    
};

@property (nonatomic, strong) NSString *alertMessage;


- (void)updateWithArray:(NSArray *)array andMessage:(NSString *)message;
- (void)clear;
- (BOOL)isUnavialable:(UnavailableSection)unavailableSection;

@end
