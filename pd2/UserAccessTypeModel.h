//
//  UserAccessTypeModel.h
//  pd2
//
//  Created by Andrey on 09/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXBaseModel.h"


typedef NS_ENUM(NSInteger, UserAccessType) {
    
    //system
    UserAdminAccessTypeSystemAccountInformation,
    UserAdminAccessTypeSystemBulkDownload,
    UserAdminAccessTypeSystemPersonalisedHomePage,
    UserAdminAccessTypeSystemSystemColourSchemes,
    UserAdminAccessTypeSystemWebsiteLoginForm,
    
    //manage
    UserAdminAccessTypeManageRooms,
    UserAdminAccessTypeManageOccupancy,
    UserAdminAccessTypeManageFoodMenuHelpers,
    UserAdminAccessTypeManageProfileSections,
    
    //setting
    UserAdminAccessTypeSettingGeneralNotes,
    UserAdminAccessTypeSettingDocuments,
    UserAdminAccessTypeSettingVisitors,
    UserAdminAccessTypeSettingFireDrills,
    UserAdminAccessTypeSettingFirePlans,
    UserAdminAccessTypeSettingRiskAssessments,
    UserAdminAccessTypeSettingSEF,
    UserAdminAccessTypeSettingCalendar,
    
    //daily
    UserAdminAccessTypeDailyNotes,
    UserAdminAccessTypeDailyActions,
    UserAdminAccessTypeDailyBottles,
    UserAdminAccessTypeDailyToileting,
    UserAdminAccessTypeDailySleeping,
    UserAdminAccessTypeDailyTemperatures,
    UserAdminAccessTypeDailyFoodMenus,
    UserAdminAccessTypeDailyChecklist,
    UserAdminAccessTypeDailyRegister,
    
    //children
    UserAdminAccessTypeChildrenChildren,
    UserAdminAccessTypeChildrenIncluded,
    UserAdminAccessTypeChildrenMoving,
    UserAdminAccessTypeChildrenWaiting,
    UserAdminAccessTypeChildrenDeparted,
    UserAdminAccessTypeChildrenBirthdays,
    
    //parents
    UserAdminAccessTypeParentsParentalAccess,
    UserAdminAccessTypeParentsISign,
    UserAdminAccessTypeParentsAuthorisedPersons,
    
    //staff
    UserAdminAccessTypeStaffMyProfile,
    UserAdminAccessTypeStaffAdministrators,
    UserAdminAccessTypeStaffAccessTimes,
    UserAdminAccessTypeStaffISignFull,
    UserAdminAccessTypeStaffISignSole,
    UserAdminAccessTypeStaffAssistantRegister,
    UserAdminAccessTypeStaffRegularHours,
    UserAdminAccessTypeStaffPoliciesFull,
    UserAdminAccessTypeStaffPoliciesSole,
    UserAdminAccessTypeStaffAccidentsIncidentsFull,
    UserAdminAccessTypeStaffAccidentsIncidentsSole,
    
    //progress and planing
    UserAdminAccessTypeProgressAndPlaningProgressLearningJournal,
    UserAdminAccessTypeProgressAndPlaningNextStepsILP,
    UserAdminAccessTypeProgressAndPlaningStartingPoints,
    UserAdminAccessTypeProgressAndPlaningProgressTrackers,
    UserAdminAccessTypeProgressAndPlaningProgressStatistics,
    UserAdminAccessTypeProgressAndPlaningAchievementsSearch,
    UserAdminAccessTypeProgressAndPlaningTermlyReports,
    UserAdminAccessTypeProgressAndPlaning2YearProgressCheck,
    UserAdminAccessTypeProgressAndPlaningOutcomeSearch,
    UserAdminAccessTypeProgressAndPlaningPlanning,
    
    //medical
    UserAdminAccessTypeMedicalOverview,
    UserAdminAccessTypeMedicalVaccinations,
    UserAdminAccessTypeMedicalAccidents,
    UserAdminAccessTypeMedicalShortTerm,
    UserAdminAccessTypeMedicalLongTerm,
    UserAdminAccessTypeMedicalConcerns,
    UserAdminAccessTypeMedicalMedicines,
    UserAdminAccessTypeMedicalFirstAidBoxes,
    
    //business and legal
    UserAdminAccessTypeBusinessLegalAccounts,
    UserAdminAccessTypeBusinessLegalContracts,
    UserAdminAccessTypeBusinessLegalPermissions,
    UserAdminAccessTypeBusinessLegalPolicies,
    
    //galleries
    UserAdminAccessTypeGalleriesPhoto,
    UserAdminAccessTypeGalleriesPhotoComments,
    UserAdminAccessTypeGalleriesPhotoStatistics,
    UserAdminAccessTypeGalleriesPhotoBulkDownload,
    UserAdminAccessTypeGalleriesVideo,
    UserAdminAccessTypeGalleriesVideoComments,
    UserAdminAccessTypeGalleriesVideoStatistics,
    
    //communicate
    UserAdminAccessTypeCommunicateOverview,
    UserAdminAccessTypeCommunicateComplimentsComplaints,
    UserAdminAccessTypeCommunicateNewsletters,
    UserAdminAccessTypeCommunicateQuestionnaires,
    UserAdminAccessTypeCommunicateQuickMessages,
    UserAdminAccessTypeCommunicatePrivateMessages
    
};

@interface UserAccessTypeModel : AXBaseModel

@property (nonatomic, assign) UserAccessType type;

- (instancetype)initWithType:(UserAccessType)type;

@end
