//
//  DailyNotesCommentModel.h
//  pd2
//
//  Created by i11 on 08/06/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AXBaseModel.h"

@interface DailyNotesCommentModel : AXBaseModel

@property (nonatomic, strong) NSNumber *commentId;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *photoPath;
@property (nonatomic, strong) NSString *text;


@property (nonatomic, strong) NSDate *date;

- (instancetype)initWithDict:(NSDictionary *)dict;
- (void)updateWithDict:(NSDictionary *)dict;


@end
