#import "DSDatePicker.h"

@class DateTextField;
@protocol DateTextFieldDelegate <NSObject>
@required
- (void) dateTextFieldTap:(DateTextField*)textField;

@end

@interface DateTextField : UITextField <UITextFieldDelegate>
@property (nonatomic) kDatePickerType type;
@property (nonatomic, weak) id<DateTextFieldDelegate> textDelegate;
@property (strong, nonatomic) NSNumber * year, *month, *day;

@property (strong, nonatomic) NSDate *itsDate;

//@property (assign, nonatomic) BOOL isPrintableDefault;
//@property (assign, nonatomic) BOOL isPrintableInput;

@property (strong, nonatomic) NSLocale *en_USLocale;

- (void) setType:(kDatePickerType)type date:(NSString*)date;
- (void) typeForDateObject:(NSDate *)date; //для поддержки timeFormat
- (void) setActive:(BOOL)active;
- (void) show:(BOOL)force;

@end
