//
//  ChildrenBirthdaysVC.h
//  pd2
//
//  Created by Eugene Fozekosh on 25.06.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Pd2TableViewController.h"

@interface ChildrenBirthdaysVC : Pd2TableViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@end
