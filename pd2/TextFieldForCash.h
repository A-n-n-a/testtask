//
//  TextFieldForCash.h
//  pd2
//
//  Created by Vasyl Balazh on 29.05.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldForCash : UITextField <UITextFieldDelegate>

@property (nonatomic, assign) BOOL shouldUpdateAttributes;

@end
