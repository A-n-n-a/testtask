//
//  DailyNotesOverviewParentSideViewController.m
//  pd2
//
//  Created by Andrey on 24/01/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesOverviewParentSideViewController.h"
#import "DailyNotesChildrenNotesListParentSideViewController.h"

#import "DailyNotesOverviewChildrenParentSideCell.h"


#import "DailyNotesChildrenParentSideModel.h"


#define CHILDREN_SECTIONS_TAG @"childrenSectionTag"

@interface DailyNotesOverviewParentSideViewController () <UITextFieldDelegate>

{
    
    NSString *PDChildrenSection;
    NSString *PDChildrenHeader;
    
    NSString *PDChildrenCell;
    
    NSString *PDSpacer35Cell;
    NSString *PDSpacer45Cell;
    
}


@end

@implementation DailyNotesOverviewParentSideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initRows];
    [self initLabels];
    AXSection *topSection = [[AXSection alloc] initWithType:PDTopSection];
    [topSection addRow:[self.sections genRowWithType:PDInfoCell]];
    [self.sections addSection:topSection];
    
    [self refreshData];
    
    
    AXSection *bottomSection = [[AXSection alloc] initWithType:PDBottomSection];
    [bottomSection addRow:[self.sections genRowWithType:PDSpacer35Cell]];
    
    [self.sections addSection:bottomSection];
    
    self.topTitle = @"Daily Notes";
    self.view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];

    
}


- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if ([[DataStateManager sharedManager] isChanged:DataStateDailyNotes forViewController:self]) {
        
        [self refreshData];
        
    }
    
    
}



- (void)initRows {
    [super initRows];
    
    PDChildrenSection = @"PDChildrenSection";
    PDChildrenHeader = @"PDChildrenHeader";
    
    PDChildrenCell = @"PDChildrenCell";
    
    PDSpacer35Cell = @"PDSpacer35Cell";
    PDSpacer45Cell = @"PDSpacer45Cell";
    
    
    [self.sections rememberType:PDInfoCell withEstimatedHeight:180.f identifier:@"infoCell"];
    

    [self.sections rememberType:PDChildrenHeader withHeight:44.f identifier:@"childrenHeaderCell"];
    [self.sections rememberType:PDChildrenCell withEstimatedHeight:173.f identifier:@"childrenCell"];
    
    [self.sections rememberType:PDSpacer35Cell withEstimatedHeight:35.f identifier:@"spacerCell"];
    [self.sections rememberType:PDSpacer45Cell withEstimatedHeight:45.f identifier:@"spacerCell"];
    
    [self.infoCells addObject:PDInfoCell];
    [self.contentFitCells addObject:PDChildrenCell];

}


#pragma mark - Configure cells


- (UIView *)viewForPDChildrenHeader:(AXHeader *)header inSection:(AXSection *)section {
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:header.identifier];
    UIView *view = cell.contentView;
    
    UIImageView *arrowImageView = (UIImageView *)[view viewWithTag:HEADER_ARROW_IMAGE_VIEW_TAG];
    [self updateHeaderArrowImage:arrowImageView withState:section.isHide];
    
    
    UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
    
    
    [self addBorder:PDBorderTopAndBottom toView:containerView forToolbar:NO];
    
    
    return view;
    
}






- (UITableViewCell *)configurePDChildrenCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    DailyNotesOverviewChildrenParentSideCell *cell = (DailyNotesOverviewChildrenParentSideCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        DailyNotesChildrenParentSideModel *model = row.model;
        
        cell = [self offscreenRenderingCell:cell withRow:row];
        [cell updateImageWithModel:model];
        
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        
        [self addBorder:PDBorderTop toView:cell.toolbarView forToolbar:NO];
        [self addBorder:PDBorderTopAndBottomDynamic toView:containerView forToolbar:NO];

        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}




#pragma mark - OffScreen rendering


- (id)offscreenRenderingPDChildrenCell:(DailyNotesOverviewChildrenParentSideCell *)cell withRow:(AXRow *)row {
    
    DailyNotesChildrenParentSideModel *model = row.model;
    [cell updateWithModel:model];
    
    return cell;
}



#pragma mark - Remote

- (void)refreshData {
    
    [[DataStateManager sharedManager] updated:DataStateDailyNotes forViewController:self];

    
    [self cleanForRefresh];
    
    __weak __typeof(self)weakSelf = self;
    
    [[NetworkManager sharedInstance] dailyNotesParentSideOverviewAndShowProgressHUD:YES withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            if([[response valueForKeyPath:@"result.status"] intValue] == 1)
            {
                NSArray *childrens = [response valueForKeyPath:@"data"];
                //NSArray *notes = [response valueForKeyPath:@"data.panding"];
                [strongSelf updateTableWithData:childrens];
            }
            else
            {
                Alert(@"Error",[response valueForKeyPath:@"result.message"] );
            }
        }
        else if(error)
        {
            Alert(@"Error", @"Error happened while operation");
        }
    }];
    
}




- (void)cleanForRefresh {
    
    //Очищаем mainSection
    NSIndexSet *indexSet = [self.sections getIndexSetForSectionsWithType:PDMainSection];
    [self.sections removeSectionsByIndexSet:indexSet];
    
    indexSet = [self.sections getIndexSetForSectionsWithType:PDChildrenSection];
    [self.sections removeSectionsByIndexSet:indexSet];
    
    [self.tableView reloadData];
}


#pragma mark - Remote processing

- (void)updateTableWithData:(NSArray *)data {
    
    if (!data || ![data isKindOfClass:[NSArray class]]) {
        [self cleanForRefresh];
        [self insertMessageCell]; //Empty default
        return;
    }
    
    
    
    NSMutableArray *modelsPrepared = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in data) {
        
        DailyNotesChildrenParentSideModel *model = [[DailyNotesChildrenParentSideModel alloc] initWithDict:dict];
        
        if (model && [model isExist]) {
            [modelsPrepared addObject:model];
        }
    }
    
    
    AXSection *mainSection = [[AXSection alloc] initWithType:PDMainSection];
    [mainSection addRow:[self.sections genRowWithType:PDSpacerCell]];

    AXSection *childrenSection = [[AXSection alloc] initWithType:PDChildrenSection];
    childrenSection.isHide = NO;
    AXHeader *childrenHeader = [self.sections genHeaderWithType:PDChildrenHeader];
    childrenSection.header = childrenHeader;
    
    if ([modelsPrepared count]) {
        for (DailyNotesChildrenParentSideModel *model in modelsPrepared) {
            AXRow *row = [self.sections genRowWithType:PDChildrenCell];
            row.model = model;
            [childrenSection addRow:row];
        }
    }
    else {
        [childrenSection addRow:[self.sections genRowWithType:PDMessageCell]];
    }
    
    [self.sections insertSections:@[mainSection, childrenSection] afterSectionType:PDTopSection];

    
    [self.tableView reloadData];

    
}


- (void)cleanForRefreshWithAnimation {
    
    //Очищаем mainSection
    NSIndexSet *indexSet = [self.sections getIndexSetForSectionsWithType:PDMainSection];
    [self.sections removeSectionsByIndexSet:indexSet];
    
    
    [self.tableView beginUpdates];
    
    if ([indexSet count] > 0) {
        [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.tableView endUpdates];
    
    //[self.tableView reloadData];
}





#pragma mark - Actions


- (IBAction)viewChildAction:(id)sender {
    
    [self performSegueWithIdentifier:@"viewSegue" sender:sender];
    
}









#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

   if ([segue.identifier isEqualToString:@"viewSegue"]) {
        
        AXRow *row = [self rowForView:sender];
        
        DailyNotesChildrenParentSideModel *model = row.model;
        
        if (!model) {
            return;
        }
        
        DailyNotesChildrenNotesListParentSideViewController *viewController = segue.destinationViewController;
        viewController.childrenModel = model;
    }
    
    
}



@end
