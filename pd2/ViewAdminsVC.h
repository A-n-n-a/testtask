//
//  ViewAdminsVC.h
//  pd2
//
//  Created by User on 17.07.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "Pd2ViewController.h"

@interface ViewAdminsVC : Pd2ViewController

@property (nonatomic,retain) NSMutableArray *adminsArray;
@property (nonatomic,retain)IBOutlet UITableView *tableView;

-(IBAction)deleteAdmin:(id)sender;


@end
