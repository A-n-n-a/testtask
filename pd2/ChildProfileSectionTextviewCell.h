//
//  ChildProfileSectionTextviewCell.h
//  pd2
//
//  Created by User on 22.03.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChildProfileSectionTextviewCell : UITableViewCell

@property (nonatomic,retain)IBOutlet UILabel *profileSectionTitleLabel;
@property (nonatomic,retain)IBOutlet UITextView *profileSectionTextView;

@end
