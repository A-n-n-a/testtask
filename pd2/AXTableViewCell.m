//
//  AXTableViewCell.m
//  pd2
//
//  Created by i11 on 08/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AXTableViewCell.h"
#import "Utils.h"
#import "NSDate+MonthAge.h"
#import "NetworkManager.h"

@implementation AXTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (UIView *)containerView {
    
    return [self.contentView viewWithTag:CONTAINER_VIEW_TAG];
}


- (void)updateProfileImageView:(UIImageView *)imageView withImagePath:(NSString *)imagePath andDefaultImagePath:(NSString *)defaultImagePath downloadImageCompletition:(void(^)(UIImage *downloadedImage))downloadImageCompletion {

    imageView.image = [UIImage imageNamed:@"profile-photo-loading"];
    
    if (imagePath) {
        //Он и так weak должен быть задан
        [[Utils sharedInstance] downloadImageAtRelativePath:imagePath completionHandler:^(UIImage *image) {
            if (image) {
                imageView.image = image;
                
                if (downloadImageCompletion) {
                    downloadImageCompletion(image);
                }
                
            }
            else {
                if (defaultImagePath) {
                    [self updateImageView:imageView withDefaultImagePath:defaultImagePath];
                }
                else {
                    //error
                    imageView.image = [UIImage imageNamed:@"profile-photo-loading"];
                }
            }
            
        }];
    } else if (defaultImagePath) {
        [self updateImageView:imageView withDefaultImagePath:defaultImagePath];
    }
    
    
    
    //Вузуальное оформление
    [self decorateProfileView:imageView];
}

- (void)decorateProfileView:(UIView *)view {
    
    CALayer *layer = view.layer;
    layer.masksToBounds = YES;
    layer.cornerRadius = layer.frame.size.width / 2;
    layer.borderWidth = 0.3f;
    layer.borderColor = [UIColor grayColor].CGColor;
    
}

- (void)updateImageView:(UIImageView *)imageView withDefaultImagePath:(NSString *)defaultImagePath {
    [[Utils sharedInstance] downloadImageAtRelativePath:defaultImagePath completionHandler:^(UIImage *image) {
        if (image) {
            imageView.image = image;
        }
        else {
            //error
            imageView.image = [UIImage imageNamed:@"profile-photo-loading"];
        }
    }];
}


#pragma mark - Activity Indicator

- (void)activityIndicatorSetStatus:(BOOL)status {
    
    UIView *view = [self containerView];
    
    //Изначально всегда удаляем (так как ячейка с переиспользоваением)
    [self removeActivityIndicatorFromView:view];
    if (status) {
        [self addActivityIndicatorToView:view withAnimation:NO];
    }
}

- (void)addActivityIndicatorToView:(UIView *)destinationView withAnimation:(BOOL)animation {
    
    //TODO: fade анимация
    
    CGRect destFrame = destinationView.frame;
    destFrame.origin.x = 0;
    destFrame.origin.y = 0;
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, destFrame.size.width, destFrame.size.height)];
    UIView *view = [[UIView alloc] initWithFrame:destFrame];
    view.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5f];
    view.tag = ACTIVITY_INDICATOR_VIEW_TAG;
    
    [destinationView addSubview:view];
    
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc]
                                                      initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    CGRect frame = activityIndicatorView.frame;
    frame.origin.x = view.frame.size.width / 2 - frame.size.width / 2;
    frame.origin.y = view.frame.size.height / 2 - frame.size.height / 2;
    activityIndicatorView.frame = frame;
    
    [activityIndicatorView startAnimating];
    
    if (animation) {
        
        
        [UIView transitionWithView:view duration:1.5
                           options:UIViewAnimationOptionTransitionCrossDissolve //change to whatever animation you like
                        animations:^ { [view addSubview:activityIndicatorView]; }
                        completion:nil];
    } else {
        [view addSubview:activityIndicatorView];
    }
    
    
}

- (void)removeActivityIndicatorFromView:(UIView *)destinationView {
    
    UIView *removeView;
    
    while((removeView = [destinationView viewWithTag:ACTIVITY_INDICATOR_VIEW_TAG]) != nil) {
        [removeView removeFromSuperview];
    }
    
}

#pragma mark - Utils

+ (NSString *)stringFromDate:(NSDate *)date withDateFormat:(NSString *)dateFormat {
    
    if (!date) {
        return @"N/A";
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    return [dateFormatter stringFromDate:date];
    
}

- (NSString *)stringFromDate:(NSDate *)date withDateFormat:(NSString *)dateFormat {
    return [AXTableViewCell stringFromDate:date withDateFormat:dateFormat];
}


+ (NSString *)monthsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate {
    
    if (!startDate || !endDate ) {
        return @"N/A";
    }
    
    NSString *singular = @"month";
    NSString *plural = @"months";
    NSString *string = @"";
    
    NSInteger months = [startDate monthsToDate:endDate];
    
    if (months == 1) {
        string = singular;
    }
    else {
        string = plural;
    }
    
    return [NSString stringWithFormat:@"%i %@", (int)months, string];
    
}

- (NSString *)monthsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate {
    return [AXTableViewCell monthsFromDate:startDate toDate:endDate];
}



+ (NSString *)monthsFromDate:(NSDate *)startDate {
    
    return [self monthsFromDate:startDate toDate:[NSDate date]];
    
}

- (NSString *)monthsFromDate:(NSDate *)startDate {
    
    return [AXTableViewCell monthsFromDate:startDate];
    
}


+ (NSString *)fullNameFromFirstName:(NSString *)firstName
                        andLastName:(NSString *)lastName {
    
    //Метод, чтобы не получалось "Ivan N/A" или "N/A N/A"
    
    if (firstName && lastName) {
        return [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    }
    
    if (firstName) {
        return firstName;
    }

    if (lastName) {
        return lastName;
    }
    
    return @"N/A";
    
}

- (NSString *)fullNameFromFirstName:(NSString *)firstName
                        andLastName:(NSString *)lastName {
    return [AXTableViewCell fullNameFromFirstName:[firstName capitalizedString] andLastName:[lastName capitalizedString]];
}


#pragma mark -


- (BOOL)isScottish {
    
    return [[NetworkManager sharedInstance] isScottish];
    
    
}
@end
