//
//  Pd2ViewController.m
//  pd2
//
//  Created by admin on 6/22/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "Pd2ViewController.h"
#import "MyColorTheme.h"
#import "TimerUIApplication.h"

#import "MainMenuVC.h"

#import "UnavailableSectionManager.h"
#import "WL.h"


@interface Pd2ViewController ()

@property (strong, nonatomic) NSTimer *timeOutCheatTimer;

@end


@implementation Pd2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar.layer setBorderWidth: 0];
    NSUInteger ind = [self.navigationController.viewControllers indexOfObject:self];
    NSLog(@"index of this view is %lu",(unsigned long)ind);
    if (ind!=NSNotFound&&ind>1)
    {
        UIButton *leftButton=[UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *backImage=[[UIImage imageNamed:@"back-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIImageView *iv=[[UIImageView alloc]initWithImage:backImage];
        iv.frame=CGRectMake(5, 5, 20, 20);
        [leftButton addSubview:iv];
        leftButton.frame=CGRectMake(0, 0, 30, 30);
        [leftButton addTarget:self.navigationController action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
        //leftButton.tintColor=[[MyColorTheme sharedInstance]navigaionButtonsColor];
        leftButton.tintColor=[UIColor whiteColor];
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
        self.leftButton = leftButton;
        [self setTopTitle:_topTitle];
        
        /*
        UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 300)];
        titleLabel.text = _topTitle.length?_topTitle:@"Baby's Days";
        titleLabel.textColor=[UIColor whiteColor];
        titleLabel.font=[UIFont boldSystemFontOfSize:23];
        self.navigationItem.titleView=titleLabel;*/
    }
    [self setColorTheme];
}

-(void)setTopTitle:(NSString *)topTitle
{
    _topTitle=topTitle;
    
    CGFloat barButtonWidth = 60;
    UIView * view = [[UIView alloc] initWithFrame:(CGRect){
        .origin = {barButtonWidth,0},
        .size   = {[UIScreen mainScreen].bounds.size.width - barButtonWidth*2, self.navigationController.navigationBar.frame.size.height}
    }];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:view.bounds];
    titleLabel.text          = _topTitle.length?_topTitle:[WL name];
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.minimumScaleFactor = 0.5;
    titleLabel.font          = [UIFont boldSystemFontOfSize:23];
    titleLabel.textColor     = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [view addSubview:titleLabel];
    self.navigationItem.titleView = view;
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (self.timeOutCheatTimer) {
        [self.timeOutCheatTimer invalidate];
    }

}

-(void)setColorTheme
{
    [[MyColorTheme sharedInstance]setColorThemeForViewController:self];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - InactiveTimer

- (void)disableInactiveTimer {
    
    //Сбрасываем таймер приложения
    int timeout = kApplicationTimeoutInMinutes * 60 - 1;
    self.timeOutCheatTimer = [NSTimer scheduledTimerWithTimeInterval:timeout target:self selector:@selector(timerAction) userInfo:nil repeats:NO];
}

- (void)timerAction {
    [(TimerUIApplication *)[UIApplication sharedApplication] resetIdleTimer];
}

- (void) pushViewController:(NSString *)viewControllerIdentifier
                 storyboard:(NSString *)storyboardName
{
    @try {
        UIStoryboard * nextStepsStoryboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
        UIViewController * viewController = [nextStepsStoryboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    @catch (NSException *exception) {}
}

#pragma mark - back to main menu button


- (void)addHomeButton {
    
    UIButton *rightButton=[UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *homeImage=[[UIImage imageNamed:@"home-icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImageView *ivHome=[[UIImageView alloc]initWithImage:homeImage];
    ivHome.frame=CGRectMake(5, 5, 20, 20);
    [rightButton addSubview:ivHome];
    rightButton.frame=CGRectMake(0, 0, 30, 30);
    [rightButton addTarget:self action:@selector(popToMenu:) forControlEvents:UIControlEventTouchUpInside];
    //rightButton.tintColor=[[MyColorTheme sharedInstance]navigaionButtonsColor];
    rightButton.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:rightButton];
}

- (void)popToMenu:(id)sender {
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        if ([controller isKindOfClass:[MainMenuVC class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            return;
        }
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

#pragma mark - Push Failure alert
//Алерт о некорректном переходе по пушу. Когда не удалось что то подгрузить
- (void)showPushFailureAlert {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Something went wrong" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    
}

@end
