//
//  DataManager.h
//  pd2
//
//  Created by User on 22.07.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "Singleton.h"

@interface DataManager : Singleton
{
    NSDictionary *diarySectionsDictionary;
}

@property int adminId, adminAccessLevel;
@property NSMutableDictionary *adminInfo;

@property (nonatomic,retain)NSMutableArray *positions;
@property (nonatomic,retain)NSMutableArray *admins;
@property (nonatomic,retain)NSMutableArray *rooms;
@property (nonatomic,retain)NSMutableArray *profileSections;
@property (nonatomic,retain)NSMutableArray *childrenList;
@property (nonatomic,retain)NSMutableArray *childrenForParentalAccess;
@property (nonatomic,retain)NSMutableArray *diaryHelpers;


//positions
-(NSString *)getPositionTitleById:(int)positionId;
-(int)idOfPositionWithTitle:(NSString *)title;
-(NSMutableArray*)positionList;
-(NSMutableArray*)positionListWithoutPosition:(NSString*)string;
-(BOOL)positionExistsWithName:(NSString *)positionName;

//admins
-(NSString *)adminNameWithId:(int)adminId;
-(NSString *)adminFullNameWithId:(int)adminId;
-(NSString *)getAdminLevelTitleById:(int)levelId;
-(int)getNumberOfAdminsWithPositions:(int)adminPositions;
-(BOOL)adminExistsWithEmail:(NSString *)email;
-(int)getAclForCurrentUser;
-(int)getRoomNumForCurrentRoomAdmin;
-(BOOL)adminExistsWithUserName:(NSString *)username;


//rooms
-(NSArray*)roomList;
-(NSString*)getRoomTitleWithId:(int)roomId;
-(int)roomIdWithTitle:(NSString*)title;
-(NSString *)roomImagePathWithTitle:(NSString*)title;
-(NSArray*)roomListWithoutRoom:(NSString*)roomTitle;
-(bool)roomExistsWithName:(NSString*)roomName roomId:(int)roomId;
-(NSDictionary*)roomIDsWithImagePaths;

//children
-(NSDictionary *)infoAboutChildWithId:(int)childId;

//diary sections
-(NSArray *)diarySectionsTitles;
-(NSString *)diarySectionTitleForKey:(NSString *)key;
-(NSString *)diarySectionKeyForTitle:(NSString *)title;

//parental access
-(NSString*)childNameWithId:(int)childId;
//-(int)childIdWithName:(NSString*)name;

-(NSArray *)childrenList;
-(NSArray *)childrenWithoutChild:(int)childId;
-(NSArray *)childrenIdList;

//diary helpers
- (NSArray*)diaryHelpersOfCategory:(NSString*)category;
- (NSArray*)diaryHelpersTitlesOfCategory:(NSString*)category;
- (NSArray*)helperItemsOfHelper:(NSString *)helperTitle ofCategory:(NSString*)category;

-(bool)profileSectionExistWithName:(NSString*)name profileSectionId:(int)profileSectionId;

-(void)refresh;

-(NSString *) getVideoIdFromUTubeURL:(NSString*)uTubeURL;

//Months
-(NSArray *)getMonthsList;
-(NSArray *)getShortMonthsList;
-(NSString *)monthNameFromNum:(int)monthNum;
-(NSString *)monthShortNameFromNum:(int)monthNum;
-(int)monthNumFromName:(NSString*)monthStr;



- (NSArray *)yearsList;
- (NSArray *)hoursList;
- (NSArray *)minutesList;


- (NSArray *)ageDueList;

@end
