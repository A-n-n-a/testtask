//
//  ChildrenBirthdaysModel.h
//  pd2
//
//  Created by Eugene Fozekosh on 04.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "BaseModel.h"

@interface ChildrenBirthdaysModel : BaseModel

@property (strong, nonatomic) NSString *childFirstName;
@property (strong, nonatomic) NSString *childLastName;
@property (strong, nonatomic) NSString *childImage;
@property (strong, nonatomic) NSString *childRoomID;
@property (strong, nonatomic) NSString *childID;
@property (strong, nonatomic) NSString *childRoomName;
@property (strong, nonatomic) NSString *childBirthday;
@property (strong, nonatomic) NSDate *childBirthdayDate;
@property (strong, nonatomic) NSString *childNextAge;

- (void)updateFromDictionary:(NSDictionary*)dict;

@end
