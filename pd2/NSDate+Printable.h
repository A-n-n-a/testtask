//
//  NSDate+Printable.h
//  pd2
//
//  Created by Andrey on 09/01/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Printable)

typedef NS_ENUM(NSUInteger, DateType) {
    DateTypeDefault
};

- (NSString *)reallyDefault;
- (NSString *)reallyDefaultTime;
- (NSString *)reallyDefaultWithTime;
- (NSString *)reallyDefaultWithWeekDay;

- (NSString *)printableDefault;
- (NSString *)printableDefaultWithTime;
- (NSString *)printableDefaultWithAtTime;
- (NSString *)printableMonthShortYear;
- (NSString *)printableDefaultInput;
- (NSString *)printableDefaultTime;
- (NSString *)printableDefaultWithWeekDay;
- (NSString *)printableDefaultTmp;
- (NSString *)printableDefaultDayMonth;
- (NSString *)printableMonthYear;
- (NSString *)printableMonth;
- (NSString *)printableBirthday;
- (NSString *)printableDefaultTimeDate;
- (NSString *)printablePermissionsSign;

- (NSString *)printableWithSlaches;
- (NSString *)printableWithDateSlachesTime;

- (NSString *)printableDayMonth;

+ (NSString *)printableShortMonthForIndex:(int)monthNum;
+ (NSString *)printableMonthForIndex:(int)monthNum;



- (NSString *)printableRegisterDayMonthYear;
+ (NSString *)printableRegisterIntervalForStartDate:(NSDate *)startDate endDate:(NSDate *)endDate;

- (void)testDateFormates;


@end
