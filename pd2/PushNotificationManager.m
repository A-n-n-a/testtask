//
//  PushNotificationManager.m
//  pd2
//
//  Created by Andrey on 04/10/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "PushNotificationManager.h"
#import "PushNotificationModel.h"
#import "PushEYOModel.h"

#import "UserManager.h"



@interface PushNotificationManager ()

@property (nonatomic, strong) NSMutableArray *pushes;

@property (nonatomic, assign) BOOL isShowAlert;

@end


@implementation PushNotificationManager




- (id)init {
    self = [super init];
    if (self) {
        self.pushes = [[NSMutableArray alloc] init];
    }
    return self;
}



- (void)addPush:(NSDictionary *)dict andBackground:(BOOL)isBackground{
    
    PushNotificationModel *model = [[PushNotificationModel alloc] initWithDict:dict];
    model.isBackground = isBackground;
    
    if ([model isExist]) {
        [self.pushes addObject:model];
    }
    
    [self processPush];
    
}

- (void)processPush {
    
    if (self.isShowAlert || ![self.pushes count]) {
        return;
    }

    PushNotificationModel *model = [self.pushes firstObject];
    
    //Проверка host!
    
    //Если пуш из бекграунда, то просто переходим
    if (model.isBackground) {
        [self.pushes removeObject:model];
        self.currentPush = model;
        [self jump];
        return;
    }
    
    self.isShowAlert = YES;
    
    if (model.pushAction == PushNotificationRegisterIn || model.pushAction == PushNotificationRegisterOut) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Notification" message:model.message preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.pushes removeObject:model];
            self.isShowAlert = NO;
            [self processPush];
        }];
        
        [alert addAction:ok];
        
        [[AppDelegate pTopViewController] presentViewController:alert animated:YES completion:nil];

    }
    else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Notification" message:model.message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self.pushes removeObject:model];
            self.isShowAlert = NO;
            [self processPush];
        }];
        [alert addAction:cancel];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"View" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.pushes removeObject:model];
            self.isShowAlert = NO;
            self.currentPush = model;
            [self jump];
            [self processPush];
            
        }];
        [alert addAction:ok];
        
        [[AppDelegate pTopViewController] presentViewController:alert animated:YES completion:nil];
    }
    
    
    
    
}

- (void)jump {
    
    
    if (!self.currentPush) {
        return;
    }
    
    PushNotificationModel *push = self.currentPush;
    PushNotificationAction action = push.pushAction;
    
    User *user = [UserManager sharedManager].loggedUser;
    UserType userType = user.userType;
    
    //Не залогинены (ждем пока залогинится)
    if (!user) {
        return;
    }
    
    //Залогинены не под тем и не там
    NSLog(@"h=%@", [NetworkManager sharedInstance].subdomain);
    NSLog(@"uid=%i, puid=%i", user.uid, [user.puid intValue]);
    if (![self isLoggedUserWithId:push.userId] || !([[NetworkManager sharedInstance].subdomain isEqualToString:push.host]))  {
        /*
        //Удалять переход
        self.currentPush = nil;
        //Удаляем очередь
        self.pushes = [NSMutableArray array];
        */
        [self clearQueue];
        return;
    }
    
   
    UINavigationController *navigationController = [AppDelegate pTopViewController].navigationController;
    if (!navigationController) {
        NSLog(@"Не получен Navigation Controller для Push");
        self.currentPush = nil;
        return;
    }
    

    
    
    
    //[[AppDelegate pTopViewController] presentViewController:alert animated:YES completion:nil];
    self.currentPush = nil;
    
}




- (BOOL)isLoggedUserWithId:(NSNumber *)uid {
    
    User *user = [UserManager sharedManager].loggedUser;
    
    if (user.uid == [uid intValue]) {
        return YES;
    }
    
    /*
    if (user.userType == UserTypeAdmin) {
        if (user.uid == [uid intValue]) {
            return YES;
        }
    }
    else if (user.userType == UserTypeParent) {
        if ([user.puid intValue] == [uid intValue]) {
            return YES;
        }
    }
    */
    return NO;
    
}


- (void)clearQueue {
    
    self.currentPush = nil;
    self.pushes = [NSMutableArray array];
    
}





@end
