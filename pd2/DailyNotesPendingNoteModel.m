//
//  DailyNotesPendingNoteModel.m
//  pd2
//
//  Created by Andrey on 09/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesPendingNoteModel.h"


@implementation DailyNotesPendingNoteModel
/*
{
 "id": "15",
 "child_id": "0",
 "note": "yuiy",
 "date_added": "2017-10-03 08:59:47",
 "title": "24th Oct 2017 @ 10:43",
 "added_by": "677",
 "pending": "1",
 "masterId": "0",
 "notes": "1",
 "allChildIds": "164,169",
 "children_count": "2"
}
*/
- (instancetype)initWithDict:(NSDictionary *)dict {
    
    self = [super initWithDict:dict];
    if (self) {
        
        self.noteId = [self safeNumber:dict[@"id"]];
        self.noteTitle = [self safeString:dict[@"title"]];
        self.noteBox = [self safeInt:dict[@"notes"]];
        self.date = [self dateFrom:dict[@"date_added"] withDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        self.childrenCount = [self safeInt:dict[@"children_count"]];
        
        /*
        NSString *childs = [self safeString:dict[@"childIds"]];
        if (childs) {
            self.childrenCount = (int)[[childs componentsSeparatedByString:@","] count];
        }
        */
    
    }
    return self;
    
}

- (BOOL)isExist {
    
    if (self.noteId) {
        return YES;
    }
    
    return NO;
    
}




@end
