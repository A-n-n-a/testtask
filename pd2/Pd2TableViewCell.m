#import "Pd2TableViewCell.h"
#import "DateTextField.h"

@interface Pd2TableViewCell()<DateTextFieldDelegate>
@end

@implementation Pd2TableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self findTargetFields];
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    // Use to find dynamically added textfields and textviews
    [self findTargetFields];
}

- (void) findTargetFields
{
    self.array_textFields = [NSMutableArray new];
    self.array_textViews  = [NSMutableArray new];
    [self findTargetFieldsInView:self.contentView];
    
    for (UITextView * textView in self.array_textViews) {
        textView.delegate = self;
    }
    for (UITextField * textField in self.array_textFields) {
        if ([textField isMemberOfClass:[DateTextField class]]) {
            ((DateTextField *)textField).textDelegate = self;
        } else {
            textField.delegate = self;
        }
    }
}

- (void) findTargetFieldsInView:(UIView *)view
{
    for (id object in view.subviews) {
        if ([object isKindOfClass:[UITextField class]]) {
            [self.array_textFields addObject:object];
        } else if ([object isKindOfClass:[UITextView class]]) {
            [self.array_textViews addObject:object];
        } else if ([object isKindOfClass:[UIView class]]) {
            [self findTargetFieldsInView:object];
        }
    }
}

- (void) sendBegindEditingField:(id)field
{
    if (self.scrollKeyboardDelegate) {
        [self.scrollKeyboardDelegate pd2TableViewCell:self beginEditingField:field];
    }
}



#pragma mark - DateTextFieldDelegate -

- (void) dateTextFieldTap:(DateTextField *)textField
{
    [self sendBegindEditingField:textField];
}



#pragma mark - UITextViewDelegate -

- (void) textViewDidChange:(UITextView *)textView
{
    [self sendBegindEditingField:textView];
}

- (void) textViewDidBeginEditing:(UITextView *)textView
{
    [self sendBegindEditingField:textView];
}



#pragma mark - UITextFieldDelegate -

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    [self sendBegindEditingField:textField];
}


@end
