//
//  ImageCache.h
//  pd2
//
//  Created by User on 27.11.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "Singleton.h"

@interface ImageCache : Singleton

@property (nonatomic,retain)NSMutableDictionary *cache;

-(void)saveImage:(UIImage *)image fromLink:(NSString*)link;

-(UIImage *)imageFromLink:(NSString*)link;

-(void)deleteImageForLink:(NSString*)link;

@end
