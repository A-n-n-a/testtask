//
//  UIImage+ContainsImage.h
//  pd2
//
//  Created by Andrey on 05/02/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ContainsImage)

- (BOOL)containsImage;

@end
