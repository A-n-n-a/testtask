#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, SectionChildModelSourceType) {
    SectionChildModelSourceTypeProgress,
    SectionChildModelSourceTypeNextSteps,
    SectionChildModelSourceTypeWellbeing,
    SectionChildModelSourceTypeParentProgress,
    SectionChildModelSourceTypeParentWellbeing
};

@interface SectionChildModel : NSObject
@property (nonatomic, strong) NSNumber * child_id;
@property (nonatomic, strong) NSString * firstName;
@property (nonatomic, strong) NSString * lastName;
@property (nonatomic, strong) NSString * roomTitle;
@property (nonatomic, strong) NSNumber * roomId;
@property (nonatomic, strong) NSString * dateOfBirthString;
@property (nonatomic, strong) NSDate * dateOfBith;
@property (nonatomic, strong) NSNumber * monthsCount;
@property (nonatomic, strong) NSString * photoPath;

- (void) loadInfoWithSourceType:(SectionChildModelSourceType)sourceType fromDict:(NSDictionary*)childInfo;

@end
