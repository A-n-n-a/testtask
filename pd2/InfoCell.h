//
//  infoCell.h
//  pd2
//
//  Created by i11 on 06/02/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@property (weak, nonatomic) IBOutlet UIImageView *childImageView;
@property (weak, nonatomic) IBOutlet UITextView *infoTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoTextViewHeightConstraint;

@property (assign, nonatomic) CGFloat height;
@property (assign, nonatomic) CGFloat textViewHeight;


@end
