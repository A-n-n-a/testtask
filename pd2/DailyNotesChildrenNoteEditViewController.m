//
//  DailyNotesChildrenNoteEditViewController.m
//  pd2
//
//  Created by Andrey on 17/01/2016.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesChildrenNoteEditViewController.h"

#import "DailyNotesChildrenModel.h"

#define NAME_TEXT_FIELD_TAG 480
#define NOTE_TEXT_VIEW_TAG 490

@interface DailyNotesChildrenNoteEditViewController () <UITextFieldDelegate, UITextViewDelegate>

{
    
    NSString *PDNoteCell;
    
}


@end

@implementation DailyNotesChildrenNoteEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initRows];
    [self initLabels];

    AXSection *topSection = [[AXSection alloc] initWithType:PDTopSection];
    [topSection addRow:[self.sections genRowWithType:PDInfoPicCell]];
    [topSection addRow:[self.sections genRowWithType:PDSpoilerCell]];
    [self.sections addSection:topSection];
    
    AXSection *mainSection = [[AXSection alloc] initWithType:PDMainSection];
    [mainSection addRow:[self.sections genRowWithType:PDNoteCell]];
    [mainSection addRow:[self.sections genRowWithType:PDSpacerCell]];
    [self.sections addSection:mainSection];
    
    [self.freeTapGestureRecognizer setEnabled:YES];
    
    self.topTitle = @"Edit Daily Note";
    
    self.view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;

}

- (void)viewWillAppear:(BOOL)animated {
    
    //"выключаем лишний scroll"
    //[super viewWillAppear:animated];
    
    //альтернативный метод скролла для длинных ячеек когда вызываем
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShow:(NSNotification *)note {
    
    CGSize keyboardSize = [[[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height) + 20.f, 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width) + 20.f, 0.0);
    }
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
}


- (void)keyboardWillHide:(NSNotification *)note {
    
    self.tableView.contentInset = UIEdgeInsetsZero;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    
}


- (void)initRows {
    [super initRows];
    
    
    PDNoteCell = @"PDNoteCell";
    
    [self.sections rememberType:PDInfoPicCell withEstimatedHeight:180.f identifier:@"infoPicCell"];
    [self.sections rememberType:PDSpoilerCell withHeight:57.f identifier:@"spoilerCell"];
    [self.sections rememberType:PDNoteCell withHeight:271.f identifier:@"noteCell"];
    
    [self.infoCells addObject:PDInfoPicCell];
    [self.infoCells addObject:PDInfoSecondCell];

}


#pragma mark - Configure cells

- (InfoPicCell *)configurePDInfoPicCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    InfoPicCell *cell = (InfoPicCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        cell = [self offscreenRenderingCell:cell withRow:row];
        [cell updateImageWithImagePath:self.childrenModel.photoPath];
        
        [self addBorder:PDBorderTopAndBottomDynamic toView:cell.containerView forToolbar:NO];
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}


- (UITableViewCell *)configurePDNoteCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        
        UITextField *nameTextField = (UITextField *)[cell viewWithTag:NAME_TEXT_FIELD_TAG];
        UITextView *noteTextView = (UITextView *)[cell viewWithTag:NOTE_TEXT_VIEW_TAG];

        nameTextField.text = [self.nameString firstLetterUppercaseString];
        noteTextView.text = self.noteString;
    
        
        nameTextField.delegate = self;
        noteTextView.delegate = self;
        
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}

#pragma mark - OffScreen rendering

- (id)offscreenRenderingPDInfoPicCell:(InfoPicCell *)cell withRow:(AXRow *)row {
    
    DailyNotesChildrenModel *model = self.childrenModel;
    
    //NSString *firstAndLastName = @"N/A";
    
    /*
    if (model.firstName && model.lastName) {
        firstAndLastName = [NSString stringWithFormat:@"%@ %@", [model.firstName capitalizedString], [model.lastName capitalizedString]];
    }
    */
    
    
    NSMutableAttributedString *attributedString = [self attributedStringWithKey:PDInfoLabel forObject:cell.infoTextView];
    /*
    [self replace:@"%firstAndLastName%"
       withString:firstAndLastName
inAttributedString:&attributedString];
    */
    
    [self replace:@"%title%"
       withString:notAvailStringIfNil(self.nameOrigString)
inAttributedString:&attributedString];
    
    [self replace:@"%age%"
       withString:[AXTableViewCell monthsFromDate:model.dateOfBirth]
inAttributedString:&attributedString];
    
    [self replace:@"%dateOfBirth%"
       withString:notAvailStringIfNil([model.dateOfBirth printableDefault])
inAttributedString:&attributedString];
    
    [self replace:@"%roomName%"
       withString:notAvailStringIfNil(model.roomName)
inAttributedString:&attributedString];
    
    cell.infoTextView.textContainer.exclusionPaths = @[[cell picBezierPath]];
    
    
    if  (![cell.infoTextView.attributedText isEqualToAttributedString:attributedString]) {
        cell.infoTextView.attributedText = attributedString;
        [row needUpdateHeight];
    }
    
    return cell;
}



#pragma mark - UITextFieldDelegate


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField.tag == NAME_TEXT_FIELD_TAG) {
        self.nameString = text;
    }
    
    return YES;
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return NO;
    
}



#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    [self scrollToCursorForTextView:textView];
    
}


- (void)textViewDidChange:(UITextView *)textView {
    
    [self scrollToCursorForTextView:textView];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSString *newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    
    if (textView.tag == NOTE_TEXT_VIEW_TAG) {
        
        self.noteString = newText;
    }

    return YES;
}


#pragma mark - remote



- (void)remoteSave {
    
    
    
    [[NetworkManager sharedInstance] dailyNotesEditNoteWithId:self.noteId title:self.nameString note:self.noteString showProgressHUD:YES withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            
            if([[response valueForKeyPath:@"result.success"] intValue] == 1)
            {
                
                [[DataStateManager sharedManager]changed:DataStateDailyNotes];
                
                
                [AppDelegate showCompletedHUDWithCompletionBlock:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                
                
            }
            else
            {
                Alert(@"Error",[response valueForKeyPath:@"result.message"] );
                
            }
        }
        else if(error)
        {
            Alert(@"Error", @"Error happened while operation");
            
        }
    }];
    
}


#pragma mark - Actions

- (IBAction)sendAction:(id)sender {
    

    
    if (!self.nameString || self.nameString.isEmpty) {
        Alert(@"Error", @"Please complete Title");
        return;
    }

    if (!self.noteString || self.noteString.isEmpty) {
        Alert(@"Error", @"Please complete Description");
        return;
    }
    
    
    
    [self remoteSave];
    
    
}





@end
