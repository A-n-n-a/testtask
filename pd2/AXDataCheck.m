//
//  AXDataCheck.m
//  pd2
//
//  Created by i11 on 23/04/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AXDataCheck.h"

@implementation AXDataCheck


- (NSString *)stringFrom:(id)data {
    
    if (!data) {
        return @"Nil!";
    } else if ([data isEqual:[NSNull null]]) {
        return @"NULL!";
    } else if (![data isKindOfClass:[NSString class]]) {
        return @"NOT STRING!";
    } else if ([(NSString *)data isEqualToString:@""]) {
        return @"EMPTY STRING!";
    }
    
    return (NSString *)data;
    
}

+ (BOOL)checkObject:(id)object {
    
    if (!object) {
        return NO; //nil
    } else if ([object isEqual:[NSNull null]]) {
        return NO;
    }
    
    return YES;
}

+ (BOOL)checkDictionaryObjects:(NSDictionary *)dict onlyKeys:(NSArray *)onlyKeys exceptKeys:(NSArray *)exceptKeys {
    
    if (![dict isKindOfClass:[NSDictionary class]]) {
        return NO;
    }
    
    int onlyKeysChechedCounter = 0;
    
    for (id key in [dict allKeys]) {
        
        //Проверяем только помеченные к проверке
        if (onlyKeys && ![onlyKeys containsObject:key]) {
            continue;
        }
        
        //Исключаем из проверки отмеченные
        if (exceptKeys && [exceptKeys containsObject:key]) {
            continue;
        }
        
        //Кто прошел отбор - проверяем
        id obj = [dict objectForKey:key];
        if ([AXDataCheck checkObject:obj]) {
            if (onlyKeys && [onlyKeys containsObject:key]) {
                onlyKeysChechedCounter++;
            }
        }
        else {
            NSLog(@"Не прошел проверку! dict: %@, obj key: %@", dict, key);
            return NO;
        }
        
    }
    
    if (onlyKeys) {
        if ([onlyKeys count] == onlyKeysChechedCounter) {
            return YES;
        }
        else {
            return NO;
        }
    }

    return YES;
}

//TODO: giveMeBool

#pragma mark - Safe types

+ (NSNumber *)safeNumber:(id)data {

    if (![AXDataCheck checkObject:data]) {
        return nil;
    }
    
    
    if ([data isKindOfClass:[NSString class]]) {
        //Мы не знаем приходящий формат (int, float, etc). Выясняем
        
        if ([(NSString *)data isEqualToString:@"off"] || [(NSString *)data isEqualToString:@"no"] || [(NSString *)data caseInsensitiveCompare:@"no"] == NSOrderedSame) {
            return @NO;
        }
        else if ([(NSString *)data isEqualToString:@"on"] || [(NSString *)data isEqualToString:@"yes"] || [(NSString *)data caseInsensitiveCompare:@"yes"] == NSOrderedSame) {
            return @YES;
        }
        else {
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
            return [numberFormatter numberFromString:data];
        }
        
    }
    if ([data isKindOfClass:[NSNumber class]]) {
        return data;
    }
    
    return nil;
}

+ (BOOL)safeBool:(id)data {
    
    NSNumber *num = [AXDataCheck safeNumber:data];
    
    if (num) {
        return [num boolValue];
    }
    
    return NO;
}

+ (int)safeInt:(id)data {
    
    NSNumber *num = [AXDataCheck safeNumber:data];
    
    if (num) {
        return [num intValue];
    }
    
    return 0;
}

+ (float)safeFloat:(id)data {
    
    NSNumber *num = [AXDataCheck safeNumber:data];
    
    if (num) {
        return [num floatValue];
    }
    
    return 0.f;
}



+ (NSString *)safeString:(id)data {
    
    if (![AXDataCheck checkObject:data]) {
        return nil;
    }
    
    if ([data isKindOfClass:[NSString class]] && [data length] > 0) {
        return data;
    }
    
    return nil;
}

+ (NSString *)safeEmptyStringIfNil:(id)data {

    /*
     static inline NSString* emptyStringIfNil(NSString *s) {
     return s ? s : @"";
     }
    */
    
    NSString *string = [AXDataCheck safeString:data];
    return string ? : @"";
    
}


@end
