//
//  UIImage+fixOrientation.h
//  pd2
//
//  Created by i11 on 19/11/15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;
- (UIImage *)fixOrientationWith:(UIImageOrientation)orientation;

@end
