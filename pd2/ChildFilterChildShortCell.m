//
//  ChildFilterChildShortCell.m
//  pd2
//
//  Created by Andrey on 7/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ChildFilterChildShortCell.h"
#import "ChildFilterModel.h"

@implementation ChildFilterChildShortCell


- (void)updateWithModel:(ChildFilterModel *)model {
    
    
    self.firstAndLastNameLabel.text = [self fullNameFromFirstName:[model.firstName capitalizedString] andLastName:[model.lastName capitalizedString]];
    
    
    if (model.dateOfBirth) {
        self.dobAndAgeLabel.attributedText = [self dobAndAgeStringForDate:model.dateOfBirth];
    }
    else {
        self.dobAndAgeLabel.text = @"N/A";
    }
    
    [self check:model.isChecked];
    
}


- (void)updateImageWithModel:(ChildFilterModel *)model {
    //Onscreen
    [self updateProfileImageView:self.picImageView withImagePath:model.photoPath andDefaultImagePath:nil downloadImageCompletition:nil];
    
}


- (void)check:(BOOL)check {
    
    //Для некоторых ячеек чекбокса нет
    if (self.checkboxImageView) {
        self.checkboxImageView.image = check ? [UIImage imageNamed:@"checked"] : [UIImage imageNamed:@"unchecked"];
    }
    
}


- (NSAttributedString *)dobAndAgeStringForDate:(NSDate *)date {
    
    
    UIColor *color = [[MyColorTheme sharedInstance]spoilerTextColor]; 
    
    
    UIFont *lightFont = [UIFont systemFontOfSize:11.f];
    UIFont *mediumFont = [UIFont boldSystemFontOfSize:11.f];
    
    NSDictionary *lightDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     lightFont, NSFontAttributeName,
                                     color, NSForegroundColorAttributeName, nil];
    NSDictionary *mediumDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     mediumFont, NSFontAttributeName,
                                     color, NSForegroundColorAttributeName, nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@""];
    [string appendAttributedString:[[NSAttributedString alloc] initWithString:@"D.O.B: " attributes:lightDictionary]];
    [string appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [self stringFromDate:date withDateFormat:@"dd.MM.yyyy"]] attributes:mediumDictionary]];
    
    [string appendAttributedString:[[NSAttributedString alloc] initWithString:@", Age: " attributes:lightDictionary]];
    [string appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [self monthsFromDate:date]] attributes:mediumDictionary]];
    
    return string;
    
}


@end
