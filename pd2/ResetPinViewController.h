//
//  ResetPinViewController.h
//  pd2
//
//  Created by i11 on 23/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Pd2ViewController.h"

@class User;

@interface ResetPinViewController : Pd2ViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIView *passwordView;

@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UIButton *forgot;
@property (weak, nonatomic) IBOutlet UIButton *sign;


@property (nonatomic, weak) IBOutlet UIImageView *logoImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTop;


@property (strong, nonatomic) User *user;

- (IBAction)submitButton:(UIButton *)sender;
- (IBAction)backAction:(UIButton *)sender;

- (IBAction)forgotPasswordAction:(id)sender;
- (IBAction)signUpAction:(id)sender;

@end
