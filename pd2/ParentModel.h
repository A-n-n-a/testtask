//
//  ParentModel.h
//  pd2
//
//  Created by Andrey on 05/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXBaseModel.h"

@interface ParentModel : AXBaseModel

/*
{
    "first_name": "sdf",
    "last_name": "sdf",
    "image": "images/sted/parents/default.png",
    "ec_first_name": "",
    "ec_last_name": "",
    "ec_house": "",
    "ec_street_1": "",
    "ec_street_2": "",
    "ec_area": "",
    "ec_town": "",
    "ec_county": "",
    "ec_postcode": "",
    "ec_telephone": "",
    "ec_mobile": "",
    "ec_workplace": "",
    "ec_work_telephone": "",
    "ec_doctor_name": "",
    "ec_doctor_tel": "",
    "dip": "",
    "hib": "",
    "mea": "",
    "mum": "",
    "pol": "",
    "rub": "",
    "tet": "",
    "who": "",
    "ch_street_1": "",
    "ch_area": "",
    "ch_town": "",
    "ch_postcode": "",
    "ch_county": "",
    "parent_show_alert": true,
    "house": "",
    "street_1": "",
    "street_2": "",
    "area": "",
    "town": "",
    "county": "",
    "postcode": "",
    "phone": "",
    "mobile": "",
    "workplace": "",
    "work_telephone": "",
    "status": "on",
    "diaries": "on",
    "progress": "on",
    "gallery": "on",
    "policies": "on",
    "permissions": "on",
    "register": "on",
    "bookkeeping": "on",
    "communicate": "on",
    "meal_planning": "on",
    "medical": "on",
    "questionnaires": "on",
    "nextsteps": "on",
    "observations": "on",
    "contracts": "on",
    "risk_assessments": "on",
    "short_term_planning": "",
    "nappies": "on",
    "register_force": "on",
    "email": "ABC_ABC_mum@mybabysdays.com"
}
*/



@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;

@property (nonatomic, strong) NSString *photoPath;

@property (nonatomic, strong) NSString *street1;
@property (nonatomic, strong) NSString *street2;
@property (nonatomic, strong) NSString *area;
@property (nonatomic, strong) NSString *town;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *postCode;
@property (nonatomic, strong) NSString *tel;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *workPlace;
@property (nonatomic, strong) NSString *workTel;


- (instancetype)initWithDict:(NSDictionary *)dict;
- (NSString *)addressParent;
@end
