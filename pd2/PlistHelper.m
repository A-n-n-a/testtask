#import "PlistHelper.h"


@implementation PlistHelper
@synthesize dict_content = _dict_content;

- (void) loadDict
{
    NSString *path = [[NSBundle mainBundle] pathForResource:self.fileName ofType:@"plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]) {
        self.dict_content =[[NSDictionary alloc] initWithContentsOfFile:path];
    } else {
        NSLog(@"The file does not exist");
    }
}

- (NSDictionary *)dict_content
{
    if (_dict_content == nil) {
        [self loadDict];
    }
    return _dict_content;
}

@end
