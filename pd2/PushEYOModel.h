//
//  PushEYOModel.h
//  pd2
//
//  Created by Andrey on 03/11/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXBaseModel.h"
#import "PushNotificationModel.h"


typedef NS_ENUM(NSUInteger, PushEYORecordType) {
    PushEYORecordTypeObs,
    PushEYORecordTypeNote,
    PushEYORecordTypeUnknown
};

@interface PushEYOModel : AXBaseModel



@property (nonatomic, strong) NSNumber *identifier;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *tab;
@property (nonatomic, strong) NSString *rowId;
@property (nonatomic, strong) NSString *date;

@property (nonatomic, assign) PushNotificationAction pushAction;
@property (nonatomic, assign) PushEYORecordType recordType;


- (instancetype)initWithDict:(NSDictionary *)dict andPushTypeAction:(PushNotificationAction)actionType;
- (BOOL)isExist;



@end
