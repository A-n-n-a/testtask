#import "PlistHelper.h"

@interface AOLPlist : PlistHelper
+ (id) sharedPlist;
+ (NSString *) fullAgeFromInfo:(NSDictionary *)info;

/**
 * @return NSArray of NSString's
 */
- (NSArray *) sortedAreasOfLearningsKeys;
- (NSArray *) areasOfLearnings;
- (NSDictionary *) areasOfLearningsWithColors;
- (NSArray *) aspectsForArea:(NSString *)area;
- (NSArray *) eyosForArea:(NSString *)area aspect:(NSString *)aspect;
- (NSInteger) tabNumberForArea:(NSString *)area aspect:(NSString *)aspect;

- (NSDictionary *) aspectForAOL:(NSString *)aol eyo:(NSString *)eyo;
- (NSDictionary *) eyoInfoForAOL:(NSString *)aol eyoKey:(NSString *)eyoKey;
- (NSDictionary *) earlyOutcomesInfoForKey:(NSString *)key;

- (NSDictionary *) aolsTitles;
- (NSDictionary *) aolsIcons;
- (NSString *) generalColor;
- (NSString *) mainColorForType:(NSString *)type;
- (NSString *) highlightedColorForType:(NSString *)type;
- (NSDictionary *) aolsButtonsColors;

- (NSDictionary *) tabsIcons;
- (NSDictionary *) tabsIndexes;

@end
