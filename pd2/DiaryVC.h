//
//  DiaryVC.h
//  pd2
//
//  Created by Andrey on 08.03.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

#import "Pd2MenuViewController.h"

@interface DiaryVC : Pd2MenuViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@end
