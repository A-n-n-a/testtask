//
//  DailyNotesChildrenNoteCommentsViewController.m
//  pd2
//
//  Created by Andrey on 09/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesChildrenNoteCommentsViewController.h"
#import "DailyNotesChildrenNoteCommentCell.h"
#import "DailyNotesChildrenNoteCommentsNoteCell.h"

#import "DailyNotesChildrenModel.h"
#import "DailyNotesChildrenNoteModel.h"
#import "DailyNotesCommentModel.h"

#import "NSString+RemoveEmoji.h"

#import <CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h>


#define TABLE_VIEW_TAG 5550
#define COMMENT_TAG @"commentTag"




@interface DailyNotesChildrenNoteCommentsViewController () <UITextViewDelegate>

{
    
    NSString *PDNoteLabel;
    
    NSString *PDNoteCell;
    
    NSString *PDCommentsSection;
    NSString *PDCommentsHeader;
    NSString *PDCommentCell;
    NSString *PDNoCommentsCell;
    
    NSString *PDSpacer40Cell;

    
}

@property (nonatomic, strong) DailyNotesChildrenNoteModel *noteModel;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeight;

@property (assign, nonatomic) CGRect previousRect;
@property (assign, nonatomic) BOOL isNewLineLast;


@property (assign, nonatomic) BOOL isKeyboardVisible;



@property (nonatomic, strong) UIRefreshControl *bottomRefreshControl;


@end



@implementation DailyNotesChildrenNoteCommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initRows];
    [self initLabels];
    
    
    if (self.isPush) {
        [self pushLoadPrepare];
    }
    else {
        [self baseLoad];
    }
    

    [self.freeTapGestureRecognizer setEnabled:NO];
    
    self.tableView = [self.view viewWithTag:TABLE_VIEW_TAG];

    
    //UIRefreshControl *refreshControl = [UIRefreshControl new];
    
    //NSDictionary *attributedDict = [NSDictionary dictionaryWithObjectsAndKeys:[[MyColorTheme sharedInstance] spoilerBackGroundColor], NSForegroundColorAttributeName, nil];
    //refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull up to reload!" attributes:attributedDict];
    
    //refreshControl.tintColor = [[MyColorTheme sharedInstance] spoilerBackGroundColor];
    //refreshControl.triggerVerticalOffset = 60.f;
    //[refreshControl addTarget:self action:@selector(refreshBottom) forControlEvents:UIControlEventValueChanged];
    //self.tableView.bottomRefreshControl = refreshControl;
    //self.bottomRefreshControl = refreshControl;
    
    self.topTitle = @"View Daily Note";
    self.view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    self.bottomContainerView.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];

    
    
    self.messageTextField.delegate = self;
    self.previousRect = CGRectZero;
    
    self.messageTextField.placeholderFont = [UIFont systemFontOfSize:14.f];
    self.messageTextField.placeholder = @"Add a comment...";
    
    /*
    [self.leftButton removeTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
    [self.leftButton addTarget:self action:@selector(popViewControllerAction:) forControlEvents:UIControlEventTouchUpInside];
    */
}


- (void)baseLoad {
    
    AXSection *topSection = [[AXSection alloc] initWithType:PDTopSection];
    [topSection addRow:[self.sections genRowWithType:PDInfoPicCell]];
    [topSection addRow:[self.sections genRowWithType:PDSpoilerCell]];
    //[topSection addRow:[self.sections genRowWithType:PDSpacerCell]];
    
    [self.sections addSection:topSection];
    
    [self refreshData];
    
    AXSection *bottomSection = [[AXSection alloc] initWithType:PDBottomSection];
    [bottomSection addRow:[self.sections genRowWithType:PDSpacerCell]];
    [self.sections addSection:bottomSection];
    
}




- (void)scrollToBottom
{
    
    CGFloat yOffset = 0;
    
    if (self.tableView.contentSize.height > self.tableView.bounds.size.height) {
        yOffset = self.tableView.contentSize.height - self.tableView.bounds.size.height;
    }
    
    [self.tableView setContentOffset:CGPointMake(0, yOffset) animated:NO];
    
}


- (void)setMaskTo:(id)sender byRoundingCorners:(UIRectCorner)corners withCornerRadii:(CGSize)radii
{
    // UIButton requires this
    [sender layer].cornerRadius = 0.0;
    
    UIBezierPath *shapePath = [UIBezierPath bezierPathWithRoundedRect:[sender bounds]
                                                    byRoundingCorners:corners
                                                          cornerRadii:radii];
    
    CAShapeLayer *newCornerLayer = [CAShapeLayer layer];
    newCornerLayer.frame = [sender bounds];
    newCornerLayer.path = shapePath.CGPath;
    [sender layer].mask = newCornerLayer;
}


- (void)keyboardWillShow:(NSNotification *)notification
{
    
    NSDictionary* userInfo = [notification userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    CGSize keyboardSizeNew = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.bottomHeight.constant = keyboardSizeNew.height;
                         [self.view layoutIfNeeded]; // Called on parent view
                     }];
    
    [self scrollToBottom];
    
    self.isKeyboardVisible = YES;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.bottomHeight.constant = 0;
                         [self.view layoutIfNeeded]; 
                     }];
}


- (void)keyboardDidHide:(NSNotification *)notification {

    self.isKeyboardVisible = NO;

}


- (void)updateActionButton {
    
    
    [self.actionButton setTitleForAllStates:@"Save"];
    
    
}


- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}


- (void)initLabels {
    
    [super initLabels];
    
    PDNoteLabel = @"PDNoteLabel";

    
}


- (void)initRows {
    [super initRows];

    PDNoteCell = @"PDNoteCell";
    
    PDCommentsSection = @"PDCommentsSection";
    PDCommentsHeader = @"PDCommentsHeader";
    PDCommentCell = @"PDCommentCell";
    PDNoCommentsCell = @"PDNoCommentsCell";
    PDSpacer40Cell = @"PDSpacer40Cell";
    
    
    [self.sections rememberType:PDInfoPicCell withEstimatedHeight:180.f identifier:@"infoPicCell"];
    
    [self.sections rememberType:PDNoteCell withEstimatedHeight:97.f identifier:@"noteCell"];
    
    [self.sections rememberType:PDCommentCell withEstimatedHeight:120.f identifier:@"commentCell"];
    [self.sections rememberType:PDNoCommentsCell withHeight:76.f identifier:@"noCommentsCell"];

    
    [self.sections rememberType:PDCommentsHeader withHeight:44.f identifier:@"commentsHeaderCell"];
    [self.sections rememberType:PDSpacer40Cell withHeight:40.f identifier:@"spacerCell"];

    
    //[self.sections rememberType:PDSpacer16Cell withEstimatedHeight:16.f identifier:@"spacerCell"];
    
    [self.infoCells addObject:PDInfoPicCell];
    //[self.infoCells addObject:PDNoteCell];
    [self.contentFitCells addObject:PDNoteCell];
    [self.contentFitCells addObject:PDCommentCell];

}


#pragma mark - Configure cells


- (UIView *)viewForPDCommentsHeader:(AXHeader *)header inSection:(AXSection *)section {
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:header.identifier];
    UIView *view = cell.contentView;
    
    UIImageView *arrowImageView = (UIImageView *)[view viewWithTag:HEADER_ARROW_IMAGE_VIEW_TAG];
    [self updateHeaderArrowImage:arrowImageView withState:section.isHide];
    
    
    UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
    
    
    [self addBorder:PDBorderTopAndBottom toView:containerView forToolbar:NO];
    
    
    return view;
    
}


- (InfoPicCell *)configurePDInfoPicCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    InfoPicCell *cell = (InfoPicCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        cell = [self offscreenRenderingCell:cell withRow:row];
        [cell updateImageWithImagePath:self.childrenModel.photoPath];
        
        [self addBorder:PDBorderTopAndBottomDynamic toView:cell.containerView forToolbar:NO];
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}


- (UITableViewCell *)configurePDNoteCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    DailyNotesChildrenNoteCommentsNoteCell *cell = (DailyNotesChildrenNoteCommentsNoteCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        cell = [self offscreenRenderingCell:cell withRow:row];
        
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];

        [self addBorder:PDBorderTopAndBottomDynamic toView:containerView forToolbar:NO];
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}


- (UITableViewCell *)configurePDCommentCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    DailyNotesChildrenNoteCommentCell *cell = (DailyNotesChildrenNoteCommentCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        cell = [self offscreenRenderingCell:cell withRow:row];
        
        [cell updateImageWithModel:row.model];
        
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        /*
        BOOL isTop = [row.userInfo objectForKey:IS_TOP_COMMENT] ? [[row.userInfo objectForKey:IS_TOP_COMMENT] boolValue] : NO;
        if (isTop) {
            [self addBorder:PDBorderTopAndBottomDynamic toView:containerView forToolbar:NO];
        }
        else {
            [self addBorder:PDBorderBottomDynamic toView:containerView forToolbar:NO];
        }
        */
        
        [self addBorder:PDBorderBottomDynamic toView:containerView forToolbar:NO];
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}



#pragma mark - OffScreen rendering

- (id)offscreenRenderingPDInfoPicCell:(InfoPicCell *)cell withRow:(AXRow *)row {
    
    DailyNotesChildrenModel *model = self.childrenModel;
    
    //NSString *firstAndLastName = @"N/A";
    
    /*
     if (model.firstName && model.lastName) {
     firstAndLastName = [NSString stringWithFormat:@"%@ %@", [model.firstName capitalizedString], [model.lastName capitalizedString]];
     }
     */
    
    
    NSMutableAttributedString *attributedString = [self attributedStringWithKey:PDInfoLabel forObject:cell.infoTextView];
    /*
     [self replace:@"%firstAndLastName%"
     withString:firstAndLastName
     inAttributedString:&attributedString];
     */
    
    [self replace:@"%title%"
       withString:notAvailStringIfNil(self.noteModel.noteTitle)
inAttributedString:&attributedString];
    
    [self replace:@"%age%"
       withString:[AXTableViewCell monthsFromDate:model.dateOfBirth]
inAttributedString:&attributedString];
    
    [self replace:@"%dateOfBirth%"
       withString:notAvailStringIfNil([model.dateOfBirth printableDefault])
inAttributedString:&attributedString];
    
    [self replace:@"%roomName%"
       withString:notAvailStringIfNil(model.roomName)
inAttributedString:&attributedString];
    
    cell.infoTextView.textContainer.exclusionPaths = @[[cell picBezierPath]];
    
    
    if  (![cell.infoTextView.attributedText isEqualToAttributedString:attributedString]) {
        cell.infoTextView.attributedText = attributedString;
        [row needUpdateHeight];
    }
    
    return cell;
}


- (id)offscreenRenderingPDNoteCell:(DailyNotesChildrenNoteCommentsNoteCell *)cell withRow:(AXRow *)row {
    
    [cell updateWithModel:self.noteModel];
    
    return cell;
}

/*
- (id)offscreenRenderingPDNoteCell:(InfoCell *)cell withRow:(AXRow *)row {
    
    DailyNotesChildrenNoteModel *model = self.noteModel;
    
    NSString *dateString = @"N/A";
    if (model.date) {
        dateString = [model.date printableDefault];
    }
    
    
    NSMutableAttributedString *attributedString = [self attributedStringWithKey:PDNoteLabel forObject:cell.infoLabel];
    
    [self replace:@"%Title%"
       withString:notAvailStringIfNil(self.noteModel.noteTitle)
inAttributedString:&attributedString];
    
    [self replace:@"%firstAndLastName%"
       withString:[AXTableViewCell fullNameFromFirstName:model.adminFirstName andLastName:model.adminLastName]
inAttributedString:&attributedString];
    
    [self replace:@"%date%"
       withString:dateString
inAttributedString:&attributedString];

    [self replace:@"%note%"
       withString:notAvailStringIfNil(model.note)
inAttributedString:&attributedString];
    
    
    if  (![cell.infoLabel.attributedText isEqualToAttributedString:attributedString]) {
        cell.infoLabel.attributedText = attributedString;
        
        [row needUpdateHeight];
        
    }
    
    return cell;
}
*/



- (id)offscreenRenderingPDCommentCell:(DailyNotesChildrenNoteCommentCell *)cell withRow:(AXRow *)row {
    
    [cell updateWithModel:row.model];
    
    return cell;
}


#pragma mark - Cells Actions


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.view endEditing:YES];
    
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    DailyNotesCommentModel *model = row.model;

    NSString *type = row.type;
    
    //if ([type isEqualToString:PDCommentCell] && model.isRead == NO) {
    if ([type isEqualToString:PDCommentCell]) {
        
        if (self.isKeyboardVisible == YES) {
            return;
        }
        
        
        PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:nil message:nil preferredStyle:PSTAlertControllerStyleActionSheet];
        
        
        [alert addAction:[PSTAlertAction actionWithTitle:@"Delete" style:PSTAlertActionStyleDestructive handler:^(PSTAlertAction *action) {
            
            [self deleteCommentModel:model];
            
        }]];
        
        
        [alert addAction:[PSTAlertAction actionWithTitle:@"Dismiss" style:PSTAlertActionStyleCancel handler:^(PSTAlertAction *action) {
            
        }]];
        
        [alert showWithSender:self controller:self animated:YES
                   completion:nil];
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    DailyNotesCommentModel *model = row.model;
    
//    if ([row.type isEqualToString:PDOutgoingMessageCell] && model.isRead == NO) {

    if ([row.type isEqualToString:PDCommentCell]) {
        
        return YES;
    }
    
    return NO;
}


#pragma mark - Remote

- (void)refreshData {
    
    [self cleanForRefresh];
    
    __weak __typeof(self)weakSelf = self;
    
    [[NetworkManager sharedInstance] dailyNotesGetNoteWithId:self.noteId showProgressHUD:YES withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            if([[response valueForKeyPath:@"result.success"] intValue] == 1)
            {
                [[DataStateManager sharedManager]changed:DataStatePrivateMessages];
                
                NSDictionary *note = [response valueForKeyPath:@"data.note"];
                NSArray *comments = [response valueForKeyPath:@"data.comments"];
                [strongSelf updateTableWithNote:note comments:comments pull:NO];

            }
            else
            {
                
                //[self deleteLoadingSection];
                //[self updateTableWithRefreshMessage:[response valueForKeyPath:@"result.msg"]];
                Alert(@"Error",[response valueForKeyPath:@"msg"] );
                
            }
        }
        else if(error)
        {
            //[self deleteLoadingSection];
            //[self updateTableWithRefreshMessage:@"Error happened while operation"];
            Alert(@"Error", @"Error happened while operation");
        }
    }];
    
}


- (void)refreshPullData {
    
    //[self cleanForRefresh];
    [self.tableView.bottomRefreshControl endRefreshing];
    __weak __typeof(self)weakSelf = self;
    /*
    [[NetworkManager sharedInstance] privateMessagesViewDialogId:[self.dialogModel.recordId intValue] showProgressHUD:YES withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            if([[response valueForKeyPath:@"complete"] intValue] == 1)
            {
                [[DataStateManager sharedManager]changed:DataStatePrivateMessages];
                
                NSArray *data = [response valueForKeyPath:@"result.messages"];
                [strongSelf.tableView.bottomRefreshControl endRefreshing];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [strongSelf updateTableWithData:data pull:YES];
                });
                
                
                
            }
            else
            {
                
                //[self deleteLoadingSection];
                //[self updateTableWithRefreshMessage:[response valueForKeyPath:@"result.msg"]];
                [strongSelf.tableView.bottomRefreshControl endRefreshing];
                Alert(@"Error",[response valueForKeyPath:@"msg"] );
                
            }
        }
        else if(error)
        {
            //[self deleteLoadingSection];
            //[self updateTableWithRefreshMessage:@"Error happened while operation"];
            [self.tableView.bottomRefreshControl endRefreshing];
            //Alert(@"Error", @"Error happened while operation");
        }
    }];
    */
}


- (void)pushLoadPrepare {
    
    __weak __typeof(self)weakSelf = self;
    /*
    
    [[NetworkManager sharedInstance] privateMessagesGetListAndShowProgressHUD:YES withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            if([[response valueForKeyPath:@"complete"] intValue] == 1)
            {
                NSDictionary *data = [response valueForKeyPath:@"result"];
                [strongSelf pushFillWithData:data];
            }
            else
            {
                
                //[self deleteLoadingSection];
                //[self updateTableWithRefreshMessage:[response valueForKeyPath:@"result.msg"]];
                Alert(@"Error",[response valueForKeyPath:@"msg"] );
                
            }
        }
        else if(error)
        {
            //[self deleteLoadingSection];
            //[self updateTableWithRefreshMessage:@"Error happened while operation"];
            Alert(@"Error", @"Error happened while operation");
        }
    }];
    
    */
}


- (void)remoteAddCommentModel:(DailyNotesCommentModel *)model {
    
    __weak __typeof(self)weakSelf = self;
    
    [[NetworkManager sharedInstance] dailyNotesAddComment:model.text forChild:[self.childrenModel.chId intValue] note:self.noteId showProgressHUD:YES withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            if([[response valueForKeyPath:@"result.success"] intValue] == 1)
            {
                [[DataStateManager sharedManager]changed:DataStateDailyNotes];
                
                weakSelf.messageTextField.text = @"";
                [weakSelf textViewHeightUpdate];
                
                NSDictionary *data = [response valueForKeyPath:@"data"];
                
                [weakSelf updateWithSendComment:model andData:data];
            }
            else
            {
                
                Alert(@"Error",[response valueForKeyPath:@"msg"] );
                
            }
        }
        else if(error)
        {
            Alert(@"Error", @"Error happened while operation");
        }
    }];
    
    
}



- (void)remoteDeleteCommentModel:(DailyNotesCommentModel *)model {
    
    __weak __typeof(self)weakSelf = self;
    
    [[NetworkManager sharedInstance] dailyNotesDeleteCommentWithId:[model.commentId intValue] showProgressHUD:YES withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            if([[response valueForKeyPath:@"result.success"] intValue] == 1)
            {
                [[DataStateManager sharedManager]changed:DataStateDailyNotes];
                [strongSelf deleteRowWithModel:model];
 
            }
            else
            {
                
                //[self deleteLoadingSection];
                //[self updateTableWithRefreshMessage:[response valueForKeyPath:@"result.msg"]];
                //[weakSelf.tableView setEditing:NO animated:YES];
            
                Alert(@"Error",[response valueForKeyPath:@"result.message"]);

            
                
            }
        }
        else if(error)
        {
            //[self deleteLoadingSection];
            //[self updateTableWithRefreshMessage:@"Error happened while operation"];
            //[weakSelf.tableView setEditing:NO animated:YES];
            Alert(@"Error", @"Error happened while operation");
        }
    }];
    
    
}



#pragma mark - Remote processing

- (void)updateTableWithNote:(NSDictionary *)note comments:(NSArray *)comments pull:(BOOL)isPull{
    
    //note
    DailyNotesChildrenNoteModel *noteModel;
    
    if ([note isKindOfClass:[NSDictionary class]]) {
        
        DailyNotesChildrenNoteModel *model = [[DailyNotesChildrenNoteModel alloc] initWithDict:note];
        if (model && [model isExist]) {
            noteModel = model;
            self.noteModel = model;
        }
        
    }
    
    
    //обрабатываем active
    NSMutableArray *commentsPrepared = [[NSMutableArray alloc] init];
    
    if ([comments isKindOfClass:[NSArray class]]) {
        
        for (NSDictionary *dict in comments) {
            DailyNotesCommentModel *model = [[DailyNotesCommentModel alloc] initWithDict:dict];
            if (model && [model isExist]) {
                [commentsPrepared addObject:model];
            }
        }
    }
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
    [commentsPrepared sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];

    
    /*
    if (![messagesPrepared count]) {
        [self cleanForRefreshWithAnimation];
        [self insertMessageCell]; //Empty default
        return;
    }
    */
    
    NSMutableArray *sections = [NSMutableArray array];
    
    AXSection *mainSection = [[AXSection alloc] initWithType:PDMainSection];
    AXRow *noteRow = [self.sections genRowWithType:PDNoteCell];
    noteRow.model = noteModel;
    [mainSection addRow:[self.sections genRowWithType:PDSpacerCell]];
    [mainSection addRow:noteRow];
    [mainSection addRow:[self.sections genRowWithType:PDSpacer40Cell]];
    [sections addObject:mainSection];
    
    AXSection *commentsSection = [[AXSection alloc] initWithType:PDCommentsSection];
    AXHeader *commentsHeader = [self.sections genHeaderWithType:PDCommentsHeader];
    commentsSection.header = commentsHeader;
    commentsSection.isHide = NO;
    if (![commentsPrepared count]) {
        AXRow *emptyRow = [self.sections genRowWithType:PDNoCommentsCell];
        [emptyRow.tags addObject:COMMENT_TAG];
        [commentsSection addRow:emptyRow];
    }
    else {
        [commentsSection addRow:[self.sections genRowWithType:PDSpacerCell]];

        for (DailyNotesCommentModel *model in commentsPrepared) {
            
            AXRow *commentRow = [self.sections genRowWithType:PDCommentCell];
            commentRow.model = model;
            [commentRow.tags addObject:COMMENT_TAG];
            [commentsSection addRow:commentRow];

        }
    }
    [sections addObject:commentsSection];
    
    
    [self cleanForRefresh];
    
    NSIndexSet *insertedSectionsIndexSet =  [self.sections insertSections:sections afterSectionType:PDTopSection];
    
    /*
     [self.tableView beginUpdates];
     [self.tableView insertSections:insertedSectionsIndexSet withRowAnimation:UITableViewRowAnimationNone];
     [self.tableView endUpdates];
     */
    [self.tableView reloadData];
    
    //[self scrollToBottom];
}

/*
- (void)pushFillWithData:(NSDictionary *)data {
    
    
    //обрабатываем active
    NSMutableArray *personsPrepared = [[NSMutableArray alloc] init];
    
    if ([data isKindOfClass:[NSDictionary class]]) {
        for (NSDictionary *personsDict in [data allValues]) {
            NSDictionary *personDict = [personsDict objectForKey:@"admin"];
            NSArray *dialogsArray = [personsDict objectForKey:@"rows"];
            if (personDict && [personDict isKindOfClass:[NSDictionary class]] && dialogsArray && [dialogsArray isKindOfClass:[NSArray class]]) {
                
                PrivateMessagesPersonModel *model = [[PrivateMessagesPersonModel alloc] initWithPersonDict:personDict dialogs:dialogsArray];
                
                if (model) {
                    [personsPrepared addObject:model];
                }
                
            }
            
        }
    }
    
    for (PrivateMessagesPersonModel *model in personsPrepared) {
        for (PrivateMessagesDialogModel *dialogModel in model.dialogsArray) {
            if ([dialogModel.recordId intValue] == self.pushDialogId) {
                self.dialogModel = dialogModel;
                continue;
            }
        }
    }
    
    if (self.dialogModel) {
        [self baseLoad];
    }
    else {
        [self showPushFailureAlert];
    }
    
}
*/



- (void)updateWithSendComment:(DailyNotesCommentModel *)model andData:(NSDictionary *)data {
    
    
    //Обрабатываем model
    [model updateWithDict:data];
    
    AXRow *commentRow = [self.sections rowWithRowType:PDCommentCell];
    if (!commentRow) {
        NSArray *indexPathsToDelete = [self.sections getIndexPathsForRowWithTag:COMMENT_TAG];
        [self.sections removeArrayRowsByIndexPaths:indexPathsToDelete];
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationRight];
        [self.tableView endUpdates];
        
        
        //Добавляем Commenn
        AXRow *spacerRow = [self.sections genRowWithType:PDSpacerCell];
        [spacerRow.tags addObject:COMMENT_TAG];
        
        AXRow *commentRow = [self.sections genRowWithType:PDCommentCell];
        [commentRow.tags addObject:COMMENT_TAG];
        commentRow.model = model;
        
        NSArray *insertedRowsIndexPaths = [self.sections insertRows:@[spacerRow, commentRow] fromIndex:0 inSection:[self.sections sectionWithSectionType:PDCommentsSection]];
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:insertedRowsIndexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
    }
    else {
        AXRow *commentRow = [self.sections genRowWithType:PDCommentCell];
        [commentRow.tags addObject:COMMENT_TAG];
        commentRow.model = model;
        
        NSArray *insertedRowsIndexPaths = [self.sections insertRows:@[commentRow] afterLastRowType:PDCommentCell];
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:insertedRowsIndexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
        
    }

    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self scrollToBottom];
        
        
    });
    
    

    
    
    
}




- (void)deleteRowWithModel:(DailyNotesCommentModel *)model {
    
    NSMutableArray *indexPaths = [[self.sections getIndexPathsForRowWithModel:model] mutableCopy];
    if (![indexPaths count]) {
        return;
    }
    
    
    /*
    if ([self isLastUnreadMessage]) {
        NSArray *unreadSpacerIndexPath = [self.sections getIndexPathsForRowType:PDUnreadMessagesCell];
        [indexPaths addObjectsFromArray:unreadSpacerIndexPath];
        
    }
    */
    
    [self.sections removeArrayRowsByIndexPaths:indexPaths];
    
    
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationRight];
    [self.tableView endUpdates];
    
    
    //Удаляем остальные
    AXRow *commentRow = [self.sections rowWithRowType:PDCommentCell];
    if (!commentRow) {
        NSArray *indexPathsToDelete = [self.sections getIndexPathsForRowWithTag:COMMENT_TAG];
        [self.sections removeArrayRowsByIndexPaths:indexPathsToDelete];
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationRight];
        [self.tableView endUpdates];
        
    
        //Добавляем empty
        AXRow *emptyRow = [self.sections genRowWithType:PDNoCommentsCell];
        [emptyRow.tags addObject:COMMENT_TAG];
        
        NSArray *insertedRowsIndexPaths = [self.sections insertRows:@[emptyRow] fromIndex:0 inSection:[self.sections sectionWithSectionType:PDCommentsSection]];
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:insertedRowsIndexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
    
    }
    
    
    
}



- (void)cleanForRefreshWithAnimation {
    
    //Очищаем mainSection
    NSIndexSet *indexSet = [self.sections getIndexSetForSectionsWithType:PDMainSection];
    [self.sections removeSectionsByIndexSet:indexSet];
    
    
    [self.tableView beginUpdates];
    
    if ([indexSet count] > 0) {
        [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.tableView endUpdates];
    
}



#pragma mark - Actions

- (IBAction)sendMessage:(id)sender {

    NSString *text = self.messageTextField.text;
    text = [text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    if (![[text stringByRemovingEmoji] length]) {
        Alert(@"Error", @"Please complete Message");
        self.messageTextField.text = @"";
        return;
    }
    
    DailyNotesCommentModel *model = [[DailyNotesCommentModel alloc] init];
    model.text = text;
    //model.isRead = NO;
    
    [self.view endEditing:YES];
    
    [self remoteAddCommentModel:model];

}


- (void)deleteCommentModel:(DailyNotesCommentModel *)model {
    
    NSString *message = [NSString stringWithFormat:@"Are you sure you want to delete this Comment from the system?\nDeleting this Comment can NOT be reversed.\nClick OK below to delete this Comment from the system or Cancel to return to the previous page."];
    
    PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning!" message:message preferredStyle:PSTAlertControllerStyleAlert];
    
    [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:nil]];
    
    [alert addAction:[PSTAlertAction actionWithTitle:@"Delete" style:PSTAlertActionStyleDestructive handler:^(PSTAlertAction *action) {
        
        [self remoteDeleteCommentModel:model];
    }]];
    
    [alert showWithSender:self controller:self animated:YES
               completion:nil];

    
    
    
}


- (void)refreshBottom {
    
    [self refreshPullData];
    
}

/*
- (void)popViewControllerAction:(UIButton *)sender {
    
    int prevPrev = (int)[self.navigationController.viewControllers count] - 2;
    if (prevPrev >= 0) {
        UIViewController *controller = [self.navigationController.viewControllers objectAtIndex:prevPrev];
        if ([controller isKindOfClass:[self class]]) {
            
            for (UIViewController *controller in self.navigationController.viewControllers) {
                
                if ([controller isKindOfClass:[PrivateMessagesListViewController class]] || [controller isKindOfClass:[PrivateMessagesListDefViewController class]]) {
                    
                    [self.navigationController popToViewController:controller
                                                          animated:YES];
                    return;
                }
                
            }
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
*/

#pragma mark - height

- (void)textViewHeightUpdate
{
    CGSize size;

    UITextView *textView = self.messageTextField;
    
    if (!textView.attributedText) {
        
        NSString *text = textView.text;
        if (self.isNewLineLast) {
            text = [text stringByAppendingString:@"t"];
        }
        
        textView = [[UITextView alloc] init];
        textView.attributedText = [[NSAttributedString alloc] initWithAttributedString:[[NSAttributedString alloc] initWithString:text]];
    }
    size = [textView sizeThatFits:CGSizeMake(193.0f, FLT_MAX)];
    NSLog(@"height = %f", size.height);
    
    //if (size.height != self.textViewHeight.constant) {
    
    if (size.height > 133) {
        self.textViewHeight.constant = 133.f;
    }
    else if (size.height > 33) {
        self.textViewHeight.constant = size.height;
    }
    else {
        self.textViewHeight.constant = 33.f;
    }
    
    double time = self.isNewLineLast ? 0.09f : 0.f;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(time * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self.bottomContainerView layoutIfNeeded]; // Called on parent view
        [self.view layoutIfNeeded]; // Called on parent view

        [self scrollToBottom];

        if (size.height <= 133) {
            CGFloat topCorrect = ([textView bounds].size.height - [textView contentSize].height)/2.0;
            topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
            textView.contentOffset = (CGPoint){ .x = 0, .y = -topCorrect };
        }

        
    });
    
    
   NSLog(@"time = %f", time);
    
    
    
    //}
    
    
    

    
    /*
    if (size.height + 60 < 146.0f) {
        return 146.f;
    }
    return size.height + 60;
    */
    
    
}

- (void)textViewDidChange:(UITextView *)textView {
    
    UITextPosition* pos = textView.endOfDocument;//explore others like beginningOfDocument if you want to customize the behaviour
    CGRect currentRect = [textView caretRectForPosition:pos];
    
    if (currentRect.origin.y > self.previousRect.origin.y){
        self.isNewLineLast = YES;
    }
    else {
        self.isNewLineLast = NO;
    }
    self.previousRect = currentRect;
    [self textViewHeightUpdate];
}



@end
