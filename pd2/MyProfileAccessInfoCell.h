//
//  MyProfileAccessInfoCell.h
//  pd2
//
//  Created by Eugene Fozekosh on 20.11.15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileAccessInfoCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *usernameTF;
@property (strong, nonatomic) IBOutlet UITextField *firstNameTF;
@property (strong, nonatomic) IBOutlet UITextField *lastNameTF;
@property (strong, nonatomic) IBOutlet UITextField *emailTF;
@property (strong, nonatomic) IBOutlet UITextField *passwordTF;
@property (strong, nonatomic) IBOutlet UITextField *verifyPasswordTF;
@property (strong, nonatomic) IBOutlet UITextField *nPasswordTF;

@end
