//
//  NSDateFormatter+Locale.h
//  pd2
//
//  Created by i11 on 03/04/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

//see: http://stackoverflow.com/questions/6613110/what-is-the-best-way-to-deal-with-the-nsdateformatter-locale-feature

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Locale)

- (id)initWithSafeLocale;

@end
