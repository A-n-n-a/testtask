//
//  ViewAdminsVC.m
//  pd2
//
//  Created by User on 17.07.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "ViewAdminsVC.h"
#import "AdminCell.h"
#import "NewAdminVC.h"

#import "UserManager.h"

@interface ViewAdminsVC ()

@property int indexToDelete;

@end

@implementation ViewAdminsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addHomeButton];

    // Do any additional setup after loading the view.
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 300)];
    titleLabel.text = @"Administrators";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont boldSystemFontOfSize:23];
    self.navigationItem.titleView=titleLabel;

}

-(void)viewWillAppear:(BOOL)animated
{
    [self reloadData];
}

-(void)reloadData
{
    [[NetworkManager sharedInstance]getAdminListAndShowProgressHUD:YES withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            NSArray *positions=[response valueForKeyPath:@"adminList"];
            self.adminsArray = [NSMutableArray arrayWithArray:positions];

            [self sortArrayByFirstName:self.adminsArray];
                        
            [self.tableView reloadData];
        }
        else if(error)
        {
            
        }
    }];
}


- (void)sortArrayByFirstName:(id)array
{
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"first_name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    if([array isKindOfClass:[NSMutableArray class]]){
        NSMutableArray *a = (NSMutableArray*)array;
        [a sortUsingDescriptors:@[sortDescriptor1]];
    }else if([array isKindOfClass:[NSArray class]]){
        NSArray *a = (NSArray*)array;
        a = [a sortedArrayUsingDescriptors:@[sortDescriptor1]];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.adminsArray.count+4;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        return 60;
    }else if(indexPath.row==1)
    {
        return 58;
    }

    else if(indexPath.row==2)
    {
        return 56;
    }
    else if(indexPath.row==self.adminsArray.count+3)
    {
        return 68;
    }
    else
    {
        return 117;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"infoCell"];
        if (cell)
        {
            NSLog(@"cell for row %ld found!",(long)indexPath.row);
            cell.contentView.layer.borderWidth=0.5;
            cell.contentView.layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];
            
            NoteView *nv=cell.contentView.subviews[0];
            nv.topView=true;
            //[nv makeFirstLineBold];
            
            return cell;
        }
        else
        {
            return [[UITableViewCell alloc]init];
        }
    }else if(indexPath.row==1)
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"addAdmin1Cell"];
        if (cell)
        {
            NSLog(@"cell for row %ld found!",(long)indexPath.row);
            return cell;
        }
        else
        {
            return [[UITableViewCell alloc]init];
        }
        
    }
    else if(indexPath.row==2)
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"existingAdminsCell"];
        if (cell)
        {
            NSLog(@"cell for row %ld found!",(long)indexPath.row);
            [cell.contentView viewWithTag:101].layer.borderWidth=0.5;
            [cell.contentView viewWithTag:101].layer.borderColor=[[[MyColorTheme sharedInstance]onePxBorderColor]CGColor];
            
            return cell;
        }
        else
        {
            return [[UITableViewCell alloc]init];
        }
        
    }
    else if(indexPath.row==self.adminsArray.count+3)
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"addAdminCell"];
        if (cell)
        {
            NSLog(@"cell for row %ld found!",(long)indexPath.row);
            return cell;
        }
        else
        {
            return [[UITableViewCell alloc]init];
        }
        
    }
    else
    {
        AdminCell *cell=[tableView dequeueReusableCellWithIdentifier:@"adminCell"];
        cell.renameButton.tag=cell.deleteButton.tag=indexPath.row-3;
        NSDictionary *dict = [self.adminsArray objectAtIndex: indexPath.row-3];
        cell.infoDict=dict;
        cell.renameButton.tag=indexPath.row-3;
        cell.deleteButton.tag=indexPath.row-3;
        [cell.contentView viewWithTag:102].layer.borderWidth=0.5;
        [cell.contentView viewWithTag:102].layer.borderColor=[[[MyColorTheme sharedInstance] onePxBorderColor]CGColor];
        
        return cell;
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NewAdminVC *newAdmin=[segue destinationViewController];
    if ([segue.identifier isEqualToString:@"editAdminSegue"])
    {
        newAdmin.editMode=true;
        newAdmin.infoDict=[self.adminsArray objectAtIndex:[sender tag]];
    }
}


-(IBAction)deleteAdmin:(id)sender
{
    self.indexToDelete=(int)[sender tag];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Are you sure you want to delete this Administrator, deleting an Administrator from the system can not be reversed, are you sure you want to proceed?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
    [alert show];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1)
    {
        [self deleteAdminAtIndex:self.indexToDelete];
    }
}

-(void)deleteAdminAtIndex:(int)index
{
    NSDictionary *dict=[self.adminsArray objectAtIndex:index];
    NSNumber *obj_id=dict[@"uid"];
    [[NetworkManager sharedInstance] deleteAdminWithId:obj_id.intValue completion:^(id response, NSError *error)
     {
         if (response)
         {
             [self.adminsArray removeObjectAtIndex:index];
             NSIndexPath *ip=[NSIndexPath indexPathForRow:index+2 inSection:0];
             [_tableView deleteRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationFade];
             //[self.tableView reloadData];
             
             //Удлаяем админа из pinLogin
             [[UserManager sharedManager] deleteUserWithId:obj_id.intValue andSubdomain:[NetworkManager sharedInstance].subdomain];
             
             [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(reloadData) userInfo:nil repeats:NO];

         }
         else if(error)
         {
         }
     }];
}


@end
