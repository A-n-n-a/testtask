//
//  RoomModel.m
//  pd2
//
//  Created by Andrey on 05/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "RoomModel.h"

@implementation RoomModel



/*
 {
 adminRoom = 0;
 capacity = 10;
 childRoom = 4;
 from = 12;
 id = 1;
 image = "images/sted/rooms/default.jpg";
 ordering = 0;
 published = 0;
 ratio = 1;
 title = Default;
 to = 36;
 }
*/

- (instancetype)initWithDict:(NSDictionary *)dict {
    
    self = [super initWithDict:dict];
    if (self) {
        
        
        self.roomName = [self safeStringAndDecodingHTML:dict[@"title"]];
        self.roomId = [self safeNumber:dict[@"id"]];
        
        self.capacity = [self safeInt:dict[@"capacity"]];
        self.childrensInRoomCount = [self safeInt:dict[@"numChildren"]];
        
        self.photoPath = [self safeString:dict[@"image"]];
        
    }
    return self;
    
}


/*
 {
 "id": "1",
 "title": "Default",
 "capacity": "10",
 "num_children": "14"
 }
 */

- (instancetype)initWithMovingDict:(NSDictionary *)dict {
    
    self = [super initWithDict:dict];
    if (self) {
        
        
        self.roomName = [self safeStringAndDecodingHTML:dict[@"title"]];
        self.roomId = [self safeNumber:dict[@"id"]];
        
        self.capacity = [self safeInt:dict[@"capacity"]];
        self.childrensInRoomCount = [self safeInt:dict[@"num_children"]];
                
    }
    return self;
    
}


- (BOOL)isExist {
    
    if (self.roomName && self.roomId) {
        return YES;
    }
    
    return NO;
}




@end
