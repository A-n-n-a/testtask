#import <Foundation/Foundation.h>

@interface NSString (Utils)
+ (NSString *) md5:(NSString *) input;
+ (NSString *)toBase64String:(NSString *)string;
+ (NSString *)fromBase64String:(NSString *)string;
- (NSString *) firstLetterUppercaseString;

@end
