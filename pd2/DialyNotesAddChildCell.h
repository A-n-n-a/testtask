//
//  DialyNotesAddChildCell.h
//  pd2
//
//  Created by Andrey on 11/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@class DailyNotesNoteModel;

@interface DialyNotesAddChildCell : AXTableViewCell


@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UILabel *childrenNamesLabel;
@property (weak, nonatomic) IBOutlet UIImageView *childrenImageView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelSpacerConstraint;


- (void)updateWithModel:(DailyNotesNoteModel *)model;
//- (void)updateImageWithModel:(DepartedChildrenModel *)model;

@end
