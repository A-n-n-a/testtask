//
//  TextField.m
//  pd2
//
//  Created by i11 on 24/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "TextField.h"

@interface TextField ()

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end


@implementation TextField


- (void)showActivityIndicator {
    
    /*
     if ([self.isActivityIndicatorActive boolValue]) {
     return;
     }
     */
    
    if (!self.activityIndicator) {
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.hidesWhenStopped = YES;
        
        [activityIndicator setCenter:CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds))];
        [self addSubview:activityIndicator];
        
        self.activityIndicator = activityIndicator;
    }
    
    [self.activityIndicator startAnimating];
    
    //[self hideText];
    
    [self setUserInteractionEnabled:NO];
    
    //self.isActivityIndicatorActive = @YES;
    
}

- (void)hideActivityIndicator {
    
    //[self showText];
    [self.activityIndicator stopAnimating];
    //[self.activityIndicator removeFromSuperview];
    [self setUserInteractionEnabled:YES];
    
    //self.isActivityIndicatorActive = @NO;
    
}

- (void)setActivityIndicatorStatus:(BOOL)status {
    if (status) {
        [self showActivityIndicator];
    } else {
        [self hideActivityIndicator];
    }
}


@end
