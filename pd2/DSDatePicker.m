#import "DSDatePicker.h"
#import "NSDate+Utilities.h"
#import "UIView+Frame.h"

@implementation DSDatePicker

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.en_USLocale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)];

    self.view.frame=[UIScreen mainScreen].bounds;
    self.button_done.action=@selector(close:);
    self.view.backgroundColor = [UIColor clearColor];
    
    [self.view sendSubviewToBack:self.button_close];
    
    [self baseInit];
}

- (void) baseInit
{
    self.en_USLocale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"en_US",nil)];
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    calendar.locale = self.en_USLocale;
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSDate *currentDate     = [NSDate date];
    
    if (self.maxDate == nil) {
        [comps setYear:30];
        self.maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    }
    if (self.minDate == nil) {
        [comps setYear:-30];
        self.minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];

    }
    
    if (self.initialDate == nil) {
        self.initialDate = currentDate;
    }
    
    switch (self.dateType) {
        case kDatePickerTypeAll:
            [self initForAll];
            break;
        case kDatePickerTypeAllDate:
            [self initForAll];
            break;
        case kDatePickerTypeYearMonth:
            [self initForYearMonth];
            break;
        case kDatePickerTypeYear:
            [self initForYear];
            break;
    }
    
    [self datePickerValueChanged];
}

- (void) initForAll
{
    self.datePicker_full.hidden      = NO;
    self.datepicker_yearMonth.hidden = YES;
    self.datepicker_year.hidden      = YES;
    
    self.datePicker_full.backgroundColor = [UIColor whiteColor];
    self.datePicker_full.maximumDate     = self.maxDate;
    
    if(self.initialDate) {
        self.datePicker_full.date = self.initialDate;
    } else {
        self.datePicker_full.date = [NSDate dateYesterday];
    }
}

- (void) initForYearMonth
{
    self.datePicker_full.hidden      = YES;
    self.datepicker_yearMonth.hidden = NO;
    self.datepicker_year.hidden      = YES;
    
    self.datepicker_yearMonth.maximumDate = self.maxDate;
    self.datepicker_yearMonth.minimumDate = self.minDate;
    
    if(self.initialDate) {
        self.datepicker_yearMonth.date = self.initialDate;
    }
}

- (void) initForYear
{
    self.datePicker_full.hidden      = YES;
    self.datepicker_yearMonth.hidden = YES;
    self.datepicker_year.hidden      = NO;
    
    self.datepicker_year.maximumDate = self.maxDate;
    self.datepicker_year.minimumDate = self.minDate;
    self.datepicker_year.dateFormatTemplate = @"yyyy";
    
    if(self.initialDate) {
       self.datepicker_year.date = self.initialDate;
    }
}


- (void) setDateType:(kDatePickerType)dateType
{
    _dateType = dateType;
    [self baseInit];
}


#pragma mark -
#pragma mark - Actions

-(IBAction)close:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        [self.view increaseYPos: self.view.frame.size.height];
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        
        if (self.dateHandler) {
            switch (self.dateType) {
                case kDatePickerTypeAll:
                    self.dateHandler(self.datePicker_full.date);
                    break;
                case kDatePickerTypeAllDate:
                    self.dateHandler(self.datePicker_full.date);
                    break;
                case kDatePickerTypeYearMonth:
                    self.dateHandler(self.datepicker_yearMonth.date);
                    break;
                case kDatePickerTypeYear:
                    self.dateHandler(self.datepicker_year.date);
                    break;
            }
            
        }
        
        if (self.finishHandler)  {
            self.finishHandler();
        }
        self.dateHandler   = nil;
        self.finishHandler = nil;
    }];
}

-(IBAction)hide
{
    NSLog(@"datepicker.hide");
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view increaseYPos: self.view.frame.size.height];
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
        if (self.finishHandler) {
            self.finishHandler();
        }
        self.dateHandler   = nil;
        self.finishHandler =nil;
    }];
}

-(void)show
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    CGRect frame = CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height);
    CGRect startRect = frame;
    startRect.origin.y += self.view.frame.size.height;
    
    self.view.frame = startRect;
    
    AppDelegate *delegate = [AppDelegate delegate];
    [delegate.window addSubview:self.view];
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = frame;
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    });
}

-(IBAction)datePickerValueChanged
{
    
    return;
}

@end
