//
//  NSString+VersionNumbers.h
//  pd2
//
//  Created by i11 on 21/11/15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (VersionNumbers)

- (NSString *)shortenedVersionNumberString;

@end
