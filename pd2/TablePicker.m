//
//  TablePicker.m
//  pd2
//
//  Created by User on 03.11.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "TablePicker.h"

@interface TablePicker ()

@end

@implementation TablePicker

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.frame=[UIScreen mainScreen].bounds;
    self.doneButton.action=@selector(close:);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self hide];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[[UITableViewCell alloc]init];
    [cell.textLabel setText:[_dataArray objectAtIndex:indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.tapHandler)
    {
        self.tapHandler([_dataArray objectAtIndex:indexPath.row],(int)indexPath.row );
    }
}

-(IBAction)close:(id)sender
{
    NSLog(@"tablepicker.close");
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
    }];
    
}

-(void)hide
{
    NSLog(@"tablepicker.hide");
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
    }];
}


-(void)show
{
    //int height=[UIScreen mainScreen].bounds.size.height;
    CGRect frame = CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height);
    CGRect startRect = frame;
    startRect.origin.y += self.view.frame.size.height;
    
    self.view.frame = startRect;
    
    AppDelegate *delegate=[AppDelegate delegate];
    [delegate.window addSubview:self.view];
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    }];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
