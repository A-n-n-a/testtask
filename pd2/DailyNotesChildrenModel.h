//
//  DailyNotesChildrenModel.h
//  pd2
//
//  Created by Andrey on 07/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXBaseModel.h"

@interface DailyNotesChildrenModel : AXBaseModel


@property (nonatomic, strong) NSNumber *chId;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSDate *dateOfBirth;
@property (nonatomic, strong) NSString *photoPath;

@property (nonatomic, assign) int monthlyNotesTotal;
@property (nonatomic, assign) int monthlyNotes;

@property (nonatomic, assign) int commentsTotal;
@property (nonatomic, assign) int commentsNew;


@property (nonatomic, strong) NSString *roomName;
@property (nonatomic, strong) NSNumber *roomId;
@property (nonatomic, strong) NSString *roomImage;




- (instancetype)initWithChildDict:(NSDictionary *)childDict roomDict:(NSDictionary *)roomDict;

- (BOOL)nameContain:(NSString *)containString;



@end
