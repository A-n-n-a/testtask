//
//  DailyNotesAddNoteViewController.m
//  pd2
//
//  Created by Andrey on 05/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "DailyNotesAddNoteViewController.h"

#import "DailyNotesAddNoteCopyCell.h"
#import "DailyNotesAddNoteTitleCell.h"
#import "DialyNotesAddChildCell.h"
#import "DailyNotesAddNoteHelperCell.h"
#import "DailyNotesAddNotePendingCell.h"

#import "DailyNotesNoteModel.h"

//Helpers
#import "DailyNotesHelperModel.h"
#import "DailyNotesHelpersModel.h"

//Filter
#import "ChildFilterViewController.h"
#import "ChildFilterModel.h"

#import "DOBPicker.h"
#import "TablePicker.h"
#import "ListPicker.h"

//#define PARENT_NUM_KEY @"parentNumKey"


@interface DailyNotesAddNoteViewController () <UITextFieldDelegate, UITextViewDelegate, ChildFilterMultipleDelegate>

{

    NSString *PDNoteSection;
    
    NSString *PDAddInfoCell;
    NSString *PDEditInfoCell;
    
    
    
    NSString *PDNoteHeader;
    NSString *PDCopyCell;
    NSString *PDTitleCell;
    NSString *PDChildrenCell;
    NSString *PDHelperCell;
    NSString *PDPendingCell;
    NSString *PDAddNoteCell;
    NSString *PDDelAddNoteCell;
    
    NSString *PDSaveCell;

    NSString *PDSpacer8Cell;
    NSString *PDSpacer35Cell;

}

@property (nonatomic, strong) DailyNotesHelpersModel *helpersModel;

@property (nonatomic, strong) NSArray *notes;

//@property (nonatomic, strong) NSMutableArray *currentChildsIdArray;
@property (nonatomic, strong) DailyNotesNoteModel *selectedDailyNoteModel;


@property (nonatomic, strong) DOBPicker *dobPicker;
@property (nonatomic, strong) TablePicker *tablePicker;
@property (nonatomic, strong) ListPicker *listPicker;

@property (nonatomic, assign) int remoteOperationCounter;

@property (nonatomic, assign) int isNoteFirstResponder;

@end

@implementation DailyNotesAddNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    [self initRows];
    [self initLabels];
    AXSection *topSection = [[AXSection alloc] initWithType:PDTopSection];
    [topSection addRow:[self.sections genRowWithType:self.isEdit ? PDEditInfoCell : PDAddInfoCell]];
    [topSection addRow:[self.sections genRowWithType:PDSpacerCell]];
    [self.sections addSection:topSection];
    
    //[self refreshData];
    
    if (self.isEdit) {
        [self refreshDataForEditNote];
    }
    else {
        self.notes = [NSArray arrayWithObject:[[DailyNotesNoteModel alloc] init]];
        [self refreshDataForNewNote];
        
    }
    
    AXSection *bottomSection = [[AXSection alloc] initWithType:PDBottomSection];
    //[bottomSection addRow:[self.sections genRowWithType:PDAddNoteCell]];
    [bottomSection addRow:[self.sections genRowWithType:PDSaveCell]];
    [bottomSection addRow:[self.sections genRowWithType:PDSpacerCell]];
    [self.sections addSection:bottomSection];
    
    [self.freeTapGestureRecognizer setEnabled:NO];

    
    self.topTitle = self.isEdit ? @"Edit Daily Note" : @"Add Daily Note";
    self.view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;

    
}


- (void)viewWillAppear:(BOOL)animated {
    
    //"выключаем лишний scroll"
    //[super viewWillAppear:animated];
    
    
    
    //альтернативный метод скролла для длинных ячеек когда вызываем
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.dobPicker hide];
    [self.tablePicker hide];
    [self.listPicker hide];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWillShow:(NSNotification *)note {
    
    CGSize keyboardSize = [[[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    //UIView *firstResponder = [self firstRe]
    
    //float inset = self.isNoteFirstResponder ? 80.f : 20.f;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height) + 20.f, 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width) + 20.f, 0.0);
    }
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
}


- (void)keyboardWillHide:(NSNotification *)note {
    
    self.tableView.contentInset = UIEdgeInsetsZero;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    
}

- (void)initRows {
    [super initRows];
    
    
    PDNoteSection = @"PDNoteSection";
    PDNoteHeader = @"PDNoteHeader";
    
    
    PDAddInfoCell = @"PDAddInfoCell";
    PDEditInfoCell = @"PDEditInfoCell";
    
    PDCopyCell = @"PDCopyCell";
    PDTitleCell = @"PDTitleCell";
    PDChildrenCell = @"PDChildrenCell";
    PDHelperCell = @"PDHelperCell";
    PDPendingCell = @"PDPendingCell";
    PDAddNoteCell = @"PDAddNoteCell";
    PDDelAddNoteCell = @"PDDelAddNoteCell";
    
    
    PDSaveCell = @"PDSaveCell";
    
    PDSpacer8Cell = @"PDSpacer8Cell";
    PDSpacer35Cell = @"PDSpacer35Cell";

    
    
    
    [self.sections rememberType:PDAddInfoCell withEstimatedHeight:180.f identifier:@"infoAddCell"];
    [self.sections rememberType:PDEditInfoCell withEstimatedHeight:180.f identifier:@"infoEditCell"];
    
    [self.sections rememberType:PDNoteHeader withHeight:44.f identifier:@"headerCell"];
    [self.sections rememberType:PDCopyCell withHeight:75.f identifier:@"copyNoteCell"];
    [self.sections rememberType:PDTitleCell withHeight:166.f identifier:@"titleCell"];
    [self.sections rememberType:PDChildrenCell withHeight:44.f identifier:@"selectChildrenCell"];

    [self.sections rememberType:PDHelperCell withHeight:279.f identifier:@"helperCell"];
    [self.sections rememberType:PDPendingCell withHeight:44.f identifier:@"pendingCell"];
    [self.sections rememberType:PDAddNoteCell withHeight:56.f identifier:@"addNoteCell"];
    [self.sections rememberType:PDDelAddNoteCell withHeight:56.f identifier:@"delAddNoteCell"];

    [self.sections rememberType:PDSaveCell withHeight:56.f identifier:@"saveCell"];


    [self.sections rememberType:PDSpacer8Cell withHeight:8.f identifier:@"spacerCell"];
    [self.sections rememberType:PDSpacer35Cell withHeight:35.f identifier:@"spacerCell"];

    
    [self.infoCells addObject:PDAddInfoCell];
    [self.infoCells addObject:PDEditInfoCell];
    
}

- (void)initLabels {
    
    [super initLabels];
    
}

#pragma mark - Configure cells

- (UIView *)viewForPDNoteHeader:(AXHeader *)header inSection:(AXSection *)section {
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:header.identifier];
    UIView *view = cell.contentView;
    
    UIImageView *arrowImageView = (UIImageView *)[view viewWithTag:HEADER_ARROW_IMAGE_VIEW_TAG];
    [self updateHeaderArrowImage:arrowImageView withState:section.isHide];
    UILabel *label = [cell.contentView viewWithTag:HEADER_LABEL_TAG];
    
    DailyNotesNoteModel *model = header.model;
    
    
    label.text = [NSString stringWithFormat:@"Daily Note %lu", (unsigned long)[self noteIndexForNote:model]];

    
    UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
    
    
    [self addBorder:PDBorderTopAndBottom toView:containerView forToolbar:NO];
    
    
    return view;
    
}




- (UITableViewCell *)configurePDCopyCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    DailyNotesAddNoteCopyCell *cell = [tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        DailyNotesNoteModel *model = row.model;
        DailyNotesNoteModel *copyModel = model.copyingFromNote;
        
        [cell updateCopyIndex:[self noteIndexForNote:copyModel]];
        
        cell.noteCopyTextField.delegate = self;
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        //NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}


- (UITableViewCell *)configurePDTitleCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    DailyNotesAddNoteTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        DailyNotesNoteModel *model = row.model;
        
        [cell updateWithModel:model];
        
        cell.noteTitleTextField.delegate = self;
        cell.dateTextField.delegate = self;
        
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        //NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}


- (UITableViewCell *)configurePDChildrenCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    DialyNotesAddChildCell *cell = [tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        DailyNotesNoteModel *model = row.model;
        
        [cell updateWithModel:model];
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        //NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}


- (UITableViewCell *)configurePDHelperCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    DailyNotesAddNoteHelperCell *cell = [tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        DailyNotesNoteModel *model = row.model;
        
        [cell updateWithModel:model];
        
        cell.helperTextField.delegate = self;
        cell.noteTextView.delegate = self;
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        //NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}


- (UITableViewCell *)configurePDPendingCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    DailyNotesAddNotePendingCell *cell = [tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        DailyNotesNoteModel *model = row.model;
        [cell updateWithModel:model];
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        //NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}
//

#pragma mark - OffScreen rendering

#pragma mark - Select Cell

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.view endEditing:YES];
    [self.dobPicker hide];
    [self.tablePicker hide];
    [self.listPicker hide];
    
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    NSString *type = row.type;
    
    if ([type isEqualToString:PDChildrenCell]) {
        
        DailyNotesNoteModel *model = row.model;
        self.selectedDailyNoteModel = model;
        
        [self performSegueWithIdentifier:@"childFilter@Utilities_iPhone" sender:row];
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}


#pragma mark - Remote


- (void)refreshDataForNewNote {
    
    self.remoteOperationCounter = 1;

    [self cleanForRefresh];

    [self remoteLoadHelpers];

}

- (void)refreshDataForEditNote {
    
    self.remoteOperationCounter = 2;
    
    [self cleanForRefresh];
    
    [self remoteLoadNote];
    [self remoteLoadHelpers];
}



- (void)remoteLoadNote {
    
    __weak __typeof(self)weakSelf = self;
    
    [[NetworkManager sharedInstance] dailyNotesGetPendingNoteWithId:self.noteId showProgressHUD:YES withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            if([[response valueForKeyPath:@"result.success"] intValue] == 1)
            {
                NSArray *notes = [response valueForKeyPath:@"data"];
                strongSelf.remoteOperationCounter--;
                [strongSelf storeNotes:notes];
            }
            else
            {
                Alert(@"Error",[response valueForKeyPath:@"result.message"] );
                
            }
        }
        else if(error)
        {
            Alert(@"Error", @"Error happened while operation");
        }
    }];
    
   
}


- (void)remoteLoadHelpers {
    
    __weak __typeof(self)weakSelf = self;
    
    [[NetworkManager sharedInstance] dailyNotesGetHelpersAndShowProgressHUD:NO withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            if([[response valueForKeyPath:@"result.success"] intValue] == 1)
            

            {
                NSDictionary *data = [response valueForKeyPath:@"data"];
                strongSelf.remoteOperationCounter--;
                [strongSelf storeHelpers:data];
            }
            else
            {
                
                //[self deleteLoadingSection];
                //[self updateTableWithRefreshMessage:[response valueForKeyPath:@"result.msg"]];
                Alert(@"Error",[response valueForKeyPath:@"result.message"] );
                
            }
        }
        else if(error)
        {
            
            //[self deleteLoadingSection];
            //[self updateTableWithRefreshMessage:@"Error happened while operation"];
            Alert(@"Error", @"Error happened while operation");
        }
    }];
    
}





- (void)remoteSaveNotes:(NSArray *)notes {
    
    __weak __typeof(self)weakSelf = self;
    
    [[NetworkManager sharedInstance] dailyNotesSaveNotes:notes showProgressHUD:YES withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            if([[response valueForKeyPath:@"result.success"] intValue] == 1)
            {
                [[DataStateManager sharedManager]changed:DataStateDailyNotes];
                
                
                [AppDelegate showCompletedHUDWithCompletionBlock:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            else
            {
                
                //[self deleteLoadingSection];
                //[self updateTableWithRefreshMessage:[response valueForKeyPath:@"result.msg"]];
                Alert(@"Error",[response valueForKeyPath:@"result.message"] );
                
            }
        }
        else if(error)
        {
            
            //[self deleteLoadingSection];
            //[self updateTableWithRefreshMessage:@"Error happened while operation"];
            Alert(@"Error", @"Error happened while operation");
        }
    }];
    
}


#pragma mark - Remote processing


- (void)storeNotes:(NSArray *)array {
    
    NSMutableArray *preparedNotes = [NSMutableArray array];
    
    if (array && [array isKindOfClass:[NSArray class]]) {
        for (NSDictionary *dict in array) {
            DailyNotesNoteModel *model = [[DailyNotesNoteModel alloc] initWithDict:dict];
            if (model && [model isExist]) {
                [preparedNotes addObject:model];
            }
        }
    }
    
    self.notes = [NSArray arrayWithArray:preparedNotes];
    
    if (self.remoteOperationCounter == 0) {
        [self updateTable];
    }
    
}

- (void)storeHelpers:(NSDictionary *)dict {
    
    if (dict && [dict isKindOfClass:[NSDictionary class]]) {
        DailyNotesHelpersModel *model = [[DailyNotesHelpersModel alloc] initWithDict:dict];
        if (!model) {
            //            [self cleanForRefresh];
            //            [self insertMessageCell]; //Empty default
            //            return;
        }
        self.helpersModel = model;
    }
    
    if (self.remoteOperationCounter == 0) {
        [self updateTable];
    }
    
}


- (void)updateTable {
    
    if (self.remoteOperationCounter != 0) {
        return;
    }
    
    /*
    if (!self.model || !self.roomsModel) {
        [self cleanForRefresh];
        [self insertMessageCell]; //Empty default
        return;
    }
    */
    
    if (![self.notes count] || !self.helpersModel) {
        [self cleanForRefresh];
        [self insertMessageCell]; //Empty default
        return;
    }
    
    
    
    
    NSMutableArray *sections = [NSMutableArray array];
    
    BOOL isFirst = YES;
    for (DailyNotesNoteModel *model in self.notes) {
        [sections addObject:[self genSectionForNote:model isCopyAvailable:NO isFirstNote:isFirst]];
        isFirst = NO;
    }
    
    
    [self.sections insertSections:sections afterSectionType:PDTopSection];
    [self.tableView reloadData];
    
    
    
}


- (AXSection *)genSectionForNote:(DailyNotesNoteModel *)model isCopyAvailable:(BOOL)isCopyAvailable isFirstNote:(BOOL)isFirstNote{
    
    
    AXSection *section = [[AXSection alloc] initWithType:PDNoteSection];
    section.model = model;
    AXHeader *header = [self.sections genHeaderWithType:PDNoteHeader];
    header.model = model;
    section.header = header;
    
    if (isCopyAvailable) {
        AXRow *copyRow = [self.sections genRowWithType:PDCopyCell];
        copyRow.model = model;
        [section addRow:copyRow];
    }
    
    AXRow *titleRow = [self.sections genRowWithType:PDTitleCell];
    titleRow.model = model;
    [section addRow:titleRow];
    
    AXRow *childrenRow = [self.sections genRowWithType:PDChildrenCell];
    childrenRow.model = model;
    [section addRow:childrenRow];
    
    AXRow *helperRow = [self.sections genRowWithType:PDHelperCell];
    helperRow.model = model;
    [section addRow:helperRow];
    
    AXRow *pendingRow = [self.sections genRowWithType:PDPendingCell];
    pendingRow.model = model;
    [section addRow:pendingRow];
    
    AXRow *actionRow = [self.sections genRowWithType:isFirstNote ? PDAddNoteCell : PDDelAddNoteCell];
    actionRow.model = model;
    [section addRow:actionRow];

    AXRow *spacerRow = [self.sections genRowWithType:PDSpacerCell];
    spacerRow.model = model;
    [section addRow:spacerRow];

    
    return section;
    
    
}




- (void)cleanForRefreshWithAnimation {
    
    //Очищаем mainSection
    NSIndexSet *indexSet = [self.sections getIndexSetForSectionsWithType:PDMainSection];
    [self.sections removeSectionsByIndexSet:indexSet];
    
    
    [self.tableView beginUpdates];
    
    if ([indexSet count] > 0) {
        [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.tableView endUpdates];
    
    //[self.tableView reloadData];
}



#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
        self.isNoteFirstResponder = NO;
    
        NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
        AXRow *row = [self rowForView:textField];
        
        
        if ([row.type isEqualToString:PDTitleCell]) {
            
            DailyNotesNoteModel *model = row.model;
            
            DailyNotesAddNoteTitleCell *cell = (DailyNotesAddNoteTitleCell *)row.cell;
            
            if ([textField isEqual:cell.noteTitleTextField]) {
                model.title = text;
            }
        }
    
    
    return YES;

    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    self.isNoteFirstResponder = NO;
    
    [textField resignFirstResponder];
    return NO;
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    self.isNoteFirstResponder = NO;
    
    [self.dobPicker hide];
    [self.listPicker hide];
    [self.tablePicker hide];
    
    
    AXRow *row = [self rowForView:textField];
    
    
    if ([row.type isEqualToString:PDCopyCell]) {
        
        DailyNotesNoteModel *model = row.model;
        
        NSUInteger noteIndex = [self noteIndexForNote:model];
        if (noteIndex == NSNotFound || noteIndex == 1) {
            return NO;
        }
        
        NSMutableArray *array = [NSMutableArray array];
        
        NSArray *availableNotes = [self notesExcludeNote:model];
        [array addObject:@"Do Not Copy Notes"];
        for (DailyNotesNoteModel *copyModel in availableNotes) {
            NSUInteger noteIndex = [self noteIndexForNote:copyModel];
            [array addObject:[NSString stringWithFormat:@"Daily Note %lu", (unsigned long)noteIndex]];
        }
        
        self.listPicker = [[ListPicker alloc] init];
        self.listPicker.dataArray = array;
        
        
        __weak typeof(model)weakModel = model;
        __weak typeof(self)weakSelf = self;
        __strong typeof(availableNotes)strongAvailableNotes = availableNotes;

        
        [self.listPicker setHandler:^(NSString * value, int index) {
            
            if (index == 0) {
                weakModel.copyingFromNote = nil;
            }
            else {
                DailyNotesNoteModel *note = [strongAvailableNotes objectAtIndex:index-1];
                weakModel.copyingFromNote = note;
                weakModel.note = [note.note copy];
            }
           
            [weakSelf.tableView reloadData];
            
        }];
        
        [self.listPicker show];
        
        return NO;
        
        
    
    }
    else if ([row.type isEqualToString:PDTitleCell]) {
        
        DailyNotesNoteModel *model = row.model;
        
        DailyNotesAddNoteTitleCell *cell = (DailyNotesAddNoteTitleCell *)row.cell;

    
        if ([textField isEqual:cell.dateTextField]) {
            
            
            NSDate *date = model.date;
            
            [self.view endEditing:YES];
            
            self.dobPicker = [[DOBPicker alloc] init];
            [self.dobPicker setInitialDate:date ? : [NSDate date]];
            
            __weak typeof(textField) weakTextField = textField;
            __weak typeof(model)weakModel = model;
            [self.dobPicker setDateHandler:^(NSDate *date) {
                weakTextField.text = notAvailStringIfNil([date printableDefault]);
                weakModel.date = date;
            }];
            
            [self.dobPicker show];
            
            return NO;
            
        }

    }
    
    else if ([row.type isEqualToString:PDHelperCell]) {
        
        DailyNotesNoteModel *model = row.model;
        
        DailyNotesAddNoteHelperCell *cell = (DailyNotesAddNoteHelperCell *)row.cell;

        
        
        if ([textField isEqual:cell.helperTextField]) {
        
            
            [self.view endEditing:YES];
            [self.dobPicker hide];
            [self.tablePicker hide];
            [self.listPicker hide];
            
            NSArray *helpersCategoriesTitlesArray = self.helpersModel.helpersTitle;

            
            self.listPicker = [[ListPicker alloc] init];
            self.listPicker.dataArray = helpersCategoriesTitlesArray;
            self.listPicker.rightButtonCustomTitle = @"Select";
            self.listPicker.leftCustomTitle = @"Helpers:";
            
            __weak typeof(model)weakModel = model;
            __weak typeof(self)weakSelf = self;
            
            //level 1
            [self.listPicker setHandler:^(NSString * value, int index) {
                
                
                [weakSelf.dobPicker hide];
                [weakSelf.tablePicker hide];
                [weakSelf.listPicker hide];
                
                
                NSArray *helpersArray = [weakSelf.self.helpersModel.helpers objectAtIndex:index];
                
                NSMutableArray *helperTitles = [NSMutableArray array];
                for (DailyNotesHelperModel *model in helpersArray) {
                    [helperTitles addObject:model.title ? : @""];
                }
                
                
                weakSelf.listPicker = [[ListPicker alloc] init];
                weakSelf.listPicker.dataArray = helperTitles;
                weakSelf.listPicker.rightButtonCustomTitle = @"Select";
                weakSelf.listPicker.leftCustomTitle = @"Helpers:";
                
                __weak typeof(helpersArray)weakHelpers = helpersArray;
                
                //level 2
                [weakSelf.listPicker setHandler:^(NSString * value, int index) {
                    
                    DailyNotesHelperModel *helperModel = [weakHelpers objectAtIndex:index];
                    if (!helperModel) {
                        return;
                    }
                    
                    [weakSelf.dobPicker hide];
                    [weakSelf.tablePicker hide];
                    [weakSelf.listPicker hide];
                    
                    weakSelf.tablePicker = [[TablePicker alloc] init];
                    weakSelf.tablePicker.dataArray = helperModel.values;
                    
                    //level 3
                    [weakSelf.tablePicker setTapHandler:^(NSString *str, int ind) {
                        //[weakSelf.tablePicker hide];
                        
                        
                        if ([weakModel.note length]) {
                            weakModel.note = [NSString stringWithFormat:@"%@, %@", weakModel.note, str];
                        }
                        else {
                            weakModel.note = [NSString stringWithFormat:@"%@", str];
                        }
                        
                        [weakSelf updateNoteForModel:weakModel];
                        
                        //cell.descriptionTextView.text = [cell.descriptionTextView.text stringByAppendingString:[NSString stringWithFormat:@" %@",str]];
                        //[weakSelf textViewDidEndEditing:cell.descriptionTextView];
                        
                    }];
                    
                    [weakSelf.tablePicker show];
                    
                }];
                
                [weakSelf.listPicker show];
            
                
            }];
            
            [self.listPicker show];
            
            return NO;
        
        }
        
    }
    
    

    return YES;
    
}


- (void)updateNoteForModel:(DailyNotesNoteModel *)model {
    
    NSArray *indexPaths = [self.sections getIndexPathsForRowType:PDHelperCell atIndex:AXRowFirst withCondition:^BOOL(AXSection *section, AXRow *row) {
        
        if ([row.model isEqual:model])  {
            return YES;
        }
        return NO;
    }];
    
    if ([indexPaths count]) {
        AXRow *row = [self.sections rowAtIndexPath:[indexPaths firstObject]];
        [self updateRow:row];
        
    }
    
}




#pragma mark - UITextViewDelegate

- (BOOL)isRectVisible:(CGRect)rect {
    CGRect visibleRect;
    visibleRect.origin = self.tableView.contentOffset;
    visibleRect.origin.y += self.tableView.contentInset.top;
    visibleRect.size = self.tableView.bounds.size;
    visibleRect.size.height -= self.tableView.contentInset.top + self.tableView.contentInset.bottom;
    
    return CGRectContainsRect(visibleRect, rect);
}

- (void)scrollToCursorForTextView:(UITextView *)textView {
    
    //[self.tableView beginUpdates];
    //[self.tableView endUpdates];
    
    
    //Скролится на всю высоту текста в этом случае, что не правильно
    //CGRect cursorRect = [textView caretRectForPosition:textView.selectedTextRange.start];
    
    
    CGRect cursorRect = [textView caretRectForPosition:textView.beginningOfDocument];
    cursorRect = [self.tableView convertRect:cursorRect fromView:textView];
    cursorRect.size.height = cursorRect.size.height + 110;
    
    if (![self isRectVisible:cursorRect]) {
        //cursorRect.size.height += 85;
        [self.tableView scrollRectToVisible:cursorRect animated:YES];
    }
    
}


- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    self.isNoteFirstResponder = YES;
    
    [self scrollToCursorForTextView:textView];
    
}


- (void)textViewDidChange:(UITextView *)textView {
    
    self.isNoteFirstResponder = YES;
    
    [self scrollToCursorForTextView:textView];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    self.isNoteFirstResponder = YES;
    
    NSString *newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    AXRow *row = [self rowForView:textView];
    
    
    if ([row.type isEqualToString:PDHelperCell]) {
        
        DailyNotesNoteModel *model = row.model;
        
        DailyNotesAddNoteHelperCell *cell = (DailyNotesAddNoteHelperCell *)row.cell;
        
        if ([textView isEqual:cell.noteTextView]) {
            model.note = newText;
        }
    }
    
    return YES;
}
/*
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    NSInteger tag = textView.tag;
    NSString *text = textView.text;
    
    AXRow *row = [self rowForView:textView];
    UploadVideoModel *model = row.model;
    
    
    if (tag == DESCRIPTION_TEXT_VIEW_TAG) {
        model.descr = text;
    }
}
*/


#pragma mark - ChildFilterMultipleDelegate

- (void)didSelectedChildModels:(NSArray *)models {
    
    if (!self.selectedDailyNoteModel) {
        NSLog(@"Error");
        return;
    }
    
    self.selectedDailyNoteModel.childrens = models;
    self.selectedDailyNoteModel = nil;
    
    //reload tableview
    [self.tableView  reloadData];
    
    //Если в скрытых спойлерах - краш
    //NSArray *selectorIndexPaths = [self.sections getIndexPathsForRowType:PDSelectChildrenCell];
    //[self.tableView reloadRowsAtIndexPaths:selectorIndexPaths withRowAnimation:UITableViewRowAnimationNone];
    
}


- (NSUInteger)noteIndexForNote:(DailyNotesNoteModel *)note {

    if (!note) {
        return NSNotFound;
    }
    
    NSArray *indexPaths = [self.sections getIndexPathsForRowType:PDTitleCell];
    
    
    if (!indexPaths || [indexPaths count] == 0) {
        return NSNotFound;
    }
    
    NSArray *rows = [self.sections rowsAtIndexPaths:indexPaths];
    
    NSUInteger desireIndex = NSNotFound;
    for (int i = 0; i < [rows count]; i++) {
        AXRow *row = [rows objectAtIndex:i];
        if ([row.model isEqual:note]) {
            desireIndex = i+1;
        }
    }
    
    return desireIndex;
    
}


- (NSArray *)notesExcludeNote:(DailyNotesNoteModel *)note {
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSArray *indexPaths = [self.sections getIndexPathsForRowType:PDTitleCell];
    NSArray *rows = [self.sections rowsAtIndexPaths:indexPaths];
    
    for (AXRow *row in rows) {
        if (![row.model isEqual:note]) {
            [array addObject:row.model];
        }
        
    }
    
    return array;
}



#pragma mark - Actions


- (IBAction)pendingCheckboxAction:(id)sender {

    AXRow *row = [self rowForView:sender];
    
    
    DailyNotesNoteModel *model = row.model;
    
    DailyNotesAddNotePendingCell *cell = (DailyNotesAddNotePendingCell *)row.cell;
    
    model.isPending = !model.isPending;

    [cell updateWithModel:model];

    
}



- (IBAction)copyChildNameAction:(id)sender {
    
    AXRow *row = [self rowForView:sender];
    
    //[self.view endEditing:YES];
        
    DailyNotesNoteModel *model = row.model;
    DailyNotesAddNoteHelperCell *cell = (DailyNotesAddNoteHelperCell *)row.cell;
    
    NSString *template = @"%CFirstName%";
    
    if (cell) {
        
        if ([cell.noteTextView isFirstResponder]) {
            [cell.noteTextView replaceRange:cell.noteTextView.selectedTextRange withText:template];
            model.note = cell.noteTextView.text;
        }
        else {
            if ([model.note length]) {
                model.note = [NSString stringWithFormat:@"%@ %@", model.note, @"%CFirstName%"];
            }
            else {
                model.note = template;
            }
        
            [cell updateWithModel:model];
        }
        
        
    }
    
    
    
}

- (IBAction)insertNewNoteAction:(id)sender {
    
    AXRow *senderRow = [self rowForView:sender];
    if (!senderRow) {
        return;
    }
    
    
    DailyNotesNoteModel *model = [[DailyNotesNoteModel alloc] init];
    
    AXSection *section = [self genSectionForNote:model isCopyAvailable:YES isFirstNote:NO];
    /*
    NSIndexSet *sectionsIndexSet = [self.sections getIndexSetForSectionsWithType:PDNoteSection];

    if (![sectionsIndexSet count]) {
        return;
    }
    */

    
    NSIndexSet *addedSection = [self.sections insertSections:@[section] afterSectionIndex:senderRow.section.sectionIndex];
    
    [self.tableView beginUpdates];
    [self.tableView insertSections:addedSection withRowAnimation:UITableViewRowAnimationRight];
    [self.tableView endUpdates];

    //Обновляем нумерацию note
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
    
}


- (IBAction)deleteNoteAction:(id)sender {
    
    
    AXRow *senderRow = [self rowForView:sender];
    if (!senderRow) {
        return;
    }
    
    NSIndexSet *indexSetToDelete = [NSIndexSet indexSetWithIndex:senderRow.section.sectionIndex];
    
    bool completlyRemove = [self.sections removeSectionsByIndexSet:indexSetToDelete];

    if (completlyRemove) {
        [self.tableView beginUpdates];
        [self.tableView deleteSections:indexSetToDelete withRowAnimation:UITableViewRowAnimationLeft];
        [self.tableView endUpdates];
    
        //Обновляем нумерацию note
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }
    
    
}


- (IBAction)saveAction:(id)sender {
    
    
    [self.view endEditing:YES];
    
    NSMutableArray *notes = [NSMutableArray array];
    
    NSArray *rowsIndexPaths = [self.sections getIndexPathsForRowType:PDTitleCell];
    if ([rowsIndexPaths count]) {
        NSArray *rows = [self.sections rowsAtIndexPaths:rowsIndexPaths];
        for (AXRow *row in rows) {
            [notes addObject:row.model];
        }
    }

    
    if (![notes count]) {
        return;
    }
    
    if ([self isAllRequiredFieldsForNotes:notes]) {
        [self remoteSaveNotes:notes];
    }
}




- (BOOL)isAllRequiredFieldsForNotes:(NSArray *)notes {
    
    
    NSMutableArray *array = [NSMutableArray array];
    
    for (int i = 0; i < [notes count]; i++) {
        
        DailyNotesNoteModel *model = [notes objectAtIndex:i];
        
        if (!model.title || [model.title isEmpty]) {
            [array addObject:[NSString stringWithFormat:@"Note %d: Title", i+1]];
        }
        if (!model.date) {
            [array addObject:[NSString stringWithFormat:@"Note %d: Date", i+1]];
        }
        if (![model.childrens count]) {
            [array addObject:[NSString stringWithFormat:@"Note %d: Select Children", i+1]];
        }
        if (!model.note || [model.note isEmpty]) {
            [array addObject:[NSString stringWithFormat:@"Note %d: Note", i+1]];
        }
    }
    

    
    if ([array count]) {
        NSString *message = [NSString stringWithFormat:@"Not all required fields have been completed to save this form.\nYou must complete the following fields: %@", [array componentsJoinedByString:@", "]];
        Alert(@"Error", message);
        return NO;
    }
     
    return YES;
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"childFilter@Utilities_iPhone"]) {
        
        if (!self.selectedDailyNoteModel) {
            return;
        }
        
        
        ChildFilterViewController *viewController = segue.destinationViewController;
        
        NSMutableArray *selectedChildIds = [NSMutableArray array];
        for (ChildFilterModel *children in self.selectedDailyNoteModel.childrens) {
            [selectedChildIds addObject:children.chId];
        }
        
        viewController.selectedChildIds = selectedChildIds;
       
        viewController.showArchive = NO;
        viewController.multipleSelect = YES;
        viewController.isSelectAll = YES;
        viewController.filterMultipleDelegate = self;
        
    }
    
}



@end
