//
//  ResetPinViewController.m
//  pd2
//
//  Created by i11 on 23/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ResetPinViewController.h"
#import "UserManager.h"
#import "User.h"
#import "NetworkManager.h"
#import "AccessTimeManager.h"
#import "RegisterForceManager.h"
#import "SetupPinViewController.h"
#import "LoginTableViewController.h"

#import "UnavailableSectionManager.h"

#import "PSTAlertController.h"

#import "NSString+VersionNumbers.h"
#import "WL.h"

@interface ResetPinViewController ()

@end


@implementation ResetPinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
#warning tap area у кнопки сделать больше

    UIImage *backImage = [[UIImage imageNamed:@"back-icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.backButton.tintColor = [UIColor whiteColor];
    [self.backButton setImage:backImage forState:UIControlStateNormal];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self.view action:@selector(endEditing:)];
    [self.view addGestureRecognizer:tap];
    
    self.titleLabel.text = [NSString stringWithFormat:@"Reset Pin for %@", [self.user.userName capitalizedString]];
    
    [self setButton:self.forgot withAlignment:NSTextAlignmentLeft];
    [self setButton:self.sign withAlignment:NSTextAlignmentRight];
    
    //self.passwordView.layer.borderColor = [[MyColorTheme sharedInstance] loginTextColor].CGColor;
    //self.passwordView.layer.borderWidth = 0.5f;

    
    self.passwordTextField.delegate = self;
    //[self runColorCheck:YES];

    self.logoImageView.image = [UIImage imageNamed:[WL logoResetPinFileName]];
    
    self.view.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];

    
    [self disableInactiveTimer];

    [[UserManager sharedManager] debugPrintUsers];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    self.logoTop.constant = [WL logoResetPinRect].origin.y;
    self.logoWidth.constant = [WL logoResetPinRect].size.width;
    self.logoHeight.constant = [WL logoResetPinRect].size.height;
    
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}


- (void)setColorTheme
{
    [super setColorTheme];
    self.view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
}

- (void)runColorCheck:(BOOL)bo {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        bo ? [[MyColorTheme sharedInstance]setBlueTheme] : [[MyColorTheme sharedInstance]setYellowTheme];
        [self setColorTheme];
        [self runColorCheck:!bo];
        
    });
}


- (IBAction)submitButton:(UIButton *)sender {
    
    NSString *password = self.passwordTextField.text;
    
    if(password.length < 1) {
        //TODO: password.length в EXTERN
#warning TODO: right alert password length
        Alert(@"Error", @"Password is too short");
        [self.passwordTextField becomeFirstResponder];
        return;
    }
    
    if (self.user) {
        
        [[NetworkManager sharedInstance] resetPinForUserID:self.user.uid password:password subdomain:self.user.subDomain withCompletion:^(id response, NSError *error) {
            
            if (response)
            {
                if([[response valueForKeyPath:@"result.complete"]intValue]==1)
                {
                    
                    [self.user manualSetHavePin:NO];
                    //[self performSegueWithIdentifier:@"setupPinSegue" sender:self];
                    
                    [self doLogin];
                    
                }
                else
                {
                    Alert(@"Error",[response valueForKeyPath:@"result.msg"] );
                    
                }
            }
            else if(error)
            {
                Alert(@"Error", @"Error happened while auth");
            }
        }];
        
    }

}

- (IBAction)backAction:(UIButton *)sender {
    
    
    if (self.user.isPinFailedAttempt) {

        //Если перешел после 5ти неправильных вводов пина - отправляем на страницу логина по паролю
        //т.к. login vc может быть не в стеке navigation controller - задать анимацию

        //TODO: сделать изящнее переход
        LoginTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginTableViewController"];
        vc.activeUser = self.user;
        [self.navigationController pushViewController:vc animated:NO];
        
        /*
        [UIView animateWithDuration:0.75
                         animations:^{
                             [UIView setAnimationCurve:UIViewAnimationCurveLinear];
                             [self.navigationController pushViewController:vc animated:NO];
                             [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.navigationController.view cache:NO];
        }];
        */
        
    } else {

        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}


- (IBAction)forgotPasswordAction:(id)sender {
    
    //http://client`s/index.php?option=com_sted&controller=user&task=reset

    NSString *link = [NSString stringWithFormat:@"http://%@/index.php?option=com_sted&controller=user&task=reset", [[NetworkManager sharedInstance] subdomain]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
}

- (IBAction)signUpAction:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.babysdays.com/index.php?option=com_billing&view=billing"]];
    
}


#warning временный костыль, пока reset pin api не возвращает токен для следующего шага
- (void)doLogin {
    [[NetworkManager sharedInstance] loginWithUserId:self.user.uid password:self.passwordTextField.text subdomain:self.user.subDomain completion:^(id response, NSError *error) {
        if (response)
        {
            if([[response valueForKeyPath:@"result.complete"]intValue]==1)
            {
                
                
                NSString *iosString;
                
                @try {
                    iosString = [response valueForKeyPath:@"result.ios"];
                }
                @catch (NSException *exception) {
                    NSLog(@"Exception. Reason: %@", exception.reason);
                    NSLog(@"Для parent'а неверный response result");
                }
                
                NSNumber *iosUpdate = iosString ? [NSNumber numberWithInt:[iosString intValue]] : nil;
                
                
                //Сервер рекомендует обновиться
                if (iosUpdate && [iosUpdate isEqualToNumber:@1]) {
                    //Alert(@"Notice", @"Please update your application to the latest version");
                    
                    PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning" message:[response valueForKeyPath:@"result.msg"] preferredStyle:PSTAlertControllerStyleAlert];
                    
                    [alert addAction:[PSTAlertAction actionWithTitle:@"Update" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                        
                        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[WL appStoreLink]]];
                        [self loginProcedureWithResponse:response subdomain:self.user.subDomain login:self.user.userName];
                    }]];
                    
                    [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:^(PSTAlertAction *action) {
                        [self loginProcedureWithResponse:response subdomain:self.user.subDomain login:self.user.userName];
                        
                        
                    }]];
                    
                    
                    [alert showWithSender:self controller:self animated:YES completion:nil];
                    
                }
                else {
                    [self loginProcedureWithResponse:response subdomain:self.user.subDomain login:self.user.userName];
                }
                
            }
            else
            {
                NSString *iosString;
                
                @try {
                    iosString = [response valueForKeyPath:@"result.ios"];
                }
                @catch (NSException *exception) {
                    NSLog(@"Exception. Reason: %@", exception.reason);
                    NSLog(@"Неверный response result");
                }
                
                NSNumber *iosUpdate = iosString ? [NSNumber numberWithInt:[iosString intValue]] : nil;
                
                if (iosUpdate && [iosUpdate isEqualToNumber:@2]) {
                    //Сервер настоятельно рекоммендует обновиться
                    //Alert(@"Error", @"Please update your application to the latest version");
                    //Alert(@"Error",[response valueForKeyPath:@"result.msg"] );
                    
                    
                    PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning" message:[response valueForKeyPath:@"result.msg"] preferredStyle:PSTAlertControllerStyleAlert];
                    
                    [alert addAction:[PSTAlertAction actionWithTitle:@"Update" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                        
                        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[WL appStoreLink]]];
                        
                    }]];
                    
                    [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:nil]];
                    
                    [alert showWithSender:self controller:self animated:YES completion:nil];
                                        
                    return;
                    
                }
                else {
                    //для блокированных систем выводим msg в заголовок, text в тело сообщения. +tel по кнопке звонка
                    NSString *msgString = [response valueForKeyPath:@"result.msg"];
                    NSString *textString = [response valueForKeyPath:@"result.text"];
                    NSString *telString = [response valueForKeyPath:@"result.tel"];
                    
                    if (msgString && textString && telString) {
                        
                        PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:msgString message:textString preferredStyle:PSTAlertControllerStyleAlert];
                        
                        [alert addAction:[PSTAlertAction actionWithTitle:@"OK" style:PSTAlertActionStyleDefault handler:nil]];
                        
                        [alert addAction:[PSTAlertAction actionWithTitle:@"Call" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                            
                            NSString *tel = [NSString stringWithFormat:@"tel:%@", telString];
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
                            
                        }]];
                        
                        [alert showWithSender:self controller:self animated:YES completion:nil];
                        
                    }
                    else {
                        Alert(@"Error",[response valueForKeyPath:@"result.msg"]);
                    }

                }
                
            }
        }
        else if(error)
        {
            Alert(@"Error", @"Error happened while auth");
        }
    }];
}



- (void)loginProcedureWithResponse:(NSDictionary *)response subdomain:(NSString *)subDomain login:(NSString *)login{
    
    
    NSString *uidString;
    NSString *aclString;
    NSString *vString;
    NSString *appString;
    NSString *apiVersionString;
    NSString *betaString;
    NSString *j3String;

    @try {
        uidString = [response valueForKeyPath:@"result.aid"];
        aclString = [response valueForKeyPath:@"result.acl"];
        vString = [response valueForKeyPath:@"result.v"];
        appString = [response valueForKeyPath:@"result.app"];
        apiVersionString = [response valueForKeyPath:@"result.api"];
        betaString = [response valueForKeyPath:@"result.beta"];
        j3String = [response valueForKeyPath:@"result.version_system"];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception. Reason: %@", exception.reason);
        NSLog(@"Для parent'а неверный response result");
    }
    
    if (![uidString isKindOfClass:[NSString class]] || ![aclString isKindOfClass:[NSString class]]) {
        Alert(@"Error", @"Wrong user");
        //self.submitButton.enabled=YES;
        return;
    }
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    
    int uid = [uidString intValue];
    int acl = [aclString intValue];
    BOOL v = [vString boolValue];
    BOOL appDenied = appString && [appString intValue] == 0 ? YES : NO;
    NSArray *unavailableSections = [response valueForKeyPath:@"result.section_block"];
    NSString *unavailableAlert = [response valueForKeyPath:@"result.section_block_message"];
    
    
    [NetworkManager sharedInstance].serverApiVersion = apiVersionString;
    
    //Доступ на данном сервере специально запрещен
    if (appDenied) {
        Alert(@"Error", @"Sorry, your subscription plan does not support the use of the mobile app");
        //self.submitButton.enabled=YES;
        return;
    }
    
    //Приложение не поддерживает версию апи на сервере. Версия должна быть ниже
    if (apiVersionString) {
        if (!([[API_VERSION_THRESHOLD shortenedVersionNumberString] compare:[apiVersionString shortenedVersionNumberString] options:NSNumericSearch] == NSOrderedDescending)) {
            //!(apiVersionString < API_VERSION_THRESHOLD)
            Alert(@"Error", @"Please update your application to the latest version");
            //self.submitButton.enabled=YES;
            return;
            
        }
    }
    
    
    //Доступ для буккипинга запрещен
    if (acl == 5) {
        Alert(@"Error", @"No Access");
        //self.submitButton.enabled=YES;
        return;
    }
    
    if (betaString && [betaString isKindOfClass:[NSNumber class]] && [betaString intValue] == 1) {
        [NetworkManager sharedInstance].isBeta = YES;
    }
    else {
        [NetworkManager sharedInstance].isBeta = NO;
    }
    
    if (j3String && [j3String isKindOfClass:[NSString class]] && [j3String isEqualToString:@"j3"]) {
        [NetworkManager sharedInstance].isJ3 = YES;
    }
    else {
        [NetworkManager sharedInstance].isJ3 = NO;
    }
    
    
    [[NetworkManager sharedInstance] updateSessionTimer];
    
    [NetworkManager sharedInstance].baseUrl = self.user.subDomain;
    
    
    //10.06.2015 Перенесено из NetworkManager
    [DataManager sharedInstance].adminId= uid;
    [DataManager sharedInstance].adminAccessLevel = acl;
    [NetworkManager sharedInstance].token=[response valueForKeyPath:@"result.access_token"];
    [NetworkManager sharedInstance].isScottish = v;
    [[NetworkManager sharedInstance]getAdminWithId:[DataManager sharedInstance].adminId completion:^(id responseAdmin, NSError *error) {
        [DataManager sharedInstance].adminInfo= [NSMutableDictionary dictionaryWithDictionary:[responseAdmin valueForKey:@"admin_info"]];
    }];
    
    [NetworkManager sharedInstance].videoServerHost = nil;
    NSLog(@"admin id :%d",[DataManager sharedInstance].adminId);
    
    self.user.acl = acl;
    [[UserManager sharedManager] successfullyLoggedUser:self.user];
    
    //Для парента заполняем доступные секции
    if (self.user.userType == UserTypeParent) {
        NSDictionary *options = nil;
        NSString *firstName;
        NSString *lastName;
        NSNumber *parentNum;
        @try {
            options = [response valueForKeyPath:@"result.additional_info.options"];
            firstName = [response valueForKeyPath:@"result.additional_info.first_name"];
            lastName = [response valueForKeyPath:@"result.additional_info.last_name"];
            parentNum = [response valueForKeyPath:@"result.parent_num"];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception. Reason: %@", exception.reason);
            NSLog(@"Для parent'а неверный response result");
        }
        [self.user fillUserOptionsWithDict:options]; //проверка внутри
        self.user.firstName = firstName;
        self.user.lastName = lastName;
        
        if (parentNum && [parentNum isKindOfClass:[NSNumber class]]) {
            self.user.parentNum = @([parentNum intValue] + 1);
        }
        
        [[UserManager sharedManager] saveLocalUsers];
        
        /*
         NSDictionary *additionalInfo = [response objectForKey:@"additional_info"];
         if (additionalInfo && [additionalInfo isKindOfClass:[NSDictionary class]]) {
         NSDictionary *options = [additionalInfo objectForKey:@"options"];
         [user fillUserOptionsWithDict:options]; //проверка внутри
         }
         */
    }
    
    if (self.user.userType == UserTypeAdmin && [NetworkManager sharedInstance].isJ3) {
        NSDictionary *options = nil;
        @try {
            options = [response valueForKeyPath:@"result.options"];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception. Reason: %@", exception.reason);
            NSLog(@"Для admin неверный response result");
        }
        [self.user updateAccessLevelWithDict:options]; //проверка внутри
    }
    
    [[AccessTimeManager sharedInstance] disableTimer];
    if (self.user.userType == UserTypeAdmin && self.user.acl != 1) {
        //time access
        NSNumber *timeLeft = [response valueForKeyPath:@"result.left_time"];
        if (timeLeft && [timeLeft isKindOfClass:[NSNumber class]]) {
            [[AccessTimeManager sharedInstance] startWithSeconds:[timeLeft longLongValue]];
        }
    }
    
    [[UnavailableSectionManager sharedInstance] updateWithArray:unavailableSections andMessage:unavailableAlert];
    
    //Переход в register для парентсайда при наличии неподписанных часов
    //[NetworkManager sharedInstance].registerLoginArray = [response valueForKeyPath:@"result.register.ActiveChildrenList"];
    [[RegisterForceManager sharedManager] updateWithRegisterArray:[response valueForKeyPath:@"result.register.ActiveChildrenList"]];
    
    //Старт менеджера видео загрузок
    
    
    [self performSegueWithIdentifier:@"setupPinSegue" sender:self];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"setupPinSegue"]) {
        SetupPinViewController *vc = segue.destinationViewController;
        vc.user = self.user;
    }
    
}

- (void)setButton:(UIButton *)button withAlignment:(NSTextAlignment)alignmentStyle {
    
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:alignmentStyle];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    
    UIFont *font = [UIFont systemFontOfSize:12.6f];
    
    NSDictionary *dict = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
                           NSFontAttributeName:font,
                           NSParagraphStyleAttributeName:style,
                           NSForegroundColorAttributeName:[UIColor whiteColor]};
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:button.titleLabel.text    attributes:dict]];
    
    [button setAttributedTitle:attString forState:UIControlStateNormal];
    [[button titleLabel] setNumberOfLines:0];
    [[button titleLabel] setLineBreakMode:NSLineBreakByWordWrapping];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:self.passwordTextField]) {
        [textField resignFirstResponder];
        [self submitButton:nil];
    }
    return YES;
}


@end
