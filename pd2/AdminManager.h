//
//  AdminManager.h
//  pd2
//
//  Created by i11 on 10/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Singleton.h"

@class Admin2Model;

extern NSString* const AdminManagerHasBeenUpdateAdminsNotification;
extern NSString* const AdminManagerNotificationKey;



@interface AdminManager : Singleton

@property (nonatomic, strong) NSArray *admins;

- (Admin2Model *)adminWithAdminId:(NSNumber *)adminId;
- (NSArray *)adminsOnly;
- (NSArray *)adminOnlyNames;

- (void)refresh;
- (BOOL)isLoaded;

@end
