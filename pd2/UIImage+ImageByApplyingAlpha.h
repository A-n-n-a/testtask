//
//  UIImage+ImageByApplyingAlpha.h
//  pd2
//
//  Created by i11 on 16/03/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageByApplyingAlpha)

- (UIImage *)imageByApplyingAlpha:(CGFloat)alpha;

@end
