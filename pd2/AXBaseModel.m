//
//  AXBaseModel.m
//  pd2
//
//  Created by i11 on 09/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AXBaseModel.h"
#import "AXDataCheck.h"

#import "NSDateFormatter+Locale.h"
#import "NSString+HTML.h"


@implementation AXBaseModel

#pragma mark - 


- (instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        
        if (![self isDictionary:dict]) {
            return nil;
        }
        
    }
    return self;
}


- (instancetype)initWithArray:(NSArray *)array {
    self = [super init];
    if (self) {
        
        if (![self isArray:array]) {
            return nil;
        }
        
    }
    return self;
}


- (BOOL)isExist {
    return YES;
}

- (NSDate *)dateFrom:(id)data withDateFormat:(NSString *)dateFormat {
    
    return [AXBaseModel dateFrom:data withDateFormat:dateFormat];
    
}

+ (NSDate *)dateFrom:(id)data withDateFormat:(NSString *)dateFormat {
    
    NSString *safeString = [self safeString:data];
    
    if (!safeString) {
        return nil;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] initWithSafeLocale];
    [dateFormatter setDateFormat:dateFormat];
    
    return [dateFormatter dateFromString:safeString];
    
}


#pragma mark - fast safe

- (NSNumber *)safeNumber:(id)data {
    return [AXDataCheck safeNumber:data];
}

- (BOOL)safeBool:(id)data {
    return [AXDataCheck safeBool:data];
}

- (int)safeInt:(id)data {
    return [AXDataCheck safeInt:data];
}

- (float)safeFloat:(id)data {
    return [AXDataCheck safeFloat:data];
}

- (NSString *)safeString:(id)data {
    return [AXDataCheck safeString:data];
}

+ (NSString *)safeString:(id)data {
    return [AXDataCheck safeString:data];
}

//TODO: перевести выборочно
- (NSString *)safeStringAndDecodingHTML:(id)data {
    return [[AXDataCheck safeString:data] stringByDecodingHTMLEntities];
}


- (NSString *)safeEmptyStringIfNil:(id)data {
    return [AXDataCheck safeEmptyStringIfNil:data];
}

- (NSDictionary *)safeDictionary:(id)data {
    
    if (data && [data isKindOfClass:[NSDictionary class]]) {
        return data;
    }
    
    return nil;
    
}

- (NSArray *)safeArray:(id)data {
    
    if (data && [data isKindOfClass:[NSArray class]]) {
        return data;
    }
    
    return nil;
    
}



#pragma mark - check types 

- (BOOL)isArray:(NSArray *)array {
    
    return ([array isKindOfClass:[NSArray class]] && array);
}

- (BOOL)isDictionary:(NSDictionary *)dict {
    
    return ([dict isKindOfClass:[NSDictionary class]] && dict);
}

@end
