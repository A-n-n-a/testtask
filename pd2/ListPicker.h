//
//  ListPicker.h
//  Exchange
//
//  Created by User on 16.06.14.
//  Copyright (c) 2014 Vexadev LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListPicker : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UIGestureRecognizerDelegate>

@property (copy) void (^handler)(NSString *string, int index);
@property (copy) void (^finishHandler)();
@property (copy) void (^clearHandler)();

@property int selectedIndex;
@property NSString *selectedString;
@property (nonatomic,retain)NSArray *dataArray;
@property (nonatomic,assign) BOOL showClearButton;
@property (nonatomic, strong) NSString *rightButtonCustomTitle;
@property (nonatomic, strong) NSString *leftCustomTitle;

@property (nonatomic,retain)IBOutlet UIPickerView *picker;
@property (nonatomic,retain)IBOutlet UIBarButtonItem *doneButton, *cancelButton;
@property (nonatomic,retain)IBOutlet UIBarButtonItem *clearButton;


-(void)show;
-(IBAction)hide;
-(IBAction)clear;

@end
