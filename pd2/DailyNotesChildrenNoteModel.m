//
//  DailyNotesChildrenNoteModel.m
//  pd2
//
//  Created by Andrey on 09/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesChildrenNoteModel.h"
#import "NSDate+Printable.h"

@implementation DailyNotesChildrenNoteModel
/*
 {
 "id": "43",
 "note": "123412321331",
 "date_added": "2017-11-02 10:54:28",
 "title": "2nd Nov 2017 @ 10:54",
 "added_by": "677",
 "pending": "0",
 "masterId": null,
 "dom": "2",
 "first_name": "Sarah",
 "last_name": "Johnson",
 "comments": 0,
 "viewed": 0,
 "unviewed": 0
 }
*/


- (instancetype)initWithDict:(NSDictionary *)dict {
    
    self = [super initWithDict:dict];
    if (self) {
        
        self.noteId = [self safeNumber:dict[@"id"]];
        self.noteTitle = [self safeString:dict[@"title"]];
        self.note = [self safeString:dict[@"note"]];
        self.adminFirstName = [self safeString:dict[@"first_name"]];
        self.adminLastName = [self safeString:dict[@"last_name"]];
        self.date = [self dateFrom:dict[@"date_added"] withDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        self.commentsCount = [self safeInt:dict[@"comments"]];
    
    }
    return self;
    
}

- (BOOL)isExist {
    
    if (self.noteId && self.date) {
        return YES;
    }
    
    return NO;
    
}



//Filter
- (BOOL)modelContain:(NSString *)containString {
    
    if (!containString || ![containString isKindOfClass:[NSString class]]) {
        return YES;
    }
    
    if (!containString.length) {
        return YES;
    }
    
    if (self.adminFirstName && [self.adminFirstName rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    if (self.adminLastName && [self.adminLastName rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    if (self.noteTitle && [self.noteTitle rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    if (self.date && [[self.date printableDefault] rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    return NO;
    
}




@end
