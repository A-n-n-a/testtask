//
//  PincodeViewController.h
//  pd2
//
//  Created by i11 on 22/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Pd2TableViewController.h"

@class User;

@interface SetupPinViewController : Pd2TableViewController
- (IBAction)pinButtonTouchUpInside:(id)sender;
- (IBAction)cancelAction:(id)sender;
- (IBAction)skipAction:(UIButton *)sender;

@property (strong, nonatomic) User *user;


@end
