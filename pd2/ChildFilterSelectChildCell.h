//
//  ChildFilterSelectChildCell.h
//  pd2
//
//  Created by Andrey on 27/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@interface ChildFilterSelectChildCell : AXTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *selectChildLabel;
@property (weak, nonatomic) IBOutlet UIImageView *childIconImageView;

- (void)update;

@end
