//
//  AXSections.h
//  pd2
//
//  Created by i11 on 10/02/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AXSection.h"

@class AXRow, AXHeader;

@interface AXSections : NSObject <AXSectionDelegate>

@property (strong, nonatomic) NSMutableArray *sections;


//TODO:
//insertSectionAfterType:... last, first, index

- (NSInteger)indexForSection:(AXSection *)section; //protocol

- (void)addSection:(AXSection *)section;
- (void)addSections:(NSArray *)sections;
- (void)removeSections;
- (BOOL)removeSectionsByIndexSet:(NSIndexSet *)indexSet;

- (AXSection *)sectionAtIndex:(NSUInteger)index;
- (NSArray *)sectionsAtIndexSet:(NSIndexSet *)indexSet;
- (AXSection *)sectionAtIndexPath:(NSIndexPath *)indexPath;
- (AXSection *)sectionWithSectionType:(NSString *)sectionType;
- (AXRow *)rowAtIndexPath:(NSIndexPath *)indexPath;
- (NSArray *)rowsAtIndexPaths:(NSArray *)indexPaths;
- (AXRow *)rowWithRowType:(NSString *)rowType; //return AXRow or nil
- (BOOL)isRowWithRowType:(NSString *)rowType;
- (BOOL)isRowAvailableAtIndexPath:(NSIndexPath *)indexPath; //Для скрытых секций вернет no;

- (AXHeader *)headerAtSectionIndex:(NSInteger)index;

- (NSUInteger)countOfSections;
- (NSUInteger)countOfRowsInSection:(NSUInteger)index;

- (CGFloat)heightForRowAtIndexPath:(NSIndexPath *)indexPath;

- (CGFloat)heightForHeaderInSection:(NSUInteger)index;

- (void)needUpdateHeightForRowType:(NSString *)rowType;
- (void)needUpdateHeightForRowTypes:(NSArray *)rowTypes;


- (NSString *)rowTypeAtIndexPath:(NSIndexPath *)indexPath;
- (NSUInteger)getSectionIndexForView:(UIView *)view;

- (NSArray *)getIndexPathsForRowWithModel:(id)model;
- (NSArray *)getIndexPathsForRowType:(NSString *)type;
- (NSArray *)getIndexPathsNotHiddenForRowType:(NSString *)type;
- (NSArray *)getIndexPathsForRows:(NSArray *)rows;
- (NSArray *)getIndexPathsForRowTypes:(NSArray *)rowTypes;
- (NSArray *)getIndexPathsForRowType:(NSString *)type atIndex:(id)index withCondition:(BOOL(^)(AXSection *section, AXRow *row))condition;
- (NSUInteger)getIndexForRowType:(NSString *)rowType atIndexPath:(NSIndexPath *)indexPath;
- (NSArray *)getIndexPathsForRowWithTag:(NSString *)tag;
- (void)removeArrayRowsByIndexPaths:(NSArray *)indexPaths;
- (NSIndexSet *)genIndexSetForSection:(NSUInteger)section fromIndexPaths:(NSArray *)indexPaths withUsedIndexPaths:(NSMutableArray **)usedIndexPath;

- (NSIndexSet *)getIndexSetForSections:(NSArray *)sections;
- (NSIndexSet *)getIndexSetForSectionsWithType:(NSString *)type;
- (NSIndexSet *)getIndexSetForSectionsWithTypes:(NSArray *)types;

- (NSIndexSet *)getIndexSetForSectionsWithTag:(NSString *)tag;
- (NSIndexSet *)getIndexSetForSectionsWithTag:(NSString *)tag andExcludeTag:(NSString *)excludeTag;
- (NSIndexSet *)getIndexSetForSectionsWithTags:(NSSet *)tags;

//Диапазоны
- (NSArray *)getRangeIndexPathsFromRowType:(NSString *)type1 atIndex:(id)index1 toRowType:(NSString *)type2 atIndex:(id)index2;

#pragma mark - insertSections after
- (NSIndexSet *)insertSections:(NSArray *)sections afterSectionType:(NSString *)sectionType;
- (NSIndexSet *)insertSections:(NSArray *)sections afterSectionIndex:(NSUInteger)sectionIndex;

#pragma mark - insertRows after
- (NSArray *)insertRows:(NSArray *)rows afterIndexPath:(NSIndexPath *)indexPath;
- (NSArray *)insertRows:(NSArray *)rows afterRowType:(NSString *)rowType atIndex:(id)index;
- (NSArray *)insertRows:(NSArray *)rows afterFirstRowType:(NSString *)rowType;
- (NSArray *)insertRows:(NSArray *)rows afterLastRowType:(NSString *)rowType;

- (NSArray *)insertRows:(NSArray *)rows fromIndex:(NSInteger)index inSection:(AXSection *)section;

#pragma mark - deleteRows after
- (NSArray *)deleteRowsAfterRowType:(NSString *)rowType;

#pragma mark - replace
- (NSArray *)replaceOldRow:(AXRow *)oldRow toNewRow:(AXRow *)newRow;

#pragma mark - remember

- (void)rememberType:(NSString *)type withHeight:(CGFloat)height identifier:(NSString *)identifier;
- (void)rememberType:(NSString *)type withEstimatedHeight:(CGFloat)estimatedHeight identifier:(NSString *)identifier;
- (AXRow *)genRowWithType:(NSString *)rowType;
- (AXHeader *)genHeaderWithType:(NSString *)headerType;


@end
