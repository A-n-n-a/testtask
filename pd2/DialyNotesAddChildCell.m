//
//  DialyNotesAddChildCell.m
//  pd2
//
//  Created by Andrey on 11/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "DialyNotesAddChildCell.h"
#import "DailyNotesNoteModel.h"
#import "ChildFilterModel.h"
#import "UILabel+LinesCount.h"


@implementation DialyNotesAddChildCell

- (void)updateWithModel:(DailyNotesNoteModel *)model {
    
    if (model.childrens && [model.childrens count] > 0) {
        
        NSMutableArray *namesArray = [NSMutableArray array];
        for (ChildFilterModel *child in model.childrens) {
            [namesArray addObject:[child childrenShortName]];
        }
        
        self.childrenNamesLabel.text = [namesArray componentsJoinedByString:@", "];
        self.childrenNamesLabel.numberOfLines = 0;
        NSInteger lineCount = [self.childrenNamesLabel linesCount];
        self.childrenNamesLabel.numberOfLines = 1;
        
        if (lineCount < 2) {
            self.mainLabel.text = [NSString stringWithFormat:@"%lu Children Selected:", (unsigned long)[model.childrens count]];
            self.labelSpacerConstraint.constant = 3.f;
        }
        else {
            self.mainLabel.text = [NSString stringWithFormat:@"%lu Children Selected", (unsigned long)[model.childrens count]];
            self.childrenNamesLabel.text = @"";
            self.labelSpacerConstraint.constant = 0.f;
        }
        
        
        
    }
    else {
        self.mainLabel.text = @"Select Children";
        self.childrenNamesLabel.text = @"";
        self.labelSpacerConstraint.constant = 0.f;
    }
    
    
    self.childrenNamesLabel.textColor = [MyColorTheme sharedInstance].backgroundColor;
    
    
    self.childrenImageView.image = [[UIImage imageNamed:[NetworkManager sharedInstance].isJ3 ? @"j3_noCirc_spoiler_children_.png" : @"activeChildrenIcon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.childrenImageView.tintColor = [UIColor darkGrayColor];
    
}

/*
- (void)updateImageWithModel:(DepartedChildrenModel *)model {
    //Onscreen
    [self updateProfileImageView:self.picImageView withImagePath:model.photoPath andDefaultImagePath:nil downloadImageCompletition:nil];
    
}
*/

@end
