//
//  ChildFilterViewController.m
//  pd2
//
//  Created by Andrey on 10/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "ChildFilterViewController.h"
//#import "ChildFilterChildCell.h"
#import "ChildFilterChildShortCell.h"

#import "ChildFilterModel.h"


@interface ChildFilterViewController () <UITextFieldDelegate>

{
    NSString *PDActiveChildrenSection;
    NSString *PDArchivedChildrenSection;
    
    NSString *PDActiveHeader;
    NSString *PDArchivedHeader;

    NSString *PDButtonCell;
    NSString *PDChildrenCell;
    NSString *PDSelectAllCell;
    NSString *PDSelectInRoomCell;
    NSString *PDSpacer45Cell;
}


@property (nonatomic, strong) NSMutableDictionary *roomsAndChildDictionary;
@property (nonatomic, strong) NSMutableDictionary *roomsAndChildFilteredDictionary;
@property (nonatomic, strong) NSString *filterString;
@property (nonatomic, weak) UITextField *filterTextField;

@property (nonatomic, strong) NSArray *childrensArray;


@end




@implementation ChildFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initRows];

    //Top section
    AXSection *topSection = [[AXSection alloc] initWithType:PDTopSection];
    if (self.multipleSelect) {
        [topSection addRow:[self.sections genRowWithType:PDButtonCell]];
    }
    [topSection addRow:[self.sections genRowWithType:PDFilterCell]];
    
    [self.sections addSection:topSection];
    
    [self refreshData];
    
    if (self.multipleSelect) {
        [self.freeTapGestureRecognizer setEnabled:YES];
    }
    else {
        [self.freeTapGestureRecognizer setEnabled:NO];
    }
    
    AXSection *bottomSection = [[AXSection alloc] initWithType:PDBottomSection];
    if (self.multipleSelect) {
        [bottomSection addRow:[self.sections genRowWithType:PDButtonCell]];
    }
    [bottomSection addRow:[self.sections genRowWithType:PDSpacerCell]];
    [self.sections addSection:bottomSection];

    self.topTitle = @"Select Child";
    self.view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
}


- (void)initRows {
    [super initRows];
    
    PDActiveHeader = @"PDActiveHeader";
    PDArchivedHeader = @"PDArchivedHeader";
    
    PDActiveChildrenSection = @"PDActiveChildrenSection";
    PDArchivedChildrenSection = @"PDArchivedChildrenSection";
    
    PDButtonCell = @"PDButtonCell";
    PDChildrenCell = @"PDChildrenCell";
    PDSelectAllCell = @"PDSelectAllCell";
    PDSelectInRoomCell = @"PDSelectInRoomCell";
    PDSpacer45Cell = @"PDSpacer45Cell";
    
    
    [self.sections rememberType:PDActiveHeader withHeight:44.f identifier:@"activeHeaderCell"];
    [self.sections rememberType:PDArchivedHeader withHeight:44.f identifier:@"archiveHeaderCell"];

    [self.sections rememberType:PDButtonCell withEstimatedHeight:56.f identifier:@"buttonCell"];
    [self.sections rememberType:PDChildrenCell withEstimatedHeight:173.f identifier:self.multipleSelect ? @"childrenCheckboxCell" : @"childrenCell"];
    [self.sections rememberType:PDSelectAllCell withHeight:44.f identifier:@"selectChildrenCell"];
    [self.sections rememberType:PDSelectInRoomCell withHeight:44.f identifier:@"selectChildrenInRoomCell"];
    
    [self.sections rememberType:PDSpacer45Cell withEstimatedHeight:45.f identifier:@"spacerCell"];
    
    [self.contentFitCells addObject:PDChildrenCell];
}


#pragma mark - Configure cells

- (UIView *)viewForPDRoomHeader:(AXHeader *)header inSection:(AXSection *)section {
    
    RoomHeaderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:header.identifier];
    UIView *view = cell.contentView;
    
    /*
     //выставляется само в классе объекта
     UIImageView *imageView = (UIImageView *)[view viewWithTag:HEADER_IMAGE_VIEW_TAG];
     imageView.image = [[UIImage imageNamed:@"photo-icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
     header.imageView = imageView;
     */
    
    [self updateHeaderArrowImage:cell.arrowImageView withState:section.isHide];
    
    
    if (header.title) {
        [cell updateWithRoomName:header.title];
        [cell updateWithRoomImagePath:[[DataManager sharedInstance] roomImagePathWithTitle:header.title]];
    }
    
    
    UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
    [self addBorder:PDBorderBottom toView:containerView forToolbar:NO];
    
    
    return view;
    
}

- (UIView *)viewForPDActiveHeader:(AXHeader *)header inSection:(AXSection *)section {
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:header.identifier];
    UIView *view = cell.contentView;
    
    UILabel *label = [view viewWithTag:HEADER_LABEL_TAG];
    
    UIImageView *arrowImageView = (UIImageView *)[view viewWithTag:HEADER_ARROW_IMAGE_VIEW_TAG];
    [self updateHeaderArrowImage:arrowImageView withState:section.isHide];
    
    
    UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
    
    
    [self addBorder:PDBorderTopAndBottom toView:containerView forToolbar:NO];
    
    
    return view;
    
}


- (UIView *)viewForPDArchivedHeader:(AXHeader *)header inSection:(AXSection *)section {
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:header.identifier];
    UIView *view = cell.contentView;
    
    UILabel *label = [view viewWithTag:HEADER_LABEL_TAG];
    
    UIImageView *arrowImageView = (UIImageView *)[view viewWithTag:HEADER_ARROW_IMAGE_VIEW_TAG];
    [self updateHeaderArrowImage:arrowImageView withState:section.isHide];
    
    
    UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
    
    
    [self addBorder:PDBorderTopAndBottom toView:containerView forToolbar:NO];
    
    
    return view;
    
}



- (UITableViewCell *)configurePDFilterCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    FilterCell *cell = (FilterCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        self.filterTextField = cell.filterTextField;
        
        cell.filterTextField.delegate = self;
        cell.filterTextField.text = [self.filterString copy];
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}


- (UITableViewCell *)configurePDChildrenCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    ChildFilterChildShortCell *cell = (ChildFilterChildShortCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        ChildFilterModel *model = row.model;
        
        cell = [self offscreenRenderingCell:cell withRow:row];
        [cell updateImageWithModel:model];
        
        
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        

        [self addBorder:PDBorderBottomDynamic toView:containerView forToolbar:NO];
        containerView.backgroundColor = [[MyColorTheme sharedInstance] spoilerBackGroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}

/*
- (UITableViewCell *)configurePDSelectAllCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    SelectRoomChildrensCell *cell = (SelectRoomChildrensCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        [cell check:[self isAllChildrenChecked]];
        
        
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        
        [self addBorder:PDBorderTop toView:containerView forToolbar:NO];
        [self addBorder:PDBorderBottom toView:containerView forToolbar:NO];
        
        //cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        cell.backgroundColor = [UIColor whiteColor];
        
    }
    
    return cell;
}
*/

/*
- (UITableViewCell *)configurePDSelectInRoomCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    SelectRoomChildrensCell *cell = (SelectRoomChildrensCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        [cell check:[self isAllChildrenCheckedInRoomName:row.text]];
        
        
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        
        [self addBorder:PDBorderTop toView:containerView forToolbar:NO];
        [self addBorder:PDBorderBottom toView:containerView forToolbar:NO];
        
        //cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        cell.backgroundColor = [UIColor whiteColor];
        
    }
    
    return cell;
}
*/


#pragma mark - OffScreen rendering

- (id)offscreenRenderingPDChildrenCell:(ChildFilterChildShortCell *)cell withRow:(AXRow *)row {
    
    ChildFilterModel *model = row.model;
    [cell updateWithModel:model];
    
    return cell;
}


#pragma mark - table select

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.view endEditing:YES];
    
    //[[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    //[self setEditing:NO];
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    NSString *type = row.type;
    
    if ([type isEqualToString:PDChildrenCell]) {
        
        [self.navigationController popViewControllerAnimated:YES];
        if (self.filterDelegate) {
            [self.filterDelegate didSelectedModel:row.model];
        }
    }
    
}


- (void)refreshData {
    
    [self cleanForRefresh];
    
    
    __weak __typeof(self)weakSelf = self;
    
    [[NetworkManager sharedInstance] getChildrenListWithCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            if([[response valueForKeyPath:@"result.complete"] intValue] == 1)
            {
                NSArray *activeChildrens = [response valueForKeyPath:@"result_active.child_list"];
                NSArray *archiveChildrens = [response valueForKeyPath:@"result_archive.child_list"];
                [strongSelf updateTableWithActive:activeChildrens archive:archiveChildrens];
            }
            else
            {
                
                //[self deleteLoadingSection];
                //[self updateTableWithRefreshMessage:[response valueForKeyPath:@"result.msg"]];
                Alert(@"Error",[response valueForKeyPath:@"result.msg"] );
                
            }
        }
        else if(error)
        {
            
            //[self deleteLoadingSection];
            //[self updateTableWithRefreshMessage:@"Error happened while operation"];
            Alert(@"Error", @"Error happened while operation");
        }
    }];
    
}


#pragma mark - Remote processing
- (void)updateTableWithActive:(NSArray *)activeChildrens archive:(NSArray *)archiveChildrens {
    
    
    //обрабатываем active
    NSMutableArray *childrensPrepared = [[NSMutableArray alloc] init];
    
    [childrensPrepared addObjectsFromArray: [self childrensFromArray:activeChildrens isArchive:NO]];
    if (self.showArchive) {
        [childrensPrepared addObjectsFromArray: [self childrensFromArray:archiveChildrens isArchive:YES]];
    }
    
    if (![childrensPrepared count]) {
        [self cleanForRefreshWithAnimation];
        [self insertMessageCell]; //Empty default
        return;
    }
    
    
    //Заполняем список комнат
    self.roomsAndChildDictionary = [NSMutableDictionary dictionary];
    for (ChildFilterModel *child in childrensPrepared) {
        [self addChild:child toRoom:child.roomName dictType:FilterDictTypeBase];
    }
    
     self.childrensArray = childrensPrepared;
    
    
    [self updateTableFiltered];
    
    
}


- (NSArray *)childrensFromArray:(NSArray *)array isArchive:(BOOL)isArchive {
    
    NSMutableArray *childrensPrepared = [[NSMutableArray alloc] init];
    
    if ([array isKindOfClass:[NSArray class]]) {
        for (NSDictionary *child in array) {
            ChildFilterModel *model = [[ChildFilterModel alloc] initWithDict:child];
            if (model) {
                model.isArchive = isArchive;
                
                //Ставим галочки
                if (self.multipleSelect && self.selectedChildIds) {
                    for (NSNumber *chid in self.selectedChildIds) {
                        if ([model.chId intValue] == [chid intValue]) {
                            model.isChecked = YES;
                        }
                    }
                }
                
                //Исключаем запрещенных к выборке детей
                if (self.excludeChildIds) {
                    BOOL have = NO;
                    for (NSNumber *chid in self.excludeChildIds) {
                        if ([model.chId intValue] == [chid intValue]) {
                            have = YES;
                        }
                    }
                    if (!have) {
                        [childrensPrepared addObject:model];
                    }
                }
                else if (self.includeChildIds) {
                    BOOL have = NO;
                    for (NSNumber *chid in self.includeChildIds) {
                        if ([model.chId intValue] == [chid intValue]) {
                            have = YES;
                        }
                    }
                    if (have) {
                        [childrensPrepared addObject:model];
                    }
                }
                else {
                    [childrensPrepared addObject:model];
                }
                
            }
        }
    }
    
    return (NSArray *)childrensPrepared;
}


- (void)cleanForRefreshWithAnimation {
    
    //Очищаем mainSection
    NSIndexSet *indexSet = [self.sections getIndexSetForSectionsWithType:PDMainSection];
    [self.sections removeSectionsByIndexSet:indexSet];
    
    
    [self.tableView beginUpdates];
    
    if ([indexSet count] > 0) {
        [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.tableView endUpdates];
    
    //[self.tableView reloadData];
}


- (void)updateTableFiltered {
    
    if (self.showArchive) {
        [self updateTableFilteredWithArchived];
    }
    else {
        [self updateTableFilteredOnlyActive];
    }
}



- (void)updateTableFilteredWithArchived {
    
    [self sortedRoomAndChildsWithFilter:self.filterString];
    
    
    NSMutableArray *sections = [NSMutableArray array];
    AXSection *sectionSpacer = [[AXSection alloc] initWithType:PDMainSection];
    [sectionSpacer addRow:[self.sections genRowWithType:PDSpacerCell]];
    [sections addObject:sectionSpacer];
    
    if (self.isSelectAll && ![self isSearch]) {
        AXSection *selectAllSection = [[AXSection alloc] initWithType:PDMainSection];
        [selectAllSection addRow:[self.sections genRowWithType:PDSelectAllCell]];
        [selectAllSection addRow:[self.sections genRowWithType:PDSpacerCell]];
        [sections addObject:selectAllSection];
    }
    
    
    NSDictionary *activeChildsDict = [self filtredDictChildIsArchived:NO];
    NSArray *activeSections = [self sectionsForChildsDict:activeChildsDict isHide:NO statusName:@"active" isEmpty:nil];
    [sections addObjectsFromArray:activeSections];
    
    NSDictionary *archivedChildsDict = [self filtredDictChildIsArchived:YES];
    NSArray *archivedSections = [self sectionsForChildsDict:archivedChildsDict isHide:NO statusName:@"archive" isEmpty:nil];
    [sections addObjectsFromArray:archivedSections];
    
    
    //NSIndexSet *removeIndexSet = [self.sections getIndexSetForSectionsWithType:PDMainSection];
    NSIndexSet *removeIndexSet = [self.sections getIndexSetForSectionsWithTag:@"main"];
    [self.sections removeSectionsByIndexSet:removeIndexSet];
    if ([removeIndexSet count] > 0) {
        //[self.tableView deleteSections:removeIndexSet withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView reloadData];
    }
    
    
    
    NSIndexSet *indexSet = [self.sections insertSections:sections afterSectionType:PDTopSection];
    
    
    if ([indexSet count] > 0) {
        //[self.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView reloadData];
    }
    
    
    
}


- (void)updateTableFilteredOnlyActive {
    
    
    [self sortedRoomAndChildsWithFilter:self.filterString];
    
    NSArray *sortedRoomNames = [[self.roomsAndChildFilteredDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
    
    
    if (![sortedRoomNames count]) {
        [self cleanForRefresh];
        [self insertMessageCell]; //Empty default
        return;
    }
    
    
    NSMutableArray *sections = [NSMutableArray array];
    
    AXSection *sectionSpacer = [[AXSection alloc] initWithType:PDMainSection];
    [sectionSpacer addRow:[self.sections genRowWithType:PDSpacerCell]];
    [sections addObject:sectionSpacer];
    
    if (self.isSelectAll && ![self isSearch]) {
        AXSection *selectAllSection = [[AXSection alloc] initWithType:PDMainSection];
        [selectAllSection addRow:[self.sections genRowWithType:PDSelectAllCell]];
        [selectAllSection addRow:[self.sections genRowWithType:PDSpacerCell]];
        [sections addObject:selectAllSection];
    }
    
    for (NSString *roomName in sortedRoomNames) {
        
        AXSection *section = [[AXSection alloc] initWithType:PDMainSection];
        section.isHide = NO;
        
        
        AXHeader *header = [self.sections genHeaderWithType:PDRoomHeader];
        header.title = roomName;
        //[header.userInfo setObject:roomName forKey:@"roomName"];
        //[header.userInfo setObject:statusName forKey:@"photo"];
        section.header = header;
        
        [section addRow:[self.sections genRowWithType:PDSpacerCell]];
        
        if (self.isSelectAll && ![self isSearch]) {
            AXRow *selectAllInRoomRow = [self.sections genRowWithType:PDSelectInRoomCell];
            selectAllInRoomRow.text = roomName;
            [section addRow:selectAllInRoomRow];
            [section addRow:[self.sections genRowWithType:PDSpacerCell]];
        }
        /*
        NSArray *childs = [self.roomsAndChildFilteredDictionary objectForKey:roomName];
        for (GalleryPhotoChildModel *child in childs) {
            AXRow *row = [self.sections genRowWithType:PDChildrenCell];
            row.model = child;
            [section addRow:row];
            
            //header save child
            header.object = child;
        }
        */
        if ([roomName isEqualToString:[sortedRoomNames lastObject]]) {
            [section addRow:[self.sections genRowWithType:PDSpacerCell]];
        }
        else {
            [section addRow:[self.sections genRowWithType:PDSpacer45Cell]];
        }
        
        
        [sections addObject:section];
        
    }
    
    //[UIView setAnimationsEnabled:NO];
    //[CATransaction setDisableActions:YES];
    //[self.tableView beginUpdates];
    //[self cleanForRefreshWithAnimation];
    
    //[self.sections insertSections:sections afterSectionType:PDTopSection];
    
    
    NSIndexSet *removeIndexSet = [self.sections getIndexSetForSectionsWithType:PDMainSection];
    [self.sections removeSectionsByIndexSet:removeIndexSet];
    if ([removeIndexSet count] > 0) {
        //[self.tableView deleteSections:removeIndexSet withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView reloadData];
    }
    
    
    
    NSIndexSet *indexSet = [self.sections insertSections:sections afterSectionType:PDTopSection];
    
    
    if ([indexSet count] > 0) {
        //[self.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView reloadData];
    }
    //    [self.tableView endUpdates];
    //    [UIView setAnimationsEnabled:YES];
    //[CATransaction setDisableActions:NO];
    
    
    //[self.tableView reloadData];
    
    
}


- (NSMutableDictionary *)filtredDictChildIsArchived:(BOOL)isArchived {
    
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    for (NSString *roomName in [self.roomsAndChildFilteredDictionary allKeys]) {
        NSArray *childs = [self.roomsAndChildFilteredDictionary objectForKey:roomName];
        for (ChildFilterModel *child in childs) {
            if (child.isArchive == isArchived) {
                NSMutableArray *childsInRoom = [dict objectForKey:roomName] ? : [[NSMutableArray alloc] init];
                [childsInRoom addObject:child];
                [dict setObject:childsInRoom forKey:roomName];
            }
        }
    }
    
    return dict;
    
}


- (void)addChild:(id)child toRoom:(NSString *)roomName dictType:(FilterDictType)dictType{
    
    if (!roomName) {
        roomName = @"N/A";
    }
    
    NSMutableDictionary *dict;
    
    if (dictType == FilterDictTypeBase) {
        dict = self.roomsAndChildDictionary;
    }
    else {
        dict = self.roomsAndChildFilteredDictionary;
    }
    
    
    NSMutableArray *childs = [dict objectForKey:roomName];
    if ([childs isKindOfClass:[NSMutableArray class]]) {
        [childs addObject:child];
    }
    else {
        childs = [[NSMutableArray alloc] init];
        [childs addObject:child];
        [dict setObject:childs forKey:roomName];
    }
    
    
}


- (void)sortedRoomAndChildsWithFilter:(NSString *)filterString {
    
    //Создаем фильтрованный список
    self.roomsAndChildFilteredDictionary = [[NSMutableDictionary alloc] init];
    
    for (NSString *roomName in self.roomsAndChildDictionary) {
        
        NSArray *childs = [self.roomsAndChildDictionary objectForKey:roomName];
        
        for (ChildFilterModel *child in childs) {
            if ([child nameContain:filterString]) {
                [self addChild:child toRoom:child.roomName dictType:FilterDictTypeFiltered];
            }
        }
    }
    
    //Создаем сортированный список детей
    NSMutableDictionary *filteredDictionary = [[NSMutableDictionary alloc] init];
    for (NSString *roomName in [self.roomsAndChildFilteredDictionary allKeys]) {
        
        NSArray *childs = [self.roomsAndChildFilteredDictionary objectForKey:roomName];
        
        
        NSSortDescriptor *lastNameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName"
                                                                               ascending:YES
                                                                                selector:@selector(localizedCaseInsensitiveCompare:)];
        
        NSSortDescriptor *firstNameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firstName"
                                                                                ascending:YES
                                                                                 selector:@selector(localizedCaseInsensitiveCompare:)];
        
        NSArray *sortDescriptors = [NSArray arrayWithObjects:firstNameSortDescriptor, lastNameSortDescriptor, nil];
        
        [filteredDictionary setObject:[childs sortedArrayUsingDescriptors:sortDescriptors] forKey:roomName];
        
    }
    
    self.roomsAndChildFilteredDictionary = filteredDictionary;
    
}



- (NSArray *)sectionsForChildsDict:(NSDictionary *)dict isHide:(BOOL)isHide statusName:(NSString *)statusName isEmpty:(BOOL *)isEmpty {
    
    
    NSArray *sortedRoomNames = [[dict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSMutableArray *sections = [NSMutableArray array];

    
    AXSection *section = [[AXSection alloc] initWithType:[statusName isEqualToString:@"active"] ? PDActiveChildrenSection : PDArchivedChildrenSection];
    section.isHide = isHide;
    [section.tags addObject:statusName];
    [section.tags addObject:@"main"];
    [section.tags addObject:@"top"];
    
    AXHeader *header = [self.sections genHeaderWithType:[statusName isEqualToString:@"active"] ? PDActiveHeader : PDArchivedHeader];
    [header.userInfo setObject:statusName forKey:@"title"];
    [header.userInfo setObject:statusName forKey:@"tag"];
    header.object = dict;
    section.header = header;
    
    
    
    if([dict count] > 0) {
        //spacer + имена детей
        AXRow *spacer = [self.sections genRowWithType:PDSpacerCell];
        [section addRow:spacer];
        
        [sections addObject:section];
        /*
        if (!isHide) {
            
            for (NSString *roomName in sortedRoomNames) {
                
                AXSection *section = [[AXSection alloc] initWithType:PDMainSection];
                [section.tags addObject:statusName];
                section.isHide = NO;
                
                
                AXHeader *header = [self.sections genHeaderWithType:PDRoomHeader];
                header.title = roomName;
                //[header.userInfo setObject:roomName forKey:@"roomName"];
                //[header.userInfo setObject:statusName forKey:@"photo"];
                section.header = header;
                
                NSArray *childs = [dict objectForKey:roomName];
                for (ChildFilterModel *child in childs) {
                    AXRow *row = [self.sections genRowWithType:PDChildrenCell];
                    row.model = child;
                    [section addRow:row];
                    
                    //header save child
                    header.object = child;
                }
                
                if ([roomName isEqualToString:[sortedRoomNames lastObject]]) {
                    [section addRow:[self.sections genRowWithType:PDSpacerCell]];
                }
                else {
                    [section addRow:[self.sections genRowWithType:PDSpacer45Cell]];
                }
                
                
                [sections addObject:section];
                
            }
            

            //[sections addObjectsFromArray:childrensSections];
        }
        */
        
        [sections addObjectsFromArray:[self roomAndChildsSection:dict isHide:NO statusName:statusName]];
        
        
        //*isEmpty = NO;
        
    } else {
        //empty + spacer?
        
        AXRow *empty = [self.sections genRowWithType:PDMessageCell];
        [section addRow:empty];
        
        //AXRow *spacer = [self.sections genRowWithType:PDSpacerCell];
        //[section addRow:spacer];
        
        [sections addObject:section];
    }
    
    
    
    if (!isHide) {
        //Если открыта - внизу отдельной секцией отбивка перед следующей секцией
        AXSection *spacerSection = [self sectionsForSpacerWithTag:@[statusName]];
        [sections addObject:spacerSection];
    }

    return sections;
    
    
}


- (NSArray *)roomAndChildsSection:(NSDictionary *)dict isHide:(BOOL)isHide statusName:(NSString *)statusName{
    
    NSMutableArray *sections = [NSMutableArray array];
    
    
    NSArray *sortedRoomNames = [[dict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    
    if (!isHide) {
        
        for (NSString *roomName in sortedRoomNames) {
            
            AXSection *section = [[AXSection alloc] initWithType:PDMainSection];
            [section.tags addObject:statusName];
            [section.tags addObject:@"main"];
            section.isHide = NO;
            
            
            AXHeader *header = [self.sections genHeaderWithType:PDRoomHeader];
            header.title = roomName;
            //[header.userInfo setObject:roomName forKey:@"roomName"];
            //[header.userInfo setObject:statusName forKey:@"photo"];
            section.header = header;
            
            [section addRow:[self.sections genRowWithType:PDSpacerCell]];
            
            NSArray *childs = [dict objectForKey:roomName];
            for (ChildFilterModel *child in childs) {
                AXRow *row = [self.sections genRowWithType:PDChildrenCell];
                row.model = child;
                [section addRow:row];
                
                //header save child
                header.object = child;
            }
            
            if ([roomName isEqualToString:[sortedRoomNames lastObject]]) {
                [section addRow:[self.sections genRowWithType:PDSpacerCell]];
            }
            else {
                [section addRow:[self.sections genRowWithType:PDSpacer45Cell]];
            }
            
            
            [sections addObject:section];
            
        }
        
        
        //[sections addObjectsFromArray:childrensSections];
    }

    return sections;
}


-(AXSection *)sectionsForSpacerWithTag:(NSArray *)tags {
    
    AXSection *spacerSection = [[AXSection alloc] initWithType:PDSpacerSection];
    [spacerSection.tags addObjectsFromArray:tags];
    [spacerSection.tags addObject:@"main"];
    AXRow *spacerRow = [self.sections genRowWithType:PDSpacerCell];
    [spacerSection addRow:spacerRow];
    
    return spacerSection;
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == FILTER_TEXT_FIELD_TAG) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        self.filterString = newString;
        
        
        NSIndexPath *indexPath = [self indexPathForView:textField];
        [self updateTableFiltered];
        
        FilterCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [cell.filterTextField becomeFirstResponder];
        //[textField becomeFirstResponder];
        return NO;
    }
    
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    
    if (textField.tag == FILTER_TEXT_FIELD_TAG) {
        [textField resignFirstResponder];
    }
    
    
    return YES;
}


#pragma mark - Actions

- (void)headerTap:(UIGestureRecognizer *)gestureRecognizer {
    
    NSLog (@"tapped");
    
    //UITableViewCell *viewCell = gestureRecognizer.view;
    //UIView *view = viewCell.contentView;
    
    
    [self.view endEditing:YES];
    
    NSLog (@"next tap");
    
    UIView *view = gestureRecognizer.view;
    
    NSUInteger index = [self.sections getSectionIndexForView:view];
    
    if (index == NSUIntegerMax) {
        return;
    }
    
    NSIndexSet *reloadIndexSet = [NSIndexSet indexSetWithIndex:index];
    
    AXHeader *header = [self.sections headerAtSectionIndex:index];
    
    AXSection *section = [self.sections sectionAtIndex:index];
    section.isHide = !section.isHide;
    [self updateHeaderArrowImage:header.arrowImageView withState:section.isHide];
    
    NSIndexSet *removeIndexSet = [[NSIndexSet alloc] init];
    NSIndexSet *insertIndexSet = [[NSIndexSet alloc] init];
    
    NSString *tag = [header.userInfo objectForKey:@"tag"];
    
    if ([header.type isEqualToString:PDActiveHeader]) {
        if (section.isHide) {
            
            removeIndexSet = [self.sections getIndexSetForSectionsWithTag:tag andExcludeTag:@"top"];
            [self.sections removeSectionsByIndexSet:removeIndexSet];
            
        } else {
            NSMutableArray *sections = [[NSMutableArray alloc] init];
            
            //NSArray *childrensSections = [self sectionsForChildsDict:[self filtredDictChildIsArchived:NO] isHide:NO statusName:@"active" isEmpty:nil];
            NSArray *childrensSections = [self roomAndChildsSection:[self filtredDictChildIsArchived:NO] isHide:NO statusName:@"active"];
            AXSection *spacerSection = [self sectionsForSpacerWithTag:@[tag]];
            
            [sections addObjectsFromArray:childrensSections];
            [sections addObject:spacerSection];
            
            insertIndexSet = [self.sections insertSections:sections afterSectionIndex:section.sectionIndex];
        }
    }
    else if ([header.type isEqualToString:PDArchivedHeader]) {
        if (section.isHide) {
            
            removeIndexSet = [self.sections getIndexSetForSectionsWithTag:tag andExcludeTag:@"top"];
            [self.sections removeSectionsByIndexSet:removeIndexSet];
            
        } else {
            NSMutableArray *sections = [[NSMutableArray alloc] init];
            
            //NSArray *childrensSections = [self sectionsForChildsDict:[self filtredDictChildIsArchived:NO] isHide:NO statusName:@"active" isEmpty:nil];
            NSArray *childrensSections = [self roomAndChildsSection:[self filtredDictChildIsArchived:YES] isHide:NO statusName:@"archive"];
            AXSection *spacerSection = [self sectionsForSpacerWithTag:@[tag]];
            
            [sections addObjectsFromArray:childrensSections];
            [sections addObject:spacerSection];
            
            insertIndexSet = [self.sections insertSections:sections afterSectionIndex:section.sectionIndex];
        }
    }
    
    [self.tableView beginUpdates];
    
    //сама кликнутая секция
    [self.tableView reloadSections:reloadIndexSet withRowAnimation:UITableViewRowAnimationFade];
    
    if ([insertIndexSet count] > 0) {
        [self.tableView insertSections:insertIndexSet withRowAnimation:UITableViewRowAnimationFade];
    }
    
    if ([removeIndexSet count] > 0) {
        [self.tableView deleteSections:removeIndexSet withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.tableView endUpdates];
    
    //
}



- (IBAction)checkAction:(id)sender {
    
    AXRow *row = [self rowForView:sender];
    
    if (!row) {
        return;
    }
    
    ChildFilterModel *model = row.model;
    model.isChecked = !model.isChecked;
    
    [self updateRow:row];
    
    if (self.isSelectAll && ![self isSearch]) {
        [self updateCheckAllInRoomName:model.roomName];
        [self updateCheckAll];
    }
    
}

- (IBAction)checkAllChildrenAction:(id)sender {
    
    AXRow *checkAllRow = [self rowForView:sender];
    
    if (!checkAllRow) {
        return;
    }
    
    BOOL isChecked = ![self isAllChildrenChecked];
    /*
    SelectRoomChildrensCell *cell = (SelectRoomChildrensCell *)checkAllRow.cell;
    
    if ([self isCellOnScreen:cell]) {
        [cell check:isChecked];
    }
    */
    //сами дети
    NSArray *childrenIndexPaths = [self.sections getIndexPathsForRowType:PDChildrenCell];
    NSArray *childrenRows = [self.sections rowsAtIndexPaths:childrenIndexPaths];
    
    
    for (AXRow *row in childrenRows) {
        ChildFilterModel *model = row.model;
        if (model.isChecked != isChecked) {
            model.isChecked = isChecked;
            [self updateCheckForChildren:row.model];
        }
    }
        
    
    //флажки в комнатах
    NSArray *selectInRoowIndexPaths = [self.sections getIndexPathsForRowType:PDSelectInRoomCell];
    NSArray *selectInRoomRows = [self.sections rowsAtIndexPaths:selectInRoowIndexPaths];
    
    for (AXRow *row in selectInRoomRows) {
        [self updateCheckAllInRoomName:row.text];
    }
    
    
    [self updateCheckAll];

    
    
}


- (IBAction)checkAllChildrenInRoomAction:(id)sender {
    
    
    AXRow *roomRow = [self rowForView:sender];
    
    if (!roomRow) {
        return;
    }
    
    BOOL isChecked = ![self isAllChildrenCheckedInRoomName:roomRow.text];
    /*
    SelectRoomChildrensCell *cell = (SelectRoomChildrensCell *)roomRow.cell;
    
    if (![self isCellOnScreen:cell]) {
        return;
    }
    
    [cell check:isChecked];
    */
    
    
    //Маркируем детей
    NSArray *childrensInRoomIndexPaths = [self.sections getIndexPathsForRowType:PDChildrenCell atIndex:AXRowAll withCondition:^BOOL(AXSection *section, AXRow *row) {
        if (section.sectionIndex == roomRow.indexPath.section) {
            return YES;
        }
        return NO;
    }];
    
    
    NSArray *childrenRows = [self.sections rowsAtIndexPaths:childrensInRoomIndexPaths];
    
    if (![childrenRows count]) {
        return;
    }
    
    
    for (AXRow *row in childrenRows) {
        ChildFilterModel *model = row.model;
        if (model.isChecked != isChecked) {
            model.isChecked = isChecked;
            [self updateCheckForChildren:row.model];
        }
    }

    [self updateCheckAll];
}



-(IBAction)saveAction:(id)sender {
    
    if (self.filterMultipleDelegate) {
        
        NSMutableArray *childrens = [NSMutableArray array];
        for (ChildFilterModel *model in self.childrensArray) {
            if (model.isChecked) {
                [childrens addObject:model];
            }
        }
        
        [self.filterMultipleDelegate didSelectedChildModels:childrens];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}


- (BOOL)isSearch {
    
    if (self.filterString && [self.filterString length]) {
        return YES;
    }
    return NO;
    
    
}

//Все дети в комнате
- (BOOL)isAllChildrenCheckedInRoomName:(NSString *)roomName {
    
    
    NSArray *childrensInRoomIndexPaths = [self.sections getIndexPathsForRowType:PDChildrenCell atIndex:AXRowAll withCondition:^BOOL(AXSection *section, AXRow *row) {
        /*
        if (section.sectionIndex == childrenRow.indexPath.section) {
            return YES;
        }
        */
        
        ChildFilterModel *model = row.model;
        if ([model.roomName isEqualToString:roomName]) {
            return YES;
        }
        
        return NO;
    }];
    
    
    NSArray *childrenRows = [self.sections rowsAtIndexPaths:childrensInRoomIndexPaths];
    
    if (![childrenRows count]) {
        return NO;
    }
    
    int checked = 0;
    
    for (AXRow *row in childrenRows) {
        ChildFilterModel *model = row.model;
        if (model.isChecked) {
            checked++;
        }
    }
    
    return [childrenRows count] == checked;

}


//Вообще все дети
- (BOOL)isAllChildrenChecked {
    
    
    NSArray *childrenIndexPath = [self.sections getIndexPathsForRowType:PDChildrenCell];
    NSArray *childrenRows = [self.sections rowsAtIndexPaths:childrenIndexPath];
    
    if (![childrenRows count]) {
        return NO;
    }
    
    int checked = 0;
    
    for (AXRow *row in childrenRows) {
        ChildFilterModel *model = row.model;
        if (model.isChecked) {
            checked++;
        }
    }
    
    return [childrenRows count] == checked;
    
}



- (void)updateCheckAllInRoomName:(NSString *)roomName {
    
    
    //Получаем select childs in room
    NSArray *selectRoomChildrensIndexPaths = [self.sections getIndexPathsForRowType:PDSelectInRoomCell atIndex:AXRowAll withCondition:^BOOL(AXSection *section, AXRow *row) {
        if ([row.text isEqualToString:roomName]) {
            return YES;
        }
        return NO;
    }];
    
    
    if (![selectRoomChildrensIndexPaths count]) {
        return;
    }
    
    AXRow *selectRoomChildrensRow = [[self.sections rowsAtIndexPaths:selectRoomChildrensIndexPaths] firstObject];
    selectRoomChildrensRow.isChecked = [self isAllChildrenCheckedInRoomName:roomName];
    
    
    /*
    SelectRoomChildrensCell *cell = (SelectRoomChildrensCell *)selectRoomChildrensRow.cell;
    
    if (![self isCellOnScreen:cell]) {
        return;
    }
    
    [cell check:[self isAllChildrenCheckedInRoomName:roomName]];
    */
    
}


- (void)updateCheckAll {
    
    
    AXRow *row = [self.sections rowWithRowType:PDSelectAllCell];
    /*
    SelectRoomChildrensCell *cell = (SelectRoomChildrensCell *)row.cell;
    
    if (![self isCellOnScreen:cell]) {
        return;
    }
    
    [cell check:[self isAllChildrenChecked]];
    */
    
}



- (void)updateCheckForChildren:(ChildFilterModel *)children {
    
    
    AXRow *childrenRow;
    NSArray *childrenIndexPaths = [self.sections getIndexPathsForRowWithModel:children];
    if (childrenIndexPaths && [childrenIndexPaths count]) {
        childrenRow = [self.sections rowAtIndexPath:[childrenIndexPaths firstObject]];
    }
    
    if (!childrenRow) {
        return;
    }
    
    ChildFilterChildShortCell *cell = (ChildFilterChildShortCell *)childrenRow.cell;
    
    if ([self isCellOnScreen:cell]) {
        [cell check:children.isChecked];
    }
    
    
}


@end
