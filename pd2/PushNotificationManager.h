//
//  PushNotificationManager.h
//  pd2
//
//  Created by Andrey on 04/10/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Singleton.h"

@class PushNotificationModel;

@interface PushNotificationManager : Singleton

@property (nonatomic, strong) PushNotificationModel *currentPush;

- (void)addPush:(NSDictionary *)dict andBackground:(BOOL)isBackground;
- (void)jump;
- (void)clearQueue;


@end
