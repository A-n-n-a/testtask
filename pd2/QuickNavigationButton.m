//
//  QuickNavigationButton.m
//  pd2
//
//  Created by Andrey on 01.03.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

#import "QuickNavigationButton.h"

@implementation QuickNavigationButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    UIImage *image=[self backgroundImageForState:UIControlStateNormal];
    image=[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self setBackgroundImage:image forState:UIControlStateNormal];
    self.tintColor=[[MyColorTheme sharedInstance]menuButtonsColor];
    self.backgroundColor = [[MyColorTheme sharedInstance]backgroundColor];
    //self.tintColor=[[MyColorTheme sharedInstance]backgroundColor];
    //self.backgroundColor = [[MyColorTheme sharedInstance]menuButtonsColor];
    
    
    self.layer.cornerRadius = 12.0f;
    self.layer.masksToBounds = YES;
}

@end
