//
//  NSString+RandomString.m
//  pd2
//
//  Created by i11 on 19/06/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "NSString+RandomString.h"

@implementation NSString (RandomString)



+ (NSString *)randomStringWithLength:(int)len {
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((unsigned int)[letters length])]];
    }
    
    return randomString;
}

@end
