//
//  ChildCell.h
//  pd2
//
//  Created by Admin on 06.02.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ManageChildModel.h"

@interface ChildCell : UITableViewCell

@property bool archived;

@property (nonatomic,retain)IBOutlet UIButton *editButton;
@property (nonatomic,retain)IBOutlet UIButton *archiveButton;
@property (nonatomic,retain)IBOutlet UIButton *deleteButton;


@property (weak, nonatomic) IBOutlet UIButton *showButton;



@property (nonatomic,retain)IBOutlet UIImageView *editImage;
@property (nonatomic,retain)IBOutlet UIImageView *deleteImage;
@property (weak, nonatomic) IBOutlet UIButton *childCopyButton;


@property (nonatomic,retain)IBOutlet UILabel *roomLabel;
@property (nonatomic,retain)IBOutlet UILabel *firstNameLabel;
@property (nonatomic,retain)IBOutlet UILabel *lastNameLabel;
@property (nonatomic,retain)IBOutlet UILabel *dateOfBirthLabel;
@property (nonatomic,retain)IBOutlet UILabel *siblingsLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *dobLabel;

@property (nonatomic,retain)IBOutlet UIImageView *photoImageView;

@property (nonatomic,retain) NSDictionary *infoDict;

@property (nonatomic,retain)NSMutableArray *siblings;


-(void)initWithModel:(ManageChildModel*) model;

@end
