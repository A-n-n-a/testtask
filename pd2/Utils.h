//
//  Utils.h
//  pd2
//
//  Created by User on 27.11.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "Singleton.h"
#import "MWPhotoBrowser.h"

typedef void(^imageCacheDownloadCompletionHandler)(UIImage* image);
id firstNotNil(id obj1, id obj2);

@interface Utils : Singleton

-(NSString*)parametersStringFromDictionary:(NSDictionary *)optionsdict;

- (void)getInfoAboutYoutubeVideoWithId:(NSString *)videoId completion:(void (^)(id response, NSError *error))completion;
- (void)downloadImageAtPath:(NSString*)path cache:(BOOL)cache completionHandler:(imageCacheDownloadCompletionHandler)completion;
- (void)downloadImageAtPath:(NSString*)path completionHandler:(imageCacheDownloadCompletionHandler)completion; //стандартный, используемый в 95% контоллеров
- (void)downloadImageAtRelativePath:(NSString*)path cache:(BOOL)cache completionHandler:(imageCacheDownloadCompletionHandler)completion;
- (void)downloadImageAtRelativePath:(NSString*)path completionHandler:(imageCacheDownloadCompletionHandler)completion; //стандартный, используемый в 95% контоллеров
- (NSString *)base64StringFromImage:(UIImage *)image;
- (NSString *)base64StringFromJPEGImage:(UIImage *)image withQuality:(CGFloat)quality;
- (UIImage *)imageScaledToAvatar:(UIImage*)image;
- (UIImage *)imageScaledToAvatarHiRes:(UIImage*)image;
- (UIImage *)imageScaledToDiaryPhotoThumb:(UIImage *)image;
- (UIImage *)imageScaledToDiaryPhoto:(UIImage *)image;
- (UIImage *)imageScaledToRoomImage:(UIImage*)image;
- (UIImage *)imageScaledToRoomImageHiRes:(UIImage*)image;
- (UIImage *)imageScaledToRegularVisitorImage:(UIImage*)image;
- (UIImage *)imageScaledToMedicineImage:(UIImage*)image;
- (UIImage *)imageScaledToGalleryPhoto:(UIImage *)image;
- (UIImage *)imageScaledToGalleryPhotoThumb:(UIImage *)image;


-(NSDateFormatter*)standartDateFormatter;
-(NSDateFormatter*)dotsDateFormatter;
-(void)getExifDataFromImage:(NSDictionary*) info completion:(void (^)(NSDictionary *exifInfo))completion;
-(NSString *) stringByStrippingHTML:(NSString*)str;
-(NSDateComponents*)getComponentsFromDate:(NSDate*)date;

-(void)showShortAlertWithText:(NSString*)text;
-(NSString *)handleNilString:(NSString*)string;
+(NSString*)ordinalNumberFormat:(NSInteger)num;

+ (id)safeRemoveEmoji:(id)object;

+ (BOOL)isFaceIdSupported;

@end
