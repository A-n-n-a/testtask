//
//  ConfirmButton.m
//  pd2
//
//  Created by User on 15.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "ConfirmButton.h"
#import "MyColorTheme.h"
#import "UIImage+Utils.h"
#import "UIImage+ImageByApplyingAlpha.h"
@implementation ConfirmButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

/*
-(void)awakeFromNib
{
    //NSLog(@"ConfirmButton.awakeFromNib");
    NSData *imageData=[NSData dataWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"greenButton" ofType:@"png"]];
    UIImage *image=[UIImage imageWithData:imageData scale:2.0];//imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIEdgeInsets insets = UIEdgeInsetsMake(15, 50, 15, 50);
    UIImage *stretchableImage = [image resizableImageWithCapInsets:insets];
    [self setBackgroundImage:stretchableImage forState:UIControlStateNormal];
    
    NSData *imageData2=[NSData dataWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"greenButton_pressed" ofType:@"png"]];
    UIImage *image2=[UIImage imageWithData:imageData2 scale:2.0];//imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *stretchableImage2 = [image2 resizableImageWithCapInsets:insets];
    [self setBackgroundImage:stretchableImage2 forState:UIControlStateHighlighted];
    
    //self.tintColor=[[MyColorTheme sharedInstance]buttonConfirmBackgroundColor];
    self.titleLabel.textColor=[[MyColorTheme sharedInstance]buttonConfirmTextColor];
}
*/

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialSetup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialSetup];
    }
    return self;
}
/*
-(void)awakeFromNib {
    
    [super initialSetup];
    [self initialSetup];
    
}
*/
-(void)initialSetup {
    [super initialSetup];
    [super makeMeAsConfirmButton];
    
}


@end
