//
//  ArchivedChildCell.m
//  pd2
//
//  Created by Eugene Fozekosh on 16.10.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ArchivedChildCell.h"

@implementation ArchivedChildCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _siblings=[[NSMutableArray alloc]init];
    }
    return self;
}


-(void)initWithModel:(ManageChildModel*) model
{
    self.firstNameLabel.text = model.childFirstName ? [model.childFirstName capitalizedString] : @"N/A";
    self.lastNameLabel.text = model.childLastName ? [model.childLastName capitalizedString] : @"N/A";
    self.dateOfBirthLabel.text = model.childBirthday ? model.childBirthday : @"N/A";
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"dd.MM.yyyy";
    NSDate *birthDate=[formatter dateFromString:model.childBirthday];
    if (!birthDate){
        birthDate = [NSDate date];
    }
    NSInteger month = [[[NSCalendar currentCalendar] components: NSCalendarUnitMonth
                                                       fromDate: birthDate
                                                         toDate: [NSDate date]
                                                        options: 0] month];
    
    NSString *monthString = month == 1 ? @"month" : @"months";
    
    self.ageLabel.text = [NSString stringWithFormat:@"%d %@", (int)month, monthString];
    
    NSString *imagePath=[NSString stringWithFormat:@"%@/%@",[NetworkManager sharedInstance].baseUrl,model.childImage];
    [[Utils sharedInstance]downloadImageAtPath:imagePath completionHandler:^(UIImage *image) {
        _photoImageView.image=image;
        _photoImageView.layer.borderWidth=0.5;
        _photoImageView.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        _photoImageView.layer.cornerRadius=38;
        _photoImageView.layer.masksToBounds=YES;
    }];
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
