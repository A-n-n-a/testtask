//
//  DailyNotesChildrenNoteCommentsNoteCell.h
//  pd2
//
//  Created by Andrey on 30.03.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@class DailyNotesChildrenNoteModel;

@interface DailyNotesChildrenNoteCommentsNoteCell : AXTableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *addedOnLabel;
@property (nonatomic, weak) IBOutlet UILabel *addedByLabel;
@property (nonatomic, weak) IBOutlet UILabel *descrLabel;

- (void)updateWithModel:(DailyNotesChildrenNoteModel *)model;


@end
