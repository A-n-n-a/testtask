//
//  EditBoxIcon.m
//  pd2
//
//  Created by User on 13.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "EditBoxIcon.h"

@implementation EditBoxIcon

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    self.image=[self.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.tintColor=[[MyColorTheme sharedInstance]backgroundColor];
}

@end
