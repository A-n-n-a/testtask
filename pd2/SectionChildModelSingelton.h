#import "Singleton.h"
#import "SectionChildModel.h"

@interface SectionChildModelSingelton : Singleton
@property (nonatomic, strong) SectionChildModel * childModel;

- (void) loadInfoWithSourceType:(SectionChildModelSourceType)sourceType fromDict:(NSDictionary *)childInfo;
@end
