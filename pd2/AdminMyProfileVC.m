//
//  AdminMyProfileVC.m
//  pd2
//
//  Created by Eugene Fozekosh on 19.11.15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import "AdminMyProfileVC.h"
#import "AXSections.h"
#import "InfoCell.h"
#import "AXHeader.h"
#import "MyProfileAccessInfoCell.h"
#import "MyProfileAssignedInfoCell.h"
#import "LogoContentCell.h"
#import "UIView+superCell.h"


#define CONTAINER_VIEW_TAG 250
#define SPOILER_IMAGE_VIEW_TAG 100
#define HEADER_ARROW_IMAGE_VIEW_TAG 150

@interface AdminMyProfileVC ()
{
    NSString *PDTopSection;
    NSString *PDAccessInfoSection;
    NSString *PDAdminLevelSection;
    NSString *PDAdminPhotoSection;
    
    NSString *PDInfoCell;
    NSString *PDInfo2Cell;
    NSString *PDInfo3Cell;
    NSString *PDInfo4Cell;
    
    NSString *PDSpoilerCell;
    NSString *PDSpoiler2Cell;
    NSString *PDSpoiler3Cell;
    
    NSString *PDAccessInfoCell;
    NSString *PDAdminLevelCell;
    NSString *PDAdminPhotoContentCell;
    NSString *PDButtonCell;
}

@property (strong, nonatomic) AXSections *sections;
@property (strong, nonatomic) InfoCell *autolayoutInfoCell;
@property (strong, nonatomic) InfoCell *infoCell;
@property (strong, nonatomic) UIImage *picImage;
@property (strong, nonatomic) NSMutableDictionary *adminInfo;
@property (nonatomic,retain) UIButton *addPhotoButton;
@property (strong, nonatomic) UIActionSheet * actionSheet_photo;

@end


@implementation AdminMyProfileVC



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initializeCellTypes];
    [self configureSections];
    [self setupSections];
    self.topTitle = @"My Profile";
    [self setupAdminInfo];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(freeTapAction:)];
    [self.view addGestureRecognizer:tap];
}


-(void) setupAdminInfo
{
    self.adminInfo = [NSMutableDictionary new];
    
    [self.adminInfo setObject:[DataManager sharedInstance].adminInfo[@"username"] forKey:@"username"];
    [self.adminInfo setObject:[DataManager sharedInstance].adminInfo[@"first_name"] forKey:@"first_name"];
    [self.adminInfo setObject:[DataManager sharedInstance].adminInfo[@"last_name"] forKey:@"last_name"];
    [self.adminInfo setObject:[DataManager sharedInstance].adminInfo[@"email"] forKey:@"email"];
    [self.adminInfo setObject:@"****" forKey:@"password"];
    [self.adminInfo setObject:@"" forKey:@"nPassword"];
    [self.adminInfo setObject:@"" forKey:@"vPassword"];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}



- (void)freeTapAction:(id)sender
{
    [self.view endEditing:YES];
}


- (void)initializeCellTypes
{
    PDInfoCell = @"PDInfoCell";
    PDInfo2Cell = @"PDInfo2Cell";
    PDInfo3Cell = @"PDInfo3Cell";
    PDInfo4Cell = @"PDInfo4Cell";
    
    PDSpoilerCell = @"PDSpoilerCell";
    PDSpoiler2Cell = @"PDSpoiler2Cell";
    PDSpoiler3Cell = @"PDSpoiler3Cell";
    
    PDTopSection = @"PDTopSection";
    PDAccessInfoSection = @"PDAccessInfoSection";
    PDAdminLevelSection = @"PDAdminLevelSection";
    PDAdminPhotoSection = @"PDAdminPhotoSection";
    
    PDAccessInfoCell = @"PDAccessInfoCell";
    PDAdminLevelCell = @"PDAdminLevelCell";
    PDAdminPhotoContentCell = @"PDAdminPhotoContentCell";
    PDButtonCell = @"PDButtonCell";
}


- (void)configureSections
{
    self.sections = [[AXSections alloc] init];
    [self.sections rememberType:PDInfoCell withHeight:70.f identifier:@"infoCell"];
    [self.sections rememberType:PDInfo2Cell withHeight:89.f identifier:@"info2Cell"];
    [self.sections rememberType:PDInfo3Cell withHeight:76.f identifier:@"info3Cell"];
    [self.sections rememberType:PDInfo4Cell withHeight:194.f identifier:@"info4Cell"];
    
    [self.sections rememberType:PDSpoilerCell withHeight:57.f identifier:@"spoilerCell"];
    [self.sections rememberType:PDSpoiler2Cell withHeight:57.f identifier:@"spoiler2Cell"];
    [self.sections rememberType:PDSpoiler3Cell withHeight:57.f identifier:@"spoiler3Cell"];
   
    [self.sections rememberType:PDAccessInfoCell withHeight:503.f identifier:@"accessInfoCell"];
    [self.sections rememberType:PDAdminLevelCell withHeight:210.f identifier:@"adminLevelInfoCell"];
    [self.sections rememberType:PDAdminPhotoContentCell withHeight:160.f identifier:@"adminPhotoContent"];
    [self.sections rememberType:PDButtonCell withHeight:90.f identifier:@"buttonCell"];
    
}


-(void) setupSections
{
    
    //Top Section
    AXSection *topSection = [[AXSection alloc] initWithType:PDTopSection];
    [topSection addRow:[self.sections genRowWithType:PDInfoCell]];
    [self.sections addSection:topSection];
    
    //Access Section
    AXSection *accessInfoSection = [[AXSection alloc] initWithType:PDAccessInfoSection];
    AXHeader *accessHeader = [self.sections genHeaderWithType:PDSpoilerCell];
    [accessInfoSection addHeader:accessHeader];
    [accessInfoSection addRow:[self.sections genRowWithType:PDInfo2Cell]];
    [accessInfoSection addRow:[self.sections genRowWithType:PDAccessInfoCell]];
    [self.sections addSection:accessInfoSection];
    

    //Assign Section
    AXSection *assignedInfoSection = [[AXSection alloc] initWithType:PDAdminLevelSection];
    AXHeader *assignedHeader = [self.sections genHeaderWithType:PDSpoiler2Cell];
    [assignedInfoSection addHeader:assignedHeader];
    [assignedInfoSection addRow:[self.sections genRowWithType:PDInfo3Cell]];
    [assignedInfoSection addRow:[self.sections genRowWithType:PDAdminLevelCell]];
    [self.sections addSection:assignedInfoSection];

    //Photo Section
    AXSection *photo = [[AXSection alloc] initWithType:PDAdminPhotoSection];
    AXHeader *buttonSettingsHeader = [self.sections genHeaderWithType:PDSpoiler3Cell];
    [photo addHeader:buttonSettingsHeader];
//    [photo addRow:[self.sections genRowWithType:PDInfo4Cell]];
    [photo addRow:[self.sections genRowWithType:PDAdminPhotoContentCell]];
    [photo addRow:[self.sections genRowWithType:PDButtonCell]];
    [self.sections addSection:photo];
    
    
    [self.tableView reloadData];
}


#pragma mark - TableView Header

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [self.sections heightForHeaderInSection:section];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    AXHeader *header = [self.sections headerAtSectionIndex:section];
    
    if (header) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:header.identifier];
        UIView *view = cell.contentView;
        
        //бордеры
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        [self addBorder:PDBorderBottom toView:containerView forToolbar:NO];
        
        //Обязательное удаление gestureRecognizers, иначе при longPress crash
        while (cell.contentView.gestureRecognizers.count) {
            [cell.contentView removeGestureRecognizer:[cell.contentView.gestureRecognizers objectAtIndex:0]];
        }
        
        
        view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        header.view = view;
    }
    return header.view;
}


#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sections countOfSections];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sections countOfRowsInSection:section];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    if  ([row.type isEqualToString:PDInfoCell]) {
        self.infoCell = (InfoCell*)[self configureInfoCellForTableView:tableView indexPath:indexPath];
        CGFloat height = self.infoCell.height;
        [self.infoCell setHeight:height];
        return height;
    }
    return row.height;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    NSString *rowType = row.type;
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    if ([rowType isEqualToString:PDInfoCell]) {
        cell = [self configureInfoCellForTableView:tableView indexPath:indexPath];
        
    } else if ([rowType isEqualToString:PDInfo2Cell]) {
        cell = [self configureInfo2CellForTableView:tableView indexPath:indexPath];
        
    } else if ([rowType isEqualToString:PDInfo3Cell]) {
        cell = [self configureInfo3CellForTableView:tableView indexPath:indexPath];
        
    }else if ([rowType isEqualToString:PDInfo4Cell]) {
        cell = [self configureInfo4CellForTableView:tableView indexPath:indexPath];
        
    }else if ([rowType isEqualToString:PDSpoilerCell]) {
        cell = [self configureSpoilerCellForTableView:tableView indexPath:indexPath];
        
    }else if ([rowType isEqualToString:PDSpoiler2Cell]) {
        cell = [self configureSpoiler2CellForTableView:tableView indexPath:indexPath];
        
    }else if ([rowType isEqualToString:PDSpoiler3Cell]) {
        cell = [self configureSpoiler3CellForTableView:tableView indexPath:indexPath];
        
    }else if ([rowType isEqualToString:PDAccessInfoCell]) {
        cell = [self configureAccessInfoCellForTableView:tableView indexPath:indexPath];
        
    }else if ([rowType isEqualToString:PDAdminLevelCell]) {
        cell = [self configureAdminLevelCellForTableView:tableView indexPath:indexPath];
        
    }else if ([rowType isEqualToString:PDAdminPhotoContentCell]) {
        cell = [self configureLogoContentCellForTableView:tableView indexPath:indexPath];
        
    }else if ([rowType isEqualToString:PDButtonCell]) {
        cell = [self configureButtonCellForTableView:tableView indexPath:indexPath];
        
    }
    
    row.cell = cell;
    
    return cell;
}


- (UITableViewCell*) configureButtonCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"buttonCell"];
    
    return cell;
}



- (UITableViewCell*) configureLogoContentCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    LogoContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"adminPhotoContent"];
    NSString *imagePath=[NSString stringWithFormat:@"%@/%@",[NetworkManager sharedInstance].baseUrl, [DataManager sharedInstance].adminInfo[@"image"]];
   
    self.addPhotoButton = cell.logoButton;

    [[Utils sharedInstance]downloadImageAtPath:imagePath completionHandler:^(UIImage *image) {
        self.picImage = image;
        if (image) {
            cell.bottomLabel.text = @"Upload Photo";
        }
        [cell.logoButton setBackgroundImage:image forState:UIControlStateNormal];
    }];
    cell.logoButton.layer.borderWidth=0.5;
    cell.logoButton.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    cell.logoButton.layer.cornerRadius=38;
    cell.logoButton.layer.masksToBounds=YES;
    
    return cell;
}


- (UITableViewCell*) configureAccessInfoCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    MyProfileAccessInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"accessInfoCell"];
    
    
    cell.usernameTF.text =  self.adminInfo[@"username"];
    cell.firstNameTF.text = self.adminInfo[@"first_name"];
    cell.lastNameTF.text = self.adminInfo[@"last_name"];
    cell.emailTF.text = self.adminInfo[@"email"];
    cell.passwordTF.text = self.adminInfo[@"password"];
    cell.passwordTF.userInteractionEnabled = NO;
   
    cell.nPasswordTF.text = self.adminInfo[@"nPassword"];
    cell.verifyPasswordTF.text = self.adminInfo[@"vPassword"];
    
    return cell;
}


- (UITableViewCell*) configureAdminLevelCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    MyProfileAssignedInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"adminLevelInfoCell"];
    
    cell.adminLevelTF.text = [NSString stringWithFormat:@"Administrator Level: %@", [[DataManager sharedInstance] getAdminLevelTitleById:[[DataManager sharedInstance].adminInfo[@"acl"] intValue]]];
    
    
    int acl = [[DataManager sharedInstance].adminInfo[@"acl"] intValue];

    BOOL onlyOneRoom = NO;
    
    if (acl==3) {
        onlyOneRoom = YES;
    }
    
    NSString *roomName = @"N/A";
    id roomTitle = [DataManager sharedInstance].adminInfo[@"room_title"];
    
    if (roomTitle) {
     
        if (onlyOneRoom && [roomTitle isKindOfClass:[NSString class]]) {
            roomName = roomTitle;
        }
        
    }
    

    
    if (onlyOneRoom) {
        cell.assignedRoomTF.text = [NSString stringWithFormat:@"Assigned Room: %@", roomName];
    }else{
        cell.assignedRoomTF.text = [NSString stringWithFormat:@"Assigned Room: All Rooms"];
    }

    
    /*
    if ([[DataManager sharedInstance].adminInfo[@"room_title"] intValue] == 0) {
        cell.assignedRoomTF.text = [NSString stringWithFormat:@"Assigned Room: All Rooms"];
        
    }else{
        cell.assignedRoomTF.text = [NSString stringWithFormat:@"Assigned Room: %@", [DataManager sharedInstance].adminInfo[@"room_title"]];
    }
    */
    cell.assignedPositionTF.text = [NSString stringWithFormat:@"Assigned Position: %@", [DataManager sharedInstance].adminInfo[@"position_title"]];
    
    
    return cell;
}


- (UITableViewCell *)configureSpoilerCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"spoilerCell"];
    if (cell) {
        UIImageView *spoilerImage = (UIImageView *)[cell.contentView viewWithTag:SPOILER_IMAGE_VIEW_TAG];
        spoilerImage.image = [spoilerImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        [self addBorder:PDBorderTopAndBottom toView:containerView forToolbar:NO];
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    }
    return cell;
}

- (UITableViewCell *)configureSpoiler2CellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"spoiler2Cell"];
    if (cell) {
        UIImageView *spoilerImage = (UIImageView *)[cell.contentView viewWithTag:SPOILER_IMAGE_VIEW_TAG];
        spoilerImage.image = [spoilerImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        [self addBorder:PDBorderTopAndBottom toView:containerView forToolbar:NO];
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    }
    return cell;
}


- (UITableViewCell *)configureSpoiler3CellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"spoiler3Cell"];
    if (cell) {
        UIImageView *spoilerImage = (UIImageView *)[cell.contentView viewWithTag:SPOILER_IMAGE_VIEW_TAG];
        spoilerImage.image = [spoilerImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        [self addBorder:PDBorderTopAndBottom toView:containerView forToolbar:NO];
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    }
    return cell;
}


- (InfoCell *)configureInfoCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    InfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"infoCell"];
    return [self configureBaseInfoCell:cell indexPath:indexPath];
}



- (InfoCell *)configureInfo2CellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    InfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"info2Cell"];
    return [self configureBaseInfoCell:cell indexPath:indexPath];
}


- (InfoCell *)configureInfo3CellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    InfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"info3Cell"];
    return [self configureBaseInfoCell:cell indexPath:indexPath];
}


- (InfoCell *)configureInfo4CellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    InfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"info4Cell"];
    return [self configureBaseInfoCell:cell indexPath:indexPath];
}



- (InfoCell*)configureBaseInfoCell:(InfoCell*)cell indexPath:(NSIndexPath *)indexPath
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    if (cell) {
        if ([row.type isEqualToString:PDInfoCell]) {
            NSString *attrStr = cell.infoLabel.text;
            
            NSString *adminName = [NSString stringWithFormat: @"%@ %@",[DataManager sharedInstance].adminInfo[@"first_name"], [DataManager sharedInstance].adminInfo[@"last_name"]];
            
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%ADMIN_NAME%" withString:adminName];
            
            NSString *firstString;
            NSRange foundRange = [attrStr rangeOfString:@"\n"];
            if (foundRange.location != NSNotFound) {
                foundRange.length = foundRange.location + 1;
                foundRange.location = 0;
                firstString = [[NSString alloc] initWithString:[attrStr substringWithRange:foundRange]];
            }
            
            cell.infoLabel.text = attrStr;
            cell.infoLabel.attributedText = [self highlightString:firstString inString:attrStr forRow:row];
            cell.infoLabel.textColor = [MyColorTheme sharedInstance].textViewTextColor;
        }
    }
    return cell;
}


- (NSAttributedString *)highlightString:(NSString*)subString inString:(NSString *)mainString forRow:(AXRow*)row
{
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString:[[NSAttributedString alloc] initWithString:mainString]];
    if (!subString) {
        subString = mainString;
    }

    if ([row.type isEqualToString:PDInfoCell]) {
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:11.f];
        NSRange titleRange = [[mainString lowercaseString] rangeOfString:[subString lowercaseString]];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(titleRange.location, subString.length)];
    }
    return text;
}


- (void)keyboardWillShow:(NSNotification *)notification
{
    // alternative
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height) + 20.f, 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width) + 20.f, 0.0);
    }
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
}


- (void)keyboardWillHide:(NSNotification *)notification
{
    // alternative
    self.tableView.contentInset = UIEdgeInsetsZero;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    UITableViewCell *cell = [textField superCell];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    row.text = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if (row.type == PDAccessInfoCell){
        if (textField.tag==1) {
            [self.adminInfo setObject:row.text forKey:@"username"];
        }else if (textField.tag==2){
            [self.adminInfo setObject:row.text forKey:@"first_name"];
        }else if (textField.tag==3){
            [self.adminInfo setObject:row.text forKey:@"last_name"];
        }else if (textField.tag==4){
            [self.adminInfo setObject:row.text forKey:@"email"];
        }else if (textField.tag==6){
            [self.adminInfo setObject:row.text forKey:@"nPassword"];
        }else if (textField.tag==7){
            [self.adminInfo setObject:row.text forKey:@"vPassword"];
        }
    }
    
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{

}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark Image Picker

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Camera
    if (buttonIndex==0) {
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate   = self;
        [self.navigationController presentViewController:picker animated:YES completion:nil];
    }
    // Library
    else if(buttonIndex==1) {
        UIImagePickerController *picker=[[UIImagePickerController alloc]init];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate   = self;
        [self.navigationController presentViewController:picker animated:YES completion:nil];
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.picImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    self.picImage=[[Utils sharedInstance]imageScaledToRoomImage:self.picImage];
    
    [self.addPhotoButton setBackgroundImage:self.picImage forState:UIControlStateNormal];
    self.isImageChanged = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)uploadPhotoTapped:(id)sender
{
    self.actionSheet_photo = [[UIActionSheet alloc]initWithTitle:@"Please select photo source" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take photo from camera", @"Choose photo from library", nil];
    [self.actionSheet_photo showInView:self.view];
}



- (IBAction)saveChangesTapped:(id)sender
{
    if ([self.adminInfo[@"nPassword"] isEqualToString:self.adminInfo[@"vPassword"]]) {
        if ([self.adminInfo[@"nPassword"] length]<4 && [self.adminInfo[@"nPassword"] length]>0) {
            ErrorAlert(@"Password must be at least 4 characters long");
            return;
        }
    }else {
        ErrorAlert(@"The Password and the verify Password fields do not match, please try again.");
        return;
    }
    
    
    if ([self checkAllFieldsInDict:self.adminInfo]) {
        [[NetworkManager sharedInstance] updateAdminWithId:[[DataManager sharedInstance].adminInfo[@"joomla_user_id"] intValue] firstName:self.adminInfo[@"first_name"] lastName:self.adminInfo[@"last_name"] email:self.adminInfo[@"email"] username:self.adminInfo[@"username"] password:self.adminInfo[@"vPassword"] accessLevel:[[DataManager sharedInstance].adminInfo[@"acl"] intValue] position:[[DataManager sharedInstance].adminInfo[@"position"] intValue] room:[[DataManager sharedInstance].adminInfo[@"room"] intValue] image:self.isImageChanged ? self.picImage : nil completion:^(id response, NSError * error) {
            
            if ([response[@"result"][@"complete"] intValue] == 1)
            {
                NSLog(@"update admin success:%@",response);
                
                [DataManager sharedInstance].adminInfo[@"username"] = self.adminInfo[@"username"];
                [DataManager sharedInstance].adminInfo[@"first_name"] = self.adminInfo[@"first_name"];
                [DataManager sharedInstance].adminInfo[@"last_name"] = self.adminInfo[@"last_name"];
                [DataManager sharedInstance].adminInfo[@"email"] = self.adminInfo[@"email"];
                
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                NSString *emailMessage = [response valueForKeyPath:@"result.messages.email"];
                NSString *usernameMessage = [response valueForKeyPath:@"result.messages.username"];
                NSString *finalMessage = [NSString new];
                
                if ([emailMessage length] > 0 && [usernameMessage length] > 0) {
                    finalMessage = [NSString stringWithFormat:@"%@ \n %@", emailMessage, usernameMessage];
                }else if ([emailMessage length] > 0){
                    finalMessage = emailMessage;
                }else if ([usernameMessage length] > 0){
                    finalMessage = usernameMessage;
                }
                
                ErrorAlert(finalMessage);
            }
        }];
    }
}


- (BOOL)checkAllFieldsInDict:(NSDictionary*)dict
{
    NSMutableArray *empty = [NSMutableArray new];
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
    [dictionary setObject:@"1" forKey:@"nPassword"];
    [dictionary setObject:@"1" forKey:@"vPassword"];
    
    for (int i=0; i<dictionary.count; i++) {
        NSString *string=[dictionary objectForKey:[[dictionary allKeys] objectAtIndex:i]];
        if (string.length == 0) {
            [empty addObject:[[dictionary allKeys] objectAtIndex:i]];
        }
    }
    if (empty.count>0) {
        NSArray *capArray = [empty valueForKeyPath:@"capitalizedString"];
        NSString *alert = [NSString stringWithFormat:@"Not all required fields have been completed to save this form.\nYou must complete the following fields: %@",[capArray componentsJoinedByString:@", "]];
        Alert(@"Error", alert);
        return NO;
    } else {
        return YES;
    }
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



@end
