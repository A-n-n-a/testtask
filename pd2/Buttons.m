//
//  Buttons.m
//  pd2
//
//  Created by i11 on 16/04/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Buttons.h"
#import "MyColorTheme.h"
#import "UIImage+Utils.h"
#import "UIImage+ImageByApplyingAlpha.h"

@interface Buttons ()

@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;


@end

@implementation Buttons

//TODO: enum c текущим цветом (confirm, cancel) кнопки
//TODO: Label должен быть medium?
//TODO: Поправить цвета?
//TODO: Сделать animation перекрашивание?
//TODO: Добавить остальные states?


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialSetup];
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialSetup];
    }
    return self;
}

/*
- (void)awakeFromNib {
    //[self initialSetup];
}
*/

- (void)initialSetup
{

    self.layer.cornerRadius = 5.f;
    self.layer.masksToBounds = YES;

}

- (void)makeMeAsConfirmButton {
    
    UIImage *normalStateImage = [UIImage imageWithColor:[MyColorTheme sharedInstance].buttonConfirmBackgroundColor];
    
    [self setBackgroundImage:normalStateImage forState:UIControlStateNormal];
    [self setBackgroundImage:[normalStateImage imageByApplyingAlpha:0.3f] forState:UIControlStateDisabled];
    [self setBackgroundImage:[normalStateImage imageByApplyingAlpha:0.5f] forState:UIControlStateSelected];
    [self setBackgroundImage:[normalStateImage imageByApplyingAlpha:0.5f] forState:UIControlStateHighlighted];
    
    
    [self setTitleColor:[MyColorTheme sharedInstance].buttonConfirmTextColor forState:UIControlStateNormal];
    [self setTitleColor:[MyColorTheme sharedInstance].buttonConfirmTextColor forState:UIControlStateDisabled];
    [self setTitleColor:[MyColorTheme sharedInstance].buttonConfirmTextColor forState:UIControlStateSelected];
    [self setTitleColor:[MyColorTheme sharedInstance].buttonConfirmTextColor forState:UIControlStateHighlighted];
    
    self.textColor = [MyColorTheme sharedInstance].buttonConfirmTextColor;
}

- (void)makeMeAsCancelButton {
    
    UIImage *normalStateImage = [UIImage imageWithColor:[MyColorTheme sharedInstance].buttonCancelBackgroundColor];
    
    [self setBackgroundImage:normalStateImage forState:UIControlStateNormal];
    [self setBackgroundImage:[normalStateImage imageByApplyingAlpha:0.3f] forState:UIControlStateDisabled];
    [self setBackgroundImage:[normalStateImage imageByApplyingAlpha:0.5f] forState:UIControlStateSelected];
    [self setBackgroundImage:[normalStateImage imageByApplyingAlpha:0.5f] forState:UIControlStateHighlighted];

    
    [self setTitleColor:[MyColorTheme sharedInstance].buttonCancelTextColor forState:UIControlStateNormal];
    [self setTitleColor:[MyColorTheme sharedInstance].buttonCancelTextColor forState:UIControlStateDisabled];
    [self setTitleColor:[MyColorTheme sharedInstance].buttonCancelTextColor forState:UIControlStateSelected];
    [self setTitleColor:[MyColorTheme sharedInstance].buttonCancelTextColor forState:UIControlStateHighlighted];
    
    self.textColor = [MyColorTheme sharedInstance].buttonCancelTextColor;
}

- (void)setTitleForAllStates:(NSString *)title {
    
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitle:title forState:UIControlStateDisabled];
    [self setTitle:title forState:UIControlStateSelected];
    [self setTitle:title forState:UIControlStateHighlighted];
}


- (void)hideText {
 
    [self setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor clearColor] forState:UIControlStateDisabled];
    [self setTitleColor:[UIColor clearColor] forState:UIControlStateSelected];
    [self setTitleColor:[UIColor clearColor] forState:UIControlStateHighlighted];
    
}


- (void)showText {
    
    [self setTitleColor:self.textColor forState:UIControlStateNormal];
    [self setTitleColor:self.textColor forState:UIControlStateDisabled];
    [self setTitleColor:self.textColor forState:UIControlStateSelected];
    [self setTitleColor:self.textColor forState:UIControlStateHighlighted];
    
}


- (void)showActivityIndicator {
    
    /*
    if ([self.isActivityIndicatorActive boolValue]) {
        return;
    }
    */
    
    if (!self.activityIndicator) {
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityIndicator.hidesWhenStopped = YES;

        [activityIndicator setCenter:CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds))];
        [self addSubview:activityIndicator];
        
        self.activityIndicator = activityIndicator;
    }
    
    [self.activityIndicator startAnimating];
    
    [self hideText];
    
    [self setUserInteractionEnabled:NO];
    
    //self.isActivityIndicatorActive = @YES;
    
}

- (void)hideActivityIndicator {
    
    [self showText];
    [self.activityIndicator stopAnimating];
    //[self.activityIndicator removeFromSuperview];
    [self setUserInteractionEnabled:YES];
    
    //self.isActivityIndicatorActive = @NO;
    
}

- (void)setActivityIndicatorStatus:(BOOL)status {
    if (status) {
        [self showActivityIndicator];
    } else {
        [self hideActivityIndicator];
    }
}

@end
