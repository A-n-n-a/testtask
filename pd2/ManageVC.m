//
//  ManageVC.m
//  pd2
//
//  Created by User on 09.07.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "ManageVC.h"
#import "VideoView.h"

#define COLLECTION_BUTTON_TAG 100


@interface ManageVC ()

@property NSArray *buttonsArray;

@end

@implementation ManageVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addHomeButton];
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 300)];
    titleLabel.text = @"Manage";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont boldSystemFontOfSize:23];
    self.navigationItem.titleView=titleLabel;
    [AppDelegate delegate].navigationController = self.navigationController;
    
    
    
    
        self.buttonsArray = @[@"administratorsCell"];
        
    
    
    
    
    
    self.view.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];

}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _buttonsArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier=[_buttonsArray objectAtIndex:indexPath.row];
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    if (cell) {
        
        UIButton *button = (UIButton *)[cell.contentView viewWithTag:COLLECTION_BUTTON_TAG];
        
        
        if ([cellIdentifier isEqualToString:@"roomsCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_mainscreen_rooms.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"roomsButton.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
        }

        if ([cellIdentifier isEqualToString:@"profileSectionsCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_mainscreen_profile-sections.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"profileSectionsButton.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
        }
        
        
        
        if ([cellIdentifier isEqualToString:@"administratorsCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageAdministrators]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"myProfileCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageMyProfile]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"childrenCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageChildren]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"parentalAccessCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageParentalAccess]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"positionsCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManagePositions]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"roomsCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageRooms]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"diaryHelpersCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageDiaryHelpers]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"menuHelpersCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageDiaryHelpers]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"profileSectionsCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageProfileSections]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"birthdaysCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageBirthdays]) {
            [self buttonAsDisabled:button];
        }
        
    }

    
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    NSLog(@"Identifier: %@", identifier);
    
    if ([identifier isEqualToString:@"administrators@Manage_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageAdministrators]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"childrenSegue"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageChildren]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"parentalAccess@Manage_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageParentalAccess]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"positions@Manage_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManagePositions]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"rooms@Manage_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageRooms]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"diaryHelpers@Manage_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageDiaryHelpers]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"profileSections@Manage_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageProfileSections]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"childrenBirthdays@Manage_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageBirthdays]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"myProfile@Manage_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageMyProfile]) {
        [self showUnavailableAlert];
        return NO;
    }
    else {
        return YES;
    }
    
    
    
}

@end
