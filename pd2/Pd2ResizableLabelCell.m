#import "Pd2ResizableLabelCell.h"
#import "UIView+Frame.h"
#import "UILabel+LinesCount.h"

@interface Pd2ResizableLabelCell()

@property (nonatomic, strong) UILabel * label_title;

@end

@implementation Pd2ResizableLabelCell
@synthesize height = _height;

- (void) awakeFromNib
{
    CGFloat xOffset = 15;
    self.label_title = [[UILabel alloc] initWithFrame:(CGRect) {
        .origin = {xOffset, 0},
        .size   = {
            self.contentView.width - xOffset*2,
            self.contentView.height
        }
    }];
    self.label_title.numberOfLines = 0;
    self.label_title.preferredMaxLayoutWidth = self.label_title.frame.size.width;
    self.label_title.tag = 1001;
    
    [self.contentView addSubview:self.label_title];
}

- (void) updateUI
{
    CGSize size = [self.label_title sizeThatFits:CGSizeMake(self.label_title.width, CGFLOAT_MAX)];
    CGFloat height = size.height < 44 ? 44 : size.height;
    height = [self.label_title linesCount] >= 3 ? height + 16 : height;
    
    self.label_title.height = height;
    _height = height;
}


#pragma mark - Setters/Getters -

- (void) setFont:(UIFont *)font
{
    self.label_title.font = font;
    [self updateUI];
}

- (UIFont *) font
{
    return self.label_title.font;
}

- (void) setText:(NSString *)text
{
    self.label_title.text = text;
    [self updateUI];
}

- (NSString *) text
{
    return self.label_title.text;
}

- (void) setTextColor:(UIColor *)textColor
{
    self.label_title.textColor = textColor;
}

- (UIColor *) textColor
{
    return self.label_title.textColor;
}

- (CGFloat) height
{
    return _height;
}

@end
