//
//  ChildContent2Cell.m
//  pd2
//
//  Created by User on 22.03.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ChildContent2Cell.h"

@implementation ChildContent2Cell

- (void)awakeFromNib {
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"dd.MM.yyyy";
    self.childDateOfBirthTextField.text = [formatter stringFromDate:[NSDate date]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
