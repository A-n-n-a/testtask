#import "NSString+Utils.h"
#import <CommonCrypto/CommonDigest.h>
#import <Foundation/NSString.h>
#import "NSData+Base64.h"

@implementation NSString (Utils)
+ (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    return  output;
}

+ (NSString *)toBase64String:(NSString *)string
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSData *base64Data = [data base64EncodedDataWithOptions:0];
    return [[NSString alloc] initWithData:base64Data encoding:NSUTF8StringEncoding];
}

+ (NSString *)fromBase64String:(NSString *)string
{
    NSData  *base64Data = [NSData dataWithBase64EncodedString:string];
    
    NSString* decryptedStr = [[NSString alloc] initWithData:base64Data encoding:NSUnicodeStringEncoding];
    
    return decryptedStr;
}

- (NSString *) firstLetterUppercaseString
{
    /* get first char */
    NSString *firstChar = [self substringToIndex:1];
    
    /* remove any diacritic mark */
    NSString *folded = [firstChar stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale currentLocale]];
    
    return [[folded uppercaseString] stringByAppendingString:[self substringFromIndex:1]];
}

@end
