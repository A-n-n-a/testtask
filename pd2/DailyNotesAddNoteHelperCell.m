//
//  DailyNotesAddNoteHelperCell.m
//  pd2
//
//  Created by Andrey on 28/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "DailyNotesAddNoteHelperCell.h"
#import "DailyNotesNoteModel.h"

@implementation DailyNotesAddNoteHelperCell



- (void)updateWithModel:(DailyNotesNoteModel *)model {
    
    self.helperTextField.text = @"";
    self.noteTextView.text = model.note;
    

}

@end
