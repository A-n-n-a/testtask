//
//  Admin2Model.m
//  pd2
//
//  Created by i11 on 11/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Admin2Model.h"

@implementation Admin2Model

/*
 {"id":"1","uid":"63","acl":"1","position":"2","room":"1","chid":"0","chid2":"0","chid3":"0","first_name":"Zero","last_name":"Zero","image":"images/sted/admins/admin_63.png","pass":"","pin":"a670d6ab69d9e49ed40206d7dcd323b2:3FMiReUZvJbMyY87ayaOYQ07uISk8Sxq","log_id":null,"confirm":null,"confirm_email":null,"activate":null,"options":null,"ordering":"0","published":"0","block":"0","pm":"0","homepage_settings":"","username":"admin","email":"a@1a.com","title":"Def1"}
 */

//Не полная модель. По мере потребности заполняется.
//Chid..3 - сделать классами child и тд (room, acl - можно тоже классами)

- (instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        
        self.adminId = [self safeNumber:dict[@"id"]];
        self.uid = [self safeNumber:dict[@"uid"]];
        self.acl = [self safeNumber:dict[@"acl"]];
                
        if (self.acl) {
            self.adminType = [self.acl intValue];
        }
        
        self.position = [self safeNumber:dict[@"position"]];
        self.room = [self safeNumber:dict[@"room"]];
        self.chid = [self safeNumber:dict[@"chid"]];
        self.chid2 = [self safeNumber:dict[@"chid2"]];
        self.chid3 = [self safeNumber:dict[@"chid3"]];
        
        self.firstName = [self safeString:dict[@"first_name"]];
        self.lastName = [self safeString:dict[@"last_name"]];
        self.userName = [self safeString:dict[@"username"]];
        self.email = [self safeString:dict[@"email"]];
        self.title = [self safeString:dict[@"title"]];
        self.imagePath = [self safeString:dict[@"image"]];
        
    }
    return self;
}



@end
