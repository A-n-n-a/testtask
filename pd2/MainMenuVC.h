//
//  MainMenuVC.h
//  pd2
//
//  Created by User on 09.07.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "Pd2MenuViewController.h"


@interface MainMenuVC : Pd2MenuViewController <UIGestureRecognizerDelegate,UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic,retain)IBOutlet UIScrollView *scrollView;
@property (nonatomic,retain)IBOutlet UIView *contentView;
@property (nonatomic,retain)IBOutlet UIPageControl *pageControl;

@property (nonatomic, weak) IBOutlet UILabel *versionLabel;

@property (nonatomic, weak) IBOutlet UIView *quickMenuContainerView;


- (IBAction)addLongTerm:(id)sender;
- (IBAction)addShortTerm:(id)sender;

@end
