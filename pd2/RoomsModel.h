//
//  RoomsModel.h
//  pd2
//
//  Created by Andrey on 05/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXBaseModel.h"

@class RoomModel;

@interface RoomsModel : AXBaseModel

@property (nonatomic, strong) NSArray *rooms;

- (instancetype)initWithArray:(NSArray *)array;

- (NSArray *)roomList;
- (RoomModel *)roomModelByRoomId:(int)roomId;
- (RoomModel *)roomModelByRoomName:(NSString *)roomName;

@end
