//
//  infoCell.m
//  pd2
//
//  Created by i11 on 06/02/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "InfoCell.h"
#import "EditBoxLabel.h"


@implementation InfoCell

- (void)awakeFromNib
{
    while (self.contentView.gestureRecognizers.count > 0) {
        [self.contentView removeGestureRecognizer:[self.contentView.gestureRecognizers lastObject]];
    }
    for (UIView *view in self.contentView.subviews) {
        
        if ([view isKindOfClass:[EditBoxLabel class]]) {
            continue;
        }
        
        view.backgroundColor = [[MyColorTheme sharedInstance] textViewBackGroundColor];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.contentView layoutIfNeeded];
    self.infoLabel.preferredMaxLayoutWidth = self.infoLabel.frame.size.width;
    //self.infoLabel.font = [UIFont systemFontOfSize:14];
    /*
    
    NSMutableAttributedString* attributedString = [self.infoLabel.attributedText mutableCopy];
    
    {
    
        [attributedString beginEditing];
        
        [attributedString enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attributedString.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
            
            
            UIFont *font = value;
            if ([font.fontName containsString:@"Medium"]) {
                font = [UIFont boldSystemFontOfSize:12];
            }
            else {
                font = [UIFont systemFontOfSize:12];
            }
            
            NSLog(@"fontName = %@", font.fontName);
            
            //font = [font fontWithSize:fontSize];
            
            [attributedString removeAttribute:NSFontAttributeName range:range];
            [attributedString addAttribute:NSFontAttributeName value:font range:range];
        }];
        
        [attributedString endEditing];
    }
    
    self.infoLabel.attributedText = attributedString;
    */
}

- (CGFloat)height {
    
    [self layoutIfNeeded];
    
    return [self.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;

}

-(CGFloat)textViewHeight
{
    CGFloat height = [self.infoTextView sizeThatFits:CGSizeMake(self.infoTextView.frame.size.width, FLT_MAX)].height;
    self.infoTextViewHeightConstraint.constant = height;

    return height;
}

@end
