//
//  BirthdayCollectionCell.h
//  pd2
//
//  Created by Eugene Fozekosh on 25.06.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BirthdayCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *childImageView;
@property (weak, nonatomic) IBOutlet UILabel *childNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthInfoLabel;

@end
