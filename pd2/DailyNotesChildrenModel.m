//
//  DailyNotesChildrenModel.m
//  pd2
//
//  Created by Andrey on 09/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesChildrenModel.h"
#import "BottlesDayModel.h"
#import "BottlesModel.h"

@implementation DailyNotesChildrenModel


/*
 "room": 
 {
 "id": "11",
 "title": "Giraffes",
 "image": "images/sted/rooms/11_img_2017-06-07-21-06-37.jpg",
 "from": "14",
 "to": "53",
 "ratio": "5",
 "capacity": "11",
 "ordering": "0",
 "published": "0"
 },
*/

/*
child
 {
 "chid": "171",
 "first_name": "sadfsadf",
 "last_name": "sdfsadf",
 "birth": "2017-10-25",
 "child_image": "",
 "id": "13",
 "title": "Elephants",
 "numNotes": "2",
 "numMonthNotes": "2",
 "viewed": "2",
 "numComments": "2",
 "unviewed": 0
},
*/

- (instancetype)initWithChildDict:(NSDictionary *)childDict roomDict:(NSDictionary *)roomDict {
    
    self = [super init];
    if (self) {
        
        [self updateWithChildDict:childDict];
        [self updateWithRoomDict:roomDict];
        
    }
    return self;
    
}

- (void)updateWithChildDict:(NSDictionary *)dict {
    
    if (!dict || ![dict isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    self.chId = [self safeNumber:dict[@"chid"]];
    
    self.firstName = [self safeStringAndDecodingHTML:dict[@"first_name"]];
    self.lastName = [self safeStringAndDecodingHTML:dict[@"last_name"]];
    self.dateOfBirth = [self dateFrom:dict[@"birth"] withDateFormat:@"yyyy-MM-dd"];
    self.photoPath = [self safeString:dict[@"child_image"]];
    
    self.monthlyNotesTotal = [self safeInt:dict[@"numNotes"]];
    self.monthlyNotes = [self safeInt:dict[@"numMonthNotes"]];
    
    self.commentsTotal = [self safeInt:dict[@"numComments"]];
    self.commentsNew = [self safeInt:dict[@"unviewed"]];
    

}


- (void)updateWithRoomDict:(NSDictionary *)dict {
    
    if (!dict || ![dict isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    self.roomId = [self safeNumber:dict[@"id"]];
    
    self.roomName = [self safeStringAndDecodingHTML:dict[@"title"]];
    self.roomImage = [self safeString:dict[@"image"]];
    
}

- (BOOL)isExist {
    
    if (self.firstName && self.lastName && self.chId) {
        return YES;
    }
    
    return NO;
    
}



//Filter
- (BOOL)nameContain:(NSString *)containString {
    
    containString = [containString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (!containString || ![containString isKindOfClass:[NSString class]]) {
        return YES;
    }
    
    if (!containString.length) {
        return YES;
    }
    
    if (self.firstName && [self.firstName rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    if (self.lastName && [self.lastName rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    //Может быть John Dow или Dow John
    if (self.firstName && self.lastName) {
        
        if ([[NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName] rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
            return YES;
        }
        
        if ([[NSString stringWithFormat:@"%@ %@", self.lastName, self.firstName] rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
            return YES;
        }
    }
    
    
    return NO;
    
}




@end
