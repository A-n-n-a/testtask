//
//  RoomModel.h
//  pd2
//
//  Created by Andrey on 05/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXBaseModel.h"

@interface RoomModel : AXBaseModel


/*
 {
 adminRoom = 0;
 capacity = 10;
 childRoom = 4;
 from = 12;
 id = 1;
 image = "images/sted/rooms/default.jpg";
 ordering = 0;
 published = 0;
 ratio = 1;
 title = Default;
 to = 36;
 }
 */



@property (nonatomic, strong) NSString *roomName;
@property (nonatomic, strong) NSNumber *roomId;
@property (nonatomic, strong) NSString *photoPath;
@property (nonatomic, assign) int capacity;
@property (nonatomic, assign) int childrensInRoomCount;

- (instancetype)initWithDict:(NSDictionary *)dict;
- (instancetype)initWithMovingDict:(NSDictionary *)dict;

@end
