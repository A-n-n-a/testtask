//
//  CheckboxButton.m
//  pd2
//
//  Created by i11 on 14/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "CheckboxButton.h"
#import "UIImage+ImageByApplyingAlpha.h"


@interface CheckboxButton ()

@property (nonatomic, strong) UIImage *checkedImage;
@property (nonatomic, strong) UIImage *uncheckedImage;

@end

@implementation CheckboxButton



-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self) {
        // Initialization code
        
        self.checkedImage = [UIImage imageNamed:@"checked"];
        self.uncheckedImage = [UIImage imageNamed:@"unchecked"];
        
        [self setChecked];
        
    }
    return self;
}

- (void)setCheckboxStatus:(BOOL)status {

    UIImage *image;
    
    if (status) {
        image = self.checkedImage;
        self.isChecked = YES;
    }
    else {
        image = self.uncheckedImage;
        self.isChecked = NO;
    }

    [self setImage:image forState:UIControlStateNormal];
    [self setImage:[image imageByApplyingAlpha:0.3f] forState:UIControlStateDisabled];
    [self setImage:[image imageByApplyingAlpha:0.5f] forState:UIControlStateSelected];
    [self setImage:[image imageByApplyingAlpha:0.5f] forState:UIControlStateHighlighted];
    
}


- (void)setChecked {
    [self setCheckboxStatus:YES];
}

- (void)setUnchecked {
    [self setCheckboxStatus:NO];
}



@end
