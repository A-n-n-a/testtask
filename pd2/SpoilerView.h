//
//  SpoilerView.h
//  pd2
//
//  Created by User on 13.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

enum spoilerType{ SPOILER_PARENTCARER=1, SPOILER_SENDLOGINDETAILS=2,SPOILER_ACCESS=3,SPOILER_PARENTCARERACCESS=4,SPOILER_LINKWITHSIBLINGS=5};

@interface SpoilerView : UIView

@property (nonatomic) enum spoilerType type;

@property (nonatomic,retain)IBOutlet UIImageView *icon;
@property (nonatomic,retain)IBOutlet UIImageView *arrow;
@property (nonatomic,retain)IBOutlet UILabel *label;

@property (assign, nonatomic) BOOL manualBorders;
@property (assign, nonatomic) BOOL isHeader;
@property (assign, nonatomic) BOOL isJ3;
@property (strong, nonatomic) NSString *iconName;




- (void) clearGestures;
- (void) clearParentGestures;
- (void) rotateArrow;
- (void)newIconName:(NSString *)iconName;

@end
