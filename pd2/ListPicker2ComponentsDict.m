//
//  ListPicker2ComponentsDict.m
//  pd2
//
//  Created by i11 on 26/08/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ListPicker2ComponentsDict.h"

@interface ListPicker2ComponentsDict ()

@property int selectedIndexIn0Component;
@property int selectedIndexIn1Component;
@property (nonatomic,retain) NSArray *dataArray0Component;
@property (nonatomic,retain) NSArray *dataArray1Component;


@end

@implementation ListPicker2ComponentsDict

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.frame=[UIScreen mainScreen].bounds;
    self.picker.backgroundColor=[UIColor whiteColor];
    self.doneButton.action=@selector(close:);
    NSLog(@"picker:%@",self.picker);
    
    
    [self actualizeDataArrays];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    NSUInteger index0component = [self.dataArray0Component indexOfObject:self.selectedStringIn0Component];
    
    self.selectedIndexIn0Component = index0component != NSNotFound ? (int)index0component : 0;

    
    NSUInteger index1component = [self.dataArray1Component indexOfObject:self.selectedStringIn1Component];
    
    self.selectedIndexIn1Component = index1component != NSNotFound ? (int)index1component : 0;

    
    
    [self.picker selectRow:self.selectedIndexIn0Component inComponent:0 animated:NO];
    [self.picker selectRow:self.selectedIndexIn1Component inComponent:1 animated:NO];
    
    //При вызове pickerView:didSelectRow идет обнуление selectedIndexIn1Component
    //[self pickerView:self.picker didSelectRow:self.selectedIndexIn0Component inComponent:0];
    //[self pickerView:self.picker didSelectRow:self.selectedIndexIn1Component inComponent:1];
}


-(IBAction)close:(id)sender
{
    NSLog(@"listpicker.close");
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        if (self.handler)
        {
            self.handler(_selectedStringIn0Component, _selectedStringIn1Component);
        }
        
        if (self.finishHandler)
        {
            self.finishHandler();
        }
        self.handler=nil;
        self.finishHandler=nil;
    }];
}

-(void)hide
{
    NSLog(@"listpicker.hide");
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    } completion:^(BOOL finished) {
        if (self.finishHandler)
        {
            self.finishHandler();
        }
        self.finishHandler=nil;
        [self.view removeFromSuperview];
        
    }];
}


-(void)show
{
    //int height=[UIScreen mainScreen].bounds.size.height;
    CGRect frame =[UIScreen mainScreen].bounds;// CGRectMake(0,0, 320, 568);
    
    CGRect startRect = frame;
    startRect.origin.y += self.view.frame.size.height;
    
    self.view.frame = startRect;
    
    AppDelegate *delegate=[AppDelegate delegate];
    [delegate.window addSubview:self.view];
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    }];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Utils 

- (void)actualizeDataArrays {

    self.dataArray0Component = [[self.dataDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    self.dataArray1Component = [self.dataDict objectForKey:self.selectedStringIn0Component];
    
    //На случай если такого значения нет
    if (!self.dataArray1Component) {
        id firstKey = [[self.dataDict allKeys] firstObject];
        self.dataArray1Component = [self.dataDict objectForKey:firstKey];
        NSLog(@"selectedStringIn0Component не верен. Выводим первое по первому значению");
    }
    
}


#pragma mark - Delegate

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return [self.dataArray0Component count];
    } else {
        return [self.dataArray1Component count];
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    
    //Apple bug with some sized components
    //http://stackoverflow.com/questions/19825688/ios-7-uipickerview-non-selected-row-curvature-modification-multiple-components
    return 320.f/3;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if (component == 0) {
        return [self.dataArray0Component objectAtIndex:row];
    } else {
        return [self.dataArray1Component objectAtIndex:row];
    }
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(0,
                                                            0,
                                                            [pickerView rowSizeForComponent:component].width,
                                                            20)];
    
    label.font=[UIFont systemFontOfSize:16];
    label.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    label.textAlignment=NSTextAlignmentCenter;
    //label.backgroundColor = [UIColor purpleColor];
    
    
    return label;
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        
        if (self.dataArray0Component.count)
        {
            NSString *str=[self.dataArray0Component objectAtIndex:row];
            
            _selectedIndexIn0Component=(int)row;
            _selectedStringIn0Component=str;
            
            [self actualizeDataArrays];
            
            [pickerView reloadComponent:1];
            [pickerView selectRow:0 inComponent:1 animated:YES];
            
            /*
             if (self.handler)
             {
             self.handler(str,(int)row);
             }
             */
        }
    }
    else {
        
        if (self.dataArray1Component.count)
        {
            NSString *str=[self.dataArray1Component objectAtIndex:row];
            
            _selectedIndexIn1Component=(int)row;
            _selectedStringIn1Component=str;
            
            /*
             if (self.handler)
             {
             self.handler(str,(int)row);
             }
             */
        }
    }
}
@end
