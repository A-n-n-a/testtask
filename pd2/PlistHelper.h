#import <Foundation/Foundation.h>

@interface PlistHelper : Singleton
@property (strong, nonatomic) NSDictionary * dict_content;
@property (strong, nonatomic) NSString * fileName;
@end
