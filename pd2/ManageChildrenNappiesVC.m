//
//  ManageChildrenNappiesVC.m
//  pd2
//
//  Created by User on 08.02.17.
//  Copyright (c) 2017 Andrey. All rights reserved.
//

#import "ManageChildrenNappiesVC.h"

#define COLLECTION_BUTTON_TAG 100


@interface ManageChildrenNappiesVC ()

@property NSArray *buttonsArray;

@end

@implementation ManageChildrenNappiesVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addHomeButton];
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 300)];
    titleLabel.text = @"Nappy Monitor";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont boldSystemFontOfSize:23];
    self.navigationItem.titleView=titleLabel;
    [AppDelegate delegate].navigationController = self.navigationController;
    
    self.buttonsArray = @[@"childrenInNappiesCell", @"nappyOverviewCell"];
    //int acl=[[DataManager sharedInstance] getAclForCurrentUser];
    
    self.view.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];

}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _buttonsArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier=[_buttonsArray objectAtIndex:indexPath.row];
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    if (cell) {
        
        UIButton *button = (UIButton *)[cell.contentView viewWithTag:COLLECTION_BUTTON_TAG];
        
        if ([cellIdentifier isEqualToString:@"childrenInNappiesCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageChildrenNappiesChildrenInNappies]) {
            [self buttonAsDisabled:button];
        }
        
        else if ([cellIdentifier isEqualToString:@"nappyOverviewCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageChildrenNappiesNappyOverview]) {
            [self buttonAsDisabled:button];
        }
    }

    
    return cell;
}



- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    NSLog(@"Identifier: %@", identifier);
    
    if ([identifier isEqualToString:@"childrenInNappies@Nappies_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageChildrenNappiesChildrenInNappies]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"nappyOverview@Nappies_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageChildrenNappiesNappyOverview]) {
        [self showUnavailableAlert];
        return NO;
    }
    else {
        return YES;
    }
    
    
    
}

@end
