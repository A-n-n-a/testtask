//
//  DailyNotesOverviewChildrenParentSideCell.h
//  pd2
//
//  Created by Andrey on 08.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@class DailyNotesChildrenParentSideModel;

@interface DailyNotesOverviewChildrenParentSideCell : AXTableViewCell


@property (nonatomic, weak) IBOutlet UILabel *firstNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *lastNameLabel;

@property (nonatomic, weak) IBOutlet UILabel *monthlyNotesLabel;
@property (nonatomic, weak) IBOutlet UILabel *commentsLabel;

@property (nonatomic, weak) IBOutlet UIImageView *photoImageView;

@property (nonatomic, weak) IBOutlet IconButton *viewButton;

@property (nonatomic, weak) IBOutlet UIView *toolbarView;

- (void)updateWithModel:(DailyNotesChildrenParentSideModel *)model;
- (void)updateImageWithModel:(DailyNotesChildrenParentSideModel *)model;


@end
