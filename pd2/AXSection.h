//
//  AXSection.h
//  pd2
//
//  Created by i11 on 29/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AXRow.h"

extern NSString * const AXSectionFirst;
extern NSString * const AXSectionLast;
extern NSString * const AXSectionAll;


@protocol AXSectionDelegate, AXRowDelegate;

@class AXRow, AXHeader;

@interface AXSection : NSObject <AXRowDelegate>

@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSMutableSet *tags;

@property (strong, nonatomic) AXHeader *header;

@property (strong, nonatomic) NSMutableArray *rows;

@property (assign, nonatomic) BOOL isHide;
@property (assign, nonatomic) NSArray *storedSections; //для "вложенных секций"

//Удалить?
@property (strong, nonatomic) NSString *headerType;
@property (strong, nonatomic) NSString *headerIdentifier;
@property (weak, nonatomic) UIView *headerView;
@property (assign, nonatomic) CGFloat headerHeight;

@property (weak, nonatomic) id <AXSectionDelegate> delegate;
@property (assign, nonatomic, readonly) NSInteger sectionIndex;

@property (strong, nonatomic) id model;
@property (strong, nonatomic) id object;

@property (strong, nonatomic) NSMutableDictionary *userInfo;


- (instancetype)initWithType:(NSString *)type;

- (NSIndexPath *)indexPathForRow:(AXRow *)row; //protocol

- (BOOL)addRow:(AXRow *)row;
- (void)addRows:(NSArray *)rows;
- (BOOL)addRowWithType:(NSString *)type andHeight:(CGFloat)height;

- (void)addHeader:(AXHeader *)header;

- (CGFloat)heightForRowAtIndex:(NSUInteger)index;


- (NSIndexSet *)hideSectionsWithTags:(NSSet *)tags;
- (NSIndexSet *)showHiddenSections;



@end

@protocol AXSectionDelegate
@required
- (NSInteger)indexForSection:(AXSection *)section;
- (BOOL)removeSectionsByIndexSet:(NSIndexSet *)indexSet;
- (NSArray *)sectionsAtIndexSet:(NSIndexSet *)indexSet;
- (NSIndexSet *)getIndexSetForSectionsWithTags:(NSSet *)tags;
- (NSIndexSet *)insertSections:(NSArray *)sections afterSectionIndex:(NSUInteger)sectionIndex;

@end
