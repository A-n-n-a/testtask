#import <Foundation/Foundation.h>

@interface NSArray (JSON)
- (NSString *) toJSON;
@end
