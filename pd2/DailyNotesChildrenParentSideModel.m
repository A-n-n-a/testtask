//
//  DailyNotesChildrenParentSideModel.m
//  pd2
//
//  Created by Andrey on 09/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesChildrenParentSideModel.h"

@implementation DailyNotesChildrenParentSideModel

/*
{
 "id": "199",
 "image": "",
 "first_name": "andy",
 "last_name": "andy",
 "birth": "2017-12-12",
 "room_id": "13",
 "room_title": "Elephants",
 "total_count": "1",
 "total_comments": "10",
 "viewed": "2",
 "unviewed": "8",
 "monthly_count": 1
}
*/

- (instancetype)initWithDict:(NSDictionary *)dict {
    
    self = [super initWithDict:dict];
    if (self) {
        
        self.chId = [self safeNumber:dict[@"id"]];
        
        self.firstName = [self safeStringAndDecodingHTML:dict[@"first_name"]];
        self.lastName = [self safeStringAndDecodingHTML:dict[@"last_name"]];
        self.dateOfBirth = [self dateFrom:dict[@"birth"] withDateFormat:@"yyyy-MM-dd"];
        self.photoPath = [self safeString:dict[@"image"]];
        
        self.monthlyNotesTotal = [self safeInt:dict[@"total_count"]];
        self.monthlyNotes = [self safeInt:dict[@"monthly_count"]];
        
        self.commentsTotal = [self safeInt:dict[@"total_comments"]];
        self.commentsNew = [self safeInt:dict[@"unviewed"]];
        
        self.roomId = [self safeNumber:dict[@"room_id"]];
        self.roomName = [self safeString:dict[@"room_title"]];
        
    }
    return self;
    
}


- (BOOL)isExist {
    
    if (self.firstName && self.lastName && self.chId) {
        return YES;
    }
    
    return NO;
    
}







@end
