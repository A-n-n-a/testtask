#import "SectionChildModel.h"
#import "NSDateFormatter+Convert.h"

@interface NSDictionary (NAValue)
- (id) valueForKeyOrNA:(NSString *)key;
@end

@implementation NSDictionary (NAValue)
- (id) valueForKeyOrNA:(NSString *)key
{
    if ([self valueForKey:key] == nil && ![[self valueForKey:key] isKindOfClass:[NSString class]]) {
        return @"N/A";
    }
    return [self valueForKey:key];
}
@end


@interface SectionChildModel()
@property (nonatomic, strong) NSString *idKey, *firstNameKey, *lastNameKey, *roomIdKey, *roomTitleKey, *photoPathKey, *dateOfBirthKey, *dateOfBirthFormat;
@property (nonatomic, strong) NSDictionary * sourceDict;
@end

@implementation SectionChildModel

- (void) loadInfoWithSourceType:(SectionChildModelSourceType)sourceType fromDict:(NSDictionary *)childInfo
{
    switch (sourceType) {
        case SectionChildModelSourceTypeNextSteps:
            [self loadInfoFromNSDict:childInfo];
            break;
        case SectionChildModelSourceTypeProgress:
            [self loadInfoFromProgressDict:childInfo];
            break;
        case SectionChildModelSourceTypeWellbeing:
            [self loadInfoFromWellbeingDict:childInfo];
            break;
        case SectionChildModelSourceTypeParentWellbeing:
            [self loadInfoFromParentWellbeingDict:childInfo];
            break;
    }
}

- (void) loadInfoFromNSDict:(NSDictionary *)childInfo
{
    self.idKey          = @"Id";
    self.firstNameKey   = @"Fist_name";
    self.lastNameKey    = @"Last_name";
    self.roomIdKey      = @"Room_id";
    self.roomTitleKey   = @"Room_title";
    self.photoPathKey   = @"Photograph";
    self.dateOfBirthKey = @"Birth";
    self.dateOfBirthFormat = @"yyyy-MM-dd";
    self.sourceDict = childInfo;
    [self updateFromSource];
}

- (void) loadInfoFromProgressDict:(NSDictionary *)childInfo
{
    // Fix for scottish version
    if ([[NetworkManager sharedInstance] isScottish]) {
        [self loadInfoFromWellbeingDict:childInfo];
        return;
    }
    
    self.idKey          = @"id";
    self.firstNameKey   = @"first_name";
    self.lastNameKey    = @"last_name";
    self.roomIdKey      = @"room_id";
    self.roomTitleKey   = @"room_title";
    self.photoPathKey   = @"photograph";
    self.dateOfBirthKey = @"birthday";
    self.dateOfBirthFormat = @"dd.MM.yyyy";
    self.sourceDict = childInfo;
    [self updateFromSource];
}

- (void) loadInfoFromParentWellbeingDict:(NSDictionary*)childInfo
{
    self.idKey          = @"Id";
    self.firstNameKey   = @"Fist_name";
    self.lastNameKey    = @"Last_name";
    self.roomIdKey      = @"Room_id";
    self.roomTitleKey   = @"Room_title";
    self.photoPathKey   = @"Photograph";
    self.dateOfBirthKey = @"DOB";
    self.dateOfBirthFormat = @"dd.MM.yyyy";
    self.sourceDict = childInfo;
    [self updateFromSource];
}

/**
 * Keys:
 *
 * birthday
 * first_name
 * id
 * last_name
 * parents
 * photograph
 * room_id
 * room_title
 *
 */
- (void) loadInfoFromWellbeingDict:(NSDictionary *)childInfo
{
    self.idKey          = @"id";
    self.firstNameKey   = @"first_name";
    self.lastNameKey    = @"last_name";
    self.roomIdKey      = @"room_id";
    self.roomTitleKey   = @"room_title";
    self.photoPathKey   = @"photograph";
    self.dateOfBirthKey = @"birthday";
    self.dateOfBirthFormat = @"dd.MM.yyyy";
    self.sourceDict = childInfo;
    [self updateFromSource];
    
}

- (void) updateFromSource
{
    _child_id  = [_sourceDict valueForKey:_idKey];
    _firstName = [[_sourceDict valueForKeyOrNA:_firstNameKey] capitalizedString];
    _lastName  = [[_sourceDict valueForKeyOrNA:_lastNameKey] capitalizedString];
    _roomId    = [_sourceDict valueForKey:_roomIdKey];
    _roomTitle = [_sourceDict valueForKeyOrNA:_roomTitleKey];
    _photoPath = [_sourceDict valueForKey:_photoPathKey];
    _dateOfBirthString = [_sourceDict valueForKey:_dateOfBirthKey];
    [self updateDateOfBirthFromFormat:_dateOfBirthFormat];
}



#pragma mark - Getters -

- (NSString *) dateOfBirthString
{
    return _dateOfBirthString == nil ? @"N/A" : _dateOfBirthString;
}

- (NSString *) firstName
{
    return _firstName == nil ? @"N/A" : _firstName;
}

- (NSString *) lastName
{
    return _lastName == nil ? @"N/A" : _lastName;
}

- (NSString *) roomTitle
{
    return _roomTitle == nil ? @"N/A" : _roomTitle;
}



#pragma mark - Private -

- (void) updateDateOfBirthFromFormat:(NSString *)dateFormat
{
    _dateOfBirthString = [NSDateFormatter convertDateFromString:_dateOfBirthString fromFormat:dateFormat toFormat:@"dd.MM.yyyy"];
    if (_dateOfBirthString == nil) {
        _dateOfBith = [NSDate date];
        _monthsCount = @(0);
    }
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd.MM.yyyy";
    _dateOfBith = [formatter dateFromString:_dateOfBirthString];
    
    if (_dateOfBith == nil) {
        _dateOfBith = [NSDate date];
    }
    
    NSInteger month = [[[NSCalendar currentCalendar] components: NSCalendarUnitMonth
                                                       fromDate: _dateOfBith
                                                         toDate: [NSDate date]
                                                        options: 0] month];
    _monthsCount = @(month);
}

@end
