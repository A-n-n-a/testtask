//
//  Singleton.h
//  ValterTaxiClient
//
//  Created by Evgeniy Tka4enko on 19.02.14.
//  Copyright (c) 2014 vexadev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject

+ (instancetype)sharedInstance;

+ (void)destroyInstance;

@end
