//
//  DailyNotesOverviewNoteCell.m
//  pd2
//
//  Created by Andrey on 08.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesOverviewNoteCell.h"
#import "DailyNotesPendingNoteModel.h"


@implementation DailyNotesOverviewNoteCell

- (void)updateWithModel:(DailyNotesPendingNoteModel *)model {
    
    self.noteTitleLabel.text = notAvailStringIfNil(hasLeadingNumberInString(model.noteTitle) ? model.noteTitle : [model.noteTitle uppercaseFirstLetter]);
    self.noteBoxLabel.text = [NSString stringWithFormat:@"%d", model.noteBox];
    self.dateLabel.text = notAvailStringIfNil([model.date printableDefault]);
    self.childrenCountLabel.text = [NSString stringWithFormat:@"%d", model.childrenCount];

}




@end
