//
//  NewAdminVC.h
//  pd2
//
//  Created by User on 17.07.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "Pd2ViewController.h"
#import "Pd2TableViewController.h"
#import "ListPicker.h"

@interface NewAdminVC : Pd2TableViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIActionSheetDelegate>

@property bool editMode;
@property (nonatomic,retain)NSDictionary *infoDict;
@property (nonatomic,retain)ListPicker *picker;

@property (nonatomic,retain)IBOutlet UITextView *infoText;
@property (strong, nonatomic) IBOutlet UITextView *secondTextView;

@property (nonatomic,retain)IBOutlet UITextField * tfUsername;
@property (nonatomic,retain)IBOutlet UITextField * tfFirstName;
@property (nonatomic,retain)IBOutlet UITextField * tfLastName;
@property (nonatomic,retain)IBOutlet UITextField * tfEmail;
@property (nonatomic,retain)IBOutlet UITextField * tfPassword1;
@property (nonatomic,retain)IBOutlet UITextField * tfPassword2;

@property (nonatomic,retain)IBOutlet UITextField *tfLevel;
@property (nonatomic,retain)IBOutlet UITextField *tfRoom;
@property (nonatomic,retain)IBOutlet UITextField *tfPosition;

@property (nonatomic,retain)IBOutlet UIButton *addPhotoButton;
@property (nonatomic,retain)IBOutlet UIButton *saveButton;

@property (nonatomic,retain)UIImage *adminImage;

-(IBAction)saveButtonPressed:(id)sender;
-(IBAction)uploadImage:(id)sender;

@end
