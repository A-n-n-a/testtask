//
//  EditChildViewController.h
//  pd2
//
//  Created by User on 05.08.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "Pd2TableViewController.h"
#import "CheckSwitch.h"
#import "ListPicker.h"
#import "DOBPicker.h"

@interface EditChildViewController : Pd2TableViewController<UITextFieldDelegate, UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate, CheckSwitchDelegate>

@property (nonatomic,strong)NSMutableDictionary *infoDict;
@property (nonatomic) bool editMode, initialized, showRoomMoveDate,showProgressDates, isCopyMode;

@property (nonatomic,retain)UIImage *image;

@property (nonatomic,strong) NSArray *checkArray;
@property (nonatomic,strong) NSArray *arrowArray;
@property (nonatomic,strong) NSMutableArray* profileSections, *childSectionCells;
@property (nonatomic,strong) NSMutableDictionary *textfields, *requiredTextFields, *checkSwitches;

@property (nonatomic,strong) ListPicker *listPicker;
@property (nonatomic,strong) DOBPicker *datePicker;

@property bool showSection0, showSection1,showSection2,showSection3,showSection4,showSection5,showSection6,parent2added;
@property int section0height, section1height,section2height,section3height,section4height,section5height,section6height;
@property (nonatomic,strong)UIImageView *arrow1,*arrow2,*arrow3,*arrow4,*arrow5,*arrow6;
@property (nonatomic,strong) UITextView *topTextView;


//child information
@property (nonatomic,strong) UITextField *childFirstNameTextField;
@property (nonatomic,strong) UITextField *childLastNameTextField;
@property (nonatomic,strong) UITextField *childPostCodeTextField;
@property (nonatomic,strong) UITextField *childHouseStreetTextField;
@property (nonatomic,strong) UITextField *childAreaTextField;
@property (nonatomic,strong) UITextField *childTownTextField;
@property (nonatomic,strong) UITextField *childCountryTextField;

@property (nonatomic,strong) UITextField *childBoyOrGirlTextField;
@property (nonatomic,strong) UITextField *childDateOfBirthTextField;
@property (nonatomic,strong) CheckSwitch *childSpecialEducationCheckSwitch;
@property (nonatomic,strong) CheckSwitch *childInNappiesCheckSwitch;
@property (nonatomic,strong) CheckSwitch *childFundedCheckSwitch;
@property (nonatomic,strong) CheckSwitch *childEALCheckSwitch;
@property (nonatomic,strong) CheckSwitch *childPremiumCheckSwitch;

@property (nonatomic,strong) UITextField *childRoomTextField;
@property (nonatomic,strong) UITextField *childRoomMoveTextField;

@property (nonatomic,strong) UITextField *childMoveDateTextField;

@property (nonatomic,strong) CheckSwitch *childBackDateProgressCheckSwitch;

@property (nonatomic,retain) UITextField *childStartingDateTextField;
@property (nonatomic,retain) UITextField *childFinishedDateTextField;
@property (nonatomic,retain) CheckSwitch *parentAlertsSwitch;


//parent/carer1 information
@property (nonatomic,retain) UITextField *parent1FirstNameTextField;
@property (nonatomic,retain) UITextField *parent1LastNameTextField;
@property (nonatomic,retain) UITextField *parent1PostCodeTextField;
@property (nonatomic,retain) UITextField *parent1HouseStreetTextField;
@property (nonatomic,retain) UITextField *parent1AreaTextField;
@property (nonatomic,retain) UITextField *parent1TownTextField;
@property (nonatomic,retain) UITextField *parent1CountryTextField;
@property (nonatomic,retain) UITextField *parent1EmailTextField;
@property (nonatomic,retain) UITextField *parent1TelephoneTextField;
@property (nonatomic,retain) UITextField *parent1MobileTextField;
@property (nonatomic,retain) UITextField *parent1PlaceOfWorkTextField;
@property (nonatomic,retain) UITextField *parent1WorkTelephoneTextField;

//parent/carer2 information
@property (nonatomic,retain) UITextField *parent2FirstNameTextField;
@property (nonatomic,retain) UITextField *parent2LastNameTextField;
@property (nonatomic,retain) UITextField *parent2PostCodeTextField;
@property (nonatomic,retain) UITextField *parent2HouseStreetTextField;
@property (nonatomic,retain) UITextField *parent2AreaTextField;
@property (nonatomic,retain) UITextField *parent2TownTextField;
@property (nonatomic,retain) UITextField *parent2CountryTextField;
@property (nonatomic,retain) UITextField *parent2EmailTextField;
@property (nonatomic,retain) UITextField *parent2TelephoneTextField;
@property (nonatomic,retain) UITextField *parent2MobileTextField;
@property (nonatomic,retain) UITextField *parent2PlaceOfWorkTextField;
@property (nonatomic,retain) UITextField *parent2WorkTelephoneTextField;

//send login details
@property (nonatomic,retain) CheckSwitch *sendEmailToParent1CheckSwitch;
@property (nonatomic,retain) CheckSwitch *sendEmailToParent2CheckSwitch;

//parent access
@property (nonatomic,retain) CheckSwitch *enableParent1AccessCheckSwitch;
@property (nonatomic,retain) CheckSwitch *enableParent2AccessCheckSwitch;

@property (nonatomic,strong) CheckSwitch *selectAllSwitch;
@property (nonatomic,strong) CheckSwitch *deselectAllSwitch;


@property (nonatomic,retain) CheckSwitch *parent1AccessToDiariesCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToProgressCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToObservationsCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToNextstepsCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToPoliciesCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToPermissionsCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToMedicalCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToGalleriesCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToRegisterCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToBookKeepingCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToCommunicateCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToMenusCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToQuestionsCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToContactsCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1AccessToRiskAssessmentsCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent1NappiesSwitch;
@property (nonatomic,retain) CheckSwitch *parent1PlanningSwitch;
//new
@property (nonatomic,retain) CheckSwitch *parent1CalendarSwitch;
@property (nonatomic,retain) CheckSwitch *parent1DailyNotesSwitch;
@property (nonatomic,retain) CheckSwitch *parent1SleepingSwitch;
@property (nonatomic,retain) CheckSwitch *parent1BottlesSwitch;


@property (nonatomic,retain) CheckSwitch *parent2AccessToDiariesCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToProgressCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToObservationsCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToNextstepsCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToPoliciesCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToPermissionsCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToMedicalCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToGalleriesCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToRegisterCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToBookKeepingCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToCommunicateCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToMenusCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToQuestionsCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToContactsCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2AccessToRiskAssessmentsCheckSwitch;
@property (nonatomic,retain) CheckSwitch *parent2NappiesSwitch;
@property (nonatomic,retain) CheckSwitch *parent2PlanningSwitch;
//new
@property (nonatomic,retain) CheckSwitch *parent2CalendarSwitch;
@property (nonatomic,retain) CheckSwitch *parent2DailyNotesSwitch;
@property (nonatomic,retain) CheckSwitch *parent2SleepingSwitch;
@property (nonatomic,retain) CheckSwitch *parent2BottlesSwitch;


//emergency details
@property (nonatomic,retain) UITextField *emergencyFirstNameTextField;
@property (nonatomic,retain) UITextField *emergencyLastNameTextField;
@property (nonatomic,retain) UITextField *emergencyPostCodeTextField;
@property (nonatomic,retain) UITextField *emergencyHouseStreetTextField;
@property (nonatomic,retain) UITextField *emergencyAreaTextField;
@property (nonatomic,retain) UITextField *emergencyTownTextField;
@property (nonatomic,retain) UITextField *emergencyCountryTextField;
@property (nonatomic,retain) UITextField *emergencyTelephoneTextField;
@property (nonatomic,retain) UITextField *emergencyMobileTextField;
@property (nonatomic,retain) UITextField *emergencyPlaceOfWorkTextField;
@property (nonatomic,retain) UITextField *emergencyWorkTelephoneTextField;

@property (nonatomic,retain) UITextField *doctorsNameTextField;
@property (nonatomic,retain) UITextField *doctorsPhoneTextField;

//vaccinations
@property (nonatomic,retain) CheckSwitch *diphteriaCheckSwitch;
@property (nonatomic,retain) CheckSwitch *measlesCheckSwitch;
@property (nonatomic,retain) CheckSwitch *polioCheckSwitch;
@property (nonatomic,retain) CheckSwitch *tetanusCheckSwitch;
@property (nonatomic,retain) CheckSwitch *meningitisCheckSwitch;
@property (nonatomic,retain) CheckSwitch *mumpsCheckSwitch;
@property (nonatomic,retain) CheckSwitch *rubellaCheckSwitch;
@property (nonatomic,retain) CheckSwitch *whoopingCoughCheckSwitch;

@property (nonatomic,retain) UIButton *addPhotoButton;
@property (nonatomic,retain) UIButton *saveButton;

//properties
@property (nonatomic,retain)NSString *religion;
@property (nonatomic,retain)NSString *ethnicOrigin;
@property (nonatomic,retain)NSNumber *day, *month, *year,*gender,*backDayProgress,*roomId,*roomMoveId;
@property (nonatomic,retain)NSDate *startingDate, *finishedDate,*moveDate;



-(IBAction)findAddressForChild:(id)sender;
-(IBAction)findAddressForParent1:(id)sender;
-(IBAction)copyChildsAddressForParent1:(id)sender;

-(IBAction)addParentOrCarer:(id)sender;

-(IBAction)findAddressForParent2:(id)sender;
-(IBAction)copyChildsAddressForParent2:(id)sender;

-(IBAction)findAddressForEmergencyContact:(id)sender;

-(IBAction)uploadPhoto:(id)sender;

-(IBAction)saveButtonPressed:(id)sender;

@end
