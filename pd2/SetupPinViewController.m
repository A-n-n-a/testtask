//
//  PincodeViewController.m
//  pd2
//
//  Created by i11 on 22/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "SetupPinViewController.h"
#import "UserManager.h"
#import "User.h"
#import <AudioToolbox/AudioToolbox.h>

#import "SetupPinLogoCell.h"

#import "VPBiometricAuthenticationFacade.h"
#import <LocalAuthentication/LocalAuthentication.h>

#import "PSTAlertController.h"
#import "WL.h"

#define PIN_DOTS_VIEW_TAG 100
#define TITLE_LABEL_TAG 150

NSString * const kPDSetupPinLogo = @"kPDLogo";
NSString * const kPDSetupPinTitle = @"kPDTitle";
NSString * const kPDSetupPinDots = @"kPDPinDots";
NSString * const kPDSetupPinPad = @"kPDPinPad";
NSString * const kPDSetupPinActionButtons = @"kPDActionButtons";

@interface SetupPinViewController ()

@property (weak, nonatomic) UITableViewCell *pinPadCell;

@property (weak, nonatomic) UILabel *titleLabel;
@property (weak, nonatomic) UIView *pinDotsView;
@property (strong, nonatomic) NSMutableArray *pinButtons;



@property (strong, nonatomic) NSMutableString *enteredPinCode;
@property (strong, nonatomic) NSString *pinCode;

@property (assign, nonatomic) int buttonsPressedCount;
@property (assign, nonatomic) int failedAttempts;
@property (assign, nonatomic) BOOL pinConfirmation;


@property (strong, nonatomic) NSMutableArray *rows;



@end

//TODO: Проверить пятно tap'а у skip и cancel

@implementation SetupPinViewController


- (void)loadView {
    [super loadView];
    
    
    
    self.rows = [NSMutableArray arrayWithObjects:
                 [NSMutableDictionary dictionaryWithObject:kPDSetupPinLogo forKey:@"type"],
                 [NSMutableDictionary dictionaryWithObject:kPDSetupPinTitle forKey:@"type"],
                 [NSMutableDictionary dictionaryWithObject:kPDSetupPinDots forKey:@"type"],
                 [NSMutableDictionary dictionaryWithObject:kPDSetupPinPad forKey:@"type"],
                 [NSMutableDictionary dictionaryWithObject:kPDSetupPinActionButtons forKey:@"type"],
                 nil];

    if (!self.user) {
        self.user = [[UserManager sharedManager] loggedUser];
    }
    
    [self disableInactiveTimer];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.enteredPinCode = [[NSMutableString alloc] init];
    self.pinCode = @"";
    self.pinConfirmation = NO;
    
    [self updateScroll];
    
    self.view.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
    
    //[self runColorCheck:YES];
    
    [[UserManager sharedManager] debugPrintUsers];

}



- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
- (void)setColorTheme
{
    [super setColorTheme];
    
    for (NSDictionary *dict in self.rows) {
        
        UITableViewCell *cell = [dict objectForKey:@"cell"];
        NSString *type = [dict objectForKey:@"type"];
        
        //change cells background
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        
        //change buttons background
        if ([type isEqualToString:kPDSetupPinPad]) {
            UITableViewCell *cell = [dict objectForKey:@"cell"];
            for (int i = 0; i <= 9; i++)
            {
                NSInteger tag = i + 100;
                UIButton *button = (UIButton *)[cell viewWithTag:tag];
                button.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
            }
        }
    }
    
    
}
*/


- (void)updateScroll {
    
    //Обязательно вызываем, иначе contentSize.height = 0
    [self.tableView layoutIfNeeded];
    
    NSLog(@"self.tableView.contentSize.height = %f", self.tableView.contentSize.height);
    NSLog(@"self.tableView.frame.size.height = %f", self.tableView.frame.size.height);
    //TODO: add method to parent class ?
    //Найти способ автоматически вызывать этот метод при изменении размеров таблицы
    //Отслеживать редактируемый textField, если textField скрывается за клавиатурой - разрешать scroll
    //Отслеживать table animation
    
    if (self.tableView.contentSize.height < self.tableView.frame.size.height) {
        self.tableView.scrollEnabled = NO;
    } else {
        self.tableView.scrollEnabled = YES;
    }}


- (void)runColorCheck:(BOOL)bo {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        bo ? [[MyColorTheme sharedInstance]setBlueTheme] : [[MyColorTheme sharedInstance]setYellowTheme];
        [self setColorTheme];
        [self runColorCheck:!bo];
        
    });
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *row = [[self.rows objectAtIndex:indexPath.row] objectForKey:@"type"];
    
    if ([row isEqualToString:kPDSetupPinLogo]) {
        return 73.f;
    }
    else if ([row isEqualToString:kPDSetupPinTitle]) {
        return 41.f;
    }
    else if ([row isEqualToString:kPDSetupPinDots]) {
        return 39.f;
    }
    else if ([row isEqualToString:kPDSetupPinPad]) {
        return 372.f;
    }
    else if ([row isEqualToString:kPDSetupPinActionButtons]) {
        return 25.f;
    }
    else {
        return 0.f;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.rows count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *rowType = [[self.rows objectAtIndex:indexPath.row] objectForKey:@"type"];
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    if ([rowType isEqualToString:kPDSetupPinLogo]) {
        cell = [self configureLogoCellForTableView:tableView indexPath:indexPath];
    } else if ([rowType isEqualToString:kPDSetupPinTitle]) {
        cell = [self configureTitleCellForTableView:tableView indexPath:indexPath];
    } else if ([rowType isEqualToString:kPDSetupPinDots]) {
        cell = [self configurePinDotsCellForTableView:tableView indexPath:indexPath];
    } else if ([rowType isEqualToString:kPDSetupPinPad]) {
        cell = [self configurePinPadCellForTableView:tableView indexPath:indexPath];
    } else if ([rowType isEqualToString:kPDSetupPinActionButtons]) {
        cell = [self configureActionButtonsCellForTableView:tableView indexPath:indexPath];
    }
    
    [self setCell:cell forRow:indexPath.row];
    
    return cell;
}

- (UITableViewCell *)configureLogoCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    SetupPinLogoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"logoCell"];
    
    if (cell)
    {
        cell.logoTop.constant = [WL logoSetupPinRect].origin.y;
        cell.logoWidth.constant = [WL logoSetupPinRect].size.width;
        cell.logoHeight.constant = [WL logoSetupPinRect].size.height;
        cell.logoImageView.image = [UIImage imageNamed:[WL logoSetupPinFileName]];

        cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
    }
    
    
    return cell;
}

- (UITableViewCell *)configureTitleCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"titleCell"];
    
    if (cell)
    {
        self.titleLabel = (UILabel *)[cell viewWithTag:TITLE_LABEL_TAG];
        self.titleLabel.text = [NSString stringWithFormat:@"Setup Pin for %@", [self.user.userName capitalizedString]];
        cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
    }
    
    return cell;
}

- (UITableViewCell *)configurePinDotsCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pinDotsCell"];
    
    if (cell)
    {
        self.pinDotsView = [cell viewWithTag:PIN_DOTS_VIEW_TAG];
        
        for (NSInteger tag = 1; tag <= 4; tag++)
        {
            UIView *view = [self.pinDotsView viewWithTag:tag];
            [view setBackgroundColor:[UIColor clearColor]];
            [view.layer setBorderColor:[[MyColorTheme sharedInstance] loginTextColor].CGColor];
            [view.layer setBorderWidth:1.0f];
            CGFloat radius = view.frame.size.width / 2;
            [view.layer setCornerRadius:radius];
        }
        cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
    }
    
    return cell;
}

- (UITableViewCell *)configurePinPadCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pinPadCell"];
    
    if (cell)
    {
        self.pinPadCell = cell;
        
        for (int i = 0; i <= 9; i++)
        {
            NSInteger tag = i + 100;
            UIButton *button = (UIButton *)[cell viewWithTag:tag];
            [button setBackgroundColor:[[MyColorTheme sharedInstance] loginBackgroundColor]];
            [button.layer setBorderColor:[UIColor whiteColor].CGColor];
            //[button.layer setBorderWidth:1.1f];
            [button.layer setBorderWidth:.0f];
            CGFloat radius = button.frame.size.width / 2;
            [button.layer setCornerRadius:radius];
            
            int viewTag = i + 10;
            UIView *view = [self.pinPadCell viewWithTag:viewTag];
            UILabel *digitLabel = (UILabel *)[view viewWithTag:20];
            digitLabel.textColor = [[MyColorTheme sharedInstance] loginTextColor];
        }
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
    }
    
    return cell;
}

- (UITableViewCell *)configureActionButtonsCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"actionButtonsCell"];
    
    if (cell)
    {
        cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
    }
    
    return cell;
}

- (IBAction)pinButtonTouchUpInside:(UIButton *)sender {
    
    [self pinPadUserInteractionEnable:NO];
    
    int digit = (int)[sender tag] - 100;
    
    NSLog(@"touch button = %i", digit);
    
    //Play KeyPressed sq_tock.caf
    //AudioServicesPlaySystemSound(1105);
    
    int viewTag = digit + 10;
    
    UIView *view = [self.pinPadCell viewWithTag:viewTag];
    UILabel *digitLabel = (UILabel *)[view viewWithTag:20];
    UILabel *alphabeticalLabel = (UILabel *)[view viewWithTag:30];
    
    //[self hightLightView:view];
    [self hightLightView:digitLabel];
    //[self hightLightView:alphabeticalLabel];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //[self unHightLightView:view];
        [self unHightLightView:digitLabel];
        //[self unHightLightView:alphabeticalLabel];
    });
    
    
    self.buttonsPressedCount++;
    [self.enteredPinCode appendString:[NSString stringWithFormat:@"%i", digit]];
    
    UIView *pinDotView = [self.pinDotsView viewWithTag:self.buttonsPressedCount];
    [pinDotView setBackgroundColor:[[MyColorTheme sharedInstance] loginTextColor]];
    
    if (_buttonsPressedCount == 4)
    {
        [self checkPinCode];
    } else {
        [self pinPadUserInteractionEnable:YES];
    }
}

- (IBAction)cancelAction:(id)sender {
    
    //NSLog(@"pin = %@", self.enteredPinCode);
    NSLog(@"length = %lu", (unsigned long)[self.enteredPinCode length]);
    NSLog(@"buttonsPressedCount = %i", self.buttonsPressedCount);
    
    if ([self.enteredPinCode length] > 0)
    {
        
        UIView *pinDotView = [self.pinDotsView viewWithTag:self.buttonsPressedCount];
        [pinDotView setBackgroundColor:[UIColor clearColor]];
        
        NSRange range = NSMakeRange([self.enteredPinCode length] - 1, 1);
        [self.enteredPinCode deleteCharactersInRange:range];
        
        self.buttonsPressedCount--;
    }
    
}

- (IBAction)skipAction:(UIButton *)sender {

    [self setupTouchIdOrShowMenu];
}

- (void)setupTouchIdOrShowMenu {
    
    
    VPBiometricAuthenticationFacade *biometricFacede = [[VPBiometricAuthenticationFacade alloc] init];
    
    BOOL isNeedSetupTouchId = [biometricFacede isAuthenticationAvailable] && ![UserManager sharedManager].isUsersWithTouchId && [[UserManager sharedManager] loggedUser].isNewUser;
    BOOL isFirstLaunchAfterUpdate = [biometricFacede isAuthenticationAvailable] && ![UserManager sharedManager].isUsersWithTouchId && [AppDelegate isFirstLaunchAfterTouchIdUpdate];
    //isNeedSetupTouchId = NO;
    //isFirstLaunchAfterUpdate = NO;
    if (isNeedSetupTouchId || isFirstLaunchAfterUpdate) {
        
        PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:[Utils isFaceIdSupported] ? @"Face ID" : @"Touch ID" message:[Utils isFaceIdSupported] ? @"Do you want to enable Face ID?" : @"Do you want to enable Touch ID?" preferredStyle:PSTAlertControllerStyleAlert];
        
        [alert addAction:[PSTAlertAction actionWithTitle:@"Yes" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
            
            [self setupTouchId];
            
            
        }]];
        
        [alert addAction:[PSTAlertAction actionWithTitle:@"Skip" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
            //User.skip
            [self showMenu];
            
            
        }]];
        
        [alert showWithSender:self controller:self animated:YES
                   completion:nil];
        [AppDelegate appLaunchedAfterTouchIdUpdate];
        
    }
    else {
        [self showMenu];
    }
    
}


- (void)setupTouchId {
    
    LAContext *context = [[LAContext alloc] init];
    context.localizedFallbackTitle = @"";
    
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:[Utils isFaceIdSupported] ? @"Unlock using Face ID" : @"Touch the Home button to login"
                          reply:^(BOOL success, NSError *error) {
                              
                              if (error) {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      
                                      PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Error" message:[Utils isFaceIdSupported] ? @"Face ID authentication failed" : @"Touch ID authentication failed" preferredStyle:PSTAlertControllerStyleAlert];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Retry" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          
                                          [self setupTouchId];
                                          
                                      }]];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Skip" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          [self showMenu];
                                          
                                      }]];
                                      
                                      [alert showWithSender:nil controller:self animated:YES
                                                 completion:nil];
                                  });
                                  return;
                                  
                              }
                              
                              if (success) {
                                  
                                  User *user = [[UserManager sharedManager] lastLoggedUser];
                                  user.touchIdToken = [AppDelegate randomString];
                                  
                                  
                                  [[NetworkManager sharedInstance] createTouchIdToken:user.touchIdToken forUser:user.uid isParent:user.userType == UserTypeParent ? YES : NO withCompletion:^(id response, NSError *error) {
                                      
                                      if (response)
                                      {
                                          if([[response valueForKeyPath:user.userType == UserTypeAdmin ? @"result.complete" : @"data.result.complete"]intValue]==1)
                                          {
                                              user.isTouchId = YES;
                                              [self showMenu];
                                          }
                                          else
                                          {
                                              Alert(@"Error",[response valueForKeyPath:user.userType == UserTypeAdmin ? @"result.msg" : @"data.result.msg"] );
                                              [self showMenu];
                                              
                                          }
                                      }
                                      else if(error)
                                      {
                                          Alert(@"Error", @"Error happened while auth");
                                          [self showMenu];
                                      }
                                  }];
                                  
                              } else {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      
                                      PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Error" message:@"You are not the device owner." preferredStyle:PSTAlertControllerStyleAlert];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Retry" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          
                                          [self setupTouchId];
                                          
                                      }]];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Skip" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          [self showMenu];
                                          
                                      }]];
                                      
                                      [alert showWithSender:nil controller:self animated:YES
                                                 completion:nil];
                                      
                                  });
                              }
                              
                          }];
        
    }/* else {
      
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
      message:@"Your device cannot authenticate using TouchID."
      delegate:nil
      cancelButtonTitle:@"Ok"
      otherButtonTitles:nil];
      [alert show];
      
      }
      */
}



- (void)showMenu {
    
    if ([[UserManager sharedManager] loggedUser].userType == UserTypeAdmin) {
        [self performSegueWithIdentifier:@"@Main_iPhone" sender:self];
    } else {
        [self performSegueWithIdentifier:@"@Main-ParentSide_iPhone" sender:self];
    }
    
}

- (void)checkPinCode
{
    
    if (self.pinConfirmation) {
        
        if ([self.enteredPinCode isEqualToString:self.pinCode])
        {
            
            [[NetworkManager sharedInstance] createPin:self.pinCode forUser:self.user.uid withCompletion:^(id response, NSError *error) {
                if (response)
                {
                    if([[response valueForKeyPath:@"result.complete"]intValue]==1)
                    {
                        [self setupTouchIdOrShowMenu];
                    }
                    else
                    {
#warning предусмотреть действие на получение неизвестной ошибки
                        Alert(@"Error",[response valueForKeyPath:@"result.msg"] );
                    }
                }

            }];
       
        } else {
            
            self.titleLabel.text = [NSString stringWithFormat:@"Setup Pin for %@", self.user.userName];
            [self.enteredPinCode setString:@""];
            self.pinCode = @"";
            self.pinConfirmation = NO;
            
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            
            self.buttonsPressedCount = 0;
            
            [self animatePinDotsView];
            
            [self performSelector:@selector(updatePinDotsView) withObject:nil afterDelay:0.5];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self pinPadUserInteractionEnable:YES];
            });
        }
        
        
    } else {
        
        self.titleLabel.text = [NSString stringWithFormat:@"Confirm Pin for %@", [self.user.userName capitalizedString]];
        self.pinCode = [self.enteredPinCode copy];
        [self.enteredPinCode setString:@""];
        self.buttonsPressedCount = 0;
        self.pinConfirmation = YES;
        [self performSelector:@selector(updatePinDotsView) withObject:nil afterDelay:0.2];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self pinPadUserInteractionEnable:YES];
        });
    }
    
}

- (void)updatePinDotsView
{
    for (NSInteger tag = 1; tag <= 4; tag++)
    {
        UIView *view = [self.pinDotsView viewWithTag:tag];
        [view setBackgroundColor:[UIColor clearColor]];
    }
}

- (void)animatePinDotsView
{
    CABasicAnimation *animation =
    [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.05];
    [animation setRepeatCount:3];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([self.pinDotsView center].x - 15.0f, [self.pinDotsView center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([self.pinDotsView center].x + 15.0f, [self.pinDotsView center].y)]];
    [self.pinDotsView.layer addAnimation:animation forKey:@"position"];
}

- (void)hightLightView:(UIView *)view {
    view.layer.shadowColor = [[UIColor whiteColor] CGColor];
    view.layer.shadowRadius = 4.0f;
    view.layer.shadowOpacity = .9;
    view.layer.shadowOffset = CGSizeZero;
    view.layer.masksToBounds = NO;
}

- (void)unHightLightView:(UIView *)view {
    view.layer.shadowRadius = 0.f;
    view.layer.shadowOpacity = 1;
}


- (void)pinPadUserInteractionEnable:(BOOL)value {
    
    for (int i = 0; i <= 9; i++)
    {
        NSInteger tag = i + 100;
        UIButton *button = (UIButton *)[self.pinPadCell viewWithTag:tag];
        button.userInteractionEnabled = value;
    }
    
}


//Для setColorTheme необходимо ханить ссылки на ячейки
- (void)setCell:(UITableViewCell *)cell forRow:(NSInteger)row {
    //__weak или rows все равно delete при потере актуальности?
    [[self.rows objectAtIndex:row] setObject:cell forKey:@"cell"];
}

@end
