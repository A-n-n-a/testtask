//
//  NSDate+Printable.m
//  pd2
//
//  Created by Andrey on 09/01/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "NSDate+Printable.h"
#import "NSDate+DayWithSuffix.h"
#import "NSDateFormatter+Locale.h"
#import "NSDate+Utilities.h"

#define checkDateBOOL YES

@implementation NSDate (Printable)


- (NSString *)reallyDefault {
    
    //1st Jan 2017
    NSDateFormatter *shortMonthFormatter = [[NSDateFormatter alloc] init];
    [shortMonthFormatter setDateFormat:@"LLL"];
    
    NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
    [yearFormatter setDateFormat:@"yyyy"];
    
    if (checkDateBOOL) {
        return [NSString stringWithFormat:@"!%@ %@ %@!", [self dayWithSuffix], [shortMonthFormatter stringFromDate:self],[yearFormatter stringFromDate:self]];
    }
    else {
        return [NSString stringWithFormat:@"%@ %@ %@", [self dayWithSuffix], [shortMonthFormatter stringFromDate:self],[yearFormatter stringFromDate:self]];
    }
}


- (NSString *)reallyDefaultTime {
    
    //15:15
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (checkDateBOOL) {
        [dateFormatter setDateFormat:@"!HH:mm!"];
    }
    else {
        [dateFormatter setDateFormat:@"HH:mm"];
    }
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    return [dateFormatter stringFromDate:self];
    
}


- (NSString *)reallyDefaultWithTime {
    
    //1st Jan 2017 @ 10:10
    return [NSString stringWithFormat:@"%@ @ %@", [self reallyDefault], [self reallyDefaultTime]];
}


- (NSString *)reallyDefaultWithWeekDay {
    
    //Thu, 1st Jan 2017
    NSDateFormatter *shortMonthFormatter = [[NSDateFormatter alloc] init];
    [shortMonthFormatter setDateFormat:@"LLL"];
    
    NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
    [yearFormatter setDateFormat:@"yyyy"];
    
    NSDateFormatter *weekDayFormatter = [[NSDateFormatter alloc] init];
    [weekDayFormatter setDateFormat:@"ccc"];
    
    if (checkDateBOOL) {
        return [NSString stringWithFormat:@"!%@, %@ %@ %@!", [weekDayFormatter stringFromDate:self], [self dayWithSuffix], [shortMonthFormatter stringFromDate:self],[yearFormatter stringFromDate:self]];
    }
    else {
        return [NSString stringWithFormat:@"%@, %@ %@ %@", [weekDayFormatter stringFromDate:self], [self dayWithSuffix], [shortMonthFormatter stringFromDate:self],[yearFormatter stringFromDate:self]];
    }
}


- (NSString *)reallyDefaultMonthYear {
    
    //
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (checkDateBOOL) {
        [dateFormatter setDateFormat:@"!MMM yyyy!"];
    }
    else {
        [dateFormatter setDateFormat:@"MMM yyyy"];
    }
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    return [dateFormatter stringFromDate:self];
    
}





- (NSString *)printableDefault {

    
    /*
    //01.01.2017
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (checkDateBOOL) {
        [dateFormatter setDateFormat:@"!dd.MM.yyyy!"];
    }
    else {
        [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    }
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    return [dateFormatter stringFromDate:self];
    */
    
    return [self reallyDefault];
    
    
}

- (NSString *)printableDefaultWithTime {
    
    
    //return [NSString stringWithFormat:@"%@ @ %@", [self printableDefaultTmp], [self printableDefaultTime]];
    
    return [self reallyDefaultWithTime];
}

- (NSString *)printableDefaultWithAtTime {
    
    
    //return [NSString stringWithFormat:@"%@ at %@", [self printableDefaultTmp], [self printableDefaultTime]];
    return [self reallyDefaultWithTime];
    
}



- (NSString *)printableDefaultInput {
    //return [self printableDefault];
    return [self reallyDefault];
}

- (NSString *)printableDefaultTime {
    /*
    //15:15
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (checkDateBOOL) {
        [dateFormatter setDateFormat:@"!HH:mm!"];
    }
    else {
        [dateFormatter setDateFormat:@"HH:mm"];
    }
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    return [dateFormatter stringFromDate:self];
    */
    return [self reallyDefaultTime];
}

- (NSString *)printableDefaultWithWeekDay {
    /*
    NSDateFormatter *shortMonthFormatter = [[NSDateFormatter alloc] init];
    [shortMonthFormatter setDateFormat:@"LLL"];
    
    NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
    [yearFormatter setDateFormat:@"yyyy"];
    
    NSDateFormatter *weekDayFormatter = [[NSDateFormatter alloc] init];
    [weekDayFormatter setDateFormat:@"cccc"];
    
    if (checkDateBOOL) {
        return [NSString stringWithFormat:@"!%@, %@ %@ %@!", [weekDayFormatter stringFromDate:self], [self dayWithSuffix], [shortMonthFormatter stringFromDate:self],[yearFormatter stringFromDate:self]];
    }
    else {
        return [NSString stringWithFormat:@"%@, %@ %@ %@", [weekDayFormatter stringFromDate:self], [self dayWithSuffix], [shortMonthFormatter stringFromDate:self],[yearFormatter stringFromDate:self]];
    }
    */
    return [self reallyDefaultWithWeekDay];
    
}


- (NSString *)printableDefaultTmp {
    
    //TODO: заменить в коде этот метод на printableDefault
    
    //NSString or nil
    
    /*
     //1st Jan 2017
     NSDateFormatter *shortMonthFormatter = [[NSDateFormatter alloc] init];
     [shortMonthFormatter setDateFormat:@"LLL"];
     
     NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
     [yearFormatter setDateFormat:@"yyyy"];
    
    if (checkDateBOOL) {
        return [NSString stringWithFormat:@"!%@ %@ %@!", [self dayWithSuffix], [shortMonthFormatter stringFromDate:self],[yearFormatter stringFromDate:self]];
    }
    else {
        return [NSString stringWithFormat:@"%@ %@ %@", [self dayWithSuffix], [shortMonthFormatter stringFromDate:self],[yearFormatter stringFromDate:self]];
    }
    */
    return [self reallyDefault];

}


- (NSString *)printableDefaultDayMonth {
    
    //NSString or nil
    
    /*
    //1st Jan
    NSDateFormatter *shortMonthFormatter = [[NSDateFormatter alloc] init];
    [shortMonthFormatter setDateFormat:@"LLL"];
    
    NSDateFormatter *yearFormatter = [[NSDateFormatter alloc] init];
    [yearFormatter setDateFormat:@"yyyy"];
    
    if (checkDateBOOL) {
        return [NSString stringWithFormat:@"!%@ %@ %@!", [self dayWithSuffix], [shortMonthFormatter stringFromDate:self],[yearFormatter stringFromDate:self]];
    }
    else {
        return [NSString stringWithFormat:@"%@ %@ %@", [self dayWithSuffix], [shortMonthFormatter stringFromDate:self],[yearFormatter stringFromDate:self]];
    }
    */
    return [self reallyDefault];

}


//Comments

- (NSString *)printableDefaultTimeDate {
    
    /*
    //01.01.2017
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (checkDateBOOL) {
        [dateFormatter setDateFormat:@"!HH:mm, MMMM d, yyyy!"];
    }
    else {
        [dateFormatter setDateFormat:@"HH:mm, MMMM d, yyyy"];
    }
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    return [dateFormatter stringFromDate:self];
    */
    
    return [self reallyDefaultWithTime];

}

- (NSString *)printableMonthShortYear {
    
    /*
    //01.01.2017
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (checkDateBOOL) {
        [dateFormatter setDateFormat:@"!MMM yyyy!"];
    }
    else {
        [dateFormatter setDateFormat:@"MMM yyyy"];
    }
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    return [dateFormatter stringFromDate:self];
    */
    
    return [self reallyDefaultMonthYear];
}



//Manage - Birthdays
- (NSString *)printableBirthday {
    
    //Sat, 30th December
    
    NSDateFormatter *dayFormatter = [[NSDateFormatter alloc] init];
    [dayFormatter setDateFormat:@"EEE"];
    [dayFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    //[dayFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    
    NSDateFormatter *monthFormatter = [[NSDateFormatter alloc] init];
    [monthFormatter setDateFormat:@"LLLL"];
    [monthFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    if (checkDateBOOL) {
        return [NSString stringWithFormat:@"!%@, %@ %@!", [dayFormatter stringFromDate:[self dateByDate:self]], [self dayWithSuffix], [monthFormatter stringFromDate:self]];
    }
    else {
        return [NSString stringWithFormat:@"%@, %@ %@", [dayFormatter stringFromDate:[self dateByDate:self]], [self dayWithSuffix], [monthFormatter stringFromDate:self]];
    }

    
}


- (NSString *)printableMonthYear {
    /*
    //01.01.2017
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (checkDateBOOL) {
        [dateFormatter setDateFormat:@"!MMMM yyyy!"];
    }
    else {
        [dateFormatter setDateFormat:@"MMMM yyyy"];
    }
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    return [dateFormatter stringFromDate:self];
    */
    return [self reallyDefaultMonthYear];
}



- (NSString *)printableMonth {
    
    //01.01.2017
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (checkDateBOOL) {
        [dateFormatter setDateFormat:@"!MMMM!"];
    }
    else {
        [dateFormatter setDateFormat:@"MMMM"];
    }
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    return [dateFormatter stringFromDate:self];
    
}




//Костыль взят из birthdays
-(NSDate *)dateByDate:(NSDate *)date {
    
    NSCalendar *calendar = [[NSCalendar alloc]
                            initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSCalendarUnit unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    
    //gather date components from date
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:date];
    NSDateComponents *todayComponents = [calendar components:unitFlags fromDate:[NSDate date]];
    
    //set date components
    [dateComponents setDay:[dateComponents day]];
    [dateComponents setMonth:[dateComponents month]];
    [dateComponents setYear:[todayComponents year]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //save date relative from date
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [dateFormatter setTimeZone:timeZone];
    
    return [calendar dateFromComponents:dateComponents];
    
    
}

//permission & policies
//June 15th, 2016 at 1:18pm

- (NSString *)printablePermissionsSign {
    /*
    NSDateFormatter *startDateFormatter = [[NSDateFormatter alloc] init];
    [startDateFormatter setDateFormat:@"LLLL"];
    
    NSDateFormatter *centerDateFormatter = [[NSDateFormatter alloc] init];
    [centerDateFormatter setDateFormat:@"yyyy"];
    
    NSDateFormatter *endDateFormatter = [[NSDateFormatter alloc] init];
    [endDateFormatter setDateFormat:@"h:mm a"];
    
    
    if (checkDateBOOL) {
        return [NSString stringWithFormat:@"!%@ %@, %@ at %@!", [startDateFormatter stringFromDate:self], [self dayWithSuffix], [centerDateFormatter stringFromDate:self], [[endDateFormatter stringFromDate:self] lowercaseString]];
    }
    else {
        return [NSString stringWithFormat:@"%@ %@, %@ at %@", [startDateFormatter stringFromDate:self], [self dayWithSuffix], [centerDateFormatter stringFromDate:self], [[endDateFormatter stringFromDate:self] lowercaseString]];
    }
    */
    return [self reallyDefaultWithTime];

}


//Medications

- (NSString *)printableWithSlaches
{
    /*
    // 01/01/2017
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (checkDateBOOL) {
        [dateFormatter setDateFormat:@"!dd/MM/yyyy!"];
    }
    else {
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    }
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    return [dateFormatter stringFromDate:self];
    */
    return [self reallyDefault];

}


- (NSString *)printableWithDateSlachesTime
{
    /*
    // 09/11/2017, 12:31
    if (checkDateBOOL) {
        return [NSString stringWithFormat:@"!%@, %@!", [self printableWithSlaches], [self printableDefaultTime]];
    }
    else {
        return [NSString stringWithFormat:@"%@, %@", [self printableWithSlaches], [self printableDefaultTime]];
    }
    */
    return [self reallyDefaultWithTime];

}

//NS Plan 
- (NSString *)printableDayMonth {
    
        //1st Jan
        NSDateFormatter *shortMonthFormatter = [[NSDateFormatter alloc] init];
        [shortMonthFormatter setDateFormat:@"LLL"];
        
    
        if (checkDateBOOL) {
            return [NSString stringWithFormat:@"!%@ %@!", [self dayWithSuffix], [shortMonthFormatter stringFromDate:self]];
        }
        else {
            return [NSString stringWithFormat:@"%@ %@", [self dayWithSuffix], [shortMonthFormatter stringFromDate:self]];
        }
    
}


+ (NSString *)printableShortMonthForIndex:(int)monthNum {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] initWithSafeLocale];
    NSArray *months = dateFormatter.shortMonthSymbols;
    
    
    int reallyMonthIndex = monthNum - 1;
    
    if ([months count] <= reallyMonthIndex) {
        return @"N/A";
    }
    
    NSString *monthName = [months objectAtIndex:reallyMonthIndex];
    
    if (checkDateBOOL) {
        monthName = [NSString stringWithFormat:@"!%@!", monthName];
    }
    
    return monthName;
}


+ (NSString *)printableMonthForIndex:(int)monthNum {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] initWithSafeLocale];
    NSArray *months = dateFormatter.monthSymbols;
    
    
    int reallyMonthIndex = monthNum - 1;
    
    if ([months count] <= reallyMonthIndex) {
        return @"N/A";
    }
    
    NSString *monthName = [months objectAtIndex:reallyMonthIndex];
    
    if (checkDateBOOL) {
        monthName = [NSString stringWithFormat:@"!%@!", monthName];
    }
    
    return monthName;
}



- (NSString *)printableRegisterDayMonthYear {
    
    
    /*
    //1st Jan
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] initWithSafeLocale];
    [dateFormatter setDateFormat:@"cccc, dd.MM.yyyy"];
    
    
    if (checkDateBOOL) {
        return [NSString stringWithFormat:@"!%@!",[dateFormatter stringFromDate:self]];
    }
    else {
        return [dateFormatter stringFromDate:self];
    }
    */
    
    return [self reallyDefaultWithWeekDay];
}


+ (NSString *)printableRegisterIntervalForStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    
    NSDateFormatter *monthDateFormatter = [[NSDateFormatter alloc] initWithSafeLocale];
    [monthDateFormatter setDateFormat:@"MMM"];
    
    NSString *string;
    
    if ([startDate month] == [endDate month]) {
        string = [NSString stringWithFormat:@"%@ to %@ %@ %i",
                           [startDate dayWithSuffix],
                           [endDate dayWithSuffix],
                           [monthDateFormatter stringFromDate:startDate],
                           (int)[endDate year]];
    }
    else {
        string = [NSString stringWithFormat:@"%@ %@ to %@ %@ %i",
                           [startDate dayWithSuffix],
                           [monthDateFormatter stringFromDate:startDate],
                           [endDate dayWithSuffix],
                           [monthDateFormatter stringFromDate:endDate],
                           (int)[endDate year]];
    }
    
    if (checkDateBOOL) {
        return [NSString stringWithFormat:@"!%@!",string];
    }
    else {
        return string;
    }
}



- (void)testDateFormates {
    
    
    NSLog([self printableDefault]);
    NSLog([self printableDefaultWithTime]);
    NSLog([self printableDefaultWithAtTime]);
    NSLog([self printableMonthShortYear]);
    NSLog([self printableDefaultTime]);
    NSLog([self printableDefaultWithWeekDay]);
    NSLog([self printableDefaultTmp]);
    NSLog([self printableDefaultDayMonth]);
    NSLog([self printableDefaultTimeDate]);
    NSLog([self printableMonthShortYear]);
    NSLog([self printableBirthday]);
    NSLog([self printableMonthYear]);
    NSLog([self printableMonth]);
    NSLog([self printablePermissionsSign]);
    NSLog([self printableWithSlaches]);
    NSLog([self printableWithDateSlachesTime]);
    NSLog([self printableDefaultDayMonth]);
}



@end
