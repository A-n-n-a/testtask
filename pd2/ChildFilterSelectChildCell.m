//
//  ChildFilterSelectChildCell.m
//  pd2
//
//  Created by Andrey on 27/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ChildFilterSelectChildCell.h"

@implementation ChildFilterSelectChildCell

- (void)update {
    
    
    self.childIconImageView.image = [[UIImage imageNamed:[NetworkManager sharedInstance].isJ3 ? @"j3_noCirc_spoiler_children_.png" : @"activeChildrenIcon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.childIconImageView.tintColor = [UIColor darkGrayColor];
}


@end
