//
//  DailyNotesChildrenNoteCell.m
//  pd2
//
//  Created by Andrey on 30.03.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "DailyNotesChildrenNoteCell.h"
#import "DailyNotesChildrenNoteModel.h"

@implementation DailyNotesChildrenNoteCell

- (void)updateWithModel:(DailyNotesChildrenNoteModel *)model {
    
    
    self.nameLabel.text = notAvailStringIfNil([model.noteTitle uppercaseFirstLetter]);
    self.adminNameLabel.text = [self fullNameFromFirstName:model.adminFirstName andLastName:model.adminLastName];
    self.dateLabel.text = notAvailStringIfNil([model.date printableDefault]);
    self.commentsLabel.text = [NSString stringWithFormat:@"%d", model.commentsCount];
    
}


@end
