//
//  BackgroundColorView.m
//  pd2
//
//  Created by User on 14.01.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "BackgroundColorView.h"

@implementation BackgroundColorView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    self.backgroundColor=[[MyColorTheme sharedInstance]backgroundColor];
}

@end
