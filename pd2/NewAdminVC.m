//
//  NewAdminVC.m
//  pd2
//
//  Created by User on 17.07.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "NewAdminVC.h"
#import "ImageCache.h"
#import "NoteView.h"

#import "PECropViewController.h"

@interface NewAdminVC () <PECropViewControllerDelegate>

@property    int chosenAccessLevel;
@property    int chosenPositionId;
@property    int chosenRoomNumber;

@property int selectedLevelIndex, selectedRoomIndex, selectedPositionIndex;

@end

@implementation NewAdminVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tfRoom.enabled=NO;
    [(NoteView*)_infoText.superview setTopView:true];
    if (self.editMode)
    {
        NSLog(@"edit admin with dict:%@",_infoDict);
        _tfUsername.text=_infoDict[@"username"];
        _tfFirstName.text=_infoDict[@"first_name"];
        _tfLastName.text=_infoDict[@"last_name"];
        _tfEmail.text=_infoDict[@"email"];
        
        NSNumber *accLev=_infoDict[@"acl"];
        _tfLevel.text=[[DataManager sharedInstance]getAdminLevelTitleById:accLev.intValue];
        _selectedLevelIndex=accLev.intValue-1;
        
        NSNumber *positionNum=_infoDict[@"position"];
        NSString *positionStr=[[DataManager sharedInstance]getPositionTitleById:positionNum.intValue];
        _tfPosition.text=positionStr;
        
        int index=(int)[[[DataManager sharedInstance]positionList] indexOfObjectIdenticalTo:positionStr];
        
        _selectedPositionIndex=index;
        
        NSNumber *roomNum=_infoDict[@"room"];
        if (accLev.intValue==1||accLev.intValue==2||accLev.intValue==5)
        {
            _tfRoom.text=NSLocalizedString(@"as.admin.addEdit.accLev.allRooms", nil);
        }
        else
        {
            NSString *roomNum=_infoDict[@"title"];
            if ([roomNum isKindOfClass:[NSNull class]]) {
                roomNum = @"";
            }
            _tfRoom.text = roomNum;
        }
        _addPhotoButton.layer.borderWidth=0.5;
        _addPhotoButton.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        _addPhotoButton.layer.cornerRadius=38;
        _addPhotoButton.layer.masksToBounds=YES;
        
        [[Utils sharedInstance]downloadImageAtRelativePath:_infoDict[@"image"] completionHandler:^(UIImage *image) {
            [_addPhotoButton setBackgroundImage:image  forState:UIControlStateNormal];
        }];
        
        //labels
        [_saveButton setTitle:NSLocalizedString(@"as.admin.addEdit.saveTitle", nil) forState:UIControlStateNormal];
        NSAttributedString *str=[[NSAttributedString alloc]initWithString:[NSString stringWithFormat:NSLocalizedString(@"as.admin.addEdit.infoText.edit", nil),_infoDict[@"first_name"],_infoDict[@"last_name"]]];
        _infoText.attributedText=str;
        [_infoText makeFirstLineBold];
        
        
        NSAttributedString *str1=[[NSAttributedString alloc]initWithString:[NSString stringWithFormat:NSLocalizedString(@"as.admin.addEdit.infoText.info", nil),_infoDict[@"first_name"],_infoDict[@"last_name"]]];
        self.secondTextView.attributedText = str1;
        self.secondTextView.textColor = [MyColorTheme sharedInstance].textViewTextColor;
        
        
        _chosenAccessLevel=accLev.intValue;
        _chosenPositionId=positionNum.intValue;
        _chosenRoomNumber=roomNum.intValue;
        if (accLev.intValue==3)
        {
            _tfRoom.enabled=YES;
        }
    }
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 300)];
    titleLabel.text = _editMode?NSLocalizedString(@"as.admin.addEdit.editTitle", nil):NSLocalizedString(@"as.admin.addEdit.newTitle", nil);
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont boldSystemFontOfSize:23];
    self.navigationItem.titleView=titleLabel;
    
    _addPhotoButton.layer.borderWidth=0.5;
    _addPhotoButton.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    _addPhotoButton.layer.cornerRadius=38;
    _addPhotoButton.layer.masksToBounds=YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height), 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width), 0.0);
    }
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    self.tableView.contentInset = UIEdgeInsetsZero;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}


-(void)viewWillDisappear:(BOOL)animated
{
    [self.picker hide];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (bool)allFieldsFilled
{
    bool filled= _tfUsername.text.length&&
    _tfFirstName.text.length&&
    _tfLastName.text.length&&
    _tfEmail.text.length&&
    _tfLevel.text.length&&
    _tfPosition.text.length;
    
    NSArray *requiredFields = @[_tfUsername, _tfFirstName, _tfLastName, _tfEmail, _tfLevel, _tfPosition];
    NSArray *nameFields = @[NSLocalizedString(@"as.admin.addEdit.nameFields.username", nil), NSLocalizedString(@"as.admin.addEdit.nameFields.firstName", nil), NSLocalizedString(@"as.admin.addEdit.nameFields.lastName", nil), NSLocalizedString(@"as.admin.addEdit.nameFields.email", nil), NSLocalizedString(@"as.admin.addEdit.nameFields.level", nil), NSLocalizedString(@"as.admin.addEdit.nameFields.position", nil)];
    NSMutableArray *emptyFields = [NSMutableArray new];
    
    //check
    for (int i = 0; i < requiredFields.count; i++) {
        UITextField *tf = [requiredFields objectAtIndex:i];
        if (tf.text.length == 0) {
            [emptyFields addObject:nameFields[i]];
        }
    }
    
    if (emptyFields.count) {
        NSString *alert = [NSString stringWithFormat:NSLocalizedString(@"as.admin.addEdit.alert.notAllFields", nil),[emptyFields componentsJoinedByString:@", "]];
        ErrorAlert(alert);
        return NO;
    }
    
    
    if (!_editMode) {
        if ([_tfPassword1.text isEqualToString:_tfPassword2.text]) {
            if (_tfPassword1.text.length<4) {
                ErrorAlert(NSLocalizedString(@"as.admin.addEdit.alert.password", nil));
                return false;
            }
        }
        else
        {
            ErrorAlert(NSLocalizedString(@"as.admin.addEdit.alert.notMatch", nil));
            return false;
        }
    }
    
    NSLog(@"all fields filled:%d",filled);
    if (self.chosenAccessLevel==3&&_tfRoom.text.length==0) {
        ErrorAlert(NSLocalizedString(@"as.admin.addEdit.alert.chooseRoom", nil));
        return false;
    }
    
    if (![self validateEmail:_tfEmail.text]) {
        ErrorAlert(NSLocalizedString(@"as.admin.addEdit.alert.emailFormat", nil));
        return false;
    }
    
    return true;
}


#pragma mark - Validate Email

- (BOOL)validateEmail:(NSString*)candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}


-(IBAction)saveButtonPressed:(id)sender
{
    if (![self allFieldsFilled])
    {
        return;
    }
    
    
    UIImage *image;
    if (self.adminImage) {
        image = [[Utils sharedInstance]imageScaledToAvatarHiRes:self.adminImage];
    }
    
    if (_editMode){
        [[NetworkManager sharedInstance] updateAdminWithId:[_infoDict[@"uid"] intValue] firstName:_tfFirstName.text lastName:_tfLastName.text email:_tfEmail.text username:_tfUsername.text password:_tfPassword1.text accessLevel:_chosenAccessLevel position:_chosenPositionId room:_chosenRoomNumber image:image completion:^(id response, NSError * error) {
            if ([response[@"result"][@"complete"] intValue] == 1)
            {
                if (_adminImage)
                {
                    [[ImageCache sharedInstance]deleteImageForLink:_infoDict[@"image"]];
                }
                
                NSLog(@"%@",  [DataManager sharedInstance].adminInfo);
                
                [[NetworkManager sharedInstance]getAdminWithId:[DataManager sharedInstance].adminId completion:^(id responseAdmin, NSError *error) {
                    
                    [DataManager sharedInstance].adminInfo= [NSMutableDictionary dictionaryWithDictionary:[responseAdmin valueForKey:@"admin_info"]];
                    
                    NSLog(@"%@",  [DataManager sharedInstance].adminInfo);
                    
                }];
                
                NSLog(@"update admin success:%@",response);
                [self.navigationController popViewControllerAnimated:YES];
                
            }else {
                NSString *emailMessage = [response valueForKeyPath:@"result.messages.email"];
                NSString *usernameMessage = [response valueForKeyPath:@"result.messages.username"];
                NSString *finalMessage = [NSString new];
                
                if ([emailMessage length] > 0 && [usernameMessage length] > 0) {
                    finalMessage = [NSString stringWithFormat:@"%@ \n %@", emailMessage, usernameMessage];
                }else if ([emailMessage length] > 0){
                    finalMessage = emailMessage;
                }else if ([usernameMessage length] > 0){
                    finalMessage = usernameMessage;
                }
                
                ErrorAlert(finalMessage);
            }
        }];
    }else{
        
        [[NetworkManager sharedInstance]registerAdminWithFirstName:_tfFirstName.text lastName:_tfLastName.text email:_tfEmail.text username:_tfUsername.text password:_tfPassword1.text accessLevel:_chosenAccessLevel position:_chosenPositionId room:_chosenRoomNumber image:image completion:^(id response, NSError *error) {
            if (response)
            {
                NSLog(@"register admin success:%@",response);
                NSNumber *n=[response valueForKeyPath:@"result.complete"];
                if (![n boolValue]) {
                    NSString *emailMessage = [response valueForKeyPath:@"result.messages.email"];
                    NSString *usernameMessage = [response valueForKeyPath:@"result.messages.username"];
                    NSString *finalMessage = [NSString new];
                    
                    if ([emailMessage length] > 0 && [usernameMessage length] > 0) {
                        finalMessage = [NSString stringWithFormat:@"%@ \n %@", emailMessage, usernameMessage];
                    }else if ([emailMessage length] > 0){
                        finalMessage = emailMessage;
                    }else if ([usernameMessage length] > 0){
                        finalMessage = usernameMessage;
                    }
                    
                    ErrorAlert(finalMessage);
                }
                else
                {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
            else if(error)
            {
                NSLog(@"register admin fail:%@",error);
                ErrorAlert(error.localizedDescription);
            }
        }];
    }
}

-(IBAction)uploadImage:(id)sender
{
    UIActionSheet *sheet=[[UIActionSheet alloc]initWithTitle: NSLocalizedString(@"as.admin.addEdit.actionSheet.selectPhoto", nil) delegate:self cancelButtonTitle: NSLocalizedString(@"as.admin.addEdit.actionSheet.cancel", nil) destructiveButtonTitle:nil otherButtonTitles: NSLocalizedString(@"as.admin.addEdit.actionSheet.takePhoto", nil), NSLocalizedString(@"as.admin.addEdit.actionSheet.choosePhoto", nil), nil];
    [sheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"actionSheet %d",(int)buttonIndex);
    
    if (buttonIndex==0)//camera
    {
        UIImagePickerController *picker=[[UIImagePickerController alloc]init];
        picker.sourceType=UIImagePickerControllerSourceTypeCamera;
        picker.delegate=self;
        [self.navigationController presentViewController:picker animated:YES completion:nil];
    }
    else if(buttonIndex==1)//library
    {
        UIImagePickerController *picker=[[UIImagePickerController alloc]init];
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate=self;
        [self.navigationController presentViewController:picker animated:YES completion:nil];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"info:%@",info);
    //self.adminImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditorForImage:[info valueForKey:UIImagePickerControllerOriginalImage]];
    }];
    
    
    /*
     NSLog(@"orientation:%ld",_adminImage.imageOrientation);
     
     self.adminImage=[[Utils sharedInstance]imageScaledToAvatar:self.adminImage];
     
     [[Utils sharedInstance]base64StringFromImage:_adminImage];
     
     [self.addPhotoButton setBackgroundImage:_adminImage forState:UIControlStateNormal];
     */
}


- (void)openEditorForImage:(UIImage *)image {
    
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = image;
    
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat length = MIN(width, height);
    controller.keepingCropAspectRatio = YES;
    controller.toolbarHidden = YES;
    /*
     controller.imageCropRect = CGRectMake((width - length) / 2,
     (height - length) / 2,
     length,
     length);
     */
    controller.imageCropRect = CGRectMake((width - length*0.66) / 2,
                                          (height - length) / 2,
                                          length*0.66,
                                          length);
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    [self presentViewController:navigationController animated:YES completion:NULL];
    
    //[navigationController presentViewController:controller animated:YES completion:NULL];
}


#pragma mark - PECropViewControllerDelegate methods

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    [self processForCroppedImage:croppedImage];
    
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller image:(UIImage *)image
{
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
    [self processForCroppedImage:image];
}


- (void)processForCroppedImage:(UIImage *)image {
    
    //self.adminImage = [[Utils sharedInstance]imageScaledToAvatar:self.adminImage]
    //NSLog(@"orientation:%ld",_adminImage.imageOrientation);
    
    //[[Utils sharedInstance]base64StringFromImage:_adminImage];
    
    self.adminImage = image;
    
    [self.addPhotoButton setBackgroundImage:_adminImage forState:UIControlStateNormal];
    
}

#pragma mark - Delegate methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    //[self.view endEditing:YES];
    [self.picker hide];
    
    CGPoint offset=[self.tableView convertPoint:textField.frame.origin fromView:textField.superview];
    [self.tableView setContentOffset:CGPointMake(0, offset.y-50) animated:YES];
    
    
    __weak typeof(self) weakSelf = self;
    if (textField==_tfLevel)
    {
        [self.view endEditing:YES];
        self.picker = [[ListPicker alloc]init];
        self.picker.dataArray = @[NSLocalizedString(@"as.admin.addEdit.picker.master", nil), NSLocalizedString(@"as.admin.addEdit.picker.super", nil), NSLocalizedString(@"as.admin.addEdit.picker.room", nil), NSLocalizedString(@"as.admin.addEdit.picker.bookkeeping", nil)];
        if (textField.text.length)
        {
            NSString *tfText=textField.text;
            NSUInteger index=[self.picker.dataArray indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                if ([(NSString*)obj isEqualToString:tfText])
                {
                    return YES;
                }
                else
                {
                    return NO;
                }
            }];
            if (index==NSNotFound)
            {
                NSLog(@"element not found:(");
            }
            else
            {
                self.picker.selectedIndex=(int)index;
            }
        }
        
        //self.picker.selectedIndex = _selectedLevelIndex;
        [self.picker setHandler:^(NSString * value, int index) {
            
            int acl = INT16_MAX;
            if ([value isEqualToString:NSLocalizedString(@"as.admin.addEdit.picker.master", nil)]) {
                acl = 1;
            }
            else if ([value isEqualToString:NSLocalizedString(@"as.admin.addEdit.picker.super", nil)]) {
                acl = 2;
            }
            else if ([value isEqualToString:NSLocalizedString(@"as.admin.addEdit.picker.room", nil)]) {
                acl = 3;
            }
            else if ([value isEqualToString:NSLocalizedString(@"as.admin.addEdit.picker.bookkeeping", nil)]) {
                acl = 5;
            }
            
            if ([value isEqualToString:NSLocalizedString(@"as.admin.addEdit.picker.room", nil)])
            {
                weakSelf.tfRoom.enabled=YES;
                if (weakSelf.chosenAccessLevel!=acl)
                {
                    weakSelf.tfRoom.text=@"";
                }
            }
            else
            {
                weakSelf.tfRoom.enabled=NO;
                weakSelf.tfRoom.text=NSLocalizedString(@"as.admin.addEdit.accLev.allRooms", nil);
            }
            weakSelf.tfLevel.text=value;
            weakSelf.chosenAccessLevel=acl;
            weakSelf.selectedLevelIndex=index;
        }];
        [self.picker show];
        return NO;
    }
    else if(textField==_tfPosition)
    {
        [self.view endEditing:YES];
        self.picker=[[ListPicker alloc]init];
        NSArray *array=[[DataManager sharedInstance]positionList];
        self.picker.dataArray=array;
        if (textField.text.length)
        {
            NSString *tfText=textField.text;
            NSUInteger index=[self.picker.dataArray indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                if ([(NSString*)obj isEqualToString:tfText])
                {
                    return YES;
                }
                else
                {
                    return NO;
                }
            }];
            if (index==NSNotFound)
            {
                NSLog(@"element not found:(");
            }
            else
            {
                self.picker.selectedIndex=(int)index;
            }
        }
        
        //self.picker.selectedIndex=_selectedPositionIndex;
        [self.picker setHandler:^(NSString * value, int index) {
            weakSelf.tfPosition.text=value;
            weakSelf.chosenPositionId=[[DataManager sharedInstance]idOfPositionWithTitle:value];
            weakSelf.selectedPositionIndex=index;
        }];
        [_picker setFinishHandler:^{
            [weakSelf.tableView setContentOffset:CGPointMake(0,weakSelf.tableView.contentSize.height-weakSelf.tableView.frame.size.height) animated:YES];
        }];
        [self.picker show];
        return NO;
    }
    else if (textField==_tfRoom)
    {
        [self.view endEditing:YES];
        self.picker=[[ListPicker alloc]init];
        self.picker.dataArray = [DataManager sharedInstance].roomList;
        if (textField.text.length)
        {
            NSString *tfText=textField.text;
            NSUInteger index=[self.picker.dataArray indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                if ([(NSString*)obj isEqualToString:tfText])
                {
                    return YES;
                }
                else
                {
                    return NO;
                }
            }];
            if (index==NSNotFound)
            {
                NSLog(@"element not found:(");
            }
            else
            {
                self.picker.selectedIndex=(int)index;
            }
        }
        //self.picker.selectedIndex=_selectedRoomIndex;
        [self.picker setHandler:^(NSString * value, int index) {
            weakSelf.tfRoom.text=value;
            weakSelf.chosenRoomNumber=[[DataManager sharedInstance]roomIdWithTitle:value];
            weakSelf.selectedRoomIndex=index;
        }];
        [_picker setFinishHandler:^{
            [weakSelf.tableView setContentOffset:CGPointMake(0,weakSelf.tableView.contentSize.height-weakSelf.tableView.frame.size.height) animated:YES];
        }];
        [self.picker show];
        return NO;
    }
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField setRightViewMode:UITextFieldViewModeAlways];
    
    UIImageView *good = [[UIImageView alloc] initWithFrame:CGRectMake(25, 15, 18, 18)];
    good.image = [UIImage imageNamed:@"check-y.png"];
    UIView *goodPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    [goodPaddingView addSubview:good];
    
    UIImageView *bad = [[UIImageView alloc] initWithFrame:CGRectMake(25, 15, 18, 18)];
    bad.image = [UIImage imageNamed:@"check-n.png"];
    textField.rightViewMode = UITextFieldViewModeAlways;
    UIView *badPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    [badPaddingView addSubview:bad];
    
    
    if (textField == self.tfPassword1) {
        if (textField.text.length < 4) {
            textField.rightView = badPaddingView;
        }else{
            textField.rightView = nil;
        }
    }else if (textField == self.tfPassword2){
        if ([textField.text isEqualToString:self.tfPassword1.text]) {
            textField.rightView = goodPaddingView;
        }else{
            textField.rightView = badPaddingView;
        }
    }else if (textField == self.tfEmail){
        if ([self validateEmail:textField.text]) {
            textField.rightView = goodPaddingView;
        }else{
            textField.rightView = badPaddingView;
        }
    }
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([@[_tfLevel,_tfRoom,_tfPosition]containsObject:textField])
    {
        [self.tableView setContentOffset:CGPointMake(0,self.tableView.contentSize.height-self.tableView.frame.size.height)];
    }
    return YES;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return 60;
            break;
        case 1:
            return 572;
            break;
        case 2:
            return 720;
            break;
        case 3:
            return 290;
            break;
            
        default:
            break;
    }
    return 0;
}

@end

