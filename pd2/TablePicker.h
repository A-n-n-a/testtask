//
//  TablePicker.h
//  pd2
//
//  Created by User on 03.11.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TablePicker : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (copy) void (^tapHandler)(NSString *string, int index);
@property (nonatomic,retain)NSArray *dataArray;
@property (nonatomic,retain)IBOutlet UITableView *tableView;
@property (nonatomic,retain)IBOutlet UIBarButtonItem *doneButton;
-(void)show;
-(void)hide;

@end
