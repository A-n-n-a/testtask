//
//  ManageChildModel.m
//  pd2
//
//  Created by Eugene Fozekosh on 02.07.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ManageChildModel.h"
#import "Singleton.h"

@implementation ManageChildModel

- (void)updateFromDictionary:(NSDictionary*)dict
{
    self.childBirthday = [self safeStringFrom:[dict objectForKey:@"birth"]];
    self.childFirstName = [self safeStringFrom:[dict objectForKey:@"first_name"]];
    self.childLastName = [self safeStringFrom:[dict objectForKey:@"last_name"]];
    self.childImage = [self safeStringFrom:[dict objectForKey:@"image"]];
    self.childID = [self safeStringFrom:[dict objectForKey:@"id"]];
    self.childRoomID = [self safeStringFrom:[dict objectForKey:@"room"]];
    self.childRoomName = [self safeStringFrom:[dict objectForKey:@"room_title"]];
}

- (BOOL)nameContain:(NSString *)containString {
    
    if (!containString || ![containString isKindOfClass:[NSString class]]) {
        return YES;
    }
    
    if (!containString.length) {
        return YES;
    }
    
    if (self.childFirstName && [self.childFirstName rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    if (self.childLastName && [self.childLastName rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    return NO;
    
}


@end
