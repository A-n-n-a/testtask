//
//  AuthorisedPersonModel.h
//  pd2
//
//  Created by Andrey on 24/01/2017.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXBaseModel.h"


typedef NS_ENUM(NSUInteger, AuthorisedPersonPhotoType) {
    AuthorisedPersonPhotoTypeNo,
    AuthorisedPersonPhotoTypeUploadNow,
    AuthorisedPersonPhotoTypeUploadByParent
};

typedef NS_ENUM(NSUInteger, AuthorisedPersonAuthorisationType) {
    AuthorisedPersonAuthorisationTypeEmpty,
    AuthorisedPersonAuthorisationTypeByCall,
    AuthorisedPersonAuthorisationTypeByPassword,
    AuthorisedPersonAuthorisationTypeByPhoto,
    AuthorisedPersonAuthorisationTypeBySignature
};

@interface AuthorisedPersonModel : AXBaseModel <NSCopying>

/*
{
    "id": "2",
    "child_id": "17",
    "first_name": "Bilbo",
    "last_name": "Baggins",
    "relationship": "Uncle",
    "signature": "1",
    "parent_to_sign": "1",
    "signature_image": "",
    "password": "12345",
    "photograpth_type": "0",
    "photograph": "",
    "authorisation": "call",
    "agreement_required": "0",
    "parent_1_signature": null,
    "parent_1_name": null,
    "parent_1_datetime": null,
    "parent_2_signature": null,
    "parent_2_name": null,
    "parent_2_datetime": null,
    "details": "Everything is fine",
    "notes": "Everything is fine",
    "date_added": "2016-11-29 12:36:38"
}
*/

@property (nonatomic, strong) NSNumber *personId;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *relationship;
@property (nonatomic, strong) NSNumber *signatureRequired;
@property (nonatomic, strong) NSNumber *signatureNow;
@property (nonatomic, strong) NSString *signaturePath;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, assign) AuthorisedPersonPhotoType photoType;
@property (nonatomic, strong) NSString *photoPath;
@property (nonatomic, assign) AuthorisedPersonAuthorisationType authorisationType;

@property (nonatomic, strong) NSString *parent1Name;
@property (nonatomic, strong) NSString *parent1SignPath;
@property (nonatomic, strong) NSDate *parent1SignDate;

@property (nonatomic, strong) NSString *parent2Name;
@property (nonatomic, strong) NSString *parent2SignPath;
@property (nonatomic, strong) NSDate *parent2SignDate;

@property (nonatomic, strong) NSString *details;
@property (nonatomic, strong) NSString *notes;
@property (nonatomic, strong) NSDate *addedDate;


- (instancetype)initWithDict:(NSDictionary *)dict;

- (BOOL)isAlert;

@end
