//
//  IconButton.m
//  pd2
//
//  Created by i11 on 03/04/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "IconButton.h"
#import "UIImage+ImageByApplyingAlpha.h"

@interface IconButton ()

@property (strong, nonatomic) UIImage *defaultImage;
@property (strong, nonatomic) UIImage *zeroImage;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation IconButton

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self) {
        // Initialization code
        
        self.defaultImage = self.imageView.image;
        self.zeroImage = [[UIImage alloc] init];
        
        [self applyDefaultImage];
        
    }
    return self;
}


- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    [self hideActivityIndicator];
}

- (void)setNewDefaultImage:(UIImage *)image {

    if (!image) {
        return;
    }
    
    self.defaultImage = image;
    [self applyDefaultImage];
    
}

- (void)applyDefaultImage {
    [self setImage:self.defaultImage forState:UIControlStateNormal];
    [self setImage:[self.defaultImage imageByApplyingAlpha:0.3f] forState:UIControlStateDisabled];
    [self setImage:[self.defaultImage imageByApplyingAlpha:0.5f] forState:UIControlStateSelected];
    [self setImage:[self.defaultImage imageByApplyingAlpha:0.5f] forState:UIControlStateHighlighted];
}

- (void)applyZeroImage {
    [self setImage:self.zeroImage forState:UIControlStateNormal];
    [self setImage:self.zeroImage forState:UIControlStateDisabled];
    [self setImage:self.zeroImage forState:UIControlStateSelected];
    [self setImage:self.zeroImage forState:UIControlStateHighlighted];
}


- (void)showActivityIndicator {

#warning проверить нормально ли срабатывает при перерисовке (см Buttons.m)
    if (self.isActivityIndicatorActive) {
        return;
    }
    
    if (!self.activityIndicator) {
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.hidesWhenStopped = YES;
        
        //Изображение кнопки как правило не по центру. Размещаем activityIndicator с учетом этого
        CGRect imageFrame = self.imageView.frame;
        [activityIndicator setCenter:CGPointMake((imageFrame.origin.x + imageFrame.size.width/2),
                                                 (imageFrame.origin.y + imageFrame.size.height/2))];
        [self addSubview:activityIndicator];
        
        self.activityIndicator = activityIndicator;
    }
    
    [self.activityIndicator startAnimating];
    
    [self applyZeroImage];
    
    [self setUserInteractionEnabled:NO];
    
    self.isActivityIndicatorActive = YES;

}

- (void)hideActivityIndicator {
    
    [self applyDefaultImage];
    [self.activityIndicator stopAnimating];

    [self setUserInteractionEnabled:YES];
    
    self.isActivityIndicatorActive = NO;
    
}

- (void)setActivityIndicatorStatus:(BOOL)status {
    if (status) {
        [self showActivityIndicator];
    } else {
        [self hideActivityIndicator];
    }
}


@end
