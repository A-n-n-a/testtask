//
//  PincodeViewController.m
//  pd2
//
//  Created by i11 on 22/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "LoginPinViewController.h"
#import "UserManager.h"
#import "Utils.h"
#import "User.h"
#import "NetworkManager.h"
#import "AccessTimeManager.h"
#import "RegisterForceManager.h"
#import "ResetPinViewController.h"
#import "LoginTableViewController.h"
#import <AudioToolbox/AudioToolbox.h>

#import "PSTAlertController.h"

#import "NSString+VersionNumbers.h"
#import "UnavailableSectionManager.h"

#import "MyColorTheme.h"
#import "ListPicker.h"
#import "WL.h"

#import "VPBiometricAuthenticationFacade.h"
#import <LocalAuthentication/LocalAuthentication.h>

#import "UIImage+ContainsImage.h"

#define MAX_FAILED_ATTEMPTS 5


#define SPOILER_USER_IMAGE_TAG 10
#define SPOILER_LABEL_TAG 20
#define SPOILER_ARROW_IMAGE_TAG 30

#define PIN_DOTS_VIEW_TAG 100

#define ALERTVIEW_FAILED_ATTEMPTS_TAG 10


NSString * const kPDSpoiler = @"kPDSpoiler";
NSString * const kPDSpoilerUser = @"kPDSpoilerUser";
NSString * const kPDSpoilerLoginWithPassword = @"kPDSpoilerLoginWithPassword";
NSString * const kPDTitle = @"kPDTitle";
NSString * const kPDPinDots = @"kPDPinDots";
NSString * const kPDPinPad = @"kPDPinPad";
NSString * const kPDActionButtons = @"kPDActionButtons";

typedef enum {
    PDLoginPinSectionSpoilers,
    PDLoginPinSectionRows} PDLoginPinSections;

@interface LoginPinViewController ()

@property (weak, nonatomic) UITableViewCell *pinPadCell;


@property (weak, nonatomic) UIView *pinDotsView;
@property (strong, nonatomic) NSMutableArray *pinButtons;


@property (weak, nonatomic) UIImageView *spoilerArrowImageView;
@property (weak, nonatomic) UIButton *touchIdButton;


@property (strong, nonatomic) NSMutableString *enteredPinCode;
@property (strong, nonatomic) NSString *pinCode;

@property (assign, nonatomic) int buttonsPressedCount;
@property (assign, nonatomic) int failedAttempts;

@property (strong, nonatomic) NSArray *spoilers;
@property (strong, nonatomic) NSMutableArray *rows;
@property (assign, nonatomic) BOOL isSpoilerExpanded;

@property (strong, nonatomic) NSArray *users;
@property (strong, nonatomic) User *activeUser;

@property (strong, nonatomic) ListPicker *listPicker;

@end


@implementation LoginPinViewController

#warning при клике на свободное место - скрывать список

- (void)loadView {
    [super loadView];
    
    
    self.spoilers = [NSMutableArray arrayWithObjects:
                 [NSMutableDictionary dictionaryWithObject:kPDSpoiler forKey:@"type"],
                 nil];

    self.rows = [NSMutableArray arrayWithObjects:
                 [NSMutableDictionary dictionaryWithObject:kPDTitle forKey:@"type"],
                 [NSMutableDictionary dictionaryWithObject:kPDPinDots forKey:@"type"],
                 [NSMutableDictionary dictionaryWithObject:kPDPinPad forKey:@"type"],
                 [NSMutableDictionary dictionaryWithObject:kPDActionButtons forKey:@"type"],
                 nil];

    self.users = [[UserManager sharedManager] sortedUsersLastLoggedWithPinIsFirst];
    //self.activeUser = [[UserManager sharedManager] lastLoggedUser];
    self.activeUser = [self.users firstObject];
    
    self.isSpoilerExpanded = NO;
    [self updateSpoiler];
    
    [self disableInactiveTimer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*
    self.pinDotsView = [self.pinFieldsCell viewWithTag:PIN_DOTS_VIEW_TAG];
    for (NSInteger tag = 1; tag <= 4; tag++)
    {
        UIView *view = [self.pinDotsView viewWithTag:tag];
        [view setBackgroundColor:[UIColor clearColor]];
        [view.layer setBorderColor:[UIColor whiteColor].CGColor];
        [view.layer setBorderWidth:1.0f];
        CGFloat radius = view.frame.size.width / 2;
        [view.layer setCornerRadius:radius];
    }
    */
     
    /*
    for (UIButton *button in self.pinButtons) {
        [button.layer setBorderColor:[UIColor whiteColor].CGColor];
        [button.layer setBorderWidth:1.1f];
        CGFloat radius = button.frame.size.width / 2;
        [button.layer setCornerRadius:radius];
    }
    */
    
    self.view.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
    
    self.enteredPinCode = [[NSMutableString alloc] init];

#warning delete check in release
//Add temporary fast push to specifics usernames
    
    /*
    if([self.activeUser.userName isEqualToString:@"andrey"]) {
        [self.enteredPinCode setString:@"2222"];
        [self remotePinCheck];
    }
    */
    /*
    if ([self.activeUser.userName isEqualToString:@"YOUR USERNAME"]){
        [self.enteredPinCode setString:@"YOUR PIN"];
        [self remotePinCheck];
    }
    */
    
    [self updateScroll];
    
    //[self runColorCheck:YES];
    
    //[[MyColorTheme sharedInstance]setBlueTheme];
    //[self setColorTheme];
    
    [[UserManager sharedManager] debugPrintUsers];

    
}



- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    
    if ([NetworkManager sharedInstance].isWhiteLabel) {
        
        BOOL showAlert = NO;
        
        if (![NetworkManager sharedInstance].whiteLabelSubdomains || ![[NetworkManager sharedInstance].whiteLabelSubdomains isKindOfClass:[NSArray class]]) {
            showAlert = YES;
        }
        
        if (![[NetworkManager sharedInstance].whiteLabelSubdomains count]) {
            showAlert = YES;
        }
        
        if (showAlert) {
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:@"Error"
                                                message:@"Try again later"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *action =
            [UIAlertAction actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       [AppDelegate logoutAndSendDeviceTokenIs:NO];
                                   }];
            
            [alert addAction:action];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            return;
        }
        
        
    }
    
}


- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self loginByTouchIdForUser:self.activeUser];
    [self updateTouchIdButton];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
-(void)setColorTheme
{
    [super setColorTheme];
    
    for (NSDictionary *dict in self.spoilers) {
    
        UITableViewCell *cell = [dict objectForKey:@"cell"];
        NSString *type = [dict objectForKey:@"type"];
        
        
        if ([type isEqualToString:kPDSpoiler]) {
            cell.backgroundColor = [[MyColorTheme sharedInstance] spoilerBackGroundColor];
            
            UILabel *label = (UILabel *)[cell viewWithTag:SPOILER_LABEL_TAG];
            label.textColor = [[MyColorTheme sharedInstance] spoilerTextColor];
            
            UIImageView *arrowImageView = (UIImageView *)[cell viewWithTag:SPOILER_ARROW_IMAGE_TAG];
            //[arrowImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [arrowImageView setTintColor:[[MyColorTheme sharedInstance] backgroundColor]];
        
        } else if ([type isEqualToString:kPDSpoilerLoginWithPassword]) {
            
            cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
            
            UILabel *label = (UILabel *)[cell viewWithTag:SPOILER_LABEL_TAG];
            label.textColor = [[MyColorTheme sharedInstance] spoilerTextColor];
            
        }
#warning из за добавленной прозрачности по выбору ячейки - проверять index и менять цвет
        
    }
    
    
    for (NSDictionary *dict in self.rows) {
        
        UITableViewCell *cell = [dict objectForKey:@"cell"];
        NSString *type = [dict objectForKey:@"type"];
        
        //change cells background
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        
        //change buttons background
        if ([type isEqualToString:kPDPinPad]) {
            UITableViewCell *cell = [dict objectForKey:@"cell"];
            for (int i = 0; i <= 9; i++)
            {
                NSInteger tag = i + 100;
                UIButton *button = (UIButton *)[cell viewWithTag:tag];
                 button.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
            }
        }
    }
    

}
*/

- (void)updateScroll {
    
    //Обязательно вызываем, иначе contentSize.height = 0
    [self.tableView layoutIfNeeded];
    
    NSLog(@"self.tableView.contentSize.height = %f", self.tableView.contentSize.height);
    NSLog(@"self.tableView.frame.size.height = %f", self.tableView.frame.size.height);
    //TODO: add method to parent class ?
    //Найти способ автоматически вызывать этот метод при изменении размеров таблицы
    //Отслеживать редактируемый textField, если textField скрывается за клавиатурой - разрешать scroll
    //Отслеживать table animation
    
    if (self.tableView.contentSize.height < self.tableView.frame.size.height) {
        self.tableView.scrollEnabled = NO;
    } else {
        self.tableView.scrollEnabled = YES;
    }
    
}


- (void)runColorCheck:(BOOL)bo {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        bo ? [[MyColorTheme sharedInstance]setBlueTheme] : [[MyColorTheme sharedInstance]setYellowTheme];
        [self setColorTheme];
        [self runColorCheck:!bo];
    
    });
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *row;
    
    if (indexPath.section == PDLoginPinSectionSpoilers) {
        row = [[self.spoilers objectAtIndex:indexPath.row] objectForKey:@"type"];
    } else  if( indexPath.section == PDLoginPinSectionRows){
        row = [[self.rows objectAtIndex:indexPath.row] objectForKey:@"type"];
    }
    
    
    if ([row isEqualToString:kPDSpoiler]) {
        return 44.f;
    }
    else if ([row isEqualToString:kPDSpoilerUser]) {
        return 44.f;
    }
    else if ([row isEqualToString:kPDSpoilerLoginWithPassword]) {
        return 44.f;
    }
    else if ([row isEqualToString:kPDTitle]) {
        return 68.f;
    }
    else if ([row isEqualToString:kPDPinDots]) {
        return 32.f;
    }
    else if ([row isEqualToString:kPDPinPad]) {
        return 372.f;
    }
    else if ([row isEqualToString:kPDActionButtons]) {
        return 30.f;
    }
    else {
        return 0.f;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == PDLoginPinSectionSpoilers) {
        
        NSString *row = [[self.spoilers objectAtIndex:indexPath.row] objectForKey:@"type"];
        
        if ([row isEqualToString:kPDSpoiler]) {

            User *user = [[self.spoilers objectAtIndex:indexPath.row] objectForKey:@"user"];
            
            if ([self.activeUser isEqual:user]) {
                //кликнули по первому
                [self runExpandOrCollapseTableView:tableView];
                [self updateScroll];

            } else if ([user isCanEnterPin]) {
                [self resetPinDots];
                self.activeUser = user;
                [self runExpandOrCollapseTableView:tableView];
                [self updateScroll];
                [self loginByTouchIdForUser:self.activeUser];
                [self updateTouchIdButton];

            } else {
                //Стоит флаг запрета ввода Pin или не установлен Pin
                self.activeUser = user;
                [self performSegueWithIdentifier:@"loginSegue" sender:self];
            }
            
        }else if([row isEqualToString:kPDSpoilerLoginWithPassword]) {
            self.activeUser = nil;
            [self performSegueWithIdentifier:@"loginSegue" sender:self];
        }
        
        
        NSLog(@"tap on %li", (long)indexPath.row);
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == PDLoginPinSectionSpoilers) {
        return [self.spoilers count];
    } else if(section == PDLoginPinSectionRows){
        return [self.rows count];
    }

    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *rowType;
    
    if (indexPath.section == PDLoginPinSectionSpoilers) {
        rowType = [[self.spoilers objectAtIndex:indexPath.row] objectForKey:@"type"];
    } else  if( indexPath.section == PDLoginPinSectionRows){
        rowType = [[self.rows objectAtIndex:indexPath.row] objectForKey:@"type"];
    }
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    if ([rowType isEqualToString:kPDSpoiler]) {
        cell = [self configureSpoilerCellForTableView:tableView indexPath:indexPath];
    } else if ([rowType isEqualToString:kPDSpoilerUser]) {
        cell = [self configureSpoilerUserCellForTableView:tableView indexPath:indexPath];
    } else if ([rowType isEqualToString:kPDSpoilerLoginWithPassword]) {
        cell = [self configureSpoilerLoginWithPasswordCellForTableView:tableView indexPath:indexPath];
    } else if ([rowType isEqualToString:kPDTitle]) {
        cell = [self configureTitleCellForTableView:tableView indexPath:indexPath];
    } else if ([rowType isEqualToString:kPDPinDots]) {
        cell = [self configurePinDotsCellForTableView:tableView indexPath:indexPath];
    } else if ([rowType isEqualToString:kPDPinPad]) {
        cell = [self configurePinPadCellForTableView:tableView indexPath:indexPath];
    } else if ([rowType isEqualToString:kPDActionButtons]) {
        cell = [self configureActionButtonsCellForTableView:tableView indexPath:indexPath];
    }
    
    
    [self setCell:cell forIndexPath:indexPath];
    
    
    return cell;
}

- (UITableViewCell *)configureSpoilerCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"spoilerCell"];
    
    if (cell)
    {
        User *user = [[self.spoilers objectAtIndex:indexPath.row] objectForKey:@"user"];
        
        UIImageView *arrowImageView = (UIImageView *)[cell viewWithTag:SPOILER_ARROW_IMAGE_TAG];
        
        if (indexPath.row == 0 ) {
            
            arrowImageView.image = [[UIImage imageNamed:(self.isSpoilerExpanded ? @"arrowUP" : @"arrowDown")] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [arrowImageView setTintColor:[[MyColorTheme sharedInstance] backgroundColor]];
            /*
            UIImage *image = [[UIImage imageNamed:(@"arrowUp")] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            arrowImageView.image = image;
            */
            
            self.spoilerArrowImageView = arrowImageView; //Стрелка верхнего спойлера
        } else {
            arrowImageView.image = nil;
        }
        
        
        UIImageView *userPic = (UIImageView *)[cell viewWithTag:SPOILER_USER_IMAGE_TAG];
        userPic.image = nil; //устраняем мигание, из за смены предыдущего изображения
        
        __weak UIImageView *weakImageView = userPic;
        

        if (user.userType == UserTypeAdmin) {
            /*
            NSString *basePath = [[NetworkManager sharedInstance] urlForSubdomain:user.subDomain];
            NSString *url = [NSString stringWithFormat:@"%@/%@", basePath, user.picURL];
            [[Utils sharedInstance] downloadImageAtPath:url completionHandler:^(UIImage *image) {
                weakImageView.image = image;
            }];
            */
            
            if (user.image) {
                weakImageView.image = user.image;
            }
            else {
                weakImageView.image = [UIImage imageNamed:@"j3_default_admin"];
                
                NSString *basePath = [[NetworkManager sharedInstance] urlForSubdomain:user.subDomain];
                NSString *url = [NSString stringWithFormat:@"%@/%@", basePath, user.picURL];
                [[Utils sharedInstance] downloadImageAtPath:url completionHandler:^(UIImage *image) {
                    if ([image containsImage]) {
                        weakImageView.image = image;
                    }
                    /*
                    else {
                        weakImageView.image = [UIImage imageNamed:@"j3_default_admin"];
                    }
                    */
                    
                }];
                
                //weakImageView.image = [UIImage imageNamed:@"parent-default-icon.png"];
            }
            
        }
        else {
            if (user.image) {
                weakImageView.image = user.image;
            }
            else {
                weakImageView.image = [UIImage imageNamed:@"j3_default_parent"];
                
                NSString *basePath = [[NetworkManager sharedInstance] urlForSubdomain:user.subDomain];
                NSString *url = [NSString stringWithFormat:@"%@/%@", basePath, user.picURL];
                [[Utils sharedInstance] downloadImageAtPath:url completionHandler:^(UIImage *image) {
                    
                    if ([image containsImage]) {
                        weakImageView.image = image;
                    }
                    /*
                    else {
                        weakImageView.image = [UIImage imageNamed:@"j3_default_parent"];
                    }
                    */
                }];
                
                
            }
            
            //weakImageView.image = [UIImage imageNamed:@"parent-default-icon.png"];
        }


        userPic.backgroundColor = [UIColor whiteColor];
        CALayer *layer = userPic.layer;
        layer.masksToBounds = YES;
        layer.cornerRadius = layer.frame.size.width / 2;

        
        UILabel *label = (UILabel *)[cell viewWithTag:SPOILER_LABEL_TAG];
        label.text = user.description;
        
        if (indexPath.row == 0 ) {
            cell.backgroundColor = [[MyColorTheme sharedInstance] spoilerBackGroundColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
            label.textColor = [[MyColorTheme sharedInstance] pinPadSelectedSpoilerTextColor];
        }else{
            cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
            cell.contentView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.2f];
            label.textColor = [UIColor whiteColor];
        }
        
    }
    
    
    return cell;
}

- (BOOL)containsImage:(UIImage*)image {
    BOOL result = NO;
    
    CGImageRef cgref = [image CGImage];
    CIImage *cim = [image CIImage];
    
    if (cim != nil || cgref != NULL) { // contains image
        result = YES;
    }
    
    return result;
}


- (UITableViewCell *)configureSpoilerUserCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"spoilerUserCell"];
    
    if (cell)
    {
        User *user = [[self.spoilers objectAtIndex:indexPath.row] objectForKey:@"user"];
        
        UIImageView *userPic = (UIImageView *)[cell viewWithTag:SPOILER_USER_IMAGE_TAG];
        userPic.image = [UIImage imageNamed:@"tmp_user1.png"];
        CALayer *layer = userPic.layer;
        layer.masksToBounds = YES;
        layer.cornerRadius = layer.frame.size.width / 2;
        
        
        UILabel *label = (UILabel *)[cell viewWithTag:SPOILER_LABEL_TAG];
        label.text = user.description;
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] spoilerBackGroundColor];
        label.textColor = [[MyColorTheme sharedInstance] spoilerTextColor];
    }
    
    
    return cell;
}


- (UITableViewCell *)configureSpoilerLoginWithPasswordCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"spoilerLoginWithPasswordCell"];
    
    if (cell)
    {
        
        UILabel *label = (UILabel *)[cell viewWithTag:SPOILER_LABEL_TAG];
        //
        
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        cell.contentView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.2f];
        //label.textColor = [[MyColorTheme sharedInstance] spoilerTextColor];
        label.textColor = [UIColor whiteColor];
    }
    
    
    return cell;
}

- (UITableViewCell *)configureTitleCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"titleCell"];
    
    if (cell)
    {
        cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(freeTapAction:)];
        [cell addGestureRecognizer:tap];
    }
    
    return cell;
}

- (UITableViewCell *)configurePinDotsCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pinDotsCell"];
    
    if (cell)
    {
        self.pinDotsView = [cell viewWithTag:PIN_DOTS_VIEW_TAG];

        for (NSInteger tag = 1; tag <= 4; tag++)
        {
            UIView *view = [self.pinDotsView viewWithTag:tag];
            [view setBackgroundColor:[UIColor clearColor]];
            [view.layer setBorderColor:[[MyColorTheme sharedInstance] loginTextColor].CGColor];
            [view.layer setBorderWidth:1.0f];
            CGFloat radius = view.frame.size.width / 2;
            [view.layer setCornerRadius:radius];
        }
        cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(freeTapAction:)];
        [cell addGestureRecognizer:tap];
    }
    
    return cell;
}

- (UITableViewCell *)configurePinPadCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pinPadCell"];
    
    if (cell)
    {
        self.pinPadCell = cell;
        
        for (int i = 0; i <= 9; i++)
        {
            NSInteger tag = i + 100;
            UIButton *button = (UIButton *)[cell viewWithTag:tag];
            [button setBackgroundColor:[[MyColorTheme sharedInstance] loginBackgroundColor]];
            [button.layer setBorderColor:[UIColor whiteColor].CGColor];
            //[button.layer setBorderWidth:1.1f];
            [button.layer setBorderWidth:.0f];
            CGFloat radius = button.frame.size.width / 2;
            [button.layer setCornerRadius:radius];
        }
        
        if (!self.touchIdButton) {
            UIButton *touchIdButton = (UIButton *)[cell viewWithTag:407];
            
            UIImage *image = [[UIImage imageNamed:[Utils isFaceIdSupported]? @"Face_ID" : @"Touch_ID"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [touchIdButton setImage:image forState:UIControlStateNormal];
            [touchIdButton setTintColor:[[MyColorTheme sharedInstance] loginTextColor]];
            
             self.touchIdButton = touchIdButton;
        }
        
        [self updateTouchIdButton];
        
        
        
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
    
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(freeTapAction:)];
        [cell addGestureRecognizer:tap];
    }
    
    return cell;
}

- (UITableViewCell *)configureActionButtonsCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"actionButtonsCell"];
    
    if (cell)
    {
        cell.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];
    
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(freeTapAction:)];
        [cell addGestureRecognizer:tap];
    }
    
    return cell;
}

- (void)updatesForActiveUser {
    //обнулить окно пина, попытки и тд
}


- (void)updateTouchIdButton {
    
    VPBiometricAuthenticationFacade *biometricFacede = [[VPBiometricAuthenticationFacade alloc] init];
    
    if ([biometricFacede isAuthenticationAvailable] && self.activeUser.isTouchId) {
        self.touchIdButton.enabled = YES;
        self.touchIdButton.hidden = NO;
    }
    else {
        self.touchIdButton.enabled = NO;
        self.touchIdButton.hidden = YES;
    }
    
}

- (IBAction)pinButtonTouchUpInside:(UIButton *)sender {
    
    [self pinPadUserInteractionEnable:NO];
    
    int digit = (int)[sender tag] - 100;
    
    //Play KeyPressed sq_tock.caf
    //AudioServicesPlaySystemSound(1105);
    
    int viewTag = digit + 10;
    
    UIView *view = [self.pinPadCell viewWithTag:viewTag];
    UILabel *digitLabel = (UILabel *)[view viewWithTag:20];
    UILabel *alphabeticalLabel = (UILabel *)[view viewWithTag:30];
    
    //[self hightLightView:view];
    [self hightLightView:digitLabel];
    //[self hightLightView:alphabeticalLabel];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //[self unHightLightView:view];
        [self unHightLightView:digitLabel];
        //[self unHightLightView:alphabeticalLabel];
    });
    
    
    self.buttonsPressedCount++;
    [self.enteredPinCode appendString:[NSString stringWithFormat:@"%i", digit]];
    NSLog(@"touch button = %i, pressed-count = %i", digit, self.buttonsPressedCount);

    UIView *pinDotView = [self.pinDotsView viewWithTag:self.buttonsPressedCount];
    [pinDotView setBackgroundColor:[[MyColorTheme sharedInstance] loginTextColor]];
    
    if (self.buttonsPressedCount == 4)
    {
        [self remotePinCheck];
    } else {
        [self pinPadUserInteractionEnable:YES];
    }
}

- (IBAction)selectThemeAction:(UIButton *)sender {
    
    self.listPicker = [[ListPicker alloc] init];
    self.listPicker.dataArray = [MyColorTheme sharedInstance].themesList;
    
    [self.listPicker setHandler:^(NSString *value, int index) {
        [[MyColorTheme sharedInstance] setThemeName:value];
    }];
    [self.listPicker show];
}


- (IBAction)touchIdAction:(UIButton *)sender {
    
    [self loginByTouchIdForUser:self.activeUser];

}



- (void)loginByTouchIdForUser:(User *)user {
    
    if (!user.isTouchId) {
        return;
    }
    
    User *selectedUser = user;
    
    
    LAContext *context = [[LAContext alloc] init];
    context.localizedFallbackTitle = @"";
    
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:[Utils isFaceIdSupported] ? @"Unlock using Face ID" : @"Touch the Home button to login"
                          reply:^(BOOL success, NSError *error) {
                              
                              if (error) {
                                  /*
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                   
                                   PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Error" message:@"Touch ID authentication failed" preferredStyle:PSTAlertControllerStyleAlert];
                                   
                                   [alert addAction:[PSTAlertAction actionWithTitle:@"Retry" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                   
                                   [self loginByTouchId];
                                   
                                   }]];
                                   
                                   [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                   
                                   
                                   }]];
                                   
                                   [alert showWithSender:nil controller:self animated:YES
                                   completion:nil];
                                   });
                                   */
                                  return;
                                  
                              }
                              
                              if (success) {
                                  
                                  [[NetworkManager sharedInstance] loginWithTouchIdString:selectedUser.touchIdToken forUser:selectedUser.uid subdomain:selectedUser.subDomain withCompletion:^(id response, NSError *error) {
                                      
                                      if (response)
                                      {
                                          
                                          if([[response valueForKeyPath:@"result.complete"]intValue]==1)
                                          {
                                              
                                              
                                              NSString *iosString;
                                              
                                              
                                              @try {
                                                  iosString = [response valueForKeyPath:@"result.ios"];
                                              }
                                              @catch (NSException *exception) {
                                                  NSLog(@"Exception. Reason: %@", exception.reason);
                                                  NSLog(@"Неверный response result");
                                              }
                                              
                                              NSNumber *iosUpdate = iosString ? [NSNumber numberWithInt:[iosString intValue]] : nil;
                                              
                                              //iosUpdate = @1;
                                              
                                              //Сервер рекоммендует обновиться
                                              if (iosUpdate && [iosUpdate isEqualToNumber:@1]) {
                                                  //Alert(@"Notice", @"Please update your application to the latest version");
                                                  
                                                  PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning" message:[response valueForKeyPath:@"result.msg"] preferredStyle:PSTAlertControllerStyleAlert];
                                                  
                                                  [alert addAction:[PSTAlertAction actionWithTitle:@"Update" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                                      
                                                      [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[WL appStoreLink]]];
                                                      [self loginProcedureWithResponse:response];
                                                  }]];
                                                  
                                                  [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:^(PSTAlertAction *action) {
                                                      [self loginProcedureWithResponse:response];
                                                      
                                                      
                                                  }]];
                                                  
                                                  
                                                  [alert showWithSender:self controller:self animated:YES completion:nil];
                                                  
                                              }
                                              else {
                                                  [self loginProcedureWithResponse:response];
                                              }
                                              
                                          }
                                          else
                                          {
                                              NSString *msg;
                                              NSString *iosString;
                                              
                                              @try {
                                                  msg = [response valueForKeyPath:@"result.msg"];
                                                  iosString = [response valueForKeyPath:@"result.ios"];
                                              }
                                              @catch (NSException *exception) {
                                                  NSLog(@"Exception. Reason: %@", exception.reason);
                                                  NSLog(@"Неверный response result");
                                              }
                                              
                                              if (msg) {
                                                  
                                                  NSNumber *iosUpdate = iosString ? [NSNumber numberWithInt:[iosString intValue]] : nil;
                                                  
                                                  if (iosUpdate && [iosUpdate isEqualToNumber:@2]) {
                                                      //Сервер настоятельно рекоммендует обновиться
                                                      //Alert(@"Error", @"Please update your application to the latest version");
                                                      //Alert(@"Error",[response valueForKeyPath:@"result.msg"] );
                                                      
                                                      
                                                      PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning" message:[response valueForKeyPath:@"result.msg"] preferredStyle:PSTAlertControllerStyleAlert];
                                                      
                                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Update" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                                          
                                                          [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[WL appStoreLink]]];
                                                          
                                                      }]];
                                                      
                                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:nil]];
                                                      
                                                      [alert showWithSender:self controller:self animated:YES completion:nil];
                                                      
                                                      //[self pinCodeSuccessfulLoginIs:NO decraseAttempt:NO];
                                                      //Alert(@"Error",[response valueForKeyPath:@"result.msg"]);
                                                      return;
                                                      
                                                  }
                                                  
                                                  
                                                  if ([msg containsString:@"Username and password do not match"]) {
                                                      //[self pinCodeSuccessfulLoginIs:NO decraseAttempt:YES];
                                                      Alert(@"Error",[response valueForKeyPath:@"result.msg"]);
                                                      
                                                  }
                                                  else {
                                                      
                                                      //для блокированных систем выводим msg в заголовок, text в тело сообщения. +tel по кнопке звонка
                                                      NSString *msgString = [response valueForKeyPath:@"result.msg"];
                                                      NSString *textString = [response valueForKeyPath:@"result.text"];
                                                      NSString *telString = [response valueForKeyPath:@"result.tel"];
                                                      
                                                      if (msgString && textString && telString) {
                                                          
                                                          PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:msgString message:textString preferredStyle:PSTAlertControllerStyleAlert];
                                                          
                                                          [alert addAction:[PSTAlertAction actionWithTitle:@"OK" style:PSTAlertActionStyleDefault handler:nil]];
                                                          
                                                          [alert addAction:[PSTAlertAction actionWithTitle:@"Call" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                                              
                                                              NSString *tel = [NSString stringWithFormat:@"tel:%@", telString];
                                                              [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
                                                              
                                                          }]];
                                                          
                                                          [alert showWithSender:self controller:self animated:YES completion:nil];
                                                          
                                                      }
                                                      else {
                                                          Alert(@"Error",[response valueForKeyPath:@"result.msg"]);
                                                      }
                                                      
                                                      //[self pinCodeSuccessfulLoginIs:NO decraseAttempt:NO];
                                                  }
                                              }
                                              else {
                                                  Alert(@"Error", @"Error happened while auth");
                                                  //[self pinCodeSuccessfulLoginIs:NO decraseAttempt:NO];
                                              }
                                          }
                                      }
                                      else if(error)
                                      {
                                          Alert(@"Error", error.localizedDescription);
                                          //[self pinCodeSuccessfulLoginIs:NO decraseAttempt:NO];
                                      }

                                  }];
                                  
                              } else {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      
                                      PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Error" message:@"You are not the device owner." preferredStyle:PSTAlertControllerStyleAlert];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Retry" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          
                                          [self loginByTouchIdForUser:self.activeUser];
                                          
                                      }]];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          //[self showMenu];
                                          
                                      }]];
                                      
                                      [alert showWithSender:nil controller:self animated:YES
                                                 completion:nil];
                                      
                                  });
                                  
                              }
                              
                          }];
        
    }/* else {
      
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
      message:@"Your device cannot authenticate using TouchID."
      delegate:nil
      cancelButtonTitle:@"Ok"
      otherButtonTitles:nil];
      [alert show];
      
      }
      */
}



- (IBAction)cancelAction:(id)sender {
    
    //NSLog(@"pin = %@", self.enteredPinCode);
    NSLog(@"length = %lu", (unsigned long)[self.enteredPinCode length]);
    NSLog(@"buttonsPressedCount = %i", self.buttonsPressedCount);
    
    if ([self.enteredPinCode length] > 0)
    {
        
        UIView *pinDotView = [self.pinDotsView viewWithTag:self.buttonsPressedCount];
        [pinDotView setBackgroundColor:[UIColor clearColor]];
        
        NSRange range = NSMakeRange([self.enteredPinCode length] - 1, 1);
        [self.enteredPinCode deleteCharactersInRange:range];

        self.buttonsPressedCount--;
    }

}

- (IBAction)resetPinAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"resetPinSegue" sender:self];
}

- (void)remotePinCheck {
    
    NSLog(@"userName = %@, pin = %@", self.activeUser.userName, self.enteredPinCode);
    
    [[NetworkManager sharedInstance] loginWithPin:self.enteredPinCode forUser:self.activeUser.uid subdomain:self.activeUser.subDomain withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
        
            if([[response valueForKeyPath:@"result.complete"]intValue]==1)
            {
                
                
                NSString *iosString;
                
                
                @try {
                    iosString = [response valueForKeyPath:@"result.ios"];
                }
                @catch (NSException *exception) {
                    NSLog(@"Exception. Reason: %@", exception.reason);
                    NSLog(@"Неверный response result");
                }
                
                NSNumber *iosUpdate = iosString ? [NSNumber numberWithInt:[iosString intValue]] : nil;
                
                //iosUpdate = @1;
                
                //Сервер рекоммендует обновиться
                if (iosUpdate && [iosUpdate isEqualToNumber:@1]) {
                    //Alert(@"Notice", @"Please update your application to the latest version");
                    
                    PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning" message:[response valueForKeyPath:@"result.msg"] preferredStyle:PSTAlertControllerStyleAlert];
                    
                    [alert addAction:[PSTAlertAction actionWithTitle:@"Update" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                        
                        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[WL appStoreLink]]];
                        [self loginProcedureWithResponse:response];
                    }]];
                    
                    [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:^(PSTAlertAction *action) {
                       [self loginProcedureWithResponse:response];
                        
                        
                    }]];

                    
                    [alert showWithSender:self controller:self animated:YES completion:nil];
                    
                }
                else {
                    [self loginProcedureWithResponse:response];
                }

            }
            else
            {
                NSString *msg;
                NSString *iosString;

                @try {
                    msg = [response valueForKeyPath:@"result.msg"];
                    iosString = [response valueForKeyPath:@"result.ios"];
                }
                @catch (NSException *exception) {
                    NSLog(@"Exception. Reason: %@", exception.reason);
                    NSLog(@"Неверный response result");
                }
                
                if (msg) {
                    
                    NSNumber *iosUpdate = iosString ? [NSNumber numberWithInt:[iosString intValue]] : nil;
                    
                    if (iosUpdate && [iosUpdate isEqualToNumber:@2]) {
                        //Сервер настоятельно рекоммендует обновиться
                        //Alert(@"Error", @"Please update your application to the latest version");
                        //Alert(@"Error",[response valueForKeyPath:@"result.msg"] );
                        
                        
                        PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning" message:[response valueForKeyPath:@"result.msg"] preferredStyle:PSTAlertControllerStyleAlert];
                        
                        [alert addAction:[PSTAlertAction actionWithTitle:@"Update" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                            
                            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[WL appStoreLink]]];
                            
                        }]];
                        
                        [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:nil]];
                        
                        [alert showWithSender:self controller:self animated:YES completion:nil];
                        
                        [self pinCodeSuccessfulLoginIs:NO decraseAttempt:NO];
                        
                        return;
                       
                    }
                    
                    
                    if ([msg containsString:@"Username and password do not match"]) {
                        [self pinCodeSuccessfulLoginIs:NO decraseAttempt:YES];
                    }
                    else {
                        
                        //для блокированных систем выводим msg в заголовок, text в тело сообщения. +tel по кнопке звонка
                        NSString *msgString = [response valueForKeyPath:@"result.msg"];
                        NSString *textString = [response valueForKeyPath:@"result.text"];
                        NSString *telString = [response valueForKeyPath:@"result.tel"];
                        
                        if (msgString && textString && telString) {
                            
                            PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:msgString message:textString preferredStyle:PSTAlertControllerStyleAlert];
                            
                            [alert addAction:[PSTAlertAction actionWithTitle:@"OK" style:PSTAlertActionStyleDefault handler:nil]];
                            
                            [alert addAction:[PSTAlertAction actionWithTitle:@"Call" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                
                                NSString *tel = [NSString stringWithFormat:@"tel:%@", telString];
                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
                                
                            }]];
                            
                            [alert showWithSender:self controller:self animated:YES completion:nil];
                            
                        }
                        else {
                            Alert(@"Error",[response valueForKeyPath:@"result.msg"]);
                        }
                        
                        [self pinCodeSuccessfulLoginIs:NO decraseAttempt:NO];
                    }
                }
                else {
                    Alert(@"Error", @"Error happened while auth");
                    [self pinCodeSuccessfulLoginIs:NO decraseAttempt:NO];
                }
            }
     }
     else if(error)
     {
         Alert(@"Error", error.localizedDescription);
         [self pinCodeSuccessfulLoginIs:NO decraseAttempt:NO];
     }

    }];
}

- (void)pinCodeSuccessfulLoginIs:(BOOL)successful decraseAttempt:(BOOL)isDecraseAttempt
{

    if (successful) {
        
        [[UserManager sharedManager] successfullyLoggedUser:self.activeUser];

        //Старт менеджера видео загрузок
        
        
        [self setupTouchIdOrShowMenu];
        
        /*
        if ([[UserManager sharedManager] loggedUser].userType == UserTypeAdmin) {
            [self performSegueWithIdentifier:@"@Main_iPhone" sender:self];
        } else {
            [self performSegueWithIdentifier:@"@Main-ParentSide_iPhone" sender:self];
        }
        */
        
        
    } else {
        
        [self.enteredPinCode setString:@""];
        
        if (isDecraseAttempt) {
            self.failedAttempts ++;
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        }

        
        //0.3 sec
        [self animatePinDotsView];
        
        [self performSelector:@selector(updatePinDotsView) withObject:nil afterDelay:0.5];
       
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self pinPadUserInteractionEnable:YES];
        });

        self.buttonsPressedCount = 0;
        
        if (self.failedAttempts == MAX_FAILED_ATTEMPTS)
        {
            [self.activeUser pinFailedAttempt];
            
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Pin is incorrect. You can login by password or reset your pin."
                                                             message:nil
                                                            delegate:self
                                                   cancelButtonTitle:@"Use Password"
                                                   otherButtonTitles:@"Reset Pin", nil];
            alert.tag = ALERTVIEW_FAILED_ATTEMPTS_TAG;
            [alert show];
            
            /*
            for (UIButton *button in self.pinButtons)
            {
                [button setUserInteractionEnabled:NO];
            }
            */
        }
        
    }
    
    
}

//Необходимо чтобы обернуть обработку в отдельный вызов метода
- (void)loginProcedureWithResponse:(NSDictionary *)response {
  
    
    
    NSString *uidString;
    NSString *aclString;
    NSString *vString;
    NSString *appString;
    NSString *apiVersionString;
    NSString *betaString;
    NSString *j3String;
    
    @try {
        uidString = [response valueForKeyPath:@"result.aid"];
        aclString = [response valueForKeyPath:@"result.acl"];
        vString = [response valueForKeyPath:@"result.v"];
        appString = [response valueForKeyPath:@"result.app"];
        apiVersionString = [response valueForKeyPath:@"result.api"];
        betaString = [response valueForKeyPath:@"result.beta"];
        j3String = [response valueForKeyPath:@"result.version_system"];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception. Reason: %@", exception.reason);
        NSLog(@"Неверный response result");
    }
    
    if (![uidString isKindOfClass:[NSString class]] || ![aclString isKindOfClass:[NSString class]]) {
        Alert(@"Error", @"Wrong user");
        [self pinCodeSuccessfulLoginIs:NO decraseAttempt:NO];
        return;
    }
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    
    int uid = [uidString intValue];
    int acl = [aclString intValue];
    BOOL v = [vString boolValue];
    BOOL appDenied = appString && [appString intValue] == 0 ? YES : NO;
    NSArray *unavailableSections = [response valueForKeyPath:@"result.section_block"];
    NSString *unavailableAlert = [response valueForKeyPath:@"result.section_block_message"];
    
    
    [NetworkManager sharedInstance].serverApiVersion = apiVersionString;
    
    //appDenied = YES;
    
    //Доступ на данном сервере специально запрещен
    if (appDenied) {
        Alert(@"Error", @"Sorry, your subscription plan does not support the use of the mobile app");
        [self pinCodeSuccessfulLoginIs:NO decraseAttempt:NO];
        return;
    }
    
    //Приложение не поддерживает версию апи на сервере. Версия должна быть ниже
    if (apiVersionString) {
        if (!([[API_VERSION_THRESHOLD shortenedVersionNumberString] compare:[apiVersionString shortenedVersionNumberString] options:NSNumericSearch] == NSOrderedDescending)) {
            //!(apiVersionString < API_VERSION_THRESHOLD)
            Alert(@"Error", @"Please update your application to the latest version");
            [self pinCodeSuccessfulLoginIs:NO decraseAttempt:NO];
            return;
            
        }
    }
    
    
    
    //Доступ для буккипинга запрещен
    if (acl == 5 ) {
        Alert(@"Error", @"No Access");
        [self pinCodeSuccessfulLoginIs:NO decraseAttempt:NO];
        return;
    }
    
    
    if (betaString && [betaString isKindOfClass:[NSNumber class]] && [betaString intValue] == 1) {
        [NetworkManager sharedInstance].isBeta = YES;
    }
    else {
        [NetworkManager sharedInstance].isBeta = NO;
    }
    
    if (j3String && [j3String isKindOfClass:[NSString class]] && [j3String isEqualToString:@"j3"]) {
        [NetworkManager sharedInstance].isJ3 = YES;
    }
    else {
        [NetworkManager sharedInstance].isJ3 = NO;
    }
    
    [[NetworkManager sharedInstance] updateSessionTimer];
    
    [NetworkManager sharedInstance].baseUrl=self.activeUser.subDomain;
    
    [DataManager sharedInstance].adminId = uid;
    [DataManager sharedInstance].adminAccessLevel = acl;
    [NetworkManager sharedInstance].token=[response valueForKeyPath:@"result.access_token"];
    [NetworkManager sharedInstance].isScottish = v;
    
    [[NetworkManager sharedInstance]getAdminWithId:[DataManager sharedInstance].adminId completion:^(id response, NSError *error) {
        //[DataManager sharedInstance].adminInfo=[response valueForKey:@"admin_info"];
        [DataManager sharedInstance].adminInfo= [NSMutableDictionary dictionaryWithDictionary:[response valueForKey:@"admin_info"]];
    }];
    
    [NetworkManager sharedInstance].videoServerHost = nil;
    NSLog(@"admin id :%d",[DataManager sharedInstance].adminId);
    
    User *user = [[UserManager sharedManager] getUserByUID:uid andSubdomain:self.activeUser.subDomain];
    if (user) {
        self.activeUser = user; //если пользователь во время ожидания ответа кликнет на другого
        user.acl = acl;
        //Для парента заполняем доступные секции
        if (user.userType == UserTypeParent) {
            
            NSDictionary *options = nil;
            NSString *firstName;
            NSString *lastName;
            NSNumber *parentNum;
            @try {
                options = [response valueForKeyPath:@"result.additional_info.options"];
                firstName = [response valueForKeyPath:@"result.additional_info.first_name"];
                lastName = [response valueForKeyPath:@"result.additional_info.last_name"];
                parentNum = [response valueForKeyPath:@"result.parent_num"];
            }
            @catch (NSException *exception) {
                NSLog(@"Exception. Reason: %@", exception.reason);
                NSLog(@"Для parent'а неверный response result");
            }
            [user fillUserOptionsWithDict:options]; //проверка внутри
            
            user.firstName = firstName;
            user.lastName = lastName;
            
            if (parentNum && [parentNum isKindOfClass:[NSNumber class]]) {
                user.parentNum = @([parentNum intValue] + 1);
            }
            
            [[UserManager sharedManager] saveLocalUsers];
            /*
             NSDictionary *result = [
             NSDictionary *additionalInfo = [response objectForKey:@"additional_info"];
             if (additionalInfo && [additionalInfo isKindOfClass:[NSDictionary class]]) {
             NSDictionary *options = [additionalInfo objectForKey:@"options"];
             [user fillUserOptionsWithDict:options]; //проверка внутри
             }
             */
        }
        
        if (user.userType == UserTypeAdmin && [NetworkManager sharedInstance].isJ3) {
            NSDictionary *options = nil;
            @try {
                options = [response valueForKeyPath:@"result.options"];
            }
            @catch (NSException *exception) {
                NSLog(@"Exception. Reason: %@", exception.reason);
                NSLog(@"Для admin неверный response result");
            }
            [user updateAccessLevelWithDict:options]; //проверка внутри
        }
        
        
        [[AccessTimeManager sharedInstance] disableTimer];
        if (user.userType == UserTypeAdmin && user.acl != 1) {
            //time access
            NSNumber *timeLeft = [response valueForKeyPath:@"result.left_time"];
            if (timeLeft && [timeLeft isKindOfClass:[NSNumber class]]) {
                [[AccessTimeManager sharedInstance] startWithSeconds:[timeLeft longLongValue]];
            }
        }

        
        [[UnavailableSectionManager sharedInstance] updateWithArray:unavailableSections andMessage:unavailableAlert];
        
        
        //Переход в register для парентсайда при наличии неподписанных часов
        //[NetworkManager sharedInstance].registerLoginArray = [response valueForKeyPath:@"result.register.ActiveChildrenList"];
        [[RegisterForceManager sharedManager] updateWithRegisterArray:[response valueForKeyPath:@"result.register.ActiveChildrenList"]];
        
        [self pinCodeSuccessfulLoginIs:YES decraseAttempt:NO];
    } else {
        //Alert(@"Error",[response valueForKeyPath:@"result.msg"]);
        [self pinCodeSuccessfulLoginIs:NO decraseAttempt:YES];
    }

    
    
    
    
}


- (void)resetPinDots {
    [self.enteredPinCode setString:@""];
    [self performSelector:@selector(updatePinDotsView) withObject:nil afterDelay:0.5f];
    self.failedAttempts = 0;
    self.buttonsPressedCount = 0;
}

- (void)updatePinDotsView
{
    for (NSInteger tag = 1; tag <= 4; tag++)
    {
        UIView *view = [self.pinDotsView viewWithTag:tag];
        [view setBackgroundColor:[UIColor clearColor]];
    }
}

- (void)animatePinDotsView
{
    
    
    CABasicAnimation *animation =
    [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.05];
    [animation setRepeatCount:3];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([self.pinDotsView center].x - 15.0f, [self.pinDotsView center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([self.pinDotsView center].x + 15.0f, [self.pinDotsView center].y)]];
    [self.pinDotsView.layer addAnimation:animation forKey:@"position"];
    
}

- (void)hightLightView:(UIView *)view {
    view.layer.shadowColor = [[UIColor whiteColor] CGColor];
    view.layer.shadowRadius = 4.0f;
    view.layer.shadowOpacity = .9;
    view.layer.shadowOffset = CGSizeZero;
    view.layer.masksToBounds = NO;
}

- (void)unHightLightView:(UIView *)view {
    view.layer.shadowRadius = 0.f;
    view.layer.shadowOpacity = 1;
}


//Для setColorTheme необходимо ханить ссылки на ячейки
- (void)setCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath {
    //__weak или rows все равно delete при потере актуальности?
    __weak id weakCell = cell;
    
    if (indexPath.section == PDLoginPinSectionSpoilers) {
        [[self.spoilers objectAtIndex:indexPath.row] setObject:weakCell forKey:@"cell"];
    } else if (indexPath.section == PDLoginPinSectionRows) {
        [[self.rows objectAtIndex:indexPath.row] setObject:weakCell forKey:@"cell"];
    }
    
    
}


#pragma mark - Collapse & expand magic

- (void)updateSpoiler {
    if (self.isSpoilerExpanded) {
        self.spoilers = [self makeExpandSpoiler];
    } else {
        self.spoilers = [self makeCollapseSpoilerFromSpoilerArray: [self makeExpandSpoiler]];
    }
}

- (NSArray *)makeExpandSpoiler {
    
    NSArray *array;
    
    if (self.activeUser) {
        array = [self arrayWithOnTopUser:self.activeUser];
    } else {
        array = [NSMutableArray arrayWithArray:self.users];
    }
    
    array = [self spoilerWrapperForUsersArray:array];
    
    return array;
    
}

- (NSArray *)makeCollapseSpoilerFromSpoilerArray:(NSArray *)sourceArray {
    
    NSArray *array = [NSArray arrayWithObject:[sourceArray firstObject]];
    
    return array;
}


- (NSArray *)arrayWithOnTopUser:(User *)user {
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.users];
    [array removeObject:user];
    
    
    NSSortDescriptor *userNameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"description"
                                                                           ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:userNameSortDescriptor];
    
    array = [NSMutableArray arrayWithArray:[array sortedArrayUsingDescriptors:sortDescriptors]];

    
    [array insertObject:user atIndex:0];
    
    return (NSArray *)array;

}

- (NSArray *)spoilerWrapperForUsersArray:(NSArray *)sourceArray {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    //Необходим именно mutableDictionary. Словарь дополняется ссылкой на ячейку
    for (User * user in sourceArray) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                              user, @"user",
                              kPDSpoiler, @"type", nil];
        [array addObject:dict];
    }
    
    NSMutableDictionary *lastRow = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                          kPDSpoilerLoginWithPassword, @"type", nil];
    
    [array addObject:lastRow];
    
    return (NSArray *)array;
    
}

- (void)runExpandOrCollapseTableView:(UITableView *)tableView {
    if (self.isSpoilerExpanded) {
        //Убираем
        [self runCollapseTableView:tableView];
        
    } else {
        //Добавляем
        [self runExpandTableView:tableView];
    }
}


- (void)runExpandTableView:(UITableView *)tableView {
    self.isSpoilerExpanded = YES;
    [self updateSpoiler];
    
    NSInteger rowsAfter = [self.spoilers count];
    
    NSMutableArray *insertRows = [[NSMutableArray alloc] init];
    for (NSInteger row = 1; row < rowsAfter; row++) {
        [insertRows addObject:[NSIndexPath indexPathForRow:row inSection:PDLoginPinSectionSpoilers]];
    }
    
    
    [CATransaction begin];
    
    [CATransaction setCompletionBlock:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateScroll];
        });
    }];
    
    
    [tableView beginUpdates];
    [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:PDLoginPinSectionSpoilers]] withRowAnimation:UITableViewRowAnimationNone];
    [tableView insertRowsAtIndexPaths:insertRows withRowAnimation:UITableViewRowAnimationBottom];
    [tableView endUpdates];
    
    
    [CATransaction commit];
}

- (void)runCollapseTableView:(UITableView *)tableView {
    NSInteger rowsBefore = [self.spoilers count];
    self.isSpoilerExpanded = NO;
    [self updateSpoiler];
    
    NSMutableArray *deleteRows = [[NSMutableArray alloc] init];
    for (NSInteger row = 1; row < rowsBefore; row++) {
        [deleteRows addObject:[NSIndexPath indexPathForRow:row inSection:PDLoginPinSectionSpoilers]];
    }
    
    
    [CATransaction begin];

    [CATransaction setCompletionBlock:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateScroll];
        });
    }];

    
    [tableView beginUpdates];
    [tableView deleteRowsAtIndexPaths:deleteRows withRowAnimation:UITableViewRowAnimationBottom];
    [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:PDLoginPinSectionSpoilers]] withRowAnimation:UITableViewRowAnimationNone];
    [tableView endUpdates];
    
    [CATransaction commit];
}

- (void)freeTapAction:(UITapGestureRecognizer *)tapGestrure {
    if (self.isSpoilerExpanded) {
        //Убираем
        [self runCollapseTableView:self.tableView];
    }
    
    
}

- (void)pinPadUserInteractionEnable:(BOOL)value {
    
    for (int i = 0; i <= 9; i++)
    {
        NSInteger tag = i + 100;
        UIButton *button = (UIButton *)[self.pinPadCell viewWithTag:tag];
        button.userInteractionEnabled = value;
    }
    
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"resetPinSegue"]) {
        ResetPinViewController *vc = segue.destinationViewController;
        vc.user = self.activeUser;
    } else if([segue.identifier isEqualToString:@"loginSegue"]) {
        LoginTableViewController *vc = segue.destinationViewController;
        if (self.activeUser) {
            vc.activeUser = self.activeUser;
        } else {
            vc.addNewUser = YES; //loginWithPassword
        }
        
    }
}
#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == ALERTVIEW_FAILED_ATTEMPTS_TAG) {
        //5 неудачных попыток ввода
        
        //Use password
        if (buttonIndex == 0) {
            [self performSegueWithIdentifier:@"loginSegue" sender:self];
        } else if (buttonIndex == 1) {
            [self performSegueWithIdentifier:@"resetPinSegue" sender:self];
        }
        
    }
}


#pragma mark - Setup Touch Id

- (void)setupTouchIdOrShowMenu {
    
    VPBiometricAuthenticationFacade *biometricFacede = [[VPBiometricAuthenticationFacade alloc] init];
    
    BOOL isFirstLaunchAfterUpdate = [biometricFacede isAuthenticationAvailable] && ![UserManager sharedManager].isUsersWithTouchId && [AppDelegate isFirstLaunchAfterTouchIdUpdate];
    //isFirstLaunchAfterUpdate = NO;
    if (isFirstLaunchAfterUpdate) {
        
        PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:[Utils isFaceIdSupported] ? @"Face ID" : @"Touch ID" message:[Utils isFaceIdSupported] ? @"Do you want to enable Face ID?" : @"Do you want to enable Touch ID?" preferredStyle:PSTAlertControllerStyleAlert];
        
        [alert addAction:[PSTAlertAction actionWithTitle:@"Yes" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
            
            [self setupTouchId];
            
            
        }]];
        
        [alert addAction:[PSTAlertAction actionWithTitle:@"Skip" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
            //User.skip
            [self showMenu];
            
            
        }]];
        
        [alert showWithSender:self controller:self animated:YES
                   completion:nil];
        [AppDelegate appLaunchedAfterTouchIdUpdate];
        
    }
    else {
        [self showMenu];
    }
}


- (void)setupTouchId {
    
    LAContext *context = [[LAContext alloc] init];
    context.localizedFallbackTitle = @"";
    
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:[Utils isFaceIdSupported] ? @"Unlock using Face ID" : @"Touch the Home button to login"
                          reply:^(BOOL success, NSError *error) {
                              
                              if (error) {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      
                                      PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Error" message:[Utils isFaceIdSupported] ? @"Face ID authentication failed" : @"Touch ID authentication failed" preferredStyle:PSTAlertControllerStyleAlert];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Retry" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          
                                          [self setupTouchId];
                                          
                                      }]];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Skip" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          [self showMenu];
                                          
                                      }]];
                                      
                                      [alert showWithSender:nil controller:self animated:YES
                                                 completion:nil];
                                  });
                                  return;
                                  
                              }
                              
                              if (success) {
                                  
                                  User *user = [[UserManager sharedManager] lastLoggedUser];
                                  user.touchIdToken = [AppDelegate randomString];
                                  
                                  
                                  [[NetworkManager sharedInstance] createTouchIdToken:user.touchIdToken forUser:user.uid isParent:user.userType == UserTypeParent ? YES : NO withCompletion:^(id response, NSError *error) {
                                      
                                      if (response)
                                      {
                                          if([[response valueForKeyPath:user.userType == UserTypeAdmin ? @"result.complete" : @"data.result.complete"]intValue]==1)
                                          {
                                              user.isTouchId = YES;
                                              [self showMenu];
                                          }
                                          else
                                          {
                                              Alert(@"Error",[response valueForKeyPath:user.userType == UserTypeAdmin ? @"result.msg" : @"data.result.msg"] );
                                              [self showMenu];                                              
                                          }
                                      }
                                      else if(error)
                                      {
                                          Alert(@"Error", @"Error happened while auth");
                                          [self showMenu];
                                      }
                                  }];
                                  
                              } else {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      
                                      PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Error" message:@"You are not the device owner." preferredStyle:PSTAlertControllerStyleAlert];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Retry" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          
                                          [self setupTouchId];
                                          
                                      }]];
                                      
                                      [alert addAction:[PSTAlertAction actionWithTitle:@"Skip" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
                                          [self showMenu];
                                          
                                      }]];
                                      
                                      [alert showWithSender:nil controller:self animated:YES
                                                 completion:nil];
                                      
                                  });
                              }
                              
                          }];
        
    }/* else {
      
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
      message:@"Your device cannot authenticate using TouchID."
      delegate:nil
      cancelButtonTitle:@"Ok"
      otherButtonTitles:nil];
      [alert show];
      
      }
      */
}
- (void)showMenu {
    
    if ([[UserManager sharedManager] loggedUser].userType == UserTypeAdmin) {
        [self performSegueWithIdentifier:@"@Main_iPhone" sender:self];
    } else {
        [self performSegueWithIdentifier:@"@Main-ParentSide_iPhone" sender:self];
    }
    
}


@end
