//
//  ListPicker2ComponentsWithDelButton.h
//  Exchange
//
//  Created by Andrey on 02.04.15.
//  Copyright (c) 2014 Vexadev LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListPicker2ComponentsWithDelButton : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UIGestureRecognizerDelegate>

@property (copy) void (^handler)(NSString *string0Comp, int index0Comp, NSString *string1Comp, int index1Comp);
@property (copy) void (^finishHandler)();
@property (copy) void (^clearHandler)();


@property int selectedIndexIn0Component;
@property int selectedIndexIn1Component;

@property NSString *selectedStringIn0Component;
@property NSString *selectedStringIn1Component;

@property (nonatomic,retain) NSArray *dataArray0Component;
@property (nonatomic,retain) NSArray *dataArray1Component;

@property (nonatomic,assign) BOOL showClearButton;

@property (nonatomic,retain)IBOutlet UIPickerView *picker;
@property (nonatomic,retain)IBOutlet UIBarButtonItem *doneButton, *cancelButton;

@property (nonatomic,retain)IBOutlet UIBarButtonItem *clearButton;



-(void)show;
-(IBAction)hide;
-(IBAction)clear;


@end
