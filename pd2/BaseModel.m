//
//  BaseModel.m
//  pd2
//
//  Created by Vasyl Balazh on 05.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "BaseModel.h"
#import "NSDateFormatter+Locale.h"

@implementation BaseModel

- (NSString*)safeStringFrom:(NSString*) string
{
    if (string == nil || [string isKindOfClass:[NSNull class]]) {
        return @"";
    }
    return string;
}


- (NSDate *)dateFromString:(NSString *)string
{
    NSDate *dateFromString = [[NSDate alloc] init];
    if (![string isEqual:[NSNull null]]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] initWithSafeLocale];
        [dateFormatter setDateFormat:@"dd.MM.yyyy"];
        dateFromString = [dateFormatter dateFromString:string];
    } else {
        dateFromString = nil;
    }
    
    return dateFromString;
}


- (NSDate *)timeFromString:(NSString *)string
{
    NSDate *dateFromString = [[NSDate alloc] init];
    if (![string isEqual:[NSNull null]]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] initWithSafeLocale];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        dateFromString = [dateFormatter dateFromString:string];
    } else {
        dateFromString = nil;
    }
    
    return dateFromString;
}

- (NSDate *)dateFrom:(id)data withDateFormat:(NSString *)dateFormat
{
    
    NSString *safeString = [self safeStringFrom:data];
    
    if (!safeString) {
        return nil;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] initWithSafeLocale];
    [dateFormatter setDateFormat:dateFormat];
    
    return [dateFormatter dateFromString:safeString];
    
}


- (NSNumber*)safeNumberFromString:(id)string
{
    if([[string class] isSubclassOfClass:[NSNumber class]]){
        return string;
    }else{
        NSString *str = (NSString*)string;
        if(![string isKindOfClass:[NSNull class]] && str && str.length > 0 && ![str isEqualToString:@""]){
            NSNumberFormatter *nf = [[NSNumberFormatter alloc]init];
            [nf setNumberStyle:NSNumberFormatterDecimalStyle];
            return [nf numberFromString:str];
        }else{
            return [NSNumber numberWithInt:0];
        }
    }
}


- (id)objectOrNull:(id)object
{
    return object ?: [NSNull null];
}

@end
