//
//  AXViewController.h
//  pd2
//
//  Created by i11 on 28/04/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Pd2ViewController.h"

#import "AXSections.h"
#import "AXSection.h"
#import "AXHeader.h"
#import "AXRow.h"
#import "InfoCell.h"
#import "InfoPicCell.h"
#import "FilterCell.h"
#import "RoomHeaderCell.h"
#import "UIView+superCell.h"

#import "IconButton.h"
#import "CheckboxButton.h"
#import "ConfirmButton.h"
#import "CancelButton.h"
#import "TextField.h"

#import "DataStateManager.h"
#import "DataManager.h"
#import "AXDataCheck.h"
#import "NSDate+Utilities.h"
#import "NSString+stringIsEmpty.h"

#import "PSTAlertController.h"

#import "Pd2TableViewController.h"

#import "RoundedTextView.h"

#define MODEL @"model"

#define CONTAINER_VIEW_TAG 250

#define HEADER_ARROW_IMAGE_VIEW_TAG 150
#define HEADER_LABEL_TAG 200

#define FILTER_TEXT_FIELD_TAG 270




@protocol AXTableViewControllerUpdateDelegate;

@interface AXViewController : Pd2ViewController <AXTableViewControllerUpdateDelegate>

{

    NSString *PDTopSection;
    NSString *PDMainSection;
    NSString *PDBottomSection;
    NSString *PDSpacerSection;
    
    NSString *PDHeader;
    NSString *PDHeaderWBottomBorder;
    NSString *PDHeaderWTopBorder;
    NSString *PDRoomHeader;
    
    NSString *PDInfoCell;
    NSString *PDInfoPicCell;
    NSString *PDInfoSecondCell;

    NSString *PDSpoilerCell;
    NSString *PDSpacerCell;
    NSString *PDFilterCell;
    NSString *PDLoadingCell;
    NSString *PDRefreshCell;
    NSString *PDMessageCell;
    
    //Labels for replacements
    NSString *PDInfoLabel;
    
}


@property (nonatomic, strong) AXSections *sections;

@property (nonatomic, strong) NSMutableDictionary *autoLayoutCells;
@property (nonatomic, strong) NSMutableSet *infoCells;
@property (nonatomic, strong) NSMutableSet *contentFitCells;

@property (nonatomic, strong) UITapGestureRecognizer *freeTapGestureRecognizer;

@property (nonatomic, weak) UITableView *tableView;

- (void)initRows;
- (void)initLabels;

- (id)offscreenRenderingCell:(id)cell withRow:(AXRow *)row;

//operations with rows
- (void)insertLoadingCell;
- (void)insertLoadingCellWithAnimation;
- (void)insertMessageCell;
- (void)updateInfoCell;
- (void)cleanForRefresh;
- (void)cleanForRefreshWithAnimation;
- (void)deleteRows:(NSArray *)rows;
- (void)deleteSections:(NSArray *)sections;
- (void)updateRowCellWithRemovalActiovityIndicator:(AXRow *)row;

- (void)updateHeaderArrowImage:(UIImageView *)imageView withState:(BOOL)state;

//
- (AXRow *)rowForView:(UIView *)view;
- (NSIndexPath *)indexPathForView:(UIView *)view;
- (BOOL)isRowOnScreen:(AXRow *)row;
- (BOOL)isCellOnScreen:(UITableViewCell *)cell;
- (BOOL)isOneOfCellsOnScreen:(NSArray *)cellIndexPaths;
- (BOOL)isCellAboveVisibleArea:(NSIndexPath *)indexPath;
- (BOOL)isCellCompletelyVisibleByRow:(AXRow *)row;
- (BOOL)isCellCompletelyVisibleByIndexPath:(NSIndexPath *)indexPath;



//update Row delegate
@property (nonatomic, weak) id <AXTableViewControllerUpdateDelegate> delegate;
@property (nonatomic, weak) AXRow *sourceRow;


//string replacement
- (void)remember:(id)source withKey:(NSString *)attributedStringKey;
- (NSMutableAttributedString *)attributedStringWithKey:(NSString *)attributedStringKey
                                             forObject:(id)source;
- (NSMutableAttributedString *)replace:(NSString *)sourceString
                            withString:(NSString *)string
                    inAttributedString:(NSMutableAttributedString **)attributedString;
- (NSMutableAttributedString *)replace:(NSString *)sourceString
                            withString:(NSString *)string
                    inAttributedString:(NSMutableAttributedString **)attributedString
                       andSetTextColor:(UIColor *)color;
- (NSMutableAttributedString *)replace:(NSString *)sourceString withAttributedString:(NSAttributedString *)string inAttributedString:(NSMutableAttributedString **)attributedString;

- (void)freeTapAction:(id)sender;
- (void)smartPerformSegue:(NSString *)segueIdentifier withDestinationControllerClass:(Class)destinationControllerClass;

//Utils
- (NSString *)stringFromDate:(NSDate *)date withDateFormat:(NSString *)dateFormat;
- (NSAttributedString *)name:(NSString *)name descr:(NSString *)descr onNewLine:(BOOL)isOnNewLine;
- (NSAttributedString *)string:(NSString *)string onNewLine:(BOOL)isOnNewLine;
- (NSAttributedString *)newLineAttributed;


- (void)scrollToCursorForTextView:(UITextView *)textView;

- (void)addBorder:(PDBorderType)borderType toView:(UIView *)view forToolbar:(BOOL)toolbar;

@end


@protocol AXTableViewControllerUpdateDelegate

-(void)updateRow:(AXRow *)row;
-(void)updateIndexPaths:(NSArray *)indexPaths;

@end
