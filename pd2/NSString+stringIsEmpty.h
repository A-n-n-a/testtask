//
//  NSString+isEmpty.h
//  pd2
//
//  Created by i11 on 14/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (stringIsEmpty)

+ (BOOL)stringIsEmpty:(NSString *)aString;

@end
