//
//  NSString+isEmpty.h
//  pd2
//
//  Created by Andrey on 17/10/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (isEmpty)

- (BOOL)isEmpty;
    

@end
