//
//  DailyNotesAddNotePendingCell.m
//  pd2
//
//  Created by Andrey on 8/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesAddNotePendingCell.h"
#import "DailyNotesNoteModel.h"

@implementation DailyNotesAddNotePendingCell


- (void)updateWithModel:(DailyNotesNoteModel *)model {
    
    self.checkboxImageView.image = model.isPending ? [UIImage imageNamed:@"checked"] : [UIImage imageNamed:@"unchecked"];
    
}




@end
