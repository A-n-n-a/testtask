//
//  UIImage+ContainsImage.m
//  pd2
//
//  Created by Andrey on 05/02/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

#import "UIImage+ContainsImage.h"

@implementation UIImage (ContainsImage)


- (BOOL)containsImage {
    
    CGImageRef cgref = [self CGImage];
    CIImage *cim = [self CIImage];
    
    if (cim != nil || cgref != NULL) { // contains image
        return YES;
    }
    
    return NO;
}


@end
