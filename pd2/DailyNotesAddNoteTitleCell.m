//
//  DailyNotesAddNoteTitleCell.m
//  pd2
//
//  Created by Andrey on 28/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "DailyNotesAddNoteTitleCell.h"
#import "DailyNotesNoteModel.h"

@implementation DailyNotesAddNoteTitleCell



- (void)updateWithModel:(DailyNotesNoteModel *)model {
    
    self.noteTitleTextField.text = model.title;
    self.dateTextField.text = model.date ? [model.date printableDefault] : @"";

}

@end
