//
//  ImageCache.m
//  pd2
//
//  Created by User on 27.11.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "ImageCache.h"


@implementation ImageCache

-(instancetype)init
{
    self=[super init];
    if (self)
    {
        _cache=[[NSMutableDictionary alloc]init];
    }
    return self;
}

-(void)saveImage:(UIImage *)image fromLink:(NSString*)link
{
    [_cache setValue:image forKey:link];
}

-(UIImage *)imageFromLink:(NSString *)link
{
    UIImage *image=nil;
    if ([[_cache valueForKey:link] isKindOfClass:[UIImage class]])
    {
        image=[_cache valueForKey:link];
    }
    return image;
}

-(void)deleteImageForLink:(NSString*)link
{
    if (!link || ![link isKindOfClass:[NSString class]] || !link.length) {
        return;
    }
    
    
    for (NSString *key in _cache.allKeys)
    {
        if ([key containsString:link] || [key containsString:[link stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]])
        {
            [_cache removeObjectForKey:key];
        }
    }
}


@end
