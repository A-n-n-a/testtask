//
//  TwoLineSegmentedControl.m
//  pd2
//
//  Created by User on 27.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "TwoLineSegmentedControl.h"

@implementation TwoLineSegmentedControl

-(void)awakeFromNib
{
    self.superview.backgroundColor=[[MyColorTheme sharedInstance]spoilerBackGroundColor];
    self.backgroundColor=[[MyColorTheme sharedInstance]spoilerBackGroundColor];
    self.layer.borderWidth=1.0;
    self.layer.borderColor=[[[MyColorTheme sharedInstance]backgroundColor]CGColor];
    self.layer.cornerRadius=5.0;
    self.clipsToBounds=YES;
    _buttons = [NSMutableArray array];
    if (_b1) {
        [_buttons addObject:_b1];
    }
    if (_b2) {
        [_buttons addObject:_b2];
    }
    if (_b3) {
        [_buttons addObject:_b3];
    }
    if (_b4) {
        [_buttons addObject:_b4];
    }
    if (_b5) {
        [_buttons addObject:_b5];
    }
    if (_b6) {
        [_buttons addObject:_b6];
    }
    
    //_buttons=@[_b1,_b2,_b3,_b4,_b5,_b6];
    for (UIButton *b in _buttons)
    {
        [b setTitleColor:[[MyColorTheme sharedInstance]backgroundColor] forState:UIControlStateNormal];
        b.layer.borderWidth=1.0;
        b.layer.borderColor=[[[MyColorTheme sharedInstance]backgroundColor]CGColor];
        [b addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self buttonPressed:_b1];
}

-(void)buttonPressed:(id)sender
{
    NSLog(@"sender:%@",sender);
    for (UIButton *button in _buttons)
    {
        [button setTitleColor:[[MyColorTheme sharedInstance]backgroundColor] forState:UIControlStateNormal];
        button.backgroundColor=[UIColor clearColor];
    }
    UIButton *b=sender;
    b.backgroundColor=[[MyColorTheme sharedInstance]backgroundColor];
    [b setTitleColor:[[MyColorTheme sharedInstance]spoilerBackGroundColor] forState:UIControlStateNormal];
    _selectedButton=(int)b.tag;
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    //[_delegate segControlValueChanged:self];
}


-(void)setActiveIndex:(int)index {
    
    if (index > [self.buttons count] - 1) {
        return;
    }
    UIButton *button = [self.buttons objectAtIndex:index];
    
    
    for (UIButton *button in _buttons)
    {
        [button setTitleColor:[[MyColorTheme sharedInstance]backgroundColor] forState:UIControlStateNormal];
        button.backgroundColor=[UIColor clearColor];
    }
    
    
    button.backgroundColor=[[MyColorTheme sharedInstance]backgroundColor];
    [button setTitleColor:[[MyColorTheme sharedInstance]spoilerBackGroundColor] forState:UIControlStateNormal];
    _selectedButton=(int)button.tag;

    
    
    
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
