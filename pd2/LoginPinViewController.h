//
//  PincodeViewController.h
//  pd2
//
//  Created by i11 on 22/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Pd2TableViewController.h"

@interface LoginPinViewController : Pd2TableViewController <UIAlertViewDelegate>
- (IBAction)pinButtonTouchUpInside:(id)sender;
- (IBAction)cancelAction:(id)sender;

- (IBAction)resetPinAction:(UIButton *)sender;
- (IBAction)selectThemeAction:(UIButton *)sender;
@end
