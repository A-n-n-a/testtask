//
//  DomainCompanyModel.m
//  pd2
//
//  Created by Andrey on 19/01/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DomainCompanyModel.h"

@implementation DomainCompanyModel




- (instancetype)initWithDomain:(NSString *)domain subdomain:(NSString *)subdomain companyName:(NSString *)companyName {
    self = [super init];
    if (self) {
        
        self.domain = [self safeString:domain];
        self.subdomain = [self safeString:subdomain];
        self.companyName = [self safeString:companyName];
        
        //Todo - delete
        //self.companyName = [NSString stringWithFormat:@"%@ - Company",[self safeString:companyName]];
        
        if (!self.companyName) {
            self.companyName = self.subdomain;
        }
        
    }
    return self;
    
}



- (BOOL)isExist {
    
    if (self.domain && self.subdomain && self.companyName) {
        return YES;
    }
    
    return NO;
}



@end
