#import <UIKit/UIKit.h>

@interface UIView(Frame)

@property (nonatomic) CGFloat x, y, height, width;

- (CGFloat) x;
- (void) setX:(CGFloat)x;
- (CGFloat) y;
- (void) setY:(CGFloat)y;
- (CGFloat) width;
- (void) setWidth:(CGFloat)width;
- (CGFloat) height;
- (void) setHeight:(CGFloat)height;

- (void) increaseYPos:(CGFloat)value;
- (void) increaseHeight:(CGFloat)value;
- (void) setFrameHeight:(CGFloat)value;

// Constraints
- (void) clearConstraints:(NSLayoutAttribute)attribute;
- (void) clearHeightConstraints;
- (void) clearTopOffsetConstraints;
- (void) clearBottomOffsetConstraints;

@end
