//
//  WL.m
//  pd2
//
//  Created by Andrey on 11/04/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "WL.h"
#import "MyColorTheme.h"

@implementation WL

+ (NSString *)name {
    return @"Baby's Days";
}

+ (NSString *)appStoreLink {
    return @"https://itunes.apple.com/qa/app/babys-days/id1059695172?mt=8";
}

+ (NSNumber *)index {
    return nil;
}

+ (void)applyColorTheme {
    [[MyColorTheme sharedInstance] setBlueTheme];
}

+ (NSString *)themeName {
    return @"0 - Blue";
}



+ (NSString *)logoSplashFileName {
    return @"baby-days-logo-final-white";
}

+ (CGRect)logoSplashRect {
    return CGRectMake(86.f, 113.f, 149.f, 43.f);
}


+ (NSString *)logoPasswordLoginFileName {
    return @"baby-days-logo-final-white";
}

+ (CGRect)logoPasswordLoginRect {
    return CGRectMake(86.f, 45.f, 149.f, 43.f);
}


+ (NSString *)logoResetPinFileName {
    return @"baby-days-logo-final-white";
}

+ (CGRect)logoResetPinRect {
    return CGRectMake(89.f, 52.f, 149.f, 43.f);
}


+ (NSString *)logoSetupPinFileName {
    return @"baby-days-logo-final-white";
}

+ (CGRect)logoSetupPinRect {
    return CGRectMake(86.f, 26.f, 149.f, 43.f);
}

+ (NSString *)logoMenuFileName {
    return @"baby-days-logo-final-white";
}


//

@end
