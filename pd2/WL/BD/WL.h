//
//  WL.h
//  pd2
//
//  Created by Andrey on 11/04/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WL : NSObject

+ (NSString *)name;
+ (NSString *)appStoreLink;
+ (NSNumber *)index;
+ (NSString *)themeName;

+ (NSString *)logoSplashFileName;
+ (CGRect)logoSplashRect;
+ (NSString *)logoPasswordLoginFileName;
+ (CGRect)logoPasswordLoginRect;
+ (NSString *)logoResetPinFileName;
+ (CGRect)logoResetPinRect;
+ (NSString *)logoSetupPinFileName;
+ (CGRect)logoSetupPinRect;
+ (NSString *)logoMenuFileName;

//+ (void)applyColorTheme;
@end
