//
//  WL.m
//  pd2
//
//  Created by Andrey on 11/04/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "WL.h"
#import "MyColorTheme.h"

@implementation WL

+ (NSString *)name {
    return @"Rainbow Angels Nurseries";
}

+ (NSString *)appStoreLink {
    //return @"https://itunes.apple.com/qa/app/stars-day-nurseries/id1184927304?mt=8";
    return @"https://itunes.apple.com/";
}

+ (NSNumber *)index {
    return @1000;
}

+ (void)applyColorTheme {
    [MyColorTheme sharedInstance];
    [[MyColorTheme sharedInstance] setYellowTheme];
}

+ (NSString *)themeName {
    return @"2 - Yellow";
}


+ (NSString *)logoSplashFileName {
    return @"ra_logo_rainbow";
}

+ (CGRect)logoSplashRect {
    return CGRectMake(57.f, 172.f, 206.f, 160.f);
    

}

+ (NSString *)logoPasswordLoginFileName {
    return @"ra_logo_rainbow";
}
+ (CGRect)logoPasswordLoginRect {
    return CGRectMake(32.f, 34.f, 256.f, 85.f);
}


+ (NSString *)logoResetPinFileName {
    return @"ra_logo";
}
+ (CGRect)logoResetPinRect {
    return CGRectMake(80.f, 50.f, 160.f, 43.f);
}


+ (NSString *)logoSetupPinFileName {
    return @"ra_logo";
}
+ (CGRect)logoSetupPinRect {
    return CGRectMake(80.f, 26.f, 149.f, 43.f);
}


+ (NSString *)logoMenuFileName {
    return @"ra_logo";
}

@end
