//
//  WL.m
//  pd2
//
//  Created by Andrey on 11/04/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "WL.h"
#import "MyColorTheme.h"

@implementation WL

+ (NSString *)name {
    return @"Stars Day Nurseries";
}

+ (NSString *)appStoreLink {
    return @"https://itunes.apple.com/qa/app/stars-day-nurseries/id1184927304?mt=8";
}

+ (NSNumber *)index {
    return @3;
}

+ (void)applyColorTheme {
    [MyColorTheme sharedInstance];
    [[MyColorTheme sharedInstance] setYellowTheme];
}

+ (NSString *)themeName {
    return @"2 - Yellow";
}


+ (NSString *)logoSplashFileName {
    return @"sdn_logo_circle";
}

+ (CGRect)logoSplashRect {
    return CGRectMake(57.f, 172.f, 206.f, 160.f);
    

}

+ (NSString *)logoPasswordLoginFileName {
    return @"sdn_logo_circle";
}
+ (CGRect)logoPasswordLoginRect {
    return CGRectMake(32.f, 34.f, 256.f, 85.f);
}


+ (NSString *)logoResetPinFileName {
    return @"sdn_logo_train";
}
+ (CGRect)logoResetPinRect {
    return CGRectMake(80.f, 50.f, 160.f, 43.f);
}


+ (NSString *)logoSetupPinFileName {
    return @"sdn_logo_train";
}
+ (CGRect)logoSetupPinRect {
    return CGRectMake(80.f, 26.f, 149.f, 43.f);
}


+ (NSString *)logoMenuFileName {
    return @"sdn_logo_train";
}

@end
