//
//  DailyNotesChildrenNoteCommentsViewController.h
//  pd2
//
//  Created by Andrey on 08/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXViewController.h"

@class DailyNotesChildrenModel, DailyNotesChildrenNoteModel;

@interface DailyNotesChildrenNoteCommentsViewController : AXViewController

@property (nonatomic, strong) DailyNotesChildrenModel *childrenModel;
@property (nonatomic, assign) int noteId;



@property (nonatomic, weak) IBOutlet RoundedTextView *messageTextField;
@property (nonatomic, weak) IBOutlet UIView *bottomContainerView;
@property (nonatomic, weak) IBOutlet Buttons *actionButton;

@property (nonatomic, assign) BOOL isPush;
@property (nonatomic, assign) int pushDialogId;


- (IBAction)sendMessage:(id)sender;

//reload
- (void)refreshPullData;

@end
