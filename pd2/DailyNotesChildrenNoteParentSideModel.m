//
//  DailyNotesChildrenNoteParentSideModel.m
//  pd2
//
//  Created by Andrey on 09/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesChildrenNoteParentSideModel.h"
#import "NSDate+Printable.h"

@implementation DailyNotesChildrenNoteParentSideModel
/*
 {
 "id": "615",
 "title": "12th Dec 2017 @ 13:35",
 "first_name": "Andrey",
 "last_name": "Name",
 "date": "2017-12-12 00:00:00",
 "comments": "0",
 "viewed": "0",
 "unviewed": "0"
 }
*/


- (instancetype)initWithDict:(NSDictionary *)dict {
    
    self = [super initWithDict:dict];
    if (self) {
        
        self.noteId = [self safeNumber:dict[@"id"]];
        self.noteTitle = [self safeString:dict[@"title"]];
        //self.note = [self safeString:dict[@"note"]];
        self.adminFirstName = [self safeString:dict[@"first_name"]];
        self.adminLastName = [self safeString:dict[@"last_name"]];
        self.date = [self dateFrom:dict[@"date"] withDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        self.commentsCount = [self safeInt:dict[@"comments"]];
    
    }
    return self;
    
}

/*
 {
 "id": "615",
 "title": "12th Dec 2017 @ 13:35",
 "first_name": "Andrey",
 "last_name": "Name",
 "date_added": "2017-12-12 00:00:00",
 "description": "Test",
 "comments": []
 },
 */


- (instancetype)initWithViewDict:(NSDictionary *)dict {
    
    self = [super initWithDict:dict];
    if (self) {
        
        self.noteId = [self safeNumber:dict[@"id"]];
        self.noteTitle = [self safeString:dict[@"title"]];
        self.note = [self safeString:dict[@"description"]];
        self.adminFirstName = [self safeString:dict[@"first_name"]];
        self.adminLastName = [self safeString:dict[@"last_name"]];
        self.date = [self dateFrom:dict[@"date_added"] withDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        self.commentsCount = [self safeInt:dict[@"comments"]];
        
    }
    return self;
    
}



- (BOOL)isExist {
    
    if (self.noteId && self.date) {
        return YES;
    }
    
    return NO;
    
}



//Filter
- (BOOL)modelContain:(NSString *)containString {
    
    if (!containString || ![containString isKindOfClass:[NSString class]]) {
        return YES;
    }
    
    if (!containString.length) {
        return YES;
    }
    
    if (self.adminFirstName && [self.adminFirstName rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    if (self.adminLastName && [self.adminLastName rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    if (self.noteTitle && [self.noteTitle rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    if (self.date && [[self.date printableDefault] rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    return NO;
    
}




@end
