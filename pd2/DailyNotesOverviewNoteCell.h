//
//  DailyNotesOverviewNoteCell.h
//  pd2
//
//  Created by Andrey on 08.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@class DailyNotesPendingNoteModel;

@interface DailyNotesOverviewNoteCell : AXTableViewCell


@property (nonatomic, weak) IBOutlet UILabel *noteTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *noteBoxLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *childrenCountLabel;

@property (nonatomic, weak) IBOutlet UIView *toolbarView;
@property (nonatomic, weak) IBOutlet UIView *toolbar1View;
@property (nonatomic, weak) IBOutlet UIView *toolbar2View;

- (void)updateWithModel:(DailyNotesPendingNoteModel *)model;


@end
