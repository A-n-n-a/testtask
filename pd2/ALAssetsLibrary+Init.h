#import <AssetsLibrary/AssetsLibrary.h>

@interface ALAssetsLibrary (ALAssetsLibrary_Init)

+ (ALAssetsLibrary *)defaultAssetsLibrary;

@end
