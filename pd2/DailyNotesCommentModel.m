//
//  DailyNotesCommentModel.m
//  pd2
//
//  Created by i11 on 08/06/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "DailyNotesCommentModel.h"

@implementation DailyNotesCommentModel


/*
 "id": "7",
 "created_by_id": "1778",
 "first_name": "asdf",
 "last_name": "asdf",
 "is_parent": "1",
 "datetime": "2017-11-01 13:08:14",
 "comment": "qweqwe",
 "viewed": "1" */

- (instancetype)initWithDict:(NSDictionary *)dict {
    self = [super initWithDict:dict];
    if (self) {
        
        self.commentId = [self safeNumber:dict[@"id"]];
        self.firstName = [self safeString:dict[@"first_name"]];
        self.lastName = [self safeString:dict[@"last_name"]];
        self.photoPath = [self safeString:dict[@"image"]];
        self.text = [self safeStringAndDecodingHTML:dict[@"comment"]];
        self.date = [self dateFrom:dict[@"datetime"] withDateFormat:@"yyyy-MM-dd HH:mm:ss"];

        
    }
    return self;
}


/*
{
    "id":"26",
    "first_name":"Andrey ",
    "last_name":"Developer",
    "datetime":"2017-11-13 11:54:39"
}
*/

- (void)updateWithDict:(NSDictionary *)dict {
    
    if (!dict || ![dict isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    self.commentId = [self safeNumber:dict[@"id"]];
    self.firstName = [self safeString:dict[@"first_name"]];
    self.lastName = [self safeString:dict[@"last_name"]];
    self.photoPath = [self safeString:dict[@"image"]];
    
    self.date = [self dateFrom:dict[@"datetime"] withDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
}



- (BOOL)isExist {
    
    BOOL isExist =  self.firstName && self.lastName && self.text && self.date;
    
    if (!isExist) {
        NSLog(@"Model isExist = NO");
    }
    
    return isExist;
    
}


@end
