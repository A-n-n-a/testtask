//
//  AXRow.h
//  pd2
//
//  Created by i11 on 29/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const AXRowFirst;
extern NSString * const AXRowLast;
extern NSString * const AXRowAll;


@protocol AXRowDelegate;
@class AXSection;

@interface AXRow : NSObject


@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSMutableSet *tags;

@property (assign, nonatomic) CGFloat height;
@property (strong, nonatomic) NSNumber *heightNumber;
@property (strong, nonatomic) NSNumber *estimatedHeightNumber;
//@property (assign, nonatomic) BOOL isDynamicHeight; //было в шаблоне для генерации. Не используем
@property (strong, nonatomic) NSString *identifier;
@property (weak, nonatomic) UITableViewCell *cell; //cell for fast access

@property (assign, nonatomic) BOOL isForRemoval; //пометка к "удалению"
@property (assign, nonatomic) BOOL isChecked; //пометка 
@property (assign, nonatomic) BOOL isProgress; //пометка в "прогрессе"
@property (assign, nonatomic) BOOL isLast; //Пометка что это последний row в группе

//fast access
@property (strong, nonatomic) id object; //container for some object
@property (strong, nonatomic) id model; //container for model
@property (strong, nonatomic) NSString *text;
@property (assign, nonatomic) NSInteger valueInteger;
@property (strong, nonatomic) NSMutableDictionary *userInfo;
@property (weak, nonatomic) UIImageView *imageView; 

@property (weak, nonatomic) id <AXRowDelegate> delegate;
@property (strong, nonatomic, readonly) NSIndexPath *indexPath;
@property (strong, nonatomic, readonly) AXSection *section; //fast access

- (instancetype)initWithType:(NSString *)type;

- (BOOL)isHeightAvailable;
- (void)needUpdateHeight;
@end

@protocol AXRowDelegate
@required
- (NSIndexPath *)indexPathForRow:(AXRow *)row;

@end

