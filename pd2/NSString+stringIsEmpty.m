//
//  NSString+isEmpty.m
//  pd2
//
//  Created by i11 on 14/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "NSString+stringIsEmpty.h"

@implementation NSString (stringIsEmpty)

+ (BOOL)stringIsEmpty:(NSString *)aString {
    
    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }
    
    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    } else {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }
    
    return NO;
}


@end
