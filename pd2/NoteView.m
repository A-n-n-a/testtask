//
//  NoteView.m
//  pd2
//
//  Created by User on 15.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "NoteView.h"

@implementation UITextView (boldness)

-(void)makeFirstLineBold
{
    [self makeFirstLineBoldAndOtherStrings:nil];
}

-(void)makeFirstLineBoldAndOtherStrings:(NSArray *)strings
{
    NSMutableAttributedString  *str=[[NSMutableAttributedString alloc]initWithString:self.text];
    
    int newline = (int)[self.text rangeOfString:@"\n"].location;
    
    [str addAttribute:NSFontAttributeName
                value:[UIFont systemFontOfSize:11]
                range:NSMakeRange(0, str.length)];
    
    [str addAttribute:NSFontAttributeName
                value:[UIFont fontWithName:@"HelveticaNeue-Medium" size:11]
     range:NSMakeRange(0, newline)];
    
    [str addAttribute:NSForegroundColorAttributeName
                value:[[MyColorTheme sharedInstance]textViewTextColor]
                range:NSMakeRange(0, str.length)];
    
    self.backgroundColor=[UIColor clearColor];
    
    for (NSString *s in strings)
    {
        NSRange r=[self.text rangeOfString:s];
        if (r.length)
        {
            [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Medium" size:11] range:r];
        }
    }
    self.attributedText=str;
}

@end


@implementation NoteView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib
{
    NSLog(@"noteView.awakeFromNib");
    self.backgroundColor=[[MyColorTheme sharedInstance]textViewBackGroundColor];
    for (UIView *v in self.subviews)
    {
        if ([v isKindOfClass:[UITextView class]])
        {
            _textView=(UITextView*)v;
            _textView.textColor=[[MyColorTheme sharedInstance]textViewTextColor];
            _textView.backgroundColor=[UIColor clearColor];
            NSLog(@"textContainerInset:%f %f %f %f",_textView.textContainerInset.left,_textView.textContainerInset.top,_textView.textContainerInset.right,_textView.textContainerInset.bottom);
            _textView.layoutManager.allowsNonContiguousLayout = NO;
            [_textView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(printInfo)]];
            _textView.clipsToBounds=NO;
        }
    }
}

-(void)printInfo
{
    NSLog(@"noteView.textView:%@",_textView.description);
}

-(void)setLessHeight:(bool)lessHeight
{
    _lessHeight=lessHeight;
    [self layoutSubviews];
}

-(void)layoutSubviews
{
    [super layoutSubviews];

    NSLog(@"noteView.layoutSubviews.topView:%d",self.topView);
    int xOffsetDefault=6,yOffsetDefault=2, topInset=(self.topView?13:13);
    
    CGRect frame;
    frame.origin=CGPointMake(0, 0);
    frame.size=CGSizeMake(320, [self heightForThisView]);
    _textView.frame=frame;
    _textView.textContainerInset=UIEdgeInsetsMake(topInset-yOffsetDefault, 17-xOffsetDefault, 13-yOffsetDefault, 17-xOffsetDefault);
    _textView.contentSize=frame.size;
    _textView.clipsToBounds=NO;

    
    CGRect viewFrame=self.frame;
    viewFrame.size=CGSizeMake(320, frame.size.height-5);
    self.frame=viewFrame;
    
    //NSLog(@"noteView.layoutSubviews.textView:%@",_textView);
    [super layoutSubviews];
}

-(CGFloat)heightForThisView
{
    [self.textView sizeThatFits:CGSizeMake(320-34, 1000)];
    CGRect rect=[self.textView.attributedText boundingRectWithSize:CGSizeMake(320-34, 1000) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGFloat height=rect.size.height+2+(self.topView?17:13)+13;
    NSLog(@"heightForThisView:%f",height);
    return height;
}

@end
