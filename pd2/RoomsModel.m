//
//  RoomsModel.m
//  pd2
//
//  Created by Andrey on 05/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "RoomsModel.h"
#import "RoomModel.h"

@implementation RoomsModel



/*
 {
 adminRoom = 0;
 capacity = 10;
 childRoom = 4;
 from = 12;
 id = 1;
 image = "images/sted/rooms/default.jpg";
 ordering = 0;
 published = 0;
 ratio = 1;
 title = Default;
 to = 36;
 }
*/


- (instancetype)initWithArray:(NSArray *)array {

    self = [super init];
    if (self) {
        
        NSMutableArray *rooms = [NSMutableArray array];
        
        if (array && [array isKindOfClass:[NSArray class]]) {
            
            for (NSDictionary *dict in array) {
                RoomModel *room = [[RoomModel alloc] initWithDict:dict];
                if (room && [room isExist]) {
                    [rooms addObject:room];
                }
            }
            
        }
       
        self.rooms = rooms;
    }
    return self;
    
}


- (NSArray *)roomList {
    
    NSMutableArray *array = [NSMutableArray array];
    for (RoomModel *room in self.rooms) {
        [array addObject:room.roomName];
    }
    
    return [NSArray arrayWithArray:array];
    
}

- (RoomModel *)roomModelByRoomId:(int)roomId {
    
    for (RoomModel *room in self.rooms) {
        if ([room.roomId intValue] == roomId) {
            return room;
        }
    }
    
    return nil;
}

- (RoomModel *)roomModelByRoomName:(NSString *)roomName {
    
    for (RoomModel *room in self.rooms) {
        if ([room.roomName isEqual:roomName]) {
            return room;
        }
    }
    
    return nil;
}
/*
- (BOOL)isExist {
    
    if (self.roomName && self.roomId) {
        return YES;
    }
    
    return NO;
}
*/




@end
