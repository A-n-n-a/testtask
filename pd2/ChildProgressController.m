#import "ChildProgressController.h"

// Cells
#import "InfoPicCell.h"
#import "ChildProgressButtonsCell.h"
#import "ChildProgressFilterCell.h"
#import "SpoilerCell.h"
#import "ChildProgressCell.h"
#import "ChildProgressHeaderCell.h"
#import "ChildProgressAolsCell.h"
#import "ChildProgressTabsCell.h"

// Controllers
#import "GalleryPhotoListGridViewController.h"
#import "DSImagePickerViewController.h"
#import "ProgressPhotosUploadController.h"
#import "AddNextStepsViewController.h"
#import "AddObsToChildrensController.h"
#import "EditObsController.h"
#import "GalleryVideoGridCell.h"
#import "GalleryVideoUploadViewController.h"
#import "GalleryVideoListGridViewController.h"

#import "CTAssetsPickerController.h"
#import "CTAssetsPageViewController.h"

// View
#import "ConfirmButton.h"
#import "DateTextField.h"

// Logic
#import "ChildInfoModel.h"
#import "NSDate+Utilities.h"
#import "NSDateFormatter+Convert.h"
#import "AOLPlist.h"
#import "UploadsQueue.h"

// Models
#import "GalleryPhotoChildModel.h"
#import "GalleryVideoChildModel.h"
#import "GalleryVideoChildModel.h"


static NSString * kCellButtonsIdentifier  = @"buttonsCell";
static NSString * kCellFilterIdentifier   = @"filterCell";
static NSString * kCellSpoilerIdentifier  = @"spoilerCell";
static NSString * kCellAolsIdentifier     = @"aolsCell";
static NSString * kCellTabsIdentifier     = @"tabsCell";
static NSString * kCellButtonIdentifier   = @"buttonCell";
static NSString * kCellInfoIdentifier     = @"infoCell";
static NSString * kCellProgressIdentifier = @"progressCell";
static NSString * kCellProgressHeaderIdentifier = @"progressHeaderCell";
static NSString * kCellProgressSectionHeaderIdentifier = @"progressSectionHeader";

@interface ChildProgressController () <DSImagePickerDelegate, UITextFieldDelegate, EditObsControllerDelegate, CTAssetsPickerControllerDelegate, UIPopoverControllerDelegate, ChildProgressTabsCellDelegate, AddObsToChildrensControllerDelegate, UIActionSheetDelegate, DateTextFieldDelegate>


//nsnum что выбираем, photo или video
typedef NS_ENUM(NSUInteger, ChildProgressControllerFileType) {
    ChildProgressControllerFileTypeNone,
    ChildProgressControllerFileTypePhoto,
    ChildProgressControllerFileTypeVideo
};


// Images for selected tab
@property (strong, nonatomic) NSMutableArray      * array_images;

// Progress data
@property (strong, nonatomic) NSMutableDictionary * dict_items;
@property (strong, nonatomic) NSMutableDictionary * dict_searchItems;
@property (strong, nonatomic) NSMutableArray      * array_hiddenSections;
@property (strong, nonatomic) NSMutableDictionary * array_itemsWithTabs;
@property (strong, nonatomic) NSMutableDictionary * dict_sections;


// State
@property (strong, nonatomic) NSString * selectedTab;
@property (nonatomic) NSInteger selectedTabIndex;
@property (nonatomic) BOOL isSearch;
@property (nonatomic, strong) NSNumber * number_queriesCount;

// Tmp properties
@property (weak, nonatomic) ChildProgressCell * cell_editObsCell;
@property (weak, nonatomic) NSMutableDictionary * dict_editObsInfo;

//File types (photo or video)
@property (assign, nonatomic) ChildProgressControllerFileType fileType;


// Upload photo
@property (strong, nonatomic) NSIndexPath * indexPath_uploadIndexPath;
@property (strong, nonatomic) NSMutableDictionary * dict_editingInfo;


//Upload video
@property (strong, nonatomic) UIActionSheet * actionSheet_videos;
@property (nonatomic) BOOL lastSelectedRowHasVideos;

@property (copy, nonatomic) NSArray *selectedAssets;
@property (strong, nonatomic) NSURL *selectedVideoURL;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) UIPopoverController *popover;

// UI
@property (strong, nonatomic) ConfirmButton            * button_addNextStep;
@property (strong, nonatomic) ChildProgressButtonsCell * cell_buttons;
@property (strong, nonatomic) ChildProgressFilterCell  * cell_filter;
@property (strong, nonatomic) ChildProgressAolsCell    * cell_aols;
@property (strong, nonatomic) SpoilerCell              * cell_spoiler;
@property (strong, nonatomic) ChildProgressTabsCell    * cell_tabs;

@property (strong, nonatomic) ChildProgressCell * cell_lastSelected;

// DateAchieved
@property (nonatomic, strong) UIActionSheet * actionSheet_dateAchieved;
@property (nonatomic, strong) NSIndexPath * indexPath_dateAchieved;

@end

@implementation ChildProgressController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTopTitle:[NSString stringWithFormat:@"%@ %@'s Progress", self.dict_childInfo[@"first_name"] ? [self.dict_childInfo[@"first_name"] capitalizedString] : @"N/A", self.dict_childInfo[@"last_name"] ? [self.dict_childInfo[@"last_name"] capitalizedString] : @"N/A"]];
    
    /* Table header */
    [self prepareTableHeader];
    
    /* Table view - hide separators */
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        // iOS7+
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    self.isSearch         = NO;
    self.dict_searchItems = [NSMutableDictionary new];
    self.dict_items       = [NSMutableDictionary new];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(photoDidCompleteUploading:) name:UploadsQueueOperationCompleteNotification object:nil];

    [self reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    
   // [super viewWillAppear:animated];
}



#pragma mark - UI -

- (void) prepareTableHeader
{
    UIView * header = [self viewFromCell:kCellInfoIdentifier];
    InfoPicCell * cell = (InfoPicCell *)header.superview;
    
    ChildInfoModel * child = [[ChildInfoModel alloc] init];

    [cell updateImageWithImagePath:self.dict_childInfo[@"photograph"]];
    
    NSString * title = [NSString stringWithFormat:@"%@ %@'s Progress\n\n", self.dict_childInfo[@"first_name"] ? [self.dict_childInfo[@"first_name"] capitalizedString] : @"N/A", self.dict_childInfo[@"last_name"] ? [self.dict_childInfo[@"last_name"] capitalizedString] : @"N/A"];
    
    NSString * ageTitle   = @"Age: ";
    NSString * ageText    = [NSString stringWithFormat:@"%ld Months\n", [child monthsFromDateOfBirth:self.dict_childInfo[@"birthday"] withFormat:@"dd.MM.yyyy"]];
    NSString * birthTitle = @"Date of Birth: ";
    NSString * birthText  = [NSString stringWithFormat:@"%@\n", self.dict_childInfo[@"birthday"]];
    NSString * roomTitle  = @"Assigned Room: ";
    NSString * roomText   = [NSString stringWithFormat:@"%@\n\n", self.dict_childInfo[@"room_title"]];
    
    NSString * text = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", title, ageTitle, ageText, birthTitle, birthText, roomTitle, roomText];
    
    
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName: cell.infoTextView.textColor,
                              NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:11]
                              };
    NSDictionary *attribsBold = @{
                                  NSForegroundColorAttributeName: cell.infoTextView.textColor,
                                  NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Medium" size:11]
                                  };
    
    NSMutableAttributedString * attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
    
    // Make text bold
    NSArray * bold = @[title, ageTitle, birthTitle, roomTitle];
    [bold enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString * str = obj;
        NSRange textRange = [text rangeOfString:str];
        [attributedText setAttributes:attribsBold range:textRange];
    }];

    cell.infoTextView.textContainer.exclusionPaths = @[[cell picBezierPath]];

    cell.infoTextView.attributedText = attributedText;
    
    header.frame = (CGRect){
        .origin = CGPointZero,
        .size   = CGSizeMake(header.frame.size.width, cell.height-25)
    };
    
    self.tableView.tableHeaderView = header;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}



#pragma mark - Server and data -

-(void)reloadData
{
    self.tableView.userInteractionEnabled = NO;
    NSNumber * childId = self.dict_childInfo[@"id"];
    
    self.number_queriesCount = @(1);
    
    [[NetworkManager sharedInstance] getNewProgressWithChildAndDates:[childId integerValue] aol:self.selectedAol completion:^(id response, NSError *error) {
        if (response) {
            [self updateItems:response];
        } else if(error) {
            ErrorAlert(error.localizedDescription);
        }
    }];
}


- (void) updateItems:(NSDictionary *) response
{
    // array_itemsWithTabs store all tabs for selected type
    if ([[response valueForKey:@"newprogressList"] isKindOfClass:[NSMutableDictionary class]]) {
        self.array_itemsWithTabs = [NSMutableDictionary dictionaryWithDictionary:[response valueForKey:@"newprogressList"]];
    } else {
        self.array_itemsWithTabs = [NSMutableDictionary new];
    }
    
    NSMutableDictionary * mutableDictionary = [NSMutableDictionary new];
    
    for (NSString * tabTitle in [self.array_itemsWithTabs[self.selectedAol] allKeys]) {
        NSMutableDictionary * mutableTab = [NSMutableDictionary new];
        for (NSString * aolTitle in [self.array_itemsWithTabs[self.selectedAol][tabTitle] allKeys]) {
            NSMutableDictionary * item = [NSMutableDictionary dictionaryWithDictionary:self.array_itemsWithTabs[self.selectedAol][tabTitle][aolTitle]];
            [item setObject:[NSNull null] forKey:@"UIImage"];
            [mutableTab setObject:item forKey:aolTitle];
        }
        [mutableDictionary setObject:mutableTab forKey:tabTitle];
    }
    
    [self.array_itemsWithTabs setObject:mutableDictionary forKey:self.selectedAol];
    [self decreaseQueriesCount];
    [self updateInterface];
}

- (void) updateImages:(NSDictionary *) response
{
    self.array_images = [response valueForKeyPath:@"data.images"];
}

- (void) updateInterface
{
    if ([self.number_queriesCount integerValue] > 0) {
        return;
    }
    [self reloadTab:self.selectedTabIndex];
}

- (void) decreaseQueriesCount
{
    @synchronized(self.number_queriesCount) {
        self.number_queriesCount = @([self.number_queriesCount integerValue] - 1);
    }
}

- (void) increaseQueriesCount
{
    @synchronized(self.number_queriesCount) {
        self.number_queriesCount = @([self.number_queriesCount integerValue] + 1);
    }
}

- (NSMutableDictionary *) selectedTabsItems
{
    __block NSMutableDictionary * items = [NSMutableDictionary new];
    
    NSMutableDictionary * tabs = self.array_itemsWithTabs[self.selectedAol];
    NSArray * keys             = [[tabs allKeys]  sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    [keys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (idx == self.selectedTabIndex) {
            items = [tabs objectForKey:obj];
        }
    }];
    return items;
}

- (void) reloadTab:(NSInteger)tabIndex
{
    self.tableView.userInteractionEnabled = NO;
    
    [AppDelegate showProgressHUD];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        [self selectItems:[self selectedTabsItems]];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [AppDelegate hideProgressHUD];
            [self.tableView reloadData];
            self.tableView.userInteractionEnabled = YES;
            
        });
    });
}

- (void) updateOldTabItems
{
    /* NSInteger oldNumberOfSections = [self numberOfSectionsInTableView:self.tableView];
     [self.tableView beginUpdates];
     [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, oldNumberOfSections-1)] withRowAnimation:UITableViewRowAnimationAutomatic];
     [self.tableView endUpdates];
     */
}

-(void) selectItems:(NSMutableDictionary*)items
{
    self.cell_filter.text_filter.text = @"";

    self.dict_items = items;
    
    [self filterArrayByString:@""];
}

-(NSString*) getImageUrlWithImage:(NSString*)imagePath
{
    NSString *imageUrl=[NSString stringWithFormat:@"%@/%@",[NetworkManager sharedInstance].baseUrl, imagePath];
    return imageUrl;
}


// Need for separate data to source and filtered (to future)
- (NSMutableDictionary *) currentItems
{
    return self.dict_searchItems;
}

- (NSMutableArray *) currentHiddenSections
{
    return self.array_hiddenSections;
}

- (UITableView *) currentTableView
{
    return self.tableView;
}

- (BOOL) sectionIsHidden:(NSIndexPath *)indexPath
{
    return [[self currentHiddenSections] containsObject:indexPath];
}

- (BOOL) sectionIsOpen:(NSIndexPath *)indexPath
{
    return [[self currentHiddenSections] containsObject:indexPath];
}



#pragma mark - Items accessors -


- (NSArray *) identifiersForSection:(NSInteger)section
{
    NSString * key = [self sortedSectionsKeys][section-1];
    return self.dict_sections[key][@"identifiers"];
}

- (NSDictionary *) itemInfoForSection:(NSInteger)section
{
    NSString * key = [self sortedSectionsKeys][section-1];
    return self.dict_sections[key];
}

- (BOOL) rowHasImage:(NSString *)rowid
{
    return YES;
    NSMutableDictionary * info = [self itemForItemid:rowid];
    if (info) {
        return [info[@"Photo"] integerValue] > 0;
    }
    return NO;
}

- (BOOL) rowHasVideo:(NSString *)rowid
{
    return YES;
    NSMutableDictionary * info = [self itemForItemid:rowid];
    if (info) {
        return [info[@"Videos"] integerValue] > 0;
    }
    return NO;
}

- (NSMutableDictionary *) infoItemForIndex:(NSInteger)index
{
    NSString * key = [[self.dict_items allKeys] sortedArrayUsingSelector:@selector(compare:)][index];
    return self.dict_items[key];
    NSMutableDictionary * item = [NSMutableDictionary new];
    
    for (int ind = 0; ind < [[self currentItems] count]; ind++) {
        if (ind == index) {
            NSString * key         = [[[self currentItems] allKeys] objectAtIndex:ind];
            NSDictionary * items   = [[self currentItems] objectForKey:key];
            [item setObject:key forKey:@"key"];
            [item setObject:items forKey:@"items"];
            [item setObject:self.selectedAol forKey:@"aol"];
            [item setObject:self.dict_childInfo forKey:@"child"];
            return item;
        }
    }
    return item;
}

- (NSArray *) itemsForSection:(NSInteger)section
{
    NSString * key  = [self sortedSectionsKeys][section-1];
    NSArray * items = self.dict_sections[key][@"items"];
    if ([key isEqualToString:@"40_60"] && [self.selectedAol isEqualToString:@"cl"] && self.selectedTabIndex == 1 && [items count] == 4) {
        NSMutableArray * newItems = [items mutableCopy];
        id object = [newItems objectAtIndex:3];
        [newItems removeObjectAtIndex:3];
        [newItems insertObject:object atIndex:1];
        return [NSArray arrayWithArray:newItems];
    }
    return items;
}

- (NSMutableDictionary *) itemForIndexPath:(NSIndexPath *)indexPath
{
    return [self itemsForSection:indexPath.section][(int)(indexPath.row/2)];
}

- (NSMutableDictionary *)itemForItemid:(NSString *)itemid
{
    // Find item
    NSArray * keys  = [self sortedSectionsKeys];
    for (NSString * key in keys) {
        NSArray * items = self.dict_sections[key][@"items"];
        for (NSMutableDictionary * item in items) {
            if ([item[@"id"] isEqualToString:itemid]) {
                return item;
            }
        }
    }
    return nil;
}



#pragma mark - TableView -

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1 + [self sortedSectionsKeys].count;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 4;
    } else {
        return [self identifiersForSection:section].count;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section >= 1) {
        return 44;
    }
    return 0;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section >=1) {
        UITableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:kCellProgressSectionHeaderIdentifier];
        cell.contentView.backgroundColor = [MyColorTheme sharedInstance].backgroundColor;
        UILabel * label = (UILabel *)[cell.contentView viewWithTag:1001];
        
        NSDictionary * info = [self itemInfoForSection:section];
        label.text = info[@"title"];
        
        return cell.contentView;
    }
    return [UIView new];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                return 69;
            case 1: {
                ChildProgressAolsCell * cell = [self aolsCell];
                return [cell getHeight];
            }
            case 2:
                return 56;
            case 3:
                return 90;
        }
    } else {
        if (indexPath.row%2 == 0) {
            ChildProgressHeaderCell * cell = [self progressHeaderCellForIndexPath:indexPath];
            return cell.height;
        } else {
            ChildProgressCell * cell = [self progressCellAtIndexPath:indexPath];
            return cell.height;
        }
    }
    return 0;
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                return [self spoilerCell];
            /*case 1:
                return [self buttonsCell];*/
            case 1:
                return [self aolsCell];
            case 2:
                return [self tabsCell];
            case 3:
                return [self filterCell];
        }
    } else {
        if (indexPath.row%2 == 0) {
            return [self progressHeaderCellForIndexPath:indexPath];
        } else {
          return [self progressCellAtIndexPath:indexPath];
        }
    }
    return nil;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}



#pragma mark - Cells -

- (UITableViewCell *) spoilerCell
{
    if (self.cell_spoiler != nil) {
        return self.cell_spoiler;
    }
    
    self.cell_spoiler = [self.tableView dequeueReusableCellWithIdentifier:kCellSpoilerIdentifier];
    
    UILabel * label = (UILabel *)[self.cell_spoiler.contentView viewWithTag:222];
    
    label.text = [NSString stringWithFormat:@"%@'s Progress", self.dict_childInfo[@"first_name"]];

    
    return self.cell_spoiler;
}

- (UITableViewCell *) filterCell
{
    if (self.cell_filter != nil) {
        return self.cell_filter;
    }
    
    self.cell_filter = [self.tableView dequeueReusableCellWithIdentifier:kCellFilterIdentifier];
    [self.cell_filter.text_filter addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.cell_filter.text_filter.delegate = self;
    
    return self.cell_filter;
}

- (UITableViewCell *) buttonsCell
{
    if (self.cell_buttons != nil) {
        return self.cell_buttons;
    }
    
    self.cell_buttons = [self.tableView dequeueReusableCellWithIdentifier:kCellButtonsIdentifier];
    
    return self.cell_buttons;
}

- (ChildProgressAolsCell *) aolsCell
{
    if (self.cell_aols != nil) {
        return self.cell_aols;
    }
    
    self.cell_aols = [self.tableView dequeueReusableCellWithIdentifier:kCellAolsIdentifier];
    self.cell_aols.delegate = self;
    [self.cell_aols selectAol:self.selectedAol];
    
    return self.cell_aols;
}

- (ChildProgressTabsCell *) tabsCell
{
    if (self.cell_tabs == nil) {
        self.cell_tabs = [self.tableView dequeueReusableCellWithIdentifier:kCellTabsIdentifier];
        self.cell_tabs.type     = self.selectedAol;
        self.cell_tabs.delegate = self;
        self.cell_tabs.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self.cell_tabs;
}

- (ChildProgressCell *) progressCellAtIndexPath:(NSIndexPath*)indexPath
{
    ChildProgressCell * cell = [self.tableView dequeueReusableCellWithIdentifier:kCellProgressIdentifier];
    cell.delegate       = self;
    
    NSMutableDictionary * info  = [self itemForIndexPath:indexPath];
    [cell setInfo:info];
    
    return cell;
}

- (ChildProgressHeaderCell *) progressHeaderCellForIndexPath:(NSIndexPath *)indexPath
{
    ChildProgressHeaderCell * header = [self.tableView dequeueReusableCellWithIdentifier:kCellProgressHeaderIdentifier];
    
    /*NSIndexPath * itemIndexPath = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];
    
    [header setArrowIsUp:![self sectionIsOpen:itemIndexPath] animated:NO];

    
    // Clear gestures
    while (header.contentView.gestureRecognizers.count) {
        [header.contentView removeGestureRecognizer:[header.contentView.gestureRecognizers objectAtIndex:0]];
    }
    
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headerTap:)];
    tap.numberOfTapsRequired    = 1;
    tap.numberOfTouchesRequired = 1;
    [header.contentView addGestureRecognizer:tap];
    
    
    NSDictionary * item = [self itemsForSection:indexPath.section][(int)(indexPath.row/2)];
    [header setTitle:item[@"text"] color:item[@"color"]];
    
    DateTextField * textField = (DateTextField *)[header.contentView viewWithTag:1004];
    [textField removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
    textField.textDelegate = self;
    
    textField.text    = item[@"DateAchieved"];
    [textField addTarget:self action:@selector(dateAchievedChaned:) forControlEvents:UIControlEventEditingChanged];
    
    if (item[@"DateAchieved"] != nil && ![item[@"DateAchieved"] isEqualToString:@""]) {
        [textField setActive:NO];
    } else {
        [textField setActive:YES];
    }*/
    
    return header;
}



#pragma mark - DateAchieved -

- (void) dateTextFieldTap:(DateTextField *)textField
{
    ChildProgressHeaderCell * cell = (ChildProgressHeaderCell *)textField.superview.superview;
    NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
    
    NSDictionary * item = [self itemsForSection:indexPath.section][(int)(indexPath.row/2)];
    if (item[@"DateAchieved"] != nil && ![item[@"DateAchieved"] isEqualToString:@""]) {
        self.indexPath_dateAchieved = indexPath;
        
        self.actionSheet_dateAchieved = [[UIActionSheet alloc] initWithTitle:@"Select action:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:nil];
        
        [self.actionSheet_dateAchieved addButtonWithTitle:@"Change"];
        [self.actionSheet_dateAchieved addButtonWithTitle:@"Delete"];
        
        self.actionSheet_dateAchieved.delegate = self;
        
        [self.actionSheet_dateAchieved showInView:self.view];
    }
}

- (void) dateAchievedChaned:(DateTextField*)sender
{
    ChildProgressHeaderCell * cell = (ChildProgressHeaderCell *)sender.superview.superview;
    NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
    __block NSMutableDictionary * item = [self itemsForSection:indexPath.section][(int)(indexPath.row/2)];
    
    NSString * dateString = sender.text;
    NSString * type = self.selectedAol;
    NSString * tab = [NSString stringWithFormat:@"tab%ld", (self.selectedTabIndex + 1)];
    NSString * itemid = item[@"id"];
    
    self.tableView.userInteractionEnabled = NO;
    [AppDelegate showProgressHUD];
    
    [[NetworkManager sharedInstance] addDateAchievedWithOptions:@{@"type":type, @"tab":tab, @"itemid":itemid, @"chid":self.dict_childInfo[@"id"], @"date":dateString} withCompletions:^(id response, NSError *error) {
        
        [item setObject:dateString forKey:@"DateAchieved"];
        [AppDelegate hideProgressHUD];
        self.tableView.userInteractionEnabled = YES;
    }];
}

- (void) removeDateAchieved:(DateTextField *)textField
{
    ChildProgressHeaderCell * cell = (ChildProgressHeaderCell *)textField.superview.superview;
    NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
    __block NSMutableDictionary * item = [self itemsForSection:indexPath.section][(int)(indexPath.row/2)];
    
    NSString * dateString = textField.text;
    NSString * type = self.selectedAol;
    NSString * tab = [NSString stringWithFormat:@"tab%ld", (self.selectedTabIndex + 1)];
    NSString * itemid = item[@"id"];
    
    self.tableView.userInteractionEnabled = NO;
    [AppDelegate showProgressHUD];
    
    [[NetworkManager sharedInstance] deleteDateAchievedWithOptions:@{@"type":type, @"tab":tab, @"itemid":itemid, @"chid":self.dict_childInfo[@"id"], @"date":dateString} withCompletions:^(id response, NSError *error) {
        textField.text = @"";
        [item setObject:@"" forKey:@"DateAchieved"];
        [AppDelegate hideProgressHUD];
        self.tableView.userInteractionEnabled = YES;
    }];
    
}



#pragma mark - TextField delegate + filter -

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.cell_filter.text_filter) {
        long oldSectionsCount = 1 + [self.dict_searchItems count];
        
        [self filterArrayByString:textField.text];
        
        long newSectionsCount = 1 + [self.dict_searchItems count];
        
        if (newSectionsCount > oldSectionsCount) {
            NSIndexSet * reloadedSections = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(oldSectionsCount, newSectionsCount-oldSectionsCount)];
            [self.tableView insertSections:reloadedSections withRowAnimation:UITableViewRowAnimationFade];
        } else if(newSectionsCount < oldSectionsCount) {
            NSIndexSet * reloadedSections = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(newSectionsCount, oldSectionsCount-newSectionsCount)];
            [self.tableView deleteSections:reloadedSections withRowAnimation:UITableViewRowAnimationFade];
        }
        
        [self.tableView beginUpdates];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, newSectionsCount-1)] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (void) filterArrayByString:(NSString *) string
{
    self.array_hiddenSections = [NSMutableArray new];
    self.dict_sections        = [NSMutableDictionary new];
    NSInteger maxAgeEnd       = 0;
    NSInteger minAgeStart     = 60;
    
    // Find min and max age
    for (NSString * key in [self.dict_items allKeys]) {
         NSDictionary * eolInfo = [[AOLPlist sharedInstance] eyoInfoForAOL:self.selectedAol eyoKey:key];
        if ([eolInfo[@"age_end"] integerValue] > maxAgeEnd) {
            maxAgeEnd = [eolInfo[@"age_end"] integerValue];
        }
        
        if ([eolInfo[@"age_start"] integerValue] < minAgeStart) {
            minAgeStart = [eolInfo[@"age_start"] integerValue];
        }
    }
    
    for (NSString * key in [self.dict_items allKeys]) {
        NSDictionary * eolInfo     = [[AOLPlist sharedInstance] eyoInfoForAOL:self.selectedAol eyoKey:key];
        NSString * sectionKey = [NSString stringWithFormat:@"%@_%@", eolInfo[@"age_start"], eolInfo[@"age_end"]];
        
        NSString * ageStartTitle;
        NSString * ageEndTitle;
        if ([eolInfo[@"age_start"] integerValue] == 0) {
            ageStartTitle = @"Birth";
        } else {
            ageStartTitle = [NSString stringWithFormat:@"%@ Months", eolInfo[@"age_start"]];
        }
        
        ageEndTitle = [NSString stringWithFormat:@"%@ Months", eolInfo[@"age_end"]];
        
        
        NSString * sectionTitle = [NSString stringWithFormat:@"%@ - %@", ageStartTitle, ageEndTitle];
        
        NSMutableDictionary * sectionItem;
        if ([self.dict_sections valueForKey:sectionKey] == nil) {
            sectionItem = [NSMutableDictionary new];
            
            
            if ([eolInfo[@"age_start"] integerValue] == minAgeStart &&
                [eolInfo[@"age_end"] integerValue] == maxAgeEnd
            ) {
                sectionTitle = @"General";
                [sectionItem setObject:@(-1) forKey:@"age_start"];
            } else {
                [sectionItem setObject:eolInfo[@"age_start"] forKey:@"age_start"];
            }
            
            [sectionItem setObject:sectionTitle forKey:@"title"];
            [sectionItem setObject:eolInfo[@"age_end"] forKey:@"age_end"];
            [sectionItem setObject:[NSMutableArray new] forKey:@"items"];
            [sectionItem setObject:[NSMutableArray new] forKey:@"identifiers"];
            [self.dict_sections setObject:sectionItem forKey:sectionKey];
        } else {
            sectionItem = [self.dict_sections objectForKey:sectionKey];
        }
        
        NSMutableDictionary * item = [NSMutableDictionary dictionaryWithDictionary:self.dict_items[key]];
        if ([eolInfo[@"age_start"] integerValue] == minAgeStart &&
            [eolInfo[@"age_end"] integerValue] == maxAgeEnd
            ) {
            [item setObject:[[AOLPlist sharedInstance] generalColor] forKey:@"color"];
        } else {
            [item setObject:[[AOLPlist sharedInstance] mainColorForType:self.selectedAol] forKey:@"color"];
        }
        
        [item setObject:eolInfo[@"id"] forKey:@"id"];
        
        NSMutableCharacterSet *nonNumberCharacterSet = [NSMutableCharacterSet decimalDigitCharacterSet];
        [nonNumberCharacterSet invert];
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber * number = [f numberFromString:[[eolInfo[@"id"] componentsSeparatedByCharactersInSet:nonNumberCharacterSet] componentsJoinedByString:@""]];
        [item setObject:number forKey:@"number"];
        
        [item setObject:eolInfo[@"text"] forKey:@"text"];
        [[sectionItem valueForKeyPath:@"items"] addObject:item];
        
        [[sectionItem valueForKeyPath:@"identifiers"] addObject:kCellProgressHeaderIdentifier];
        [[sectionItem valueForKeyPath:@"identifiers"] addObject:kCellProgressIdentifier];
        
        // Sort items by id
        NSArray * items = [sectionItem objectForKey:@"items"];
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"number"  ascending:YES];
        items = [items sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
        [sectionItem setObject:[NSMutableArray arrayWithArray:items] forKeyedSubscript:@"items"];
    }
}

- (NSArray *) sortedSectionsKeys
{
    // Sort sections by age ranges
    NSArray * sortedKeys = [[self.dict_sections allKeys] sortedArrayUsingComparator:^(id obj1, id obj2) {
        NSInteger start1 = [[self.dict_sections[obj1] valueForKeyPath:@"age_start"] integerValue];
        NSInteger start2 = [[self.dict_sections[obj2] valueForKeyPath:@"age_start"] integerValue];
        if (start1 > start2)
            return (NSComparisonResult)NSOrderedDescending;
        if (start1 < start2)
            return (NSComparisonResult)NSOrderedAscending;
        return (NSComparisonResult)NSOrderedSame;
    }];

    return sortedKeys;
}



#pragma mark - Cell delegates -

- (void) childProgressCellAddNextStep:(ChildProgressCell *)cell
{
    [self addNextStepWithSample:NO];
}

- (void) childProgressCell:(ChildProgressCell *)cell didEditObservation:(NSMutableDictionary *)observation
{
    self.cell_editObsCell = cell;
    self.dict_editObsInfo = observation;
    
    [self performSegueWithIdentifier:@"editObsSegue" sender:self];
}

- (void) childProgressCell:(ChildProgressCell *)cell didDeleteObservation:(NSMutableDictionary *)observation
{
    NSIndexPath * indexPath     = [self.tableView indexPathForCell:cell];
    NSMutableDictionary * info  = [self itemsForSection:indexPath.section][(int)(indexPath.row/2)];
    NSDictionary * aol   = [[AOLPlist sharedInstance] earlyOutcomesInfoForKey:info[@"id"]];
    
    NSMutableDictionary * options = [NSMutableDictionary new];
    [options setObject:self.dict_childInfo[@"id"] forKey:@"chid"];
    [options setObject:aol[@"aol"] forKey:@"type"];
    [options setObject:aol[@"id"] forKey:@"itemid"];
    [options setObject:observation[@"date"] forKey:@"date"];
    [options setObject:observation[@"text"] forKey:@"text"];
    [options setObject:@"0" forKey:@"type2"];
    
    NSMutableArray * oldObsNotes = [info[@"ObsNotes"] mutableCopy];
    [oldObsNotes removeObject:observation];
    
    self.cell_editObsCell = cell;
    [[NetworkManager sharedInstance] deleteObservation:options withCompletion:^(id response, NSError *error) {
        [info setObject:oldObsNotes forKey:@"ObsNotes"];
        [self editObsControllerDidUpdateObs];
    }];
}

- (void) childProgressCellPhotosTap:(ChildProgressCell *)cell
{
    NSIndexPath * indexPath = [[self currentTableView] indexPathForCell:cell];
    self.indexPath_uploadIndexPath = indexPath;
    
    self.fileType = ChildProgressControllerFileTypePhoto;
    
    [self showUploadUphotoPicker];
}

- (void) childProgressCellVideosTap:(ChildProgressCell *)cell
{
    self.cell_lastSelected = cell;

    //Позволяет как выбирать из галлерее (assets) так и сразу же записывать видео (video url)
    
    self.fileType = ChildProgressControllerFileTypeVideo;
    
    self.selectedVideoURL = nil;
    self.selectedAssets = nil;
    
    NSIndexPath * indexPath       = [[self currentTableView] indexPathForCell:cell];
    NSDictionary * info           = [self itemForIndexPath:indexPath];
    self.lastSelectedRowHasVideos = [self rowHasVideo:info[@"id"]];

    NSArray * buttonsTitles = @[@"Take video from camera", @"Choose video from library"];
    if (self.lastSelectedRowHasVideos) {
        buttonsTitles = @[@"View existing videos", @"Take video from camera", @"Choose video from library"];
    }
    
    
    self.actionSheet_videos = [[UIActionSheet alloc] initWithTitle:@"Please select action"
                                                       delegate:self cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:nil];
    for (NSString * title in buttonsTitles) {
        [self.actionSheet_videos addButtonWithTitle:title];
    }
    
    [self.actionSheet_videos showInView:self.view];
}



#pragma mark - EditObsControllerDelegate -

- (void) editObsControllerDidUpdateObs
{
    NSIndexPath * indexPath = [self.tableView indexPathForCell:self.cell_editObsCell];
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
}

- (void) editObsControllerEditObs:(NSDictionary *)oldObs toObs:(NSDictionary *)newObs
{
    NSIndexPath * indexPath = [self.tableView indexPathForCell:self.cell_editObsCell];
    NSMutableDictionary * item = [self itemForIndexPath:indexPath];
    
    NSMutableArray * observations = [NSMutableArray new];
    for (NSDictionary * observation in item[@"ObsNotes"]) {
        if ([observation[@"date"] isEqualToString:oldObs[@"date"]] && [observation[@"text"] isEqualToString:oldObs[@"text"]]) {
            NSMutableDictionary * changedObservation = [NSMutableDictionary dictionaryWithDictionary:observation];
            [changedObservation setObject:newObs[@"text"] forKey:@"text"];
            [changedObservation setObject:newObs[@"date"] forKey:@"date"];
            
            [observations addObject:changedObservation];
        } else {
            [observations addObject:observation];
        }
    }
    [item setObject:observations forKey:@"ObsNotes"];
    
    CGPoint oldOffset = self.tableView.contentOffset;
    [self.tableView reloadData];
    [self.tableView setContentOffset:oldOffset];
}



#pragma mark - Actions -

- (NSIndexPath *) itemIndexPathForHeader:(ChildProgressHeaderCell*)cell
{
    NSIndexPath * headerIndexPath = [self.tableView indexPathForCell:cell];
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:headerIndexPath.row+1 inSection:headerIndexPath.section];
    return indexPath;
}

- (void) headerTap:(UIGestureRecognizer *)gesture
{
    ChildProgressHeaderCell * cell = (ChildProgressHeaderCell*)[gesture.view superview];
    NSIndexPath * indexPath = [self itemIndexPathForHeader:cell];
    
    
    if ([self sectionIsOpen:indexPath]) {
        [[self currentHiddenSections] removeObject:indexPath];
    } else {
        [[self currentHiddenSections] addObject:indexPath];
    }
    
   // [cell setArrowIsUp:!cell.arrowIsUp animated:YES];
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

- (void) aolCellSelectAol:(NSString *)aol
{
    self.cell_filter.text_filter.text = @"";
    
    // Need for reload tabs selection control
    self.cell_tabs = nil;
    if (![aol isEqualToString:self.selectedAol]) {
        self.selectedAol = aol;
        [self reloadData];
    }
}

- (void) childProgressTabsCell:(ChildProgressTabsCell *)cell didSelectTabWithIndex:(NSInteger)index
{
    self.selectedTabIndex = index;
    [self reloadTab:index];
}

- (void) aolCellDidUpdateHeight
{
    [self.tableView beginUpdates];
    //[self.tableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
}

- (void) addNextStepWithSample:(BOOL)withSample
{
    
    NSMutableDictionary * childInfo = [NSMutableDictionary dictionaryWithDictionary:self.dict_childInfo];
    [childInfo setObject:childInfo[@"first_name"] forKey:@"Fist_name"];
    [childInfo setObject:childInfo[@"last_name"] forKey:@"Last_name"];
    [childInfo setObject:childInfo[@"room_title"] forKey:@"Room_title"];
    [childInfo setObject:childInfo[@"photograph"] forKey:@"Photograph"];
    NSString *birth = [NSDateFormatter convertDateFromString:childInfo[@"birthday"] fromFormat:@"dd.MM.yyyy" toFormat:@"yyyy-MM-dd"];
    [childInfo setObject:birth forKey:@"Birth"];

    UIStoryboard * nextStepsStoryboard = [UIStoryboard storyboardWithName:@"NextSteps_iPhone" bundle:nil];
    AddNextStepsViewController * viewController = [nextStepsStoryboard instantiateViewControllerWithIdentifier:@"AddNextSteps"];
    
    viewController.childId        = [childInfo[@"id"] integerValue];
    viewController.dict_childInfo = childInfo;
    viewController.withSample     = withSample;
    
    [self.navigationController pushViewController:viewController animated:YES];
}



#pragma mark - Upload/view videos -

- (void) takeVideoFromCamera
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.videoQuality = UIImagePickerControllerQualityType640x480;
    picker.videoMaximumDuration = 60.f;
    picker.mediaTypes=[[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
    picker.delegate = self;
    [self.navigationController presentViewController:picker animated:YES completion:nil];
}

- (void) chooseVideoFromLibrary
{
    [self pickAssets:self.actionSheet_videos];
}

- (void)pickAssets:(id)sender
{
    if (!self.selectedAssets)
        self.selectedAssets = [[NSMutableArray alloc] init];
    
    CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
    picker.assetsFilter         = [ALAssetsFilter allVideos];
    picker.showsCancelButton    = (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad);
    picker.delegate             = self;
    picker.selectedAssets       = [NSMutableArray arrayWithArray:self.selectedAssets];
    
    // iPad
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        self.popover.delegate = self;
        
        [self.popover presentPopoverFromBarButtonItem:sender
                             permittedArrowDirections:UIPopoverArrowDirectionAny
                                             animated:YES];
    } else {
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void) viewVideos
{
    NSIndexPath * indexPath     = [self.tableView indexPathForCell:self.cell_lastSelected];
    NSMutableDictionary * info  = [self itemsForSection:indexPath.section][(int)(indexPath.row/2)];
    NSDictionary * aol   = [[AOLPlist sharedInstance] earlyOutcomesInfoForKey:info[@"id"]];
    
    GalleryVideoChildModel *child = [[GalleryVideoChildModel alloc] init];
    child.chId = self.dict_childInfo[@"id"];
    child.photoPath = self.dict_childInfo[@"photograph"];
    child.firstName =  self.dict_childInfo[@"first_name"];
    child.lastName  =  self.dict_childInfo[@"last_name"];
    child.roomName  =  self.dict_childInfo[@"room_title"];
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd.MM.yyyy";
    child.dateOfBirth = [formatter dateFromString:self.dict_childInfo[@"birthday"]];
    
    UIStoryboard * videoGalleryStoryboard = [UIStoryboard storyboardWithName:@"GalleryVideo_iPhone" bundle:nil];
    GalleryVideoListGridViewController * viewController = [videoGalleryStoryboard instantiateViewControllerWithIdentifier:@"commentsView"];
    
    viewController.child = child;
    viewController.galleryType = VideoGalleryTypeProgress;
    
    NSNumber * tabIndex = [[AOLPlist sharedInstance] tabsIndexes][aol[@"aspect"]];
    
    viewController.typeShort = aol[@"aol"];
    viewController.typeLong  = [aol[@"aol"] uppercaseString];
    viewController.tabShort  = [NSString stringWithFormat:@"tab%ld", ([tabIndex integerValue]+1)];
    viewController.tabLong   = aol[@"aspect"];
    viewController.rowid     = aol[@"id"];
    
    [self.navigationController pushViewController:viewController animated:YES];
}



#pragma mark - Upload photo -

- (void) showUploadUphotoPicker
{
    NSMutableDictionary * info = [self itemForIndexPath:self.indexPath_uploadIndexPath];;
    DSImagePickerViewController * imagePicker = [[DSImagePickerViewController alloc] init];
    imagePicker.delegate = self;
    [imagePicker setViewButtonEnabled:[self rowHasImage:info[@"id"]]];
    [imagePicker showImagePickerInController:self animated:YES];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
#warning andrey
    
    if (self.fileType == ChildProgressControllerFileTypePhoto) {
        //Photo
    
        UIImage * image = [[Utils sharedInstance]imageScaledToAvatar:[info valueForKey:UIImagePickerControllerOriginalImage]];
        
        // Current datetime
        NSDate * date = [NSDate dateYesterday];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy:MM:dd HH:mm:ss"];
        NSString * convertedString = [dateFormatter stringFromDate:date];
        
        NSDictionary * options = @{@"type":@"general",
                                   @"chid":self.dict_childInfo[@"id"],
                                   @"exifDateTime[0]":convertedString,
                                   @"thumb[0]":[[Utils sharedInstance]base64StringFromImage:image],
                                   @"resized[0]":[[Utils sharedInstance]base64StringFromImage:image]
                                   };
        
        __weak typeof(self) weakSelf = self;
        [AppDelegate showProgressHUD];
        [[NetworkManager sharedInstance] uploadPhotoToGeneralGalleryWithOptions:options completion:^(id response, NSError *error) {
            if (response) {
    #warning get url and save to dictionary
                [[weakSelf currentTableView] reloadRowsAtIndexPaths:@[weakSelf.indexPath_uploadIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            } else if(error) {
                ErrorAlert(error.localizedDescription);
            }
            [AppDelegate hideProgressHUD];
        }];
        [picker dismissViewControllerAnimated:YES completion:nil];

    }
    else if (self.fileType == ChildProgressControllerFileTypeVideo) {
    
        //assign the mediatype to a string
        NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        
        //check the media type string so we can determine if its a video
        if ([mediaType isEqualToString:@"public.movie"]){
            
            NSLog(@"got a movie");
            
            NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
            self.selectedVideoURL = videoURL;
            //NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
            [picker dismissViewControllerAnimated:YES completion:nil];
            
            
            [self uploadVideo];
            
        }
        
    }
    
}

- (void) photoDidCompleteUploading:(NSNotification *)notification
{
    NSString * key = notification.userInfo[@"key"];
    key = [key stringByReplacingOccurrencesOfString:@"newprogress-" withString:@""];
    NSMutableDictionary * item = [self itemForItemid:key];
    [item setObject:@"1" forKey:@"Photo"];
}


#pragma mark - DSImagePickerDelegate -

- (void) imagePickerDidSelectImages:(NSArray *)images
{
    NSMutableDictionary * info  = [self itemsForSection:self.indexPath_uploadIndexPath.section][(int)(self.indexPath_uploadIndexPath.row/2)];
    NSDictionary * aolInfo = [[AOLPlist sharedInstance] earlyOutcomesInfoForKey:info[@"id"]];
    
    
    NSArray * tabs      = [[AOLPlist sharedInstance] aspectsForArea:self.selectedAol];
    NSString * tabTitle = tabs[self.selectedTabIndex];
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Progress_iPhone" bundle:nil];
    ProgressPhotosUploadController * viewController = [storyboard instantiateViewControllerWithIdentifier:@"ProgressPhotosUpload"];
    viewController.dict_childInfo = self.dict_childInfo;
    viewController.array_assets   = images;
    viewController.aolTitle       = [[AOLPlist sharedInstance] aolsTitles][self.selectedAol];
    viewController.aolId          = self.selectedAol;
    viewController.aspectId       = [NSString stringWithFormat:@"tab%ld", (self.selectedTabIndex + 1)];
    viewController.aspectTitle    = tabTitle;
    viewController.eolText        = aolInfo[@"text"];
    viewController.eolId          = aolInfo[@"id"];
    viewController.dict_editInfo  = info;
    
    [self.navigationController pushViewController:viewController animated:YES];
}


- (void) imagePickerViewPhotosTap
{
    [self performSegueWithIdentifier:@"commentsView@GalleryPhoto_iPhone" sender:self];
}


#pragma mark - Segue -

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"addObs"]) {
        UIButton * button        = (UIButton*)sender;
        ChildProgressCell * cell = (ChildProgressCell*)button.superview.superview.superview.superview;
        NSIndexPath * indexPath  = [self.tableView indexPathForCell:cell];
        NSDictionary * info      = [self itemsForSection:indexPath.section][(int)(indexPath.row/2)];
        NSDictionary * eyoInfo   = [[AOLPlist sharedInstance] earlyOutcomesInfoForKey:info[@"id"]];
        
        AddObsToChildrensController * controller = [segue destinationViewController];
        controller.aol = eyoInfo[@"aol"];
        controller.eyo = eyoInfo[@"id"];
        controller.delegate = self;
        controller.child_id = self.dict_childInfo[@"id"];
        
    }
    if ([segue.identifier isEqualToString:@"editObsSegue"]) {
        
        NSIndexPath * indexPath = [self.tableView indexPathForCell:self.cell_editObsCell];
        NSMutableDictionary * info  = [self itemForIndexPath:indexPath];
        NSDictionary * eyoFullInfo = [[AOLPlist sharedInstance] earlyOutcomesInfoForKey:info[@"id"]];
        
        
        EditObsController * controller = [segue destinationViewController];
        controller.dict_obs = self.dict_editObsInfo;
        controller.child_id = self.dict_childInfo[@"id"];
        controller.type      = eyoFullInfo[@"aol"];
        controller.itemid    = eyoFullInfo[@"id"];
        controller.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"commentsView@GalleryPhoto_iPhone"]) {
        GalleryPhotoChildModel *child = [[GalleryPhotoChildModel alloc] init];
        child.chId      = self.dict_childInfo[@"id"];
        child.photoPath = self.dict_childInfo[@"photograph"];
        child.firstName =  self.dict_childInfo[@"first_name"];
        child.lastName  =  self.dict_childInfo[@"last_name"];
        child.roomName  =  self.dict_childInfo[@"room_title"];
        
        NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd.MM.yyyy";
        child.dateOfBirth = [formatter dateFromString:self.dict_childInfo[@"birthday"]];
        
        NSMutableDictionary * info = [self itemForIndexPath:self.indexPath_uploadIndexPath];
        NSDictionary * eyoFullInfo = [[AOLPlist sharedInstance] earlyOutcomesInfoForKey:info[@"id"]];

        NSNumber * tabIndex        = [[AOLPlist sharedInstance] tabsIndexes][info[@"aspect"]];
 
        GalleryPhotoListGridViewController * vc = segue.destinationViewController;
        vc.galleryType = PhotoGalleryTypeProgress;
        vc.child = child;
        vc.typeShort = eyoFullInfo[@"aol"];
        vc.typeLong  = [eyoFullInfo[@"aol"] uppercaseString];
        vc.tabShort  = [NSString stringWithFormat:@"tab%ld", ([tabIndex integerValue]+1)];
        vc.tabLong   = [NSString stringWithFormat:@"tab%ld", self.selectedTabIndex+1];
        vc.rowid     = eyoFullInfo[@"id"];
    }
}



#pragma mark - Popover Controller Delegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.popover = nil;
}


#pragma mark - Assets Picker Delegate

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker isDefaultAssetsGroup:(ALAssetsGroup *)group
{
    return ([[group valueForProperty:ALAssetsGroupPropertyType] integerValue] == ALAssetsGroupSavedPhotos);
}

- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    if (self.popover != nil)
        [self.popover dismissPopoverAnimated:YES];
    else
        [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
    self.selectedAssets = [NSMutableArray arrayWithArray:assets];
    
    
    [self uploadVideo];
    
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldEnableAsset:(ALAsset *)asset
{
    // Enable video clips if they are at least 5s (1s)
    if ([[asset valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo])
    {
        NSTimeInterval duration = [[asset valueForProperty:ALAssetPropertyDuration] doubleValue];
        
        ALAssetRepresentation *rep = [asset defaultRepresentation];
        unsigned long long videoSize = rep.size;
        
        return lround(duration) >= 1 && lround(duration) <= 60 && videoSize <= 209715200.f;
    }
    else
    {
        return YES;
    }
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(ALAsset *)asset
{
    if (picker.selectedAssets.count >= 10)
    {
        UIAlertView *alertView =
        [[UIAlertView alloc] initWithTitle:@"Attention"
                                   message:@"Please select not more than 10 video clips"
                                  delegate:nil
                         cancelButtonTitle:nil
                         otherButtonTitles:@"OK", nil];
        
        [alertView show];
    }
    
    if (!asset.defaultRepresentation)
    {
        UIAlertView *alertView =
        [[UIAlertView alloc] initWithTitle:@"Attention"
                                   message:@"Your asset has not yet been downloaded to your device"
                                  delegate:nil
                         cancelButtonTitle:nil
                         otherButtonTitles:@"OK", nil];
        
        [alertView show];
    }
    
    return (picker.selectedAssets.count < 10 && asset.defaultRepresentation != nil);
}



#pragma mark uploadVideo 

- (void)uploadVideo
{
     NSIndexPath * indexPath     = [self.tableView indexPathForCell:self.cell_lastSelected];
     NSMutableDictionary * info  = [self itemsForSection:indexPath.section][(int)(indexPath.row/2)];
     NSDictionary * aol   = [[AOLPlist sharedInstance] earlyOutcomesInfoForKey:info[@"id"]];
    
    //формируем нужные к передаче классы
    GalleryVideoChildModel *child = [[GalleryVideoChildModel alloc] init];
    child.chId = self.dict_childInfo[@"id"];
    child.photoPath = self.dict_childInfo[@"photograph"];
    child.firstName =  self.dict_childInfo[@"first_name"];
    child.lastName  =  self.dict_childInfo[@"last_name"];
    child.roomName  =  self.dict_childInfo[@"room_title"];
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd.MM.yyyy";
    child.dateOfBirth = [formatter dateFromString:self.dict_childInfo[@"birthday"]];
    
    
    
    UIStoryboard * videoGalleryStoryboard = [UIStoryboard storyboardWithName:@"GalleryVideo_iPhone" bundle:nil];
    
    GalleryVideoUploadViewController *vc = [videoGalleryStoryboard instantiateViewControllerWithIdentifier:@"uploadVideo"];            vc.dateUpload = [NSDate date];
    vc.videoAssests = self.selectedAssets;
    vc.videoURL = self.selectedVideoURL;
    vc.childModel = child;
    vc.galleryType = VideoGalleryTypeProgress;
    
    
    NSNumber * tabIndex = [[AOLPlist sharedInstance] tabsIndexes][aol[@"aspect"]];
    
    //!привязывать реальные данные
    vc.typeShort = aol[@"aol"];
    vc.typeLong  = aol[@"aol"];
    vc.tabShort  = [NSString stringWithFormat:@"tab%ld", ([tabIndex integerValue]+1)];
    vc.tabLong   = aol[@"aspect"];
    vc.rowId     = aol[@"id"];
    
    [self.navigationController pushViewController:vc animated:YES];
}



#pragma mark - AddObsToChildrensControllerDelegate -

- (void) obsDidAdded:(NSArray *)observations
{
    
    for (NSDictionary * obs in observations) {
        NSMutableDictionary * item = [self itemForItemid:obs[@"item"]];
        if (!item) {
            continue;
        }
        
        if (item[@"ObsNotes"] == nil || item[@"ObsNotes"] == [NSNull null] || ![item[@"ObsNotes"] isKindOfClass:[NSArray class]]) {
            item[@"ObsNotes"] = [NSArray new];
        }
        
        
        NSString * title =[obs[@"type"] isEqualToString:@"note"] ? @"Note: " : @"Observation";
        
        NSMutableArray * notes = [NSMutableArray arrayWithArray:item[@"ObsNotes"]];
        [notes addObject:@{@"date":obs[@"date"], @"text":obs[@"text"], @"type":obs[@"type"], @"title": title}];
        
        [item setObject:notes forKey:@"ObsNotes"];
    }
    
    CGPoint offset = self.tableView.contentOffset;
    [self.tableView reloadData];
    [self.tableView setContentOffset:offset];
}



#pragma mark - UIActionSheetDelegate -

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.actionSheet_dateAchieved == actionSheet) {
        ChildProgressHeaderCell * cell = (ChildProgressHeaderCell*)[self.tableView cellForRowAtIndexPath:self.indexPath_dateAchieved];
        DateTextField * textField = (DateTextField *)[cell.contentView viewWithTag:1004];
        
        // Change
        if (buttonIndex == 1) {
            [textField show:YES];
        } else if (buttonIndex == 2) {
            [self removeDateAchieved:textField];
        }
        self.indexPath_dateAchieved = nil;
        
    } else if (self.actionSheet_videos) {

        if (self.lastSelectedRowHasVideos) {
            if (buttonIndex == 1) {
                [self viewVideos];
            } else if (buttonIndex == 2) {
                [self takeVideoFromCamera];
            } else if (buttonIndex == 3) {
                [self chooseVideoFromLibrary];
            }
        } else {
            if (buttonIndex == 1) {
                [self takeVideoFromCamera];
            } else if(buttonIndex == 2) {
                [self chooseVideoFromLibrary];
            }
        }
    }
}


@end
