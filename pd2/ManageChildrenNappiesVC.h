//
//  ManageChildrenNappiesVC.h
//  pd2
//
//  Created by User on 08.02.17.
//  Copyright (c) 2017 Andrey. All rights reserved.
//

#import "Pd2MenuViewController.h"

@interface ManageChildrenNappiesVC : Pd2MenuViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@end
