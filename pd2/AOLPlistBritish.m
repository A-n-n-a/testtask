#import "AOLPlistBritish.h"
#import "NSString+Color.h"

@implementation AOLPlistBritish

-(id)init
{
    if (self =[super init]) {
        
        if ([[NetworkManager sharedInstance] isJ3]) {
            self.fileName = @"areasOfLearnings_british_J3";
        } else {
            self.fileName = @"areasOfLearnings_british";
        }
        
    }
    return self;
}

- (NSArray *) sortedAreasOfLearningsKeys
{
    return  @[
              @"cl",
              @"ead",
              @"lt",
              @"mt",
              @"pd",
              @"psed",
              @"utw"
              ];
}

- (NSString *) mainColorForType:(NSString *)type
{
    NSDictionary * mainColor = @{
                                 @"cl":   @"f7deb5",
                                 @"ead":  @"ced6de",
                                 @"lt":   @"f6d8f0",
                                 @"mt":   @"c6d6bd",
                                 @"pd":   @"d6cede",
                                 @"psed": @"e7bdbd",
                                 @"utw":  @"efefb5",
                                 @"uw":   @"efefb5"
                                 };
    return mainColor[type];
}

- (NSString *) highlightedColorForType:(NSString *)type
{
    NSDictionary * mainColor = @{
                                 @"cl":    @"efe7b1",
                                 @"ead":   @"cfdae7",
                                 @"lt":    @"eddef5",
                                 @"mt":    @"c9e0c0",
                                 @"pd":    @"d3d0ea",
                                 @"psed":  @"dbc7c3",
                                 @"uw":    @"eff5c0",
                                 @"utw":   @"eff5c0",
                                 };
    return mainColor[type];
}

- (NSString *) generalColor
{
    return @"EEEEEE";
}

- (NSDictionary *) aolsTitles
{
    return  @{
              @"cl"  : @"Communication and Language",
              @"ead" : @"Expressive arts and Design",
              @"lt"  : @"Literacy",
              @"mt"  : @"Mathematics",
              @"pd"  : @"Physical Development",
              @"psed": @"Personal, Social & Emotional Development",
              @"uw"  : @"Understanding the World",
              @"utw" : @"Understanding the World"
              };
}

- (NSDictionary *) aolsIcons
{
    return  @{
              @"cl"  : @"progress_type_cl_30",
              @"ead" : @"progress_type_ead_30",
              @"lt"  : @"progress_type_lt_30",
              @"mt"  : @"progress_type_mt_30",
              @"pd"  : @"progress_type_pd_30",
              @"psed": @"progress_type_psed_30",
              @"uw"  : @"progress_type_uw_30",
              @"utw" : @"progress_type_uw_30"
              };
}

- (NSDictionary *) tabsIcons
{
    return  @{
              // CL
              @"Listening and Attention":                 @"cltab1-1",
              @"Understanding":                           @"cltab2-1",
              @"Speaking":                                @"cltab3-1",
              // EAD
              @"Exploring and using Media and Materials": @"eadtab1-1",
              @"Being Imaginative":                       @"eadtab2-1",
              // LT
              @"Reading":                                 @"lttab1-1",
              @"Writing":                                 @"lttab2-1",
              // MT
              @"Numbers":                                 @"m_numbers-1",
              @"Shape Space and Measure":                 @"m_shapes-1",
              // PD
              @"Moving and Handling":                     @"pdtab1-1",
              @"Health and Self Care":                    @"pdtab2-1",
              // PSED
              @"Making Relationships":                    @"psedtab1-1",
              @"Self Confidence and Self Awareness":      @"psedtab2-1",
              @"Managing Feelings and Behaviour":         @"psedtab3-1",
              // UW
              @"People and Communities":                  @"utwtab1",
              @"The World":                               @"utwtab2",
              @"Technology":                              @"utwtab3",
              };
}

- (NSDictionary *) tabsIndexes
{
    return  @{
              // CL
              @"Listening and Attention":                 @0,
              @"Understanding":                           @1,
              @"Speaking":                                @2,
              // EAD
              @"Exploring and using media and materials": @0,
              @"Being Imaginative":                       @1,
              // LT
              @"Reading":                                 @0,
              @"Writing":                                 @1,
              // MT
              @"Numbers":                                 @0,
              @"Shape Space and Measure":                 @1,
              // PD
              @"Moving and Handling":                     @0,
              @"Health and Self Care":                    @1,
              // PSED
              @"Making relationships":                    @0,
              @"Self Confidence and Self Awareness":      @1,
              @"Managing feelings and behaviour":         @2,
              // UW
              @"People and Communities":                  @0,
              @"The World":                               @1,
              @"Technology":                              @2,
              };
}

- (NSDictionary *) aolsButtonsColors
{
    return @{
             @"cl": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"cl"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"cl"]]
                     },
             @"ead": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"ead"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"ead"]]
                     },
             @"lt": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"lt"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"lt"]]
                     },
             @"mt": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"mt"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"mt"]]
                     },
             @"pd": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"pd"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"pd"]]
                     },
             @"psed": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"psed"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"psed"]]
                     },
             @"uw": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"uw"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"uw"]]
                     },
             @"utw": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"utw"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"utw"]]
                     },
             };
}

@end
