//
//  DailyNotesChildrenNotesListViewController.m
//  pd2
//
//  Created by Andrey on 30.03.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "DailyNotesChildrenNotesListViewController.h"
#import "DailyNotesChildrenNoteCell.h"

#import "DailyNotesChildrenModel.h"
#import "DailyNotesChildrenNoteModel.h"

#import "DailyNotesChildrenNoteEditViewController.h"
#import "DailyNotesChildrenNoteCommentsViewController.h"


#import "ListPicker2ComponentsDict.h"

#define SELECT_YEAR_AND_MONTH_TEXT_FIELD_TAG 70


@interface DailyNotesChildrenNotesListViewController () <UITextFieldDelegate>

{
    
    NSString *PDSelectDateCell;
    NSString *PDNoteCell;
    
}

@property (nonatomic, assign) NSInteger year;
@property (nonatomic, assign) NSInteger month;

@property (nonatomic, strong) NSDictionary *yearAndMonths;


@property (nonatomic, strong) NSMutableArray *notesArray;

@property (nonatomic, strong) NSString *filterString;
@property (nonatomic, weak) UITextField *filterTextField;


@property (nonatomic, strong) ListPicker2ComponentsDict *listPicker;



@end

@implementation DailyNotesChildrenNotesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initRows];
    [self initLabels];
    
    self.year = [[NSDate date] year];
    self.month = [[NSDate date] month];
    
    self.yearAndMonths = [self genYearsAndMonths];
    
    
    AXSection *topSection = [[AXSection alloc] initWithType:PDTopSection];
    [topSection addRow:[self.sections genRowWithType:PDInfoPicCell]];
    [topSection addRow:[self.sections genRowWithType:PDSelectDateCell]];
    [topSection addRow:[self.sections genRowWithType:PDSpoilerCell]];
    [topSection addRow:[self.sections genRowWithType:PDFilterCell]];
    [self.sections addSection:topSection];
    
    //между ними лоадинг
    
//    AXSection *bottomSection = [[AXSection alloc] initWithType:PDBottomSection];
//    [bottomSection addRow:[self.sections genRowWithType:PDAddButtonCell]];
//    [bottomSection addRow:[self.sections genRowWithType:PDSpacerCell]];
//    [self.sections addSection:bottomSection];
    
    [self refreshData];
    
    self.topTitle = @"View Daily Notes";
    self.view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    
}


- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if ([[DataStateManager sharedManager] isChanged:DataStateDailyNotes forViewController:self]) {
        [[DataStateManager sharedManager] updated:DataStateDailyNotes forViewController:self];
        [self refreshData];
    }
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.listPicker hide];
    self.listPicker = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (void)initRows {
    [super initRows];
    
    PDSelectDateCell = @"PDSelectDateCell";
    PDNoteCell = @"PDNoteCell";
    
    
    [self.sections rememberType:PDInfoPicCell withEstimatedHeight:180.f identifier:@"infoPicCell"];
    [self.sections rememberType:PDSelectDateCell withHeight:43.f identifier:@"selectYearAndMonthCell"];
    [self.sections rememberType:PDNoteCell withEstimatedHeight:145.f identifier:@"noteCell"];
    
    [self.infoCells addObject:PDInfoPicCell];
    [self.contentFitCells addObject:PDNoteCell];
}


- (NSDictionary *)genYearsAndMonths {
    
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    
    for (int i = 2010; i <= 2030; i++) {
        [dict setObject:[[DataManager sharedInstance] getMonthsList] forKey:[NSString stringWithFormat:@"%d", i]];
    }
    
    return [NSDictionary dictionaryWithDictionary:dict];
    
}


#pragma mark - Configure cells

- (InfoPicCell *)configurePDInfoPicCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    InfoPicCell *cell = (InfoPicCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        cell = [self offscreenRenderingCell:cell withRow:row];
        [cell updateImageWithImagePath:self.childrenModel.photoPath];
        
        [self addBorder:PDBorderTopAndBottomDynamic toView:cell.containerView forToolbar:NO];
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}



- (UITableViewCell *)configurePDSelectDateCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        NSString *monthName = [[DataManager sharedInstance] monthShortNameFromNum:(int)self.month];
        
        NSString *yearAndMonth = [NSString stringWithFormat:@"%@ %d", monthName, (int)self.year];
        
        
        UITextField *selectYearAndMonthTextField = (UITextField *)[cell viewWithTag:SELECT_YEAR_AND_MONTH_TEXT_FIELD_TAG];
        selectYearAndMonthTextField.text = yearAndMonth;
        selectYearAndMonthTextField.delegate = self;
        
        
        //UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        
        //[self addBorder:PDBorderTop toView:containerView];
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}

- (UITableViewCell *)configurePDFilterCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    FilterCell *cell = (FilterCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        self.filterTextField = cell.filterTextField;
        
        cell.filterTextField.delegate = self;
        cell.filterTextField.text = [self.filterString copy];
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}

- (UITableViewCell *)configurePDNoteCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    DailyNotesChildrenNoteCell *cell = (DailyNotesChildrenNoteCell *)[tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        cell = [self offscreenRenderingCell:cell withRow:row];
        [cell activityIndicatorSetStatus:row.isForRemoval];
        
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        
        [self addBorder:PDBorderTop toView:containerView forToolbar:YES];
        [self addBorder:PDBorderTop toView:cell.toolbarView forToolbar:YES];
        [self addBorder:PDBorderRight toView:cell.toolbar1View forToolbar:YES];
        [self addBorder:PDBorderRight toView:cell.toolbar2View forToolbar:YES];
        [self addBorder:PDBorderBottomDynamic toView:containerView forToolbar:NO];
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        
        //NSLog(@"%@", row.userInfo);
    }
    
    return cell;
}

#pragma mark - OffScreen rendering

- (id)offscreenRenderingPDInfoPicCell:(InfoPicCell *)cell withRow:(AXRow *)row {
    
    DailyNotesChildrenModel *model = self.childrenModel;
    
    NSString *firstAndLastName = @"N/A";
    
    
    if (model.firstName && model.lastName) {
        firstAndLastName = [NSString stringWithFormat:@"%@ %@", [model.firstName capitalizedString], [model.lastName capitalizedString]];
    }
    
    
    
    NSMutableAttributedString *attributedString = [self attributedStringWithKey:PDInfoLabel forObject:cell.infoTextView];
    
    [self replace:@"%firstAndLastName%"
       withString:firstAndLastName
inAttributedString:&attributedString];
    
    
    [self replace:@"%age%"
       withString:[AXTableViewCell monthsFromDate:model.dateOfBirth]
inAttributedString:&attributedString];
    
    [self replace:@"%dateOfBirth%"
       withString:notAvailStringIfNil([model.dateOfBirth printableDefault])
inAttributedString:&attributedString];
    
    [self replace:@"%roomName%"
       withString:notAvailStringIfNil(model.roomName)
inAttributedString:&attributedString];
    
    cell.infoTextView.textContainer.exclusionPaths = @[[cell picBezierPath]];
    
    
    if  (![cell.infoTextView.attributedText isEqualToAttributedString:attributedString]) {
        cell.infoTextView.attributedText = attributedString;
        [row needUpdateHeight];
    }
    
    return cell;
}



- (id)offscreenRenderingPDNoteCell:(DailyNotesChildrenNoteCell *)cell withRow:(AXRow *)row {
    
    [cell updateWithModel:row.model];
    
    return cell;
}





#pragma mark - Remote

- (void)refreshData {
    
    [self cleanForRefresh];
    //[self updateInfoCell];
    [self insertLoadingCell];
    
    
    
    __weak __typeof(self)weakSelf = self;
    
    [[NetworkManager sharedInstance] dailyNotesGetNotesForChild:[self.childrenModel.chId intValue] year:self.year month:self.month showProgressHUD:YES withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            
            
            if([[response valueForKeyPath:@"result.success"] intValue] == 1)
            {
                
                NSArray *notes = [response valueForKeyPath:@"data"];
                [weakSelf updateTableWithData:notes];
                
            }
            else
            {
                
                //[self deleteLoadingSection];
                //[self updateTableWithRefreshMessage:[response valueForKeyPath:@"result.msg"]];
                Alert(@"Error",[response valueForKeyPath:@"result.message"] );
                
            }
        }
        else if(error)
        {
            
            //[self deleteLoadingSection];
            //[self updateTableWithRefreshMessage:@"Error happened while operation"];
            Alert(@"Error", @"Error happened while operation");
        }
    }];
    
}


- (void)deleteItemForRow:(AXRow *)row {
    
    DailyNotesChildrenNoteModel *model = row.model;
    row.isForRemoval = YES;
    [self updateRowCellWithRemovalActiovityIndicator:row];
    
    __weak __typeof(self)weakSelf = self;
    __block __weak __typeof(row)weakRow = row;
    
    
    
    [[NetworkManager sharedInstance] dailyNotesDeleteNoteWithId:[model.noteId intValue] showProgressHUD:NO withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            __strong __typeof(weakRow)strongRow = weakRow;
            if (!strongSelf || !strongRow) {
                return;
            }
            
            
            if([[response valueForKeyPath:@"result.success"] intValue] == 1)
            {
                if ([self.notesArray containsObject:model]) {
                    [self.notesArray removeObject:model];
                }
                [strongSelf deleteRows:@[strongRow]];
                [[DataStateManager sharedManager] changed:DataStateDailyNotes];
            }
            else
            {
                Alert(@"Error",[response valueForKeyPath:@"result.message"] );
                
                strongRow.isForRemoval = NO;
                [strongSelf updateRowCellWithRemovalActiovityIndicator:strongRow];
                
                
            }
        }
        else if(error)
        {
            
            Alert(@"Error", @"Error happened while operation");
            
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            __strong __typeof(weakRow)strongRow = weakRow;
            if (!strongSelf || !strongRow) {
                return;
            }
            strongRow.isForRemoval = NO;
            [strongSelf updateRowCellWithRemovalActiovityIndicator:strongRow];
            
            
        }
    }];
    
    
}



- (void)updateTableWithData:(NSArray *)data {
    
    
    if (!data || ![data isKindOfClass:[NSArray class]]) {
        [self cleanForRefresh];
        [self insertMessageCell]; //Empty default
        return;
    }
    
    
    
    NSMutableArray *modelsPrepared = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in data) {
        
        DailyNotesChildrenNoteModel *model = [[DailyNotesChildrenNoteModel alloc] initWithDict:dict];
        
        if (model && [model isExist]) {
            [modelsPrepared addObject:model];
        }
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    [modelsPrepared sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    self.notesArray = modelsPrepared;
    
    if ([modelsPrepared count] == 0) {
        [self cleanForRefresh];
        [self insertMessageCell]; //Empty default
        return;
    }
    /*
    AXSection *section = [[AXSection alloc] initWithType:PDMainSection];
    
    for (id model in modelsPrepared) {
        AXRow *row = [self.sections genRowWithType:PDNoteCell];
        row.model = model;
        
        [section addRow:row];
    }
    
    
    [self cleanForRefresh];
    
    NSIndexSet *insertedSectionsIndexSet =  [self.sections insertSections:@[section] afterSectionType:PDTopSection];
    
    
    [self.tableView reloadData];
    */
    
    
    [self updateTableFiltered];
    
}



- (void)updateTableFiltered {
    
    //Создаем фильтрованный список
    NSMutableArray *array = [NSMutableArray array];
    
    for (DailyNotesChildrenNoteModel *model in self.notesArray) {
        
        if ([model modelContain:self.filterString]) {
            [array addObject:model];
        }
        
    }
    
    if (![array count]) {
        [self cleanForRefresh];
        [self insertMessageCell]; //Empty default
        return;
    }
    
    NSMutableArray *sections = [NSMutableArray array];
    
    AXSection *section = [[AXSection alloc] initWithType:PDMainSection];
    
    for (id model in array) {
        AXRow *row = [self.sections genRowWithType:PDNoteCell];
        row.model = model;
        
        [section addRow:row];
    }
    [section addRow:[self.sections genRowWithType:PDSpacerCell]];
    [sections addObject:section];
    
    
    NSIndexSet *removeIndexSet = [self.sections getIndexSetForSectionsWithType:PDMainSection];
    [self.sections removeSectionsByIndexSet:removeIndexSet];
    if ([removeIndexSet count] > 0) {
        //[self.tableView deleteSections:removeIndexSet withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView reloadData];
    }
    
    
    
    NSIndexSet *indexSet = [self.sections insertSections:sections afterSectionType:PDTopSection];
    
    
    if ([indexSet count] > 0) {
        //[self.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView reloadData];
    }
    //    [self.tableView endUpdates];
    //    [UIView setAnimationsEnabled:YES];
    //[CATransaction setDisableActions:NO];
    
    
    //[self.tableView reloadData];
    
    
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    if (textField.tag == SELECT_YEAR_AND_MONTH_TEXT_FIELD_TAG) {
        
        //Проверка на пустой dict
        
        
        [self.view endEditing:YES];
        
        [self.listPicker hide];
        self.listPicker = [[ListPicker2ComponentsDict alloc] init];
        self.listPicker.dataDict = self.yearAndMonths;
        
        NSString *yearString = [NSString stringWithFormat:@"%d", (int)self.year];
        NSString *monthNameString = [[DataManager sharedInstance] monthNameFromNum:(int)self.month];
        
        self.listPicker.selectedStringIn0Component = yearString;
        self.listPicker.selectedStringIn1Component = monthNameString;
        
        __weak typeof(self) weakSelf = self;
        __weak typeof(textField) weakTextFiled = textField;
        
        [self.listPicker setHandler:^(NSString *comp0, NSString *comp1) {
            
            weakTextFiled.text = [NSString stringWithFormat:@"%@ %@", comp0, comp1];
            
            NSInteger year = [comp0 integerValue];
            NSInteger month = [[DataManager sharedInstance] monthNumFromName:comp1];
            
            if (weakSelf.year == year && weakSelf.month == month) {
                return;
            }
            
            weakSelf.year = year;
            weakSelf.month = month;
            
            [weakSelf refreshData];
            
        }];
        
        
        
        
        [self.listPicker show];
        
        return NO;
        
    }
    
    return YES;
    
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == FILTER_TEXT_FIELD_TAG) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        self.filterString = newString;
        
        
        NSIndexPath *indexPath = [self indexPathForView:textField];
        [self updateTableFiltered];
        
        FilterCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [cell.filterTextField becomeFirstResponder];
        //[textField becomeFirstResponder];
        return NO;
    }
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    
    if (textField.tag == FILTER_TEXT_FIELD_TAG) {
        [textField resignFirstResponder];
    }
    
    
    return YES;
}




/*
- (void)jump {
    
    if (!self.jumpToId) {
        return;
    }
    
    NSArray *indexPaths = [self.sections getIndexPathsForRowType:PDPermissionGroupCell atIndex:AXRowFirst withCondition:^BOOL(AXSection *section, AXRow *row) {
        PermissionGroupModel *model = row.model;
        if ([model.groupId isEqualToNumber:self.jumpToId]) {
            return YES;
        }
        
        return NO;
    }];
    
    self.jumpToId = nil;
    
    if (![indexPaths count]) {
        return;
    }
    
    [self.tableView scrollToRowAtIndexPath:[indexPaths firstObject] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    
}
*/


#pragma mark - Actions


- (IBAction)editAction:(id)sender {
    
    AXRow *row = [self rowForView:sender];
    GeneralNoteModel *model = row.model;
    //self.jumpToId = model.groupId;
    
    [self performSegueWithIdentifier:@"editSegue" sender:sender];
    
}


- (IBAction)viewAction:(id)sender {
    
    AXRow *row = [self rowForView:sender];
    GeneralNoteModel *model = row.model;
    //self.jumpToId = model.groupId;
    
    [self performSegueWithIdentifier:@"viewSegue" sender:sender];
    
}


- (IBAction)deleteAction:(id)sender {
    
    AXRow *row = [self rowForView:sender];
    
    PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Warning!" message:@"Deleting this information from the system can NOT be reversed!" preferredStyle:PSTAlertControllerStyleAlert];
    
    [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:nil]];
    
    [alert addAction:[PSTAlertAction actionWithTitle:@"Delete" style:PSTAlertActionStyleDestructive handler:^(PSTAlertAction *action) {
        
        [self deleteItemForRow:row];
    }]];
    
    [alert showWithSender:self controller:self animated:YES
               completion:nil];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"editSegue"]) {
        
        AXRow *row = [self rowForView:sender];
        DailyNotesChildrenNoteModel *model = row.model;
        
        DailyNotesChildrenNoteEditViewController *viewController = segue.destinationViewController;
        viewController.noteId = [model.noteId intValue];
        viewController.nameString = [model.noteTitle copy];
        viewController.nameOrigString = [model.noteTitle copy];
        viewController.noteString = [model.note copy];
        
        viewController.childrenModel = self.childrenModel;
        
    }
    
    else if ([segue.identifier isEqualToString:@"viewSegue"]) {
        
        AXRow *row = [self rowForView:sender];
        DailyNotesChildrenNoteModel *model = row.model;
        
        DailyNotesChildrenNoteCommentsViewController *viewController = segue.destinationViewController;
        viewController.noteId = [model.noteId intValue];
        viewController.childrenModel = self.childrenModel;
        
    }
    
    
}




@end
