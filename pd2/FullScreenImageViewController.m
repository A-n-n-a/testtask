//
//  FullScreenImageViewController.m
//  pd2
//
//  Created by User on 13.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "FullScreenImageViewController.h"

@interface FullScreenImageViewController ()

@end

@implementation FullScreenImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setImage:(UIImage *)image
{
    _image=image;
    _imageView=[[UIImageView alloc]initWithImage:_image];
    _imageView.frame=CGRectMake(0, 0, _image.size.width, _image.size.height);
    _scrollView=[[UIScrollView alloc]initWithFrame:[AppDelegate delegate].window.bounds];
    _scrollView.contentSize=_image.size;
    [_scrollView addSubview:_imageView];
    _scrollView.scrollEnabled=YES;
}

-(void)setImagesArray:(NSArray *)imagesArray
{
    
}

-(void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
