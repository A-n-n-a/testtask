//
//  TimerUIApplication.m
//  pd2
//
//  Created by i11 on 09/02/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "TimerUIApplication.h"

@implementation TimerUIApplication

//here we are listening for any touch. If the screen receives touch, the timer is reset
/*
-(void)sendEvent:(UIEvent *)event
{
    [super sendEvent:event];
    
    if (!self.idleTimer)
    {
        [self resetIdleTimer];
    }
    
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0)
    {
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        if (phase == UITouchPhaseBegan)
        {
            [self resetIdleTimer];
        }
        
    }
}
*/
//as labeled...reset the timer
-(void)resetIdleTimer
{
    if (self.idleTimer)
    {
        [self.idleTimer invalidate];
    }
    //convert the wait period into minutes rather than seconds
    int timeout = kApplicationTimeoutInMinutes * 60;
    self.idleTimer = [NSTimer scheduledTimerWithTimeInterval:timeout target:self selector:@selector(idleTimerExceeded) userInfo:nil repeats:NO];
    
}
//if the timer reaches the limit as defined in kApplicationTimeoutInMinutes, post this notification
-(void)idleTimerExceeded
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kApplicationDidTimeoutNotification object:nil];
}


-(void)updateNetworkActivity {
    
    
    self.lastNetworkActivityDate = [NSDate date];
    [self resetIdleTimer];
}

@end
