//
//  DailyNotesAddNoteCopyCell.h
//  pd2
//
//  Created by Andrey on 28/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@class DailyNotesNoteModel;

@interface DailyNotesAddNoteCopyCell : AXTableViewCell

@property (weak, nonatomic) IBOutlet UITextField *noteCopyTextField;

- (void)updateCopyIndex:(NSUInteger)index;


@end
