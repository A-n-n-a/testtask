//
//  AppDelegate.m
//  pd2
//
//  Created by Admin on 19.01.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "TimerUIApplication.h"
#import "UserManager.h"
#import "AccessTimeManager.h"
#import "SplashViewController.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "AFNetworkActivityLogger.h"
#import "NSDate+Utilities.h"
#import "NSDate+Printable.h"


//#import "NSString+RemoveEmoji.h"
//#import "Utils.h"


#import <AVKit/AVKit.h>

#import "PushNotificationManager.h"

#import "WL.h"

//Для AFNetworking разрешаем невалидные сертификаты ssl
#define _AFNETWORKING_ALLOW_INVALID_SSL_CERTIFICATES_


//В системе есть обращения к серверам с невалидными ssl сертификатами. Разрешаем
@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}
@end



@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidTimeout:) name:kApplicationDidTimeoutNotification object:nil];

    //[[TimerUIApplication sharedInstance] setLastNetworkActivityDate:[NSDate date]];
    
    //[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    //ActivityIndicator status bar
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
//    (TimerUIApplication *)[[TimerUIApplication sharedApplication] setLastNetworkActivityDate:NSDate date];
    

    
    //[[AFNetworkActivityLogger sharedLogger] startLogging];
    //[[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelDebug];

    
    //Push
    
    
    //NSDate *date = [NSDate dateWithTimeIntervalSince1970:1516902540];
    //[date testDateFormates];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }else{
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    
    /*
    NSString *someString = [NSNull null];
    
    NSLog(@"emoji 1: %@", [Utils safeRemoveEmoji:someString]);
    //someString = [someString stringByRemovingEmoji];
    NSLog(@"emoji 2: %@", [Utils safeRemoveEmoji:@"😂"] ? : @"No");
    */
    
    NSDictionary *apnsBody = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (apnsBody) {
        

        //NSString *json = [localNotif valueForKey:@"data"];
        //NSString *json = [NSString stringWithFormat:@"%@", apnsBody];
        
        [[PushNotificationManager sharedInstance] addPush:apnsBody andBackground:YES];
        //Alert(@"Debug", json);
    }
    
    return YES;
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSMutableString *baseStringToken = [[NSMutableString alloc] initWithFormat:@"%@", deviceToken];
    NSString *newString1 = [baseStringToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    [[NSUserDefaults standardUserDefaults] setObject:newString3 forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    //register to receive notifications
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

//For interactive notification only
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler {
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]) {
    
    }
    else if ([identifier isEqualToString:@"answerAction"]) {
    
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    
    
 
    NSLog(@"%@", userInfo);
    
    
    
    //NSString *title = [userInfo valueForKeyPath:@"aps.alert"];
    //NSString *msg = [userInfo valueForKeyPath:@"aps.url"];
    
    //Alert(title, msg);
 
    BOOL isBackground = (application.applicationState == UIApplicationStateBackground || application.applicationState == UIApplicationStateInactive) ? YES : NO;
    
    NSString *string = [NSString stringWithFormat:@"state: %@\n%@", isBackground ? @"Background" : @"Foreground", userInfo];
    //Alert(@"Debug", string);
    
    [[PushNotificationManager sharedInstance] addPush:userInfo andBackground:isBackground];
    
 
    
 

}



- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
    
}




- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    self.didEnterBackgroundDate = [NSDate date];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    /*
    if (self.didEnterBackgroundDate && [self.didEnterBackgroundDate minutesBeforeDate:[NSDate date]] > kApplicationTimeoutInMinutes) {
        [(TimerUIApplication *)[TimerUIApplication sharedInstance] resetIdleTimer];
        
            [self backToRootViewController];
    }
    */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - App timeOut acation

-(void)applicationDidTimeout:(NSNotification *)userInfo {
    NSLog (@"time exceeded!!");
    
    //UIViewController *controller = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:NULL] instantiateViewControllerWithIdentifier:@"mainView"];
    
    //[(UINavigationController *)self.window.rootViewController popToRootViewControllerAnimated:YES];
    
    //logout только если залогинены
    if ([UserManager sharedManager].loggedUser) {
        [self backToRootViewController];
    }
    

}


- (void)backToRootViewController {
    
    //Если открыто модальное окно, то оно не закрывается на popToRoot
    if ([NSStringFromClass([[self topViewController] class]) isEqualToString:@"AVFullScreenViewController"]) {

        NSLog(@"have modal view");
        if ([[self topViewController] respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]) {
            [[self topViewController] dismissViewControllerAnimated:NO completion:nil];
            NSLog(@"close modal view");
        }
        
    }
    
    [AppDelegate logoutAndSendDeviceTokenIs:NO];
    [(UINavigationController *)self.window.rootViewController popToRootViewControllerAnimated:YES];
    
}

+ (void)logoutAndSendDeviceTokenIs:(BOOL)isSendDeviceToken {
#warning копия метода логаут из MainMenuVC. Вынести в отдельный класс
    
    [[AccessTimeManager sharedInstance] disableTimer];
    
    //Нет смысла отслеживать какой получен ответ. Отвязка от токена в приложении. На сервер не передаем никаких идентификаторов
    [[NetworkManager sharedInstance] logoutAndDeviceToken:isSendDeviceToken completion:^(id response, NSError *error){}];
    
    [DataManager sharedInstance].adminId = 0;
    [NetworkManager sharedInstance].token = nil;
    
    /*if ([[UserManager sharedManager] isAllUsersRecivePinAvailability]) {
     if ([[UserManager sharedManager] isUsersWithPin]) {
     [self performSegueWithIdentifier:@"enterPinSegue" sender:self];
     NSLog(@"have users with pin");
     } else {
     [self performSegueWithIdentifier:@"loginSegue" sender:self];
     NSLog(@"NO have users with pin");
     }
     }*/
    [[UserManager sharedManager] recieveAndFillPinAvailabilityForAllUsers];
    [[UserManager sharedManager] successfullyLogout]; //запустить принудительную проверку пинов
    
    
    [[PushNotificationManager sharedInstance] clearQueue];
    
    UIWindow *window = [[UIApplication sharedApplication] delegate].window;
    
    SplashViewController *vc = [[(UINavigationController *)window.rootViewController viewControllers] firstObject];
    [vc setColorTheme]; //обновляем цвет
    [(UINavigationController *)window.rootViewController popToRootViewControllerAnimated:YES];
}


# pragma mark - Helper methods

+(AppDelegate *)delegate
{
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}

- (NSString *) applicationDocumentDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

+(void)changePositionInYofView:(UIView *)view inPixels:(int)pixels
{
    CGRect frame=view.frame;
    frame.origin.y+=pixels;
    view.frame=frame;
}


+(void)changeHeightOfView:(UIView *)view inPixels:(int)pixels
{
    CGRect frame=view.frame;
    frame.size.height+=pixels;
    view.frame=frame;
}

+(void)showProgressHUD
{
    //На время appium+selenium тестирования - отключам, так как накладывает отдельный слой
    [MBProgressHUD showHUDAddedTo:[AppDelegate delegate].window animated:YES];
}

+(void)hideProgressHUD
{

    [MBProgressHUD hideAllHUDsForView:[AppDelegate delegate].window animated:YES];
}


+ (void)showCompletedHUD
{
    [AppDelegate showCompletedHUDWithCompletionBlock:nil];
}

+ (void)showCompletedHUDWithCompletionBlock:(void(^)(void))completionBlock {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithWindow:[AppDelegate delegate].window];
    //[self.navigationController.view addSubview:HUD];
    
    [[AppDelegate delegate].window addSubview:HUD];
    
    // The sample image is based on the work by http://www.pixelpressicons.com, http://creativecommons.org/licenses/by/2.5/ca/
    // Make the customViews 37 by 37 pixels for best results (those are the bounds of the build-in progress indicators)
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark"]] ;
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeCustomView;
    
    //HUD.delegate = HUD;
    HUD.labelText = @"Completed";
    
    //HUD.color = [UIColor colorWithRed:0.23 green:0.50 blue:0.82 alpha:0.90];
    
    HUD.completionBlock = completionBlock;
    
    [HUD show:YES];
    [HUD hide:YES afterDelay:2.5f];
}

+ (void)showCompletedHUDWithMessage:(NSString *)message
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithWindow:[AppDelegate delegate].window];
    
    [[AppDelegate delegate].window addSubview:HUD];
    
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark"]] ;
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeCustomView;
    
    //HUD.delegate = HUD;
    HUD.labelText = message;
    
    [HUD show:YES];
    [HUD hide:YES afterDelay:2.5f];
}


+ (void)showSettingsHUDWithMessage:(NSString *)message
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithWindow:[AppDelegate delegate].window];
    
    [[AppDelegate delegate].window addSubview:HUD];
    
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"settingsHUD"]] ;
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeCustomView;
    
    //HUD.delegate = HUD;
    HUD.labelText = message;
    
    [HUD show:YES];
    [HUD hide:YES afterDelay:2.5f];
}


+ (void)showHUDWithMessage:(NSString *)message andCompletionBlock:(void(^)(void))completionBlock {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithWindow:[AppDelegate delegate].window];
    //[self.navigationController.view addSubview:HUD];
    
    [[AppDelegate delegate].window addSubview:HUD];
    
    // The sample image is based on the work by http://www.pixelpressicons.com, http://creativecommons.org/licenses/by/2.5/ca/
    // Make the customViews 37 by 37 pixels for best results (those are the bounds of the build-in progress indicators)
    //HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark"]] ;
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeText;
    
    //HUD.delegate = HUD;
    HUD.labelText = message;
    
    //HUD.color = [UIColor colorWithRed:0.23 green:0.50 blue:0.82 alpha:0.90];
    
    HUD.completionBlock = completionBlock;
    
    [HUD show:YES];
    [HUD hide:YES afterDelay:2.5f];
}


+ (void)showHUDWithSmallSizeMessage:(NSString *)message andCompletionBlock:(void(^)(void))completionBlock {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithWindow:[AppDelegate delegate].window];
    //[self.navigationController.view addSubview:HUD];
    
    [[AppDelegate delegate].window addSubview:HUD];
    
    // The sample image is based on the work by http://www.pixelpressicons.com, http://creativecommons.org/licenses/by/2.5/ca/
    // Make the customViews 37 by 37 pixels for best results (those are the bounds of the build-in progress indicators)
    //HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark"]] ;
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeText;
    
    //HUD.delegate = HUD;
    HUD.detailsLabelText = message;
    
    //HUD.color = [UIColor colorWithRed:0.23 green:0.50 blue:0.82 alpha:0.90];
    
    HUD.completionBlock = completionBlock;
    
    [HUD show:YES];
    [HUD hide:YES afterDelay:2.5f];
}




+ (void)showCompletedHUDWithMessage:(NSString *)message andCompletionBlock:(void(^)(void))completionBlock
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithWindow:[AppDelegate delegate].window];
    [[AppDelegate delegate].window addSubview:HUD];
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark"]] ;
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.labelText = message;
    HUD.completionBlock = completionBlock;
    [HUD show:YES];
    [HUD hide:YES afterDelay:2.5f];
}


///

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    
    
    
    
    
     return UIInterfaceOrientationMaskPortrait;
    
}


- (UIViewController*)topViewController
{
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}


- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}



+ (UIViewController*)pTopViewController
{
    return [AppDelegate pTopViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}


+ (UIViewController*)pTopViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [AppDelegate pTopViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [AppDelegate pTopViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [AppDelegate pTopViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}



//Для определения положения при прогрывании видео
+ (BOOL)isAVFullScreenViewController {
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    
    if ([[window.rootViewController presentedViewController] isKindOfClass:NSClassFromString(@"AVFullScreenViewController")]) {
        return YES;
    }
    else {
        return NO;
    }
    
}

+ (BOOL)isPortrait {
    
    if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait) {
        return YES;
    }
    else {
        return NO;
    }
    
}


+ (NSString *)randomString {
    
    int length = 8;
    
    NSMutableString *timeStamp =  [NSMutableString stringWithFormat:@"%.0f", [[NSDate date] timeIntervalSince1970]];
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    
        
    NSMutableString *randomString = [NSMutableString stringWithCapacity: length];
    
    for (int i=0; i<length; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    [timeStamp appendString:randomString];
    
    return timeStamp;
    
}



#pragma mark - first run after update

+ (BOOL)isFirstLaunchAfterTouchIdUpdate {
    //При первом случае: YES
    NSNumber *firstLaunch = [[NSUserDefaults standardUserDefaults] objectForKey:@"TouchIdIsShowsAfterUpdate"];
    
    if (firstLaunch) {
        return [firstLaunch boolValue];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:@"TouchIdIsShowsAfterUpdate"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return YES;
    }
}


+ (void)appLaunchedAfterTouchIdUpdate {
    //Когда не первый раз - NO
    [[NSUserDefaults standardUserDefaults] setObject:@(NO) forKey:@"TouchIdIsShowsAfterUpdate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}



@end
