//
//  RoundedTextView.h
//  pd2
//
//  Created by User on 12/23/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoundedTextView : UITextView

@property (nonatomic, retain) NSString *placeholder;
@property (nonatomic, retain) UIColor *placeholderColor;
@property (nonatomic, retain) UIFont *placeholderFont;

-(void)textChanged:(NSNotification*)notification;
- (CGRect) framtForMinHeight:(CGFloat) minHeight;

@end
