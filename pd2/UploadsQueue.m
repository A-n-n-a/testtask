#import "UploadsQueue.h"
#import "AFNetworking.h"
#import "NSString+Utils.h"


@interface NSNotificationCenter (MainThread)

- (void)postNotificationOnMainThread:(NSNotification *)notification;
- (void)postNotificationOnMainThreadName:(NSString *)aName object:(id)anObject;
- (void)postNotificationOnMainThreadName:(NSString *)aName object:(id)anObject userInfo:(NSDictionary *)aUserInfo;

@end

@implementation NSNotificationCenter (MainThread)

- (void)postNotificationOnMainThread:(NSNotification *)notification
{
    [self performSelectorOnMainThread:@selector(postNotification:) withObject:notification waitUntilDone:YES];
}

- (void)postNotificationOnMainThreadName:(NSString *)aName object:(id)anObject
{
    NSNotification *notification = [NSNotification notificationWithName:aName object:anObject];
    [self postNotificationOnMainThread:notification];
}

- (void)postNotificationOnMainThreadName:(NSString *)aName object:(id)anObject userInfo:(NSDictionary *)aUserInfo
{
    NSNotification *notification = [NSNotification notificationWithName:aName object:anObject userInfo:aUserInfo];
    [self postNotificationOnMainThread:notification];
}

@end

@interface UploadsQueue()

@property (strong, nonatomic) NSMutableDictionary * dict_operationsBlocks;

@end

@implementation UploadsQueue

#pragma mark - Public

- (void) addOperation:(AFHTTPRequestOperation *)operation toKey:(NSString *)key completion:(void (^)(id response, NSError *error))completion
{

    [self incrementCounterForKey:key];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        responseObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        [self decrementCounterForKey:key];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:UploadsQueueOperationCompleteNotification object:nil userInfo:@{@"key":key}];

        
        
        if (completion) {
            completion(responseObject, nil);
        }
        
        
        
        NSLog(@"--request headers:-------------------------------\n%@",[operation.request allHTTPHeaderFields]);
        NSLog(@"--request response string:-----------------------\n%@", operation.responseString);
        NSLog(@"--request string:--------------------------------\n%@",operation.request.URL.absoluteString);

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"--request headers:-------------------------------\n%@",[operation.request allHTTPHeaderFields]);
        NSLog(@"--request response string:-----------------------\n%@", operation.responseString);
        NSLog(@"--request string:--------------------------------\n%@",operation.request.URL.absoluteString);
        
        [self decrementCounterForKey:key];
        if (completion) {
            completion(nil, error);
        }
    }];
    [operation start];
}

- (BOOL) uploadsIsCompleteForKey:(NSString *)key
{
    NSNumber * counter = [self getCounterForKey:key];
    return [counter integerValue] == 0;
}



#pragma mark - Private

- (id) init
{
    if (self = [super init]) {
        self.dict_operationsBlocks = [NSMutableDictionary new];
    }
    return self;
}

- (NSNumber *) getCounterForKey:(NSString *)key
{
    key = [NSString md5:key];
    if ([self.dict_operationsBlocks valueForKey:key] == nil) {
        [self.dict_operationsBlocks setObject:[NSNumber numberWithInteger:0] forKey:key];
    }
    return self.dict_operationsBlocks[key];
}

- (void) setCounter:(NSNumber *)counter forKey:(NSString *)key
{
    key = [NSString md5:key];
    @synchronized(self.dict_operationsBlocks) {
        [self.dict_operationsBlocks setObject:counter forKey:key];
    }
}

- (void) incrementCounterForKey:(NSString *)key
{
    NSNumber * counter = [self getCounterForKey:key];
    @synchronized(counter) {
        [self setCounter:[NSNumber numberWithInteger:([counter integerValue] + 1)] forKey:key];
    }
}

- (void) decrementCounterForKey:(NSString *)key
{
    NSNumber * counter = [self getCounterForKey:key];
    @synchronized(counter) {
        [self setCounter:[NSNumber numberWithInteger:([counter integerValue] - 1)] forKey:key];
    }
}

@end
