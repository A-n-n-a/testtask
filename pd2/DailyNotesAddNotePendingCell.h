//
//  DailyNotesAddNotePendingCell.h
//  pd2
//
//  Created by Andrey on 09/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@class DailyNotesNoteModel;

@interface DailyNotesAddNotePendingCell : AXTableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *checkboxImageView;

- (void)updateWithModel:(DailyNotesNoteModel *)model;


@end
