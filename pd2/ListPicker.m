//
//  ListPicker.m
//  Exchange
//
//  Created by User on 16.06.14.
//  Copyright (c) 2014 Vexadev LLC. All rights reserved.
//

#import "ListPicker.h"

@interface ListPicker ()

@end

@implementation ListPicker

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.frame=[UIScreen mainScreen].bounds;
    self.picker.backgroundColor=[UIColor whiteColor];
    self.doneButton.action=@selector(close:);
    
    if (self.rightButtonCustomTitle) {
        self.doneButton.title = self.rightButtonCustomTitle;
    }
    
    [self.clearButton setTintColor:[UIColor redColor]];
    
    if (self.showClearButton) {
        self.clearButton.title = @"Clear";
        self.clearButton.enabled = YES;
    }
    else {
        
        if (self.leftCustomTitle) {
            
            
            NSDictionary *titleDict = @{ 
                                         NSForegroundColorAttributeName: [UIColor darkGrayColor] };
            
            [self.clearButton setTitleTextAttributes:titleDict forState:UIControlStateNormal];
            [self.clearButton setTitleTextAttributes:titleDict forState:UIControlStateDisabled];
            
            //[self.clearButton setTintColor:[UIColor grayColor]];
            self.clearButton.title = self.leftCustomTitle;
            self.clearButton.enabled = NO;
        }
        else {
            self.clearButton.title = nil;
            self.clearButton.enabled = NO;
        }
        
        
    }
    
    
    NSLog(@"picker:%@",self.picker);
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if (_selectedIndex>=0)
    {
        [self.picker selectRow:self.selectedIndex inComponent:0 animated:NO];
        [self pickerView:self.picker didSelectRow:self.selectedIndex inComponent:0];
    }
}


-(IBAction)close:(id)sender
{
    NSLog(@"listpicker.close");
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        if (self.handler)
        {
            self.handler(_selectedString,_selectedIndex);
        }

        if (self.finishHandler)
        {
            self.finishHandler();
        }
        self.handler=nil;
        self.finishHandler=nil;
    }];
}

-(IBAction)clear {
    
    if (self.clearHandler) {
        self.clearHandler();
    }
    
    [self hide];
    
}


-(void)hide
{
    NSLog(@"listpicker.hide");
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    } completion:^(BOOL finished) {
        if (self.finishHandler)
        {
            self.finishHandler();
        }
        self.finishHandler=nil;
        [self.view removeFromSuperview];
    
    }];
}


-(void)show
{
    //int height=[UIScreen mainScreen].bounds.size.height;
    CGRect frame =[UIScreen mainScreen].bounds;// CGRectMake(0,0, 320, 568);
    
    CGRect startRect = frame;
    startRect.origin.y += self.view.frame.size.height;
    
    self.view.frame = startRect;
    
    AppDelegate *delegate=[AppDelegate delegate];
    [delegate.window addSubview:self.view];
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    }];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.dataArray count];
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.dataArray objectAtIndex:row];
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 20)];
    label.textAlignment=NSTextAlignmentCenter;
    label.font=[UIFont systemFontOfSize:16];
    label.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    label.tag=row;
    
    return label;
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (self.dataArray.count)
    {
        NSString *str=[self.dataArray objectAtIndex:row];

        _selectedIndex=(int)row;
        _selectedString=str;
        
        /*
        if (self.handler)
        {
            self.handler(str,(int)row);
        }
        */
    }
}

@end
