//
//  UploadImageCell.m
//  pd2
//
//  Created by i11 on 17/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "UploadImageCell.h"

@interface UploadImageCell ()

@property (nonatomic, copy) void (^selectImageCompletion)(UIImage *image);

@end

@implementation UploadImageCell

- (void)updateWithImage:(UIImage *)image orImagePath:(NSString *)imagePath downloadImageCompletion:(void(^)(UIImage *downloadedImage))downloadImageCompletion selectImageCompletion:(void(^)(UIImage *selectedImage))selectImageCompletion {
    
    self.selectImageCompletion = selectImageCompletion;
    
    if (self.editMode) {
        self.descriptionLabel.text = @"Replace Photo";
    }
    else {
        self.descriptionLabel.text = @"Upload Photo";
    }
    
    
    if (image) {
        
        self.picImageView.image = image;
        
    } else if (imagePath && [imagePath length]) {
        
        [self updateProfileImageView:self.picImageView withImagePath:imagePath andDefaultImagePath:nil downloadImageCompletition:^(UIImage *downloadedImage) {
            if (downloadImageCompletion) {
                downloadImageCompletion(downloadedImage);
            }
        }];
        
        
    } else {
        //заглушка
        self.picImageView.image = [[UIImage imageNamed:@"addPhotoButton"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.picImageView.tintColor = [UIColor whiteColor];
    }
    
    CALayer *layer = self.picImageView.layer;
    layer.masksToBounds = YES;
    layer.cornerRadius = layer.frame.size.width / 2;
    layer.borderWidth = 0.3f;
    layer.borderColor = [UIColor grayColor].CGColor;
}



- (IBAction)uploadImageAction:(id)sender {
    
    if (![self.delegate conformsToProtocol:@protocol(UploadImageCellDelegate)]) {
        [NSException raise:@"UploadImageCellDelegate Exception"
                    format:@"Parameter does not conform to UploadImageCellDelegate protocol at line %d", (int)__LINE__];
    }
    
    [self.delegate uploadImageActionForImageView:self.picImageView withCompletion:self.selectImageCompletion];

}


@end
