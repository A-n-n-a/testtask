//
//  DailyNotesAddNoteTitleCell.h
//  pd2
//
//  Created by Andrey on 28/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@class DailyNotesNoteModel;

@interface DailyNotesAddNoteTitleCell : AXTableViewCell
@property (weak, nonatomic) IBOutlet UITextField *noteTitleTextField;
@property (weak, nonatomic) IBOutlet UITextField *dateTextField;

- (void)updateWithModel:(DailyNotesNoteModel *)model;

@end
