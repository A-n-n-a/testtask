//
//  DailyNotesAddNoteCopyCell.m
//  pd2
//
//  Created by Andrey on 28/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "DailyNotesAddNoteCopyCell.h"


@implementation DailyNotesAddNoteCopyCell

- (void)updateCopyIndex:(NSUInteger)index {
    
    if (index == NSNotFound) {
        self.noteCopyTextField.text = @"";
    }
    else {
        self.noteCopyTextField.text = [NSString stringWithFormat:@"Daily Note %lu", (unsigned long)index];
    }
    
    
    
}

@end
