//
//  DailyNotesChildrenNoteCommentCell.h
//  pd2
//
//  Created by i11 on 08/06/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@class DailyNotesCommentModel;

@interface DailyNotesChildrenNoteCommentCell : AXTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;

@property (weak, nonatomic) IBOutlet UIImageView *picImageView;


- (void)updateWithModel:(DailyNotesCommentModel *)model;
- (void)updateImageWithModel:(DailyNotesCommentModel *)model;

@end
