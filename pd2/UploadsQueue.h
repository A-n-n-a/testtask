#import "Singleton.h"

#define UploadsQueueOperationCompleteNotification @"uploadsQueue.operation.complete"
#define UploadsQueueVideoOperationCompleteNotification @"uploadsQueue.video.operation.complete"

@interface UploadsQueue : Singleton

- (void) addOperation:(AFHTTPRequestOperation *)operation
                toKey:(NSString *)key
           completion:(void (^)(id response, NSError *error))completion;
- (BOOL) uploadsIsCompleteForKey:(NSString *)key;

@end
