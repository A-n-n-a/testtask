//
//  FilterCell.h
//  pd2
//
//  Created by i11 on 25/10/15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@interface FilterCell : AXTableViewCell

@property (nonatomic, weak) IBOutlet UITextField *filterTextField;

@end
