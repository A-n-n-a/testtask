#import <Foundation/Foundation.h>

@interface NSDateFormatter (Convert)

+ (NSString *) convertDateFromString:(NSString *)date
                          fromFormat:(NSString *)fromFormat
                            toFormat:(NSString *)toFormat;

@end
