//
//  ChildFilterModel.h
//  pd2
//
//  Created by Andrey on 10/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXBaseModel.h"

@interface ChildFilterModel : AXBaseModel

@property (nonatomic, strong) NSNumber *chId;

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;

@property (nonatomic, strong) NSString *photoPath;
@property (nonatomic, strong) NSString *roomName;
@property (nonatomic, strong) NSDate *dateOfBirth;

@property (nonatomic, assign) BOOL isArchive;
@property (nonatomic, assign) BOOL isChecked;

@property (nonatomic, strong) NSDictionary *sourceDict;

- (instancetype)initWithDict:(NSDictionary *)dict;
- (instancetype)initWithDailyNotesDict:(NSDictionary *)dict;


- (NSString *)childrenFullName;
- (NSString *)childrenShortName;

//filter
- (BOOL)nameContain:(NSString *)containString;



@end
