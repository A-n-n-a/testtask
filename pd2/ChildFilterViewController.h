//
//  ChildFilterViewController.h
//  pd2
//
//  Created by Andrey on 10/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXTableViewController.h"

@class ChildFilterModel;

@protocol ChildFilterDelegate <NSObject>

- (void)didSelectedModel:(ChildFilterModel *)model;

@end

@protocol ChildFilterMultipleDelegate <NSObject>

- (void)didSelectedChildModels:(NSArray *)models;

@end



@interface ChildFilterViewController : AXTableViewController

@property (nonatomic, weak) id<ChildFilterDelegate> filterDelegate;
@property (nonatomic, weak) id<ChildFilterMultipleDelegate> filterMultipleDelegate;
@property (nonatomic, strong) NSArray *excludeChildIds;
@property (nonatomic, strong) NSArray *includeChildIds;
@property (nonatomic, strong) NSArray *selectedChildIds;


@property (nonatomic, assign) BOOL showArchive;
@property (nonatomic, assign) BOOL multipleSelect;

@property (nonatomic, assign) BOOL isSelectAll;

- (IBAction)checkAllChildrenAction:(id)sender;
- (IBAction)checkAllChildrenInRoomAction:(id)sender;



-(IBAction)saveAction:(id)sender;


@end



