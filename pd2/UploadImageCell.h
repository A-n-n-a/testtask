//
//  UploadImageCell.h
//  pd2
//
//  Created by i11 on 17/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

//********************************************************
//Класс ячейки для обработки/отображения upload image.
//Исользуется в Visitors storyboard.
//Скопировать ячейку в свою таблицу и можно использовать.
//При сохранении на сервер существующей картинки -  вызывать [[ImageCache sharedInstance] deleteImageForLink:link] для очистки кеша


//Тестируется, о найденных багах и желаемых фичах - сообщать или совместно править.
//Андрей
//********************************************************

#import "AXTableViewCell.h"
#import "UploadImageCellDelegate.h"

@interface UploadImageCell : AXTableViewCell
//UI
@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@property (weak, nonatomic) IBOutlet UIButton *uploadImageButton; //for disable userIteraction, etc. Future
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

//Contoller
@property (nonatomic, assign) BOOL editMode;
- (void)updateWithImage:(UIImage *)image orImagePath:(NSString *)imagePath downloadImageCompletion:(void(^)(UIImage *downloadedImage))downloadImageCompletion selectImageCompletion:(void(^)(UIImage *selectedImage))selectImageCompletion;
@property (nonatomic, weak) id <UploadImageCellDelegate> delegate;

- (IBAction)uploadImageAction:(id)sender;

@end

