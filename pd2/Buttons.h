//
//  Buttons.h
//  pd2
//
//  Created by i11 on 16/04/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Buttons : UIButton

@property (strong, nonatomic) NSNumber *isActivityIndicatorActive;

- (void)initialSetup;
- (void)makeMeAsConfirmButton;
- (void)makeMeAsCancelButton;

- (void)setTitleForAllStates:(NSString *)title;

- (void)setActivityIndicatorStatus:(BOOL)status;

- (void)showActivityIndicator;
- (void)hideActivityIndicator;

@end
