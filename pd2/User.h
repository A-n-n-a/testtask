//
//  User.h
//  pd2
//
//  Created by i11 on 25/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AXBaseModel.h"
#import "UserAccessTypeModel.h"

@protocol UserDelegate;

@class UserManager;

@interface User : AXBaseModel

typedef enum : NSUInteger {
    UserTypeAdmin,
    UserTypeParent
} UserType;


@property (assign, nonatomic) int uid; //user id, ex 63
@property (strong, nonatomic) NSNumber *puid; //parent user id (for push)
@property (strong, nonatomic) NSString *userName; //userName ex admin
@property (strong, nonatomic) NSString *subDomain; //local variable
@property (strong, nonatomic) NSString *companyName; //local variable

@property (assign, nonatomic) BOOL isLastLogged; //last logged in mobile device
@property (assign, nonatomic) BOOL isForStoring; //dont save if NO
@property (assign, nonatomic) BOOL isForDelete; //dont save if NO



@property (strong, nonatomic) NSString *firstName; //first_name, ex John
@property (strong, nonatomic) NSString *lastName; //last_name, ex Dow

@property (strong, nonatomic) NSString *email; //ex sales@yahoo.com
@property (strong, nonatomic) NSString *picURL; //ex image/1/2/image.jpg
@property (strong, nonatomic) UIImage *image; //ex image (j3)

@property (assign, nonatomic) int acl; //
@property (assign, nonatomic) UserType userType; //dynamic admin or parent

@property (strong, nonatomic) NSNumber *parentNum; //1...2 номер парента

@property (assign, nonatomic) BOOL havePin; //if pin exists
@property (assign, nonatomic) BOOL pinAvailabilityRecived; //Status of remote pin recive complete
@property (assign, nonatomic) BOOL isPinFailedAttempt; //5 failed attempts enter Pin

@property (assign, nonatomic) BOOL isTouchId; //Если выбран как пользователь для touch id
@property (strong, nonatomic) NSString *touchIdToken;

@property (assign, nonatomic) BOOL isNewUser;

@property (weak, nonatomic) id <UserDelegate> delegate;

//Для parent заполняем доступность секций (options)
@property (assign, nonatomic) BOOL bookkeepingAvailable;
@property (assign, nonatomic) BOOL communicateAvailable;
@property (assign, nonatomic) BOOL contractsAvailable;
@property (assign, nonatomic) BOOL diariesAvailable;
@property (assign, nonatomic) BOOL galleryAvailable;
@property (assign, nonatomic) BOOL menusAvailable;
@property (assign, nonatomic) BOOL medicalAvailable;
@property (assign, nonatomic) BOOL nextstepsAvailable;
@property (assign, nonatomic) BOOL observationsAvailable;
@property (assign, nonatomic) BOOL permissionsAvailable;
@property (assign, nonatomic) BOOL progressAvailable;
@property (assign, nonatomic) BOOL questionsAvailable;
@property (assign, nonatomic) BOOL registerAvailable;
@property (assign, nonatomic) BOOL policiesAvailable;
@property (assign, nonatomic) BOOL nappiesAvailable;
@property (assign, nonatomic) BOOL riskAssessmentsAvailable;
@property (assign, nonatomic) BOOL statusAvailable;
@property (assign, nonatomic) BOOL pushSettingsAvailable;


- (id)initWithLocalUserInfo:(NSDictionary *)userInfo andDelegate:(id <UserDelegate>)delegate;
- (void)fillUserOptionsWithDict:(NSDictionary *)userInfo;
- (NSDictionary *)userInfoForLocalSave;


- (BOOL)isCanEnterPin;

- (void)recieveAndFillPinAvailability;
- (void)updateWithRemoteUserInfo:(NSDictionary *)userInfo;

- (void)manualSetHavePin:(BOOL)havePin;
- (void)pinFailedAttempt;

- (NSString *)debugDescription;

//j3 access level
- (void)updateAccessLevelWithDict:(NSDictionary *)dict;
- (BOOL)statusFor:(UserAccessType)accessType;


@end


@protocol UserDelegate
@required
- (void)userHasBeenRecivePinAvailability;
- (void)saveLocalUsers;


@end
