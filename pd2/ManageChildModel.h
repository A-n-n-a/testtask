//
//  ManageChildModel.h
//  pd2
//
//  Created by Eugene Fozekosh on 02.07.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "BaseModel.h"

@interface ManageChildModel : BaseModel

@property (strong, nonatomic) NSString *childFirstName;
@property (strong, nonatomic) NSString *childLastName;
@property (strong, nonatomic) NSString *childImage;
@property (strong, nonatomic) NSString *childRoomID;
@property (strong, nonatomic) NSString *childID;
@property (strong, nonatomic) NSString *childRoomName;
@property (strong, nonatomic) NSString *childBirthday;

@property (assign, nonatomic) bool childArchived;

- (void)updateFromDictionary:(NSDictionary*)dict;
- (BOOL)nameContain:(NSString *)containString;

@end
