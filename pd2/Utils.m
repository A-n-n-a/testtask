//
//  Utils.m
//  pd2
//
//  Created by User on 27.11.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "Utils.h"
#import "AFHTTPRequestOperationManager.h"
#import "ImageCache.h"
#import "MBProgressHUD.h"
#import "UIImage+ProportionalFill.h"

#import "UIImage+Resize.h"

#import "NSString+RemoveEmoji.h"

#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
#import <ImageIO/CGImageSource.h>
#import <ImageIO/CGImageProperties.h>
#import <ImageIO/ImageIO.h>

#import <LocalAuthentication/LocalAuthentication.h>


id firstNotNil(id obj1, id obj2)
{
    if (obj1!=nil)
    {
        return obj1;
    }
    return obj2;
}


@implementation UIImage (normalize)

- (UIImage *)normalizedImage {
    if (self.imageOrientation == UIImageOrientationUp) return self;
    
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    [self drawInRect:(CGRect){0, 0, self.size}];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return normalizedImage;
}

@end

@implementation Utils

#pragma mark - Tools

- (void)getInfoAboutYoutubeVideoWithId:(NSString *)videoId completion:(void (^)(id response, NSError *error))completion
{
    NSLog(@"getInfoAboutYoutubeVideoWithId:%@",videoId);
    NSString *requestString=[NSString stringWithFormat:@"http://gdata.youtube.com/feeds/api/videos/%@?v=2&alt=jsonc",videoId];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"GET"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         
         NSLog(@"getInfoAboutYoutubeVideoWithId success!:%@",responseObject);
         if (completion)
         {
             completion(responseObject,nil);
             NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
             NSLog(@"request string:%@",operation.request.URL.absoluteString);
             NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];
         
         NSLog(@"getInfoAboutYoutubeVideoWithId fail:%@",error);
         if (completion)
         {
             completion(nil,error);
             NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
             NSLog(@"request string:%@",operation.request.URL.absoluteString);
             NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];
    
}


- (void)downloadImageAtPath:(NSString*)path completionHandler:(imageCacheDownloadCompletionHandler)completion
{

    [self downloadImageAtPath:path cache:YES completionHandler:completion];
    
}


- (void)downloadImageAtPath:(NSString*)path cache:(BOOL)cache completionHandler:(imageCacheDownloadCompletionHandler)completion
{
    
    NSLog(@"downloadImageAtPAth:%@",path);
    UIImage *photoImage;
    if (cache) {
        photoImage=[[ImageCache sharedInstance]imageFromLink:path];
    }
    
    if (photoImage)
    {
        if (completion)
        {
            completion(photoImage);
        }
    }
    else
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),
                       ^{
                           NSData* data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:path]];
                           UIImage *photoImage = [UIImage imageWithData:data];
                           dispatch_async(dispatch_get_main_queue(), ^{
                               if (photoImage)
                               {
                                   if (cache) {
                                       [[ImageCache sharedInstance]saveImage:photoImage fromLink:path];
                                   }
                                   
                                   if (completion)
                                   {
                                       completion(photoImage);
                                   }
                               }
                           });
                       });
    }
    
}


- (void)downloadImageAtRelativePath:(NSString*)path cache:(BOOL)cache completionHandler:(imageCacheDownloadCompletionHandler)completion
{
    
    
    if ([path isKindOfClass:[NSNull class]]) {
        return;
    }
    
    NSString *relativePath=[path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if ([relativePath isEqualToString:@"images/sted/rooms/0"])
    {
        relativePath=@"images/sted/rooms/default.jpg";
    }
    if ([relativePath length] > 0 && [[relativePath substringToIndex:1]isEqualToString:@"/"])
    {
        relativePath=[relativePath substringFromIndex:1];
    }
    
    NSString *imagePath=[NSString stringWithFormat:@"%@/%@",[NetworkManager sharedInstance].baseUrl,relativePath];
    [[Utils sharedInstance]downloadImageAtPath:imagePath cache:cache completionHandler:completion];
}


- (void)downloadImageAtRelativePath:(NSString*)path completionHandler:(imageCacheDownloadCompletionHandler)completion
{
    [self downloadImageAtRelativePath:path cache:YES completionHandler:completion];
    
}


-(NSString*)parametersStringFromDictionary:(NSDictionary *)optionsdict
{
    //TODO: обработка массивов?
    //Если в objectForKey:@"key" находится NSArray/NSMutableArray можно добавить формирование строки key[0]=array[0],key[1]=array[1], etc
    
    NSString*str=[[NSString alloc]init];
    NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithDictionary:optionsdict];
    [dict setValue:[NetworkManager sharedInstance].token forKey:@"access_token"];
    for (NSString *key in dict.allKeys)
    {
        NSString *value=[NSString stringWithFormat:@"%@",dict[key]];
        str =[str stringByAppendingString:[NSString stringWithFormat:@"%@=%@&",key,[self addPercentEncoding:value]]];
    }
    str=[str substringToIndex:str.length-1];
    return str;
}

- (NSString *)base64StringFromImage:(UIImage *)image
{
    NSMutableData *data = [NSMutableData dataWithData:UIImagePNGRepresentation(image)];
    int remain = (int)data.length%3;
    //NSLog(@"oldremain:%d",remain);
    if (remain==1)
    {
        [data appendData:[NSData dataWithBytes:"0" length:1]];
    }
    remain=(int)data.length%3;
    //NSLog(@"newremain:%d",remain);
    NSString *str=[data base64EncodedStringWithOptions:0];
    
    //NSLog(@"str64:%@",str);
    str = [NSString stringWithFormat:@"data:image/png;base64,%@",str];
    //str = [self addPercentEncoding:str];
    return str;
}

- (NSString *)base64StringFromJPEGImage:(UIImage *)image withQuality:(CGFloat)quality
{
    NSMutableData *data = [NSMutableData dataWithData:UIImageJPEGRepresentation(image, quality)];
    int remain = (int)data.length%3;
    //NSLog(@"oldremain:%d",remain);
    if (remain==1)
    {
        [data appendData:[NSData dataWithBytes:"0" length:1]];
    }
    remain=(int)data.length%3;
    //NSLog(@"newremain:%d",remain);
    NSString *str=[data base64EncodedStringWithOptions:0];

    
    NSLog(@"str64:%@",str);
    str = [NSString stringWithFormat:@"data:image/jpeg;base64,%@",str];
    //str = [self addPercentEncoding:str];
    return str;
}

-(NSString *)addPercentEncoding:(NSString*)str
{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)str,NULL,CFSTR("!*'();:@&=+$,/?%#[]\" "),kCFStringEncodingUTF8));
}

#pragma mark - Resizing

-(UIImage*)resizeImage:(UIImage *)image toSize:(CGSize)size
{
    image=[image normalizedImage];
    
    NSLog(@"original image:%@",image);
    
    if (image.size.width<=size.width&&image.size.height<=size.height)
    {
        NSLog(@"image does not need to resize:%d %d;",(int)image.size.width,(int)image.size.height);
        return image;
    }
    
    float height = size.height, width = size.width;
    float ratio=image.size.width/image.size.height;
    NSLog(@"ratio=%f",ratio);
    if (ratio>width/height)
    {
        height=(int)(width/ratio);
    }
    else
    {
        width=(int)(height*ratio);
    }
    NSLog(@"width:%f, height:%f",width,height);
    CGRect newRect = CGRectIntegral(CGRectMake(0,0,width,height));
    
    /*
     UIGraphicsBeginImageContext( newRect.size );
     [image drawInRect:newRect];
     UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     */
    
    
    CGImageRef imageRef = [image CGImage];
    size_t bytesPerRow = CGImageGetBitsPerPixel(imageRef) / CGImageGetBitsPerComponent(imageRef) * newRect.size.width;
    NSLog(@"old bytes per row:%zu",bytesPerRow);
    bytesPerRow = (bytesPerRow + 15) & ~15;  // Make it 16-byte aligned
    NSLog(@"new bytes per row:%zu",bytesPerRow);
    CGContextRef bitmap = CGBitmapContextCreate(NULL,
                                                newRect.size.width,
                                                newRect.size.height,
                                                CGImageGetBitsPerComponent(imageRef),
                                                bytesPerRow,
                                                CGImageGetColorSpace(imageRef),
                                                CGImageGetBitmapInfo(imageRef));
    CGContextSetInterpolationQuality(bitmap, kCGInterpolationHigh);
    
    CGContextDrawImage(bitmap, newRect, imageRef);
    
    CGImageRef resizedImageRef = CGBitmapContextCreateImage(bitmap);
    UIImage *resizedImage = [UIImage imageWithCGImage:resizedImageRef];
    
    CGContextRelease(bitmap);
    CGImageRelease(resizedImageRef);
    
    NSLog(@"resized image:%@",resizedImage);
    return resizedImage;
}


- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIImage *sourceImage = image;
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = newSize.width;
    CGFloat targetHeight = newSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, newSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
        {
            scaleFactor = widthFactor; // scale to fit height
        }
        else
        {
            scaleFactor = heightFactor; // scale to fit width
        }
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
        {
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
        }
    }
    
    UIGraphicsBeginImageContext(newSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(newImage == nil)
    {
        NSLog(@"could not scale image");
    }
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (UIImage *)imageScaledToAvatar:(UIImage*)image
{
    int maxwidth=200,maxheight=200;
    if (image.size.width<=maxwidth&&image.size.height<=maxheight)
    {
        NSLog(@"image does not need to resize toDiaryPhotoThumb:%d %d;",(int)image.size.width,(int)image.size.height);
        return image;
    }
    NSLog(@"original image:%@",image);

//    UIImage * resized=[self resizeImage:image toSize:CGSizeMake(60, 80)];
    UIImage * resized = [self imageWithImage:image scaledToSize:CGSizeMake(60, 80)];

    NSLog(@"resized image:%@",resized);
    
    return resized;
}

- (UIImage *)imageScaledToAvatarHiRes:(UIImage*)image
{
    int maxwidth=300,maxheight=400;
    if (image.size.width<=maxwidth&&image.size.height<=maxheight)
    {
        NSLog(@"image does not need to resize toDiaryPhotoThumb:%d %d;",(int)image.size.width,(int)image.size.height);
        return image;
    }
    NSLog(@"original image:%@",image);
    
    //    UIImage * resized=[self resizeImage:image toSize:CGSizeMake(60, 80)];
    UIImage * resized = [self imageWithImage:image scaledToSize:CGSizeMake(300, 400)];
    
    NSLog(@"resized image:%@",resized);
    
    return resized;
}




- (UIImage *)imageScaledToRoomImage:(UIImage*)image
{
    CGSize newSize = CGSizeMake(150, 100);
   
    UIImage * resized = [self imageWithImage:image scaledToSize:newSize];
    
    return resized;
}


- (UIImage *)imageScaledToRoomImageHiRes:(UIImage*)image
{
    CGSize newSize = CGSizeMake(300, 200);
    
    UIImage * resized = [self imageWithImage:image scaledToSize:newSize];
    
    return resized;
}

- (UIImage *)imageScaledToMedicineImage:(UIImage*)image
{
    CGSize newSize = CGSizeMake(100, 100);
    UIImage * resized = [self imageWithImage:image scaledToSize:newSize];
    
    return resized;
}


- (UIImage *)imageScaledToRegularVisitorImage:(UIImage*)image
{
    int maxwidth=200,maxheight=300;
    if (image.size.width<=maxwidth&&image.size.height<=maxheight)
    {
        NSLog(@"image does not need to resize ToRoomImage:%d %d;",(int)image.size.width,(int)image.size.height);
        return image;
    }
    
    NSLog(@"original image:%@",image);

    UIImage * resized = [self imageWithImage:image scaledToSize:CGSizeMake(200, 300)];
    NSLog(@"resized image:%@",resized);
    
    return resized;
}


- (UIImage *)imageScaledToDiaryPhotoThumb:(UIImage *)image
{
    int maxwidth=120,maxheight=120;
    if (image.size.width<=maxwidth&&image.size.height<=maxheight)
    {
        NSLog(@"image does not need to resize toDiaryPhotoThumb:%d %d;",(int)image.size.width,(int)image.size.height);
        return image;
    }
    NSLog(@"original image:%@",image);
    
    //UIImage *resized=[image imageScaledToFitSize:CGSizeMake(maxwidth, maxheight)];
    UIImage *resized=[self resizeImage:image toSize:CGSizeMake(maxwidth, maxheight)];
    NSLog(@"resized image:%@",resized);
    
    return resized;
}

- (UIImage *)imageScaledToDiaryPhoto:(UIImage *)image
{
    int maxwidth=1000,maxheight=800;
    if (image.size.width<=maxwidth&&image.size.height<=maxheight)
    {
        NSLog(@"image does not need to resize toDiaryPhoto:%d %d;",(int)image.size.width,(int)image.size.height);
        return image;
    }
    NSLog(@"original image:%@",image);
    
    //UIImage *resized=[image imageScaledToFitSize:CGSizeMake(maxwidth, maxheight)];
    UIImage *resized=[self resizeImage:image toSize:CGSizeMake(maxwidth, maxheight)];
    NSLog(@"resized image:%@",resized);
    
    return resized;
}


- (UIImage *)imageScaledToGalleryPhoto:(UIImage *)image
{
    int maxwidth=1000,maxheight=800;
    NSLog(@"image w:%f, h:%f", image.size.width, image.size.height);
    
    if (image.size.width<=maxwidth&&image.size.height<=maxheight)
    {
        NSLog(@"image does not need to resize toGalleryPhoto:%d %d;",(int)image.size.width,(int)image.size.height);
        return image;
    }
    NSLog(@"original image:%@",image);
    
    //UIImage *resized=[image imageScaledToFitSize:CGSizeMake(maxwidth, maxheight)];
    UIImage *resized=[image resizedImageToSize:CGSizeMake(maxwidth, maxheight)];
    NSLog(@"resized image:%@",resized);
    
    return resized;
}


- (UIImage *)imageScaledToGalleryPhotoThumb:(UIImage *)image
{
    int maxwidth=120,maxheight=120;
    if (image.size.width<=maxwidth&&image.size.height<=maxheight)
    {
        NSLog(@"image does not need to resize toGalleryPhotoThumb:%d %d;",(int)image.size.width,(int)image.size.height);
        return image;
    }
    NSLog(@"original image:%@",image);
    
    //UIImage *resized=[image imageScaledToFitSize:CGSizeMake(maxwidth, maxheight)];
    UIImage *resized=[image resizedImageToSize:CGSizeMake(maxwidth, maxheight)];
    NSLog(@"resized image:%@",resized);
    
    return resized;
}


-(NSDateFormatter*)standartDateFormatter
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"yyyy-MM-dd";
    return formatter;
}

-(NSDateFormatter*)dotsDateFormatter
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"dd.MM.yyyy";
    return formatter;
}

-(void)getExifDataFromImage:(NSDictionary*) info completion:(void (^)(NSDictionary *exifInfo))completion;
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:[info objectForKey:UIImagePickerControllerReferenceURL]
             resultBlock:^(ALAsset *asset) {
                 
                 ALAssetRepresentation *image_representation = [asset defaultRepresentation];
                 
                 // create a buffer to hold image data
                 uint8_t *buffer = (Byte*)malloc(image_representation.size);
                 NSUInteger length = [image_representation getBytes:buffer fromOffset: 0.0  length:image_representation.size error:nil];
                 
                 if (length != 0)  {
                     
                     // buffer -> NSData object; free buffer afterwards
                     NSData *adata = [[NSData alloc] initWithBytesNoCopy:buffer length:(int)image_representation.size freeWhenDone:YES];
                     
                     // identify image type (jpeg, png, RAW file, ...) using UTI hint
                     NSDictionary* sourceOptionsDict = [NSDictionary dictionaryWithObjectsAndKeys:(id)[image_representation UTI] ,kCGImageSourceTypeIdentifierHint,nil];
                     
                     // create CGImageSource with NSData
                     CGImageSourceRef sourceRef = CGImageSourceCreateWithData((__bridge CFDataRef) adata,  (__bridge CFDictionaryRef) sourceOptionsDict);
                     
                     // get imagePropertiesDictionary
                     CFDictionaryRef imagePropertiesDictionary;
                     imagePropertiesDictionary = CGImageSourceCopyPropertiesAtIndex(sourceRef,0, NULL);
                     
                     // get exif data
                     CFDictionaryRef exif = (CFDictionaryRef)CFDictionaryGetValue(imagePropertiesDictionary, kCGImagePropertyExifDictionary);
                     NSDictionary *exif_dict = (__bridge NSDictionary*)exif;
                     NSLog(@"exif_dict: %@",exif_dict);
                     
                     if (completion)
                     {
                         completion(exif_dict);
                     }
                     
                     CFRelease(imagePropertiesDictionary);
                     CFRelease(sourceRef);
                 }
                 else {
                     NSLog(@"image_representation buffer length == 0");
                 }
             }
            failureBlock:^(NSError *error) {
                NSLog(@"couldn't get asset: %@", error);
            }
     ];
}

-(NSString *) stringByStrippingHTML:(NSString*)s
{
    NSRange r;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

-(NSDateComponents*)getComponentsFromDate:(NSDate*)date
{
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:~ NSTimeZoneCalendarUnit fromDate:date];
    return dateComponents;
    
}

-(void)showShortAlertWithText:(NSString*)text
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[AppDelegate delegate].window.subviews[0] animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = text;
    hud.userInteractionEnabled=NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [hud hide:YES];
    });
}

-(NSString *)handleNilString:(NSString*)string
{
    if (!string) {
        return @"";
    }
    if (![string isKindOfClass:[NSString class]])
    {
        return @"";
    }
    return string;
}

+(NSString*)ordinalNumberFormat:(NSInteger)num
{
    NSString *ending;
    
    int ones = num % 10;
    int tens = floor(num / 10);
    tens = tens % 10;
    if(tens == 1){
        ending = @"th";
    }else {
        switch (ones) {
            case 1:
                ending = @"st";
                break;
            case 2:
                ending = @"nd";
                break;
            case 3:
                ending = @"rd";
                break;
            default:
                ending = @"th";
                break;
        }
    }
    return [NSString stringWithFormat:@"%ld%@", (long)num, ending];
}


+ (id)safeRemoveEmoji:(id)object {
    
    if (object && [object isKindOfClass:[NSString class]]) {
        return [(NSString *)object stringByRemovingEmoji];
    }
    else {
        return object;
    }
    
    
    
    
}

#pragma mark - FaceId check

+ (BOOL)isFaceIdSupported {
    if (@available(iOS 11.0, *)) {
        LAContext *context = [[LAContext alloc] init];
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil]){
            return ( context.biometryType == LABiometryTypeFaceID);
        }
    }
    return NO;
}


@end
