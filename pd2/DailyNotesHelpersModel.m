//
//  DailyNotesHelpersModel.m
//  pd2
//
//  Created by Andrey on 08/03/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesHelpersModel.h"
#import "DailyNotesHelperModel.h"

@interface DailyNotesHelpersModel ()



@end

@implementation DailyNotesHelpersModel

/*
(
    {
        helpertitle = 1br;
        id = 23;
        idname = breakfast;
        ordering = 1;
        published = 0;
        title = 1;
    },
    {
        helpertitle = 2ms;
        id = 24;
        idname = "m_snack";
        ordering = 0;
        published = 0;
        title = 2;
    }
)
*/


- (instancetype)initWithDict:(NSDictionary *)dict {
    
    self = [super initWithDict:dict];
    if (self) {
        
        if (!dict || ![dict isKindOfClass:[NSDictionary class]]) {
            return nil;
        }
        
        self.breakfast = [self getHelpersWithDict:dict[@"breakfast"]];
        self.morningSnack = [self getHelpersWithDict:dict[@"m_snack"]];
        self.lunch = [self getHelpersWithDict:dict[@"lunch"]];
        self.afternoonSnack = [self getHelpersWithDict:dict[@"a_snack"]];
    
        
        //Fill arrays
        self.helpers = [NSMutableArray array];
        self.helpersTitle = [NSMutableArray array];
        
        if ([self.breakfast count]) {
            [self.helpersTitle addObject:@"Breakfast"];
            [self.helpers addObject:self.breakfast];
        }
        if ([self.morningSnack count]) {
            [self.helpersTitle addObject:@"Morning Snack"];
            [self.helpers addObject:self.morningSnack];
        }
        if ([self.lunch count]) {
            [self.helpersTitle addObject:@"Lunch"];
            [self.helpers addObject:self.lunch];
        }
        if ([self.afternoonSnack count]) {
            [self.helpersTitle addObject:@"Afternoon Snack"];
            [self.helpers addObject:self.afternoonSnack];
        }
     
    }
    return self;
    
}



/*
{
    "id": "breakfast",
    "title": "Breakfast",
    "options": [
                {
                    "title": "Fruit",
                    "options": [
                                "Apple",
                                "Banana",
                                "Blackberries",
                                "Blueberries",
                                "Cherries",
                                "Cranberries",
                                "Currants",
                                "Grapes",
                                "Grapefruit",
                                "Gooseberries",
                                "Mandarine",
                                "Melon",
                                "Orange",
                                "Pear",
                                "Peach",
                                "Pineapple",
                                "Plum",
                                "Raspberries",
                                "Strawberries",
                                "Watermelon"
                                ]
                },
                {
                    "title": "mytesttitle",
                    "options": [
                                "123",
                                "2343",
                                "34453",
                                "2435"
                                ]
                },
                {
                    "title": "Amount",
                    "options": [
                                "Ate none",
                                "Ate half",
                                "Ate all",
                                "Refused"
                                ]
                }
                ]
}
*/


- (NSMutableArray *)getHelpersWithDict:(NSDictionary *)dict {
    
    NSMutableArray *array = [NSMutableArray array];
    if (!dict || ![dict isKindOfClass:[NSDictionary class]]) {
        return array;
    }
    
    NSArray *helpersArray = [self safeArray:dict[@"options"]];
    if (helpersArray) {
        for (NSDictionary *helperDict in helpersArray) {
            DailyNotesHelperModel *model = [[DailyNotesHelperModel alloc] initWithDict:helperDict];
            if (model && [model isExist]) {
                [array addObject:model];
            }
        }
    }
    
    return array;
    
    
    
}


- (BOOL)isExist {
    
    //if (self.firstName && self.lastName && self.chId) {
        return YES;
    //}
    
    return NO;
    
}







@end
