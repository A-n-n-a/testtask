//
//  AppSettingsVC.h
//  pd2
//
//  Created by User on 01.03.17.
//  Copyright (c) 2017 Andrey. All rights reserved.
//

#import "Pd2MenuViewController.h"

@interface AppSettingsVC : Pd2MenuViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@end
