//
//  DailyNotesChildrenNoteCommentsNoteParentSideCell.m
//  pd2
//
//  Created by Andrey on 30.03.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "DailyNotesChildrenNoteCommentsNoteParentSideCell.h"
#import "DailyNotesChildrenNoteParentSideModel.h"

@implementation DailyNotesChildrenNoteCommentsNoteParentSideCell

- (void)updateWithModel:(DailyNotesChildrenNoteParentSideModel *)model {
    
    
    self.titleLabel.text = notAvailStringIfNil([model.noteTitle uppercaseFirstLetter]);
    self.addedOnLabel.text = notAvailStringIfNil([model.date printableDefault]);
    self.addedByLabel.text = [self fullNameFromFirstName:model.adminFirstName andLastName:model.adminLastName];
    self.descrLabel.text = notAvailStringIfNil(model.note);
    
}


@end
