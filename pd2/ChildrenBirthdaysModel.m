//
//  ChildrenBirthdaysModel.m
//  pd2
//
//  Created by Eugene Fozekosh on 04.08.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ChildrenBirthdaysModel.h"

@implementation ChildrenBirthdaysModel


- (void)updateFromDictionary:(NSDictionary*)dict
{
    self.childBirthday = [self safeStringFrom:[dict objectForKey:@"day_of_birth"]];
    self.childFirstName = [self safeStringFrom:[dict objectForKey:@"first_name"]];
    self.childLastName = [self safeStringFrom:[dict objectForKey:@"last_name"]];
    self.childImage = [self safeStringFrom:[dict objectForKey:@"photograph"]];
    self.childID = [self safeStringFrom:[dict objectForKey:@"child_id"]];
    self.childRoomID = [self safeStringFrom:[dict objectForKey:@"room_id"]];
    self.childRoomName = [self safeStringFrom:[dict objectForKey:@"room_title"]];
    self.childNextAge = [self safeStringFrom:[dict objectForKey:@"age_next_birthday"]];

    self.childBirthdayDate = [self dateFromString:self.childBirthday];

}

@end
