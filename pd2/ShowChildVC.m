//
//  ShowChildVC.m
//  pd2
//
//  Created by Eugene Fozekosh on 07.09.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ShowChildVC.h"
#import "AXSections.h"
#import "InfoCell.h"
#import "AXHeader.h"
#import "BaseModel.h"

#define CONTAINER_VIEW_TAG 250
#define SPOILER_IMAGE_VIEW_TAG 100
#define HEADER_ARROW_IMAGE_VIEW_TAG 150


@interface ShowChildVC ()
{
    NSString *PDTopSection;
    NSString *PDMainInfoSection;
    NSString *PDParentInfoSection;
    NSString *PDEmergencyInfoSection;
    NSString *PDDoctorsInfoSection;

    NSString *PDInfoCell;
    NSString *PDInfo2Cell;
    NSString *PDInfo3Cell;
    NSString *PDInfo4Cell;
    NSString *PDInfo5Cell;
    NSString *PDInfo6Cell;
    NSString *PDButtonsCell;

    
    NSString *PDSpoilerCell;
    NSString *PDSpoiler2Cell;
    NSString *PDSpoiler3Cell;
}

@property (strong, nonatomic) AXSections *sections;
@property (strong, nonatomic) InfoCell *autolayoutInfoCell;

@end


@implementation ShowChildVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTopTitle:@"View Child"];
//    [self getProfileSections];
    [self initializeCellTypes];
    [self configureSections];
    [self setupSections];
}

- (void)initializeCellTypes
{
    PDInfoCell = @"PDInfoCell";
    PDInfo2Cell = @"PDInfo2Cell";
    PDInfo3Cell = @"PDInfo3Cell";
    PDInfo4Cell = @"PDInfo4Cell";
    PDInfo5Cell = @"PDInfo5Cell";
    PDInfo6Cell = @"PDInfo6Cell";

    
    PDSpoilerCell = @"PDSpoilerCell";
    PDSpoiler2Cell = @"PDSpoiler2Cell";
    PDSpoiler3Cell = @"PDSpoiler3Cell";
  
    PDMainInfoSection = @"PDMainInfoSection";
    PDTopSection = @"PDTopSection";
    PDParentInfoSection = @"PDParentInfoSection";
    PDEmergencyInfoSection = @"PDEmergencyInfoSection";
    PDDoctorsInfoSection = @"PDDoctorsInfoSection";
}


- (void)configureSections
{
    self.sections = [[AXSections alloc] init];
    [self.sections rememberType:PDInfoCell withHeight:100.f identifier:@"infoCell"];
    [self.sections rememberType:PDInfo2Cell withHeight:200.f identifier:@"info2Cell"];
    [self.sections rememberType:PDInfo3Cell withHeight:200.f identifier:@"info3Cell"];
    [self.sections rememberType:PDInfo4Cell withHeight:210.f identifier:@"info4Cell"];
    [self.sections rememberType:PDInfo5Cell withHeight:220.f identifier:@"info5Cell"];
    [self.sections rememberType:PDInfo6Cell withHeight:260.f identifier:[NetworkManager sharedInstance].isJ3 ? @"info6Cell" : @"info6J15Cell"];

    [self.sections rememberType:PDSpoilerCell withHeight:57.f identifier:@"spoilerCell"];
    [self.sections rememberType:PDSpoiler2Cell withHeight:44.f identifier:@"spoiler2Cell"];
    [self.sections rememberType:PDSpoiler3Cell withHeight:46.f identifier:@"spoiler3Cell"];
}


-(void) setupSections
{
    
    //Top Section
    AXSection *topSection = [[AXSection alloc] initWithType:PDTopSection];
    [topSection addRow:[self.sections genRowWithType:PDInfoCell]];
    [self.sections addSection:topSection];

    //Child info Section
    AXSection *mainInfoSection = [[AXSection alloc] initWithType:PDMainInfoSection];
    [mainInfoSection addHeader:[self.sections genHeaderWithType:PDSpoilerCell]];
    [mainInfoSection addRow:[self.sections genRowWithType:PDInfo2Cell]];
    [self.sections addSection:mainInfoSection];
    
    
    NSArray *parentsArray = self.childDict[@"parents"];
    
    
    
    //Parents Section
    AXSection *parentInfoSection = [[AXSection alloc] initWithType:PDParentInfoSection];
    [parentInfoSection addHeader:[self.sections genHeaderWithType:PDSpoiler2Cell]];
    AXRow *infoRow3 = [self.sections genRowWithType:PDInfo3Cell];
    AXRow *infoRow4 = [self.sections genRowWithType:PDInfo4Cell];

    if (parentsArray.count>1) {
        infoRow3.object = parentsArray.firstObject;
        infoRow4.object = parentsArray.lastObject;
        [parentInfoSection addRow:infoRow3];
        [parentInfoSection addRow:infoRow4];
    }else{
        infoRow3.object = parentsArray.firstObject;
        [parentInfoSection addRow:infoRow3];
    }
    
    [self.sections addSection:parentInfoSection];
    
    //Emergency
    AXSection *emergencySection = [[AXSection alloc] initWithType:PDEmergencyInfoSection];
    [emergencySection addHeader:[self.sections genHeaderWithType:PDSpoiler3Cell]];
    [emergencySection addRow:[self.sections genRowWithType:PDInfo5Cell]];
    [emergencySection addRow:[self.sections genRowWithType:PDInfo6Cell]];

    [self.sections addSection:emergencySection];

    

    [self.tableView reloadData];
}


-(void) getProfileSections
{
    self.profileSections = [[NSMutableArray alloc]initWithArray:[DataManager sharedInstance].profileSections];
}

#pragma mark - TableView Header

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [self.sections heightForHeaderInSection:section];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    AXSection *sect = [self.sections sectionAtIndex:section];
    AXHeader *header = [self.sections headerAtSectionIndex:section];
    
    if (header) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:header.identifier];
        UIView *view = cell.contentView;
        //стрелка
        UIImageView *arrowImageView = (UIImageView *)[view viewWithTag:HEADER_ARROW_IMAGE_VIEW_TAG];
        [self updateHeaderArrowImage:arrowImageView withState:sect.isHide];
        
        UILabel *titleLabel = (UILabel *)[view viewWithTag:200];
        titleLabel.text = [NSString stringWithFormat:@"%@'s Personal Info", self.childDict[@"child"][@"first_name"] ? [self.childDict[@"child"][@"first_name"] capitalizedString] : @"N/A" ];
        //бордеры
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        [self addBorder:PDBorderBottom toView:containerView forToolbar:NO];
        
        //Обязательное удаление gestureRecognizers, иначе при longPress crash
        while (cell.contentView.gestureRecognizers.count) {
            [cell.contentView removeGestureRecognizer:[cell.contentView.gestureRecognizers objectAtIndex:0]];
        }
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headerTap:)];
        [view addGestureRecognizer:tap];
        
        view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        header.view = view;
    }
    return header.view;
}


#pragma mark - Handle Header Tap

- (void)headerTap:(UIGestureRecognizer *)gestureRecognizer
{
    UIView *view = gestureRecognizer.view;
    NSUInteger index = [self.sections getSectionIndexForView:view];
    if (index == NSUIntegerMax) {
        return;
    }
    NSIndexSet *reloadIndexSet = [NSIndexSet indexSetWithIndex:index];
    
    AXHeader *header = [self.sections headerAtSectionIndex:index];
    AXSection *section = [self.sections sectionAtIndex:index];
    section.isHide = !section.isHide;
    [self updateHeaderArrowImage:header.arrowImageView withState:section.isHide];
    
    [self.tableView beginUpdates];
    [self.tableView reloadSections:reloadIndexSet withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}


- (void)updateHeaderArrowImage:(UIImageView *)imageView withState:(BOOL)state
{
    imageView.image = [[UIImage imageNamed:(state ? @"arrowDown" : @"arrowUp")] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}


#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sections countOfSections];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sections countOfRowsInSection:section];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    if([row.type isEqualToString:PDInfoCell]) {
        self.autolayoutInfoCell = [self configureInfoCellForTableView:tableView indexPath:indexPath];
        CGFloat height = self.autolayoutInfoCell.textViewHeight;
        [self.autolayoutInfoCell setHeight:height];
        return height+5;
    }else if ([row.type isEqualToString:PDInfo2Cell]){
        self.autolayoutInfoCell = [self configureInfo2CellForTableView:tableView indexPath:indexPath];
        CGFloat height = self.autolayoutInfoCell.height;
        [self.autolayoutInfoCell setHeight:height];
        return height;
    }else if ([row.type isEqualToString:PDInfo3Cell] || [row.type isEqualToString:PDInfo4Cell] || [row.type isEqualToString:PDInfo5Cell] || [row.type isEqualToString:PDInfo6Cell]){
        self.autolayoutInfoCell = [self configureInfo3CellForTableView:tableView indexPath:indexPath row:row];
        CGFloat height = self.autolayoutInfoCell.height;
        [self.autolayoutInfoCell setHeight:height];
        return height;
    }else{
        return row.height;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    NSString *rowType = row.type;
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    if ([rowType isEqualToString:PDInfoCell]) {
        cell = [self configureInfoCellForTableView:tableView indexPath:indexPath];
        
    } else if ([rowType isEqualToString:PDInfo2Cell]) {
        cell = [self configureInfo2CellForTableView:tableView indexPath:indexPath];
        
    } else if ([rowType isEqualToString:PDInfo3Cell]) {
        cell = [self configureInfo3CellForTableView:tableView indexPath:indexPath row:row];
        
    }else if ([rowType isEqualToString:PDInfo4Cell]) {
        cell = [self configureInfo3CellForTableView:tableView indexPath:indexPath row:row];
        
    }else if ([rowType isEqualToString:PDInfo5Cell]) {
        cell = [self configureInfo3CellForTableView:tableView indexPath:indexPath row:row];
        
    }else if ([rowType isEqualToString:PDInfo6Cell]) {
        cell = [self configureInfo3CellForTableView:tableView indexPath:indexPath row:row];
        
    }else if ([rowType isEqualToString:PDSpoilerCell]) {
        cell = [self configureSpoilerCellForTableView:tableView indexPath:indexPath];
        
    }else if ([rowType isEqualToString:PDSpoiler2Cell]) {
        cell = [self configureSpoiler2CellForTableView:tableView indexPath:indexPath];
        
    }else if ([rowType isEqualToString:PDSpoiler3Cell]) {
        cell = [self configureSpoiler3CellForTableView:tableView indexPath:indexPath];
        
    }
    row.cell = cell;
    
    return cell;
}


- (UITableViewCell *)configureSpoilerCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"spoilerCell"];
    if (cell) {
        UIImageView *spoilerImage = (UIImageView *)[cell.contentView viewWithTag:SPOILER_IMAGE_VIEW_TAG];
        spoilerImage.image = [spoilerImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        [self addBorder:PDBorderTopAndBottom toView:containerView forToolbar:NO];
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    }
    return cell;
}

- (UITableViewCell *)configureSpoiler2CellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"spoiler2Cell"];
    if (cell) {
        UIImageView *spoilerImage = (UIImageView *)[cell.contentView viewWithTag:SPOILER_IMAGE_VIEW_TAG];
        spoilerImage.image = [spoilerImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        [self addBorder:PDBorderTopAndBottom toView:containerView forToolbar:NO];
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    }
    return cell;
}


- (UITableViewCell *)configureSpoiler3CellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"spoiler3Cell"];
    if (cell) {
        UIImageView *spoilerImage = (UIImageView *)[cell.contentView viewWithTag:SPOILER_IMAGE_VIEW_TAG];
        spoilerImage.image = [spoilerImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        [self addBorder:PDBorderTopAndBottom toView:containerView forToolbar:NO];
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    }
    return cell;
}


- (InfoCell *)configureInfoCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    InfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"infoCell"];
    return [self configureBaseInfoCell:cell indexPath:indexPath];
}



- (InfoCell *)configureInfo2CellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    InfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"info2Cell"];
    return [self configureBaseInfoCell:cell indexPath:indexPath];
}


- (InfoCell *)configureInfo3CellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath row:(AXRow*)row
{
    InfoCell *cell = [tableView dequeueReusableCellWithIdentifier:row.identifier];

    if (cell) {
        UIView *containerView = [cell.contentView viewWithTag:CONTAINER_VIEW_TAG];
        NSString *attrStr = cell.infoLabel.text;
        
        NSString *firstName = [row.object[@"first_name"] length] ? [row.object[@"first_name"] capitalizedString] : @"N/A";
        NSString *lastName = [row.object[@"last_name"] length] ? [row.object[@"last_name"] capitalizedString]: @"N/A";
        NSString *street = [row.object[@"street_1"] length] ? row.object[@"street_1"] : @"-";
        NSString *area = [row.object[@"area"] length] ? row.object[@"area"] : @"-";
        NSString *town = [row.object[@"town"] length] ? row.object[@"town"] : @"-";
        NSString *county = [row.object[@"county"] length] ? row.object[@"county"] : @"-";
        NSString *postcode = [row.object[@"postcode"] length] ? row.object[@"postcode"] : @"-";
        NSString *parentAddress =  [NSString stringWithFormat:@" %@, %@, %@, %@, %@", street, area, town, county, postcode];

        [self cleanAddress:&parentAddress];

        /*NSString *lastCharacter = [parentAddress substringFromIndex: [parentAddress length] - 1];
        if ([lastCharacter isEqualToString:@"-"]) {
            parentAddress = [parentAddress substringToIndex:parentAddress.length-3];
        }
         */
        
        NSString *phone = [row.object[@"phone"] length] ? row.object[@"phone"] : @"N/A";
        NSString *mobile = [row.object[@"mobile"] length] ? row.object[@"mobile"] : @"N/A";
        NSString *email = [row.object[@"email"] length] ? row.object[@"email"] : @"N/A";
        NSString *work = [row.object[@"workplace"] length] ?  row.object[@"workplace"] : @"N/A";
        NSString *workPhone = [row.object[@"work_telephone"] length] ? row.object[@"work_telephone"] : @"N/A";

        if ([row.type isEqualToString:PDInfo3Cell] || [row.type isEqualToString:PDInfo4Cell]) {
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%first_name1%" withString:firstName];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%first_name2%" withString:firstName];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%last_name1%" withString:lastName];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%last_name2%" withString:lastName];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%parent1_address%" withString:parentAddress];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%parent2_address%" withString:parentAddress];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%phone1%" withString:phone];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%phone2%" withString:phone];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%mobile1%" withString:mobile];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%mobile2%" withString:mobile];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%email1%" withString:email];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%email2%" withString:email];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%work1%" withString:work];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%work2%" withString:work];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%work_phone1%" withString:workPhone];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%work_phone2%" withString:workPhone];
            
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"-," withString:@""];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"     -" withString:@"N/A"];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"<null>" withString:@"N/A"];

        }else if ([row.type isEqualToString:PDInfo5Cell]){
            NSString *e_firstName = [self.childDict[@"child"][@"ec_first_name"] length] ? self.childDict[@"child"][@"ec_first_name"] : @"N/A";
            NSString *e_lastName = [self.childDict[@"child"][@"ec_last_name"] length] ? self.childDict[@"child"][@"ec_last_name"] : @"N/A";
            
            NSString *e_house = [self.childDict[@"child"][@"ec_house"] length] ? self.childDict[@"child"][@"ec_house"] : @"-";
            NSString *e_street = [self.childDict[@"child"][@"ec_street_1"] length] ? self.childDict[@"child"][@"ec_street_1"] : @"-";
            NSString *e_area = [self.childDict[@"child"][@"ec_area"] length] ? self.childDict[@"child"][@"ec_area"] : @"-";
            NSString *e_town = [self.childDict[@"child"][@"ec_town"] length] ? self.childDict[@"child"][@"ec_town"] : @"-";
            NSString *e_county = [self.childDict[@"child"][@"ec_county"] length] ? self.childDict[@"child"][@"ec_county"] : @"-";
            NSString *e_postcode = [self.childDict[@"child"][@"ec_postcode"] length] ? self.childDict[@"child"][@"ec_postcode"] : @"-";
            NSString *emergencyAddress =  [NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@",  e_house, e_street, e_area, e_town, e_county, e_postcode];
            
            [self cleanAddress:&emergencyAddress];
            
            /*
            NSString *lastCharacter = [emergencyAddress substringFromIndex: [emergencyAddress length] - 1];
            if ([lastCharacter isEqualToString:@"-"]) {
                emergencyAddress = [emergencyAddress substringToIndex:emergencyAddress.length-3];
            }
            */
                
            NSString *e_phone = [self.childDict[@"child"][@"ec_telephone"] length] ? self.childDict[@"child"][@"ec_telephone"] : @"N/A";
            NSString *e_mobile = [self.childDict[@"child"][@"ec_mobile"] length] ? self.childDict[@"child"][@"ec_mobile"] : @"N/A";
            NSString *e_work = [self.childDict[@"child"][@"ec_workplace"] length] ? self.childDict[@"child"][@"ec_workplace"] : @"N/A";
            NSString *e_workPhone = [self.childDict[@"child"][@"ec_work_telephone"] length] ? self.childDict[@"child"][@"ec_work_telephone"] : @"N/A";
            
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%e_first_name%" withString:e_firstName];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%e_last_name%" withString:e_lastName];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%e_address%" withString:emergencyAddress];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%e_phone%" withString:e_phone];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%e_mobile%" withString:e_mobile];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%e_work%" withString:e_work];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%e_work_phone%" withString:e_workPhone];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"-," withString:@""];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"     -" withString:@"N/A"];

        }else if ([row.type isEqualToString:PDInfo6Cell]){
           
            NSString *e_doctorName = [self.childDict[@"child"][@"ec_doctor_name"] length] ? self.childDict[@"child"][@"ec_doctor_name"] : @"N/A";
            NSString *ch_firstName = [self.childDict[@"child"][@"first_name"] length] ? [self.childDict[@"child"][@"first_name"] capitalizedString] : @"N/A";
            NSString *e_doctor_tel = [self.childDict[@"child"][@"ec_doctor_tel"] length] ? self.childDict[@"child"][@"ec_doctor_tel"] : @"N/A";
           
            NSString *diphteria = [self.childDict[@"child"][@"dip"] isEqualToString:@"on"] ? @"Yes" : @"No";
            NSString *measles = [self.childDict[@"child"][@"mea"] isEqualToString:@"on"] ? @"Yes" : @"No";
            NSString *mumps = [self.childDict[@"child"][@"mum"] isEqualToString:@"on"] ? @"Yes" : @"No";
            NSString *meningitis = [self.childDict[@"child"][@"hib"] isEqualToString:@"on"] ? @"Yes" : @"No";
            NSString *polio = [self.childDict[@"child"][@"pol"] isEqualToString:@"on"] ? @"Yes" : @"No";
            NSString *rubella = [self.childDict[@"child"][@"rub"] isEqualToString:@"on"] ? @"Yes" : @"No";
            NSString *tetanus = [self.childDict[@"child"][@"tet"] isEqualToString:@"on"] ? @"Yes" : @"No";
            NSString *whooping = [self.childDict[@"child"][@"who"] isEqualToString:@"on"] ? @"Yes" : @"No";
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%dip%" withString:diphteria];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%hib%" withString:meningitis];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%mea%" withString:measles];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%mum%" withString:mumps];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%pol%" withString:polio];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%rub%" withString:rubella];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%tet%" withString:tetanus];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%who%" withString:whooping];

            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%d_name%" withString:e_doctorName];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%d_phone%" withString:e_doctor_tel];
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%child_first%" withString:ch_firstName];
        }
        
        cell.infoLabel.text = attrStr;
        
        NSString *firstString;
        NSRange foundRange = [attrStr rangeOfString:@"\n"];
        if (foundRange.location != NSNotFound) {
            foundRange.length = foundRange.location + 1;
            foundRange.location = 0;
            firstString = [[NSString alloc] initWithString:[attrStr substringWithRange:foundRange]];
        }
        cell.infoLabel.text = attrStr;
        cell.infoLabel.attributedText = [self highlightString:firstString inString:attrStr forRow:row];
        cell.infoLabel.textColor = [MyColorTheme sharedInstance].textViewTextColor;

        [self addBorder:PDBorderTopAndBottomDynamic toView:containerView forToolbar:NO];
    }
    return cell;
}



- (InfoCell*)configureBaseInfoCell:(InfoCell*)cell indexPath:(NSIndexPath *)indexPath
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];

    if (cell) {
        UIView *containerView = [cell.contentView viewWithTag:CONTAINER_VIEW_TAG];
        
        if ([NetworkManager sharedInstance].isJ3) {
            cell.infoLabel.text = @"%child_full%'s Personal Information:\n\nAddress: %address%\nBoy or Girl: %gender%\nDate of Birth: %dob%\nSEN: %sen% \nFunded: %funded% \nEnglish as Additional Language (EAL): %eal% \nPupil Premium: %premium% \nStarting Date: %start_date% \nFinished Date: %finish_date%\n";
        }
        else {
            cell.infoLabel.text = @"%child_full%'s Personal Information:\n\nAddress: %address%\nBoy or Girl: %gender%\nDate of Birth: %dob%\nSEN: %sen% \nIn Nappies: %inNappies% \nFunded: %funded% \nEnglish as Additional Language (EAL): %eal% \nPupil Premium: %premium% \nStarting Date: %start_date% \nFinished Date: %finish_date%\n";
            
        }
        
        
        
        NSString *attrStr = [row.type isEqualToString:PDInfoCell] ? cell.infoTextView.text : cell.infoLabel.text;
        NSString *fullName = [NSString stringWithFormat:@"%@ %@", self.childDict[@"child"][@"first_name"] ? [self.childDict[@"child"][@"first_name"] capitalizedString] : @"N/A", self.childDict[@"child"][@"last_name"] ? [self.childDict[@"child"][@"last_name"] capitalizedString] : @"N/A"];
        NSString *age = [self getAgeInMonthFromDateString:self.childDict[@"child"][@"birth"]];

        
        NSString *ch_street = [self.childDict[@"child"][@"ch_street_1"] length] ? self.childDict[@"child"][@"ch_street_1"] : @"-";
        NSString *ch_area = [self.childDict[@"child"][@"ch_area"] length] ? self.childDict[@"child"][@"ch_area"] : @"-";
        NSString *ch_town = [self.childDict[@"child"][@"ch_town"] length] ? self.childDict[@"child"][@"ch_town"] : @"-";
        NSString *ch_county = [self.childDict[@"child"][@"ch_county"] length] ? self.childDict[@"child"][@"ch_county"] : @"-";
        NSString *ch_postcode = [self.childDict[@"child"][@"ch_postcode"] length] ? self.childDict[@"child"][@"ch_postcode"] : @"-";

        NSString *address =  [NSString stringWithFormat:@" %@, %@, %@, %@, %@ \n", ch_street, ch_area, ch_town, ch_county, ch_postcode];
        
        NSString *lastCharacter = [address substringFromIndex: [address length] - 1];
        if ([lastCharacter isEqualToString:@"-"]) {
            address = [address substringToIndex:address.length-3];
        }
        
        
        NSNumber *inNappiesNumber = self.childDict[@"child"][@"in_nappies"];
        NSNumber *fundedNumber = self.childDict[@"child"][@"funded"];
        NSNumber *ealNumber = self.childDict[@"child"][@"english_as_addition"];
        NSNumber *premiumNumber = self.childDict[@"child"][@"premium"];
        
        
        NSString *gender = [self.childDict[@"child"][@"gender"] isEqualToString:@"0"] ? @"Boy" : @"Girl";
        NSString *senco = [self.childDict[@"child"][@"senco"] isEqualToString:@"off"] ? @"No" : @"Yes";
        NSString *inNappies = (inNappiesNumber && [inNappiesNumber intValue] == 1) ? @"Yes" : @"No";
        NSString *funded = (fundedNumber && [fundedNumber intValue] == 1) ? @"Yes" : @"No";
        NSString *eal = (ealNumber && [ealNumber intValue] == 1) ? @"Yes" : @"No";
        NSString *premium = (premiumNumber && [premiumNumber intValue] == 1) ? @"Yes" : @"No";
        
        
        NSString *startingDate = [self.childDict[@"child"][@"starting"] isEqualToString:@"01.01.1970"] || [self.childDict[@"child"][@"starting"] isEqualToString:@"00.00.0000"] ? @"N/A" : self.childDict[@"child"][@"starting"];
        NSString *finishDate = [self.childDict[@"child"][@"finished"] isEqualToString:@"01.01.1970"] || [self.childDict[@"child"][@"finished"] isEqualToString:@"00.00.0000"] ? @"N/A" : self.childDict[@"child"][@"finished"];

        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%child_full%" withString:fullName];
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%child%" withString:self.childDict[@"child"][@"first_name"] ? [self.childDict[@"child"][@"first_name"] capitalizedString] : @"N/A"];
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%dob%" withString:self.childDict[@"child"][@"birth"]];
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%room%" withString:self.childDict[@"child"][@"room_title"]];
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%address%" withString:address];
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%gender%" withString:gender];
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%start_date%" withString:startingDate];
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%finish_date%" withString:finishDate];
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%age%" withString:age];

        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%sen%" withString:senco];
        if (![NetworkManager sharedInstance].isJ3) {
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%inNappies%" withString:inNappies];
        }
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%funded%" withString:funded];
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%eal%" withString:eal];
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"%premium%" withString:premium];
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"-," withString:@""];
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@" ," withString:@""];
        
        attrStr = [attrStr stringByReplacingOccurrencesOfString:@"     - \n" withString:@" N/A"];

        cell.infoTextView.text = attrStr;
        cell.infoLabel.text = attrStr;

        NSString *firstString;
        NSRange foundRange = [attrStr rangeOfString:@"\n"];
        if (foundRange.location != NSNotFound) {
            foundRange.length = foundRange.location + 1;
            foundRange.location = 0;
            firstString = [[NSString alloc] initWithString:[attrStr substringWithRange:foundRange]];
        }

        if ([row.type isEqualToString:PDInfo2Cell]) {
            for (NSDictionary *helper in self.profileSections) {
                NSString *helperTitle = [helper[@"title"] length] >=1 ? helper[@"title"] : @"N/A";
                NSString *helperString = [NSString stringWithFormat:@"\n%@: %@", helper[@"helperTitle"], helperTitle];
                attrStr = [NSString stringWithFormat:@"%@ %@", attrStr, helperString];
            }
            attrStr = [attrStr stringByReplacingOccurrencesOfString:@"<null>" withString:@"N/A"];
            cell.infoLabel.text = attrStr;
            cell.infoLabel.attributedText = [self highlightString:firstString inString:attrStr forRow:row];
           
            NSLog(@"%@", cell.infoLabel.font.fontName);

            cell.infoLabel.textColor = [MyColorTheme sharedInstance].textViewTextColor;
        }
        if([row.type isEqualToString:PDInfoCell]){
            NSString *imagePath=[NSString stringWithFormat:@"%@/%@",[NetworkManager sharedInstance].baseUrl, self.childDict[@"child"][@"image"]];
            
            [[Utils sharedInstance]downloadImageAtPath:imagePath completionHandler:^(UIImage *image) {
                cell.childImageView.image = image;
                cell.childImageView.layer.borderWidth = 0.5;
                cell.childImageView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
                cell.childImageView.layer.cornerRadius = cell.childImageView.frame.size.height/2;
                cell.childImageView.layer.masksToBounds = YES;
            }];
            cell.infoTextView.attributedText = [self highlightString:firstString inString:attrStr forRow:row];
            cell.infoTextView.textColor = [MyColorTheme sharedInstance].textViewTextColor;
            
            UIBezierPath * imgRect = [UIBezierPath bezierPathWithRect:CGRectMake(cell.childImageView.frame.origin.x-5, cell.childImageView.frame.origin.y-5, cell.childImageView.frame.size.width, cell.childImageView.frame.size.height)];
            cell.infoTextView.textContainer.exclusionPaths = @[imgRect];
        }
        [self addBorder:PDBorderTopAndBottomDynamic toView:containerView forToolbar:NO];
    }
    [cell.infoLabel sizeThatFits:cell.infoLabel.bounds.size];
    return cell;
}


- (NSString*)getAgeInMonthFromDateString:(NSString*)dateString
{
    NSString *ageInMonth = [NSString new];
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd.MM.yyyy"];
    NSDate *childDate = [formatter dateFromString:dateString];
    NSDateComponents *componentsAgeMonths = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:childDate toDate:[NSDate date] options:0];
    ageInMonth = [NSString stringWithFormat:@"%d %@", (int)componentsAgeMonths.month, componentsAgeMonths.month==1?@"Month":@"Months"];
    return ageInMonth;
}



- (NSAttributedString *)highlightString:(NSString*)subString inString:(NSString *)mainString forRow:(AXRow*)row
{
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString:[[NSAttributedString alloc] initWithString:mainString]];
    if (!subString) {
        subString = mainString;
    }
    
    if ([row.type isEqualToString:PDInfo2Cell] || [row.type isEqualToString:PDInfo3Cell] || [row.type isEqualToString:PDInfo4Cell] || [row.type isEqualToString:PDInfo5Cell] || [row.type isEqualToString:PDInfo6Cell]) {
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.f];
        NSRange titleRange = [[mainString lowercaseString] rangeOfString:[subString lowercaseString]];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(titleRange.location, subString.length)];
    }
    
    if ([row.type isEqualToString:PDInfoCell]) {
        UIFont *font= [UIFont fontWithName:@"HelveticaNeue-Medium" size:11.f];
        
        NSRange titleRange = [[mainString lowercaseString] rangeOfString:[subString lowercaseString]];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(titleRange.location, subString.length)];
    }
    if ([mainString containsString:@"Immunisations / Vaccinations"]) {
        UIFont *font= [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.f];
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Immunisations / Vaccinations"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 28)];
    }
    UIFont *font= [UIFont fontWithName:@"HelveticaNeue-Medium" size:11.f];

    if ([mainString containsString:@"Date of Birth:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Date of Birth:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 14)];
    }
    if ([mainString containsString:@"Address:"]) {
        NSRange rangeOfDoseString = [mainString rangeOfString:@"Address:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfDoseString.location, 8)];
    }
    if ([mainString containsString:@"Email:"]) {
        NSRange rangeOfDoseString = [mainString rangeOfString:@"Email:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfDoseString.location, 6)];
    }
    if ([mainString containsString:@"Assigned Room:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Assigned Room:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 14)];
    }
    if ([mainString containsString:@"Boy or Girl:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Boy or Girl:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 12)];
    }
    if ([mainString containsString:@"SEN:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"SEN:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 4)];
    }
    if ([mainString containsString:@"In Nappies:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"In Nappies:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 11)];
    }
    if ([mainString containsString:@"Funded:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Funded:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 7)];
    }
    if ([mainString containsString:@"English as Additional Language (EAL):"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"English as Additional Language (EAL):"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 37)];
    }
    if ([mainString containsString:@"Pupil Premium:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Pupil Premium:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 14)];
    }
    if ([mainString containsString:@"Starting Date:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Starting Date:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 14)];
    }
    if ([mainString containsString:@"Finished Date:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Finished Date:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 14)];
    }
    if ([mainString containsString:@"Name:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Name:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 5)];
    }
    if ([mainString containsString:@"Telephone:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Telephone:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 10)];
    }
    if ([mainString containsString:@"Mobile:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Mobile:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 7)];
    }
    if ([mainString containsString:@"Place of Work:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Place of Work:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 14)];
    }
    if ([mainString containsString:@"Work Telephone:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Work Telephone:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 15)];
    }
    if ([mainString containsString:@"Age:"]) {
        NSRange rangeOfLeftBracketString = [mainString rangeOfString:@"Age:"];
        [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, 4)];
    }

    
    if ([row.type isEqualToString:PDInfo2Cell]) {
        for (NSDictionary *helper in self.profileSections) {
            NSString *helperString = [NSString stringWithFormat:@"%@", helper[@"helperTitle"]];
            if (helperString.length) {
                NSRange rangeOfLeftBracketString = [mainString rangeOfString:helperString];
                [text addAttribute:NSFontAttributeName value:font range:NSMakeRange(rangeOfLeftBracketString.location, helperString.length)];
            }
        }
    }

    return text;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// - address clean

- (void)cleanAddress:(NSString **)address {
    
    BOOL lengthIsOk = [*address length] >= 3 ? YES : NO;
    
    do {
        
        if (!lengthIsOk) {
            break;
        }
        
        
        NSString *lastCharacter = [*address substringFromIndex: [*address length] - 1];
        if ([lastCharacter isEqualToString:@"-"]) {
            *address = [*address substringToIndex:[*address length]-3];
            lengthIsOk = [*address length] >= 3 ? YES : NO;
        }
        else {
            break;
        }
    } while (lengthIsOk);
    
    if (![*address length] || [*address isEqualToString:@"-"] || [*address isEqualToString:@" -"]) {
        *address = @"N/A";
    }
    
    *address = [self stringByTrimmingLeadingWhitespace:*address];
    
}


- (NSString *)stringByTrimmingLeadingWhitespace:(NSString *)string {
    NSInteger i = 0;
    
    while ((i < [string length])
           && [[NSCharacterSet whitespaceCharacterSet] characterIsMember:[string characterAtIndex:i]]) {
        i++;
    }
    return [string substringFromIndex:i];
}

@end
