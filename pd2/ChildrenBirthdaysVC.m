//
//  ChildrenBirthdaysVC.m
//  pd2
//
//  Created by Eugene Fozekosh on 25.06.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ChildrenBirthdaysVC.h"
#import "AXSections.h"
#import "AXHeader.h"
#import "UIView+superCell.h"
#import "ThisWeekBirthCell.h"
#import "BirthdayCollectionCell.h"
#import "AdditionalBirthInfoCell.h"
#import "ChildrenBirthdaysModel.h"
#import "UIImageView+AFNetworking.h"

#import "NSDate+Utilities.h"

#import "Utils.h"

#define CONTAINER_VIEW_TAG 250
#define HEADER_ARROW_IMAGE_VIEW_TAG 150

@interface ChildrenBirthdaysVC ()
{
    NSString *PDTopSection;
    NSString *PDThisWeekBirthdaysSection;
    NSString *PDUpcomingBirthdaysSection;
    NSString *PDChildrenSection;
    NSString *PDSpacerSection;
    
    NSString *PDInfoCell;
    NSString *PDInfo2Cell;
    NSString *PDBirthdayCell;
    NSString *PDSpoiler1Cell;
    NSString *PDSpoiler2Cell;
    NSString *PDChildSpoiler3Cell;
    NSString *PDChildBirthInfoCell;
    NSString *PDSpacerCell;
}

@property (strong, nonatomic) AXSections *sections;
@property (strong, nonatomic) NSMutableArray *birthdays;
@property (strong, nonatomic) NSMutableArray *thisWeekBirthdays;
@property (strong, nonatomic) NSMutableDictionary *sortedChildrenDict;

@end

@implementation ChildrenBirthdaysVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.topTitle = @"Children's Birthdays";
    [self initializeCellTypes];
    [self configureSections];
    [self initTopSection];
    [self getChildrenBirthdays];
}


-(void) getChildrenBirthdays
{
    [[NetworkManager sharedInstance] getChildrenBirthdaysWithCompletion:^(id response, NSArray *birthdays, NSError *error) {
        self.birthdays = [[self sortedChildsByMonthDay:birthdays] mutableCopy];
        //self.birthdays = [NSMutableArray arrayWithArray:birthdays];
        
        
        
        self.thisWeekBirthdays = [NSMutableArray new];

        
        [self getNext6BirthdaysFromArray:self.birthdays];
//        [self.birthdays removeObjectsInArray:self.thisWeekBirthdays];

        self.sortedChildrenDict = [NSMutableDictionary new];

        [self initThisWeekBirthdaysSection];
        [self sortBirthdays:self.birthdays byNameInDictionary:self.sortedChildrenDict];
        
        [self setupUpcomingSection];
    }];
}


-(void) sortBirthdays:(NSMutableArray*)birthdays byNameInDictionary:(NSMutableDictionary*)dictionary
{
    NSMutableArray *spoilers = [NSMutableArray new];
    
    for (ChildrenBirthdaysModel *birth in birthdays) {
        NSString *childName = [NSString stringWithFormat:@"%@. %@", birth.childFirstName, birth.childLastName];
        [spoilers addObject:childName];
    }
    
    for (NSString *name in spoilers) {
        NSMutableArray *roomChildren = [NSMutableArray new];
        for (ChildrenBirthdaysModel *birthday in birthdays) {
            NSString *childName = [NSString stringWithFormat:@"%@. %@", birthday.childFirstName, birthday.childLastName];
            if ([childName isEqualToString:name]) {
                [roomChildren addObject:birthday];
                break;
            }
        }
        [dictionary setObject:roomChildren forKey:name];
    }
}


- (NSArray *)sortUpcomingBirthdays:(NSArray *)birthdays {
    
    NSMutableArray *childrenBirths = [NSMutableArray new];
    
    for (ChildrenBirthdaysModel *birth in birthdays) {
        [childrenBirths addObject:[self dateFromString:birth.childBirthday]];
    }
    
    NSDictionary *sorted = [self sortingDates:childrenBirths];
    
    
    NSSortDescriptor *sortByMonth = [NSSortDescriptor sortDescriptorWithKey:@"month" ascending:YES];
    NSSortDescriptor *sortByDay = [NSSortDescriptor sortDescriptorWithKey:@"day" ascending:YES];
    NSArray *sorts = @[sortByMonth, sortByDay];
    
    NSArray *orderedUpcoming = [[sorted objectForKey:@"upcoming"] sortedArrayUsingDescriptors:sorts];
    NSArray *orderedGone = [[sorted objectForKey:@"next"] sortedArrayUsingDescriptors:sorts];
    
    
    NSMutableArray *sortedDates = [NSMutableArray arrayWithArray:orderedUpcoming];
    [sortedDates addObjectsFromArray:orderedGone];
    
    
    /*
    //Сортируем children spoilers по новому порядку дат
    NSMutableArray *sortedChildrens = [NSMutableArray array];
    
    for (NSDate *date in sortedDates) {
        if (!date) {
            return;
        }
        
        for (NSString *key in self.sortedChildrenDict) {
            
            NSArray *childrensWithNameArray = [self.sortedChildrenDict objectForKey:key];
            
            for (ChildrenBirthdaysModel *model in childrensWithNameArray) {
                
                NSDate *childrenBirthday = [self dateFromString:model.childBirthday];
                
                if ([childrenBirthday isEqualToDate:date]) {
                    [sortedChildrens addObject:model];
                }
            }
            
            
            
        }
        
    }
    */
    
    return [NSArray arrayWithArray:sortedDates];
}


- (void)initializeCellTypes
{
    PDTopSection = @"PDTopSection";
    PDSpacerSection = @"PDSpacerSection";
    
    PDInfoCell = @"PDInfoCell";
    PDInfo2Cell = @"PDInfo2Cell";
    PDThisWeekBirthdaysSection = @"PDThisWeekBirthdaysSection";
    PDUpcomingBirthdaysSection = @"PDUpcomingBirthdaysSection";
    PDChildrenSection = @"PDChildrenSection";
    PDChildBirthInfoCell = @"PDChildBirthInfoCell";
    PDBirthdayCell = @"PDBirthdayCell";
    PDSpoiler1Cell = @"PDSpoiler1Cell";
    PDSpoiler2Cell = @"PDSpoiler2Cell";
    PDChildSpoiler3Cell = @"PDChildSpoiler3Cell";
    PDSpacerCell = @"PDSpacerCell";
}


- (void)configureSections
{
    self.sections = [[AXSections alloc] init];
    
    [self.sections rememberType:PDInfoCell withHeight:70.f identifier:@"infoCell"];
    [self.sections rememberType:PDInfo2Cell withHeight:76.f identifier:@"info2Cell"];
    [self.sections rememberType:PDSpoiler1Cell withHeight:44.f identifier:@"spoiler1Cell"];
    [self.sections rememberType:PDSpoiler2Cell withHeight:44.f identifier:@"spoiler2Cell"];
    [self.sections rememberType:PDChildSpoiler3Cell withHeight:44.f identifier:@"spoiler3Cell"];
    [self.sections rememberType:PDChildBirthInfoCell withHeight:110.f identifier:@"additionalInfoCell"];
    [self.sections rememberType:PDBirthdayCell withEstimatedHeight:160.f identifier:@"weekBirthdaysCell"];
    [self.sections rememberType:PDSpacerCell withEstimatedHeight:13.f identifier:@"spacerCell"];
}


-(void) initTopSection
{
    AXSection *topSection = [[AXSection alloc] initWithType:PDTopSection];
    [topSection addRow:[self.sections genRowWithType:PDInfoCell]];
    [self.sections addSection:topSection];
    
    [self.tableView reloadData];
}


-(void) initThisWeekBirthdaysSection
{
    AXSection *thisWeek = [[AXSection alloc] initWithType:PDThisWeekBirthdaysSection];
    AXHeader *header = [self.sections genHeaderWithType:PDSpoiler1Cell];
    [thisWeek addRow:[self.sections genRowWithType:PDInfo2Cell]];
    if (self.thisWeekBirthdays.count) {
        [thisWeek addRow:[self.sections genRowWithType:PDBirthdayCell]];
    }
    [thisWeek addHeader:header];
    thisWeek.isHide = NO;
    [self.sections addSection:thisWeek];
    
    [self.tableView reloadData];
}


-(AXSection *)sectionsForSpacerWithTag:(NSArray *)tags {
    
    AXSection *spacerSection = [[AXSection alloc] initWithType:PDSpacerSection];
    [spacerSection.tags addObjectsFromArray:tags];
    [spacerSection.tags addObject:@"spacer"];
    AXRow *spacerRow = [self.sections genRowWithType:PDSpacerCell];
    [spacerSection addRow:spacerRow];
    
    return spacerSection;
}



#pragma mark - TableView Header

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [self.sections heightForHeaderInSection:section];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    AXSection *sect = [self.sections sectionAtIndex:section];
    AXHeader *header = [self.sections headerAtSectionIndex:section];
    
    if (header) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:header.identifier];
        UIView *view = cell.contentView;
        //стрелка
        UIImageView *arrowImageView = (UIImageView *)[view viewWithTag:HEADER_ARROW_IMAGE_VIEW_TAG];
        [self updateHeaderArrowImage:arrowImageView withState:sect.isHide];
        
        //бордеры
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        [self addBorder:PDBorderBottom toView:containerView forToolbar:NO];
       
        if ([header.type isEqualToString:PDChildSpoiler3Cell]) {
            
            UILabel *titleLabel = (UILabel *)[view viewWithTag:215];
            UILabel *dateLabel = (UILabel *)[view viewWithTag:216];

            NSArray *objectArray = header.object;
            if (objectArray && [objectArray isKindOfClass:[NSArray class]]) {
                ChildrenBirthdaysModel *child = [objectArray firstObject];
                dateLabel.text = [child.childBirthdayDate printableDefault];
            }
                        
            //dateLabel.text = [[header.object valueForKey:@"childBirthday"] firstObject];
            
            NSString *firstName = [[[[header.userInfo objectForKey:@"title"] valueForKey:@"childFirstName"] firstObject] substringToIndex:1];
            NSString *childName = [NSString stringWithFormat:@"%@. %@", firstName, [[[header.userInfo objectForKey:@"title"] valueForKey:@"childLastName"] firstObject]];

            titleLabel.text = [NSString stringWithFormat:@"%@", [childName capitalizedString]];
            
            titleLabel.textColor = [[MyColorTheme sharedInstance]spoilerRoomTextColor];
            
            UIImageView *childrenImageView = (UIImageView *)[view viewWithTag:66];
            NSString *roomImagePath  = [[header.userInfo objectForKey:@"childImage"] lastObject];
            
            NSString *imagePath=[NSString stringWithFormat:@"%@/%@",[NetworkManager sharedInstance].baseUrl, roomImagePath];
            [[Utils sharedInstance]downloadImageAtPath:imagePath completionHandler:^(UIImage *image) {
                childrenImageView.image = image;
                childrenImageView.layer.cornerRadius = 20;
                childrenImageView.layer.masksToBounds = YES;
            }];
        }

        //Обязательное удаление gestureRecognizers, иначе при longPress crash
        while (cell.contentView.gestureRecognizers.count) {
            [cell.contentView removeGestureRecognizer:[cell.contentView.gestureRecognizers objectAtIndex:0]];
        }
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headerTap:)];
        [view addGestureRecognizer:tap];
        
        view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
        header.view = view;
    }
    return header.view;
}


#pragma mark - Handle Header Tap

- (void)headerTap:(UIGestureRecognizer *)gestureRecognizer
{
    UIView *view = gestureRecognizer.view;
    
    NSUInteger index = [self.sections getSectionIndexForView:view];
    
    if (index == NSUIntegerMax) {
        return;
    }
    
    NSIndexSet *reloadIndexSet = [NSIndexSet indexSetWithIndex:index];
    
    AXHeader *header = [self.sections headerAtSectionIndex:index];
    
    AXSection *section = [self.sections sectionAtIndex:index];
    section.isHide = !section.isHide;
    [self updateHeaderArrowImage:header.arrowImageView withState:section.isHide];
    
    NSMutableIndexSet *removeIndexSet = [[NSMutableIndexSet alloc] init];
    NSIndexSet *insertIndexSet = [[NSIndexSet alloc] init];
    
    if ([header.type isEqualToString:PDSpoiler2Cell]) {
        if (section.isHide) {
            
            [removeIndexSet addIndexes:[self.sections getIndexSetForSectionsWithTag:PDChildrenSection andExcludeTag:@"top"]];
            [removeIndexSet addIndexes:[self.sections getIndexSetForSectionsWithTag:@"spacer"]];

            [self.sections removeSectionsByIndexSet:removeIndexSet];
            
        } else {
            NSMutableArray *sections = [[NSMutableArray alloc] init];

            NSArray *childrensSections = [self sectionsForRooms:header.object isHide:YES];
            AXSection *spacerSection = [self sectionsForSpacerWithTag:[NSArray arrayWithObject:@"spacer"]];
            [sections addObjectsFromArray:[NSArray arrayWithArray:childrensSections]];
            [sections addObject:spacerSection];
            insertIndexSet = [self.sections insertSections:sections afterSectionIndex:section.sectionIndex];
        }
    }
    [self.tableView beginUpdates];
    
    //сама кликнутая секция
    [self.tableView reloadSections:reloadIndexSet withRowAnimation:UITableViewRowAnimationNone];
    
    if ([insertIndexSet count] > 0) {
        [self.tableView insertSections:insertIndexSet withRowAnimation:UITableViewRowAnimationNone];
    }
    
    if ([removeIndexSet count] > 0) {
        [self.tableView deleteSections:removeIndexSet withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.tableView endUpdates];
}


- (void)updateHeaderArrowImage:(UIImageView *)imageView withState:(BOOL)state
{
    imageView.image = [[UIImage imageNamed:(state ? @"arrowDown" : @"arrowUp")] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}


#pragma mark - UITableView stuff

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sections countOfSections];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sections countOfRowsInSection:section];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    if (row.type == PDBirthdayCell) {
        int height = self.thisWeekBirthdays.count%2;
        return height == 1 ? row.height*self.thisWeekBirthdays.count/2 + row.height/2 + self.thisWeekBirthdays.count/2*10 :  row.height*self.thisWeekBirthdays.count/2 + self.thisWeekBirthdays.count/2*10;
    }
    return row.height;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    NSString *rowType = row.type;
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    if ([rowType isEqualToString:PDInfoCell] || [rowType isEqualToString:PDInfo2Cell]) {
        cell = [self configureInfoCellForTableView:tableView indexPath:indexPath];
    }else if ([rowType isEqualToString:PDBirthdayCell]) {
        cell = [self configureBirthdayCellForTableView:tableView indexPath:indexPath];
    }else if ([rowType isEqualToString:PDChildBirthInfoCell]) {
        cell = [self configureChildBirthInfoCellForTableView:tableView indexPath:indexPath];
    }else if ([rowType isEqualToString:PDSpoiler1Cell] || [rowType isEqualToString:PDSpoiler2Cell] || [rowType isEqualToString:PDChildSpoiler3Cell]) {
        cell = [self configureDefaultCellForTableView:tableView indexPath:indexPath withBorder:PDBorderBottom];
    }else if ([rowType isEqualToString:PDSpacerCell]) {
        cell = [self configureSpacerCellForTableView:tableView indexPath:indexPath];
    }
    row.cell = cell;
    
    return cell;
}


- (UITableViewCell*) configureSpacerCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"spacerCell"];
    
    return cell;
}

- (UITableViewCell *)configureInfoCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    NSString *rowType = row.type;

    NSString *identifier = [rowType isEqualToString:PDInfoCell] ? @"infoCell" : @"info2Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell) {

    }
    return cell;
}


- (UITableViewCell *)configureBirthdayCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    ThisWeekBirthCell *cell = (ThisWeekBirthCell *)[tableView dequeueReusableCellWithIdentifier:@"weekBirthdaysCell"];
    if (cell) {
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    }
    return cell;
}


- (UITableViewCell *)configureChildBirthInfoCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    AdditionalBirthInfoCell *cell = (AdditionalBirthInfoCell *)[tableView dequeueReusableCellWithIdentifier:@"additionalInfoCell"];
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    ChildrenBirthdaysModel *child = row.object;
    if (cell) {
        cell.firstNameLabel.text = [child.childFirstName capitalizedString];
        cell.lastNameLabel.text = [child.childLastName capitalizedString];
        cell.roomNameLabel.text = child.childRoomName;
        cell.dobLabel.text = [child.childBirthdayDate printableDefault];
        cell.nextAgeLabel.text = [NSString stringWithFormat:@"%@ Years", child.childNextAge];
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    }
    return cell;
}


- (UITableViewCell *)configureDefaultCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath withBorder:(PDBorderType)borderType
{
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:row.identifier];
    if (cell) {
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    }
    return cell;
}


- (void)insertRows:(id)rows afterRowType:(NSString *)rowType andWithRowAnimation:(UITableViewRowAnimation)rowAnimation
{
    NSArray *addRows = [[NSArray alloc] init];
    if ([rows isKindOfClass:[AXRow class]]) {
        addRows = [NSArray arrayWithObject:rows];
    } else if ([rows isKindOfClass:[NSArray class]]) {
        addRows = rows;
    }
    
    NSArray *addedIndexPaths = [self.sections insertRows:addRows afterRowType:rowType atIndex:AXRowFirst];
    if ([addedIndexPaths count] > 0) {
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:addedIndexPaths  withRowAnimation:rowAnimation];
        [self.tableView endUpdates];
    }
    [self.tableView reloadData];
}


- (void)deleteRowsAfterRowType:(NSString *)rowType andWithRowAnimation:(UITableViewRowAnimation)rowAnimation
{
    NSArray *deletingIndexPaths = [self.sections deleteRowsAfterRowType:rowType];
    [self.sections removeArrayRowsByIndexPaths:deletingIndexPaths];
    
    if ([deletingIndexPaths count] > 0) {
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:deletingIndexPaths withRowAnimation:rowAnimation];
        [self.tableView endUpdates];
    }
}


#pragma mark - Collection View

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BirthdayCollectionCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"childBirthInfo" forIndexPath:indexPath];
    
    ChildrenBirthdaysModel *child = [self.thisWeekBirthdays objectAtIndex:indexPath.row];
    
    //NSURL *imageUrl = [NSURL URLWithString:[self getImageUrlWithImage:child.childImage]];
    cell.childNameLabel.text = [NSString stringWithFormat:@"%@ %@", [child.childFirstName capitalizedString], [child.childLastName capitalizedString]];

    NSDate *birthDate = [self dateFromString:child.childBirthday];
    /*
    NSString *birthdayMonth = [self getDayByBirthday:birthDate withFormat:@"MMMM"];
    NSString *birthdayDay = [self getDayByBirthday:birthDate withFormat:@"dd"];
    NSString *birthdayFullDay = [self getDayByBirthday:birthDate withFormat:@"EEE"];
     */
    //cell.birthInfoLabel.text = [NSString stringWithFormat:@"Will be %@ years old on %@, %@ %@", child.childNextAge, birthdayFullDay, [self addSuffixToNumber:[birthdayDay intValue]] , birthdayMonth];
    cell.birthInfoLabel.text = [NSString stringWithFormat:@"Will be %@ years old on %@", child.childNextAge, [birthDate printableBirthday]];
    
    
    /*
    [cell.childImageView setImageWithURL:imageUrl placeholderImage:nil];
    
    cell.childImageView.layer.borderWidth=0.5;
    cell.childImageView.layer.borderColor=[[UIColor whiteColor]CGColor];
    cell.childImageView.layer.cornerRadius=32;
    cell.childImageView.layer.masksToBounds=YES;
    */
    
    cell.childImageView.image = [UIImage imageNamed:@"profile-photo-loading"];
    [[Utils sharedInstance] downloadImageAtRelativePath:child.childImage completionHandler:^(UIImage *image) {
        if (image) {
            cell.childImageView.image = image;
            
        }
        else {
                //error
                cell.childImageView.image = [UIImage imageNamed:@"profile-photo-loading"];
            }
        
        
    }];
    
    CALayer *layer = cell.childImageView.layer;
    layer.masksToBounds = YES;
    layer.cornerRadius = layer.frame.size.width / 2;
    layer.borderWidth = 0.3f;
    layer.borderColor = [UIColor grayColor].CGColor;
    
    return cell;
}



-(NSString*) getImageUrlWithImage:(NSString*)imagePath
{
    NSString *imageUrl=[NSString stringWithFormat:@"%@/%@",[NetworkManager sharedInstance].baseUrl, imagePath];
    NSLog(@"image path: %@", imageUrl);
    return imageUrl;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.thisWeekBirthdays count];
}


- (NSDate *)dateFromString:(NSString *)string
{
    NSDate *dateFromString = [[NSDate alloc] init];
    if (![string isEqual:[NSNull null]]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        [dateFormatter setTimeZone:timeZone];
        [dateFormatter setDateFormat:@"dd.MM.yyyy"];
        dateFromString = [dateFormatter dateFromString:string];
    } else {
        dateFromString = nil;
    }
    
    return dateFromString;
}


-(NSString*) getDayByBirthday:(NSDate*)birthday withFormat:(NSString*)format
{
    NSCalendar *calendar = [[NSCalendar alloc]
                            initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSCalendarUnit unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    
    //gather date components from date
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:birthday];
    NSDateComponents *todayComponents = [calendar components:unitFlags fromDate:[NSDate date]];
    
    //set date components
    [dateComponents setDay:[dateComponents day]];
    [dateComponents setMonth:[dateComponents month]];
    [dateComponents setYear:[todayComponents year]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //save date relative from date
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [dateFormatter setTimeZone:timeZone];
    NSDate *date = [calendar dateFromComponents:dateComponents];
    
    [dateFormatter setLocale:[NSLocale currentLocale]];
    dateFormatter.dateFormat = @"EEE, dd";
    
    NSLog(@"%@",[dateFormatter stringFromDate:date]);
    
    dateFormatter.dateFormat = format;
    
    if ([format isEqualToString:@"EEE"]) {
        return [dateFormatter stringFromDate:date];
    }else{
        return [dateFormatter stringFromDate:birthday];
    }
}



# pragma mark - sections 

-(void) setupUpcomingSection
{
    BOOL isEmpty = YES;
    NSArray *upcomingSections = [self sectionsForChildrenTypeWithChildrenDict:self.sortedChildrenDict isHide:NO isEmpty:&isEmpty];
    
    
    NSIndexSet *indexSet = [self.sections insertSections:upcomingSections afterSectionType:PDThisWeekBirthdaysSection];
    NSArray *infoCellIndexPaths = [self.sections getIndexPathsForRowType:PDInfoCell];
    
    [self.tableView beginUpdates];
    
    if ([indexSet count] > 0) {
        [self.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }
    
    if ([infoCellIndexPaths count] > 0) {
        [self.tableView reloadRowsAtIndexPaths:infoCellIndexPaths withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.tableView endUpdates];
}


-(NSArray *)sectionsForChildrenTypeWithChildrenDict:(NSMutableDictionary *)dict isHide:(BOOL)isHide isEmpty:(BOOL *)isEmpty
{
    NSMutableArray *sections = [[NSMutableArray alloc] init];
    
    AXSection *section = [[AXSection alloc] initWithType:PDUpcomingBirthdaysSection];
    section.isHide = isHide;
    [section addRow:[self.sections genRowWithType:PDSpacerCell]];
    [section.tags addObject:PDUpcomingBirthdaysSection];
    [section.tags addObject:@"main"];
    [section.tags addObject:@"top"];
    
    AXHeader *header = [self.sections genHeaderWithType:PDSpoiler2Cell];
    [header.userInfo setObject:PDUpcomingBirthdaysSection forKey:@"title"];
    [header.userInfo setObject:PDUpcomingBirthdaysSection forKey:@"tag"];
    header.object = dict;
    section.header = header;
    
    [sections addObject:section];
    
    if(dict) {
        NSArray *childrensSections = [self sectionsForRooms:dict isHide:YES];
        [sections addObjectsFromArray:childrensSections];
        [sections addObject:[self sectionsForSpacerWithTag:[NSArray arrayWithObjects:PDUpcomingBirthdaysSection, nil]]];

        *isEmpty = NO;
    }
    return  (NSArray *)sections;
    
}



-(NSArray *)sectionsForRooms:(NSMutableDictionary *)dict isHide:(BOOL)isHide
{
    NSMutableArray *sections = [[NSMutableArray alloc] init];
    
    
    /*
    NSMutableArray *birthdaysArray = [NSMutableArray new];
    
    for (int i = 0; i < dict.allKeys.count; i++) {
        NSString *room = [dict.allKeys objectAtIndex:i];
        [birthdaysArray addObject: [self dateFromString:[[[dict objectForKey:room] firstObject] valueForKey:@"childBirthday"]]];
    }
    
    NSSortDescriptor *sortByMonth = [NSSortDescriptor sortDescriptorWithKey:@"month" ascending:YES];
    NSSortDescriptor *sortByDay = [NSSortDescriptor sortDescriptorWithKey:@"day" ascending:YES];

    NSArray *descriptors=[NSArray arrayWithObjects: sortByMonth, sortByDay, nil];
    NSArray *sortedBirthdayArray=[birthdaysArray sortedArrayUsingDescriptors:descriptors];
    */
    
    //сортируем: сначала будущие, затем прошедшие
    
    NSArray *sortedBirthdayArray = [self sortUpcomingBirthdays:self.birthdays];
    
    
    
    
    NSMutableOrderedSet *names = [NSMutableOrderedSet new];
    
    for (NSDate *childBirth in sortedBirthdayArray) {
        for (int i = 0; i<dict.allKeys.count; i++) {
            NSString *name = dict.allKeys[i];
            NSDate *date = [self dateFromString:[[[dict objectForKey:name] firstObject] valueForKey:@"childBirthday"]];
            if ([childBirth isEqual:date]) {
                [names addObject:name];
            }
        }
    }
    
    BOOL isFirstChild = YES;
    NSDate *prevDate;
    
    for (NSString *room in [names array]) {
        if (![room isEqualToString:@"key"]) {
            
            NSArray *childArray = [dict objectForKey:room];
            ChildrenBirthdaysModel *model;
            if (childArray && [childArray count]) {
                model = [childArray firstObject];
            }
            
            NSDate *date = [self dateFromString:model.childBirthday];
            if (isFirstChild) {
                prevDate = date;
                isFirstChild = NO;
            }
            
            if ([prevDate month] != [date month]) {
                AXSection *childSection = [[AXSection alloc] initWithType:PDChildrenSection];
                AXRow *spacerRow = [self.sections genRowWithType:PDSpacerCell];
                [childSection addRow:spacerRow];
                [sections addObject:childSection];
            }
            prevDate = date;
            
            
            AXSection *childSection = [[AXSection alloc] initWithType:PDChildrenSection];
            childSection.isHide =  YES;
            [childSection.tags addObject:PDChildrenSection];
            [childSection.tags addObject:@"main"];
            [childSection.tags addObject:@"childrenTop"];
            [childSection.tags addObject:room];
            
            AXHeader *childHeader = [self.sections genHeaderWithType:PDChildSpoiler3Cell];
            childHeader.object = childArray;
            [childHeader.userInfo setObject:PDChildrenSection forKey:@"statusTag"];
            [childHeader.userInfo setObject:[dict objectForKey:room] forKey:@"title"];
            [childHeader.userInfo setObject:PDChildrenSection forKey:@"tag"];
            [childHeader.userInfo setObject:[[dict objectForKey:room] valueForKey:@"childImage"] forKey:@"childImage"];
            
            childSection.header = childHeader;
            
            for (ChildrenBirthdaysModel *child in [dict objectForKey:room]) {
                if(child) {
                    AXRow *childRow = [self.sections genRowWithType:PDChildBirthInfoCell];
                    childRow.object = child;
                    [childSection addRow:childRow];
                }
            }
            [sections addObject:childSection];
        }
    }
    
    return  (NSArray *)sections;
}


#pragma mark - Compare dates


-(void)getNext6BirthdaysFromArray:(NSArray*)birthdays
{
    NSMutableArray *childrenBirths = [NSMutableArray new];

    for (ChildrenBirthdaysModel *birth in birthdays) {
        [childrenBirths addObject:[self dateFromString:birth.childBirthday]];
    }
    
    NSDictionary *sorted = [self sortingDates:childrenBirths];
    
    NSMutableArray *next6 = [NSMutableArray new];
    
    NSSortDescriptor *sortByMonth = [NSSortDescriptor sortDescriptorWithKey:@"month" ascending:YES];
    NSSortDescriptor *sortByDay = [NSSortDescriptor sortDescriptorWithKey:@"day" ascending:YES];
    NSArray *sorts = @[sortByMonth, sortByDay];
    
    NSArray *orderedUpcoming = [[sorted objectForKey:@"upcoming"] sortedArrayUsingDescriptors:sorts];

    if ([orderedUpcoming count]>=6) {
        next6 = [NSMutableArray arrayWithArray:[orderedUpcoming subarrayWithRange:NSMakeRange(0, 6)]];
    }else{
        next6 = [NSMutableArray arrayWithArray:[orderedUpcoming subarrayWithRange:NSMakeRange(0, [orderedUpcoming count])]];
    }
    
    if (next6.count<6) {
        for (NSDate *date in [sorted objectForKey:@"next"]) {
            [next6 addObject:date];
            if (next6.count==6) {
                break;
            }
        }
    }    
    
    //NSArray *sortedNext6 = [self sortedDatesByMonthDay:next6];
    
    NSMutableOrderedSet *set = [NSMutableOrderedSet new];
    
    for (NSDate *date in next6) {
        [self.birthdays enumerateObjectsUsingBlock:^(ChildrenBirthdaysModel *model, NSUInteger idx, BOOL *stop) {
             NSDate *childDate = [self dateFromString:model.childBirthday];
            if ([date compare:childDate] == NSOrderedSame) {
                //if (self.thisWeekBirthdays.count==6) {
                if (set.count==6) {
                    return;
                }
                [set addObject:model];
            }
        }];
    }
    
    //Не thisWeek должно быть, а следующие 6 дней рождений
    
    [self.thisWeekBirthdays addObjectsFromArray:set.array];
}



/*
//Вариант когда приходит массив NSDate
- (NSArray*)sortedDatesByMonthDay:(NSArray*)inputArray
{
    NSArray *sortedArray = [inputArray sortedArrayUsingComparator: ^(NSDate *d1, NSDate *d2) {
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:d1];
        NSNumber *day1 = @(components1.day);
        NSNumber *month1 = @(components1.month);
        
        NSDateComponents *customComponents1 = [[NSDateComponents alloc] init];
        [customComponents1 setDay:[day1 intValue]];
        [customComponents1 setMonth:[month1 intValue]];
        [customComponents1 setYear:2015];
        
        NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:d2];
        NSNumber *day2 = @(components2.day);
        NSNumber *month2 = @(components2.month);
        
        NSDateComponents *customComponents2 = [[NSDateComponents alloc] init];
        [customComponents2 setDay:[day2 intValue]];
        [customComponents2 setMonth:[month2 intValue]];
        [customComponents2 setYear:2015];
        
        NSDate *customDate1 = [calendar dateFromComponents:customComponents1];
        NSDate *customDate2 = [calendar dateFromComponents:customComponents2];
        
        return [customDate1 compare:customDate2];
    }];
    return sortedArray;
}
*/

- (NSArray *)sortedChildsByMonthDay:(NSArray*)inputArray
{
    NSArray *sortedArray = [inputArray sortedArrayUsingComparator: ^(ChildrenBirthdaysModel *m1, ChildrenBirthdaysModel *m2) {
        
        NSDate *d1 = [self dateFromString:m1.childBirthday];
        NSDate *d2 = [self dateFromString:m2.childBirthday];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:d1];
        NSNumber *day1 = @(components1.day);
        NSNumber *month1 = @(components1.month);
        
        NSDateComponents *customComponents1 = [[NSDateComponents alloc] init];
        [customComponents1 setDay:[day1 intValue]];
        [customComponents1 setMonth:[month1 intValue]];
        [customComponents1 setYear:2015];
        
        NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:d2];
        NSNumber *day2 = @(components2.day);
        NSNumber *month2 = @(components2.month);
        
        NSDateComponents *customComponents2 = [[NSDateComponents alloc] init];
        [customComponents2 setDay:[day2 intValue]];
        [customComponents2 setMonth:[month2 intValue]];
        [customComponents2 setYear:2015];
        
        NSDate *customDate1 = [calendar dateFromComponents:customComponents1];
        NSDate *customDate2 = [calendar dateFromComponents:customComponents2];
        
        return [customDate1 compare:customDate2];
    }];
    return sortedArray;
}



-(NSDictionary*)sortingDates:(NSArray*)dates
{
    NSMutableDictionary *datesDict = [NSMutableDictionary new];
    NSMutableArray *next = [NSMutableArray new];
    NSMutableArray *upcoming = [NSMutableArray new];

    NSCalendar *calendar = [[NSCalendar alloc]
                            initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

    for (NSDate *birth in dates) {
        NSDateComponents *birthComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:birth];
        
        NSDateComponents *todayComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        
        [todayComponents setYear:[birthComponents year]];
        
        NSDate *today = [calendar dateFromComponents:todayComponents];
        
        
        
        if ([birth compare:today] == NSOrderedAscending) {
            [next addObject:birth];
        }else{
            [upcoming addObject:birth];
        }
        
    }
    [datesDict setObject:next forKey:@"next"];
    [datesDict setObject:upcoming forKey:@"upcoming"];

    return datesDict;
}


-(NSString *) addSuffixToNumber:(int) number
{
    NSString *suffix;
    int ones = number % 10;
    int tens = (number/10) % 10;
    
    if (tens ==1) {
        suffix = @"th";
    } else if (ones ==1){
        suffix = @"st";
    } else if (ones ==2){
        suffix = @"nd";
    } else if (ones ==3){
        suffix = @"rd";
    } else {
        suffix = @"th";
    }
    
    NSString *completeAsString = [NSString stringWithFormat:@"%d%@",number,suffix];
    return completeAsString;
}



//calculating next week birthdays

//-(void)filterByThisWeekWithBirthdays:(NSArray*)birthdaysArray
//{
//    NSMutableArray *childrenBirths = [NSMutableArray new];
//    NSMutableArray *this = [NSMutableArray new];
//    NSMutableArray *upcoming = [NSMutableArray new];
//    
//    for (NSDate *birthDay in childrenBirths) {
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"D"];
//        
//        NSUInteger todayDayOfWeek = [[formatter stringFromDate:[NSDate date]] intValue];
//        NSUInteger birthDayOfWeek = [[formatter stringFromDate:birthDay] intValue];
//        
//        
//        NSUInteger result = todayDayOfWeek - birthDayOfWeek;
//        
//        if (result >= -7) {
//            [this addObject:birthDay];
//        }else{
//            [upcoming addObject:birthDay];
//        }
//    }
//    
//    for (ChildrenBirthdaysModel *model in self.birthdays) {
//        if ([this containsObject:[self dateFromString:model.childBirthday]]) {
//            [self.thisWeekBirthdays addObject:model];
//        }else{
//            [self.upcomingBirthdays addObject:model];
//        }
//    }
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
