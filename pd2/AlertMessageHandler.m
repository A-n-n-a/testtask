//
//  AlertMessageHandler.m
//  pd2
//
//  Created by Andrey on 02/08/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AlertMessageHandler.h"
#import "PSTAlertController.h"

@implementation AlertMessageHandler
+ (void)showAlertForMsg:(NSString *)msgString {
    
    
    if (!msgString || ![msgString isKindOfClass:[NSString class]]) {
        Alert(@"Error", @"Some error");
        return;
    }
    //Not logged in or session expired
    //В medical PS это option=com_sted_mobile&controller=parent&task=list_sted_parent с ответом {"result":"No access. Sign in.","complete":false}
    
    
    
    if (([msgString containsString:@"expired"] && [msgString containsString:@"session"]) || [msgString isEqualToString:@"No access. Sign in."]) {
        
        
        
        //        PSTAlertController *alert = [PSTAlertController alertControllerWithTitle:@"Error" message:@"Please try logging in again" preferredStyle:PSTAlertControllerStyleAlert];
        //
        //        [alert addAction:[PSTAlertAction actionWithTitle:@"Retry login" style:PSTAlertActionStyleDefault handler:^(PSTAlertAction *action) {
        //
        //            [AppDelegate logoutAndSendDeviceTokenIs:YES];
        //
        //
        //        }]];
        //
        //        [alert addAction:[PSTAlertAction actionWithTitle:@"Cancel" style:PSTAlertActionStyleCancel handler:^(PSTAlertAction *action) {
        //
        //        }]];
        //
        //        [alert showWithSender:weakSelf controller:weakSelf animated:YES
        //                   completion:nil];
        //
        //
        //
        //
        //        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please try logging in again" preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction *logout = [UIAlertAction actionWithTitle:@"Retry login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //            [AppDelegate logoutAndSendDeviceTokenIs:YES];
        //        }];
        //        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:logout];
        //        [alert addAction:cancel];
        
        
        //[self.navigationController presentViewController:alert animated:YES completion:nil];
        
        UINavigationController *navigationController = [AppDelegate pTopViewController].navigationController;
        if (navigationController && [navigationController isKindOfClass:[UINavigationController class]]) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please try logging in again" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Retry login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [AppDelegate logoutAndSendDeviceTokenIs:YES];
            }];
            //UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            //[alert addAction:cancel];
            
            [navigationController presentViewController:alert animated:YES completion:nil];
        }
        else {
            Alert(@"Error", @"Please try logging in again");
        }
        
    }
    else {
        Alert(@"Error", msgString);
    }
    
}
@end
