#import "Pd2ResizableCell.h"

@interface Pd2ResizableLabelCell : Pd2ResizableCell

@property (nonatomic, strong) NSString * text;
@property (nonatomic, strong) UIColor * textColor;
@property (nonatomic, strong) UIFont * font;

@end
