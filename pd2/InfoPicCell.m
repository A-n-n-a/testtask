//
//  InfoPicCell.m
//  pd2
//
//  Created by i11 on 12/08/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "InfoPicCell.h"



@implementation InfoPicCell

- (void)awakeFromNib {

    while (self.contentView.gestureRecognizers.count > 0) {
        [self.contentView removeGestureRecognizer:[self.contentView.gestureRecognizers lastObject]];
    }

    self.infoTextView.textContainer.lineFragmentPadding = 0;
    self.infoTextView.textContainerInset = UIEdgeInsetsZero;

    for (UIView *view in self.contentView.subviews) {
        view.backgroundColor = [[MyColorTheme sharedInstance] textViewBackGroundColor];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.contentView layoutIfNeeded];
    //self.infoLabel.preferredMaxLayoutWidth = self.infoLabel.frame.size.width;
}

- (CGFloat)height {
    

    [self layoutIfNeeded];
    
    
    CGFloat height = [self.infoTextView sizeThatFits:CGSizeMake(self.infoTextView.frame.size.width, FLT_MAX)].height;
    
    self.infoTextViewHeightConstraint.constant = height;
    
    
    return [self.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
}


- (void)updateImageWithImagePath:(NSString *)imagePath {
    
    [self updateProfileImageView:self.picImageView withImagePath:imagePath andDefaultImagePath:nil downloadImageCompletition:nil];
    
}


- (UIBezierPath *)picBezierPath {
    
    return [UIBezierPath bezierPathWithRect:CGRectMake(self.picImageView.frame.origin.x-17, self.picImageView.frame.origin.y-8, self.picImageView.frame.size.width+20, self.picImageView.frame.size.height+9)];
    
}

- (NSDictionary *) mainTextAttributes
{
    return @{
      NSForegroundColorAttributeName: [UIColor colorWithRed:109.f/255.f green:109.f/255.f blue:114.f/255.f alpha:1],
      NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:11]
    };
}

- (NSDictionary *) boldTextAttributes
{
    return @{
             NSForegroundColorAttributeName: [UIColor colorWithRed:109.f/255.f green:109.f/255.f blue:114.f/255.f alpha:1],
             NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Medium" size:11]
    };
}





@end
