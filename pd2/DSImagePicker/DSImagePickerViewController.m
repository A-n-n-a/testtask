#import "DSImagePickerViewController.h"
#import "DSCollectionPhotoCell.h"
#import "QBImagePickerController.h"
#import "MyColorTheme.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ALAssetsLibrary+Init.h"

#define kGalleryHeight     100.0f
#define kButtonHeight      45.0f
#define kTopOffset         5.0f
#define kAnimationDuration 0.25f

@interface UIButton (TitleInit)
+ (id) buttonWithType:(UIButtonType)buttonType title:(NSString *)title;
- (void) normalFont;
- (void) disabledFont;
- (void) boldFont;
@end

@implementation UIButton (TitleInit)

+ (id) buttonWithType:(UIButtonType)buttonType title:(NSString *)title
{
    UIButton * button = [UIButton buttonWithType:buttonType];
    [button setTitle:title forState:UIControlStateNormal];
    return button;
}

- (void) normalFont
{
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f];
    [self setTitleColor:UIColorFromRGB(0x056cc4) forState:UIControlStateNormal];
    [self setTitleColor:UIColorFromRGB(0x93c3ec) forState:UIControlStateHighlighted];
}

- (void) disabledFont
{
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f];
    [self setTitleColor:UIColorFromRGB(0x93c3ec) forState:UIControlStateNormal];
    [self setTitleColor:UIColorFromRGB(0x93c3ec) forState:UIControlStateHighlighted];
}

- (void) boldFont
{
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17.0f];
    [self setTitleColor:UIColorFromRGB(0x056cc4) forState:UIControlStateNormal];
    [self setTitleColor:UIColorFromRGB(0x93c3ec) forState:UIControlStateHighlighted];
}

@end


@interface DSImagePickerViewController () <UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, QBImagePickerControllerDelegate, UIImagePickerControllerDelegate>

@property (strong, nonatomic) UIWindow * window;

@property (strong, nonatomic) UIView * view_background;
@property (strong, nonatomic) UIView * view_content;
@property (strong, nonatomic) UIView * view_bottomFiller;
@property (strong, nonatomic) UICollectionView * collection_photos;

@property (strong, nonatomic) UIButton * button_take;
@property (strong, nonatomic) UIButton * button_cameraRoll;
@property (strong, nonatomic) UIButton * button_view;
@property (strong, nonatomic) UIButton * button_cancel;

@property (strong, nonatomic) NSMutableArray * array_selectedIndexes;
@property (strong, nonatomic) NSMutableArray * array_buttonsTitles;
@property (strong, nonatomic) NSMutableArray * array_buttons;
@property (strong, nonatomic) NSMutableArray * array_assets;


@property (nonatomic) BOOL viewButtonEnabled;
@property (readwrite) CGFloat oldY, coeff;

@end



@implementation DSImagePickerViewController
@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    self.array_assets          = [@[] mutableCopy];
    self.array_selectedIndexes = [@[] mutableCopy];
    
    self.button_cameraRoll = [UIButton buttonWithType:UIButtonTypeCustom title:@"Upload Photos/Camera Roll"];
    [self.button_cameraRoll addTarget:self action:@selector(cameraRollTap:) forControlEvents:UIControlEventTouchUpInside];
    
    self.button_take       = [UIButton buttonWithType:UIButtonTypeCustom title:@"Take Photo"];
    [self.button_take addTarget:self action:@selector(takePhotoTap:) forControlEvents:UIControlEventTouchUpInside];
    
    self.button_view       = [UIButton buttonWithType:UIButtonTypeCustom title:@"View Existing Photos"];
    self.button_view.enabled = self.viewButtonEnabled;
    [self.button_view addTarget:self action:@selector(viewPhotosTap:) forControlEvents:UIControlEventTouchUpInside];
    
    self.button_cancel     = [UIButton buttonWithType:UIButtonTypeCustom title:@"Cancel"];
    [self.button_cancel addTarget:self action:@selector(cancelTap:) forControlEvents:UIControlEventTouchUpInside];
    
    self.array_buttons    = [NSMutableArray arrayWithArray:@[self.button_cameraRoll, self.button_take,  self.button_view, self.button_cancel]];
    
    [self.button_cameraRoll normalFont];
    [self.button_take normalFont];
    [self.button_cancel boldFont];
    
    
    if (self.button_view.enabled) {
        [self.button_view boldFont];
    } else {
        [self.button_view disabledFont];
    }
    
    
    [self.array_buttonsTitles addObject:@"Cancel"];
}

- (void) setupInterface
{
    self.window = [UIApplication sharedApplication].keyWindow;
    
    // Filler
    self.view_bottomFiller = [[UIView alloc] initWithFrame:(CGRect) {
        .origin = {0, self.window.bounds.size.height},
        .size = {self.window.frame.size.width, 1}
    }];
    self.view_bottomFiller.backgroundColor = [UIColor whiteColor];
    
    // Background
    self.view_background = [[UIView alloc] initWithFrame:self.view.bounds];
    self.view_background.backgroundColor = [UIColor blackColor];
    self.view_background.alpha = 0;
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTap:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [self.view_background addGestureRecognizer:tap];
    
    // Content
    self.view_content = [[UIView alloc] initWithFrame:[self contentHiddenFrame]];
    self.view_content.backgroundColor = [UIColor whiteColor];
    
    [self.window addSubview: self.view_background];
    [self.window addSubview: self.view_bottomFiller];
    [self.window addSubview: self.view_content];
    
    // Bounces gesture
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [self.window addGestureRecognizer:panRecognizer];
    
    void(^addLineView)(CGFloat, CGFloat) = ^void(CGFloat offsetY, CGFloat offsetX){
        UIView * line = [[UIView alloc] initWithFrame:(CGRect){
            .origin = {offsetX, offsetY},
            .size   = {self.view_content.frame.size.width-offsetX, 1}
        }];
        line.backgroundColor = [[MyColorTheme sharedInstance] toolbarLineColor];
        [self.view_content addSubview: line];
        [self.view_content bringSubviewToFront:line];
    };
    
    __block CGFloat offset = kGalleryHeight + kTopOffset * 2;
    
    [self.array_buttons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIButton * button = (UIButton *) obj;
        button.frame = (CGRect) {
            .origin = {0, offset},
            .size   = {self.view_content.frame.size.width, kButtonHeight}
        };

        addLineView(offset, idx == 0 ? 0 : 8);
        [self.view_content addSubview:button];
        
        offset += kButtonHeight;
    }];
    addLineView(offset, 8);

    [self afterImageSelection];
    
    // Photos
    UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    aFlowLayout.itemSize = CGSizeMake(kGalleryHeight, kGalleryHeight);
    [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    CGRect collectionViewFrame = (CGRect){
        .origin = {kTopOffset, kTopOffset},
        .size   = {self.view.bounds.size.width - kTopOffset*2, kGalleryHeight}
    };
    
    self.collection_photos = [[UICollectionView alloc] initWithFrame:collectionViewFrame collectionViewLayout:aFlowLayout];
    [self.collection_photos setBackgroundColor:[UIColor whiteColor]];
    self.collection_photos.showsHorizontalScrollIndicator = NO;
    self.collection_photos.showsVerticalScrollIndicator = NO;
    self.collection_photos.delegate = self;
    self.collection_photos.dataSource = self;
    [self.collection_photos registerClass:[DSCollectionPhotoCell class] forCellWithReuseIdentifier:@"Cell"];
    
    
    [self.view_content addSubview:self.collection_photos];
    [self getCameraRollImages];
}

- (void) setContentFrame:(CGRect)frame
{
    // Content
    self.view_content.frame = frame;
    
    // Fill space between content and bottom border
    CGFloat toFill = self.window.frame.size.height - (self.view_content.frame.origin.y + self.view_content.frame.size.height);
    self.view_bottomFiller.frame = (CGRect){
        .origin = {0, self.view_content.frame.origin.y + self.view_content.frame.size.height},
        .size = {self.window.frame.size.width, toFill}
    };
}

- (CGRect) contentVisibleFrame
{
    CGFloat contentHeight = kTopOffset * 2 + kGalleryHeight + kButtonHeight * [self.array_buttons count];
    return (CGRect) {
        .origin = {0, self.window.frame.size.height - self.view_content.frame.size.height},
        .size   = {self.view.bounds.size.width, contentHeight}
    };
}

- (CGRect) contentHiddenFrame
{
    CGFloat contentHeight = kTopOffset * 2 + kGalleryHeight + kButtonHeight * [self.array_buttons count];
    return (CGRect) {
        .origin = {0, self.window.frame.size.height},
        .size   = {self.view.bounds.size.width, contentHeight}
    };
}

- (void) afterImageSelection
{
    if (self.array_selectedIndexes.count > 0) {
        [self.button_cameraRoll setTitle:[NSString  stringWithFormat:@"Upload %ld photos", self.array_selectedIndexes.count] forState:UIControlStateNormal];
        [self.button_cameraRoll boldFont];
    } else {
        [self.button_cameraRoll setTitle:@"Camera Roll" forState:UIControlStateNormal];
        [self.button_cameraRoll normalFont];
    }
}


#pragma mark - Show/Dismiss -

- (void)showImagePickerInController:(UIViewController *)controller {
    [self showImagePickerInController:controller animated:YES];
}

- (void)showImagePickerInController:(UIViewController *)controller animated:(BOOL)animated
{
    self.modalPresentationStyle = UIModalPresentationCustom;
    [controller presentViewController:self animated:NO completion:nil];
    
    [self setupInterface];
    [self showContent: animated];
}

- (void) showContent:(BOOL)animated
{
    [UIView animateWithDuration:animated ? kAnimationDuration : 0 animations:^{
        self.view_content.frame = [self contentVisibleFrame];
        self.view_background.alpha = 0.3f;
    }];
}

- (void) hideContent:(BOOL)animated
{
    [UIView animateWithDuration: animated ? kAnimationDuration : 0 animations:^{
        [self setContentFrame:[self contentHiddenFrame]];
        self.view_background.alpha = 0;
    } completion:^(BOOL finished) {
        [self.view_background removeFromSuperview];
        [self.view_content removeFromSuperview];
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

- (void) dissmiss:(BOOL)animated
{
    [self hideContent:animated];
}



#pragma mark - Bounce -

-(void)move:(UIPanGestureRecognizer *)sender
{
    [self.view bringSubviewToFront:[(UIPanGestureRecognizer*)sender view]];
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.window];
    
    if ([sender state] == UIGestureRecognizerStateBegan) {
        self.oldY  = translatedPoint.y;
        self.coeff = 0.4;
    } else if ([sender state] == UIGestureRecognizerStateEnded) {
        CGFloat diff = self.view_content.frame.origin.y - [self contentVisibleFrame].origin.y;
        if (diff > self.view_content.frame.size.height * 0.2) {
            [self hideContent:YES];
        } else {
            [UIView animateWithDuration:0.25 animations:^{
                [self setContentFrame:[self contentVisibleFrame]];
            }];
        }
    } else {
        CGFloat step = translatedPoint.y - self.oldY;
        step *= self.coeff;
        self.oldY = translatedPoint.y;
        [UIView animateWithDuration:0.25 animations:^{
            [self setContentFrame:(CGRect) {
                .origin = {self.view_content.frame.origin.x, self.view_content.frame.origin.y + step},
                .size = self.view_content.frame.size
            }];
        }];
    }
}



#pragma mark - Collection view

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.array_assets count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DSCollectionPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    [cell setSelectableEnabled:!self.singlePhotoSelection];
    
    if ([self.array_selectedIndexes containsObject:indexPath]) {
        [cell selectImage:NO];
    } else {
        [cell deselectImage:NO];
    }
    
    ALAsset *asset = self.array_assets[[self.array_assets count] - 1 - indexPath.row];
    [cell setImage:[UIImage imageWithCGImage:[asset thumbnail]]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];

    
    if (self.singlePhotoSelection) {
        NSMutableArray * assets = [@[] mutableCopy];
        [assets addObject:self.array_assets[[self.array_assets count] - 1 - indexPath.row]];
        [self sendAssetsToDelegate:assets];
        return;
    }
    
    DSCollectionPhotoCell * cell = (DSCollectionPhotoCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if ([self.array_selectedIndexes containsObject:indexPath]) {
        [cell deselectImage:YES];
        [self.array_selectedIndexes removeObject:indexPath];
    } else {
        [cell selectImage:YES];
        [self.array_selectedIndexes addObject:indexPath];
    }
    [self afterImageSelection];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(kGalleryHeight, kGalleryHeight);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return kTopOffset;
}



#pragma mark - Image library -

- (void)getCameraRollImages
{
    self.array_assets = [@[] mutableCopy];
    __block NSMutableArray *tmpAssets = [@[] mutableCopy];
    ALAssetsLibrary *assetsLibrary = [ALAssetsLibrary defaultAssetsLibrary];
    [assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
            if(result)
            {
                [tmpAssets addObject:result];
            }
        }];
        
        ALAssetsGroupEnumerationResultsBlock assetsEnumerationBlock = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
            if (result) {
                [self.array_assets addObject:result];
            }
        };
        
        ALAssetsFilter *onlyPhotosFilter = [ALAssetsFilter allPhotos];
        [group setAssetsFilter:onlyPhotosFilter];
        [group enumerateAssetsUsingBlock:assetsEnumerationBlock];
        
        [self.collection_photos reloadData];
    } failureBlock:^(NSError *error) {
        NSLog(@"Error loading images %@", error);
    }];
}



#pragma mark - QBImagePickerControllerDelegate -

- (void) qb_imagePickerController:(QBImagePickerController *)imagePickerController didSelectAsset:(ALAsset *)asset
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self sendAssetsToDelegate: @[asset]];
    }];
}

- (void) qb_imagePickerController:(QBImagePickerController *)imagePickerController didSelectAssets:(NSArray *)assets
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self sendAssetsToDelegate: assets];
    }];
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}



#pragma mark - Actions -

- (void) backgroundTap:(id)sender
{
    [self hideContent:YES];
}


- (void) cameraRollTap:(id)sender
{
    if (self.array_selectedIndexes.count > 0) {
        NSMutableArray * assets = [@[] mutableCopy];
        for (NSIndexPath * indexPath in self.array_selectedIndexes) {
            ALAsset *asset = self.array_assets[[self.array_assets count] - 1 - indexPath.row];
            [assets addObject:asset];
        }
        [self sendAssetsToDelegate:assets];
    } else {
        QBImagePickerController *imagePickerController    = [QBImagePickerController new];
        imagePickerController.showsNumberOfSelectedAssets = YES;
        imagePickerController.delegate                    = self;
        imagePickerController.filterType = QBImagePickerControllerFilterTypePhotos;
        
        if (self.singlePhotoSelection) {
            imagePickerController.maximumNumberOfSelection = 1;
            imagePickerController.allowsMultipleSelection  = NO;
        } else {
            imagePickerController.allowsMultipleSelection  = YES;
        }
        [self presentViewController:imagePickerController animated:YES completion:NULL];
    }
}

- (void) takePhotoTap:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void) viewPhotosTap:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(imagePickerViewPhotosTap)]) {
        [self.delegate imagePickerViewPhotosTap];
    }
    [self dissmiss:YES];
}

- (void) cancelTap:(id)sender
{
    [self hideContent:YES];
}

- (void) sendAssetsToDelegate:(NSArray *)assets
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(imagePickerDidSelectImages:)]) {
        [self.delegate imagePickerDidSelectImages:assets];
    }
    [self dissmiss:YES];
    
}

- (void) setViewButtonEnabled:(BOOL)enabled
{
    _viewButtonEnabled = enabled;
    if (self.button_view != nil) {
        if (enabled) {
            [self.button_view boldFont];
        } else {
            [self.button_view disabledFont];
        }
        self.button_view.enabled = enabled;
    }
}


#pragma mark - UIImagePickerController delegate -

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self sendAssetsToDelegate:@[[info objectForKey:UIImagePickerControllerOriginalImage]]];
}


@end




