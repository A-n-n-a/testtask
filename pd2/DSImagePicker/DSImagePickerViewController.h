#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/UTCoreTypes.h>

@class DSImagePickerViewController;
@protocol DSImagePickerDelegate <NSObject>

// Array of ALAsset's
- (void) imagePickerDidSelectImages:(NSArray *)images;
- (void) imagePickerViewPhotosTap;

@end

@interface DSImagePickerViewController : UIViewController
@property (weak, nonatomic) id<DSImagePickerDelegate>delegate;
@property (nonatomic) BOOL singlePhotoSelection;
- (void) setViewButtonEnabled:(BOOL)enabled;
- (void) showImagePickerInController:(UIViewController *)controller animated:(BOOL)animated;
- (void) dissmiss:(BOOL)animated;
@end