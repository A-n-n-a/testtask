#import "DSRoundedCheckbox.h"

@interface DSRoundedCheckbox()
@property (strong, nonatomic) UIImageView * imageView_checked;
@property (strong, nonatomic) UIImageView * imageView_unchecked;
@property (readwrite) CGRect defFrame;
@property (readwrite) CGPoint defCenter;
@end

@implementation DSRoundedCheckbox



#pragma mark - Init -

- (id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setFrame:frame];
        [self baseInit];
    }
    return self;
}

- (id) init
{
    if (self = [super init]) {
        [self baseInit];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self baseInit];
    }
    return self;
}

- (void) baseInit
{
    self.backgroundColor = [UIColor clearColor];
    
    CGFloat size = self.defFrame.size.width;
    
    if (self.imageView_checked == nil) {
        self.imageView_checked = [[UIImageView alloc] init];
        self.imageView_checked.image = [UIImage imageNamed:@"checkbox_checked_green"];
        self.imageView_checked.layer.cornerRadius  = size/2;
        self.imageView_checked.layer.masksToBounds = YES;
        [self addSubview:self.imageView_checked];
    }
    if (self.imageView_unchecked == nil) {
        self.imageView_unchecked = [[UIImageView alloc] init];
        self.imageView_unchecked.image = [UIImage imageNamed:@"checkbox_unchecked"];
        self.imageView_unchecked.layer.cornerRadius  = size/2;
        self.imageView_unchecked.layer.masksToBounds = YES;
        [self addSubview:self.imageView_unchecked];
    }
    self.imageView_checked.frame = self.defFrame;
    self.imageView_unchecked.frame = self.defFrame;
    
    [self bringSubviewToFront:self.imageView_unchecked];
    [self bringSubviewToFront:self.imageView_checked];
    
    [self setUnchecked:NO];
}

- (void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    self.defFrame  = (CGRect){.origin = CGPointZero, .size = frame.size};
    self.defCenter = (CGPoint){CGRectGetMidX(self.defFrame), CGRectGetMidY(self.defFrame)};
    [self baseInit];
}



#pragma mark - Checked -

- (void) setChecked:(BOOL)animated
{
    [UIView animateWithDuration:animated ? 0.15 : 0 animations:^{
        self.imageView_unchecked.frame = [self frameForScale:0.8];
        self.imageView_checked.frame   = [self frameForScale: 1.2];
        self.imageView_unchecked.alpha = 0;
        self.imageView_checked.alpha   = 1;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:animated ? 0.25 : 0 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.imageView_checked.frame  = [self frameForScale:1];
        } completion:^(BOOL finished) {}];
    }];
}

- (void) setUnchecked:(BOOL)animated
{
    [UIView animateWithDuration:animated ? 0.15 : 0 animations:^{
        self.imageView_unchecked.frame = [self frameForScale:1];
        self.imageView_checked.frame   = [self frameForScale:0];
        self.imageView_unchecked.alpha = 1;
        self.imageView_checked.alpha   = 0;
    }];
}

- (CGRect) frameForScale:(CGFloat)scale
{
    CGFloat size = self.defFrame.size.width*scale;
    return (CGRect) {
        .origin = {self.defCenter.x-size/2, self.defCenter.y-size/2},
        .size   = {size, size}
    };
}

@end
