#import "DSCollectionPhotoCell.h"
#import "DSSelectableImageView.h"

@interface DSCollectionPhotoCell()

@property (strong, nonatomic) DSSelectableImageView * imageView_selectable;

@end

@implementation DSCollectionPhotoCell

- (CGFloat) size
{
    return self.contentView.frame.size.width*0.2;
}

- (void) baseInit
{
    self.imageView_selectable = [[DSSelectableImageView alloc] initWithFrame:self.contentView.bounds];
    self.imageView_selectable.clipsToBounds = YES;
    self.imageView_selectable.contentMode   = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:self.imageView_selectable];
}

- (id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self baseInit];
    }
    return self;
}

- (void) setImage:(UIImage *)image
{
    self.imageView_selectable.image = image;
}

- (void) selectImage:(BOOL)animated
{
    [self.imageView_selectable setSelected:YES animated:animated];
}

- (void) deselectImage:(BOOL)animated
{
    [self.imageView_selectable setSelected:NO animated:animated];
}

- (void) setSelectableEnabled:(BOOL)enabled
{
    [self.imageView_selectable setCheckboxHidden:!enabled];
}

@end
