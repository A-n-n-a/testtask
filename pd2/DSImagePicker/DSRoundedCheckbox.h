#import <UIKit/UIKit.h>

@interface DSRoundedCheckbox : UIView
@property (readwrite) BOOL isChecked;

- (void) setChecked:(BOOL)animated;
- (void) setUnchecked:(BOOL)animated;
- (void) setFrame:(CGRect)frame;
@end
