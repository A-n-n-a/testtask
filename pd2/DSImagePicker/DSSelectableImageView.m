#import "DSSelectableImageView.h"
#import "DSRoundedCheckbox.h"

@interface DSSelectableImageView()
@property (strong, nonatomic) DSRoundedCheckbox * view_checkbox;
@end

@implementation DSSelectableImageView

#pragma mark - Init -

- (id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self updateCheckbox];
    }
    return self;
}

- (id) initWithImage:(UIImage *)image
{
    if (self = [super initWithImage:image]) {
        [self updateCheckbox];
    }
    return self;
}

- (id) initWithImage:(UIImage *)image highlightedImage:(UIImage *)highlightedImage
{
    if (self = [super initWithImage:image highlightedImage:highlightedImage]) {
        [self updateCheckbox];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self updateCheckbox];
    }
    return self;
}



#pragma mark - Checkbox -

- (void) updateCheckbox
{
    if (self.view_checkbox == nil) {
        self.view_checkbox = [[DSRoundedCheckbox alloc] initWithFrame:CGRectZero];
        [self addSubview:self.view_checkbox];
    }
    
    CGFloat selfWidth = self.frame.size.width;
    CGFloat size      = selfWidth*0.2f;
    CGFloat offset    = 5;
    
    CGRect frame = (CGRect){
        .origin = {selfWidth - offset - size, selfWidth - offset - size},
        .size   = {size, size}
    };
    [self.view_checkbox setFrame: frame];
}

- (void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self updateCheckbox];
}

- (void) setSelected:(BOOL)selected animated:(BOOL)animated
{
    if (selected) {
        [self.view_checkbox setChecked:animated];
    } else {
        [self.view_checkbox setUnchecked:animated];
    }
}

- (void) setCheckboxHidden:(BOOL)hidden
{
    self.view_checkbox.hidden = hidden;
}

@end
