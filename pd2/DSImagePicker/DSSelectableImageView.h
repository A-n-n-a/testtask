#import <UIKit/UIKit.h>

@interface DSSelectableImageView : UIImageView

- (void) setCheckboxHidden:(BOOL)hidden;
- (void) setSelected:(BOOL)selected animated:(BOOL)animated;

@end
