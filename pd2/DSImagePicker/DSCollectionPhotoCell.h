//
//  DSCollectionPhotoCell.h
//  iOS8Style-ImagePicker
//
//  Created by Denis Skripnichenko on 06/05/15.
//  Copyright (c) 2015 Jake Sieradzki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSCollectionPhotoCell : UICollectionViewCell

- (void) setSelectableEnabled:(BOOL)enabled;
- (void) setImage:(UIImage *)image;
- (void) selectImage:(BOOL)animated;
- (void) deselectImage:(BOOL)animated;

@end
