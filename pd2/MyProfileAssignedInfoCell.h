//
//  MyProfileAssignedInfoCell.h
//  pd2
//
//  Created by Eugene Fozekosh on 20.11.15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileAssignedInfoCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *adminLevelTF;
@property (strong, nonatomic) IBOutlet UITextField *assignedRoomTF;
@property (strong, nonatomic) IBOutlet UITextField *assignedPositionTF;

@end
