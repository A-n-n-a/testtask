//
//  NoteView.h
//  pd2
//
//  Created by User on 15.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (boldness)
-(void)makeFirstLineBold;
-(void)makeFirstLineBoldAndOtherStrings:(NSArray*)strings;

@end


@interface NoteView : UIView

@property(nonatomic,strong) UITextView *textView;
@property(nonatomic) bool firstLineBold,topView,lessHeight;

-(CGFloat)heightForThisView;

@end
