//
//  ChildContentCell.h
//  pd2
//
//  Created by User on 22.03.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckSwitch.h"

@interface ChildContentCell : UITableViewCell

@property (nonatomic,strong)IBOutlet UITextField *childFirstNameTextField;
@property (nonatomic,strong)IBOutlet UITextField *childLastNameTextField;
@property (nonatomic,strong)IBOutlet UITextField *childPostCodeTextField;
@property (nonatomic,strong)IBOutlet UITextField *childHouseStreetTextField;
@property (nonatomic,strong)IBOutlet UITextField *childAreaTextField;
@property (nonatomic,strong)IBOutlet UITextField *childTownTextField;
@property (nonatomic,strong)IBOutlet UITextField *childCountryTextField;
@property (strong, nonatomic) IBOutlet UITextField *childStartingDateTF;
@property (strong, nonatomic) IBOutlet UITextField *childFinishDateTF;
@property (strong, nonatomic) IBOutlet CheckSwitch *parentAlertsSwitch;

@end
