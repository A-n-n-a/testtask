#import "AOLPlistScottish.h"
#import "NSString+Color.h"

@implementation AOLPlistScottish

-(id)init
{
    if (self =[super init]) {
        self.fileName = @"areasOfLearnings_scottish";
    }
    return self;
}

- (NSArray *) sortedAreasOfLearningsKeys
{
    return  @[
              @"sa",
              @"he",
              @"ac",
              @"nu",
              @"at",
              @"rd",
              @"rb",
              @"in"
              ];
}

- (NSString *) mainColorForType:(NSString *)type
{
    NSDictionary * mainColor = @{
                                 @"sa": @"e7bdbd",
                                 @"he": @"c6d6bd",
                                 @"ac": @"ced6de",
                                 @"nu": @"efefad",
                                 @"at": @"d6cede",
                                 @"rd": @"f7d6ef",
                                 @"rb": @"9cdede",
                                 @"in": @"f7deb5"
                                 };
    return mainColor[type];
}

- (NSString *) highlightedColorForType:(NSString *)type
{
    NSDictionary * mainColor = @{
                                 @"sa": @"F3E5E5",
                                 @"he": @"EBEFE9",
                                 @"ac": @"EBEEF1",
                                 @"nu": @"F6F6E6",
                                 @"at": @"F1EFF3",
                                 @"rd": @"FCEEF9",
                                 @"rb": @"EBFBFB",
                                 @"in": @"FFFCF7"
                                 };
    return mainColor[type];
}


- (NSString *) generalColor
{
    return @"EEEEEE";
}

- (NSDictionary *) aolsTitles
{
    return  @{
              @"sa": @"Safe",
              @"he": @"Healthy",
              @"ac": @"Achieving",
              @"nu": @"Nurtured",
              @"at": @"Active",
              @"rd": @"Respected",
              @"rb": @"Responsible",
              @"in": @"Included"
    };
}

- (NSDictionary *) aolsIcons
{
    return  @{
              @"sa": @"wellbeing_aol_s_21",
              @"he": @"wellbeing_aol_h_21",
              @"ac": @"wellbeing_aol_a_21",
              @"nu": @"wellbeing_aol_n_21",
              @"at": @"wellbeing_aol_a_21",
              @"rd": @"wellbeing_aol_r_21",
              @"rb": @"wellbeing_aol_r_21",
              @"in": @"wellbeing_aol_i_21"
              };
}

- (NSDictionary *) tabsIcons
{
    return  @{};
}

- (NSDictionary *) tabsIndexes
{
    return  @{@"tab1":@0};
}

- (NSDictionary *) aolsButtonsColors
{
    return @{
             @"sa": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"sa"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"sa"]]
                     },
             @"he": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"he"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"he"]]
                     },
             @"ac": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"ac"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"ac"]]
                     },
             @"nu": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"nu"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"nu"]]
                     },
             @"at": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"at"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"at"]]
                     },
             @"rd": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"rd"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"rd"]]
                     },
             @"rb": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"rb"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"rb"]]
                     },
             @"in": @{
                     @"normal":[NSString colorFromString:[self mainColorForType:@"in"]],
                     @"highlighted": [NSString colorFromString:[self highlightedColorForType:@"in"]]
                     },
    };
}


@end
