#import "NSDateFormatter+Convert.h"

@implementation NSDateFormatter (Convert)

+ (NSString *) convertDateFromString:(NSString *)dateStr
                          fromFormat:(NSString *)fromFormat
                            toFormat:(NSString *)toFormat
{
    NSLocale *loc = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:loc];
    [dateFormatter setDateFormat:fromFormat];
    
    NSDate * date = [dateFormatter dateFromString:dateStr];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:loc];
    [dateFormatter setDateFormat:toFormat];
    
    NSString * convertedString = [dateFormatter stringFromDate:date];
    return convertedString;
}

@end
