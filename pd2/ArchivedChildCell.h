//
//  ArchivedChildCell.h
//  pd2
//
//  Created by Eugene Fozekosh on 16.10.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ManageChildModel.h"

@interface ArchivedChildCell : UITableViewCell

@property bool archived;

@property (nonatomic,retain)IBOutlet UIButton *archiveButton;

@property (nonatomic,retain)IBOutlet UILabel *firstNameLabel;
@property (nonatomic,retain)IBOutlet UILabel *lastNameLabel;
@property (nonatomic,retain)IBOutlet UILabel *dateOfBirthLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (nonatomic,retain)IBOutlet UIImageView *photoImageView;

@property (nonatomic,retain) NSDictionary *infoDict;

@property (nonatomic,retain)NSMutableArray *siblings;

-(void)initWithModel:(ManageChildModel*) model;


@end
