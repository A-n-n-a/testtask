//
//  VideoView.h
//  pd2
//
//  Created by User on 06.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface VideoView : UIView

@property (nonatomic,retain) NSURL *videoURL;
@property (nonatomic,retain) MPMoviePlayerController *mpController;

@property (nonatomic,retain) AVPlayer *player;
@property (nonatomic,retain) AVPlayerLayer *playerLayer;

@end
