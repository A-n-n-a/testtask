//
//  DailyNotesChildrenNoteParentSideModel.h
//  pd2
//
//  Created by Andrey on 07/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXBaseModel.h"


@interface DailyNotesChildrenNoteParentSideModel : AXBaseModel


@property (nonatomic, strong) NSNumber *noteId;
@property (nonatomic, strong) NSString *noteTitle;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, strong) NSString *adminFirstName;
@property (nonatomic, strong) NSString *adminLastName;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, assign) int commentsCount;



- (instancetype)initWithDict:(NSDictionary *)dict;
- (instancetype)initWithViewDict:(NSDictionary *)dict;

- (BOOL)modelContain:(NSString *)containString;

@end
