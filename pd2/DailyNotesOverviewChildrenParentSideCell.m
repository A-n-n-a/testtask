//
//  DailyNotesOverviewChildrenParentSideCell.m
//  pd2
//
//  Created by Andrey on 08.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesOverviewChildrenParentSideCell.h"
#import "DailyNotesChildrenParentSideModel.h"


@interface DailyNotesOverviewChildrenParentSideCell ()

@end

@implementation DailyNotesOverviewChildrenParentSideCell

- (void)updateWithModel:(DailyNotesChildrenParentSideModel *)model {
    
    self.firstNameLabel.text = notAvailStringIfNil([model.firstName capitalizedString]);
    self.lastNameLabel.text = notAvailStringIfNil([model.lastName capitalizedString]);
    self.monthlyNotesLabel.attributedText = [self attributedStringForMonthlyNotes:model.monthlyNotes andTotal:model.monthlyNotesTotal];
    self.commentsLabel.attributedText = [self attributedStringForNewComments:model.commentsNew andTotal:model.commentsTotal];

    
}


- (void)updateImageWithModel:(DailyNotesChildrenParentSideModel *)model {
    
    [self updateProfileImageView:self.photoImageView withImagePath:model.photoPath andDefaultImagePath:nil downloadImageCompletition:nil];
}


- (NSAttributedString *)attributedStringForMonthlyNotes:(int)monthly andTotal:(int)total {
    
    UIColor *color = [UIColor darkGrayColor];
    
    
    UIFont *mediumFont = [UIFont boldSystemFontOfSize:12.f];
    UIFont *lightFont = [UIFont systemFontOfSize:12.f];
    
    NSDictionary *mediumDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     mediumFont, NSFontAttributeName,
                                     color, NSForegroundColorAttributeName, nil];
    
    NSDictionary *lightDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                lightFont, NSFontAttributeName,
                                color, NSForegroundColorAttributeName, nil];
    
    
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d", monthly] attributes:mediumDict];
    
    [attributed appendAttributedString:[[NSAttributedString alloc] initWithString:@" (Total: " attributes:lightDict]];
    [attributed appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d", total] attributes:mediumDict]];
    [attributed appendAttributedString:[[NSAttributedString alloc] initWithString:@")" attributes:lightDict]];
    
    
    return attributed;
    
}


- (NSAttributedString *)attributedStringForNewComments:(int)newComments andTotal:(int)total {
    
    UIColor *color = [UIColor darkGrayColor];
    UIColor *greenColor = [MyColorTheme sharedInstance].buttonConfirmBackgroundColor;
    
    
    UIFont *mediumFont = [UIFont boldSystemFontOfSize:12.f];
    UIFont *lightFont = [UIFont systemFontOfSize:12.f];
    
    NSDictionary *mediumDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                mediumFont, NSFontAttributeName,
                                color, NSForegroundColorAttributeName, nil];
    
    NSDictionary *lightDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                               lightFont, NSFontAttributeName,
                               color, NSForegroundColorAttributeName, nil];
    
    NSDictionary *mediumColoredDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                mediumFont, NSFontAttributeName,
                                greenColor, NSForegroundColorAttributeName, nil];
    
    
    if (newComments == 0) {
        return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d", total] attributes:mediumDict];
    }
    else {
        NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d", total] attributes:mediumDict];
        
        [attributed appendAttributedString:[[NSAttributedString alloc] initWithString:@" (" attributes:lightDict]];
        [attributed appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d New", newComments] attributes:mediumColoredDict]];
        [attributed appendAttributedString:[[NSAttributedString alloc] initWithString:@")" attributes:lightDict]];
        
        
        return attributed;
    }
    
    
    
    
}



@end
