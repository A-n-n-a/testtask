//
//  UIView+superCell.m
//  pd2
//
//  Created by i11 on 08/02/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "UIView+superCell.h"

@implementation UIView (superCell)

- (UITableViewCell*)superCell {
    if(!self.superview) {
        return nil;
    }
    
    if ([self.superview isKindOfClass:[UITableViewCell class]]) {
        return (UITableViewCell*)self.superview;
    }
    
    return [self.superview superCell];
}

@end
