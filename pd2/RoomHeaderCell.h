//
//  RoomHeaderCell.h
//  pd2
//
//  Created by i11 on 25/10/15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@interface RoomHeaderCell : AXTableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *roomImageView;
@property (nonatomic, weak) IBOutlet UILabel *roomNameLabel;
@property (nonatomic, weak) IBOutlet UIImageView *arrowImageView;

- (void)updateWithRoomName:(NSString *)roomName;
- (void)updateWithRoomImagePath:(NSString *)roomImagePath;

@end
