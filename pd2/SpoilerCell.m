#import "SpoilerCell.h"

@implementation SpoilerCell

- (void)awakeFromNib
{
    while (self.contentView.gestureRecognizers.count) {
        [self.contentView removeGestureRecognizer:[self.contentView.gestureRecognizers objectAtIndex:0]];
    }
}

@end
