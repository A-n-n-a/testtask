//
//  Admin2Model.h
//  pd2
//
//  Created by i11 on 11/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AXBaseModel.h"

@interface Admin2Model : AXBaseModel

/*
{"id":"1","uid":"63","acl":"1","position":"2","room":"1","chid":"0","chid2":"0","chid3":"0","first_name":"Zero","last_name":"Zero","image":"images/sted/admins/admin_63.png","pass":"","pin":"a670d6ab69d9e49ed40206d7dcd323b2:3FMiReUZvJbMyY87ayaOYQ07uISk8Sxq","log_id":null,"confirm":null,"confirm_email":null,"activate":null,"options":null,"ordering":"0","published":"0","block":"0","pm":"0","homepage_settings":"","username":"admin","email":"a@1a.com","title":"Def1"}
*/

typedef NS_ENUM(NSUInteger, Admin2ModelType) {
    Admin2ModelTypeUnknown = 0,
    Admin2ModelTypeMaster = 1,
    Admin2ModelTypeSuper = 2,
    Admin2ModelTypeRoom = 3,
    Admin2ModelTypeBookkeeping = 5
};

@property (nonatomic, strong) NSNumber *adminId;
@property (nonatomic, strong) NSNumber *uid;
@property (nonatomic, strong) NSNumber *acl;
@property (nonatomic, assign) Admin2ModelType adminType;
@property (nonatomic, strong) NSNumber *position;
@property (nonatomic, strong) NSNumber *room;
@property (nonatomic, strong) NSNumber *chid;
@property (nonatomic, strong) NSNumber *chid2;
@property (nonatomic, strong) NSNumber *chid3;

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imagePath;

- (instancetype)initWithDict:(NSDictionary *)dict;



@end
