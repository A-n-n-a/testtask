//
//  ChildFilterChildShortCell.h
//  pd2
//
//  Created by Andrey on 7/92/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@class ChildFilterModel;

@interface ChildFilterChildShortCell : AXTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *firstAndLastNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dobAndAgeLabel;

@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@property (weak, nonatomic) IBOutlet UIImageView *checkboxImageView;

- (void)updateWithModel:(ChildFilterModel *)model;
- (void)updateImageWithModel:(ChildFilterModel *)model;
- (void)check:(BOOL)check;

@end
