//
//  ChildMoveDateCell.h
//  pd2
//
//  Created by User on 22.03.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChildMoveDateCell : UITableViewCell

@property (nonatomic,retain)IBOutlet UITextField *childMoveDateTextField;

@end
