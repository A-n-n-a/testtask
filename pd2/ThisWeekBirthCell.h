//
//  ThisWeekBirthCell.h
//  pd2
//
//  Created by Eugene Fozekosh on 25.06.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThisWeekBirthCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *birthdaysCollectionView;

@end
