#import "DSLinkedStoryboardSegue.h"

@interface DSLinkedStoryboardSegue()
@property (nonatomic, strong) NSString * type;
@end

@implementation DSLinkedStoryboardSegue

+ (UIViewController *)sceneNamed:(NSString *)identifier
{
    
    NSDictionary * info = [DSLinkedStoryboardSegue infoFromIdentifier:identifier];
    
    NSString * controllerId = info[@"controller"];
    NSString * storyboardId = info[@"storyboard"];
    NSString * type         = info[@"type"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardId
                                                         bundle:nil];
    UIViewController *scene = nil;
    
    if (controllerId.length == 0) {
        scene = [storyboard instantiateInitialViewController];
    }
    else {
        scene = [storyboard instantiateViewControllerWithIdentifier:controllerId];
    }
    
    return scene;
}

+ (NSDictionary *) infoFromIdentifier:(NSString *) identifier
{
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:@"((controller:){1}([a-zA-Z]+)(@){1}(storyboard:){1}([a-zA-Z]+)(@){1}(type:){1}([a-zA-Z]+))" options:0 error:nil];
    NSArray* matches = [regex matchesInString:identifier options:0 range:NSMakeRange(0, [identifier length])];
    NSAssert(matches.count != 0, @"Undefined identifier");
    
    NSTextCheckingResult * match = [matches firstObject];
    NSString * matchString = [identifier substringWithRange:[match range]];
    
    NSArray * info = [matchString componentsSeparatedByString:@"@"];
    NSString * controllerId = [info[0] stringByReplacingOccurrencesOfString:@"controller:" withString:@""];
    NSString * storyboardId = [info[1] stringByReplacingOccurrencesOfString:@"storyboard:" withString:@""];
    NSString * type         = [info[2] stringByReplacingOccurrencesOfString:@"type:" withString:@""];
    return @{
             @"controller": controllerId,
             @"storyboard": storyboardId,
             @"type"      : type
             };
}

- (id)initWithIdentifier:(NSString *)identifier
                  source:(UIViewController *)source
             destination:(UIViewController *)destination
{
    
    return [super initWithIdentifier:identifier
                              source:source
                         destination:[DSLinkedStoryboardSegue sceneNamed:identifier]];
}

- (void)perform
{
    NSString * type = [DSLinkedStoryboardSegue infoFromIdentifier:self.identifier][@"type"];
    
    UIViewController *source = (UIViewController *)self.sourceViewController;
    if ([type isEqualToString:@"modal"]) {
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.destinationViewController];
        [source presentViewController:navigationController animated:YES completion:nil];
    } else {
        [source.navigationController pushViewController:self.destinationViewController
                                               animated:YES];
    }
    
}

@end
