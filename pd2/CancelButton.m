//
//  CancelButton.m
//  pd2
//
//  Created by User on 15.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "CancelButton.h"

@implementation CancelButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
/*
-(void)awakeFromNib
{
    UIImage *image=[self backgroundImageForState:UIControlStateNormal];
    image=[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self setBackgroundImage:image forState:UIControlStateNormal];
    self.tintColor=[[MyColorTheme sharedInstance]buttonCancelBackgroundColor];
    self.titleLabel.textColor=[[MyColorTheme sharedInstance]buttonCancelTextColor];
}
*/
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialSetup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialSetup];
    }
    return self;
}
/*
-(void)awakeFromNib {

    [super initialSetup];
    [self initialSetup];
    
}
*/
-(void)initialSetup {

    [super initialSetup];
    [super makeMeAsCancelButton];

}

@end
