//
//  DailyNotesChildrenNotesListParentSideViewController.h
//  pd2
//
//  Created by Andrey on 30.03.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXTableViewController.h"

@class DailyNotesChildrenParentSideModel;

@interface DailyNotesChildrenNotesListParentSideViewController : AXTableViewController

@property (nonatomic, strong) DailyNotesChildrenParentSideModel *childrenModel;


@end
