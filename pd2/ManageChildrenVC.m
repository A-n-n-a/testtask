//
//  ManageChildrenVC.m
//  pd2
//
//  Created by User on 09.07.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "ManageChildrenVC.h"
#import "VideoView.h"

#define COLLECTION_BUTTON_TAG 100
#define COLLECTION_LABEL_TAG 200


@interface ManageChildrenVC ()

@property NSMutableArray *buttonsArray;

@end

@implementation ManageChildrenVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addHomeButton];
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 300)];
    titleLabel.text = @"Children";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont boldSystemFontOfSize:23];
    self.navigationItem.titleView=titleLabel;
    [AppDelegate delegate].navigationController = self.navigationController;
    
    
    int acl=[[DataManager sharedInstance] getAclForCurrentUser];
    
    
    self.buttonsArray = [@[@"childrenCell"] mutableCopy];
    
    
    
    
    self.view.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];

}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _buttonsArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier=[_buttonsArray objectAtIndex:indexPath.row];
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    if (cell) {
        
        UIButton *button = (UIButton *)[cell.contentView viewWithTag:COLLECTION_BUTTON_TAG];
        UILabel *label = (UILabel *)[cell.contentView viewWithTag:COLLECTION_LABEL_TAG];

        
        if ([cellIdentifier isEqualToString:@"childrenCell"]) {
            label.text = [NetworkManager sharedInstance].isJ3 ? @"All Children" : @"Existing";
            
        }
        
        
        if ([cellIdentifier isEqualToString:@"childrenCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_mainscreen_children_.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"existingsChildrenSquare.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
        }
        
        if ([cellIdentifier isEqualToString:@"movingListCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_mainscreen_moving.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"moving_children_small_icons_new.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
        }
        
        if ([cellIdentifier isEqualToString:@"departedChildCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_mainscreen_departed_.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                label.text = @"Leaving";
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"departedSquare.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                label.text = @"Departed";
            }
        }
        
        if ([cellIdentifier isEqualToString:@"birthdaysCell"]) {
            if ([NetworkManager sharedInstance].isJ3) {
                [button setBackgroundImage:[[UIImage imageNamed:@"j3_mainscreen_child-birthdayst_.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
            else {
                [button setBackgroundImage:[[UIImage imageNamed:@"childrenBirthdays.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
        }
        
        
        
        
        
        if ([cellIdentifier isEqualToString:@"childrenCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageChildrenExisting]) {
            [self buttonAsDisabled:button];
        }
        
        else if ([cellIdentifier isEqualToString:@"departedChildCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageChildrenDeparted]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"waitingListCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageChildrenWaitingList]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"waitingCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageChildrenWaitingList]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"movingListCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageChildrenMoving]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"authorisedListCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageChildrenAuthorised]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"nappyMonitorCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageChildrenNappies]) {
            [self buttonAsDisabled:button];
        }
        else if ([cellIdentifier isEqualToString:@"birthdaysCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASManageBirthdays]) {
            [self buttonAsDisabled:button];
        }
    }

    
    return cell;
}




- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    NSLog(@"Identifier: %@", identifier);
    
    if ([identifier isEqualToString:@"children@Manage_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageChildrenExisting]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"DepartedList@DepartedChildren_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageChildrenDeparted]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"list@WaitingList_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageChildrenDeparted]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"list@MovingChildren_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageChildrenMoving]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"list@Authorised_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageChildrenAuthorised]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"nappySegue"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageChildrenNappies]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"childrenBirthdays@Manage_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASManageBirthdays]) {
        [self showUnavailableAlert];
        return NO;
    }
    
    else {
        return YES;
    }
    
    
    
}

@end
