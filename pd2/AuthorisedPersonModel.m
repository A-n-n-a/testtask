//
//  AuthorisedPersonModel.m
//  pd2
//
//  Created by Andrey on 24/01/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AuthorisedPersonModel.h"
#import "RoomModel.h"

@implementation AuthorisedPersonModel

/*
 {
 "id": "2",
 "child_id": "17",
 "first_name": "Bilbo",
 "last_name": "Baggins",
 "relationship": "Uncle",
 "signature": "1",
 "parent_to_sign": "1",
 "signature_image": "",
 "password": "12345",
 "photograpth_type": "0",
 "photograph": "",
 "authorisation": "call",
 "agreement_required": "0",
 "parent_1_signature": null,
 "parent_1_name": null,
 "parent_1_datetime": null,
 "parent_2_signature": null,
 "parent_2_name": null,
 "parent_2_datetime": null,
 "details": "Everything is fine",
 "notes": "Everything is fine",
 "date_added": "2016-11-29 12:36:38"
 }
 */


- (instancetype)initWithDict:(NSDictionary *)dict {
    
    self = [super init];
    if (self) {
        
        self.personId = [self safeNumber:dict[@"id"]];
        self.firstName = [self safeStringAndDecodingHTML:dict[@"first_name"]];
        self.lastName = [self safeStringAndDecodingHTML:dict[@"last_name"]];
        self.relationship = [self safeStringAndDecodingHTML:dict[@"relationship"]];
        self.signatureRequired = [self safeNumber:dict[@"signature"]];
        self.signatureNow = [self safeNumber:dict[@"parent_to_sign"]];
        self.signaturePath = [self safeString:dict[@"signature_image"]];
        self.password = [self safeString:dict[@"password"]];
        
        NSNumber *photoType = [self safeNumber:dict[@"photograpth_type"]];
        
        if (!photoType) {
            self.photoType = AuthorisedPersonPhotoTypeNo;
        }
        else if ([photoType intValue] == 0) {
            self.photoType = AuthorisedPersonPhotoTypeNo;
        }
        else if ([photoType intValue] == 1) {
            self.photoType = AuthorisedPersonPhotoTypeUploadNow;
        }
        else if ([photoType intValue] == 2) {
            self.photoType = AuthorisedPersonPhotoTypeUploadByParent;
        }
        self.photoPath = [self safeString:dict[@"photograph"]];
        
        
        NSString *authType = [self safeString:dict[@"authorisation"]];
        
        if (!authType) {
            self.authorisationType = AuthorisedPersonAuthorisationTypeEmpty;
        }
        else if ([authType isEqualToString:@"call"]) {
            self.authorisationType = AuthorisedPersonAuthorisationTypeByCall;
        }
        else if ([authType isEqualToString:@"password"]) {
            self.authorisationType = AuthorisedPersonAuthorisationTypeByPassword;
        }
        else if ([authType isEqualToString:@"photo"]) {
            self.authorisationType = AuthorisedPersonAuthorisationTypeByPhoto;
        }
        else if ([authType isEqualToString:@"signature"]) {
            self.authorisationType = AuthorisedPersonAuthorisationTypeBySignature;
        }
        
        
        
        self.parent1Name = [self safeString:dict[@"parent_1_name"]];
        self.parent1SignPath = [self safeString:dict[@"parent_1_signature"]];
        self.parent1SignDate = [self dateFrom:dict[@"parent_1_datetime"] withDateFormat:@"yyyy-MM-dd HH:mm:ss"];

        self.parent2Name = [self safeString:dict[@"parent_2_name"]];
        self.parent2SignPath = [self safeString:dict[@"parent_2_signature"]];
        self.parent2SignDate = [self dateFrom:dict[@"parent_2_datetime"] withDateFormat:@"yyyy-MM-dd HH:mm:ss"];

        self.details = [self safeString:dict[@"details"]];
        self.notes = [self safeString:dict[@"notes"]];
        
        self.addedDate = [self dateFrom:dict[@"date_change"] withDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    return self;
    
}

- (BOOL)isAlert {
    
    if (self.photoType != AuthorisedPersonPhotoTypeNo && !self.photoPath) {
        return YES;
    }
    
    BOOL signatureRequired = self.signatureRequired && [self.signatureRequired boolValue];
    if (signatureRequired && !self.signaturePath) {
        return YES;
    }
    
    return NO;
    
}


-(id)copyWithZone:(NSZone *)zone
{
    AuthorisedPersonModel *copy = [[AuthorisedPersonModel alloc] init];
    copy.personId = [self.personId copyWithZone:zone];
    copy.firstName = [self.firstName copyWithZone:zone];
    copy.lastName = [self.lastName copyWithZone:zone];
    copy.relationship = [self.relationship copyWithZone:zone];
    copy.signatureRequired = [self.signatureRequired copyWithZone:zone];
    copy.signatureNow = [self.signatureNow copyWithZone:zone];
    copy.signaturePath = [self.signaturePath copyWithZone:zone];
    copy.password = [self.password copyWithZone:zone];
    copy.photoType = self.photoType;
    copy.photoPath = [self.photoPath copyWithZone:zone];
    copy.authorisationType = self.authorisationType;

    copy.parent1Name = [self.parent1Name copyWithZone:zone];
    copy.parent1SignPath = [self.parent1SignPath copyWithZone:zone];
    copy.parent1SignDate = [self.parent1SignDate copyWithZone:zone];
    
    copy.parent2Name = [self.parent2Name copyWithZone:zone];
    copy.parent2SignPath = [self.parent2SignPath copyWithZone:zone];
    copy.parent2SignDate = [self.parent2SignDate copyWithZone:zone];
    
    copy.details = [self.details copyWithZone:zone];
    copy.notes = [self.notes copyWithZone:zone];
    copy.addedDate = [self.addedDate copyWithZone:zone];
    
    return copy;
}


- (BOOL)isExist {
    
    if (self.firstName && self.lastName && self.personId) {
        return YES;
    }
    
    return NO;
    
}




@end
