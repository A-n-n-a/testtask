//
//  NSArray+JSON.m
//  pd2
//
//  Created by Denis Skripnichenko on 26/10/15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import "NSArray+JSON.h"

@implementation NSArray (JSON)
- (NSString *) toJSON
{
    NSError  * error;
    NSData   * jsonData;
    
    jsonData = [NSJSONSerialization dataWithJSONObject:self
                                               options:0
                                                 error:&error];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}
@end
