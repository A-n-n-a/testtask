//
//  AOLinkedStoryboardSegue.h
//  pd2
//
//  Created by i11 on 02/02/15.
//  Copyright (c) http://spin.atomicobject.com/2014/03/06/multiple-ios-storyboards/
//

#import <UIKit/UIKit.h>

@interface AOLinkedStoryboardSegue : UIStoryboardSegue

+ (UIViewController *)sceneNamed:(NSString *)identifier;

@end
