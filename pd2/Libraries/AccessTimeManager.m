//
//  AccessTimeManager.m
//  pd2
//
//  Created by Andrey on 30.06.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AccessTimeManager.h"


#define CHECK_TIME_INTERVAL 2


@interface AccessTimeManager ()

@property (nonatomic, strong) NSNumber *timeLeft;
@property (nonatomic, strong) NSDate *expireDate;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) BOOL alertIsShows;

@end

@implementation AccessTimeManager


+ (AccessTimeManager *)sharedManager {
    
    static AccessTimeManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[AccessTimeManager alloc] init];
    });
    
    return manager;
}


- (void)startWithSeconds:(long long int)seconds {
    
    
    if (seconds <= 0 ) {
        [self disableTimer];
        return;
    }
    
    self.alertIsShows = NO;
    

    self.expireDate = [[NSDate date] dateByAddingTimeInterval:seconds];
    NSLog(@"expired date: %@", self.expireDate);
    self.timer = [NSTimer scheduledTimerWithTimeInterval:CHECK_TIME_INTERVAL
                                                  target:self
                                                selector:@selector(checkTime)
                                                userInfo:nil
                                                 repeats:YES];
    
}


- (void)disableTimer {
    
    [self.timer invalidate];
    self.expireDate = nil;
    self.timer = nil;

    
}

- (void)checkTime {
    
    NSLog(@"AccessTime timer");
    
    if (!self.expireDate) {
        return;
    }
    
    //long long int leftTime = [self.timeLeft longLongValue] - CHECK_TIME_INTERVAL;
    //self.timeLeft = [NSNumber numberWithLongLong:leftTime];
    
    long long int leftTime = [self.expireDate timeIntervalSinceDate:[NSDate date]];
    
    NSLog(@"left time: %d", leftTime);
    
    if (leftTime <= 0) {
        [self disableTimer];
        [AppDelegate logoutAndSendDeviceTokenIs:YES];
    }
    else if (leftTime <= 5*60) {
        if (!self.alertIsShows) {
            self.alertIsShows = YES;
            
            int minutes = (int)ceil((float)leftTime/60.f);
            NSString *alertMessage = [NSString stringWithFormat:@"The session will expire in %i minutes", minutes];
            
            Alert(@"Warning", alertMessage);
        }
    }
    
}



@end
