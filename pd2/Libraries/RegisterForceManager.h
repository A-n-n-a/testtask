//
//  RegisterForceManager.h
//  pd2
//
//  Created by i11 on 05/03/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RegisterForceManager : NSObject

@property (strong, nonatomic) NSMutableArray *registerChildrens;

+ (RegisterForceManager *)sharedManager;

- (void)updateWithRegisterArray:(NSArray *)registerArray;
- (void)showAlert;

@end
