//
//  DataStateManager.h
//  pd2
//
//  Created by i11 on 05/03/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    DataStateSettingsDailyChecklist,
    DataStateSettingsActions,
    DataStateSettingsRiskAreas,
    DataStateSettingsNotes,
    DataStateVisitorsSingle,
    DataStateVisitorsRegular,
    DataStateGalleryVideoUpdate,
    DataStateGalleryPhotoUpdate,
    DataStateMedicalMedications,
    DataStateMedicalLongTerm,
    DataStateMedicalShortTerm,
    DataStateAccidentIncident,
    DataStatePSAccidentIncident,
    DataStateMedicalOverview,
    DataStateAccidentIncidentConcernTreatments,
    DataStateConcerns,
    DataStateVaccinationsImmunisations,
    DataStateChildrensVaccinations,
    DataStateBookkeepingSettings,
    DataStateDiaryCalendar,
    DataStateDiaryOverview,
    DataStateDiaryView,
    DataStateLocationList,
    DataStateRegisterMonth,
    DataStatePermissions,
    DataStatePermissionsCategories,
    DataStatePermissionsGroups,
    DataStatePermissionsAssigned,
    DataStatePolicies,
    DataStatePoliciesCategories,
    DataStatePoliciesGroups,
    DataStatePoliciesAssigned,
    DataStateDepartedChildrenSign,
    DataStateDepartedChildrenAdd,
    DataStateWaitingChildrenAdd,
    DataStateMovingChildrenAdd,
    DataStateAuthorisedPersonAdd,
    DataStateAuthorisedPersonSign,
    DataStateIncluded,
    DataStateNappies,
    DataStateSleeping,
    DataStateBottles,
    DataStateDiaryDuplicationsAdd,
    DataStatePrivateMessages,
    DataStateGeneralNotes,
    DataStateAdmins,
    DataStateDailyNotes


} DataStateList;

@interface DataStateManager : NSObject

@property (strong, nonatomic) NSMutableDictionary *states; //Словарь состояний
@property (strong, nonatomic) NSMutableDictionary *controllersForStates; //Словарь состояний контроллеров

+ (DataStateManager *)sharedManager;

//Изменены?
- (BOOL)isChanged:(DataStateList)target forViewController:(id)viewController;

//Изменили
- (void)changed:(DataStateList)target;

//Обновили
- (void)updated:(DataStateList)target forViewController:(id)viewController;

@end
