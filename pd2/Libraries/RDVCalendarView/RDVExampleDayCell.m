// RDVExampleDayCell.m
// RDVCalendarView
//
// Copyright (c) 2013 Robert Dimitrov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "RDVExampleDayCell.h"

@implementation RDVExampleDayCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        _notificationView = [[UIView alloc] init];
        [_notificationView setBackgroundColor:[[MyColorTheme sharedInstance]buttonConfirmBackgroundColor]];
        [_notificationView setHidden:YES];
        [self.contentView addSubview:_notificationView];
        
        _pendingView = [[UIView alloc]init];
        _pendingView.backgroundColor=[UIColor orangeColor];
        _pendingView.hidden=YES;
        [self.contentView addSubview:_pendingView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGSize viewSize = self.contentView.frame.size;
    
    CGSize nSize=CGSizeMake(30.0, 5.0);
    [[self notificationView] setFrame:CGRectMake(viewSize.width/2-nSize.width/2, viewSize.height*0.75-nSize.height/2, nSize.width, nSize.height)];
    self.notificationView.layer.cornerRadius=2.5;
    
    CGSize orangeSize=CGSizeMake(30.0, 5.0);
    _pendingView.frame=CGRectMake(viewSize.width/2-orangeSize.width/2, viewSize.height*0.75-orangeSize.height/2, orangeSize.width, orangeSize.height);
    _pendingView.layer.cornerRadius=2;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    [[self notificationView] setHidden:YES];
    self.pendingView.hidden=YES;
}

@end
