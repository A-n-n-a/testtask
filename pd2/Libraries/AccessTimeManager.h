//
//  AccessTimeManager.h
//  pd2
//
//  Created by Andrey on 30.06.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Singleton.h"

@interface AccessTimeManager : Singleton

- (void)startWithSeconds:(long long int)seconds;
- (void)disableTimer;

@end
