//
//  DataStateManager.m
//  pd2
//
//  Created by i11 on 05/03/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "DataStateManager.h"

@implementation DataStateManager

+ (DataStateManager *)sharedManager {
    
    static DataStateManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DataStateManager alloc] init];
    });
    
    return manager;
}

- (id)init {
    
    if (self = [super init]) {
        self.states = [[NSMutableDictionary alloc] init];
        self.controllersForStates = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}


- (BOOL)isChanged:(DataStateList)target forViewController:(id)viewController {
    
    [self storeViewController:viewController forTarget:target];
    
    NSString *targetAndViewController = [self keyForTarget:target andViewController:viewController];
    
    NSNumber *isChanged = [self.states objectForKey:targetAndViewController];
    
    [self LogDebug];
    
    if (isChanged && [isChanged boolValue] == YES) {
        return YES;
    } else {
        return NO;
    }
}


- (void)changed:(DataStateList)target {
    
    NSNumber *isChanged = [NSNumber numberWithBool:YES];
    NSString *targetName = [NSString stringWithFormat:@"%i", target];
    
    NSSet *viewControllersSet = [self.controllersForStates objectForKey:targetName];
    
    for (NSString *viewControllerName in viewControllersSet) {
        NSString *targetAndViewController = [self keyForTarget:target andViewControllerName:viewControllerName];
        [self.states setObject:isChanged forKey:targetAndViewController];
    }
    
    [self LogDebug];

}

- (void)updated:(DataStateList)target forViewController:(id)viewController {
    
    [self storeViewController:viewController forTarget:target];
    
    NSString *targetAndViewController = [self keyForTarget:target andViewController:viewController];
    
    NSNumber *isChanged = [NSNumber numberWithBool:NO];
    [self.states setObject:isChanged forKey:targetAndViewController];
    
    [self LogDebug];

}


#pragma mark -

- (void)storeViewController:(id)viewController forTarget:(DataStateList)target {
    
    NSString *className = NSStringFromClass([viewController class]);
    NSString *targetName = [NSString stringWithFormat:@"%i", target];

    NSMutableSet *viewControllersSet = [self.controllersForStates objectForKey:targetName];
    
    if (!viewControllersSet) {
        viewControllersSet = [[NSMutableSet alloc] init];
    }
    
    [viewControllersSet addObject:className];
    
    [self.controllersForStates setObject:viewControllersSet forKey:targetName];
    
    
}

//для класса
- (NSString *)keyForTarget:(DataStateList)target andViewController:(id)viewController {

    NSString *className = NSStringFromClass([viewController class]);
    
    return [NSString stringWithFormat:@"%i-%@", target, className];
}

//для строки
- (NSString *)keyForTarget:(DataStateList)target andViewControllerName:(NSString *)viewControllerName {

    return [NSString stringWithFormat:@"%i-%@", target, viewControllerName];
}

#pragma mark - 

- (void)LogDebug {
    NSLog(@"nsset: %@", self.controllersForStates);
    NSLog(@"states: %@", self.states);
}

@end
