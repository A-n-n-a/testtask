//
//  DailyNotesChildrenNoteParentSideCell.h
//  pd2
//
//  Created by Andrey on 30.03.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXTableViewCell.h"

@class DailyNotesChildrenNoteParentSideModel;

@interface DailyNotesChildrenNoteParentSideCell : AXTableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *adminNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *commentsLabel;


@property (weak, nonatomic) IBOutlet UIView *toolbarView;
@property (weak, nonatomic) IBOutlet UIView *toolbar1View;
@property (weak, nonatomic) IBOutlet UIView *toolbar2View;
@property (weak, nonatomic) IBOutlet UIView *toolbar3View;

@property (weak, nonatomic) IBOutlet IconButton *deleteButton;



- (void)updateWithModel:(DailyNotesChildrenNoteParentSideModel *)model;


@end
