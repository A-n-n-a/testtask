//
//  ProfileModel.h
//  pd2
//
//  Created by Andrey on 05/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXBaseModel.h"

@interface ProfileModel : AXBaseModel

/*
 {
 "id": "269",
 "chid": "63",
 "idname": "id17",
 "helperTitle": "Ethnic Origin",
 "content": "WHB - White - British;WHR - White - Irish;WHA - Any Other White Background;MWB - White and Black Caribbean;MBA - White and Black African;MWA - White and Asian;MOT - Any Other Mixed Background;AIN - Indian;APK - Pakistani;ABA - Bangladeshi;AAO - Any Other Asian Background ;BLB - Caribbean;BLF - African;BLG - Any Other Black Background;CHE - Chinese;OEO - Any Other Ethnic Group",
 "title": "WHA - Any Other White Background",
 "type": "2",
 "ordering": "5"
 }
*/

typedef NS_ENUM(NSUInteger, ProfileModelType) {
    ProfileModelTypeSingleLine,
    ProfileModelTypeMultiLine,
    ProfileModelTypeList,
    ProfileModelTypeUnknown
};

@property (nonatomic, assign) ProfileModelType profileType;


@property (nonatomic, strong) NSString *profileId;
@property (nonatomic, strong) NSString *helperTitle;

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSString *value;

@property (nonatomic, assign) int order;


- (instancetype)initWithDict:(NSDictionary *)dict isFromChild:(BOOL)isFromChild;


@end
