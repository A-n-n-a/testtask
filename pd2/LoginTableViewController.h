//
//  LoginTableViewController.h
//  pd2
//
//  Created by i11 on 21/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Pd2TableViewController.h"

@class User, IconButton, LoginLogoCell;

@interface LoginTableViewController : Pd2TableViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
    UIView *onePxLine;
}

@property (weak, nonatomic) UIImageView *logoImageView;
@property (weak, nonatomic) UITextField *loginTextField;
@property (weak, nonatomic) UIPickerView *picker;
@property (weak, nonatomic) UITextField *passwordTextField;
@property (weak, nonatomic) UITextField *domainTextField;
@property (weak, nonatomic) UIView *domainView;



@property (weak, nonatomic) LoginLogoCell *logoCell;
@property (weak, nonatomic) UITableViewCell *loginCell;
@property (weak, nonatomic) UITableViewCell *pickerCell;
@property (weak, nonatomic) UITableViewCell *passwordCell;
@property (weak, nonatomic) UITableViewCell *domainCell;
@property (weak, nonatomic) UITableViewCell *submitCell;
@property (weak, nonatomic) UITableViewCell *optionsCell;

@property (weak, nonatomic) IconButton *eyeButton;
@property (weak, nonatomic) UIButton *submitButton;
@property (weak, nonatomic) UIButton *blueb;
@property (weak, nonatomic) UIButton *yellowb;

@property (strong, nonatomic) User *activeUser;

@property (assign, nonatomic) BOOL addNewUser; //флаг для передачи из Login with password

- (void)blueAction:(UIButton *)sender;
- (void)yellowAction:(UIButton *)sender;
- (IBAction)showPasswordAction:(id)sender;
- (IBAction)forgotPasswordAction:(id)sender;
- (IBAction)signUpAction:(id)sender;

- (IBAction)selectThemeAction:(UIButton *)sender;
@end
