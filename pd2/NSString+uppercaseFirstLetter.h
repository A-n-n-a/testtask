//
//  NSString+uppercaseFirstLetter.h
//  pd2
//
//  Created by Andrey on 22/01/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (uppercaseFirstLetter)


- (NSString *)uppercaseFirstLetter;

@end
