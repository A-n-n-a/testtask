//
//  AXTableViewCell.h
//  pd2
//
//  Created by i11 on 08/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AXDataCheck.h"
#import "Buttons.h"
#import "IconButton.h"

#import "UIImageView+AFNetworking.h"

#import "MyColorTheme.h"

#import "NSDate+Printable.h"

#import "NSString+Utils.h"
#import "NSString+uppercaseFirstLetter.h"

#define CONTAINER_VIEW_TAG 250
#define ACTIVITY_INDICATOR_VIEW_TAG 260

static inline NSString* emptyStringIfNil(NSString *s) {
    return s ? s : @"";
}

static inline NSString* notAvailStringIfNil(NSString *s) {
    return s && [s length] ? s : @"N/A";
}

static inline NSString* stringFromBool(BOOL b) {
    return b ? @"Yes" : @"No";
}

static inline BOOL hasLeadingNumberInString(NSString* s) {
    if (s)
        return [s length] && isnumber([s characterAtIndex:0]);
    else
        return NO;
}


@interface AXTableViewCell : UITableViewCell

@property (nonatomic, assign) BOOL isScottish; //dynamic


- (UIView *)containerView;

//Image
-(void)updateProfileImageView:(UIImageView *)imageView
                withImagePath:(NSString *)imagePath
          andDefaultImagePath:(NSString *)defaultImagePath
    downloadImageCompletition:(void(^)(UIImage *downloadedImage))downloadImageCompletion;

- (void)decorateProfileView:(UIView *)view;

//ActivityIndicator
- (void)activityIndicatorSetStatus:(BOOL)status;
- (void)addActivityIndicatorToView:(UIView *)destinationView withAnimation:(BOOL)animation;
- (void)removeActivityIndicatorFromView:(UIView *)destinationView;


- (NSString *)stringFromDate:(NSDate *)date withDateFormat:(NSString *)dateFormat;
- (NSString *)monthsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate;
- (NSString *)monthsFromDate:(NSDate *)startDate;
- (NSString *)fullNameFromFirstName:(NSString *)firstName
                        andLastName:(NSString *)lastName;

+ (NSString *)stringFromDate:(NSDate *)date withDateFormat:(NSString *)dateFormat;
+ (NSString *)monthsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate;
+ (NSString *)monthsFromDate:(NSDate *)startDate;
+ (NSString *)fullNameFromFirstName:(NSString *)firstName
                        andLastName:(NSString *)lastName;


@end
