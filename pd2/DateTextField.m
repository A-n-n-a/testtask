#import "DateTextField.h"
#import "DSDatePicker.h"
#import "NSDateFormatter+Convert.h"
#import "NSDate+Utilities.h"
#import "NSDate+Printable.h"

@interface DateTextField()
    @property (strong, nonatomic) DSDatePicker * date_picker;
    @property (nonatomic) BOOL active;
    @property (nonatomic) BOOL isOpened;
@end

@implementation DateTextField

- (id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self baseInit];
    }
    return self;
}

- (id) init
{
    if (self = [super init]) {
        [self baseInit];
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self baseInit];
    }
    return self;
}

- (void) baseInit
{
    self.delegate = self;
    self.type     = kDatePickerTypeAll;
    self.active   = YES;
    self.isOpened = NO;
    self.en_USLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
}

- (void) setType:(kDatePickerType)type date:(NSString *)date
{
    _type = type;
    [self.date_picker setDateType:_type];
    
    switch (type) {
        case kDatePickerTypeAll:
            [self setDateAll:date];
            break;
        case kDatePickerTypeYearMonth:
            [self setDateYearMonth:date];
            break;
        case kDatePickerTypeYear:
            [self setDateYear:date];
            break;
    }
}

- (void) typeForDateObject:(NSDate *)date
{
    _type = kDatePickerTypeAllDate;
    [self.date_picker setDateType:_type];
    
    [self setDateAllFromDate:date];
}



- (void) setDateAll:(NSString *)date
{
    NSArray * dateComponents = [date componentsSeparatedByString:@"-"];
    self.year  = [NSNumber numberWithInt:[dateComponents[0] intValue]];
    self.month = [NSNumber numberWithInt:[dateComponents[1] intValue]];
    self.day   = [NSNumber numberWithInt:[dateComponents[2] intValue]];
    [self setText: [NSDateFormatter convertDateFromString:date fromFormat:@"yyyy-MM-dd" toFormat:@"dd.MM.yyyy"]];
    [self sendActionsForControlEvents:UIControlEventEditingChanged];
}


- (void) setDateAllFromDate:(NSDate *)date
{
    //NSArray * dateComponents = [date componentsSeparatedByString:@"-"];
    
    self.year  = [NSNumber numberWithInt:(int)[date year]];
    self.month = [NSNumber numberWithInt:(int)[date month]];
    self.day   = [NSNumber numberWithInt:(int)[date day]];
    [self setText:[date printableDefaultInput]];
    self.itsDate = date;
    [self sendActionsForControlEvents:UIControlEventEditingChanged];
}


- (void) setDateYearMonth:(NSString *)date
{
    NSArray * dateComponents = [date componentsSeparatedByString:@"-"];
    self.year  = [NSNumber numberWithInt:[dateComponents[0] intValue]];
    self.month = [NSNumber numberWithInt:[dateComponents[1] intValue]];
    self.day   = @(0);
    
    date = [NSDateFormatter convertDateFromString:date fromFormat:@"yyyy-MM-dd" toFormat:@"MMMM, yyyy"];
    self.text = date;
    [self sendActionsForControlEvents:UIControlEventEditingChanged];
}

- (void) setDateYear:(NSString *)date
{
    NSArray * dateComponents = [date componentsSeparatedByString:@"-"];
    self.year  = [NSNumber numberWithInt:[dateComponents[0] intValue]];
    self.month = @(0);
    self.day   = @(0);
    
    date = [NSDateFormatter convertDateFromString:date fromFormat:@"yyyy-MM-dd" toFormat:@"yyyy"];
    [self setText: date];
    [self sendActionsForControlEvents:UIControlEventEditingChanged];
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
    [self show:NO];
    
    if (self.textDelegate) {
        [self.textDelegate dateTextFieldTap:self];
    }
    
    return NO;
}


- (void) datePickerAll
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setLocale:self.en_USLocale];
    formatter.dateFormat=@"yyyy-MM-dd";
    
    __weak typeof(self) weakSelf = self;
    
    self.date_picker = [[DSDatePicker alloc]init];
    [self.date_picker setDateType:self.type];
    [self.date_picker setDateHandler:^(NSDate *newDate) {
        [weakSelf onHide];
        NSString *datestr=[formatter stringFromDate:newDate];
        weakSelf.userInteractionEnabled = YES;
        [weakSelf setDateAll: datestr];
    }];
    [self.date_picker setFinishHandler:^{
        [weakSelf onHide];
        weakSelf.userInteractionEnabled = YES;
    }];
    [self.date_picker show];
}


- (void) datePickerAllDate
{
    __weak typeof(self) weakSelf = self;
    
    self.date_picker = [[DSDatePicker alloc]init];
    [self.date_picker setDateType:self.type];
    [self.date_picker setDateHandler:^(NSDate *newDate) {
        [weakSelf onHide];
        //NSString *datestr=[formatter stringFromDate:newDate];
        weakSelf.userInteractionEnabled = YES;
        [weakSelf setDateAllFromDate:newDate];
    }];
    [self.date_picker setFinishHandler:^{
        [weakSelf onHide];
        weakSelf.userInteractionEnabled = YES;
    }];
    [self.date_picker show];
}



- (void) datePickerYear
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setLocale:self.en_USLocale];
    formatter.dateFormat=@"yyyy-MM-dd";
    
    __weak typeof(self) weakSelf = self;

    self.date_picker = [[DSDatePicker alloc]init];
    [self.date_picker setDateType:self.type];
    [self.date_picker setDateHandler:^(NSDate *newDate) {
        [weakSelf onHide];
        NSString *datestr=[formatter stringFromDate:newDate];
        weakSelf.userInteractionEnabled = YES;
        [weakSelf setDateYear:datestr];
    }];
    [self.date_picker setFinishHandler:^{
        [weakSelf onHide];
        weakSelf.userInteractionEnabled = YES;
    }];
    [self.date_picker show];
}

- (void) datePickerYearMonth
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setLocale:self.en_USLocale];
    formatter.dateFormat=@"yyyy-MM-dd";
    
    __weak typeof(self) weakSelf = self;

    self.date_picker = [[DSDatePicker alloc]init];
    [self.date_picker setDateType:self.type];
    
    
    [self.date_picker setDateHandler:^(NSDate *newDate) {
        NSString * datestr = [formatter stringFromDate:newDate];
        [weakSelf onHide];
        [weakSelf setDateYearMonth:datestr];
    }];
    [self.date_picker setFinishHandler:^{
        [weakSelf onHide];
        weakSelf.userInteractionEnabled = YES;
    }];
    
    
    [self.date_picker show];
}

- (void) setActive:(BOOL)active
{
    _active = active;
}

- (void) show:(BOOL)force
{
    if ((!_active && !force) || _isOpened) {
        return;
    }
    
    _isOpened = YES;
    
    switch (self.type) {
        case kDatePickerTypeYear:
            [self datePickerYear];
            break;
        case kDatePickerTypeYearMonth:
            [self datePickerYearMonth];
            break;
            
        case kDatePickerTypeAll:
            [self datePickerAll];
            break;
        case kDatePickerTypeAllDate:
            [self datePickerAllDate];
            break;
    }
}

- (void) onHide
{
    //weakSelf.userInteractionEnabled = YES;
    _isOpened = NO;
}

@end
