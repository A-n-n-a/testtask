//
//  AlertMessageHandler.h
//  pd2
//
//  Created by Andrey on 02/08/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlertMessageHandler : NSObject
+ (void)showAlertForMsg:(NSString *)msgString;

@end
