//
//  UserManager.m
//  pd2
//
//  Created by i11 on 25/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "UserManager.h"
#import "User.h"
#import "NetworkManager.h"

NSString* const AllUsersHasBeenRecivePinAvailabilityNotification = @"AllUsersHasBeenRecivePinAvailabilityNotification";

@interface UserManager ()

@property (strong, nonatomic) NSUserDefaults *standardUserDefaults;
@property (strong, nonatomic) NSArray *responcedUserList;


@end

@implementation UserManager

+ (UserManager *)sharedManager {
    
    static UserManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[UserManager alloc] init];
    });
    
    return manager;
}

- (id)init {
    
    if (self = [super init]) {
        
        
        
    }
    
    return self;
}

- (void)readUsersAndRecievePin {
    
    self.standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    self.users = [self readLocalUsers];
    
    [self recieveAndFillPinAvailabilityForAllUsers];
}


#pragma mark - Read and save local users

- (NSMutableArray *)readLocalUsers {

    NSMutableArray *array = [[NSMutableArray alloc] init];

    NSArray *localUsersArray = [self.standardUserDefaults objectForKey:@"users"];
    for (NSDictionary *userInfo in localUsersArray) {
        User *user = [[User alloc] initWithLocalUserInfo:userInfo andDelegate:self];
        [array addObject:user];
    }
    
    return array;
}

- (void)saveLocalUsers {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];

    for (User *user in self.availableUsers) {
        [array addObject:[user userInfoForLocalSave]];
    }
    
    [self.standardUserDefaults setObject:(NSArray *)array forKey:@"users"];
    [self.standardUserDefaults synchronize];
}


#pragma mark -

- (void)deleteUserWithId:(int)uid andSubdomain:(NSString *)subdomain {
    
    __block NSInteger deleteIndex = NSIntegerMax;
    
    [self.users enumerateObjectsUsingBlock:^(User *user, NSUInteger idx, BOOL *stop) {
        if (user.uid == uid && [user.subDomain isEqual:subdomain]) {
            deleteIndex = idx;
            *stop = YES;
        }
    }];
    
    if (deleteIndex != NSIntegerMax) {
        [self.users removeObjectAtIndex:deleteIndex];
        [self saveLocalUsers];
    }
    
    
}


#pragma mark - 

- (User *)addUserWithUserName:(NSString *)userName
                          uid:(int)uid
                    subDomain:(NSString *)subDomain
                          acl:(int)acl
                        store:(BOOL)isForStoring
         isSuccessfullyLogged:(BOOL)isSuccessfullyLogged {
    
    User *user = [self getUserByUID:uid andSubdomain:subDomain];
    if (!user) {
        
        NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
        [userInfo setObject:[NSNumber numberWithInt:uid] forKey:@"uid"];
        [userInfo setObject:[NSNumber numberWithInt:acl] forKey:@"acl"];
        [userInfo setObject:userName forKey:@"userName"];
        [userInfo setObject:subDomain forKey:@"subDomain"];
        [userInfo setObject:[NSNumber numberWithBool:isForStoring] forKey:@"isForStoring"];
        [userInfo setObject:[NSNumber numberWithBool:YES] forKey:@"isNewUser"];

        user = [[User alloc] initWithLocalUserInfo:userInfo andDelegate:self];
        [self.users addObject:user];
        
        [self recieveAndFillPinAvailabilityForAllUsers];
        
    } else {
        
        user.acl = acl;
        user.subDomain = subDomain;
        user.isForStoring = isForStoring;
        
        //В случае очистки списка юзеров на сервере - uid меняется. Обновляем его
        //Можно так же запустить повторную проверку пинов
#warning протестировать!
        if (user.uid != uid) {
            user.uid = uid;
            user.pinAvailabilityRecived = NO;
            [user recieveAndFillPinAvailability];
        }
        
        
    }
    
    if (isSuccessfullyLogged) {
        [self successfullyLoggedUser:user];
    }
    
    [self saveLocalUsers];

    return user;
}

- (void)successfullyLoggedUser:(User *)user {
    
    if (user.isForStoring) {
        for (User *user in self.users) {
            user.isLastLogged = NO;
        }
        user.isLastLogged = YES;
    }
    
    self.loggedUser = user;
    
    user.isPinFailedAttempt = NO;
    
    //start fetch and update info about users
    if (user.userType == UserTypeAdmin) {
        [self reciveAndUpdateUsersWithRemoteUserInfo];
    }
    else {
        [self reciveAndUpdateUserWithRemoteUserInfo];
    }
    
    
    
}

- (void)successfullyLogout {

    self.loggedUser = nil;

}


- (User *)getUserByUserName:(NSString *)userName andSubdomain:(NSString *)subdomain {
    
    for (User *user in self.users) {
        if ([[user.userName lowercaseString] isEqualToString:[userName lowercaseString]] && [[user.subDomain lowercaseString] isEqualToString:[subdomain lowercaseString]]){
            return user;
        }
    }
    return  nil;
}


- (User *)getUserByUID:(int)uid andSubdomain:(NSString *)subdomain {
    
    for (User *user in self.users) {
        if (user.uid == uid && [user.subDomain isEqualToString:subdomain]){
            return user;
        }
    }
    return  nil;
}


- (User *)getUserWithTouchId {
    
    for (User *user in self.availableUsers) {
        if (user.isTouchId){
            //Можно добавить проверку по наличия token
            return user;
        }
    }
    return  nil;
}


- (void)setTouchIdEnableForUser:(User *)user {
    
    for (User *user in self.availableUsers) {
        user.isTouchId = NO;
        user.touchIdToken = nil;
    }
    user.isTouchId = YES;
}


- (void)clearNewUserFlag {
    
    for (User *user in self.availableUsers) {
        user.isNewUser = NO;
    }
    
}

#pragma mark - getter & setters

- (User *)lastLoggedUser {
    
    for (User *user in self.users) {
        if (user.isLastLogged) {
            return user;
        }
    }
    
    return nil;
}


- (BOOL)isAllUsersRecivePinAvailability {
    
    for (User *user in self.availableUsers) {
        if (!user.pinAvailabilityRecived) {
            return NO;
        }
    }
    
    return YES;

}


- (NSArray *)usersWithPin {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (User *user in self.availableUsers) {
        if (user.havePin && !user.isPinFailedAttempt) {
            [array addObject:user.userName];
        }
    }
    
    return (NSArray *)array;
}


- (BOOL)isUsersWithPin {
    
    if ([[self usersWithPin] count] > 0) {
        return  YES;
    }
    
    return NO;
}

- (BOOL)isUsersWithTouchId {
    
    for (User *user in self.availableUsers) {
        if (user.isTouchId) {
            return YES;
        }
    }
    
    return NO;
}



- (NSArray *)userNamesArray {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (User *user in self.availableUsers) {
        [array addObject:user.userName];
    }
    
    return (NSArray *)array;
}


- (NSArray *)availableUsers {
    
    NSMutableArray *users = [NSMutableArray array];
    for (User *user in self.users) {
        if (!user.isForDelete) {
            [users addObject:user];
        }
    }
    
    return [NSArray arrayWithArray:users];
}


- (NSArray *)sortedUsers {

    NSSortDescriptor *descriptionSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"description"
                                                                            ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:descriptionSortDescriptor, nil];
    
    return [self.availableUsers sortedArrayUsingDescriptors:sortDescriptors];
}


- (NSArray *)sortedUsersByUserName {
    
    NSSortDescriptor *userNameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"userName"
                                                                              ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:userNameSortDescriptor, nil];
    
    return [self.availableUsers sortedArrayUsingDescriptors:sortDescriptors];
}


- (NSArray *)sortedUsersLastLoggedIsFirst {
    
    NSSortDescriptor *isLastLoggedSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"isLastLogged"
                                                                               ascending:NO];
    NSSortDescriptor *descriptionSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"description"
                                                                           ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects: isLastLoggedSortDescriptor, descriptionSortDescriptor, nil];
    
    return [self.availableUsers sortedArrayUsingDescriptors:sortDescriptors];
}

- (NSArray *)sortedUsersLastLoggedWithPinIsFirst {
    
    NSSortDescriptor *isHavePinSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"isCanEnterPin"
                                                                               ascending:NO];
    
    NSSortDescriptor *isLastLoggedSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"isLastLogged"
                                                                               ascending:NO];
    NSSortDescriptor *descriptionSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"description"
                                                                           ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects: isHavePinSortDescriptor, isLastLoggedSortDescriptor, descriptionSortDescriptor, nil];
    
    return [self.availableUsers sortedArrayUsingDescriptors:sortDescriptors];
}

- (NSArray *)sortedUsersWithPinAndLastLoggedIsFirst {
    
    NSSortDescriptor *isLastLoggedSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"isLastLogged"
                                                                            ascending:NO];
    NSSortDescriptor *descriptionSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"description"
                                                                           ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:isLastLoggedSortDescriptor, descriptionSortDescriptor, nil];
    
    return [self.usersWithPin sortedArrayUsingDescriptors:sortDescriptors];
}

- (void)clearTouchIdFlagInUsers {
    
    
    for (User *user in [UserManager sharedManager].availableUsers) {
        user.isTouchId = NO;
        user.touchIdToken = nil;
    }

    
    
}

- (NSString *)defaultSubDomain {

    //Умное подставление доменного имени
    
    if (self.lastLoggedUser) {
        return self.lastLoggedUser.subDomain;
    
    }
    
    return @".mybabysdays.com";
}

- (NSString *)defaultCompanyName {
    
    //Умное подставление имени компании
    
    if (self.lastLoggedUser) {
        if ([NetworkManager sharedInstance].isWhiteLabel && self.lastLoggedUser.companyName) {
            return self.lastLoggedUser.companyName;
        }
        
    }
    return @"";
}


#pragma mark - Network

//запускать после удачной авторизации
- (void)reciveAndUpdateUsersWithRemoteUserInfo {
    
    //для этого нужен token
    //делать после каждого успешного логина (для adminside) и часть информации (имя, фамилия линк на картинку) сохранять локально проходя по всем юзерам
    [[NetworkManager sharedInstance] getAdminListAndShowProgressHUD:NO withCompletion:^(id response, NSError *error) {
        if (response)
        {
            if ([[response objectForKey:@"adminList"] isKindOfClass:[NSArray class]]) {
                NSLog(@"resp = %@", [response objectForKey:@"adminList"] );
                [self updateUsersWithRemoteUserInfoArray:[response objectForKey:@"adminList"]];
            }
        }
        else if(error)
        {
            NSLog(@"error while getAdminListForPinSectionWithCompletion");
        }
    }];
    
}

/*
acl =     (
           {
               "id_value" = 1;
               text = "Master Admin";
           },
           {
               "id_value" = 2;
               text = "Super Admin";
           },
           {
               "id_value" = 3;
               text = "Room Admin";
           }
           );
"admin_info" =     {
    acl = 4;
    email = "room2child_asas_mum@mybabysdays.com";
    "first_name" = parent;
    image = "images/sted/parents/default.png";
    "joomla_user_id" = 285;
    "last_name" = ioslivebritish;
    position = 0;
    "position_title" = default;
    room = 0;
    "room_title" = 0;
    "sted_user_id" = 223;
    username = parent;
};
pagination = "<null>";
result =     {
    complete = 1;
    msg = "Action complete";
};
*/
//запускать после удачной авторизации
- (void)reciveAndUpdateUserWithRemoteUserInfo {
    
    
    //для этого нужен token
    //делать после каждого успешного логина (для parentside) и часть информации (имя, фамилия линк на картинку) сохранять локально проходя по всем юзерам
    [[NetworkManager sharedInstance] getAdminWithId:self.loggedUser.uid completion:^(id response, NSError *error) {
        if (response)
        {
            if ([[response valueForKeyPath:@"result.complete"]intValue] == 1) {

                [self updateUsersWithRemoteUserInfoDict:[response objectForKey:@"admin_info"]];

            }
            
        }
        else if(error)
        {
            NSLog(@"error while getAdminInfo by id for parentside");
        }
    }];
    
}


- (void)updateUsersWithRemoteUserInfoArray:(NSArray *)array{
    
    for (User *user in self.availableUsers) {
        //NSPredicate *filter = [NSPredicate predicateWithFormat:@"(uid == %i)", user.uid];
        NSPredicate *filter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            if ([[evaluatedObject objectForKey:@"uid"] intValue] == user.uid && [[NetworkManager sharedInstance].subdomain isEqualToString:user.subDomain]) return YES;
            return NO;
        }];
        NSArray *filteredArray = [array filteredArrayUsingPredicate:filter];
        if ([filteredArray count] == 1) {
            [user updateWithRemoteUserInfo:[filteredArray firstObject]];
        }
    }

    [self saveLocalUsers];
}

- (void)updateUsersWithRemoteUserInfoDict:(NSDictionary *)dict{
    
    if (dict && [dict isKindOfClass:[NSDictionary class]] && [dict objectForKey:@"joomla_user_id"] && [[dict objectForKey:@"joomla_user_id"] intValue] == self.loggedUser.uid) {
        [self.loggedUser updateWithRemoteUserInfo:dict];
    }
    
    [self saveLocalUsers];
}


- (void)recieveAndFillPinAvailabilityForAllUsers {
    
    //При отсутствии user - вручную вызываем (а не через делегат)
    //if ([self.users count] == 0) {
        //[self userHasBeenRecivePinAvailability];
    //}
    
    //Принудительно снимаем готовность (в отдельном цикле, чтобы не пересекалось)
    for (User *user in self.availableUsers) {
        user.pinAvailabilityRecived = NO;
    }
    
    for (User *user in self.availableUsers) {
        [user recieveAndFillPinAvailability];
    }
    
}


#pragma mark - User image

- (void)updateUserImage {
    
    User *user = self.loggedUser;
    if (!user) {
        return;
    }
    
    if (!user.picURL) {
        return;
    }
    
    
    [[Utils sharedInstance] downloadImageAtRelativePath:user.picURL completionHandler:^(UIImage *image) {
        if (image) {
            user.image = image;
            [self saveLocalUsers];
            NSLog(@"image downloaded");
            
        }
        else {
            NSLog(@"image not downloaded");
        }
    }];
    
    
}


#pragma mark - UserDelegate

- (void)userHasBeenRecivePinAvailability {
    
    NSLog(@"users = %@", self.availableUsers);
    
    if (self.isAllUsersRecivePinAvailability) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:AllUsersHasBeenRecivePinAvailabilityNotification
                                                            object:nil
                                                          userInfo:[NSDictionary dictionary]];
    }

}


#pragma mark - Debug description

- (void)debugPrintUsers {
    
    NSMutableString *outputString = [@"\n" mutableCopy];
    [outputString appendString:@"----------------------\n"];
    [outputString appendString:@"userList\n"];
    for (User *user in self.availableUsers) {
        [outputString appendString:[user debugDescription]];
        [outputString appendString:@"\n"];
    }
    [outputString appendString:@"----------------------\n"];
    
    NSLog(outputString);
    
}

@end
