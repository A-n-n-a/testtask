//
//  ChildContent2Cell.h
//  pd2
//
//  Created by User on 22.03.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckSwitch.h"

@interface ChildContent2Cell : UITableViewCell

@property (nonatomic,retain)IBOutlet UITextField *childBoyOrGirlTextField;
@property (nonatomic,retain)IBOutlet UITextField *childDateOfBirthTextField;
@property (nonatomic,retain)IBOutlet CheckSwitch *childSpecialEducationCheckSwitch;
@property (nonatomic,retain)IBOutlet CheckSwitch *childInNappiesCheckSwitch;
@property (nonatomic,retain)IBOutlet CheckSwitch *childFundedCheckSwitch;
@property (nonatomic,retain)IBOutlet CheckSwitch *childEALCheckSwitch;
@property (nonatomic,retain)IBOutlet CheckSwitch *childPremiumCheckSwitch;
@property (nonatomic,retain)IBOutlet CheckSwitch *childAlertsCheckSwitch;
@property (nonatomic,retain)IBOutlet UITextField *childRoomTextField;
@property (nonatomic,retain)IBOutlet UITextField *childRoomMoveTextField;

@end
