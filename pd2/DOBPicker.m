//
//  DOBPicker.m
//  Exchange
//
//  Created by User on 09.06.14.
//  Copyright (c) 2014 Vexadev LLC. All rights reserved.
//

#import "DOBPicker.h"

@interface DOBPicker ()

@end

@implementation DOBPicker

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.frame=[UIScreen mainScreen].bounds;
    self.picker.backgroundColor=[UIColor whiteColor];
    
    if (self.isTimeMode) {
        
        self.picker.datePickerMode = UIDatePickerModeTime;

        if (self.is24hFormat) {
            self.picker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
        }
        
    }
    
    self.picker.maximumDate=self.maxDate;
    self.picker.minimumDate=self.minDate;
    NSLog(@"picker:%@",self.picker);
    NSLog(@"self.initial date:%@",self.initialDate);
    if(self.initialDate)
    {
        self.picker.date=self.initialDate;
    }
    self.doneButton.action=@selector(close:);
    
    if (self.validationMode)
    {
        _validationLabel.hidden=NO;
    }
    
    [self datePickerValueChanged];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)close:(id)sender
{
    NSLog(@"datepicker.close with block:%@",self.finishHandler);
    NSLog(@"dateBlock:%@",self.dateHandler);
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        if (self.dateHandler)
        {
            self.dateHandler(self.picker.date);
        }

        if (self.finishHandler)
        {
            NSLog(@"handler must be activated");
            self.finishHandler();
        }
        self.dateHandler=nil;
        self.finishHandler=nil;
    }];
}

-(IBAction)hide
{
    NSLog(@"datepicker.hide");
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.frame = frame;
        
    } completion:^(BOOL finished) {
        
        [self.view removeFromSuperview];
        
        if (self.finishHandler)
        {
            self.finishHandler();
        }
        self.dateHandler=nil;
        self.finishHandler=nil;
    }];
}

-(void)show
{
    //int height=[UIScreen mainScreen].bounds.size.height;
    
    CGRect frame = CGRectMake(0,0, 320, self.view.frame.size.height);
    CGRect startRect = frame;
    startRect.origin.y += self.view.frame.size.height;
    
    self.view.frame = startRect;
    
    AppDelegate *delegate=[AppDelegate delegate];
    [delegate.window addSubview:self.view];
    [UIView animateWithDuration:0.3 animations:^{
       
        self.view.frame = frame;
        
    }];
}

-(IBAction)datePickerValueChanged
{
    NSLog(@"%@",self.dateHandler);
    if (_validationMode)
    {
        if (self.validationHandler)
        {
            self.validationHandler(self.picker.date);
        }
    }
    else
    {
        /*
        if (self.dateHandler)
        {
            self.dateHandler(self.picker.date);
        }
         */
    }
    return;
}

@end
