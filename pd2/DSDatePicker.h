#import <UIKit/UIKit.h>
#import "UIMonthYearPicker.h"
#import "PMEDatePicker.h"

typedef enum {
    kDatePickerTypeYear,
    kDatePickerTypeYearMonth,
    kDatePickerTypeAll,
    kDatePickerTypeAllDate
} kDatePickerType;


@interface DSDatePicker : UIViewController


@property (copy) void (^validationHandler)(NSDate *newDate);
@property (copy) void (^dateHandler)(NSDate *newDate);
@property (copy) void (^finishHandler)();

@property (nonatomic) kDatePickerType dateType;
@property (nonatomic,retain)NSDate *minDate;
@property (nonatomic,retain)NSDate *maxDate;
@property (nonatomic,retain)NSDate *initialDate;

@property (nonatomic, strong) NSLocale *en_USLocale;


// UI
@property(nonatomic,retain)IBOutlet UIDatePicker * datePicker_full;
@property (nonatomic,retain) IBOutlet UIMonthYearPicker *datepicker_yearMonth;
@property (nonatomic,retain) IBOutlet PMEDatePicker *datepicker_year;
@property(nonatomic,retain)IBOutlet UIToolbar *toolBar;
@property(nonatomic,retain)IBOutlet UIBarButtonItem * button_done;
@property(nonatomic,retain)IBOutlet UIButton * button_close;
@property(nonatomic,retain)IBOutlet UILabel * label_validation;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView * indicator_activity;


// Actions
-(IBAction)close:(id)sender;
-(IBAction)datePickerValueChanged;
-(IBAction)hide;
-(void)show;

@end
