//
//  ShowChildVC.h
//  pd2
//
//  Created by Eugene Fozekosh on 07.09.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Pd2TableViewController.h"

@interface ShowChildVC : Pd2TableViewController

@property (nonatomic, strong) NSMutableDictionary *childDict;
@property (nonatomic, strong) NSMutableArray *profileSections;

@end
