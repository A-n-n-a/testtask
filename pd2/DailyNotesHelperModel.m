//
//  DailyNotesHelperModel.m
//  pd2
//
//  Created by Andrey on 08/03/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesHelperModel.h"

@implementation DailyNotesHelperModel

/*
{
    "title": "Fruit",
    "options": [
                "Apple",
                "Banana",
                "Blackberries",
                "Blueberries",
                "Cherries",
                "Cranberries",
                "Currants",
                "Grapes",
                "Grapefruit",
                "Gooseberries",
                "Mandarine",
                "Melon",
                "Orange",
                "Pear",
                "Peach",
                "Pineapple",
                "Plum",
                "Raspberries",
                "Strawberries",
                "Watermelon"
                ]
}
*/

- (instancetype)initWithDict:(NSDictionary *)dict {
    
    self = [super init];
    if (self) {
        
        if (!dict || ![dict isKindOfClass:[NSDictionary class]]) {
            return nil;
        }
        

        self.title = [self safeString:dict[@"title"]];
        [self updateValuesWithArray:dict[@"options"]];
    
    }
    return self;
    
}


- (void)updateValuesWithArray:(NSArray *)array {
    
    if (!array || ![array isKindOfClass:[NSArray class]]) {
        return;
    }
    
    
    NSMutableArray *helpersArray = [NSMutableArray array];
    
    
    for (NSString *value in array) {
        if (value && [value isKindOfClass:[NSString class]]) {
            [helpersArray addObject:value];
        }
    }
    
    self.values = [NSArray arrayWithArray:helpersArray];
    
    
}


- (BOOL)isExist {
    
    if (self.title && self.values && [self.values count]) {
        return YES;
    }
    
    return NO;
    
}







@end
