//
//  NSString+uppercaseFirstLetter.m
//  pd2
//
//  Created by Andrey on 22/01/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

#import "NSString+uppercaseFirstLetter.h"

@implementation NSString (uppercaseFirstLetter)


- (NSString *)uppercaseFirstLetter{
    
    if (self.length > 1) {
        
        return [NSString stringWithFormat:@"%@%@",[[self substringToIndex:1] uppercaseString],[self substringFromIndex:1]];
        
    }
    else {
        return [self uppercaseString];
    }
}

@end
