//
//  TimerUIApplication.h
//  pd2
//
//  Created by i11 on 09/02/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

//the length of time before your application "times out". This number actually represents seconds, so we'll have to multiple it by 60 in the .m file
#define kApplicationTimeoutInMinutes 720

//the notification your AppDelegate needs to watch for in order to know that it has indeed "timed out"
#define kApplicationDidTimeoutNotification @"AppTimeOut"

//#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface TimerUIApplication : Singleton

@property (strong, nonatomic) NSTimer *idleTimer;
@property (strong, nonatomic) NSDate *lastNetworkActivityDate;


-(void)resetIdleTimer;
-(void)updateNetworkActivity;

@end
