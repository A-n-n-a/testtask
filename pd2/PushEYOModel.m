//
//  PushEYOModel.m
//  pd2
//
//  Created by Andrey on 03/11/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "PushEYOModel.h"

@implementation PushEYOModel

- (instancetype)initWithDict:(NSDictionary *)dict andPushTypeAction:(PushNotificationAction)actionType {
    self = [super initWithDict:dict];
    if (self) {
        
        self.pushAction = actionType;
        
        self.type = [self safeString:dict[@"type"]];
        self.tab = [self safeString:dict[@"tab"]];
        self.rowId = [self safeString:dict[@"rowId"]];
        if (!self.rowId) {
            self.rowId = [self safeString:dict[@"item"]];
        }
        
        self.date = [self safeString:dict[@"date"]];
        self.identifier = [self safeNumber:dict[@"id"]];
        
        
        self.recordType = PushEYORecordTypeUnknown;
        if (actionType == PushNotificationObservationAddedByParent) {
            NSString *recordType = [self safeString:dict[@"type"]];
            if (recordType && [recordType isEqualToString:@"obs"]) {
                self.recordType = PushEYORecordTypeObs;
            }
            else if (recordType && [recordType isEqualToString:@"note"]) {
                self.recordType = PushEYORecordTypeNote;
            }
            
        }
        else if (actionType == PushNotificationObservationAddedByAdmin) {
            NSString *recordType = [self safeString:dict[@"obtype"]];
            if (recordType && [recordType isEqualToString:@"obs"]) {
                self.recordType = PushEYORecordTypeObs;
            }
            else if (recordType && [recordType isEqualToString:@"note"]) {
                self.recordType = PushEYORecordTypeNote;
            }

        }
        
        
    }

    return self;

}


- (BOOL)isExist {
    
    
    switch (self.pushAction) {
            case PushNotificationNextStepCompleted:
                if (self.type && self.tab && self.rowId) {
                    return YES;
                }
                else {
                    return NO;
                }
                break;
    
            case PushNotificationObservationAddedByAdmin:
                if (self.type && self.tab && self.rowId && self.date && self.recordType != PushEYORecordTypeUnknown) {
                    return YES;
                }
                else {
                    return NO;
                }
                break;
            
            case PushNotificationObservationAddedByParent:
                if (self.identifier && self.recordType != PushEYORecordTypeUnknown) {
                    return YES;
                }
                else {
                    return NO;
                }
                break;
            
            default:
                return NO;
                break;
    
    }
    return NO;
}

@end
