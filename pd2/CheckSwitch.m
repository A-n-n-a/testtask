//
//  CheckSwitch.m
//  pd2
//
//  Created by User on 11.08.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "CheckSwitch.h"

@implementation CheckSwitch

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    NSLog(@"checkswitch.initWithFrame:%@",self);
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

-(void)initialize
{
    self.backgroundColor=[UIColor clearColor];
    self.imageView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
    self.imageView.tintColor=[UIColor blackColor];
    UIImage *image=[UIImage imageNamed:@"checked"];
    self.imageView.image=[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.on=YES;
    [self addSubview:self.imageView];
    
    _button=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    _button.center=CGPointMake(11, 11);
    //_button.backgroundColor=[UIColor colorWithWhite:0.5 alpha:0.5];
    
    [_button addTarget:self action:@selector(switched) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_button];
    
    self.userInteractionEnabled=YES;
    self.clipsToBounds=NO;
    self.layer.masksToBounds=NO;
    NSLog(@"checkswitch.button:%@",_button);
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    CGFloat radius = 22;
    CGRect frame = CGRectMake(-radius, -radius,
                              self.frame.size.width + radius,
                              self.frame.size.height + radius);
    
    if (CGRectContainsPoint(frame, point)) {
        return _button;
    }
    return [super hitTest:point withEvent:event];
;
}

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    NSLog(@"pointInside:%f %f",point.x,point.y);
    
    if ( CGRectContainsPoint(_button.frame, point) )
        return YES;
    
    return [super pointInside:point withEvent:event];
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    NSLog(@"checkswitch.initWithCoder:%@",self);
    if (self) {
        [self initialize];
    }
    return self;
}
-(NSString *)status
{
    if (_on) {
        return @"on";
    }
    return @"";
}

-(id)init
{
    self=[super init];
    NSLog(@"checkswitch.init:%@",self);
    if (self) {
        
        [self initialize];
    }
    return self;
}
-(void)setColor:(UIColor *)color
{
    _color=color;
    if (![MyColorTheme sharedInstance].statusBarLightContent)
    {
        _color=[UIColor blackColor];
    }
    self.imageView.tintColor=_color;
}

-(void)setOn:(BOOL)on
{
    _on=on;
    if (on)
    {
        UIImage *image=[UIImage imageNamed:@"checked"];
        self.imageView.image=[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    else
    {
        UIImage *image=[UIImage imageNamed:@"unchecked"];
        self.imageView.image=[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    
    [_delegate checkSwitch:self statusChanged:_on];
}

-(void)switched
{

    if (self.on) {
            NSLog(@"switched to off!!!");
        self.on=false;
    }
    else{
        NSLog(@"switched to on!!!");
        self.on=true;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
