#import "AOLPlist.h"
#import "AOLPlistBritish.h"
#import "AOLPlistScottish.h"

@implementation AOLPlist

-(id)init
{
    if (self =[super init]) {
        if ([[NetworkManager sharedInstance] isScottish]) {
            
            if ([[NetworkManager sharedInstance] isJ3]) {
                self.fileName = @"areasOfLearnings_british_J3";
            } else {
                self.fileName = @"areasOfLearnings_british";
            }
            
        } else {
            self.fileName = @"areasOfLearnings_scottish";
        }
    }
    return self;
}

+ (id) sharedPlist
{
    if ([[NetworkManager sharedInstance] isScottish]) {
        return [AOLPlistScottish sharedInstance];
    } else {
        return [AOLPlistBritish sharedInstance];
    }
}

+ (NSString *) fullAgeFromInfo:(NSDictionary *)info
{
    NSString * ageStart = [info[@"age_start"] integerValue] == 0 ? @"Birth" : [NSString stringWithFormat:@"%ld Months", [info[@"age_start"] integerValue]];
    return [NSString stringWithFormat:@"%@ - %ld Months", ageStart, [info[@"age_end"] integerValue]];
}

- (NSArray *) areasOfLearnings
{
    if (!self.dict_content) {
        return [NSArray new];
    }
    NSArray * areas = [self.dict_content allKeys];
    areas = [areas sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    return areas;
}

- (NSDictionary *) areasOfLearningsWithColors
{
    if (!self.dict_content) {
        return [NSDictionary new];
    }
    NSMutableDictionary * areasWithColors = [NSMutableDictionary new];
    NSArray * areas = [self.dict_content allKeys];
    areas = [areas sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [areas enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString * color = [self.dict_content[obj][@"color"] stringByReplacingOccurrencesOfString:@"#" withString:@""];
        [areasWithColors setObject:color forKey:obj];
        
    }];
    return areasWithColors;
}

- (NSArray *) aspectsForArea:(NSString *)area
{
    NSMutableArray * aspects = [NSMutableArray new];
    NSArray * items = self.dict_content[area][@"items"];
    for (NSDictionary * item in items) {
        [aspects addObject:item[@"title"]];
    }
    return aspects;
}

- (NSInteger) tabNumberForArea:(NSString *)area aspect:(NSString *)aspect
{
    NSArray * items = self.dict_content[area][@"items"];
    NSInteger number = 0;
    for (NSDictionary * item in items) {
        if ([item[@"title"] isEqualToString:aspect]) {
            return number;
        }
        number++;
    }
    return number;
}

- (NSArray *) eyosForArea:(NSString *)area aspect:(NSString *)aspect
{
    NSArray * items = self.dict_content[area][@"items"];
    for (NSDictionary * item in items) {
        if ([item[@"title"] isEqualToString:aspect]) {
            return item[@"items"];
            break;
        }
    }
    return [NSArray new];
}

- (NSDictionary *) aspectForAOL:(NSString *)aol eyo:(NSString *)_eyo
{
    aol = [aol lowercaseString];
    NSArray * aspects = self.dict_content[aol][@"items"];
    for (NSDictionary * aspect in aspects) {
        for (NSDictionary * eyo in aspect[@"items"]) {
            if ([eyo[@"id"] isEqualToString:_eyo]) {
                return aspect;
            }
        }
    }
    return nil;
}

- (NSDictionary *) eyoInfoForAOL:(NSString *)aol eyoKey:(NSString *)eyoKey
{
    aol = [aol lowercaseString];
    NSArray * aspects = self.dict_content[aol][@"items"];
    for (NSDictionary * aspect in aspects) {
        for (NSDictionary * eyo in aspect[@"items"]) {
            if ([eyo[@"id"] isEqualToString:eyoKey]) {
                NSMutableDictionary * eyoMutable = [eyo mutableCopy];
                [eyoMutable setObject:aspect[@"title"] forKey:@"aspect"];
                return eyoMutable;
            }
        }
    }
    return nil;
}

// Like a LTT98
- (NSDictionary *) earlyOutcomesInfoForKey:(NSString *)key
{
    for (NSString * aol in [self areasOfLearnings]) {
        NSMutableDictionary * info = [[self eyoInfoForAOL:aol eyoKey:key] mutableCopy];
        if (info != nil) {
            [info setObject:aol forKey:@"aol"];
            return info;
        }
    }
    return nil;
}



#pragma mark - Abstract -


- (NSArray *) sortedAreasOfLearningsKeys
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (NSString *) mainColorForType:(NSString *)type
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (NSString *) highlightedColorForType:(NSString *)type
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (NSString *) generalColor
{
    return @"EEEEEE";
}

- (NSDictionary *) aolsTitles
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (NSDictionary *) aolsIcons
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (NSDictionary *) tabsIcons
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (NSDictionary *) tabsIndexes
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (NSDictionary *) aolsButtonsColors
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

@end
