//
//  FullScreenImageViewController.h
//  pd2
//
//  Created by User on 13.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullScreenImageViewController : UIViewController

@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) UIImageView *imageView;
@property (nonatomic,strong) UIImage *image;
@property (nonatomic,strong) NSArray *imagesArray;

@end
