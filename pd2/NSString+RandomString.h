//
//  NSString+RandomString.h
//  pd2
//
//  Created by i11 on 19/06/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RandomString)

+ (NSString *)randomStringWithLength:(int)len;

@end
