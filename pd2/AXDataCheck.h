//
//  AXDataCheck.h
//  pd2
//
//  Created by i11 on 23/04/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AXDataCheck : NSObject

+ (BOOL)checkObject:(id)object;
+ (BOOL)checkDictionaryObjects:(NSDictionary *)dict onlyKeys:(NSArray *)onlyKeys exceptKeys:(NSArray *)exceptKeys;


//Safe data types
+ (NSNumber *)safeNumber:(id)data;
+ (BOOL)safeBool:(id)data;
+ (int)safeInt:(id)data;
+ (float)safeFloat:(id)data;

+ (NSString *)safeString:(id)data;
+ (NSString *)safeEmptyStringIfNil:(id)data;

@end
