//
//  DiaryVC.m
//  pd2
//
//  Created by User on 08.03.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

#import "DiaryVC.h"

#define COLLECTION_BUTTON_TAG 100


@interface DiaryVC ()

@property NSArray *buttonsArray;

@end

@implementation DiaryVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addHomeButton];
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 300)];
    titleLabel.text = @"Diaries";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont boldSystemFontOfSize:23];
    self.navigationItem.titleView=titleLabel;
    [AppDelegate delegate].navigationController = self.navigationController;
    
    self.buttonsArray = @[@"overviewCell", @"duplicationCell"];
    
    self.view.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];

}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _buttonsArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier=[_buttonsArray objectAtIndex:indexPath.row];
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    if (cell) {
        
        UIButton *button = (UIButton *)[cell.contentView viewWithTag:COLLECTION_BUTTON_TAG];
        
        if ([cellIdentifier isEqualToString:@"overviewCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASDiaryOverview]) {
            [self buttonAsDisabled:button];
        }
        
        else if ([cellIdentifier isEqualToString:@"duplicationCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASDiaryDuplication]) {
            [self buttonAsDisabled:button];
        }
    }

    
    return cell;
}




- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    NSLog(@"Identifier: %@", identifier);
    
    if ([identifier isEqualToString:@"diaryOverview@Diaries_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASDiaryOverview]) {
        [self showUnavailableAlert];
        return NO;
    }
    else if ([identifier isEqualToString:@"list@DiaryDuplication_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASDiaryDuplication]) {
        [self showUnavailableAlert];
        return NO;
    }
    else {
        return YES;
    }
    
    
    
}

@end
