//
//  ChildFilterModel.m
//  pd2
//
//  Created by Andrey on 10/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "ChildFilterModel.h"
#import "AXTableViewCell.h"


@implementation ChildFilterModel

/*
{
    birth = "14.07.2013";
    block = 0;
    "ch_area" = "";
    "ch_county" = "";
    "ch_postcode" = "";
    "ch_street_1" = "";
    "ch_town" = "";
    dip = "";
    "ec_area" = "";
    "ec_county" = "";
    "ec_doctor_name" = "";
    "ec_doctor_tel" = "";
    "ec_first_name" = "";
    "ec_house" = "";
    "ec_last_name" = "";
    "ec_mobile" = "";
    "ec_postcode" = "";
    "ec_street_1" = "";
    "ec_street_2" = "";
    "ec_telephone" = "";
    "ec_town" = "";
    "ec_work_telephone" = "";
    "ec_workplace" = "";
    finished = "17.12.2015";
    "first_name" = Maggie;
    gender = 0;
    hib = on;
    id = 2;
    image = "images/sted/children/child_2.png";
    "last_name" = Simpson;
    mea = "";
    mum = on;
    ordering = 0;
    "parent_show_alert" = 1;
    pid = 0;
    pid2 = 0;
    pol = on;
    published = 1;
    room = 10;
    "room_title" = One;
    rub = on;
    senco = off;
    starting = "08.03.2014";
    tet = on;
    who = on;
}
*/

- (instancetype)initWithDict:(NSDictionary *)dict {
    
    self = [super init];
    if (self) {
        
        self.chId = [self safeNumber:dict[@"id"]];
        
        self.firstName = [self safeStringAndDecodingHTML:dict[@"first_name"]];
        self.lastName = [self safeStringAndDecodingHTML:dict[@"last_name"]];
        
        self.photoPath = [self safeString:dict[@"image"]];
        
        self.roomName = [self safeStringAndDecodingHTML:dict[@"room_title"]];
        
        self.dateOfBirth = [self dateFrom:dict[@"birth"] withDateFormat:@"dd.MM.yyyy"];
        
        self.sourceDict = dict;
                
    }
    return self;
    
}

/*
{
    "id": "136",
    "first_name": "koss",
    "last_name": "test"
}
*/
- (instancetype)initWithDailyNotesDict:(NSDictionary *)dict {
    
    self = [super initWithDict:dict];
    if (self) {
    
        self.chId = [self safeNumber:dict[@"id"]];
        
        self.firstName = [self safeStringAndDecodingHTML:dict[@"first_name"]];
        self.lastName = [self safeStringAndDecodingHTML:dict[@"last_name"]];
    }
    return self;
    
}


- (BOOL)isExist {
    
    
    BOOL isExist =  self.firstName && self.lastName && self.chId && self.roomName;
    
    if (!isExist) {
        NSLog(@"Model isExist = NO");
    }
    
    return isExist;
    
}


- (NSString *)childrenFullName {
    
    return [[AXTableViewCell fullNameFromFirstName:self.firstName andLastName:self.lastName] capitalizedString];
    
}

- (NSString *)childrenShortName {
    //B.King
    
    NSMutableString *name = [[NSMutableString alloc] init];
    
    if (self.firstName && [self.firstName length]) {
        NSString *firstName = [NSString stringWithFormat:@"%@.", [[self.firstName substringToIndex:1] capitalizedString]];
        [name appendString:firstName];
    }
    
    if (self.lastName) {
        [name appendString:[self.lastName capitalizedString]];
    }
    
    return name;
    
}

- (BOOL)nameContain:(NSString *)containString {
    
    if (!containString || ![containString isKindOfClass:[NSString class]]) {
        return YES;
    }
    
    if (!containString.length) {
        return YES;
    }
    
    if (self.firstName && [self.firstName rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    if (self.lastName && [self.lastName rangeOfString:containString options:NSCaseInsensitiveSearch].length > 0 ) {
        return YES;
    }
    
    return NO;
    
}

@end
