#import <UIKit/UIKit.h>
/**
 * Structure:
 * controller:ControllerStoryboardID@storyboard:StoryboardName@type:modal@optionalText
 * controller:@view:@type:@ - required
 * allowed types: modal, push
 */
@interface DSLinkedStoryboardSegue : UIStoryboardSegue
+ (UIViewController *)sceneNamed:(NSString *)identifier;
@end
