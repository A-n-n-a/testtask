#import "SectionChildModelSingelton.h"
#import "NSDateFormatter+Convert.h"

@implementation SectionChildModelSingelton

- (id) init
{
    if (self = [super init]) {
        self.childModel = [SectionChildModel new];
    }
    return self;
}

- (void) loadInfoWithSourceType:(SectionChildModelSourceType)sourceType fromDict:(NSDictionary *)childInfo
{
    [_childModel loadInfoWithSourceType:sourceType fromDict:childInfo];
}

@end
