//
//  InfoPicCell.h
//  pd2
//
//  Created by i11 on 12/08/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AXTableViewCell.h"

@interface InfoPicCell : AXTableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@property (weak, nonatomic) IBOutlet UITextView *infoTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoTextViewHeightConstraint;

@property (assign, nonatomic) CGFloat height;


@property (nonatomic, strong, readonly) NSDictionary *mainTextAttributes, *boldTextAttributes;

- (void)updateImageWithImagePath:(NSString *)imagePath;
- (UIBezierPath *)picBezierPath;



@end
