//
//  ChildFilterChildCell.m
//  pd2
//
//  Created by Andrey on 10/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "ChildFilterChildCell.h"
#import "ChildFilterModel.h"

@implementation ChildFilterChildCell


- (void)updateWithModel:(ChildFilterModel *)model {
    
    
    self.firstNameLabel.text = notAvailStringIfNil([model.firstName capitalizedString]);
    self.lastNameLabel.text = notAvailStringIfNil([model.lastName capitalizedString]);
    self.dateOfBirthLabel.text = [self stringFromDate:model.dateOfBirth withDateFormat:@"dd.MM.yyyy"];
    self.ageLabel.text = [self monthsFromDate:model.dateOfBirth];
    
}

- (void)updateImageWithModel:(ChildFilterModel *)model {
    //Onscreen
    [self updateProfileImageView:self.picImageView withImagePath:model.photoPath andDefaultImagePath:nil downloadImageCompletition:nil];
    
}


@end
