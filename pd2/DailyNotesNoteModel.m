//
//  DailyNotesNoteModel.m
//  pd2
//
//  Created by Andrey on 09/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DailyNotesNoteModel.h"
#import "ChildFilterModel.h"
#import "NSDate+Printable.h"



@implementation DailyNotesNoteModel


- (instancetype)init {
    
    self = [super init];
    if (self) {
        
        NSDate *date = [NSDate date];
        
        self.date = date;
        self.title = [date printableDefaultWithTime];
        
        
        self.childrens = [NSMutableArray array];
        
    }
    return self;
    
}


/*
{
    "id": "15",
    "note": "yuiy",
    "date_added": "2017-10-03 08:59:47",
    "title": "24th Oct 2017 @ 10:43",
    "added_by": "677",
    "pending": "1",
    "children": [
                 {
                     "id": "136",
                     "first_name": "koss",
                     "last_name": "test"
                 },
                 {
                     "id": "169",
                     "first_name": "Andy",
                     "last_name": "Worry"
                 }
                 ]
}
*/

- (instancetype)initWithDict:(NSDictionary *)dict {
    
    self = [super initWithDict:dict];
    if (self) {
        
        self.noteId = [self safeNumber:dict[@"id"]];
        self.title = [self safeString:dict[@"title"]];
        self.note = [self safeString:dict[@"note"]];
        self.date = [self dateFrom:dict[@"date_added"] withDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        self.isPending = [self safeBool:dict[@"pending"]];
        self.childrens = [NSMutableArray array];
        [self fillChildrensWithArray:[self safeArray:dict[@"children"]]];
        
    }
    
    return self;
    
}


- (void)fillChildrensWithArray:(NSArray *)childrens {
    
    if (childrens && [childrens count]) {
        for (NSDictionary *childDict in childrens) {
            ChildFilterModel *model = [[ChildFilterModel alloc] initWithDailyNotesDict:childDict];
            if (model) {
                [self.childrens addObject:model];
            }
        }
    }
    
    
}

- (NSArray *)childrenIds {
    
    NSMutableArray *array = [NSMutableArray array];
    
    for (ChildFilterModel *model in self.childrens) {
        [array addObject:model.chId];
    }
    
    return [NSArray arrayWithArray:array];
    
}


- (BOOL)isExist {
    
    if (self.noteId) {
        return YES;
    }
    
    return NO;
    
}




@end
