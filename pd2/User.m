//
//  User.m
//  pd2
//
//  Created by i11 on 25/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "User.h"
#import "NetworkManager.h"
#import "UserAccessTypeModel.h"


@interface User ()

@property (nonatomic, strong) NSMutableArray *accessArray;

@end



@implementation User

- (id)initWithLocalUserInfo:(NSDictionary *)userInfo andDelegate:(id <UserDelegate>)delegate{
    
    if (self = [super init]) {
        
        self.uid = [[userInfo objectForKey:@"uid"] intValue];
        self.userName = [userInfo objectForKey:@"userName"];
        self.subDomain = [userInfo objectForKey:@"subDomain"];
        self.companyName = [userInfo objectForKey:@"companyName"];
        self.isLastLogged = [[userInfo objectForKey:@"isLastLogged"] boolValue];
        self.firstName = [userInfo objectForKey:@"firstName"];
        self.lastName = [userInfo objectForKey:@"lastName"];
        self.picURL = [userInfo objectForKey:@"picURL"];
        self.acl = [[userInfo objectForKey:@"acl"] intValue];
        self.isTouchId = [[userInfo objectForKey:@"isTouchId"] boolValue];
        self.touchIdToken = [userInfo objectForKey:@"touchIdToken"];
        self.isNewUser = [[userInfo objectForKey:@"isNewUser"] boolValue];
        
        if (self.acl <= 3) {
            self.userType = UserTypeAdmin;
        }
        else {
            self.userType = UserTypeParent;
        }
        
        NSData *imageData = [userInfo objectForKey:@"image"];
        if (imageData) {
            self.image = [UIImage imageWithData:imageData];
        }

        
        
        //Default is YES
        if ([userInfo objectForKey:@"isForStoring"]) {
            self.isForStoring = [[userInfo objectForKey:@"isForStoring"] boolValue];
        } else {
            self.isForStoring = YES;
        }

        self.isForDelete = NO;

        if ([userInfo objectForKey:@"isPinFailedAttempt"]) {
            self.isPinFailedAttempt = [[userInfo objectForKey:@"isPinFailedAttempt"] boolValue];
        } else {
            self.isPinFailedAttempt = NO;
        }
        
        self.pinAvailabilityRecived = NO;
        self.delegate = delegate;
        //[self recieveAndFillPinAvailability];
    }
    
    return self;
}


/*
 options =             {
 bookkeeping = on;
 communicate = on;
 contracts = "";
 diaries = on;
 gallery = on;
 "meal_planning" = on;
 medical = on;
 nappies = "";
 nextsteps = on;
 observations = on;
 permissions = on;
 policies = on;
 progress = on;
 questionnaires = on;
 register = on;
 "risk_assessments" = on;
 status = on;
 };
 */
- (void)fillUserOptionsWithDict:(NSDictionary *)userInfo {

    self.bookkeepingAvailable = [self checkOption:@"bookkeeping" withDict:userInfo];
    self.communicateAvailable = [self checkOption:@"communicate" withDict:userInfo];
    self.contractsAvailable = [self checkOption:@"contracts" withDict:userInfo];
    self.diariesAvailable = [self checkOption:@"diaries" withDict:userInfo];
    self.galleryAvailable = [self checkOption:@"gallery" withDict:userInfo];
    self.menusAvailable = [self checkOption:@"meal_planning" withDict:userInfo];
    self.medicalAvailable = [self checkOption:@"medical" withDict:userInfo];
    self.nextstepsAvailable = [self checkOption:@"nextsteps" withDict:userInfo];
    self.observationsAvailable = [self checkOption:@"observations" withDict:userInfo];
    self.permissionsAvailable = [self checkOption:@"permissions" withDict:userInfo];
    self.progressAvailable = [self checkOption:@"progress" withDict:userInfo];
    self.questionsAvailable = [self checkOption:@"questionnaires" withDict:userInfo];
    self.registerAvailable = [self checkOption:@"register" withDict:userInfo];
    self.policiesAvailable = [self checkOption:@"policies" withDict:userInfo];
    self.nappiesAvailable = [self checkOption:@"nappies" withDict:userInfo];
    self.riskAssessmentsAvailable = [self checkOption:@"risk_assessments" withDict:userInfo];
    self.statusAvailable = [self checkOption:@"status" withDict:userInfo];
    
}


- (BOOL)checkOption:(NSString *)optionName withDict:(NSDictionary *)userInfo {
    
    if (!userInfo || ![userInfo isKindOfClass:[NSDictionary class]]) {
        return NO;
    }
    
    id value = [userInfo objectForKey:optionName];
    
    if (value && [value isKindOfClass:[NSString class]] && [value isEqualToString:@"on"]) {
        return YES;
    }
    
    return NO;
    
}



- (void)updateAccessLevelWithDict:(NSDictionary *)dict {
    
    
    self.accessArray = [NSMutableArray array];
    
    if (!dict || ![dict isKindOfClass:[NSDictionary class]] ) {
        return;
    }
    
    for (NSString *sectionName in [dict allKeys]) {
        NSArray *dictValues = [dict objectForKey:sectionName];
        if (dictValues && [dictValues isKindOfClass:[NSArray class]]) {
            for (NSString *key in dictValues) {
                [self updateSectionsWithKey:key section:sectionName];
            }
        }
    }
    
    
}

- (void)updateSectionsWithKey:(NSString *)key section:(NSString *)section{
    
    if (!key || ![key isKindOfClass:[NSString class]]) {
        return;
    }
    
    if (!section || ![section isKindOfClass:[NSString class]]) {
        return;
    }
    
    if ([section isEqualToString:@"system"]) {
        if ([key isEqualToString:@"account"]) {
            [self addAccessType:UserAdminAccessTypeSystemAccountInformation];
        }
        else if ([key isEqualToString:@"bulk"]) {
            [self addAccessType:UserAdminAccessTypeSystemBulkDownload];
        }
        else if ([key isEqualToString:@"personalise_homepage"]) {
            [self addAccessType:UserAdminAccessTypeSystemPersonalisedHomePage];
        }
        else if ([key isEqualToString:@"personalise_colours"]) {
            [self addAccessType:UserAdminAccessTypeSystemSystemColourSchemes];
        }
        else if ([key isEqualToString:@"login"]) {
            [self addAccessType:UserAdminAccessTypeSystemWebsiteLoginForm];
        }
    }
    
    else if ([section isEqualToString:@"manage"]) {
        if ([key isEqualToString:@"rooms"]) {
            [self addAccessType:UserAdminAccessTypeManageRooms];
        }
        else if ([key isEqualToString:@"occupancy"]) {
            [self addAccessType:UserAdminAccessTypeManageOccupancy];
        }
        else if ([key isEqualToString:@"food"]) {
            [self addAccessType:UserAdminAccessTypeManageFoodMenuHelpers];
        }
        else if ([key isEqualToString:@"profile"]) {
            [self addAccessType:UserAdminAccessTypeManageProfileSections];
        }
    }
    
    
    else if ([section isEqualToString:@"setting"]) {
        if ([key isEqualToString:@"notes"]) {
            [self addAccessType:UserAdminAccessTypeSettingGeneralNotes];
        }
        else if ([key isEqualToString:@"documents"]) {
            [self addAccessType:UserAdminAccessTypeSettingDocuments];
        }
        else if ([key isEqualToString:@"visitors"]) {
            [self addAccessType:UserAdminAccessTypeSettingVisitors];
        }
        else if ([key isEqualToString:@"fire"]) {
            [self addAccessType:UserAdminAccessTypeSettingFireDrills];
        }
        else if ([key isEqualToString:@"fireplan"]) {
            [self addAccessType:UserAdminAccessTypeSettingFirePlans];
        }
        else if ([key isEqualToString:@"risk"]) {
            [self addAccessType:UserAdminAccessTypeSettingRiskAssessments];
        }
        else if ([key isEqualToString:@"sef"]) {
            [self addAccessType:UserAdminAccessTypeSettingSEF];
        }
        else if ([key isEqualToString:@"calendar"]) {
            [self addAccessType:UserAdminAccessTypeSettingCalendar];
        }
    }
    
    else if ([section isEqualToString:@"daily"]) {
        if ([key isEqualToString:@"notes"]) {
            [self addAccessType:UserAdminAccessTypeDailyNotes];
        }
        else if ([key isEqualToString:@"actions"]) {
            [self addAccessType:UserAdminAccessTypeDailyActions];
        }
        else if ([key isEqualToString:@"bottles"]) {
            [self addAccessType:UserAdminAccessTypeDailyBottles];
        }
        else if ([key isEqualToString:@"nappy"]) {
            [self addAccessType:UserAdminAccessTypeDailyToileting];
        }
        else if ([key isEqualToString:@"sleeping"]) {
            [self addAccessType:UserAdminAccessTypeDailySleeping];
        }
        else if ([key isEqualToString:@"temperatures"]) {
            [self addAccessType:UserAdminAccessTypeDailyTemperatures];
        }
        else if ([key isEqualToString:@"food"]) {
            [self addAccessType:UserAdminAccessTypeDailyFoodMenus];
        }
        else if ([key isEqualToString:@"checklist"]) {
            [self addAccessType:UserAdminAccessTypeDailyChecklist];
        }
        else if ([key isEqualToString:@"register"]) {
            [self addAccessType:UserAdminAccessTypeDailyRegister];
        }
    }
    
    
    else if ([section isEqualToString:@"children"]) {
        if ([key isEqualToString:@"children"]) {
            [self addAccessType:UserAdminAccessTypeChildrenChildren];
        }
        else if ([key isEqualToString:@"included"]) {
            [self addAccessType:UserAdminAccessTypeChildrenIncluded];
        }
        else if ([key isEqualToString:@"moving"]) {
            [self addAccessType:UserAdminAccessTypeChildrenMoving];
        }
        else if ([key isEqualToString:@"waiting"]) {
            [self addAccessType:UserAdminAccessTypeChildrenWaiting];
        }
        else if ([key isEqualToString:@"departing"]) {
            [self addAccessType:UserAdminAccessTypeChildrenDeparted];
        }
        else if ([key isEqualToString:@"birthdays"]) {
            [self addAccessType:UserAdminAccessTypeChildrenBirthdays];
        }
    }
    
    else if ([section isEqualToString:@"parents"]) {
        if ([key isEqualToString:@"access"]) {
            [self addAccessType:UserAdminAccessTypeParentsParentalAccess];
        }
        else if ([key isEqualToString:@"isign"]) {
            [self addAccessType:UserAdminAccessTypeParentsISign];
        }
        else if ([key isEqualToString:@"authorised"]) {
            [self addAccessType:UserAdminAccessTypeParentsAuthorisedPersons];
        }
    }
    
    
    else if ([section isEqualToString:@"staff"]) {
        if ([key isEqualToString:@"profile"]) {
            [self addAccessType:UserAdminAccessTypeStaffMyProfile];
        }
        else if ([key isEqualToString:@"administrator"]) {
            [self addAccessType:UserAdminAccessTypeStaffAdministrators];
        }
        else if ([key isEqualToString:@"administrators"]) { //дубль верхнего
            [self addAccessType:UserAdminAccessTypeStaffAdministrators];
        }
        else if ([key isEqualToString:@"access"]) {
            [self addAccessType:UserAdminAccessTypeStaffAccessTimes];
        }
        else if ([key isEqualToString:@"isign-full"]) {
            [self addAccessType:UserAdminAccessTypeStaffISignFull];
        }
        else if ([key isEqualToString:@"isign-sole"]) {
            [self addAccessType:UserAdminAccessTypeStaffISignSole];
        }
        else if ([key isEqualToString:@"register"]) {
            [self addAccessType:UserAdminAccessTypeStaffAssistantRegister];
        }
        else if ([key isEqualToString:@"regular"]) { //no
            [self addAccessType:UserAdminAccessTypeStaffRegularHours];
        }
        else if ([key isEqualToString:@"policies-full"]) {
            [self addAccessType:UserAdminAccessTypeStaffPoliciesFull];
        }
        else if ([key isEqualToString:@"policies-sole"]) {
            [self addAccessType:UserAdminAccessTypeStaffPoliciesSole];
        }
        else if ([key isEqualToString:@"accidents-full"]) {
            [self addAccessType:UserAdminAccessTypeStaffAccidentsIncidentsFull];
        }
        else if ([key isEqualToString:@"accidents-sole"]) {
            [self addAccessType:UserAdminAccessTypeStaffAccidentsIncidentsSole];
        }
    }
    
    else if ([section isEqualToString:@"progress"]) {
        if ([key isEqualToString:@"overview"]) {
            [self addAccessType:UserAdminAccessTypeProgressAndPlaningProgressLearningJournal];
        }
        else if ([key isEqualToString:@"ilps"]) {
            [self addAccessType:UserAdminAccessTypeProgressAndPlaningNextStepsILP];
        }
        else if ([key isEqualToString:@"starting"]) {
            [self addAccessType:UserAdminAccessTypeProgressAndPlaningStartingPoints];
        }
        else if ([key isEqualToString:@"tracker"]) {
            [self addAccessType:UserAdminAccessTypeProgressAndPlaningProgressTrackers];
        }
        else if ([key isEqualToString:@"stats"]) {
            [self addAccessType:UserAdminAccessTypeProgressAndPlaningProgressStatistics];
        }
        else if ([key isEqualToString:@"achievement_search"]) {
            [self addAccessType:UserAdminAccessTypeProgressAndPlaningAchievementsSearch];
        }
        else if ([key isEqualToString:@"termly"]) {
            [self addAccessType:UserAdminAccessTypeProgressAndPlaningTermlyReports];
        }
        else if ([key isEqualToString:@"twoyear"]) {
            [self addAccessType:UserAdminAccessTypeProgressAndPlaning2YearProgressCheck];
        }
        else if ([key isEqualToString:@"outcome_search"]) {
            [self addAccessType:UserAdminAccessTypeProgressAndPlaningOutcomeSearch];
        }
        else if ([key isEqualToString:@"planning"]) {
            [self addAccessType:UserAdminAccessTypeProgressAndPlaningPlanning];
        }
    }
    
    
    else if ([section isEqualToString:@"medical"]) {
        if ([key isEqualToString:@"overview"]) {
            [self addAccessType:UserAdminAccessTypeMedicalOverview];
        }
        else if ([key isEqualToString:@"vacinate"]) {
            [self addAccessType:UserAdminAccessTypeMedicalVaccinations];
        }
        else if ([key isEqualToString:@"accidents"]) {
            [self addAccessType:UserAdminAccessTypeMedicalAccidents];
        }
        else if ([key isEqualToString:@"short"]) {
            [self addAccessType:UserAdminAccessTypeMedicalShortTerm];
        }
        else if ([key isEqualToString:@"long"]) {
            [self addAccessType:UserAdminAccessTypeMedicalLongTerm];
        }
        else if ([key isEqualToString:@"concerns"]) {
            [self addAccessType:UserAdminAccessTypeMedicalConcerns];
        }
        else if ([key isEqualToString:@"medicines"]) {
            [self addAccessType:UserAdminAccessTypeMedicalMedicines];
        }
        else if ([key isEqualToString:@"firstaid"]) {
            [self addAccessType:UserAdminAccessTypeMedicalFirstAidBoxes];
        }
    }
    
    else if ([section isEqualToString:@"legal"]) {
        if ([key isEqualToString:@"bookkeeping"]) { //no
            [self addAccessType:UserAdminAccessTypeBusinessLegalAccounts];
        }
        else if ([key isEqualToString:@"contracts"]) {
            [self addAccessType:UserAdminAccessTypeBusinessLegalContracts];
        }
        else if ([key isEqualToString:@"permissions"]) {
            [self addAccessType:UserAdminAccessTypeBusinessLegalPermissions];
        }
        else if ([key isEqualToString:@"policies"]) {
            [self addAccessType:UserAdminAccessTypeBusinessLegalPolicies];
        }
    }
    
    else if ([section isEqualToString:@"galleries"]) {
        if ([key isEqualToString:@"photo"]) {
            [self addAccessType:UserAdminAccessTypeGalleriesPhoto];
        }
        else if ([key isEqualToString:@"photo_comments"]) {
            [self addAccessType:UserAdminAccessTypeGalleriesPhotoComments];
        }
        else if ([key isEqualToString:@"photo_stats"]) {
            [self addAccessType:UserAdminAccessTypeGalleriesPhotoStatistics];
        }
        else if ([key isEqualToString:@"bulk"]) {
            [self addAccessType:UserAdminAccessTypeGalleriesPhotoBulkDownload];
        }
        else if ([key isEqualToString:@"video"]) {
            [self addAccessType:UserAdminAccessTypeGalleriesVideo];
        }
        else if ([key isEqualToString:@"video_comments"]) {
            [self addAccessType:UserAdminAccessTypeGalleriesVideoComments];
        }
        else if ([key isEqualToString:@"video_stats"]) {
            [self addAccessType:UserAdminAccessTypeGalleriesVideoStatistics];
        }
    }
    
    else if ([section isEqualToString:@"communicate"]) {
        if ([key isEqualToString:@"overview"]) {
            [self addAccessType:UserAdminAccessTypeCommunicateOverview];
        }
        else if ([key isEqualToString:@"compliments"]) {
            [self addAccessType:UserAdminAccessTypeCommunicateComplimentsComplaints];
        }
        else if ([key isEqualToString:@"newletters"]) {
            [self addAccessType:UserAdminAccessTypeCommunicateNewsletters];
        }
        else if ([key isEqualToString:@"questionnaires"]) {
            [self addAccessType:UserAdminAccessTypeCommunicateQuestionnaires];
        }
        else if ([key isEqualToString:@"quick"]) {
            [self addAccessType:UserAdminAccessTypeCommunicateQuickMessages];
        }
        else if ([key isEqualToString:@"pm"]) {
            [self addAccessType:UserAdminAccessTypeCommunicatePrivateMessages];
        }
    }
    
}


- (void)addAccessType:(UserAccessType)accessType {
    
    UserAccessTypeModel *model = [[UserAccessTypeModel alloc] initWithType:accessType];
    [self.accessArray addObject:model];
    
}

- (BOOL)statusFor:(UserAccessType)accessType {
    
    
    //Временно для старых систем на j15
    if (![NetworkManager sharedInstance].isJ3) {
        return YES;
    }
    
    if (!self.accessArray) {
        return NO;
    }
    
    for (UserAccessTypeModel *model in self.accessArray) {
        if (model.type == accessType) {
            return YES;
        }
    }
    
    return NO;

}


- (void)updateWithRemoteUserInfo:(NSDictionary *)userInfo {
    
    if (!(userInfo && [userInfo isKindOfClass:[NSDictionary class]])) {
        return;
    }
    
    if (self.userType == UserTypeAdmin) {
        NSString *userName = [self safeString:userInfo[@"username"]];
        if (userName) {
            self.userName = userName;
        }

        self.firstName = [self safeString:userInfo[@"first_name"]];
        self.lastName = [self safeString:userInfo[@"last_name"]];
        self.picURL = [self safeString:userInfo[@"image"]];
        self.email = [self safeString:userInfo[@"email"]];
    }
    else if (self.userType == UserTypeParent) {
        NSString *userName = [self safeString:userInfo[@"username"]];
        if (userName) {
            self.userName = userName;
        }
        
        self.puid = [self safeNumber:userInfo[@"sted_user_id"]];
        self.firstName = [self safeString:userInfo[@"first_name"]];
        self.lastName = [self safeString:userInfo[@"last_name"]];
        self.picURL = [self safeString:userInfo[@"image"]];
        self.email = [self safeString:userInfo[@"email"]];
    }
    
}

- (NSDictionary *)userInfoForLocalSave {
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setObject:[NSNumber numberWithInt:self.uid] forKey:@"uid"];
    [userInfo setObject:self.userName forKey:@"userName"];
    [userInfo setObject:self.subDomain forKey:@"subDomain"];
    [userInfo setObject:[NSNumber numberWithInt:self.acl] forKey:@"acl"];
    
    [userInfo setObject:[NSNumber numberWithBool:self.isLastLogged] forKey:@"isLastLogged"];
    [userInfo setObject:[NSNumber numberWithBool:self.isPinFailedAttempt] forKey:@"isPinFailedAttempt"];
    
    if (self.firstName) {
        [userInfo setObject:self.firstName forKey:@"firstName"];
    }
    if (self.lastName) {
        [userInfo setObject:self.lastName forKey:@"lastName"];
    }
    if (self.companyName) {
        [userInfo setObject:self.companyName forKey:@"companyName"];
    }
    if (self.picURL) {
        [userInfo setObject:self.picURL forKey:@"picURL"];
    }
    if (self.image) {
        [userInfo setObject:UIImagePNGRepresentation(self.image) forKey:@"image"];
    }
    
    [userInfo setObject:[NSNumber numberWithBool:self.isTouchId] forKey:@"isTouchId"];
    if (self.touchIdToken) {
        [userInfo setObject:self.touchIdToken forKey:@"touchIdToken"];
    }
    [userInfo setObject:[NSNumber numberWithBool:self.isNewUser] forKey:@"isNewUser"];
    
      
    return (NSDictionary *)userInfo;
}

- (BOOL)isCanEnterPin {
    if (self.havePin && !self.isPinFailedAttempt) {
        return YES;
    }
    return NO;
}

- (NSString *)description {
    
    if ([self.firstName length] > 0 && [self.lastName length] > 0) {
        return [NSString stringWithFormat:@"%@ %@", [self.firstName capitalizedString], [self.lastName capitalizedString]];
    
    } else if ([self.firstName length] > 0) {
        return [self.firstName capitalizedString];
    
    } else if ([self.lastName length] > 0) {
        return [self.lastName capitalizedString];
    
    } else {
        return self.userName;
    }
    
}


- (UserType)userType {
    
    if (self.acl <= 3) {
        return UserTypeAdmin;
    }
    else {
        return UserTypeParent;
    }
    
}

#pragma mark - network

- (void)recieveAndFillPinAvailability {
    
    if (self.pinAvailabilityRecived) {
        return;
    }
    
    [[NetworkManager sharedInstance] checkIfPinExistsForUser:self.uid subdomain:self.subDomain withCompletion:^(id response, NSError *error) {
        if (response)
        {
            NSDictionary *result = [response objectForKey:@"result"];
            BOOL havePin =[[result objectForKey:@"flag"] boolValue];
            self.havePin = havePin;
            [self pinRecived];
        }
        else if(error)
        {
            //default BOOL havePin is NO
            [self pinRecived];
        }
    }];
    
}

- (void)pinRecived {
    self.pinAvailabilityRecived = YES;
    [self.delegate userHasBeenRecivePinAvailability];
}


//Manual set pin action on Set Pin Section
//No need to start pinRecived method
- (void)manualSetHavePin:(BOOL)havePin {
    self.havePin = havePin;
    self.pinAvailabilityRecived = YES;
}

- (void)pinFailedAttempt {
    self.isPinFailedAttempt = YES;
    [self.delegate saveLocalUsers];
}

//debug

- (NSString *)debugDescription {
    
    return [NSString stringWithFormat:@"id: %d | login: %@ | host: %@ name: %@ %@ | isTouch: %@ | touchToken:%@", self.uid, self.userName, self.subDomain, self.firstName, self.lastName,  self.isTouchId ? @"YES" : @"NO", self.touchIdToken];
    
    
}

@end
