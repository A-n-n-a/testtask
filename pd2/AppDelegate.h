//
//  AppDelegate.h
//  pd2
//
//  Created by Admin on 19.01.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TargetUtils.h"
#import <CoreData/CoreData.h>
#import "NetworkManager.h"
#import "MyColorTheme.h"
#import "DataManager.h"

#define Alert(TITLE, MSG) [[[UIAlertView alloc] initWithTitle:(TITLE) \
message:(MSG) \
delegate:nil \
cancelButtonTitle:@"OK" \
otherButtonTitles:nil] show]
#define ErrorAlert(MSG) Alert(@"Error", (MSG))
#define SuccessAlert(MSG) Alert(@"Success", (MSG))

#define API_VERSION_THRESHOLD @"2.0" //Приложение работает ДО этой версии апи

#define PHOTO_UPLOAD_LIMIT 50

#warning WL-name
//#define APP_NAME @"Baby's Days" //Приложение работает ДО этой версии апи
//#define APP_APPSTORE_LINK @"https://itunes.apple.com/qa/app/babys-days/id1059695172?mt=8"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) NSDate *didEnterBackgroundDate;

+(AppDelegate *)delegate;

+(void)changePositionInYofView:(UIView *)view inPixels:(int)pixels;
+(void)changeHeightOfView:(UIView *)view inPixels:(int)pixels;

+(void)showProgressHUD;
+(void)hideProgressHUD;

+ (void)showCompletedHUD;
+ (void)showCompletedHUDWithMessage:(NSString *)message;
+ (void)showCompletedHUDWithCompletionBlock:(void(^)(void))completionBlock;
+ (void)showHUDWithSmallSizeMessage:(NSString *)message andCompletionBlock:(void(^)(void))completionBlock;
+ (void)showSettingsHUDWithMessage:(NSString *)message;
+ (void)showHUDWithMessage:(NSString *)message andCompletionBlock:(void(^)(void))completionBlock;
+ (void)showCompletedHUDWithMessage:(NSString *)message andCompletionBlock:(void(^)(void))completionBlock;

+ (BOOL)isAVFullScreenViewController;
+ (BOOL)isPortrait;

+ (void)logoutAndSendDeviceTokenIs:(BOOL)isSendDeviceToken;

+ (UIViewController*)pTopViewController;
+ (UIViewController*)pTopViewControllerWithRootViewController:(UIViewController*)rootViewController;

+ (NSString *)randomString;

+ (BOOL)isFirstLaunchAfterTouchIdUpdate;
+ (void)appLaunchedAfterTouchIdUpdate;

@end
