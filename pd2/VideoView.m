//
//  VideoView.m
//  pd2
//
//  Created by User on 06.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "VideoView.h"

@implementation VideoView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)initPlayer
{
    NSLog(@"videoView.initPlayer URL:%@",self.videoURL);
    self.mpController=[[MPMoviePlayerController alloc]initWithContentURL:self.videoURL];
    self.mpController.view.frame=self.bounds;
    self.mpController.shouldAutoplay=NO;
    [self.mpController prepareToPlay];
    [self addSubview: self.mpController.view];
}

-(void)setVideoURL:(NSURL *)videoURL
{
    _videoURL=videoURL;
    [self initPlayer];
}

@end
