//
//  DataManager.m
//  pd2
//
//  Created by User on 22.07.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "DataManager.h"
#import "NSURL+QueryDictionary.h"
#import "NSDateFormatter+Locale.h"


@implementation DataManager

-(id)init
{
    self =[super init];
    if (self) {
        diarySectionsDictionary=@{
                                  @"breakfast":@"breakfast",
                                  @"m_snack":@"morning snack",
                                  @"bottles":@"bottles",
                                  @"lunch":@"lunch",
                                  @"a_snack":@"afternoon snack",
                                  @"dinner":@"dinner",
                                  @"nappy":@"nappy",
                                  @"sleep":@"sleep",
                                  @"other":@"other"
                                  };
        //[self refresh];
    }
    return self;
}

-(void)refresh
{
    [[NetworkManager sharedInstance] getPositionListWithCompletion:nil];
    [[NetworkManager sharedInstance] getAdminListAndShowProgressHUD:NO withCompletion:nil];
    [[NetworkManager sharedInstance] getChildProfileListWithCompletion:nil];
    [[NetworkManager sharedInstance] getRoomListWithCompletion:nil];
    //[[NetworkManager sharedInstance] getChildListForParentAccessWithCompletion:nil];
}

-(NSString *)getPositionTitleById:(int)positionId
{
    for (NSDictionary *dict in _positions) {
        if ([dict[@"id"]intValue]==positionId)
        {
            NSLog(@"position title for id %d is %@",positionId,dict[@"title"]);
            return dict[@"title"];
        }
    }
    return nil;
}

-(int)idOfPositionWithTitle:(NSString *)title
{
    for (NSDictionary *dict in _positions) {
        if ([dict[@"title"] isEqualToString:title])
        {
            return [dict[@"id"]intValue];
        }
    }
    return 0;
}

-(NSArray*)positionList
{
    NSMutableArray *array=[NSMutableArray new];
    for (NSDictionary *dict in _positions)
    {
        [array addObject:dict[@"title"]];
    }
    return array;
}

-(NSMutableArray *)positionListWithoutPosition:(NSString *)string
{
    NSMutableArray *array=[NSMutableArray new];
    for (NSDictionary *dict in _positions)
    {
        if (![dict[@"title"] isEqualToString:string])
        {
            [array addObject:dict[@"title"]];
        }
    }
    return array;
}

-(BOOL)positionExistsWithName:(NSString *)positionName
{
    for (NSDictionary *dict in _positions)
    {
        if([dict[@"title"] isEqualToString:positionName])
        {
            return YES;
        }
    }
    return NO;
}

#pragma mark - Admins

-(NSString *)adminNameWithId:(int)adminId
{
    NSString *name=@"admin";
    for (NSDictionary *dict in _admins) {
        if ([dict[@"uid"]intValue]==adminId)
        {
            name=[NSString stringWithFormat:@"%@. %@",[dict[@"first_name"]substringToIndex:1],dict[@"last_name"]];
        }
    }
    return name;
}


-(NSString *)adminFullNameWithId:(int)adminId
{
    NSString *name = @"admin";
    for (NSDictionary *dict in _admins) {
        if ([dict[@"uid"]intValue]==adminId)
        {
            name=[NSString stringWithFormat:@"%@ %@",dict[@"first_name"],dict[@"last_name"]];
        }
    }
    return name;
}


-(NSString *)getAdminLevelTitleById:(int)levelId
{
    
    if (levelId == 1) {
        return @"Master";
    }
    else if (levelId == 2) {
        return @"Super";
    }
    else if (levelId == 3) {
        return @"Room";
    }
    else if (levelId == 5) {
        return @"Bookkeeping";
    }
    else {
        return @"N/A";
    }
    
}

-(int)getNumberOfAdminsWithPositions:(int)adminPosition
{
    int count=0;
    for (NSDictionary *dict in _admins) {
        if ([dict[@"position"]intValue]==adminPosition) {
            count++;
        }
    }
    return count;
}


-(BOOL)adminExistsWithEmail:(NSString *)email
{
    for (NSDictionary *dict in _admins) {
        if ([dict[@"email"] isEqualToString:email])
        {
            return YES;
        }
    }
    return NO;
}


-(BOOL)adminExistsWithUserName:(NSString *)username
{
    for (NSDictionary *dict in _admins) {
        if ([dict[@"username"] isEqualToString:username])
        {
            return YES;
        }
    }
    return NO;
}


-(int)getAclForCurrentUser
{
    return _adminAccessLevel;
}

-(int)getRoomNumForCurrentRoomAdmin
{
    int roomnum = [[[DataManager sharedInstance].adminInfo valueForKey:@"room"]intValue];
    return roomnum;
    
    if ([self getAclForCurrentUser]==3)
    {
        for (NSDictionary *dict in _admins)
        {
            int aid=self.adminId;
            if ([dict[@"uid"] intValue] == aid)
            {
                return [dict[@"room"]intValue];
            }
        }
    }
    return 0;
}

#pragma mark - Rooms

-(NSArray*)roomList
{
    NSMutableArray *array=[NSMutableArray new];
    for (NSDictionary *dict in _rooms)
    {
        [array addObject:dict[@"title"]];
    }
    return array;
}


-(NSDictionary*)roomIDsWithImagePaths
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    for (NSDictionary *room in _rooms)
    {
        [dict setObject:room[@"image"] forKey:room[@"id"]];
    }
    return dict;
}


-(NSString*)getRoomTitleWithId:(int)roomId
{
    for (NSDictionary *dict in _rooms) {
        if ([dict[@"id"]intValue]==roomId)
        {
            return dict[@"title"];
        }
    }
    return 0;

    return [NSString stringWithFormat:@"room %d",arc4random()%10];
}

-(int)roomIdWithTitle:(NSString*)title
{
    NSLog(@"roomIdWithTitle:%@",title);
    for (NSDictionary *dict in _rooms)
    {
        if ([dict[@"title"] isEqualToString:title])
        {
            NSLog(@"foundRoom.Id:%d",[dict[@"id"]intValue]);
            return [dict[@"id"]intValue];
        }
    }
    return 0;
}

-(NSString *)roomImagePathWithTitle:(NSString*)title
{
    NSLog(@"roomIdWithTitle:%@",title);
    for (NSDictionary *dict in _rooms)
    {
        if ([dict[@"title"] isEqualToString:title])
        {
            return dict[@"image"];
        }
    }
    return nil;
}



-(NSArray*)roomListWithoutRoom:(NSString*)roomTitle
{
    NSMutableArray *array=[NSMutableArray new];
    for (NSDictionary *dict in _rooms)
    {
        if (![dict[@"title"]isEqualToString:roomTitle])
        {
            [array addObject:dict[@"title"]];
        }
    }
    return array;
}

-(bool)roomExistsWithName:(NSString *)roomName roomId:(int)roomId
{
    for (NSDictionary *dict in _rooms){
        if ([[dict[@"title"] lowercaseString] isEqualToString:[roomName lowercaseString]]){
            if (roomId==0){//creating
                return true;
            }else{
                if([dict[@"id"]intValue]==roomId){
                    return false;
                }else{
                    return true;
                }
            }
        }
    }
    return false;
}



#pragma mark - Diary Sections

-(NSArray *)diarySectionsTitles
{
    return [diarySectionsDictionary allValues];
}

-(NSString *)diarySectionTitleForKey:(NSString *)key
{
    return [diarySectionsDictionary objectForKey:key];
}

-(NSString *)diarySectionKeyForTitle:(NSString *)title
{
    for (NSString *key in diarySectionsDictionary.allKeys)
    {
        if ([[[diarySectionsDictionary objectForKey:key] lowercaseString] isEqualToString:[title lowercaseString]])
        {
            return key;
        }
    }
    return nil;
}

#pragma mark - Children

-(NSString*)childNameWithId:(int)childId
{
    for (NSDictionary *dict in _childrenForParentalAccess)
    {
        if ([dict[@"id"]intValue]==childId)
        {
            return [NSString stringWithFormat:@"%@ %@",dict[@"first_name"],dict[@"last_name"]];
        }
    }
    return @"";
}

-(NSDictionary *)infoAboutChildWithId:(int)childId
{
    for (NSDictionary *dict in _childrenList)
    {
        if ([dict[@"id"]intValue]==childId)
        {
            return dict;
        }
    }
    return nil;
}


/*
-(int)childIdWithName:(NSString*)name
{
    for (NSDictionary *dict in _childrenForParentalAccess)
    {
        if ([dict[@"id"]intValue]==childId)
        {
            return [NSString stringWithFormat:@"%@ %@",dict[@"first_name"],dict[@"last_name"]];
        }
    }
    return 0;
}
 */

-(NSArray*)childrenList
{
    NSMutableArray *array=[NSMutableArray new];
    for (NSDictionary *dict in _childrenForParentalAccess)
    {
        [array addObject:[NSString stringWithFormat:@"%@ %@",dict[@"first_name"],dict[@"last_name"]]];
    }
    return array;
}

-(NSArray*)childrenIdList
{
    NSMutableArray *array=[NSMutableArray new];
    for (NSDictionary *dict in _childrenForParentalAccess)
    {
        [array addObject:dict[@"id"]];
    }
    return array;
}

-(NSArray *)childrenWithoutChild:(int)childId
{
    NSMutableArray *array=[NSMutableArray new];
    for (NSDictionary *dict in _childrenForParentalAccess)
    {
        if ([dict[@"id"]intValue]!=childId)
        {
            [array addObject:dict];
        }
    }
    return array;
}



#pragma mark - Diary Helpers
-(NSArray*)diaryHelpersOfCategory:(NSString*)category
{
    NSMutableArray *array=[NSMutableArray new];
    for (NSDictionary *dict in _diaryHelpers)
    {
        if ([dict[@"idname"]isEqualToString:category])
        {
            [array addObject:dict];
        }
    }
    return array;
}

-(NSArray*)diaryHelpersTitlesOfCategory:(NSString*)category
{
    NSMutableArray *array=[NSMutableArray new];

    NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"ordering" ascending:YES selector:@selector(localizedStandardCompare:)];
    NSArray *sortDescriptors = [NSArray arrayWithObject:orderDescriptor];
    _diaryHelpers = (NSMutableArray*)[_diaryHelpers sortedArrayUsingDescriptors:sortDescriptors];
    
    for (NSDictionary *dict in _diaryHelpers)
    {
        if ([dict[@"idname"]isEqualToString:category])
        {
            [array addObject:dict[@"helpertitle"]];
        }
    }
    
    return array;
}


-(NSArray*)helperItemsOfHelper:(NSString *)helperTitle ofCategory:(NSString*)category
{
    NSArray *array=[NSMutableArray new];
    for (NSDictionary *dict in _diaryHelpers)
    {
        if ([dict[@"idname"]isEqualToString:category])
        {
            if ([dict[@"helpertitle"] isEqualToString:helperTitle])
            {
                NSString *items=dict[@"title"];
                array=[items componentsSeparatedByString:@";"];
            }
        }
    }
    return array;
}

-(NSString *) getVideoIdFromUTubeURL:(NSString*)uTubeURL
{
    NSURL *url = [NSURL URLWithString:[uTubeURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString* videoId = NULL;
    
    if ([uTubeURL rangeOfString:@"youtube.com" options:NSCaseInsensitiveSearch].location != NSNotFound)
    {
        NSDictionary* dict = [url uq_queryDictionary];
        videoId = [dict valueForKey:@"v"];
    }
    else if ([uTubeURL rangeOfString:@"youtu.be" options:NSCaseInsensitiveSearch].location != NSNotFound)
    {
        videoId = [url lastPathComponent];
    }
    
    return videoId;
}

-(bool)profileSectionExistWithName:(NSString*)name profileSectionId:(int)profileSectionId;
{
    for (NSDictionary *dict in _profileSections)
    {
        if ([dict[@"title"]isEqualToString:name])
        {
            if (profileSectionId==0)
            {
                return true;
            }
            else if ([dict[@"id"]intValue]==profileSectionId)
            {
                return false;
            }
        }
    }
    return false;
}

-(NSArray *)getMonthsList {
    NSDateFormatter *df = [[NSDateFormatter alloc] initWithSafeLocale];
    return df.monthSymbols;
}

-(NSArray *)getShortMonthsList {
    NSDateFormatter *df = [[NSDateFormatter alloc] initWithSafeLocale];
    return df.shortMonthSymbols;
}

-(NSString*)monthNameFromNum:(int)monthNum
{
    /*
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSString *monthName = [df.monthSymbols objectAtIndex:(monthNum-1)];
    NSLog(@"monthName for num %d is %@",monthNum, monthName);
    */
    
    NSArray *months = [self getMonthsList];
    
    if (![months count] >= monthNum) {
        return nil;
    }

    NSString *monthName = [months objectAtIndex:(monthNum - 1)];
    NSLog(@"monthName for num %d is %@", monthNum, monthName);
    
    return monthName;
}


-(NSString*)monthShortNameFromNum:(int)monthNum
{
    /*
     NSDateFormatter *df = [[NSDateFormatter alloc] init];
     NSString *monthName = [df.monthSymbols objectAtIndex:(monthNum-1)];
     NSLog(@"monthName for num %d is %@",monthNum, monthName);
     */
    
    NSArray *months = [self getShortMonthsList];
    
    if (![months count] >= monthNum) {
        return nil;
    }
    
    NSString *monthName = [months objectAtIndex:(monthNum - 1)];
    NSLog(@"monthName for num %d is %@", monthNum, monthName);
    
    return monthName;
}

-(int)monthNumFromName:(NSString*)monthStr;
{
    /*
    NSLog(@"looking for month num with name %@",monthStr);
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSArray *months=df.monthSymbols;
    for (int i=0; i<months.count; i++)
    {
        NSString *m=[months objectAtIndex:i];
        if ([m isEqualToString:monthStr])
        {
            
            return i+1;
        }
    }
    return 0;
    */
    
    NSLog(@"looking for month num with name %@",monthStr);
    NSArray *months = [self getMonthsList];
    
    NSInteger index = (int)[months indexOfObject:monthStr];
    
    //В коде проекта сложилось так, что проверка notFound на 0
    return index == NSNotFound ? 0 : (int)index + 1;
    
}


#pragma mark - 
//Andrey

//Для многих контроллеров в listPicker нужен список лет. Стандартно в веб-системе выбрано 2012-2020. Генерируем в одном месте
- (NSArray *)yearsList {
    
    NSMutableArray *years = [[NSMutableArray alloc] init];
    
    for (int year = 2012; year <= 2020; year++) {
        [years addObject:[NSString stringWithFormat:@"%i", year]];
    }
    
    return [NSArray arrayWithArray:years];
}

- (NSArray *)hoursList {
    
    NSMutableArray *hours = [[NSMutableArray alloc] init];
    
    for (int hour = 0; hour <= 23; hour++) {
        [hours addObject:[NSString stringWithFormat:@"%02i", hour]];
    }
    
    return [NSArray arrayWithArray:hours];
}

- (NSArray *)minutesList {
    
    NSMutableArray *minutes = [[NSMutableArray alloc] init];
    
    for (int minute = 0; minute <= 59; minute++) {
        [minutes addObject:[NSString stringWithFormat:@"%02i", minute]];
    }
    
    return [NSArray arrayWithArray:minutes];
}


// 1 - 60

- (NSArray *)ageDueList {
    
    NSMutableArray *years = [[NSMutableArray alloc] init];
    
    for (int year = 0; year <= 60; year++) {
        [years addObject:[NSString stringWithFormat:@"%i", year]];
    }
    
    return [NSArray arrayWithArray:years];
}

@end
