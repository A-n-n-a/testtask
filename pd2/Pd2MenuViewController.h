//
//  Pd2MenuViewController.h
//  pd2
//
//  Created by admin on 6/22/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnavailableSectionManager.h"

#import "UserManager.h"

@interface Pd2MenuViewController : UIViewController

@property (nonatomic,retain)NSString *topTitle;

-(void)setColorTheme;

- (void)disableInactiveTimer;

- (void) pushViewController:(NSString *)viewControllerIdentifier
                 storyboard:(NSString *)storyboardName;

- (void)addHomeButton;

//Для unavailable sections
- (void)buttonAsDisabled:(UIButton *)button;
- (void)showUnavailableAlert;

@end
