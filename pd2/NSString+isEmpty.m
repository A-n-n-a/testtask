//
//  NSString+isEmpty.m
//  pd2
//
//  Created by Andrey on 17/10/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "NSString+isEmpty.h"

@implementation NSString (isEmpty)

- (BOOL)isEmpty {
    
    
    if ([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
        return YES;
    }
    
    return NO;
    
    //emoji?
    
}

@end
