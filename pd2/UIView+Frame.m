#import "UIView+Frame.h"

@implementation UIView(Frame)



- (CGFloat) x
{
    return self.frame.origin.x;
}

- (void) setX:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat) y
{
    return self.frame.origin.y;
}

- (void) setY:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat) width
{
    return self.frame.size.width;
}

- (void) setWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat) height
{
    return self.frame.size.height;
}

- (void) setHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (void) increaseYPos:(CGFloat)value
{
    CGRect frame = self.frame;
    frame.origin.y += value;
    self.frame = frame;
}

- (void) setFrameHeight:(CGFloat)value
{
    CGRect frame = self.frame;
    frame.size.height = value;
    self.frame = frame;
}

- (void) increaseHeight:(CGFloat)value
{
    CGRect frame = self.frame;
    frame.size.height += value;
    self.frame = frame;
}


// Constraints

- (void) clearConstraints:(NSLayoutAttribute)attribute
{
    [self.constraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSLayoutConstraint * constraint = obj;
        if (constraint.firstAttribute == attribute) {
            [self removeConstraint:constraint];
        }
    }];
}

- (void) clearHeightConstraints
{
    [self clearConstraints:NSLayoutAttributeHeight];
}

- (void) clearTopOffsetConstraints
{
    [self clearConstraints:NSLayoutAttributeTop];
}

- (void) clearBottomOffsetConstraints
{
    [self clearConstraints:NSLayoutAttributeBottom];
}

@end
