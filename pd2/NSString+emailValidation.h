//
//  NSString+emailValidation.h
//  pd2
//
//  Created by Andrey on 06/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (emailValidation)

- (BOOL)isValidEmail;

@end
