//
//  TextField.h
//  pd2
//
//  Created by i11 on 24/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//


//**************************
//
//Добавляем activity indicator к textField. Тестируется. Из минусов - текст не затирается (в планах - view
//добавлять с учетом insets и цвета
//**************************

#import <UIKit/UIKit.h>

@interface TextField : UITextField

- (void)setActivityIndicatorStatus:(BOOL)status;

- (void)showActivityIndicator;
- (void)hideActivityIndicator;


@end
