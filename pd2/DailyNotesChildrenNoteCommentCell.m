//
//  DailyNotesChildrenNoteCommentCell.m
//  pd2
//
//  Created by i11 on 08/06/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "DailyNotesChildrenNoteCommentCell.h"
#import "DailyNotesCommentModel.h"

@implementation DailyNotesChildrenNoteCommentCell

- (void)updateWithModel:(DailyNotesCommentModel *)model {
    
    self.nameLabel.text = [[self fullNameFromFirstName:model.firstName andLastName:model.lastName] capitalizedString];
    self.dateLabel.text = notAvailStringIfNil([model.date printableDefaultWithTime]);
    self.commentLabel.text = notAvailStringIfNil([model.text stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"]);
    
}

- (void)updateImageWithModel:(DailyNotesCommentModel *)model {
    //Onscreen
    [self updateProfileImageView:self.picImageView withImagePath:model.photoPath andDefaultImagePath:nil downloadImageCompletition:nil];
}

@end
