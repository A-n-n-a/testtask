//
//  AXRow.m
//  pd2
//
//  Created by i11 on 29/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AXRow.h"

NSString * const AXRowFirst = @"AXRowFirst";
NSString * const AXRowLast = @"AXRowLast";
NSString * const AXRowAll = @"AXRowAll";


@implementation AXRow

- (instancetype)initWithType:(NSString *)type;
{
    self = [super init];
    if (self) {
        self.type = type;
        self.tags = [[NSMutableSet alloc] init];
        self.text = @"";
        self.userInfo = [[NSMutableDictionary alloc] init];
    }
    return self;
}

#pragma mark - getters & setters

- (CGFloat)height {
    if (self.heightNumber) {
        return [self.heightNumber floatValue];
    } else {
        return [self.estimatedHeightNumber floatValue];
    }
}

- (void)setHeight:(CGFloat)height {
    self.heightNumber = [NSNumber numberWithFloat:height];
}

- (NSIndexPath *)indexPath {
    return [self.delegate indexPathForRow:self];
}

- (AXSection *)section {
    return (AXSection *)self.delegate;
    //Или через index section from indexPath
}

#pragma mark -

- (BOOL)isHeightAvailable {
 
    if (self.heightNumber) {
        return YES;
    }
    
    return NO;
}

- (void)needUpdateHeight {
    self.heightNumber = nil;

    if (!self.estimatedHeightNumber) {
        NSLog(@"Warning while 'needUpdateHeight'! %@ fixed cell! Estimated cell not set", self.type);
    }
}

@end
