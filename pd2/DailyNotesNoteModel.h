//
//  DailyNotesNoteModel.h
//  pd2
//
//  Created by Andrey on 07/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXBaseModel.h"


@interface DailyNotesNoteModel : AXBaseModel


@property (nonatomic, strong) NSNumber *noteId;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, assign) BOOL isPending;

@property (nonatomic, strong) NSMutableArray *childrens;

@property (nonatomic, weak) DailyNotesNoteModel *copyingFromNote;



- (instancetype)initWithDict:(NSDictionary *)dict;
- (NSArray *)childrenIds;

@end
