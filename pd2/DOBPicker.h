//
//  DOBPicker.h
//  Exchange
//
//  Created by User on 09.06.14.
//  Copyright (c) 2014 Vexadev LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DOBPicker : UIViewController

@property bool validationMode;
@property NSString *validString, *invalidString;

@property (copy) void (^validationHandler)(NSDate *newDate);
@property (copy) void (^dateHandler)(NSDate *newDate);
@property (copy) void (^finishHandler)();
@property (nonatomic,retain)NSDate *minDate;
@property (nonatomic,retain)NSDate *maxDate;

@property(nonatomic,retain)IBOutlet UIDatePicker *picker;
@property(nonatomic,retain)IBOutlet UIToolbar *toolBar;
@property(nonatomic,retain)IBOutlet UIBarButtonItem *doneButton;
@property(nonatomic,retain)IBOutlet UILabel *validationLabel;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *activityInficator;

@property (nonatomic,retain)NSDate *initialDate;
@property (nonatomic,assign)BOOL isTimeMode;
@property (nonatomic,assign)BOOL is24hFormat;

-(IBAction)close:(id)sender;
-(IBAction)datePickerValueChanged;
-(void)show;
-(IBAction)hide;

@end
