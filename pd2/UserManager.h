//
//  UserManager.h
//  pd2
//
//  Created by i11 on 25/01/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

extern NSString* const AllUsersHasBeenRecivePinAvailabilityNotification;

@interface UserManager : NSObject <UserDelegate>

+ (UserManager *)sharedManager;



@property (strong, nonatomic) NSMutableArray *users; //all users(User class) from NSDefaults

@property (strong, nonatomic) NSMutableArray *availableUsers; //Dynamic Users without isForDelete flag 

- (void)readUsersAndRecievePin;

- (void)userHasBeenRecivePinAvailability;




- (User *)addUserWithUserName:(NSString *)userName uid:(int)uid
                    subDomain:(NSString *)subDomain
                          acl:(int)acl
                        store:(BOOL)isForStoring
         isSuccessfullyLogged:(BOOL)isSuccessfullyLogged;

- (void)successfullyLoggedUser:(User *)user;
- (void)successfullyLogout;

- (User *)getUserByUID:(int)uid andSubdomain:(NSString *)subdomain;

- (User *)getUserWithTouchId;
- (void)setTouchIdEnableForUser:(User *)user;
- (void)clearNewUserFlag;

- (void)saveLocalUsers;

- (void)deleteUserWithId:(int)uid andSubdomain:(NSString *)subdomain;

- (void)recieveAndFillPinAvailabilityForAllUsers;

- (void)clearTouchIdFlagInUsers;

- (void)debugPrintUsers;

@property (strong, nonatomic) NSArray *sortedUsers; //dynamic sort by description
@property (strong, nonatomic) NSArray *sortedUsersByUserName; //dynamic sort by firstName
@property (strong, nonatomic) NSArray *sortedUsersLastLoggedIsFirst; //dynamic sort by description, first is lastLogged
@property (strong, nonatomic) NSArray *sortedUsersLastLoggedWithPinIsFirst; //dynamic sort by description, first is with pin and lastLogged

@property (strong, nonatomic) NSArray *usersWithPin; //dynamic users list with pin
@property (strong, nonatomic) NSArray *sortedUsersWithPinAndLastLoggedIsFirst; //dynamic users list with pin
@property (assign, nonatomic) BOOL isUsersWithPin; //dynamic BOOL if usersWithPin > 0
@property (assign, nonatomic) BOOL isUsersWithTouchId; //dynamic BOOL if have user with touch id


@property (strong, nonatomic) NSArray *userNamesArray; //dynamic list of all local usernames


@property (strong, nonatomic) User *loggedUser; //dynamic fast access to logged User
@property (strong, nonatomic) User *lastLoggedUser; //dynamic fast access to last logged User


@property (assign, nonatomic) BOOL isAllUsersRecivePinAvailability; //dynamic pin recived status for all local users


@property (strong, nonatomic) NSString *defaultSubDomain;
@property (strong, nonatomic) NSString *defaultCompanyName;

@property (assign, nonatomic) BOOL isFirstLoginAfterAppUpdate;

- (void)updateUserImage;

@end
