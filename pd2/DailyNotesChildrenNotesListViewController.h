//
//  DailyNotesChildrenNotesListViewController.h
//  pd2
//
//  Created by Andrey on 30.03.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXTableViewController.h"

@class DailyNotesChildrenModel;

@interface DailyNotesChildrenNotesListViewController : AXTableViewController

@property (nonatomic, strong) DailyNotesChildrenModel *childrenModel;


@end
