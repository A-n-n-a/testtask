//
//  UserAccessTypeModel.m
//  pd2
//
//  Created by Andrey on 09/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "UserAccessTypeModel.h"


@implementation UserAccessTypeModel


- (instancetype)initWithType:(UserAccessType)type {
    
    self = [super init];
    if (self) {
        
        self.type = type;
        
    }
    
    return self;
    
}

@end
