//
//  LoginLabel.m
//  pd2
//
//  Created by User on 13.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "LoginLabel.h"

@implementation LoginLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    self.textColor=[[MyColorTheme sharedInstance] loginTextColor];
}

@end
