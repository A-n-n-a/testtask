//
//  BaseModel.h
//  pd2
//
//  Created by Vasyl Balazh on 05.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject

- (NSString*)safeStringFrom:(NSString*)string;

- (NSNumber*)safeNumberFromString:(id)string;

- (NSDate *)dateFromString:(NSString *)string;

- (NSDate *)timeFromString:(NSString *)string;

- (NSDate *)dateFrom:(id)data withDateFormat:(NSString *)dateFormat;

- (id)objectOrNull:(id)object;

@end
