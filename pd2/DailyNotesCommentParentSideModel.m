//
//  DailyNotesCommentParentSideModel.m
//  pd2
//
//  Created by i11 on 08/06/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "DailyNotesCommentParentSideModel.h"

@implementation DailyNotesCommentParentSideModel


/*
 "id": "68",
 "date_add": "2017-12-12 14:33:41",
 "first_name": "Developer",
 "last_name": "Developer",
 "comment": "cvcbnvm,",
 "photo": "images/sted/admins/1878_img_2017-12-12-12-34-23.jpg"
*/

- (instancetype)initWithDict:(NSDictionary *)dict {
    self = [super initWithDict:dict];
    if (self) {
        
        self.commentId = [self safeNumber:dict[@"id"]];
        self.firstName = [self safeString:dict[@"first_name"]];
        self.lastName = [self safeString:dict[@"last_name"]];
        self.photoPath = [self safeString:dict[@"image"]];
        self.text = [self safeStringAndDecodingHTML:dict[@"comment"]];
        self.date = [self dateFrom:dict[@"date_add"] withDateFormat:@"yyyy-MM-dd HH:mm:ss"];

        
    }
    return self;
}


/*
{
    "id":"26",
    "first_name":"Andrey ",
    "last_name":"Developer",
    "datetime":"2017-11-13 11:54:39"
}
*/

- (void)updateWithDict:(NSDictionary *)dict {
    
    if (!dict || ![dict isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    self.commentId = [self safeNumber:dict[@"id"]];
    self.firstName = [self safeString:dict[@"first_name"]];
    self.lastName = [self safeString:dict[@"last_name"]];
    self.photoPath = [self safeString:dict[@"image"]];
    
    self.date = [self dateFrom:dict[@"datetime"] withDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
}



- (BOOL)isExist {
    
    BOOL isExist =  self.firstName && self.lastName && self.text && self.date;
    
    if (!isExist) {
        NSLog(@"Model isExist = NO");
    }
    
    return isExist;
    
}


@end
