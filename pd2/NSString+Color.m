#import "NSString+Color.h"

@implementation NSString (Color)

+ (UIColor *) colorFromString:(NSString *)string
{
    NSString * color   = [string stringByReplacingOccurrencesOfString:@"#" withString:@""];
    unsigned rgbValue  = 0;
    NSScanner *scanner = [NSScanner scannerWithString:color];
    [scanner setScanLocation:0];
    [scanner scanHexInt:&rgbValue];
    return UIColorFromRGB(rgbValue);
}

@end
