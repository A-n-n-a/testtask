//
//  DailyNotesAddNoteViewController.h
//  pd2
//
//  Created by Andrey on 05/12/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "AXTableViewController.h"


@interface DailyNotesAddNoteViewController : AXTableViewController

@property (nonatomic, assign) BOOL isEdit;
@property (nonatomic, assign) int noteId;



- (IBAction)pendingCheckboxAction:(id)sender;
- (IBAction)copyChildNameAction:(id)sender;

- (IBAction)saveAction:(id)sender;
- (IBAction)insertNewNoteAction:(id)sender;
- (IBAction)deleteNoteAction:(id)sender;

@end
