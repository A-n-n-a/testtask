//
//  AXHeader.m
//  pd2
//
//  Created by i11 on 26/02/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AXHeader.h"

@implementation AXHeader

- (instancetype)initWithType:(NSString *)type;
{
    self = [super init];
    if (self) {
        self.type = type;
        self.userInfo = [[NSMutableDictionary alloc] init];
    }
    return self;
}

@end
