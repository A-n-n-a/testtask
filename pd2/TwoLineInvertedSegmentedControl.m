//
//  TwoLineInvertedSegmentedControl.m
//  pd2
//
//  Created by User on 27.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "TwoLineInvertedSegmentedControl.h"

@implementation TwoLineInvertedSegmentedControl

-(void)awakeFromNib
{
    self.superview.backgroundColor=[[MyColorTheme sharedInstance]backgroundColor];
    self.backgroundColor=[[MyColorTheme sharedInstance]backgroundColor];
    self.layer.borderWidth=1.0;
    self.layer.borderColor=[[[MyColorTheme sharedInstance]spoilerBackGroundColor]CGColor];
    self.layer.cornerRadius=5.0;
    self.clipsToBounds=YES;
    _buttons = [NSMutableArray array];
    if (_b1) {
        [_buttons addObject:_b1];
    }
    if (_b2) {
        [_buttons addObject:_b2];
    }
    if (_b3) {
        [_buttons addObject:_b3];
    }
    if (_b4) {
        [_buttons addObject:_b4];
    }
    if (_b5) {
        [_buttons addObject:_b5];
    }
    if (_b6) {
        [_buttons addObject:_b6];
    }
    
    //_buttons=@[_b1,_b2,_b3,_b4,_b5,_b6];
    for (UIButton *b in _buttons)
    {
        [b setTitleColor:[[MyColorTheme sharedInstance]spoilerBackGroundColor] forState:UIControlStateNormal];
        b.layer.borderWidth=1.0;
        b.layer.borderColor=[[[MyColorTheme sharedInstance]spoilerBackGroundColor]CGColor];
        [b addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self buttonPressed:_b1];
}

-(void)buttonPressed:(id)sender
{
    NSLog(@"sender:%@",sender);
    for (UIButton *button in _buttons)
    {
        [button setTitleColor:[[MyColorTheme sharedInstance]spoilerBackGroundColor] forState:UIControlStateNormal];
        button.backgroundColor=[UIColor clearColor];
    }
    UIButton *b=sender;
    b.backgroundColor=[[MyColorTheme sharedInstance]spoilerBackGroundColor];
    [b setTitleColor:[[MyColorTheme sharedInstance]backgroundColor] forState:UIControlStateNormal];
    _selectedButton=(int)b.tag;
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    //[_delegate segControlValueChanged:self];
}


-(void)setActiveIndex:(int)index {
    
    //if (index > [self.buttons count] - 1) {
    //    return;
    //}
    
    UIButton *selectedButton = nil;
    
    for (UIButton *button in _buttons)
    {
        [button setTitleColor:[[MyColorTheme sharedInstance]spoilerBackGroundColor] forState:UIControlStateNormal];
        button.backgroundColor=[UIColor clearColor];
        
        if (button.tag == index) {
            selectedButton = button;
        }
    }
    
    if (!selectedButton) {
        return;
    }
    
    selectedButton.backgroundColor=[[MyColorTheme sharedInstance]spoilerBackGroundColor];
    [selectedButton setTitleColor:[[MyColorTheme sharedInstance]backgroundColor] forState:UIControlStateNormal];
    _selectedButton=(int)selectedButton.tag;

    
    
    
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
