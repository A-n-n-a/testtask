//
//  SpoilerView.m
//  pd2
//
//  Created by User on 13.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "SpoilerView.h"

#import "NetworkManager.h"

@interface SpoilerView()
{
    BOOL arrowIsUp;

}
@end

@implementation SpoilerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithCoder:(NSCoder *)aDecoder
{
    //NSLog(@"spoilerview.initWithCoder;");
    self=[super initWithCoder:aDecoder];
    if (self)
    {
        
    }
    return self;
}

-(void)setType:(enum spoilerType)type
{
    _type=type;
    if (_type>0)
    {
        NSArray *icons=@[@"parentInfoIcon",@"sendLoginDetailsIcon",@"accessIcon",@"accessIcon",@"addPositionIcon"];
        NSArray *labels=@[@"Parent / Carer Information",@"Send Login Details",@"Access to the system",@"Parent / Carer Access",@"Link With Siblings"];
        NSString *iconName=[icons objectAtIndex:_type-1];
        NSString *labelString=[labels objectAtIndex:_type-1];
        
        _icon.image=[[UIImage imageNamed:iconName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        _label.text=labelString;
    }

}

-(void)awakeFromNib
{
    
    
    
    
    
    arrowIsUp = NO;
    
    if (_type>0)
    {
        NSArray *icons=@[@"parentInfoIcon",@"sendLoginDetailsIcon",@"accessIcon",@"accessIcon",@"addPositionIcon"];
        NSArray *labels=@[@"Parent / Carer Information",@"Send Login Details",@"Access to the system",@"Parent / Carer Access",@"Link With Siblings"];
        NSString *iconName=[icons objectAtIndex:_type-1];
        NSString *labelString=[labels objectAtIndex:_type-1];
        _icon.image=[[UIImage imageNamed:iconName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        _label.text=labelString;
    }
    
    self.backgroundColor=[[MyColorTheme sharedInstance]spoilerBackGroundColor];
    
    
    
    
    for (UIView *view in self.subviews)
    {
        if ([view isKindOfClass:[UIImageView class]])
        {
            UIImageView *iv=(UIImageView*)view;
            iv.image=[iv.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            iv.tintColor=[[MyColorTheme sharedInstance]spoilerTextColor];
        }
        if ([view isKindOfClass:[UILabel class]])
        {
            UILabel *l=(UILabel*)view;
            l.textColor=[[MyColorTheme sharedInstance]spoilerTextColor];
        }
    }
    
    
    /*
    for (UIView *view in self.subviews)
    {
        if ([view isKindOfClass:[UIImageView class]]) {
            self.icon = (UIImageView *)view;
        }
        if ([view isKindOfClass:[UILabel class]]) {
            self.label=(UILabel*)view;
        }
    }
    
    self.icon.image=[self.icon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.icon.tintColor=[[MyColorTheme sharedInstance]spoilerTextColor];
    
    self.label.textColor=[[MyColorTheme sharedInstance]spoilerTextColor];
    */
    
    [self clearParentGestures];
    [self clearGestures];
    
    if (!self.manualBorders) {
        self.layer.borderWidth=1;
        self.layer.borderColor=[[MyColorTheme sharedInstance].onePxBorderColor CGColor];
    }
    
    if ([NetworkManager sharedInstance].isJ3) {
        
        CGRect imageFrame = CGRectMake(17, 12, 20, 20);
        self.icon.frame = imageFrame;
        //self.icon.layer.borderWidth = 0.5f;
        //self.icon.layer.borderColor = [UIColor grayColor].CGColor;
        
        
        CGRect labelFrame = CGRectMake(46, 11, self.isHeader ? 240 : 248, 21);
        self.label.frame = labelFrame;
        //self.label.layer.borderWidth = 0.5f;
        //self.label.layer.borderColor = [UIColor grayColor].CGColor;
        
        
        //self.arrow.layer.borderWidth = 0.5f;
        //self.arrow.layer.borderColor = [UIColor grayColor].CGColor;
        
        
        if (self.isJ3) {
            self.icon.image=[self.icon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            self.icon.tintColor=[[MyColorTheme sharedInstance]spoilerTextColor];
            self.icon.contentMode = UIViewContentModeScaleAspectFit;
        }
        if (self.iconName) {
            [self newIconName:self.iconName];
            self.icon.contentMode = UIViewContentModeScaleAspectFit;
        }
        
    
    }
    
    
}


- (void)newIconName:(NSString *)iconName {
    
    self.icon.image=[[UIImage imageNamed:iconName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.icon.tintColor=[[MyColorTheme sharedInstance]spoilerTextColor];
    
    
}


- (void) clearGestures
{
    while (self.gestureRecognizers.count) {
        [self removeGestureRecognizer:[self.gestureRecognizers objectAtIndex:0]];
    }
}

- (void) clearParentGestures
{
    UIView *contentView = self.superview;
    if (contentView) {
        //Обязательное удаление gestureRecognizers, иначе при longPress crash
        while (contentView.gestureRecognizers.count) {
            [contentView removeGestureRecognizer:[contentView.gestureRecognizers objectAtIndex:0]];
        }
    }
}

- (void) rotateArrow
{
    
    self.arrow.image = [[UIImage imageNamed:arrowIsUp ? @"arrowDown.png" : @"arrowUp.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] ;
    arrowIsUp = !arrowIsUp;
    /*
    [UIView animateWithDuration:0.15 animations:^{
        self.arrow.alpha = 0;
    } completion:^(BOOL finished) {
        self.arrow.image = [[UIImage imageNamed:arrowIsUp ? @"arrowDown.png" : @"arrowUp.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] ;
        arrowIsUp = !arrowIsUp;
        [UIView animateWithDuration:0.15 animations:^{
            self.arrow.alpha = 1;
        }];
    }];
    */
}

@end
