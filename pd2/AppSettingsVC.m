//
//  AppSettingsVC.m
//  pd2
//
//  Created by User on 01.03.17.
//  Copyright (c) 2017 Andrey. All rights reserved.
//

#import "AppSettingsVC.h"
#import "VPBiometricAuthenticationFacade.h"

#define COLLECTION_BUTTON_TAG 100


@interface AppSettingsVC ()

@property NSArray *buttonsArray;

@end

@implementation AppSettingsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addHomeButton];
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 300)];
    titleLabel.text = @"Settings";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.font=[UIFont boldSystemFontOfSize:23];
    self.navigationItem.titleView=titleLabel;
    [AppDelegate delegate].navigationController = self.navigationController;
    

    VPBiometricAuthenticationFacade *biometric = [[VPBiometricAuthenticationFacade alloc] init];
    
    if ([biometric isAuthenticationAvailable]) {
        self.buttonsArray = @[@"pushCell", @"deleteAdminsCell", [Utils isFaceIdSupported] ? @"faceIdCell" : @"touchIdCell"];
    }
    else {
        self.buttonsArray = @[@"pushCell", @"deleteAdminsCell"];
    }
    
//    self.buttonsArray = @[@"pushCell", @"deleteAdminsCell"];
    
    //int acl=[[DataManager sharedInstance] getAclForCurrentUser];
    
    //Version on bottom
    CGRect mainFrame = self.view.frame;
    
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(90, mainFrame.size.height - 108, 215, 50)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentRight;
    label.textColor = [[MyColorTheme sharedInstance] menuButtonsColor];
    label.numberOfLines = 1;
    label.font = [UIFont systemFontOfSize:9];
    label.lineBreakMode = UILineBreakModeWordWrap;
    label.text = [NSString stringWithFormat:@"Version: %@ (%@). API: %@", appVersionString, appBuildString, [NetworkManager sharedInstance].serverApiVersion ? : @"N/A"];
    [self.view addSubview:label];
    
    
    self.view.backgroundColor = [[MyColorTheme sharedInstance] loginBackgroundColor];

}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _buttonsArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier=[_buttonsArray objectAtIndex:indexPath.row];
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    if (cell) {
        
        UIButton *button = (UIButton *)[cell.contentView viewWithTag:COLLECTION_BUTTON_TAG];
        
        if ([cellIdentifier isEqualToString:@"pushCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASAppSettingsPush]) {
            [self buttonAsDisabled:button];
        }
        if ([cellIdentifier isEqualToString:@"deleteAdmins"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASAppSettingsDeleteAdmins]) {
            [self buttonAsDisabled:button];
        }
        if ([cellIdentifier isEqualToString:@"touchIdCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASAppSettingsTouchId]) {
            [self buttonAsDisabled:button];
        }
        
        if ([cellIdentifier isEqualToString:@"faceIdCell"] && [[UnavailableSectionManager sharedInstance]isUnavialable:UnavailableSectionASAppSettingsTouchId]) {
            [self buttonAsDisabled:button];
        }
    }

    
    return cell;
}



- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    NSLog(@"Identifier: %@", identifier);
    
    if ([identifier isEqualToString:@"settingsViewController@AppSettings_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASAppSettingsPush]) {
        [self showUnavailableAlert];
        return NO;
    }
    if ([identifier isEqualToString:@"deleteAdmins@AppSettings_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASAppSettingsDeleteAdmins]) {
        [self showUnavailableAlert];
        return NO;
    }
    if ([identifier isEqualToString:@"touchId@AppSettings_iPhone"] && [[UnavailableSectionManager sharedInstance] isUnavialable:UnavailableSectionASAppSettingsTouchId]) {
        [self showUnavailableAlert];
        return NO;
    }
    else {
        return YES;
    }
    
    
    
}

@end
