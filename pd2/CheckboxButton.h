//
//  CheckboxButton.h
//  pd2
//
//  Created by i11 on 14/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckboxButton : UIButton

@property (assign, nonatomic) BOOL isChecked;

- (void)setCheckboxStatus:(BOOL)status;
- (void)setChecked;
- (void)setUnchecked;

@end
