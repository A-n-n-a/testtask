//
//  AXHeader.h
//  pd2
//
//  Created by i11 on 26/02/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AXHeader : NSObject

@property (strong, nonatomic) NSString *type;
@property (assign, nonatomic) CGFloat height;
@property (strong, nonatomic) NSString *identifier;

@property (weak, nonatomic) UITableViewCell *cell; //cell for fast access
@property (weak, nonatomic) UIView *view; //view for fast access
@property (weak, nonatomic) UIImageView *imageView; //image view for fast access
@property (weak, nonatomic) UIImageView *arrowImageView; //arrow image view for fast access

@property (assign, nonatomic) BOOL isChecked; 


@property (strong, nonatomic) NSMutableDictionary *userInfo;
@property (strong, nonatomic) id object;
@property (strong, nonatomic) id model;

@property (strong, nonatomic) NSString *title;

- (instancetype)initWithType:(NSString *)type;



@end
