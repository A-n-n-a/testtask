//
//  ChildCell.m
//  pd2
//
//  Created by Admin on 06.02.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "ChildCell.h"

@implementation ChildCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _siblings=[[NSMutableArray alloc]init];
    }
    return self;
}

-(void)setInfoDict:(NSDictionary *)infoDict
{
    _infoDict=infoDict;
    
    if (_infoDict[@"room_name"])
    {
        _roomLabel.text=infoDict[@"room_name"];
    }
    else
    {
        _roomLabel.text=infoDict[@"room_title"];
    }
    _firstNameLabel.text=infoDict[@"first_name"] ? [infoDict[@"first_name"] capitalizedString] : @"N/A";
    _lastNameLabel.text=infoDict[@"last_name"] ? [infoDict[@"last_name"] capitalizedString] : @"N/A";
    _dobLabel.text=[NSString stringWithFormat:@"%@",infoDict[@"birth"]];
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"dd.MM.yyyy";
    NSDate *birthDate=[formatter dateFromString:infoDict[@"birth"]];
    if (!birthDate)
    {
        birthDate=[NSDate date];
    }
    NSInteger month = [[[NSCalendar currentCalendar] components: NSCalendarUnitMonth
                                                       fromDate: birthDate
                                                         toDate: [NSDate date]
                                                        options: 0] month];

    
    NSString *monthString = month == 1 ? @"month" : @"months";
    
    _ageLabel.text=[NSString stringWithFormat:@"%d %@", (int)month, monthString];
    NSString *imageName=_infoDict[@"image"];
    if (_infoDict[@"child_image"])
    {
        imageName=_infoDict[@"child_image"];
    }
    NSString *imagePath=[NSString stringWithFormat:@"%@/%@",[NetworkManager sharedInstance].baseUrl,imageName];
    
    [[Utils sharedInstance]downloadImageAtPath:imagePath completionHandler:^(UIImage *image) {
        _photoImageView.image=image;
        _photoImageView.layer.borderWidth=0.5;
        _photoImageView.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        _photoImageView.layer.cornerRadius=38;
        _photoImageView.layer.masksToBounds=YES;
    }];
    if (_archived)
    {
        _editButton.hidden=YES;
        _deleteButton.hidden=YES;
        _editImage.hidden=YES;
        _deleteImage.hidden=YES;
    }
    else
    {
        _editButton.hidden=NO;
        _deleteButton.hidden=NO;
        _editImage.hidden=NO;
        _deleteImage.hidden=NO;
    }
    _siblings=[NSMutableArray new];
   
    if ([_infoDict[@"chid2"]intValue]){
        [[[DataManager sharedInstance]childNameWithId:[_infoDict[@"chid2"]intValue]] length] ? [_siblings addObject: [[DataManager sharedInstance]childNameWithId:[_infoDict[@"chid2"]intValue]]] : nil;
    }
    
    if ([_infoDict[@"chid3"]intValue]){
        [[[DataManager sharedInstance]childNameWithId:[_infoDict[@"chid3"]intValue]] length] ? [_siblings addObject: [[DataManager sharedInstance]childNameWithId:[_infoDict[@"chid3"]intValue]]] : nil;
    }
    
    if (_siblings.count){
        if (_siblings.count>1) {
            NSString *sibl=[_siblings componentsJoinedByString:@","];
            _siblingsLabel.text=[sibl capitalizedString];
        }else{
            NSString *sibl=[_siblings firstObject];
            _siblingsLabel.text=[sibl capitalizedString];
        }
    }else{
        _siblingsLabel.text=@"None";
    }
}

-(void)initWithModel:(ManageChildModel*) model
{
    self.roomLabel.text = model.childRoomName ? model.childRoomName : @"N/A";
    self.firstNameLabel.text = model.childFirstName ? [model.childFirstName capitalizedString] : @"N/A";
    self.lastNameLabel.text = model.childLastName ? [model.childLastName capitalizedString] : @"N/A";
    self.dateOfBirthLabel.text = model.childBirthday ? model.childBirthday : @"N/A";

    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"dd.MM.yyyy";
    NSDate *birthDate=[formatter dateFromString:model.childBirthday];
    if (!birthDate){
        birthDate = [NSDate date];
    }
    NSInteger month = [[[NSCalendar currentCalendar] components: NSCalendarUnitMonth
                                                       fromDate: birthDate
                                                         toDate: [NSDate date]
                                                        options: 0] month];
    
    NSString *monthString = month == 1 ? @"month" : @"months";

    self.ageLabel.text = [NSString stringWithFormat:@"%d %@", (int)month, monthString];

    NSString *imagePath=[NSString stringWithFormat:@"%@/%@",[NetworkManager sharedInstance].baseUrl,model.childImage];
    [[Utils sharedInstance]downloadImageAtPath:imagePath completionHandler:^(UIImage *image) {
        _photoImageView.image=image;
        _photoImageView.layer.borderWidth=0.5;
        _photoImageView.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        _photoImageView.layer.cornerRadius=38;
        _photoImageView.layer.masksToBounds=YES;
    }];
    
    [self setElementsHidden:self.archived];
}


-(void)setElementsHidden:(BOOL)hidden
{
    self.editButton.hidden = hidden;
    self.deleteButton.hidden = hidden;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
