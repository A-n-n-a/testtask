//
//  ChildBackDateProgressCell.h
//  pd2
//
//  Created by User on 22.03.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckSwitch.h"

@interface ChildBackDateProgressCell : UITableViewCell

@property (nonatomic,retain)IBOutlet CheckSwitch *childBackDateProgressCheckSwitch;

@end
