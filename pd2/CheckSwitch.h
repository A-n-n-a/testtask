//
//  CheckSwitch.h
//  pd2
//
//  Created by User on 11.08.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CheckSwitch : UIView

@property (nonatomic,retain) UIImageView *imageView;
@property (nonatomic,retain) UIButton *button;
@property (nonatomic) BOOL on;
@property (nonatomic,retain) NSString *status;
@property (nonatomic,retain) UIColor *color;

@end

@protocol CheckSwitchDelegate <NSObject>

-(void)checkSwitch:(CheckSwitch *)chSwitch statusChanged:(BOOL)status;

@end


@interface CheckSwitch ()

@property (nonatomic,retain) id<CheckSwitchDelegate> delegate;

@end
