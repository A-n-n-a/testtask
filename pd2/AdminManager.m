//
//  AdminManager.m
//  pd2
//
//  Created by i11 on 10/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AdminManager.h"
#import "Admin2Model.h"
#import "NetworkManager.h"

#import "NSDate+Utilities.h"

NSString* const AdminManagerHasBeenUpdateAdminsNotification = @"AdminManagerHasBeenUpdateAdminsNotification";
NSString* const AdminManagerNotificationKey = @"AdminManagerNotificationKey";



@interface AdminManager ()

@property (nonatomic, assign) BOOL iSLastUpdateSuccessful;
@property (nonatomic, strong) NSDate *lastTimeUpdate;

@end

@implementation AdminManager

-(id)init
{
    self =[super init];
    if (self) {
        self.admins = [[NSArray alloc] init];
        //[self refresh];
    }
    return self;
}


- (void)refresh {
    
    [[NetworkManager sharedInstance] getAdminListAndShowProgressHUD:NO withCompletion:^(id response, NSError *error) {
        
        if (response)
        {
            if([[response valueForKeyPath:@"result.complete"] intValue] == 1)
            {
                
                NSArray *adminList = [response valueForKeyPath:@"adminList"];
                [self processWithData:adminList];
            }
            else
            {
                
                self.iSLastUpdateSuccessful = NO;
                
            }
        }
        else if(error)
        {
            self.iSLastUpdateSuccessful = NO;
        }
    }];

    
}

- (void)processWithData:(NSArray *)adminList {
    
    self.lastTimeUpdate = [NSDate date];
    
    NSMutableArray *admins = [[NSMutableArray alloc] init];
    
    for (NSDictionary *userInfo in adminList) {
        Admin2Model *admin = [[Admin2Model alloc] initWithDict:userInfo];
        if (admin) {
            [admins addObject:admin];
        }
    }
    
    self.admins = [NSArray arrayWithArray:admins];
    
    if ([admins count] > 0) {
        self.iSLastUpdateSuccessful = YES;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:AdminManagerHasBeenUpdateAdminsNotification
                                                            object:nil
                                                          userInfo:@{AdminManagerNotificationKey:self.admins}];
    
    }

    

}


- (Admin2Model *)adminWithAdminId:(NSNumber *)adminId {
    
    if (!adminId) {
        return nil;
    }
    
    __block Admin2Model *desiredAdmin;
    
    [self.admins enumerateObjectsUsingBlock:^(Admin2Model *admin, NSUInteger idx, BOOL *stop) {
        if ([admin.adminId isEqual: adminId]) {
            desiredAdmin = admin;
            *stop = YES;
            //return admin;
        }
    }];
    
    return desiredAdmin;
    
}


//Только master, super & room admins
- (NSArray *)adminsOnly {
    
    NSMutableArray *admins = [[NSMutableArray alloc] init];
    
    for (Admin2Model *admin in self.admins) {
        
        if (admin.adminType == Admin2ModelTypeMaster || admin.adminType == Admin2ModelTypeSuper || admin.adminType == Admin2ModelTypeRoom) {
            
            
            [admins addObject:admin];
        
        }
    }
    
    return admins;
    
}


//Только master, super & room admins
- (NSArray *)adminOnlyNames {
    
    NSMutableArray *adminNames = [[NSMutableArray alloc] init];
    
    for (Admin2Model *admin in [self adminsOnly]) {

        if (admin.adminType == Admin2ModelTypeMaster || admin.adminType == Admin2ModelTypeSuper || admin.adminType == Admin2ModelTypeRoom) {
            
            NSString *adminName = [NSString stringWithFormat:@"%@ %@", admin.firstName, admin.lastName];
            [adminNames addObject:adminName];
            
        }
        
        
    }
    
    return adminNames;

}


- (BOOL)isLoaded {
    
    if (self.lastTimeUpdate && [self.lastTimeUpdate minutesBeforeDate:[NSDate date]] < 1) {
        return YES;
    }

    return NO;
    
}


@end
