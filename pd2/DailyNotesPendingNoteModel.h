//
//  DailyNotesPendingNoteModel.h
//  pd2
//
//  Created by Andrey on 07/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXBaseModel.h"


@interface DailyNotesPendingNoteModel : AXBaseModel


@property (nonatomic, strong) NSNumber *noteId;
@property (nonatomic, strong) NSString *noteTitle;
@property (nonatomic, assign) int noteBox;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, assign) int childrenCount;



- (instancetype)initWithDict:(NSDictionary *)dict;


@end
