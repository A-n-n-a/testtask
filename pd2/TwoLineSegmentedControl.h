//
//  TwoLineSegmentedControl.h
//  pd2
//
//  Created by User on 27.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TwoLineSegmentedControlDelegate <NSObject>

-(void)segControlValueChanged:(id)sender;

@end

@interface TwoLineSegmentedControl : UIControl

@property int selectedButton;
@property (nonatomic,retain)NSMutableArray *buttons;
@property (nonatomic,retain)IBOutlet UIButton *b1,*b2,*b3,*b4,*b5,*b6;

@property id<TwoLineSegmentedControlDelegate> delegate;

-(void)setActiveIndex:(int)index;

@end
