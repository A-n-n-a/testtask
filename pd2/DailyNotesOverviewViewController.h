//
//  DailyNotesOverviewViewController.h
//  pd2
//
//  Created by Andrey on 08/02/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AXTableViewController.h"

@interface DailyNotesOverviewViewController : AXTableViewController

- (IBAction)editNoteAction:(id)sender;
- (IBAction)deleteNoteAction:(id)sender;
- (IBAction)viewChildAction:(id)sender;



@end
