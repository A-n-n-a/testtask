//
//  AXBaseModel.h
//  pd2
//
//  Created by i11 on 09/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NetworkManager.h"


@interface AXBaseModel : NSObject

typedef NS_ENUM(NSInteger, GenderType) {
    GenderTypeMale,
    GenderTypeFemale,
    GenderTypeUnknown
};

- (instancetype)initWithDict:(NSDictionary *)dict;
- (instancetype)initWithArray:(NSArray *)array;

- (BOOL)isExist;

- (NSDate *)dateFrom:(id)data withDateFormat:(NSString *)dateFormat;
+ (NSDate *)dateFrom:(id)data withDateFormat:(NSString *)dateFormat;

- (NSNumber *)safeNumber:(id)data;
- (BOOL)safeBool:(id)data;
- (int)safeInt:(id)data;
- (float)safeFloat:(id)data;

- (NSString *)safeString:(id)data;
- (NSString *)safeStringAndDecodingHTML:(id)data;

- (NSString *)safeEmptyStringIfNil:(id)data;

- (NSDictionary *)safeDictionary:(id)data;
- (NSArray *)safeArray:(id)data;

- (BOOL)isArray:(NSArray *)array;
- (BOOL)isDictionary:(NSDictionary *)dict;

@end
