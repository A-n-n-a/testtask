//
//  SideBarViewController.m
//  pd2
//
//  Created by i11 on 29/07/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "SideBarViewController.h"

#import "SWRevealViewController.h"

@interface SideBarViewController ()

{
    NSArray *menuItems;
}

@end

@implementation SideBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     menuItems = @[@"1", @"2", @"3"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [menuItems objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        
        UIViewController *testViewController = (UIViewController *)[storyboard  instantiateViewControllerWithIdentifier:@"vc"];
        
        [self.revealViewController pushFrontViewController:testViewController animated:YES];
    }
    
    
    
    
}

@end
