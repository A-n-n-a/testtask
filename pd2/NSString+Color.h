#import <UIKit/UIKit.h>

@interface NSString (Color)

+ (UIColor *) colorFromString:(NSString *)string;

@end
