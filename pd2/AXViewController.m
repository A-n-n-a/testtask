//
//  AXViewController.m
//  pd2
//
//  Created by i11 on 28/04/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AXViewController.h"
#import "AXTableViewCell.h"
#import "NSDateFormatter+Locale.h"

#import "Pd2TableViewController.h"

@interface AXViewController ()

@property(nonatomic, strong) NSMutableDictionary *attributedStrings;



@end


@implementation AXViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.freeTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(freeTapAction:)];
    [self.view addGestureRecognizer:self.freeTapGestureRecognizer];
    
    self.sections = [[AXSections alloc] init];
    
    self.autoLayoutCells = [[NSMutableDictionary alloc] init];
    self.infoCells = [[NSMutableSet alloc] init];
    self.contentFitCells = [[NSMutableSet alloc] init];
    self.attributedStrings = [[NSMutableDictionary alloc] init];
    
    [self addHomeButton];
   
}




- (void)initRows {

    PDTopSection = @"PDTopSection";
    PDMainSection = @"PDMainSection";
    PDBottomSection = @"PDBottomSection";
    PDSpacerSection = @"PDSpacerSection";
    
    PDHeader = @"PDHeader";
    PDHeaderWBottomBorder = @"PDHeaderWBottomBorder";
    PDHeaderWTopBorder = @"PDHeaderWTopBorder";
    PDRoomHeader = @"PDRoomHeader";
    
    PDInfoCell = @"PDInfoCell";
    PDInfoPicCell = @"PDInfoPicCell";
    PDInfoSecondCell = @"PDInfoSecondCell";

    PDSpoilerCell = @"PDSpoilerCell";
    PDSpacerCell = @"PDSpacerCell";
    PDFilterCell = @"PDFilterCell";
    PDLoadingCell = @"PDLoadingCell";
    PDRefreshCell = @"PDRefreshCell";
    PDMessageCell = @"PDMessageCell";
    
    
    
    //В создаваемом контроллере автоматом предлагать заполнить info, spoiler и тд
   
    [self.sections rememberType:PDSpoilerCell withHeight:57.f identifier:@"spoilerCell"];
    [self.sections rememberType:PDSpacerCell withHeight:13.f identifier:@"spacerCell"];
    [self.sections rememberType:PDFilterCell withHeight:44.f identifier:@"filterCell"];
    [self.sections rememberType:PDRefreshCell withHeight:90.f identifier:@"refreshCell"];
    [self.sections rememberType:PDLoadingCell withHeight:76.f identifier:@"loadingCell"];
    [self.sections rememberType:PDMessageCell withHeight:76.f identifier:@"messageCell"];
    
    [self.sections rememberType:PDHeader withHeight:44.f identifier:@"headerCell"];
    [self.sections rememberType:PDHeaderWBottomBorder withHeight:44.f identifier:@"headerCell"];
    [self.sections rememberType:PDHeaderWTopBorder withHeight:44.f identifier:@"headerCell"];
    [self.sections rememberType:PDRoomHeader withHeight:44.f identifier:@"roomHeaderCell"];
    
    
    //Указываем какие cell будут высчитываться автоматом
    [self.infoCells addObject:PDInfoCell];
    [self.infoCells addObject:PDInfoPicCell];
    [self.infoCells addObject:PDInfoSecondCell];
    //[self.contentFitCells addObject:nil];

}

- (void)initLabels {
    
    PDInfoLabel = @"PDInfoLabel";
}




#pragma mark - UITableViewDelegate



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    NSString *rowType = row.type;
    
    CGFloat height = 0.f;
    
    NSString *selectorString = [NSString stringWithFormat:@"heightOf%@ForTableView:indexPath:row:", rowType];
    SEL heightSelector = NSSelectorFromString(selectorString);
    
    
    if ([self.infoCells containsObject:rowType]) {
        height = [self defaultInfoCellHeightForRow:row];
    }
    else if ([self.contentFitCells containsObject:rowType]) {
        height = [self defaultContentFitCellHeightForRow:row];
    }
    else if ([self respondsToSelector:heightSelector]) {
        //performSelector не может возвращать простые типы, по этому NSInvocation
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:
                                    [[self class] instanceMethodSignatureForSelector:heightSelector]];
        [invocation setSelector:heightSelector];
        [invocation setArgument:&tableView atIndex:2];
        [invocation setArgument:&indexPath atIndex:3];
        [invocation setArgument:&row atIndex:4];
        [invocation setTarget:self];
        [invocation invoke];
        [invocation getReturnValue:&height];
        
    }
    else {
        height = row.height;
    }
    
    return height;

}


- (CGFloat)defaultInfoCellHeightForRow:(AXRow *)row {
    
    NSString *key = row.type;
    
    InfoCell *cell = [self.autoLayoutCells objectForKey:key];
    if (!cell) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:row.identifier];
        [self.autoLayoutCells setObject:cell forKey:key];
    }
    
    //Вызываем в offscreenRender
    //[self remember:cell.infoLabel withKey:PDInfoLabel];
    
    //Внутри проверка необходимости пересчета
    cell = [self offscreenRenderingCell:cell withRow:row];
    
    CGFloat height;
    if ([row isHeightAvailable]) {
        height = row.height;
    }
    else {
        height = cell.height;
        [row setHeight:height];
    }
    
    return height;
    
}

- (CGFloat)defaultContentFitCellHeightForRow:(AXRow *)row {

    NSString *key = row.type;
    
    UITableViewCell *cell = [self.autoLayoutCells objectForKey:key];
    if (!cell) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:row.identifier];
        [self.autoLayoutCells setObject:cell forKey:key];
    }
    
    //Внутри проверка необходимости пересчета
    cell = [self offscreenRenderingCell:cell withRow:row];
    
    CGFloat height = 0.f;
    
    if ([row isHeightAvailable]) {
        height = row.height;
    }
    else {
        height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
        NSLog(@"height %f", height);
        [row setHeight:height];
    }
    

    return height;
    
}



//Example
/*
- (CGFloat)heightOfPDTYPECellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath row:(AXRow *)row{
        
    NSString *key = PDInfoCell;
    
    InfoCell *cell = [self.autoLayoutCells objectForKey:key];
    if (!cell) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:row.identifier];
        [self.autoLayoutCells setObject:cell forKey:key];
    }
    
    [self remember:cell.infoLabel withKey:PDInfoLabel];
    
    //Внутри проверка необходимости пересчета
    cell = [self offscreenRenderingCell:cell withRow:row];
    
    CGFloat height;
    if ([row isHeightAvailable]) {
        height = row.height;
    }
    else {
        height = cell.height;
        [row setHeight:height];
    }
    
    return height;

}
*/


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [self.sections heightForHeaderInSection:section];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.sections countOfSections];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.sections countOfRowsInSection:section];
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    AXSection *sect = [self.sections sectionAtIndex:section];
    AXHeader *header = [self.sections headerAtSectionIndex:section];
    
    if (!header) {
        return nil;
    }
    
    UIView *view = [[UIView alloc] init];
    
    NSString *selectorString = [NSString stringWithFormat:@"viewFor%@:inSection:", header.type];
    
    SEL configureSelector = NSSelectorFromString(selectorString);
    if ([self respondsToSelector:configureSelector]) {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        view = [self performSelector:configureSelector
                          withObject:header
                          withObject:sect];
    #pragma clang diagnostic pop
    }
    else if ([header.type isEqualToString:PDHeader]){
        view = [self viewForDefaultHeader:header
                                inSection:sect
                               withBorder:PDBorderTopAndBottom];
    }
    else if ([header.type isEqualToString:PDHeaderWTopBorder]){
        view = [self viewForDefaultHeader:header
                                inSection:sect
                               withBorder:PDBorderTop];
    }
    else if ([header.type isEqualToString:PDHeaderWBottomBorder]){
        view = [self viewForDefaultHeader:header
                                inSection:sect
                               withBorder:PDBorderBottom];
    }
    
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headerTap:)];
    [view addGestureRecognizer:tap];
    
    view.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    
    header.view = view;
    
    return view;

}


- (UIView *)viewForDefaultHeader:(AXHeader *)header inSection:(AXSection *)section withBorder:(PDBorderType)borderType{
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:header.identifier];
    UIView *view = cell.contentView;

    /*
     //выставляется само в классе объекта
    UIImageView *imageView = (UIImageView *)[view viewWithTag:HEADER_IMAGE_VIEW_TAG];
    imageView.image = [[UIImage imageNamed:@"photo-icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    header.imageView = imageView;
    */
    
    UIImageView *arrowImageView = (UIImageView *)[view viewWithTag:HEADER_ARROW_IMAGE_VIEW_TAG];
    [self updateHeaderArrowImage:arrowImageView withState:section.isHide];
    
    
    if (header.title) {
        UILabel *titleLabel = (UILabel *)[view viewWithTag:HEADER_LABEL_TAG];
        titleLabel.text = header.title;
    }
    
    
    UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
   
    if (borderType != PDBorderNone) {
        [self addBorder:borderType toView:containerView forToolbar:NO];
    }

    return view;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    NSString *rowType = row.type;
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    NSString *selectorString = [NSString stringWithFormat:@"configure%@ForTableView:indexPath:", rowType];
    
    SEL configureSelector = NSSelectorFromString(selectorString);
    if ([self respondsToSelector:configureSelector]) {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        cell = [self performSelector:configureSelector
                          withObject:tableView
                          withObject:indexPath];
    #pragma clang diagnostic pop
    } else if ([rowType isEqualToString:PDInfoCell]) {
        cell = [self configureDefaultCellForTableView:tableView
                                            indexPath:indexPath
                                           withBorder:PDBorderTopAndBottomDynamic];
    } else if ([rowType isEqualToString:PDLoadingCell]) {
        cell = [self configureDefaultCellForTableView:tableView
                                            indexPath:indexPath
                                           withBorder:PDBorderTopAndBottom];
    } else {
        cell = [self configureDefaultCellForTableView:tableView
                                            indexPath:indexPath
                                           withBorder:PDBorderNone];
    }
    
    row.cell = cell;
    
    
    return cell;
}


#pragma mark - Configure cells

- (UITableViewCell *)configureDefaultCellForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath withBorder:(PDBorderType)borderType {
    
    AXRow *row = [self.sections rowAtIndexPath:indexPath];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:row.identifier];
    
    if (cell) {
        
        UIView *containerView = [cell viewWithTag:CONTAINER_VIEW_TAG];
        
        cell = [self offscreenRenderingCell:cell withRow:row];
        
        if (borderType != PDBorderNone) {
            [self addBorder:borderType toView:containerView forToolbar:NO];
        }
        
        cell.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    }
    
    return cell;
}



#pragma mark - Offscreen Rendering cell updates

- (id)offscreenRenderingCell:(id)cell withRow:(AXRow *)row {
    
    
    NSString *selectorString = [NSString stringWithFormat:@"offscreenRendering%@:withRow:", row.type];
    
    SEL configureSelector = NSSelectorFromString(selectorString);
    if ([self respondsToSelector:configureSelector]) {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        cell = [self performSelector:configureSelector
                          withObject:cell
                          withObject:row];
    #pragma clang diagnostic pop
    }
    
    return cell;
}


#pragma mark - operations with rows

- (void)insertLoadingCell {
    
    AXSection *mainSection = [[AXSection alloc] initWithType:PDMainSection];
    
    
    [mainSection addRow:[self.sections genRowWithType:PDLoadingCell]];
    NSIndexSet *insertIndexSet = [self.sections insertSections:@[mainSection] afterSectionType:PDTopSection];
    
    if ([insertIndexSet count] == 0) {
        return;
    }
    
    /*
    [self.tableView beginUpdates];
    
    [self.tableView insertSections:insertIndexSet withRowAnimation:UITableViewRowAnimationNone];
    
    [self.tableView endUpdates];
    */
    [self.tableView reloadData];
}

- (void)insertLoadingCellWithAnimation {
    
    AXSection *mainSection = [[AXSection alloc] initWithType:PDMainSection];
    
    
    [mainSection addRow:[self.sections genRowWithType:PDLoadingCell]];
    NSIndexSet *insertIndexSet = [self.sections insertSections:@[mainSection] afterSectionType:PDTopSection];
    
    if ([insertIndexSet count] == 0) {
        return;
    }
    
    
    [self.tableView beginUpdates];
     
    [self.tableView insertSections:insertIndexSet withRowAnimation:UITableViewRowAnimationNone];
     
    [self.tableView endUpdates];
     
}



- (void)insertMessageCell {
    
    AXSection *mainSection = [[AXSection alloc] initWithType:PDMainSection];
    
    
    [mainSection addRow:[self.sections genRowWithType:PDMessageCell]];
    NSIndexSet *insertIndexSet = [self.sections insertSections:@[mainSection] afterSectionType:PDTopSection];
    
    if ([insertIndexSet count] == 0) {
        return;
    }
    
    
    [self.tableView beginUpdates];
    
    [self.tableView insertSections:insertIndexSet withRowAnimation:UITableViewRowAnimationNone];
    
    [self.tableView endUpdates];
    
}


- (void)updateInfoCell {
    
    NSArray * infoCellIndexPaths = [self.sections getIndexPathsForRowType:PDInfoCell];
    AXRow *infoCell = [self.sections rowWithRowType:PDInfoCell];
    [infoCell needUpdateHeight];
    
    [self.tableView beginUpdates];
    
    if ([infoCellIndexPaths count] > 0) {
        [self.tableView reloadRowsAtIndexPaths:infoCellIndexPaths withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.tableView endUpdates];
    
}

- (void)cleanForRefresh {
    
    //Очищаем mainSection
    NSIndexSet *indexSet = [self.sections getIndexSetForSectionsWithType:PDMainSection];
    [self.sections removeSectionsByIndexSet:indexSet];
    
    /*
    [self.tableView beginUpdates];
    
    if ([indexSet count] > 0) {
        [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.tableView endUpdates];
    */
    [self.tableView reloadData];
}



- (void)cleanForRefreshWithAnimation {
    
    //Очищаем mainSection
    NSIndexSet *indexSet = [self.sections getIndexSetForSectionsWithType:PDMainSection];
    [self.sections removeSectionsByIndexSet:indexSet];
    
    
    [self.tableView beginUpdates];
     
    if ([indexSet count] > 0) {
        [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }
     
    [self.tableView endUpdates];
     
    //[self.tableView reloadData];
}

- (void)deleteRows:(NSArray *)rows {

    
    NSArray *indexPaths = [self.sections getIndexPathsForRows:rows];
    [self.sections removeArrayRowsByIndexPaths:indexPaths];
    
    [self.tableView beginUpdates];
    
    if ([indexPaths count] > 0) {
        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationRight];
    }
    
    [self.tableView endUpdates];

}


- (void)deleteSections:(NSArray *)sections {
    
    
    NSIndexSet *indexSets = [self.sections getIndexSetForSections:sections];
    [self.sections removeSectionsByIndexSet:indexSets];
    
    [self.tableView beginUpdates];
    
    if ([indexSets count] > 0) {
        [self.tableView deleteSections:indexSets withRowAnimation:UITableViewRowAnimationRight];
    }
    
    [self.tableView endUpdates];
    
}


- (void)updateRowCellWithRemovalActiovityIndicator:(AXRow *)row {
    
    AXTableViewCell *cell = (AXTableViewCell *)row.cell;
    
    if (![self isCellOnScreen:cell]) {
        return;
    }
    
    [cell activityIndicatorSetStatus:row.isForRemoval];
    
}

#pragma mark - row operations

- (AXRow *)rowForView:(UIView *)view {
    
    if (!view) {
        return nil;
    }
    
    UITableViewCell *cell = [view superCell];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    return [self.sections rowAtIndexPath:indexPath];
    
}

- (NSIndexPath *)indexPathForView:(UIView *)view {
    
    UITableViewCell *cell = [view superCell];
    return [self.tableView indexPathForCell:cell];
    
}

- (BOOL)isRowOnScreen:(AXRow *)row {
    
    NSIndexPath *indexPath = row.indexPath;
    
    return [self.tableView.indexPathsForVisibleRows containsObject:indexPath];
    
}

- (BOOL)isCellOnScreen:(UITableViewCell *)cell {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    return [self.tableView.indexPathsForVisibleRows containsObject:indexPath];
    
}

- (BOOL)isOneOfCellsOnScreen:(NSArray *)cellIndexPaths {
 
    for (NSIndexPath *indexPath in cellIndexPaths) {
        if ([self.tableView.indexPathsForVisibleRows containsObject:indexPath]) {
            return YES;
        }
    }
    
    return NO;
}


- (BOOL)isCellAboveVisibleArea:(NSIndexPath *)indexPath {
    
    NSIndexPath *visibleIndexPath = [self.tableView.indexPathsForVisibleRows firstObject];
    if ([visibleIndexPath compare:indexPath] == NSOrderedDescending)
    {
        return YES;
    }
    
    return NO;
    
}

- (BOOL)isCellCompletelyVisibleByRow:(AXRow *)row {
    
    //NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    NSIndexPath *indexPath = row.indexPath;
    
    CGRect cellRect = [self.tableView rectForRowAtIndexPath:indexPath];
    BOOL completelyVisible = CGRectContainsRect(self.tableView.bounds, cellRect);
    
    return completelyVisible;
}

- (BOOL)isCellCompletelyVisibleByIndexPath:(NSIndexPath *)indexPath {
    
    if (!indexPath) {
        return YES; //При Yes мы не делаем никаких процедур
    }
    
    CGRect cellRect = [self.tableView rectForRowAtIndexPath:indexPath];
    BOOL completelyVisible = CGRectContainsRect(self.tableView.bounds, cellRect);
    
    return completelyVisible;
}





#pragma mark - Header operations

- (void)headerTap:(UIGestureRecognizer *)gestureRecognizer {
    
    
    UIView *view = gestureRecognizer.view;
    
    NSUInteger index = [self.sections getSectionIndexForView:view];
    
    if (index == NSUIntegerMax) {
        return;
    }
    
    
    AXHeader *header = [self.sections headerAtSectionIndex:index];
    AXSection *section = [self.sections sectionAtIndex:index];
    
    section.isHide = !section.isHide;
    [self updateHeaderArrowImage:header.arrowImageView withState:section.isHide];
    
    NSString *selectorString = [NSString stringWithFormat:@"header%@Tap:inSection:", header.type];

    SEL configureSelector = NSSelectorFromString(selectorString);
    if ([self respondsToSelector:configureSelector]) {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self performSelector:configureSelector
                   withObject:gestureRecognizer
                   withObject:section];
    #pragma clang diagnostic pop
    }
    else {
        [self headerDefaultTap:gestureRecognizer inSection:(AXSection *)section];
         
    }
    
}


- (void)headerDefaultTap:(UIGestureRecognizer *)gesture inSection:(AXSection *)section {
    
    NSIndexSet *reloadIndexSet = [NSIndexSet indexSetWithIndex:section.sectionIndex];
    
    [self.tableView beginUpdates];
    
    [self.tableView reloadSections:reloadIndexSet withRowAnimation:UITableViewRowAnimationFade];
    
    [self.tableView endUpdates];

    
}


- (void)updateHeaderArrowImage:(UIImageView *)imageView withState:(BOOL)state {
    
    //TODO:плавный переворот через degree?
    
    imageView.image = [[UIImage imageNamed:(state ? @"arrowDown" : @"arrowUp")] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
}


#pragma mark - attributedStrings from cells
//Отказались от вызова в height for row. Вызывается offscreenRender
- (void)remember:(id)sourceObject withKey:(NSString *)attributedStringKey {
    
    NSAttributedString *attributedString = [self.attributedStrings objectForKey:attributedStringKey];
    if (!attributedString) {
        
        if ([sourceObject isKindOfClass:[UILabel class]] || [sourceObject isKindOfClass:[UITextView class]]) {
            if ([sourceObject respondsToSelector:@selector(attributedText)]) {
                attributedString = (NSAttributedString *)[sourceObject valueForKeyPath:@"attributedText"];
            } else {
                NSLog(@"warning! не отвечает на attributedText");
            }
        } else {
            NSLog(@"warning! неизвестный класс для attributedText");
        }
        
        [self.attributedStrings setObject:[attributedString copy] forKey:attributedStringKey];
    }
}


//Вызываем для получения строки и передаем сам объект (если его нет - remember)
- (NSMutableAttributedString *)attributedStringWithKey:(NSString *)attributedStringKey forObject:(id)sourceObject {
    
    NSAttributedString *attributedString = [self.attributedStrings objectForKey:attributedStringKey];
    if (!attributedString) {
        [self remember:sourceObject withKey:attributedStringKey];
        attributedString = [self.attributedStrings objectForKey:attributedStringKey];
    }
    
    return [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
}

// replacements

- (NSMutableAttributedString *)replace:(NSString *)sourceString withString:(NSString *)string inAttributedString:(NSMutableAttributedString **)attributedString {
    
    while ([[*attributedString string] containsString:sourceString] && ![sourceString containsString:string]) {
        [*attributedString replaceCharactersInRange:[[*attributedString string] rangeOfString:sourceString] withString:string];
    }
    
    return *attributedString;
    
}


- (NSMutableAttributedString *)replace:(NSString *)sourceString withString:(NSString *)string inAttributedString:(NSMutableAttributedString **)attributedString andSetTextColor:(UIColor *)color {
    
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
    
    while ([[*attributedString string] containsString:sourceString] && ![sourceString containsString:string]) {
        //[*attributedString replaceCharactersInRange:[[*attributedString string] rangeOfString:sourceString] withString:string];
        
        [*attributedString setAttributes:attrs range:[[*attributedString string] rangeOfString:sourceString]];
        
        
        [*attributedString replaceCharactersInRange:[[*attributedString string] rangeOfString:sourceString] withString:string];
    }
    
    return *attributedString;
    
}



- (NSMutableAttributedString *)replace:(NSString *)sourceString withAttributedString:(NSAttributedString *)string inAttributedString:(NSMutableAttributedString **)attributedString {
    
    
    while ([[*attributedString string] containsString:sourceString] && ![sourceString containsString:string.string]) {
        //[*attributedString replaceCharactersInRange:[[*attributedString string] rangeOfString:sourceString] withString:string];
        
        
        [*attributedString replaceCharactersInRange:[[*attributedString string] rangeOfString:sourceString] withAttributedString:string];
    }
    
    return *attributedString;
    
}


#pragma mark - Actions
- (void)freeTapAction:(id)sender {
    
    [self.view endEditing:YES];
 
}


- (void)smartPerformSegue:(NSString *)segueIdentifier withDestinationControllerClass:(Class)destinationControllerClass {
    
    //NSInteger viewControllersStackCount = [self.navigationController.viewControllers count];
    
    
    for (id viewController in [self.navigationController.viewControllers reverseObjectEnumerator]) {
        if ([viewController isKindOfClass:destinationControllerClass]) {
            
            [self.navigationController popToViewController:viewController animated:YES];
            
            return;
        }
    }
    
    
    /*if (viewControllersStackCount > 1) {
        
        
        
        id previousViewController = [self.navigationController.viewControllers objectAtIndex:viewControllersStackCount - 2];
        if ([previousViewController isKindOfClass:destinationControllerClass]) {
            
            [self.navigationController popViewControllerAnimated:YES];
            
            return;
        }
    }
    */
    @try {
        [self performSegueWithIdentifier:segueIdentifier sender:nil];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception. Reason: %@", exception.reason);
    }
    
}

#pragma mark - utils

- (NSString *)stringFromDate:(NSDate *)date withDateFormat:(NSString *)dateFormat {
    
    if (!date) {
        return @"N/A";
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] initWithSafeLocale];
    [dateFormatter setDateFormat:dateFormat];
    
    return [dateFormatter stringFromDate:date];
    
}


- (NSAttributedString *)name:(NSString *)name descr:(NSString *)descr onNewLine:(BOOL)isOnNewLine {
    
    UIColor *color = [UIColor darkGrayColor];;
    
    UIFont *nameFont = [UIFont fontWithName:@"HelveticaNeue-Medium" size:11.f];
    UIFont *descrFont = [UIFont fontWithName:@"HelveticaNeue" size:11.f];
    
    NSDictionary *nameDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    nameFont, NSFontAttributeName,
                                    color, NSForegroundColorAttributeName, nil];
    NSDictionary *descrDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     descrFont, NSFontAttributeName,
                                     color, NSForegroundColorAttributeName, nil];
    
    NSAttributedString *nameAttributed = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@: ",isOnNewLine ? @"\n" : @"", name] attributes:nameDictionary];
    
    NSAttributedString *descrAttributed = [[NSAttributedString alloc] initWithString:descr attributes:descrDictionary];
    
    //Совмещаем
    NSMutableAttributedString *mutableAttString = [[NSMutableAttributedString alloc] initWithAttributedString:nameAttributed];
    [mutableAttString appendAttributedString:descrAttributed];
    
    
    return mutableAttString;
    
}


- (NSAttributedString *)string:(NSString *)string onNewLine:(BOOL)isOnNewLine {
    
    UIColor *color = [UIColor darkGrayColor];;
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:11.f];
    
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                font, NSFontAttributeName,
                                color, NSForegroundColorAttributeName, nil];
    
    return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",isOnNewLine ? @"\n" : @"", string] attributes:dictionary];
    
}

- (NSAttributedString *)newLineAttributed {
    
    UIColor *color = [UIColor darkGrayColor];;
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:11.f];
    
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                font, NSFontAttributeName,
                                color, NSForegroundColorAttributeName, nil];
    
    return [[NSAttributedString alloc] initWithString:@"\n" attributes:dictionary];
    
}



#pragma mark - UI Utils

- (void)scrollToCursorForTextView:(UITextView *)textView {
    
    //[self.tableView beginUpdates];
    //[self.tableView endUpdates];
    
    CGRect cursorRect = [textView caretRectForPosition:textView.selectedTextRange.start];
    cursorRect = [self.tableView convertRect:cursorRect fromView:textView];
    
    if (![self isRectVisible:cursorRect]) {
        cursorRect.size.height += 15;
        [self.tableView scrollRectToVisible:cursorRect animated:YES];
    }
}


- (BOOL)isRectVisible:(CGRect)rect {
    CGRect visibleRect;
    visibleRect.origin = self.tableView.contentOffset;
    visibleRect.origin.y += self.tableView.contentInset.top;
    visibleRect.size = self.tableView.bounds.size;
    visibleRect.size.height -= self.tableView.contentInset.top + self.tableView.contentInset.bottom;
    
    return CGRectContainsRect(visibleRect, rect);
}

#pragma mark - AXTableViewControllerUpdateDelegate


-(void)updateRow:(AXRow *)row {
    
    if (!row || !row.indexPath) {
        return;
    }
    
    NSIndexPath *indexPath = row.indexPath;
    
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    
    
}


- (void)updateIndexPaths:(NSArray *)indexPaths {
    
    if (!indexPaths || ![indexPaths count]) {
        return;
    }
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    
}



#pragma mark - borders
//Дубль из Pd2TableViewController

- (void)addBorder:(PDBorderType)borderType toView:(UIView *)view forToolbar:(BOOL)toolbar
{
    if (borderType == PDBorderTop) {
        [self addTopLineToView:view forToolbar:toolbar];
        
    } else if (borderType == PDBorderBottom) {
        [self addBottomLineToView:view dynamic:NO lightColor:NO shortLine:NO forToolbar:toolbar];
        
    } else if (borderType == PDBorderBottomLight) {
        [self addBottomLineToView:view dynamic:NO lightColor:YES shortLine:NO forToolbar:toolbar];
        
    } else if (borderType == PDBorderBottomDynamic) {
        [self addBottomLineToView:view dynamic:YES lightColor:NO shortLine:NO forToolbar:toolbar];
        
    } else if (borderType == PDBorderBottomDynamicLight) {
        [self addBottomLineToView:view dynamic:YES lightColor:YES shortLine:NO forToolbar:toolbar];
        
    } else if (borderType == PDBorderBottomDynamicLightShort) {
        [self addBottomLineToView:view dynamic:YES lightColor:YES shortLine:YES forToolbar:toolbar];
        
    } else if (borderType == PDBorderTopAndBottom) {
        [self addTopLineToView:view forToolbar:toolbar];
        [self addBottomLineToView:view dynamic:NO lightColor:NO shortLine:NO forToolbar:toolbar];
        
    } else if (borderType == PDBorderTopAndBottomDynamic) {
        [self addTopLineToView:view forToolbar:toolbar];
        [self addBottomLineToView:view dynamic:YES lightColor:NO shortLine:NO forToolbar:toolbar];
        
    } else if (borderType == PDBorderLeft) {
        [self addLeftLineToView:view forToolbar:toolbar];
        
    } else if (borderType == PDBorderRightDynamic) {
        [self addRightLineToView:view dynamic:YES forToolbar:toolbar];
        
    } else if (borderType == PDBorderRight) {
        [self addRightLineToView:view dynamic:NO forToolbar:toolbar];
    }
}

- (void)addTopLineToView:(UIView *)view forToolbar:(BOOL)toolbar {
    
    CALayer *existLineLayer = [view.layer valueForKey:TOP_LINE_LAYER_NAME];
    
    if (!existLineLayer) {
        
        CALayer *lineLayer = [CALayer layer];
        
        lineLayer.frame = CGRectMake(0.0f, 0.0f, CGRectGetWidth(view.frame), LINE_WIDTH);
        lineLayer.backgroundColor = toolbar ? LINE_COLOR.CGColor : [MyColorTheme sharedInstance].onePxBorderColor.CGColor;
        lineLayer.name = TOP_LINE_LAYER_NAME;
        
        [view.layer addSublayer:lineLayer];
        [view.layer setValue:lineLayer forKey:TOP_LINE_LAYER_NAME];
        
        //NSLog(@"Top line added");
    } else {
        //NSLog(@"Top line exists");
    }
}

- (void)addBottomLineToView:(UIView *)view dynamic:(BOOL)dynamic lightColor:(BOOL)light shortLine:(BOOL)shortLine forToolbar:(BOOL)toolbar {
    
    //Для не статичных view привязываем через constraints к нижней части (медленно)
    if (dynamic) {
        
        UIView *existLineView = [view viewWithTag:BOTTOM_LINE_VIEW_TAG];
        
        if (!existLineView) {
            
            UIView *lineView = [[UIView alloc] init];
            lineView.translatesAutoresizingMaskIntoConstraints = NO;
            
            if (toolbar) {
                lineView.backgroundColor = light ? LINE_LIGHT_COLOR : LINE_COLOR;
            } else {
                lineView.backgroundColor = [MyColorTheme sharedInstance].onePxBorderColor;
            }
            
            
            lineView.tag = BOTTOM_LINE_VIEW_TAG;
            
            [view addSubview:lineView];
            
            if (shortLine) {
                [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                                 attribute:NSLayoutAttributeLeading
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:view
                                                                 attribute:NSLayoutAttributeLeading
                                                                multiplier:1
                                                                  constant:LINE_MARGIN]];
                
                [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                                 attribute:NSLayoutAttributeWidth
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:view
                                                                 attribute:NSLayoutAttributeWidth
                                                                multiplier:1
                                                                  constant:-LINE_MARGIN*2]];
                
            }
            else {
                [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                                 attribute:NSLayoutAttributeWidth
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:view
                                                                 attribute:NSLayoutAttributeWidth
                                                                multiplier:1
                                                                  constant:0]];
                
            }
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1
                                                              constant:LINE_WIDTH]];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeBottom
                                                            multiplier:1
                                                              constant:0]];
        }
        
        //[view setNeedsUpdateConstraints];
        //[view updateConstraints];
        
    } else {
        //Для статичных view рисуем через CALayer (быстро)
        
        CALayer *existLineLayer = [view.layer valueForKey:BOTTOM_LINE_LAYER_NAME];
        
        if (!existLineLayer) {
            CGFloat height = CGRectGetHeight(view.frame);
            
            CALayer *lineLayer = [CALayer layer];
            if (shortLine) {
                lineLayer.frame = CGRectMake(LINE_MARGIN, height - LINE_WIDTH, CGRectGetWidth(view.frame) - LINE_MARGIN * 2, LINE_WIDTH);
            }
            else {
                lineLayer.frame = CGRectMake(0.0f, height - LINE_WIDTH, CGRectGetWidth(view.frame), LINE_WIDTH);
            }
            
            if (toolbar) {
                lineLayer.backgroundColor = light ? LINE_LIGHT_COLOR.CGColor : LINE_COLOR.CGColor;
            } else {
                lineLayer.backgroundColor = [MyColorTheme sharedInstance].onePxBorderColor.CGColor;
            }
            
            lineLayer.name = BOTTOM_LINE_LAYER_NAME;
            
            [view.layer addSublayer:lineLayer];
            [view.layer setValue:lineLayer forKey:BOTTOM_LINE_LAYER_NAME];
        }
    }
    
}

- (void)addLeftLineToView:(UIView *)view forToolbar:(BOOL)toolbar {
    
    CALayer *existLineLayer = [view.layer valueForKey:LEFT_LINE_LAYER_NAME];
    
    if (!existLineLayer) {
        
        CALayer *lineLayer = [CALayer layer];
        
        lineLayer.frame = CGRectMake(0.0f, 0.0f, LINE_WIDTH, CGRectGetHeight(view.frame));
        lineLayer.backgroundColor = toolbar ? LINE_COLOR.CGColor : [MyColorTheme sharedInstance].onePxBorderColor.CGColor;
        lineLayer.name = LEFT_LINE_LAYER_NAME;
        
        [view.layer addSublayer:lineLayer];
        [view.layer setValue:lineLayer forKey:LEFT_LINE_LAYER_NAME];
        
        //NSLog(@"Top line added");
    } else {
        //NSLog(@"Top line exists");
    }
}

- (void)addRightLineToView:(UIView *)view dynamic:(BOOL)dynamic forToolbar:(BOOL)toolbar {
    
    //Для не статичных view привязываем через constraints к нижней части (медленно)
    if (dynamic) {
        
        UIView *existLineView = [view viewWithTag:RIGHT_LINE_VIEW_TAG];
        
        if (!existLineView) {
            
            UIView *lineView = [[UIView alloc] init];
            lineView.translatesAutoresizingMaskIntoConstraints = NO;
            lineView.backgroundColor = toolbar ? LINE_COLOR : [MyColorTheme sharedInstance].onePxBorderColor;
            lineView.tag = RIGHT_LINE_VIEW_TAG;
            
            [view addSubview:lineView];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1
                                                              constant:LINE_WIDTH]];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0]];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeBottom
                                                            multiplier:1
                                                              constant:0]];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:lineView
                                                             attribute:NSLayoutAttributeRight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeRight
                                                            multiplier:1
                                                              constant:0]];
            
        }
        
        //[view setNeedsUpdateConstraints];
        //[view updateConstraints];
        
    } else {
        
        CALayer *existLineLayer = [view.layer valueForKey:RIGHT_LINE_LAYER_NAME];
        
        if (!existLineLayer) {
            
            CALayer *lineLayer = [CALayer layer];
            
            lineLayer.frame = CGRectMake(CGRectGetWidth(view.frame) - LINE_WIDTH, 0.0f, LINE_WIDTH, CGRectGetHeight(view.frame));
            lineLayer.backgroundColor = toolbar ? LINE_COLOR.CGColor : [MyColorTheme sharedInstance].onePxBorderColor.CGColor;
            lineLayer.name = RIGHT_LINE_LAYER_NAME;
            
            [view.layer addSublayer:lineLayer];
            [view.layer setValue:lineLayer forKey:RIGHT_LINE_LAYER_NAME];
            
            //NSLog(@"Top line added");
        } else {
            //NSLog(@"Top line exists");
        }
    }
}

@end
