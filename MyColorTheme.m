//
//  MyColorTheme.m
//  pd2
//
//  Created by admin on 6/2/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "MyColorTheme.h"
#import "WL.h"

@implementation MyColorTheme
{
    NSString *s1;
}

-(instancetype)init
{
    self=[super init];
    if (self)
    {
        //set theme here
        //[self setBlueTheme]; //- BD
        //[self setYellow]; //- SDN
        //[self setDeepOrange];
        //[self setYellowTheme];
        //[self setRedTheme];
        //[self setBrown];
        
        [self setThemeName:[WL themeName]];
        
    }
    return self;
}




-(void)setBlueTheme
{
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = NO;
    _navigaionButtonsColor=UIColorFromRGB(0xffffff);
    _menuButtonsColor=UIColorFromRGB(0xffffff);
    _headerBackGroundColor=UIColorFromRGB(0x265984);
    _textViewBackGroundColor=UIColorFromRGB(0xffffff);
    _textViewTextColor=UIColorFromRGB(0x6d6d72);
    _backgroundColor=UIColorFromRGB(0x265984);
    _spoilerBackGroundColor=UIColorFromRGB(0xefeff4);
    _spoilerTextColor=UIColorFromRGB(0x265984);
    _spoilerRoomTextColor=UIColorFromRGB(0x265984);
    _itemSelTextColor=UIColorFromRGB(0x6e6e73);
    _itemSelBackGroundColor=UIColorFromRGB(0xefeff4);
    _addSelTextColor=UIColorFromRGB(0x6e6e73);
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa);
    _editboxIconColor=UIColorFromRGB(0xffffff);
    _editBoxTitleColor=UIColorFromRGB(0xffffff);
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff);
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201);
    _buttonCancelTextColor=UIColorFromRGB(0xffffff);
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb01304);
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xffffff);
    _checkboxFieldIconColor=UIColorFromRGB(0x404040);
    _checkboxFieldTextColor=UIColorFromRGB(0x707074);
    _editboxIconColor=UIColorFromRGB(0x5b88aa);
    _subIconTextColor=UIColorFromRGB(0xffffff);
    _subButtonColor=UIColorFromRGB(0xfdeeee);
    _onePxBorderColor=UIColorFromRGB(0xc8c7cc);
    _toolbarBackgroundColor=UIColorFromRGB(0xefeff4);
    _toolbarLineColor=UIColorFromRGB(0xc8c7cc);
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    //_loginNavigationButtonColor=UIColorFromRGB(0xef5350);
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
    //_loginOnePxBorderColor=UIColorFromRGB(0xfdeeee);
    
    _pinPadSelectedSpoilerTextColor = _spoilerTextColor;
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    _selectedCellTextColor=UIColorFromRGB(0x265984); //Selected Permissions //Нет в приложении
    
    _selectedTabColor=UIColorFromRGB(0x4b688c);
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)setYellowTheme
{
    _statusBarLightContent=NO;
    _activityIndicatorLightContent = NO;
    _navigaionButtonsColor=UIColorFromRGB(0x404040);
    _headerBackGroundColor=UIColorFromRGB(0xffc000);
    _textViewBackGroundColor=UIColorFromRGB(0xffffff);
    _textViewTextColor=UIColorFromRGB(0x6d6d72);
    _backgroundColor=UIColorFromRGB(0xffc000);
    _spoilerTextColor=UIColorFromRGB(0xffc000);
    _spoilerBackGroundColor=UIColorFromRGB(0x404040);
    _spoilerRoomTextColor=UIColorFromRGB(0xffc000);
    _itemSelTextColor=UIColorFromRGB(0x6e6e73);
    _itemSelBackGroundColor=UIColorFromRGB(0xefeff4);
    _addSelTextColor=UIColorFromRGB(0x6e6e73);
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa);
    _editBoxTitleColor=UIColorFromRGB(0x6d6d72);
    _editboxIconColor=UIColorFromRGB(0xffffff);
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff);
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201);
    _buttonCancelTextColor=UIColorFromRGB(0xffffff);
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb01304);
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xffffff);
    _checkboxFieldIconColor=UIColorFromRGB(0x404040);
    _checkboxFieldTextColor=UIColorFromRGB(0x707074);
    _editboxIconColor=UIColorFromRGB(0xf7941d);
    _subIconTextColor=UIColorFromRGB(0x404040);
    _onePxBorderColor=UIColorFromRGB(0xc8c7cc);
    _toolbarLineColor=UIColorFromRGB(0xefeff4);
    
    //_pinPadSelectedSpoilerTextColor = _spoilerTextColor;
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault];
}


-(void)setRedTheme
{
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = YES;
    _navigaionButtonsColor=UIColorFromRGB(0xffffff);
    _menuButtonsColor=UIColorFromRGB(0xef5350);
    _headerBackGroundColor=UIColorFromRGB(0xef5350);
    //_headerBackGroundColor=UIColorFromRGB(0xef5350);
    _textViewBackGroundColor=UIColorFromRGB(0xfdeeee);
    _textViewTextColor=UIColorFromRGB(0x6d6d72);
    _backgroundColor=UIColorFromRGB(0xef5350);
    _spoilerTextColor=UIColorFromRGB(0xef5350);
    _spoilerBackGroundColor=UIColorFromRGB(0xffcdd2);
    _spoilerRoomTextColor=UIColorFromRGB(0xef5350);
    _itemSelTextColor=UIColorFromRGB(0x6e6e73);
    _itemSelBackGroundColor=UIColorFromRGB(0xefeff4);
    _addSelTextColor=UIColorFromRGB(0x6e6e73);
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa);
    _editBoxTitleColor=UIColorFromRGB(0xffffff);
    _editboxIconColor=UIColorFromRGB(0xffffff);
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff);
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201);
    _buttonCancelTextColor=UIColorFromRGB(0xffffff);
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb01304);
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xfdeeee);
    _checkboxFieldIconColor=UIColorFromRGB(0x404040);
    _checkboxFieldTextColor=UIColorFromRGB(0x404040);
    _editboxIconColor=UIColorFromRGB(0xef5350);
    _subIconTextColor=UIColorFromRGB(0xfdeeee);
    _subButtonColor=UIColorFromRGB(0xfdeeee);
    _onePxBorderColor=UIColorFromRGB(0xb71c1c);
    _toolbarBackgroundColor=UIColorFromRGB(0xefeff4);
    _toolbarLineColor=UIColorFromRGB(0xc8c7cc);
    _loginBackgroundColor=UIColorFromRGB(0xfdeeee);
    _loginTextColor=UIColorFromRGB(0xef5350);
    _loginNavigationButtonColor=UIColorFromRGB(0xef5350);
    _loginNavigationHeaderBackgroudColor=UIColorFromRGB(0xfdeeee);
    _loginOnePxBorderColor=UIColorFromRGB(0xfdeeee);
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
    _selectedTabColor=UIColorFromRGB(0xf7a9a8);
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
}


- (NSArray *)themesList {
    return @[@"1 - Blue",
             @"2 - Yellow",
             @"3 - Green",
             @"4 - Winterroad",
             @"5 - Red400",
             @"6 - BlueGray",
             @"7 - Brown",
             @"8 - Cyan",
             @"9 - DeepOrange",
             @"12 - Indigo",
             @"23 - SunGrass",
             @"26 - Raspberries",
             @"28 - DeepNight",
             @"39 - Light7"];
}

- (void)setThemeName:(NSString *)name {
    
    if ([name isEqualToString:@"0 - Blue"]) {
        [self setBlueTheme];
    }
    else if ([name isEqualToString:@"1 - Blue"]) {
        [self setBlue];
    }
    else if ([name isEqualToString:@"2 - Yellow"]) {
        [self setYellow];
    }
    else if ([name isEqualToString:@"3 - Green"]) {
        [self setGreen];
    }
    else if ([name isEqualToString:@"4 - Winterroad"]) {
        [self setWinterroad];
    }
    else if ([name isEqualToString:@"5 - Red400"]) {
        [self setRed400];
    }
    else if ([name isEqualToString:@"6 - BlueGray"]) {
        [self setBlueGray];
    }
    else if ([name isEqualToString:@"7 - Brown"]) {
        [self setBrown];
    }
    else if ([name isEqualToString:@"8 - Cyan"]) {
        [self setCyan];
    }
    else if ([name isEqualToString:@"9 - DeepOrange"]) {
        [self setDeepOrange];
    }
    else if ([name isEqualToString:@"12 - Indigo"]) {
        [self setIndigo];
    }
    else if ([name isEqualToString:@"23 - SunGrass"]) {
        [self setSunGrass];
    }
    else if ([name isEqualToString:@"26 - Raspberries"]) {
        [self setRaspberries];
    }
    else if ([name isEqualToString:@"28 - DeepNight"]) {
        [self setDeepNight];
    }
    else if ([name isEqualToString:@"39 - Light7"]) {
        [self setLight7];
    }
}




- (void)setBlueGray { //6
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = YES; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0xffffff); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0x546e7a); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xeceff1); //Text pole background
    _textViewTextColor=UIColorFromRGB(0x455a64); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0x455a64 ); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0xe37474f); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0xb0bec5); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0xffffff); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0x546e7a); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xeceff1); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0x475c66); //Checkbox icon (???image???)
    _checkboxFieldTextColor=UIColorFromRGB(0x475c66); //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0x546e7a); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=[UIColor whiteColor]; //Button decline text
    _subButtonColor=UIColorFromRGB(0xeceff1); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0x455a64); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xeceff1); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0x455a64); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0x546e7a); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x6d6d72); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0x6d6d72); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xc8c7cc); //Button Border
    
    _menuButtonsColor=_navigaionButtonsColor; //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0xef5350); //SubSpoiler Border //нет в приложении
    
    
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
    
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
}



- (void)setBlue { //1
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = YES; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0xffffff); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0x265984); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xffffff); //Text pole background
    _textViewTextColor=UIColorFromRGB(0x6d6d72); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0xc8c7cc); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0x5b88aa); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0xefeff4); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0xffffff); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0x265984); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xffffff); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0x404040); //Checkbox icon (???image???)
    _checkboxFieldTextColor=UIColorFromRGB(0x404040); //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0x265984); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=UIColorFromRGB(0xffffff); //Button decline text
    _subButtonColor=UIColorFromRGB(0xffffff); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0x404040); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xffffff); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0xc8c7cc); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0x265984); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x6d6d72); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0xefeff4); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xc8c7cc); //Button Border
    
    _menuButtonsColor=_navigaionButtonsColor; //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0xc8c7cc); //SubSpoiler Border //нет в приложении
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
    
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
    
}


- (void)setYellow { //2
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault];
    _statusBarLightContent=NO;
    _activityIndicatorLightContent = NO; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0x404040); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0xffc000); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xffffff); //Text pole background
    _textViewTextColor=UIColorFromRGB(0x6d6d72); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0xc8c7cc); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0x6d6d72); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0xf8e099); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0x6d6d72); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0xffc000); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xffffff); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0x404040); //Checkbox icon (???image???)
    _checkboxFieldTextColor=UIColorFromRGB(0x404040); //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0xffc000); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=UIColorFromRGB(0xffffff); //Button decline text
    _subButtonColor=UIColorFromRGB(0xfdffb4); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0x404040); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xffffff); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0xc8c7cc); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0x404040); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x6d6d72); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0xefeff4); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xc8c7cc); //Button Border
    
    _menuButtonsColor=UIColorFromRGB(0xffffff); //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0xc8c7cc); //SubSpoiler Border //нет в приложении
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    //_loginTextColor=_navigaionButtonsColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
    
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
}


- (void)setGreen { //3
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = YES; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0xffffff); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0x168039); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xeafbe7); //Text pole background
    _textViewTextColor=UIColorFromRGB(0x6d6d72); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0x044c29); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0x168039); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0x96ed89); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0xffffff); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0x168039); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xeafbe7); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0x404040); //Checkbox icon (???image???)
    _checkboxFieldTextColor=UIColorFromRGB(0x404040); //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0x168039); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=UIColorFromRGB(0xffffff); //Button decline text
    _subButtonColor=UIColorFromRGB(0xfdffb4); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0x404040); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xeafbe7); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0x044c29); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0x168039); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x6d6d72); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0xefeff4); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xc8c7cc); //Button Border
    
    _menuButtonsColor=_navigaionButtonsColor; //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0x044c29); //SubSpoiler Border //нет в приложении
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
    
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
}


- (void)setWinterroad { //4
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = YES; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0xffffff); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0x425955); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xf1f2d8); //Text pole background
    _textViewTextColor=UIColorFromRGB(0x6d6d72); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0x676656); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0x425955); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0xbfbd9f); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0xffffff); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0x425955); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xf1f2d8); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0x404040); //Checkbox icon (???image???)
    _checkboxFieldTextColor=UIColorFromRGB(0x404040); //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0x425955); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=UIColorFromRGB(0xffffff); //Button decline text
    _subButtonColor=UIColorFromRGB(0xf1f2d8); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0x404040); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xf1f2d8); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0x676656); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0x425955); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x6d6d72); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0xefeff4); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xc8c7cc); //Button Border
    
    _menuButtonsColor=_navigaionButtonsColor; //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0x676656); //SubSpoiler Border //нет в приложении
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
    
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
}


- (void)setRed400 { //5
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = YES; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0xffffff); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0xef5350); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xfdeeee); //Text pole background
    _textViewTextColor=UIColorFromRGB(0x6d6d72); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0xb71c1c); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0xef5350); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0xffcdd2); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0xffffff); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0xef5350); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xfdeeee); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0x404040); //Checkbox icon (???image???)
    _checkboxFieldTextColor=UIColorFromRGB(0x404040); //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0xef5350); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=UIColorFromRGB(0xffffff); //Button decline text
    _subButtonColor=UIColorFromRGB(0xfdeeee); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0x404040); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xfdeeee); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0xb71c1c); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0xef5350); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x6d6d72); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0xefeff4); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xc8c7cc); //Button Border
    
    _menuButtonsColor=_navigaionButtonsColor; //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0xb71c1c); //SubSpoiler Border //нет в приложении
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
 
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
}


- (void)setBrown { //7
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = YES; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0xffffff); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0x6d4c41); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xefebe9); //Text pole background
    _textViewTextColor=UIColorFromRGB(0x8d6e63); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0x4e342e ); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0x4e342e); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0xbcaaa4); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0xffffff); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0x6d4c41); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xefebe9); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0x6d4c41); //Checkbox icon (???image???)
    _checkboxFieldTextColor=_checkboxFieldIconColor; //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0x6d4c41); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=UIColorFromRGB(0xffffff); //Button decline text
    _subButtonColor=UIColorFromRGB(0xefebe9); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0x6d4c41); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xefebe9); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0x4e342e); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0x6d4c41); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x6d6d72); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0xefeff4); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xc8c7cc); //Button Border
    
    _menuButtonsColor=_navigaionButtonsColor; //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0xef5350); //SubSpoiler Border //нет в приложении
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
    
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
}


- (void)setCyan { //8
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = YES; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0xffffff); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0x00acc1); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xe0f7fa); //Text pole background
    _textViewTextColor=UIColorFromRGB(0x00bcd4); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0x00838f); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0x6064); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0x80deea); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0xffffff); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0x00acc1); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xe0f7fa); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0x00838f); //Checkbox icon (???image???)
    _checkboxFieldTextColor=_checkboxFieldIconColor; //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0x00acc1); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=UIColorFromRGB(0xffffff); //Button decline text
    _subButtonColor=UIColorFromRGB(0xe0f7fa); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0x00838f); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xe0f7fa); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0x00838f); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0x00acc1); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x6d6d72); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0xefeff4); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xc8c7cc); //Button Border
    
    _menuButtonsColor=_navigaionButtonsColor; //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0xef5350); //SubSpoiler Border //нет в приложении
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
    
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
}

- (void)setDeepOrange { //9
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = YES; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0xffffff); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0xe64a19); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xfbe9e7); //Text pole background
    _textViewTextColor=UIColorFromRGB(0xff8a65); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0xbf360c); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0xbf360c); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0xffab91); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0xffffff); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0xe64a19); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xfbe9e7); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0xff7043); //Checkbox icon (???image???)
    _checkboxFieldTextColor=_checkboxFieldIconColor; //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0xe64a19); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=UIColorFromRGB(0xffffff); //Button decline text
    _subButtonColor=UIColorFromRGB(0xe0f7fa); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0xff7043); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xfbe9e7); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0xbf360c); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0xe64a19); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x6d6d72); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0xefeff4); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xc8c7cc); //Button Border
    
    _menuButtonsColor=_navigaionButtonsColor; //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0xef5350); //SubSpoiler Border //нет в приложении
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
    
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
}

- (void)setIndigo { //12
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = YES; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0xffffff); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0x3949ab); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xe8eaf6); //Text pole background
    _textViewTextColor=UIColorFromRGB(0x5c6bc0); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0x1a237e); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0x283593); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0x9fa8da); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0xffffff); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0x3949ab); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xe8eaf6); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0x7986cb); //Checkbox icon (???image???)
    _checkboxFieldTextColor=_checkboxFieldIconColor; //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0x3949ab); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=UIColorFromRGB(0xffffff); //Button decline text
    _subButtonColor=UIColorFromRGB(0xe8eaf6); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0x7986cb); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xe8eaf6); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0x1a237e); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0x3949ab); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x6d6d72); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0xefeff4); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xc8c7cc); //Button Border
    
    _menuButtonsColor=_navigaionButtonsColor; //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0xef5350); //SubSpoiler Border //нет в приложении
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
    
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
}

- (void)setSunGrass { //23
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = YES; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0xffffff); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0x628338); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xecf1e7); //Text pole background
    _textViewTextColor=UIColorFromRGB(0x96af75); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0x566d33); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0x49592f); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0x96af75); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0xffffff); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0x628338); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xecf1e7); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0x628338); //Checkbox icon (???image???)
    _checkboxFieldTextColor=_checkboxFieldIconColor; //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0x618138); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=UIColorFromRGB(0xffffff); //Button decline text
    _subButtonColor=UIColorFromRGB(0xf8edf1); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0x628338); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xecf1e7); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0x566d33); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0x628338); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x818483); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0xf2f5f4); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xcfd2d1); //Button Border
    
    _menuButtonsColor=_navigaionButtonsColor; //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0xef5350); //SubSpoiler Border //нет в приложении
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
    
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
}

- (void)setRaspberries { //26
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = YES; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0xffffff); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0xa25677); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xf4eaee); //Text pole background
    _textViewTextColor=UIColorFromRGB(0xc489a2); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0x8e536e); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0x7c5166); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0xc489a2); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0xffffff); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0xa25677); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xf4eaee); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0xa25677); //Checkbox icon (???image???)
    _checkboxFieldTextColor=_checkboxFieldIconColor; //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0x9f5575); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=UIColorFromRGB(0xffffff); //Button decline text
    _subButtonColor=UIColorFromRGB(0xeff2fa); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0xa25677); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xf4eaee); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0x8e536e); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0xa25677); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x9b9a99); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0xf6f5f4); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xdad9d8); //Button Border
    
    _menuButtonsColor=_navigaionButtonsColor; //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0xef5350); //SubSpoiler Border //нет в приложении
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
    
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
}

- (void)setDeepNight { //28
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    _statusBarLightContent=YES;
    _activityIndicatorLightContent = YES; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0xffffff); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0x3a3d61); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xe7e7ec); //Text pole background
    _textViewTextColor=UIColorFromRGB(0x787a96); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0x2f324d); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0x25283b); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0x787a96); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0xffffff); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0x3a3d61); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xe7e7ec); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0x3a3d61); //Checkbox icon (???image???)
    _checkboxFieldTextColor=_checkboxFieldIconColor; //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0x3a3d60); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=UIColorFromRGB(0xffffff); //Button decline text
    _subButtonColor=UIColorFromRGB(0xeef4ef); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0x3a3d61); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xe7e7ec); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0x2f324d); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0x3a3d61); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x777577); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0xf3f1f3); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xcccbcc); //Button Border
    
    _menuButtonsColor=_navigaionButtonsColor; //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0xef5350); //SubSpoiler Border //нет в приложении
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
    
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
}


- (void)setLight7 { //30
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault];
    _statusBarLightContent=NO;
    _activityIndicatorLightContent = NO; //нет в списке
    
    
    _navigaionButtonsColor=UIColorFromRGB(0x37618d); //Navigation buttons
    
    _headerBackGroundColor=UIColorFromRGB(0xc7d6e5); //Header background
    _textViewBackGroundColor=UIColorFromRGB(0xf9f9f9); //Text pole background
    _textViewTextColor=UIColorFromRGB(0x717171); //Text pole text
    _onePxBorderColor=UIColorFromRGB(0xb5b5b5); //1 pixel border
    _spoilerTextColor=UIColorFromRGB(0x38db92); //Spoiler text
    _spoilerBackGroundColor=UIColorFromRGB(0x37618d); //Spoiler background
    _editBoxTitleColor=UIColorFromRGB(0x37618d); //Editbox text
    _editboxIconColor=UIColorFromRGB(0xffffff); //Editbox
    
    _backgroundColor=UIColorFromRGB(0xc7d6e5); //Main background
    _checkboxFieldBackGroundColor=UIColorFromRGB(0xf9f9f9); //Checkbox background
    _checkboxFieldIconColor=UIColorFromRGB(0x4b7cae); //Checkbox icon (???image???)
    _checkboxFieldTextColor=_checkboxFieldIconColor; //Checkbox text
    
    _editboxIconColor=UIColorFromRGB(0x37608a); //Editbox icon (???image???)
    _buttonConfirmBackgroundColor=UIColorFromRGB(0x73b201); //Button confirm background
    _buttonConfirmTextColor=UIColorFromRGB(0xffffff); //Button text
    _buttonCancelBackgroundColor=UIColorFromRGB(0xb00901); //Button decline background
    _buttonCancelTextColor=UIColorFromRGB(0xffffff); //Button decline text
    _subButtonColor=UIColorFromRGB(0x4b7cae); //Circle icon
    _subIconTextColor=_subButtonColor; //Sub icon text
    
    _itemSelTextColor=UIColorFromRGB(0x4b7cae); //Drop Down Select Menu Text
    _itemSelBackGroundColor=UIColorFromRGB(0xf9f9f9); //Drop Down Select Menu BG
    _itemSelBackGroundBorderColor=UIColorFromRGB(0xb5b5b5); //Drop Down Select Menu Border
    _addSelTextColor=_itemSelTextColor; //нет в списке
    _addSelBackGroundColor=UIColorFromRGB(0xfffafa); //нет в списке
    
    _selectedCellTextColor=UIColorFromRGB(0x4b7cae); //Selected Permissions //Нет в приложении
    _toolbarButtonColor=UIColorFromRGB(0x202020); //Button Image //Нет в приложении
    _toolbarBackgroundColor=UIColorFromRGB(0xdcdcdc); //Button Background
    _toolbarLineColor=UIColorFromRGB(0xb5b5b5); //Button Border
    
    _menuButtonsColor=_navigaionButtonsColor; //нет в списке
    
    
    _selectedTabColor=UIColorFromRGB(0xf7a9a8); //нет в списке. Выбранный таб в прогрессе
    
    
    _spoilerRoomTextColor=_spoilerTextColor; //SubSpoiler Text
    _spoilerRoomBackgroundColor=_headerBackGroundColor; //SubSpoiler BG //нет в приложении
    _spoilerRoomBorderColor=UIColorFromRGB(0xef5350); //SubSpoiler Border //нет в приложении
    
    _pickerButtonTextColor = _spoilerTextColor;
    
    //Когда loginPage была другого цвета:
    _loginBackgroundColor=_backgroundColor;
    _loginTextColor=_navigaionButtonsColor;
    _loginNavigationHeaderBackgroudColor=_headerBackGroundColor;
 
    _pinPadSelectedSpoilerTextColor = _loginTextColor;
}

-(void)setColorThemeForViewController:(UIViewController* )controller
{
    controller.view.backgroundColor=[[MyColorTheme sharedInstance]backgroundColor];
    controller.navigationController.navigationBar.barTintColor = [[MyColorTheme sharedInstance] headerBackGroundColor];
}

@end
