//
//  HeaderCell.m
//  pd2
//
//  Created by i11 on 14/04/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "HeaderCell.h"
#import "MyColorTheme.h"

@implementation HeaderCell

- (void)awakeFromNib {
    
    //Обязательное удаление gestureRecognizers, иначе при longPress crash
    while (self.contentView.gestureRecognizers.count) {
        [self.contentView removeGestureRecognizer:[self.contentView.gestureRecognizers objectAtIndex:0]];
    }
    
    //self.backgroundColor = [[MyColorTheme sharedInstance] backgroundColor];
    //mask image
    
}

//TODO: isHideMethod

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
