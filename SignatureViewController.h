//
//  SignatureViewController.h
//  pd2
//
//  Created by i11 on 18/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "Pd2ViewController.h"
#import "CancelButton.h"
#import "ConfirmButton.h"
@class SignatureView;

#warning работа класса еще не тестировалась! Пока не использовать

//********************************************************
//Класс xib для обработки/создания signature на основе кода Vasyl Balazh
//Исользуется в Visitors storyboard.

//Тестируется, о найденных багах и желаемых фичах - сообщать или совместно править.
//Андрей
//********************************************************

@interface SignatureViewController : Pd2ViewController

@property (nonatomic, weak) IBOutlet SignatureView *signatureView;
@property (nonatomic, weak) IBOutlet ConfirmButton *saveButton;
@property (nonatomic, weak) IBOutlet CancelButton *clearButton;
@property (nonatomic, weak) IBOutlet UIImageView *signIconImageView;



@property (nonatomic, assign) BOOL isPresented;
@property (nonatomic, strong) UIImage *signatureImage;

@property (nonatomic, copy) void (^signatureImageCompletion)(UIImage *image);

- (IBAction)saveButtonTapped:(id)sender;
- (IBAction)resetButtonTapped:(id)sender;
- (IBAction)backButtonTapped:(id)sender;


@end
