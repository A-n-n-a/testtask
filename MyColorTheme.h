//
//  MyColorTheme.h
//  pd2
//
//  Created by admin on 6/2/14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


#import <Foundation/Foundation.h>
#import "Singleton.h"
#import "NoteView.h"

@protocol MyColorTheme
+(UIColor *) getNavbarColor;
+(void) EditBoxStyle: (UITextField *) txt;
@end

@interface MyColorTheme : Singleton

@property BOOL statusBarLightContent;
@property BOOL activityIndicatorLightContent;

@property (nonatomic, retain) UIColor *navigaionButtonsColor;
@property (nonatomic, retain) UIColor *headerBackGroundColor;

@property (nonatomic, retain) UIColor *menuButtonsColor;

@property (nonatomic, retain) UIColor *textViewBackGroundColor;
@property (nonatomic, retain) UIColor *textViewTextColor;

@property (nonatomic, retain) UIColor *backgroundColor;

@property (nonatomic, retain) UIColor *spoilerTextColor;
@property (nonatomic, retain) UIColor *spoilerBackGroundColor;

@property (nonatomic, retain) UIColor *spoilerRoomTextColor;
@property (nonatomic, retain) UIColor *spoilerRoomBackgroundColor;
@property (nonatomic, retain) UIColor *spoilerRoomBorderColor;

@property (nonatomic, retain) UIColor *itemSelTextColor;
@property (nonatomic, retain) UIColor *itemSelBackGroundColor;
@property (nonatomic, retain) UIColor *itemSelBackGroundBorderColor;
@property (nonatomic, retain) UIColor *addSelTextColor;
@property (nonatomic, retain) UIColor *addSelBackGroundColor;

@property (nonatomic, retain) UIColor *onePxBorderColor;

@property (nonatomic, retain) UIColor *editBoxTitleColor;

@property (nonatomic, retain) UIColor *buttonConfirmTextColor;
@property (nonatomic, retain) UIColor *buttonConfirmBackgroundColor;

@property (nonatomic, retain) UIColor *buttonCancelTextColor;
@property (nonatomic, retain) UIColor *buttonCancelBackgroundColor;

@property (nonatomic, retain) UIColor *checkboxFieldBackGroundColor;
@property (nonatomic, retain) UIColor *checkboxFieldTextColor;
@property (nonatomic, retain) UIColor *checkboxFieldIconColor;

@property (nonatomic, retain) UIColor *editboxIconColor;

@property (nonatomic, retain) UIColor *subIconTextColor;
@property (nonatomic, retain) UIColor *subButtonColor;

@property (nonatomic, retain) UIColor *selectedCellTextColor;
@property (nonatomic, retain) UIColor *toolbarButtonColor;
@property (nonatomic, retain) UIColor *toolbarBackgroundColor;
@property (nonatomic, retain) UIColor *toolbarLineColor;

@property (nonatomic, retain) UIColor *loginBackgroundColor;
@property (nonatomic, retain) UIColor *loginTextColor;

@property (nonatomic, retain) UIColor *loginNavigationButtonColor;
@property (nonatomic, retain) UIColor *loginNavigationHeaderBackgroudColor;
@property (nonatomic, retain) UIColor *loginOnePxBorderColor;

@property (nonatomic, retain) UIColor *pinPadSelectedSpoilerTextColor;

@property (nonatomic, retain) UIColor *selectedTabColor;


@property (nonatomic, retain) UIColor *pickerButtonTextColor;

- (NSArray *)themesList;
- (void)setThemeName:(NSString *)name;

-(void)setBlueTheme;
-(void)setYellowTheme;

-(void)setColorThemeForViewController:(UIViewController *)controller;

@end
