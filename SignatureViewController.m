//
//  SignatureViewController.m
//  pd2
//
//  Created by i11 on 18/05/15.
//  Copyright (c) 2015 Admin. All чrights reserved.
//

#import "SignatureViewController.h"
#import "SignatureView.h"
#import "Utils.h"
#import "DataStateManager.h"
#import "MyColorTheme.h"

@interface SignatureViewController ()

@end

@implementation SignatureViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTopTitle:@"Add Signature"];
    
    [self setupSignatureViewDesign];
    self.isPresented = YES;
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationDidChange:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    self.signIconImageView.image = [self.signIconImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.signIconImageView.tintColor = [[MyColorTheme sharedInstance].backgroundColor colorWithAlphaComponent:0.3f];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
    NSNumber *value = [NSNumber numberWithInt:UIDeviceOrientationLandscapeLeft];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}


- (void)setupSignatureViewDesign
{
    [self.signatureView clear];
    [self.signatureView setLineWidth:2.0];
    self.signatureView.foregroundLineColor = [UIColor blackColor];
    self.signatureView.layer.cornerRadius = 4.0f;
    self.signatureView.layer.masksToBounds = YES;
}


- (void)orientationDidChange:(NSNotification*)notification
{
//    [self.signatureView clear];
}


#pragma mark - Actions

- (IBAction)saveButtonTapped:(id)sender
{

    if (self.signatureImageCompletion) {
        
        UIImage *image;
        
        if ([self.signatureView isSigned]) {
            image = [self.signatureView signatureImage];
        }
        
        self.signatureImageCompletion(image);
    }
    [self forceRotate];
    
    //TODO: в принимаем классе сохранять блок и вызывать при viewDidAppear, иначе UITableView не отрисована и не все методы работают
    [self.navigationController popViewControllerAnimated:YES ];
    
}


- (IBAction)resetButtonTapped:(id)sender
{
    [self.signatureView clear];
}


- (IBAction)backButtonTapped:(id)sender
{
    [self.signatureView clear];
    [self forceRotate];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)forceRotate
{
    //hack
    UIViewController *c = [[UIViewController alloc]init];
    [self presentViewController:c animated:NO completion:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}


#pragma mark - ViewWillDisappear

- (void)viewWillDisappear:(BOOL)animated
{
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    self.isPresented = NO;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
