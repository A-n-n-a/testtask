//
//  MainSideViewController.h
//  pd2
//
//  Created by i11 on 30/07/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainSideViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end
