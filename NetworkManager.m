//
//  NetworkManager.m
//  pd2
//
//  Created by User on 17.06.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//


#import "NetworkManager.h"
#import "ImageCache.h"
#import "MBProgressHUD.h"
#import "UIImage+ProportionalFill.h"
#import "ChildrenBirthdaysModel.h"
#import "UserManager.h"
#import "NSString+Utils.h"
#import "NSString+RemoveEmoji.h"
#import "NSDictionary+JSON.h"
#import "NSArray+JSON.h"
#import "NSDateFormatter+Locale.h"
#import "TimerUIApplication.h"
#import "NSDate+Utilities.h"
#import "UIImage+fixOrientation.h"

#import "NSString+URLEncoding.h"


#import "ParentModel.h"
#import "ProfileModel.h"
#import "AuthorisedPersonModel.h"





#import <sys/utsname.h> //device model

NSString * const NetworkManagerVideoServerHostInfoSuccessNotification = @"NetworkManagerVideoServerHostInfoSuccessNotification";
NSString * const NetworkManagerVideoServerHostInfoFailNotification = @"NetworkManagerVideoServerHostInfoFailNotification";


@interface NSDictionary(URL)
- (NSString *) bindToUrl:(NSString *)url;
@end

@implementation NSDictionary (URL)

- (NSString *) bindToUrl:(NSString *)url
{
    __block NSString * requestUrl = url;
    [[self allKeys] enumerateObjectsUsingBlock:^(NSString * obj, NSUInteger idx, BOOL *stop) {
        NSString * append = [NSString stringWithFormat:@"&%@=%@", obj, self[obj]];
        requestUrl = [requestUrl stringByAppendingString:append];
    }];
    return requestUrl;
}

@end


@interface NetworkManager ()

@property (nonatomic, strong) NSString *networkPrefix;

@end

@implementation NetworkManager


NSString* deviceName()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}


//TODO: выбрать основной из AFHTTPRequestOperationManager, AFHTTPRequestOperation, AFHTTPSessionManager


- (BOOL)isJ3 {
    
    return NO;
    
}

-(id)init
{
    self =[super init];
    if (self) {
        //На время тестирования appium+selenium - http
        self.networkPrefix = @"https://";
        
        self.baseUrl=@"iosdevbritish.mybabysdays.com";
        
        self.isBeta = YES;
        
        //Избегаем части проблем с невалидным ssl сертификатом
        AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc] init];
        [securityPolicy setAllowInvalidCertificates:YES];
    
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager setSecurityPolicy:securityPolicy];
        
        
        self.requestInfoLog = ^(AFHTTPRequestOperation * operation){
            
            NSLog(@"--request headers:-------------------------------\n%@",[operation.request allHTTPHeaderFields]);
            NSLog(@"--request response string:-----------------------\n%@", operation.responseString);
            NSLog(@"--request string:--------------------------------\n%@",operation.request.URL.absoluteString);
            NSLog(@"--Body:------------------------------------------\n%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
            NSLog(@"--Body end---------------------------------------\n");
        };
    }
    return self;
}

//TMP
/*
- (BOOL)isBeta {
    return YES;
}
*/

-(void)setBaseUrl:(NSString *)baseUrl
{
    _subdomain = baseUrl;
    _baseUrl=[NSString stringWithFormat:@"%@%@", self.networkPrefix, baseUrl];
}

- (NSString *)urlForSubdomain:(NSString *)subdomain
{
    return [NSString stringWithFormat:@"%@%@", self.networkPrefix, subdomain];
}


- (void)storeVideoServerHost {
    
    
    
    
    
}


- (NSMutableData *)generatePostDataForDictionary:(NSDictionary *)dict withBoundary:(NSString *)boundary close:(BOOL)isClose {
    
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *key in [dict allKeys]) {
        
        if ([[[dict allKeys] firstObject] isEqual:key]) {
            //Если первый
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        else {
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n%@", key, [dict objectForKey:key]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    if (isClose) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    return body;
    
}


- (void)updateSessionTimer {
    [[TimerUIApplication sharedInstance] updateNetworkActivity];
}


#pragma mark - WhiteLabel Hosts

-(void)getDomainsListFor:(int)index withCompletion:(void (^)(id response, NSError *error))completion {
    NSLog(@"get domian list");
    
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_billing&controller=demo&task=loginCMD&uid=%i&lang=en",@"https://www.babysdays.com",index];
    //NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_billing&controller=demo&task=loginCMD&uid=%i&lang=en",@"https://theartofscalability.org",index];
    //NSString *requestString = @"https://theartofscalability.org/index.php?option=com_server&controller=demo&task=loginCMD&uid=19&auth=test&lang=en";
    
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         self.requestInfoLog(operation);
         
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         self.requestInfoLog(operation);
         
         if (completion)
         {
             completion(nil,error);
         }
         
     }];
    
    [operation start];
}



#pragma mark - Admins

-(void)getAdminListAndShowProgressHUD:(BOOL)showProgressHUD withCompletion:(void (^)(id response, NSError *error))completion
{
    NSLog(@"get admin list");
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=admin&task=listSted&access_token=%@",_baseUrl,[NetworkManager sharedInstance].token];

    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [AppDelegate hideProgressHUD];
        [self updateSessionTimer];
        NSLog(@"get admins success!:%@",responseObject);
        NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
        NSLog(@"request string:%@",operation.request.URL.absoluteString);
        NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);

        [DataManager sharedInstance].admins=[responseObject objectForKey:@"adminList"];
        if (completion)
        {
            completion(responseObject,nil);
        }
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [AppDelegate hideProgressHUD];
        NSLog(@"get admins fail%@",error);
        NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
        NSLog(@"request string:%@",operation.request.URL.absoluteString);
        NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);

        if (completion)
        {
            completion(nil,error);
        }

    }];
    if (showProgressHUD) {
        [AppDelegate showProgressHUD];
    }

    [operation start];
}

//option=com_sted_mobile&controller=admin&task=register_save&username=admin123&first_name=First+Name&last_name=Last+Name&email=mail%40mail.com&password=123123&password2=123123&file=&filename=shot0001.png&acl=1&room=0&position=1

-(void)registerAdminWithFirstName:(NSString *)fname lastName:(NSString *)lName email:(NSString *)emailStr username:(NSString *)userName password:(NSString *)password accessLevel:(int)accLevel position:(int)position room:(int)room image:(UIImage *)image completion:(void (^)(id, NSError *))completion
{
    NSLog(@"register admin");
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php",_baseUrl];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithDictionary:@{@"option":@"com_sted_mobile",
                                                                              @"controller":@"admin",
                                                                              @"task":@"register_save",
                                                                              @"username":[userName stringByRemovingEmoji],
                                                                              @"first_name":[fname stringByRemovingEmoji],
                                                                              @"last_name":[lName stringByRemovingEmoji],
                                                                              @"email":[emailStr stringByRemovingEmoji],
                                                                              @"password":password,
                                                                              @"password2":password,
                                                                              @"acl":[NSNumber numberWithInt:accLevel],
                                                                              @"room":[NSNumber numberWithInt:room],
                                                                              @"position":[NSNumber numberWithInt:position]
                                                                              }];
    if (image)
    {
        [dict setValue:[[Utils sharedInstance] base64StringFromImage:image] forKey:@"file"];
    }
    
    NSString *dataString=[[Utils sharedInstance]parametersStringFromDictionary:dict];
    dataString = [dataString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSLog(@"data string :%@",dataString);
    NSData *data=[dataString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         
         self.requestInfoLog (operation);
         
         NSLog(@"register admin success!:%@",responseObject);
         NSLog(@"register admin response string %@", operation.responseString);
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);

         if (completion)
         {
             completion(responseObject,nil);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];
         self.requestInfoLog (operation);
         NSLog(@"register admin fail%@",error);
         NSLog(@"Response data: %@",operation.responseData);
         NSLog(@"register admin response string %@", operation.responseString);
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);

         if (completion)
         {
             completion(nil,error);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];
}

-(void)updateAdminWithId:(int)adminId firstName:(NSString *)fname lastName:(NSString *)lName email:(NSString *)emailStr username:(NSString *)userName password:(NSString *)password accessLevel:(int)accLevel position:(int)position room:(int)room image:(UIImage *)image completion:(void (^)(id, NSError *))completion
{
    NSLog(@"update admin");
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?",_baseUrl];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithDictionary:@{@"option":@"com_sted_mobile",
                                                                              @"controller":@"admin",
                                                                              @"task":@"save_sted",
                                                                              @"username":[userName stringByRemovingEmoji],
                                                                              @"first_name":[fname stringByRemovingEmoji],
                                                                              @"last_name":[lName stringByRemovingEmoji],
                                                                              @"email":[emailStr stringByRemovingEmoji],
                                                                              @"password":password,
                                                                              @"password2":password,
                                                                              @"acl":[NSNumber numberWithInt:accLevel],
                                                                              @"room":[NSNumber numberWithInt:room],
                                                                              @"position":[NSNumber numberWithInt:position],
                                                                              @"id":[NSNumber numberWithInt: adminId],
                                                                              @"sess":self.token,
                                                                              @"access_token":self.token
                                                                              
                                                                              }];
    if (image)
    {
        [dict setValue:[[Utils sharedInstance] base64StringFromImage:image] forKey:@"file"];
    }

    NSString *dataString=[[Utils sharedInstance]parametersStringFromDictionary:dict];
    //dataString=[dataString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    //dataString=[dataString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"data string :%@",dataString);
    NSData *data=[dataString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         self.requestInfoLog(operation);
         NSLog(@"update admin success!:%@",responseObject);
         if (completion)
         {
             completion(responseObject,nil);
         }
         NSString *str= [[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding];
         NSLog(@"body:%@",str);
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];
         self.requestInfoLog(operation);
         NSLog(@"update admin fail%@",error);
         NSLog(@"headers:%@",[operation.request allHTTPHeaderFields]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];
}

-(void)deleteAdminWithId:(int)admin_id completion:(void (^)(id response, NSError *error))completion
{
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=admin&task=deleteAdmin&id=%d&access_token=%@",_baseUrl, admin_id,[NetworkManager sharedInstance].token];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];

         NSLog(@"delete admin success!:%@",responseObject);
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);

         if (completion)
         {
             completion(responseObject,nil);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];

         NSLog(@"delete admin fail%@",error);
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];
}

-(void)getAdminWithId:(int)admin_id completion:(void (^)(id response, NSError *error))completion
{
    //http://ios.pleasure2deal.com/index.php?option=com_sted_mobile&controller=admin&task=getAdminById&id=63
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=admin&task=getAdminById&id=%d&access_token=%@",_baseUrl, admin_id,[NetworkManager sharedInstance].token];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];

         NSLog(@"get admin success!:%@",responseObject);
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];

         NSLog(@"get admin fail%@",error);
         NSLog(@"headers:%@",[operation.request allHTTPHeaderFields]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];

}



#pragma mark - Children

-(void)getChildrenListWithCompletion:(void (^)(id response, NSError *error))completion
{
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=sted_child_manage&task=child_manage&limitstart=0&access_token=%@",_baseUrl,[NetworkManager sharedInstance].token];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         
         [NetworkManager sharedInstance].requestInfoLog(operation);
         
         NSLog(@"get children success!:%@",responseObject);
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
         
         [DataManager sharedInstance].childrenList=[responseObject valueForKeyPath:@"result_active.child_list"];
         
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];
         
         [NetworkManager sharedInstance].requestInfoLog(operation);
         
         NSLog(@"get children fail%@",error);
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
//    [AppDelegate showProgressHUD];
    [operation start];
}

-(void)getChildInfo:(int)childId completion:(void (^)(id response, NSError *error))completion
{
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=sted_child_manage&task=viewChild&id=%d&access_token=%@",_baseUrl,childId,[NetworkManager sharedInstance].token];
        
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         
         NSLog(@"getChildInfo success!");
         self.requestInfoLog(operation);
         
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];

         NSLog(@"getChildInfo fail");
         self.requestInfoLog(operation);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];

}


-(void)createChildWithOptions:(NSDictionary*)options completion:(void (^)(id response, NSError *error))completion
{
    NSLog(@"create child with parameters:%@",options);
    
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php",_baseUrl];
    
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    NSString *dataString=[[Utils sharedInstance] parametersStringFromDictionary:options];
    NSData *data=[dataString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         self.requestInfoLog(operation);
         NSLog(@"add child success!:%@",responseObject);
         if (completion)
         {
             completion(responseObject,nil);
             NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
             NSLog(@"request string:%@",operation.request.URL.absoluteString);
             NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];
         self.requestInfoLog(operation);
         NSLog(@"add child fail%@",error);
         if (completion)
         {
             completion(nil,error);
             NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
             NSLog(@"request string:%@",operation.request.URL.absoluteString);
             NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];
}

-(void)updateChildWithId:(int)childId options:(NSDictionary*)options completion:(void (^)(id response, NSError *error))completion
{
    NSLog(@"update child with parameters:%@",options);
    
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php",_baseUrl];
    
    NSMutableDictionary *newOpt=[[NSMutableDictionary alloc]init];
    
    for (NSString *key in [options allKeys])
    {
        id obj=[options objectForKey:key];
        if ([obj isKindOfClass:[NSString class]])
        {
            obj=[obj stringByReplacingOccurrencesOfString:@" " withString:@"+"];
            
            NSLog(@"obj:%@",obj);
        }
        [newOpt setValue:obj forKey:key];
    }
    [newOpt setValue:[NetworkManager sharedInstance].token forKey:@"access_token"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.securityPolicy setAllowInvalidCertificates:YES];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [AppDelegate showProgressHUD];
    
    [manager POST:requestString parameters:newOpt success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         
         NSLog(@"create child success: %@",responseObject);
         if (completion)
         {
             completion(responseObject,nil);
         }
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];

         NSLog(@"create child fail: %@",error);
         if (completion)
         {
             completion(nil,error);
         }
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }];

}


-(void)deleteChildWithId:(int)childId completion:(void (^)(id response, NSError *error))completion
{
    NSLog(@"delete child %d",childId);
    
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=sted_child_manage&task=deleteChild&id=%d&access_token=%@",_baseUrl,childId,[NetworkManager sharedInstance].token];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         
         NSLog(@"delete child success!:%@",responseObject);
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];

         NSLog(@"delete child fail%@",error);
         NSLog(@"headers:%@",[operation.request allHTTPHeaderFields]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];

}

-(void)archiveChildWithId:(int)childId completion:(void (^)(id response, NSError *error))completion
{
    NSLog(@"archive child %d",childId);
    
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=sted_child_manage&task=archiveChild&id=%d&access_token=%@",_baseUrl,childId,[NetworkManager sharedInstance].token];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         
         NSLog(@"archive child success!:%@",responseObject);
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];

         NSLog(@"archive child fail%@",error);
         NSLog(@"headers:%@",[operation.request allHTTPHeaderFields]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];

}

-(void)unarchiveChildWithId:(int)childId completion:(void (^)(id response, NSError *error))completion
{
    NSLog(@"unarchive child %d",childId);
    
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=sted_child_manage&task=unarchiveChild&id=%d&access_token=%@",_baseUrl,childId,[NetworkManager sharedInstance].token];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         
         NSLog(@"unarchive child success!:%@",responseObject);
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];

         NSLog(@"unarchive child fail%@",error);
         NSLog(@"headers:%@",[operation.request allHTTPHeaderFields]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];
}


-(void)getChildrenBirthdaysWithCompletion:(void (^)(id response, NSArray *birthdays, NSError *error))completion
{
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=birth&task=birth&access_token=%@",_baseUrl,[NetworkManager sharedInstance].token];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         
         NSLog(@"get children success!:%@",responseObject);
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
         
         if (completion)
         {
             NSMutableArray *monthBirthdays = [NSMutableArray new];
             
             for (int i = 1; i<13; i++) {
                NSDictionary *object = [responseObject valueForKeyPath:[NSString stringWithFormat:@"result.%d.birth", i]];
                 [monthBirthdays addObject:object];
             }
             NSMutableArray *birthdaysArray = [NSMutableArray new];
             
             for (int i = 0; i<monthBirthdays.count; i++) {
                 if ([monthBirthdays[i] isKindOfClass:[NSArray class]]) {
                     [birthdaysArray addObjectsFromArray:monthBirthdays[i]];
                 }
             }
             
             completion(responseObject, nil,nil);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];
         
         NSLog(@"get children fail%@",error);
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
         if (completion)
         {
             completion(nil, nil,error);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];
}


- (void)getChildrenStatisticsWithCompletion:(void (^)(id response, NSError *error))completion
{
    NSString *requestString = [NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=sted_child_manage&task=getStatistics&access_token=%@",_baseUrl,[NetworkManager sharedInstance].token];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         self.requestInfoLog(operation);
         [self updateSessionTimer];
         
         if (completion) {
             completion(responseObject,nil);
         }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [AppDelegate hideProgressHUD];
         self.requestInfoLog(operation);
         if (completion) {
             completion(nil,error);
         }
     }];
    [operation start];
    [AppDelegate showProgressHUD];
}



#pragma mark - User & userManager (pin section)


-(void)loginWithUsername:(NSString*)username password:(NSString*)password subdomain:(NSString *)subdomain completion:(void (^)(id response, NSError * error))completion
{

    //NSString *baseUrl = [NSString *str]
    
    NSString *appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    NSString *deviceToken = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"] ? : @""];
    
    //NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=login&task=login&username=%@&password=%@&ios=%@", [self urlForSubdomain:subdomain], [username urlEncodeUsingEncoding:NSUTF8StringEncoding], [password urlEncodeUsingEncoding:NSUTF8StringEncoding], [appVersionString urlEncodeUsingEncoding:NSUTF8StringEncoding]];

    //With push
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=login&task=login&username=%@&password=%@&ios=%@&token=%@&device=%@", [self urlForSubdomain:subdomain], [[username stringByRemovingEmoji] urlEncodeUsingEncoding:NSUTF8StringEncoding], [password urlEncodeUsingEncoding:NSUTF8StringEncoding], [appVersionString urlEncodeUsingEncoding:NSUTF8StringEncoding], deviceToken, [deviceName() urlEncodeUsingEncoding:NSUTF8StringEncoding]];

    
    
    //TODO: В релизе убрать!
    NSLog(@"request string: %@", requestString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.securityPolicy setAllowInvalidCertificates:YES];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.requestSerializer.timeoutInterval = 60.f;
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //[AppDelegate showProgressHUD];
    [manager POST:requestString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //[AppDelegate hideProgressHUD];
         NSLog(@"login success: %@",responseObject);
#warning дублируется в контроллере логина
         
         /*
         if ([[responseObject valueForKeyPath:@"result.complete"]intValue]==0) {
             return;
         }
         [DataManager sharedInstance].adminId=[[responseObject valueForKeyPath:@"result.aid"]intValue];
         [DataManager sharedInstance].adminAccessLevel=[[responseObject valueForKeyPath:@"result.acl"]intValue];
         [NetworkManager sharedInstance].token=[responseObject valueForKeyPath:@"result.access_token"];
         [[NetworkManager sharedInstance]getAdminWithId:[DataManager sharedInstance].adminId completion:^(id response, NSError *error) {
             [DataManager sharedInstance].adminInfo=[response valueForKey:@"admin_info"];
         }];
          
         NSLog(@"admin id :%d",[DataManager sharedInstance].adminId);
         */
         
         self.requestInfoLog(operation);
         
         if (completion)
         {
             completion(responseObject,nil);
         }
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         //[AppDelegate hideProgressHUD];
         self.requestInfoLog(operation);
         NSLog(@"login fail: %@",error);
         if (completion)
         {
             completion(nil,error);
         }
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }];
    
    
}


-(void)loginWithUserId:(int)userId password:(NSString*)password subdomain:(NSString *)subdomain completion:(void (^)(id response, NSError * error))completion
{
    
    //NSString *baseUrl = [NSString *str]
    
    NSString *appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    NSString *deviceToken = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"]];
    
    //NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=login&task=login&username=%@&password=%@&ios=%@", [self urlForSubdomain:subdomain], [username urlEncodeUsingEncoding:NSUTF8StringEncoding], [password urlEncodeUsingEncoding:NSUTF8StringEncoding], [appVersionString urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    //With push
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=login&task=login&userId=%d&password=%@&ios=%@&token=%@&device=%@", [self urlForSubdomain:subdomain], userId, [password urlEncodeUsingEncoding:NSUTF8StringEncoding], [appVersionString urlEncodeUsingEncoding:NSUTF8StringEncoding], deviceToken, [deviceName() urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    //TODO: В релизе убрать!
    NSLog(@"request string: %@", requestString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.securityPolicy setAllowInvalidCertificates:YES];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.requestSerializer.timeoutInterval = 60.f;
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //[AppDelegate showProgressHUD];
    [manager POST:requestString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //[AppDelegate hideProgressHUD];
         NSLog(@"login success: %@",responseObject);
#warning дублируется в контроллере логина
         
         /*
          if ([[responseObject valueForKeyPath:@"result.complete"]intValue]==0) {
          return;
          }
          [DataManager sharedInstance].adminId=[[responseObject valueForKeyPath:@"result.aid"]intValue];
          [DataManager sharedInstance].adminAccessLevel=[[responseObject valueForKeyPath:@"result.acl"]intValue];
          [NetworkManager sharedInstance].token=[responseObject valueForKeyPath:@"result.access_token"];
          [[NetworkManager sharedInstance]getAdminWithId:[DataManager sharedInstance].adminId completion:^(id response, NSError *error) {
          [DataManager sharedInstance].adminInfo=[response valueForKey:@"admin_info"];
          }];
          
          NSLog(@"admin id :%d",[DataManager sharedInstance].adminId);
          */
         
         self.requestInfoLog(operation);
         
         if (completion)
         {
             completion(responseObject,nil);
         }
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         //[AppDelegate hideProgressHUD];
         self.requestInfoLog(operation);
         NSLog(@"login fail: %@",error);
         if (completion)
         {
             completion(nil,error);
         }
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }];
    
    
}


-(void)logoutAndDeviceToken:(BOOL)isWithToken completion:(void (^)(id response, NSError * error))completion
{
    NSLog(@"logout!");
    
    NSString *deviceToken = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"]];
    
    
    NSString *requestString;
    
    if (isWithToken) {
        requestString = [NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=login&task=logout&token=%@",_baseUrl, deviceToken];
    }
    else {
        requestString = [NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=login&task=logout",_baseUrl];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.securityPolicy setAllowInvalidCertificates:YES];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //[AppDelegate showProgressHUD];
    [manager POST:requestString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //[AppDelegate hideProgressHUD];
         NSLog(@"logout success: %@",responseObject);
         //[DataManager sharedInstance].adminId = 0;
         //[NetworkManager sharedInstance].token = nil;
         self.requestInfoLog(operation);
         if (completion)
         {
             completion(responseObject,nil);
         }
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         //[AppDelegate hideProgressHUD];
         NSLog(@"logout fail: %@",error);
         self.requestInfoLog(operation);
         if (completion)
         {
             completion(nil,error);
         }
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }];
    
    
}

- (void)checkIfPinExistsForUser:(int)uid subdomain:(NSString *)subdomain withCompletion:(void (^)(id response, NSError *error))completion {
    
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=login&task=checkExistPin&id=%i",[self urlForSubdomain:subdomain], uid];
    
    NSLog(@"requestString = %@", requestString);
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"get pin avalibility success!:%@",responseObject);
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"get pin avalibility fail:%@",error);
         NSLog(@"headers:%@",[operation.request allHTTPHeaderFields]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [operation start];
    
     /*
     //Вариант через requestOperationManager + инициализацию в init
      
     AFHTTPRequestOperationManager *requestOperationManager = [AFHTTPRequestOperationManager manager];
     [requestOperationManager.securityPolicy setAllowInvalidCertificates:YES];
     requestOperationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
     requestOperationManager.responseSerializer = [AFJSONResponseSerializer serializer];
     
     [requestOperationManager POST:requestString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
     NSLog(@"get pin avalibility success! %@",responseObject);
     
     if (completion)
     {
     completion(responseObject,nil);
     }
     NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
     NSLog(@"request string:%@",operation.request.URL.absoluteString);
     NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
     NSLog(@"lget pin avalibility fail: %@",error);
     if (completion)
     {
     completion(nil,error);
     }
     NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
     NSLog(@"request string:%@",operation.request.URL.absoluteString);
     NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }];
     */
    
}



- (void)createPin:(NSString *)pin forUser:(int)uid withCompletion:(void (^)(id response, NSError *error))completion {
    
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=login&task=createPin&id=%i&pin=%@&access_token=%@",_baseUrl, uid, pin, self.token];
    
    NSLog(@"requestString = %@", requestString);
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self updateSessionTimer];
         
         NSLog(@"get pin avalibility success!:%@",responseObject);
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"get pin avalibility fail:%@",error);
         NSLog(@"headers:%@",[operation.request allHTTPHeaderFields]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [operation start];
    
}

- (void)loginWithPin:(NSString *)pin forUser:(int)uid subdomain:(NSString *)subdomain withCompletion:(void (^)(id response, NSError *error))completion {
    
    NSString *appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
     NSString *deviceToken = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"]];
    
    //NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=login&task=pinLogin&user_id=%d&pin=%@&ios=%@",[self urlForSubdomain:subdomain], uid, pin, [appVersionString urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    //Push
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=login&task=pinLogin&user_id=%d&pin=%@&ios=%@&token=%@&device=%@",[self urlForSubdomain:subdomain], uid, pin, [appVersionString urlEncodeUsingEncoding:NSUTF8StringEncoding], deviceToken, [deviceName() urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSLog(@"requestString = %@", requestString);
    
//    requestString = [addRequestString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.securityPolicy setAllowInvalidCertificates:YES];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.requestSerializer.timeoutInterval = 60.f;
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    
    [manager POST:requestString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"pin login success: %@",responseObject);
         
         self.requestInfoLog(operation);
         
         if (completion)
         {
             completion(responseObject,nil);
         }
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         self.requestInfoLog(operation);
         NSLog(@"pin login fail: %@",error);
         if (completion)
         {
             completion(nil,error);
         }
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }];

    
    /*
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:[requestString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.f];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"pin login success!:%@",responseObject);
        if (completion)
         {
             completion(responseObject,nil);
         }
     }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"pin login fail:%@",error);
         NSLog(@"headers:%@",[operation.request allHTTPHeaderFields]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [operation start];
    */
}

//POST ?option=com_sted_mobile&controller=login&task=touchLogin
/*{
"touch_id": "s4d34g454fs46d4f",
"user_id": "s4d34g454fs46d4f"
}
*/
- (void)loginWithTouchIdString:(NSString *)touchIdString forUser:(int)uid subdomain:(NSString *)subdomain withCompletion:(void (^)(id response, NSError *error))completion {
    
    NSString *appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    NSString *deviceToken = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"]];
    
    //NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=login&task=pinLogin&user_id=%d&pin=%@&ios=%@",[self urlForSubdomain:subdomain], uid, pin, [appVersionString urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    //Push
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=login&task=touchLogin&user_id=%d&touch_id=%@&ios=%@&token=%@&device=%@",[self urlForSubdomain:subdomain], uid, touchIdString, [appVersionString urlEncodeUsingEncoding:NSUTF8StringEncoding], deviceToken, [deviceName() urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSLog(@"requestString = %@", requestString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.securityPolicy setAllowInvalidCertificates:YES];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    manager.requestSerializer.timeoutInterval = 60.f;
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [AppDelegate showProgressHUD];
    });
    
    [manager POST:requestString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"touch id login success: %@",responseObject);
         [AppDelegate hideProgressHUD];

         self.requestInfoLog(operation);
         
         if (completion)
         {
             completion(responseObject,nil);
         }
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];
         self.requestInfoLog(operation);
         NSLog(@"touch id login fail: %@",error);
         if (completion)
         {
             completion(nil,error);
         }
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
     }];
    
    
}


- (void)resetPinForUserID:(int)uid password:(NSString *)password subdomain:(NSString *)subdomain withCompletion:(void (^)(id response, NSError *error))completion {
    
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=login&task=resetPin&id=%i&pass=%@",[self urlForSubdomain:subdomain], uid, password];
    
    NSLog(@"requestString = %@", requestString);
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {

         NSLog(@"pin login success!:%@",responseObject);
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"pin login fail:%@",error);
         NSLog(@"headers:%@",[operation.request allHTTPHeaderFields]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [operation start];
    
}

//POST ?option=com_sted_mobile&controller=touch&task=createTouchId
/*
 {
 "touch_id": "347848hjfdsn",
 "user_id": "123"
 }
*/
- (void)createTouchIdToken:(NSString *)touchIdToken forUser:(int)uid isParent:(BOOL)isParent withCompletion:(void (^)(id response, NSError *error))completion {
    
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=%@&controller=touch&task=createTouchId&user_id=%i&touch_id=%@&access_token=%@",_baseUrl, isParent ? @"com_sted_parent_mobile" : @"com_sted_mobile", uid, touchIdToken, self.token];
    
    NSLog(@"requestString = %@", requestString);
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         
         NSLog(@"create touch id token success!:%@",responseObject);
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];
         NSLog(@"create touch id token fail:%@",error);
         NSLog(@"headers:%@",[operation.request allHTTPHeaderFields]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    dispatch_async(dispatch_get_main_queue(), ^{
        [AppDelegate showProgressHUD];
    });
    [operation start];
    
    
}


//POST ?option=com_sted_mobile&controller=touch&task=createTouchIdWithoutAccess
//POST ?option=com_sted_parent_mobile&controller=touch&task=createTouchIdWithoutAccess
/*
 {
 "touch_id": "347848hjfdsn",
 "login": "123",
 "pass": "123"
 }
 */
- (void)createTouchIdToken:(NSString *)touchIdToken forUserName:(NSString *)userName password:(NSString *)password subdomain:(NSString *)subdomain isParent:(BOOL)isParent withCompletion:(void (^)(id response, NSError *error))completion {
   
    
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=%@&controller=touch&task=createTouchIdWithoutAccess&login=%@&pass=%@&touch_id=%@", [self urlForSubdomain:subdomain], isParent ? @"com_sted_parent_mobile" : @"com_sted_mobile", [userName urlEncodeUsingEncoding:NSUTF8StringEncoding], [password urlEncodeUsingEncoding:NSUTF8StringEncoding], [touchIdToken urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"requestString = %@", requestString);
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         
         NSLog(@"create touch id token for user+pass success!:%@",responseObject);
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];
         NSLog(@"create touch id token for user+pass fail:%@",error);
         NSLog(@"headers:%@",[operation.request allHTTPHeaderFields]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    dispatch_async(dispatch_get_main_queue(), ^{
        [AppDelegate showProgressHUD];
    });
    [operation start];
    
}


#pragma mark - Child profile sections

-(void)getChildProfileListWithCompletion:(void (^)(id response, NSError *error))completion
{
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=sted_child_manage&task=Child_Profile&$limitstart=0&access_token=%@",_baseUrl,[NetworkManager sharedInstance].token];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         
         NSLog(@"get profile list success!:%@",responseObject);
         [DataManager sharedInstance].profileSections=[responseObject valueForKeyPath:@"list_child_profile"];
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];
         
         NSLog(@"get profile list fail:%@",error);
         NSLog(@"headers:%@",[operation.request allHTTPHeaderFields]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];
    
}


-(void)getRoomListWithCompletion:(void (^)(id response, NSError *error))completion
{
    //http://ios.pleasure2deal.com/index.php?option=com_sted_mobile&controller=predefined_room&task=room&limitstart=0&device=m
    
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=predefined_room&task=room&limitstart=0&device=m&access_token=%@",_baseUrl,[NetworkManager sharedInstance].token];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         
         NSLog(@"get rooms success!:%@",responseObject);
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
         
         [DataManager sharedInstance].rooms=[responseObject objectForKey:@"roomList"];
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];
         
         NSLog(@"get rooms fail%@",error);
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];
    
}

-(void)getPositionListWithCompletion:(void (^)(id response, NSError *error))completion
{
    //http://ios.pleasure2deal.com/index.php?option=com_sted_mobile&controller=admin&task=position
    
    NSString *requestString=[NSString stringWithFormat:@"%@/index.php?option=com_sted_mobile&controller=admin&task=position&access_token=%@",_baseUrl,[NetworkManager sharedInstance].token];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES;
    operation.responseSerializer=[AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [AppDelegate hideProgressHUD];
         [self updateSessionTimer];
         
         NSLog(@"get positions success!:%@",responseObject);
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
         
         [DataManager sharedInstance].positions =[responseObject valueForKey:@"position"];
         if (completion)
         {
             completion(responseObject,nil);
         }
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [AppDelegate hideProgressHUD];
         
         NSLog(@"get admins fail%@",error);
         NSLog(@"request headers:%@",[operation.request allHTTPHeaderFields]);
         NSLog(@"request string:%@",operation.request.URL.absoluteString);
         NSLog(@"Body:%@",[[NSString alloc]initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding]);
         if (completion)
         {
             completion(nil,error);
         }
     }];
    [AppDelegate showProgressHUD];
    [operation start];
}


@end
