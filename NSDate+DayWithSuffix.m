//
//  NSDate+DayWithSuffix.m
//  pd2
//
//  Created by i11 on 19/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "NSDate+DayWithSuffix.h"

@implementation NSDate (DayWithSuffix)

- (NSString *)dayWithSuffix {
    
    NSDateFormatter *monthDayFormatter = [[NSDateFormatter alloc] init];
    [monthDayFormatter setDateFormat:@"d"];
    int dateDay = [[monthDayFormatter stringFromDate:self] intValue];
    NSString *suffix_string = @"|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st";
    NSArray *suffixes = [suffix_string componentsSeparatedByString: @"|"];
    NSString *suffix = [suffixes objectAtIndex:dateDay];
    
    return [NSString stringWithFormat:@"%i%@", dateDay, suffix];
    
}


@end
