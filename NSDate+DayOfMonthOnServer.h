//
//  NSDate+DayOfMonthOnServer.h
//  pd2
//
//  Created by i11 on 19/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (DayOfMonthOnServer)

- (int)deltaDayOfMonthOnServer;
- (int)dayOfMonthOnServer;

@end
