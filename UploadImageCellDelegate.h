//
//  UploadImageCellDelegate.h
//  pd2
//
//  Created by i11 on 18/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UploadImageCellDelegate <NSObject>


@required

- (void)uploadImageActionForImageView:(UIImageView *)imageView withCompletion:(void(^)(UIImage *image))completion;


@end
