//
//  TargetUtils.h
//  pd2
//
//  Created by i11 on 09/03/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#ifndef pd2_TargetUtils_h
#define pd2_TargetUtils_h

    #ifdef DEBUG

        #define NSLog(...) NSLog(__VA_ARGS__)

    #else

        #define NSLog(...) do {} while (0) //Иначе warning на все NSLog

    #endif

#endif
