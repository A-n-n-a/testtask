//
//  HeaderCell.h
//  pd2
//
//  Created by i11 on 14/04/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *headerTitle;
@property (nonatomic, weak) IBOutlet UIImageView *headerImageView;

@end
