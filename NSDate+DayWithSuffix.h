//
//  NSDate+DayWithSuffix.h
//  pd2
//
//  Created by i11 on 19/05/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

//*************************
// return "26th"
//*************************

#import <Foundation/Foundation.h>

@interface NSDate (DayWithSuffix)

- (NSString *)dayWithSuffix;

@end
