import Foundation
import XCTest

class UIDataTests: XCTestCase {
    /*
     * PUBLIC DATA FOR ALL TESTS
     *
     * 'typeLogin' - password/pin
     */
    let infoLogin = ["typeLogin": "password",
                     "user": "Master Jenkins",
                     "login": "admin",
                     "password": "admin123",
                     "domain": "jenkins.mybabysdays.com",
                     "pin": "1111"
    ]
    let childInfo = ["firstName": "Android",
                     "lastName": "Test",
                     "age": "194",
                     ]
    
    
    /*
     * Data for test UIPhotoTests/testUploadPhoto
     */
    let valueTitlePhoto = ["T", "e", "s", "t", "space", "t", "i", "t", "l", "e"]
    let valueDescriptionPhoto = ["T", "e", "s", "t", "space", "d", "e", "s", "c", "r", "i", "p", "t", "i", "o", "n"]
    // day + month + year should match date
    let testDataUploadPhoto = [
        "day": "5", "month": "April", "year": "2016", "date": "05.04.2016",
        "title": "Test title", "description": "Test description",
        "area": "Communication and Language", "aspect": "Listening and Attention",
        "outcome": "Turns toward a familiar sound then locates range of sounds with accuracy.",
        "age_outcome": "(Birth - 11 Months) Turns toward a familiar sound then locates range of sounds with accuracy."
    ]
    
    /*
     * Data for test UIPhotoTests/testViewPhoto
     */
    let testDataViewPhoto = [
        "selectYearInput": "April, 3 of 3", "selectYearValue": "2016 April",
        "checkSelectYearValue": "Apr 2016"
    ]
    
    /*
     * Data for test UIPhotoTests/testCommentPhoto
     */
    let comment = ["T", "e", "s", "t", "space", "c", "o", "m", "m", "e", "n", "t", "u"]
    let testDataCommentPhoto = [
        "selectYearInput": "April, 3 of 3", "selectYearValue": "2016 April",
        "checkSelectYearValue": "Apr 2016", "date": "04.04.2016", "title": "N/A",
        "comment": "Test comment",
        ]
    
    
    /*
     * Data for test UIPhotoTests/testEditPhoto
     */
    // day + month + year should match date
    let editTitlePhoto = ["space", "e", "d", "i", "t"]
    let editDescriptionPhoto = ["E", "d", "i", "t", "space"]
    let testDataEditPhoto = [
        "selectYearInput": "April, 3 of 3", "selectYearValue": "2016 April",
        "checkSelectYearValue": "Apr 2016", "title": "edit", "description": "Edit",
        "day": "13", "month": "April", "year": "2016", "date": "13.04.2016",
        "area": "Communication and Language", "aspect": "Listening and Attention", "shortArea": "CL",        "outcome": "Listens to, distinguishes and responds to intonations and sounds of voices.",
        "age_outcome": "(Birth - 11 Months) Listens to, distinguishes and responds to intonations and sounds of voices."
    ]
    
    /*
     * Data for test UIPhotoTests/testDeletePhoto
     */
    let testDataDeletePhoto = ["date": "05.04.2016",
                               "title": "Test title",
                               "selectYearInput": "April, 3 of 3",
                               "selectYearValue": "2016 April",
                               "checkSelectYearValue": "Apr 2016"
    ]
    
    /*
     * Data for test UIVideoTests/testUploadVideo
     */
    let testDataUploadVideo = ["day": "5", "month": "April", "year": "2016", "date": "05.04.2016",
                               "title": "Test title", "description": "Test description",
                               "area": "Communication and Language", "aspect": "Listening and Attention",
                               "outcome": "Turns toward a familiar sound then locates range of sounds with accuracy.",
                               "age_outcome": "(Birth - 11 Months) Turns toward a familiar sound then locates range of sounds with accuracy."
    ]
    
    /*
     * Data for test UIPhotoTests/testViewVideo
     */
    let testDataViewVideo = [
        "selectYearInput": "April, 1 of 1", "selectYearValue": "2016 April",
        "checkSelectYearValue": "Apr 2016"
    ]
    
    /*
     * Data for test UIPhotoTests/testCommentVideo
     */
    let testDataCommentVideo = [
        "selectYearInput": "April, 1 of 1", "selectYearValue": "2016 April",
        "checkSelectYearValue": "Apr 2016", "date": "05.04.2016", "title": "Test title",
        "comment": "Test comment",
        ]
    
    /*
     * Data for test UIPhotoTests/testDeleteVideo
     */
    let testDataDeleteVideo = ["date": "05.04.2016",
                               "title": "Test title",
                               "selectYearInput": "April, 1 of 1",
                               "selectYearValue": "2016 April",
                               "checkSelectYearValue": "Apr 2016"
    ]
    
    /*
     * Data for test UIVideoTests/testEditVideo
     */
    // day + month + year should match date
    let editTitleVideo = ["space", "e", "d", "i", "t"]
    let editDescriptionVideo = ["E", "d", "i", "t", "space"]
    let testDataEditVideo = [
        "selectYearInput": "May, 2 of 2", "selectYearValue": "2016 May",
        "checkSelectYearValue": "May 2016", "title": "edit", "description": "Edit",
        "day": "11", "month": "May", "year": "2016", "date": "11.05.2016",
        "area": "Communication and Language", "aspect": "Listening and Attention", "shortArea": "CL",
        "outcome": "Listens to, distinguishes and responds to intonations and sounds of voices.",
        "age_outcome": "(Birth - 11 Months) Listens to, distinguishes and responds to intonations and sounds of voices."
    ]}