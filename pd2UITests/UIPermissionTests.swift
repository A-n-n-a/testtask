//
//  UIPermissionTests.swift
//  pd2
//
//  Created by Admin on 17.06.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation
import UIKit
import XCTest

class UIPermissionTests: UITestCase {
    
    
    func checkCreatedCategory(_ category : String){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 1)
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: category)
        
        sleep(5)
        viewCategory.tap()
        sleep(4)
        XCTAssert(categoryItemCheck.element.exists)
        sleep(5)
        
    }
    
    
    
    func checkSavedCategory(_ category : String){
        
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: category)
        
        sleep(7)
        
        XCTAssertTrue(categoryItemCheck.element.exists)
        // XCTAssertFalse(tablesQuery.staticTexts[category].exists)
        sleep(5)
    }
    
    func checkDelCategory(_ category : String){
        
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: category)
        
        sleep(5)
        
        XCTAssertFalse(categoryItemCheck.element.exists)
        
        sleep(5)
    }
    
    func preAddCategory() {
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 1)
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: "A test")
        
        let categoryItemCheckTwo = tablesQuery.cells.containing(.staticText, identifier: "A test two")
        let delCategory = categoryItemCheck.buttons["Trash Icon"]
        let delCategoryTwo = categoryItemCheckTwo.buttons["Trash Icon"]
        let warning = app.alerts["Warning!"]
        let delete = warning.collectionViews.buttons["Delete"]
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(5)
        
        viewCategory.tap()
        sleep(4)
        if categoryItemCheck.element.exists {
            sleep(2)
            delCategory.tap()
            sleep(1)
            delete.tap()
        }
        sleep(8)
        if categoryItemCheckTwo.element.exists {
            sleep(2)
            delCategoryTwo.tap()
            sleep(1)
            delete.tap()
        }
        
        sleep(5)
        
        back.tap()
    }
    
    func preEditCategory(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 1)
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: "A test")
        
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(2)
        
        viewCategory.tap()
        sleep(4)
        if categoryItemCheck.element.exists {
            back.tap()
            sleep(3)
        } else {
            tablesQuery.children(matching: .cell).element(boundBy: 1).buttons["Add a New Permission Category"].tap()
            
            let tablesQuery=app.tables
            let categoryTitle = tablesQuery.cells.containing(.staticText, identifier:"Category Title: *").children(matching: .textField).element
            let valueTitleCategory =  "A test"
            let uploadImage = tablesQuery.cells.containing(.staticText, identifier:"Upload Image").children(matching: .button).element
            let selectSourcePhoto = app.sheets["Please select photo source"]
            let selectFromLibrery =  selectSourcePhoto.collectionViews.buttons["Choose photo from library"]
            let selectFirstPhoto =   app.collectionViews.element.children(matching: .cell).element(boundBy: 1)
            let createCategory = tablesQuery.buttons["Create Category"]
            
            
            sleep(2)
            
            XCTAssert(categoryTitle.exists)
            categoryTitle.tap()
            categoryTitle.typeText( valueTitleCategory );
            app.buttons["Return"].tap()
            
            XCTAssert(uploadImage.exists)
            uploadImage.tap()
            
            sleep(2)
            XCTAssert(selectFromLibrery.exists)
            selectFromLibrery.tap()
            
            sleep(2)
            XCTAssert( tablesQuery.buttons["Moments"].exists)
            tablesQuery.buttons["Moments"].tap()
            
            sleep(2)
            XCTAssert( selectFirstPhoto.exists)
            selectFirstPhoto.tap()
            
            sleep(2)
            XCTAssert( createCategory.exists)
            createCategory.tap()
            
            sleep(2)
            
            if app.alerts["Error"].exists {
                app.alerts["Error"].collectionViews.buttons["OK"].tap()
            }
            sleep(3)
            
            back.tap()
        }
        
        sleep(5)
        
    }
    
    func test01AddCategory(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let addCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 0)
        let tablesQuery=app.tables
        let categoryTitle = tablesQuery.cells.containing(.staticText, identifier:"Category Title: *").children(matching: .textField).element
        let valueTitleCategory =  "A test"
        let uploadImage = tablesQuery.cells.containing(.staticText, identifier:"Upload Image").children(matching: .button).element
        let selectSourcePhoto = app.sheets["Please select photo source"]
        let selectFromLibrery =  selectSourcePhoto.collectionViews.buttons["Choose photo from library"]
        let selectFirstPhoto =   app.collectionViews.element.children(matching: .cell).element(boundBy: 1)
        let createCategory = tablesQuery.buttons["Create Category"]
        
        login()
        sleep(4)
        
        app.collectionViews.buttons["Permissions"].tap()
        
        preAddCategory()
        
        sleep(2)
        XCTAssert(addCategory.exists)
        addCategory.tap()
        
        XCTAssert(categoryTitle.exists)
        categoryTitle.tap()
        categoryTitle.typeText( valueTitleCategory );
        app.buttons["Return"].tap()
        
        XCTAssert(uploadImage.exists)
        uploadImage.tap()
        
        sleep(2)
        XCTAssert(selectFromLibrery.exists)
        selectFromLibrery.tap()
        
        sleep(2)
        XCTAssert( tablesQuery.buttons["Moments"].exists)
        tablesQuery.buttons["Moments"].tap()
        
        sleep(2)
        XCTAssert( selectFirstPhoto.exists)
        selectFirstPhoto.tap()
        
        sleep(2)
        XCTAssert( createCategory.exists)
        createCategory.tap()
        
        sleep(2)
        XCTAssertFalse( app.alerts["Error"].exists)
        
        sleep(3)
        
        checkCreatedCategory(valueTitleCategory)
    }
    
    func test02EditCategory(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 1)
        let tablesQuery=app.tables
        let categoryTitle = tablesQuery.cells.containing(.staticText, identifier:"Category Title: *")
        let valueTitleCategoryEdit =  "A test two"
        let uploadImage = tablesQuery.cells.containing(.staticText, identifier:"Replace Image").children(matching: .button).element
        let deleteKey = app.keys["delete"]
        let categoryItem = tablesQuery.cells.containing(.staticText, identifier:"A test")
        let editCategory = categoryItem.buttons["Compose Icon"]
        let selectSourcePhoto = app.sheets["Please select photo source"]
        let selectFromLibrery =  selectSourcePhoto.collectionViews.buttons["Choose photo from library"]
        let selectSecondPhoto =   app.collectionViews.element.children(matching: .cell).element(boundBy: 2)
        let saveCategory =  tablesQuery.buttons["Save Changes"]
        let selectTitle = categoryTitle.children(matching: .textField).element
        
        login()
        
        sleep(4)
        app.collectionViews.buttons["Permissions"].tap()
        
        preEditCategory()
        
        sleep(4)
        XCTAssert(viewCategory.exists)
        viewCategory.tap()
        
        sleep(5)
        
        XCTAssert(categoryItem.element.exists)
        XCTAssert(editCategory.exists)
        editCategory.tap()
        
        sleep(2)
        selectTitle.tap()
        
        let countTitle = (selectTitle.value as! String).characters.count
        for _ in 0 ..< countTitle {
            deleteKey.tap()
        }
        
        
        selectTitle.typeText( valueTitleCategoryEdit )
        app.buttons["Return"].tap()
        
        sleep(2)
        XCTAssert(uploadImage.exists)
        uploadImage.tap()
        
        sleep(2)
        XCTAssert(selectFromLibrery.exists)
        selectFromLibrery.tap()
        
        sleep(2)
        XCTAssert( tablesQuery.buttons["Moments"].exists)
        tablesQuery.buttons["Moments"].tap()
        
        sleep(2)
        XCTAssert( selectSecondPhoto.exists)
        selectSecondPhoto.tap()
        
        
        sleep(2)
        XCTAssert( saveCategory.exists)
        saveCategory.tap()
        
        XCTAssertFalse( app.alerts["Error"].exists)
        sleep(4)
        
        // checkSavedCategory(valueTitleCategoryEdit)
    }
    
    func preDelCategory(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 1)
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: "A test")
        
        let categoryItemCheckTwo = tablesQuery.cells.containing(.staticText, identifier: "A test two")
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(2)
        
        viewCategory.tap()
        sleep(4)
        if categoryItemCheck.element.exists {
            back.tap()
            sleep(3)
        } else if categoryItemCheckTwo.element.exists {
            back.tap()
            sleep(3)
        }
        else {
            tablesQuery.children(matching: .cell).element(boundBy: 1).buttons["Add a New Permission Category"].tap()
            
            let tablesQuery=app.tables
            let categoryTitle = tablesQuery.cells.containing(.staticText, identifier:"Category Title: *").children(matching: .textField).element
            let valueTitleCategory =  "A test two"
            let uploadImage = tablesQuery.cells.containing(.staticText, identifier:"Upload Image").children(matching: .button).element
            let selectSourcePhoto = app.sheets["Please select photo source"]
            let selectFromLibrery =  selectSourcePhoto.collectionViews.buttons["Choose photo from library"]
            let selectFirstPhoto =   app.collectionViews.element.children(matching: .cell).element(boundBy: 1)
            let createCategory = tablesQuery.buttons["Create Category"]
            
            
            sleep(6)
            
            XCTAssert(categoryTitle.exists)
            categoryTitle.tap()
            categoryTitle.typeText( valueTitleCategory );
            app.buttons["Return"].tap()
            
            XCTAssert(uploadImage.exists)
            uploadImage.tap()
            
            sleep(2)
            XCTAssert(selectFromLibrery.exists)
            selectFromLibrery.tap()
            
            sleep(2)
            XCTAssert( tablesQuery.buttons["Moments"].exists)
            tablesQuery.buttons["Moments"].tap()
            
            sleep(2)
            XCTAssert( selectFirstPhoto.exists)
            selectFirstPhoto.tap()
            
            sleep(2)
            XCTAssert( createCategory.exists)
            createCategory.tap()
            
            sleep(2)
            
            if app.alerts["Error"].exists {
                app.alerts["Error"].collectionViews.buttons["OK"].tap()
            }
            sleep(3)
            
            back.tap()
        }
        
        sleep(5)
    }
    func test03DelCategory(){
        
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 1)
        
        let tablesQuery=app.tables
        
        let nameCategory = "A test"
        let categoryItem = tablesQuery.cells.containing(.staticText, identifier:nameCategory)
        let delCategory = categoryItem.buttons["Trash Icon"]
        
        let nameCategoryTwo = "A test two"
        let categoryItemTwo = tablesQuery.cells.containing(.staticText, identifier:nameCategoryTwo)
        let delCategoryTwo = categoryItemTwo.buttons["Trash Icon"]
        let warning = app.alerts["Warning!"]
        let delete = warning.collectionViews.buttons["Delete"]
        login()
        
        sleep(3)
        
        
        app.collectionViews.buttons["Permissions"].tap()
        
        sleep(5)
        preDelCategory()
        sleep(5)
        
        viewCategory.tap()
        
        
        sleep(5)
        if categoryItem.element.exists {
            XCTAssert(categoryItem.element.exists)
            
            XCTAssert(delCategory.exists)
            delCategory.tap()
            
            
            XCTAssert(warning.exists)
            
            XCTAssert(delete.exists)
            
            
            delete.tap()
            
            sleep(5)
            
            checkDelCategory(nameCategory)
        } else    if categoryItemTwo.element.exists {
            XCTAssert(categoryItemTwo.element.exists)
            
            XCTAssert(delCategoryTwo.exists)
            delCategoryTwo.tap()
            
            
            XCTAssert(warning.exists)
            
            XCTAssert(delete.exists)
            
            
            delete.tap()
            
            sleep(5)
            
            checkDelCategory(nameCategoryTwo)
        }
        
    }
    
    func checkCreatedPermission(_ permissionName : String){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewPermission = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 2)
        let tablesQuery = app.tables
        let selectCategory = tablesQuery.staticTexts["Activities"]
        let permission =  tablesQuery.cells.containing(.staticText, identifier : permissionName)
        
        sleep(5)
        
        viewPermission.tap()
        
        sleep(6)
        selectCategory.tap()
        
        sleep(2)
        XCTAssert( permission.element.exists)
        
        sleep(5)
        
    }
    func checkSavedPermission(_ permissionName : String){
        
        
        let tablesQuery = app.tables
        let selectCategory = tablesQuery.staticTexts["Activities"]
        let permission =  tablesQuery.cells.containing(.staticText, identifier : permissionName)
        
        
        sleep(4)
        selectCategory.tap()
        
        sleep(3)
        XCTAssert( permission.element.exists)
        
        sleep(5)
        
    }
    
    func checkDelPermission(_ permissionName : String){
        
        let tablesQuery = app.tables
        let permission =  tablesQuery.cells.containing(.staticText, identifier : permissionName)
        
        
        sleep(5)
        XCTAssertFalse( permission.element.exists)
        
        sleep(6)
    }
    func preAddPermission(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewPermission = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 2)
        let tablesQuery=app.tables
        
        let selectCategory = tablesQuery.staticTexts["Activities"]
        
        let valueTitlePermission = "A test"
        
        let permission =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePermission)
        let permissionTwo =  tablesQuery.cells.containing(.staticText, identifier:"A test two")
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        
        sleep(2)
        XCTAssert(viewPermission.exists)
        viewPermission.tap()
        
        sleep(4)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        
        //XCTAssert( permission.element.exists)
        // XCTAssert( permissionEdit.exists)
        // permissionEdit.tap()
        if permission.element.exists {
            
            let deleteCategory = permission.buttons["Trash Icon"]
            let warning = app.alerts["Warning!"]
            let delete = warning.collectionViews.buttons["Delete"]
            
            sleep(6)
            deleteCategory.tap()
            
            sleep(8)
            
            delete.tap()
            
            sleep(6)
            
            back.tap()
            
            sleep(3)
        }
        else {
            back.tap()
            sleep(2)
        }
        
        sleep(6)
        XCTAssert(viewPermission.exists)
        viewPermission.tap()
        
        sleep(6)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        
        if permissionTwo.element.exists{
            
            let deleteCategoryTwo = permissionTwo.buttons["Trash Icon"]
            let warning = app.alerts["Warning!"]
            let delete = warning.collectionViews.buttons["Delete"]
            sleep(6)
            deleteCategoryTwo.tap()
            
            sleep(6)
            
            delete.tap()
            
            sleep(6)
            
            back.tap()
            
            sleep(3)
        }else {
            back.tap()
            sleep(2)
        }
    }
    
    
    func test04AddPermission(){
        
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let addPermission = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 1)
        let tablesQuery=app.tables
        let permissionTitle = tablesQuery.cells.containing(.staticText, identifier:"Permission Title:*").children(matching: .textField).element(boundBy: 0)
        let permissionDesc = tablesQuery.cells.containing(.staticText, identifier:"Permission Title:*").children(matching: .textView).element
        let selectTitle = tablesQuery.staticTexts["Select Permission Category:*"]
        //let selectCategory = tablesQuery.staticTexts["Well Being"]
        let selectCategory = tablesQuery.staticTexts["Activities"]
        let selectDate = tablesQuery.textFields["Date"]
        let selectDesc = tablesQuery.staticTexts["Permission Description:*"]
        let createPermission =   tablesQuery.buttons["Create Permission"]
        let valueTitlePermission = "A test"
        let datePickersQuery = app.datePickers
        let valueDescPermission = "A description"
        
        
        login()
        
        sleep(3)
        
        app.collectionViews.buttons["Permissions"].tap()
        
        preAddPermission()
        sleep(5)
        XCTAssert(addPermission.exists)
        addPermission.tap()
        
        sleep(6)
        XCTAssert(selectTitle.exists)
        selectTitle.tap()
        
        sleep(4)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        
        sleep(4)
        XCTAssert(permissionTitle.exists)
        permissionTitle.tap()
        permissionTitle.typeText( valueTitlePermission )
        app.buttons["Return"].tap()
        
        sleep(5)
        XCTAssert(selectDate.exists)
        selectDate.tap()
        XCTAssertTrue(datePickersQuery.element.exists)
        app.toolbars.buttons["Done"].tap()
        
        sleep(5)
        XCTAssert(permissionDesc.exists)
        permissionDesc.tap()
        permissionDesc.typeText( valueDescPermission )
        
        
        sleep(5)
        XCTAssert(selectDesc.exists)
        selectDesc.tap()
        
        sleep(5)
        XCTAssert(createPermission.exists)
        createPermission.tap()
        
        sleep(5)
        XCTAssertFalse( app.alerts["Error"].exists)
        checkCreatedPermission(valueTitlePermission)
        
    }
    
    func preEditPermission() {
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewPermission = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 2)
        let tablesQuery=app.tables
        
        let selectCategory = tablesQuery.staticTexts["Activities"]
        
        let valueTitlePermission = "A test"
        
        let permission =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePermission)
        
        let valueTitlePermissionTwo = "A test two"
        
        let permissionTwo =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePermissionTwo)
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        let existingText = tablesQuery.staticTexts["Existing Permissions"]
        let illnessText =  tablesQuery.staticTexts["Illnesses Accidents and Incidents"]
        
        sleep(2)
        XCTAssert(viewPermission.exists)
        viewPermission.tap()
        
        sleep(6)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        
        
        
        if permissionTwo.element.exists {
            
            let deleteCategoryTwo = permissionTwo.buttons["Trash Icon"]
            let warning = app.alerts["Warning!"]
            let delete = warning.collectionViews.buttons["Delete"]
            
            deleteCategoryTwo.tap()
            
            sleep(4)
            
            delete.tap()
            
            sleep(12)
            
            // back.tap()
        }else {
            existingText.tap()
            existingText.swipeUp()
            sleep(3)
            if permissionTwo.element.exists {
                
                let deleteCategoryTwo = permissionTwo.buttons["Trash Icon"]
                let warning = app.alerts["Warning!"]
                let delete = warning.collectionViews.buttons["Delete"]
                
                deleteCategoryTwo.tap()
                
                sleep(4)
                
                delete.tap()
                
                sleep(12)
                
                // back.tap()
            }
            sleep(12)
            illnessText.tap()
            illnessText.swipeDown()
        }
        
        if !permission.element.exists {
            
            back.tap()
            let addPermission = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 1)
            
            let permissionTitle = tablesQuery.cells.containing(.staticText, identifier:"Permission Title:*").children(matching: .textField).element(boundBy: 0)
            let permissionDesc = tablesQuery.cells.containing(.staticText, identifier:"Permission Title:*").children(matching: .textView).element
            let selectTitle = tablesQuery.staticTexts["Select Permission Category:*"]
            //let selectCategory = tablesQuery.staticTexts["Well Being"]
            let selectCategory = tablesQuery.staticTexts["Activities"]
            let selectDate = tablesQuery.textFields["Date"]
            let selectDesc = tablesQuery.staticTexts["Permission Description:*"]
            let createPermission =   tablesQuery.buttons["Create Permission"]
            let valueTitlePermission = "A test"
            let datePickersQuery = app.datePickers
            let valueDescPermission = "A description"
            
            XCTAssert(addPermission.exists)
            addPermission.tap()
            
            sleep(2)
            XCTAssert(selectTitle.exists)
            selectTitle.tap()
            
            sleep(2)
            XCTAssert(selectCategory.exists)
            selectCategory.tap()
            
            sleep(2)
            XCTAssert(permissionTitle.exists)
            permissionTitle.tap()
            permissionTitle.typeText( valueTitlePermission )
            app.buttons["Return"].tap()
            
            sleep(2)
            XCTAssert(selectDate.exists)
            selectDate.tap()
            XCTAssertTrue(datePickersQuery.element.exists)
            //datePickersQuery.pickerWheels["29"].tap()
            //datePickersQuery.pickerWheels["2016"].tap()
            //datePickersQuery.pickerWheels["June"].tap()
            app.toolbars.buttons["Done"].tap()
            
            sleep(2)
            XCTAssert(permissionDesc.exists)
            permissionDesc.tap()
            permissionDesc.typeText( valueDescPermission )
            
            
            sleep(2)
            XCTAssert(selectDesc.exists)
            selectDesc.tap()
            
            sleep(2)
            XCTAssert(createPermission.exists)
            createPermission.tap()
            
            back.tap()
            
            sleep(3)
        } else {
            back.tap()
            sleep(2)
        }
    }
    func test05EditPermission(){
        
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewPermission = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 2)
        let tablesQuery=app.tables
        let permissionTitle = tablesQuery.cells.containing(.staticText, identifier:"Permission Title:*").children(matching: .textField).element(boundBy: 0)
        let permissionDate = tablesQuery.cells.containing(.staticText, identifier:"Permission Title:*").children(matching: .textField).element(boundBy: 1)
        let permissionDesc = tablesQuery.cells.containing(.staticText, identifier:"Permission Title:*").children(matching: .textView).element
        let selectCategory = tablesQuery.staticTexts["Activities"]
        let savePermission =   tablesQuery.buttons["Save Changes"]
        //tablesQuery.staticTexts["Well Being"].tap()
        
        let valueTitlePermission = "A test"
        let valueTitlePermissionEdit = "A test two"
        let datePickersQuery = app.datePickers
        let valueDescPermission = "A description two"
        let deleteKey = app.keys["delete"]
        let permission =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePermission)
        let permissionEdit = permission.buttons["Compose Icon"]
        
        login()
        
        sleep(3)
        app.collectionViews.buttons["Permissions"].tap()
        
        sleep(3)
        preEditPermission()
        
        sleep(2)
        XCTAssert(viewPermission.exists)
        viewPermission.tap()
        
        sleep(4)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        
        sleep(2)
        XCTAssert( permission.element.exists)
        XCTAssert( permissionEdit.exists)
        permissionEdit.tap()
        
        sleep(5)
        XCTAssert(permissionTitle.exists)
        permissionTitle.tap()
        
        let countTitle = (permissionTitle.value as! String).characters.count
        for _ in 0 ..< countTitle {
            deleteKey.tap()
        }
        permissionTitle.typeText( valueTitlePermissionEdit )
        app.buttons["Return"].tap()
        
        sleep(2)
        XCTAssert(permissionDate.exists)
        permissionDate.tap()
        XCTAssertTrue(datePickersQuery.element.exists)
        /*
         datePickersQuery.pickerWheels["22"].tap()
         datePickersQuery.pickerWheels["2016"].tap()
         datePickersQuery.pickerWheels["June"].tap()
         */
        app.toolbars.buttons["Done"].tap()
        
        sleep(2)
        XCTAssert(permissionDesc.exists)
        permissionDesc.tap()
        let countDesc = (permissionDesc.value as! String).characters.count
        for _ in 0 ..< countDesc {
            deleteKey.tap()
        }
        
        permissionDesc.typeText(valueDescPermission)
        
        //tablesQuery.staticTexts["Permission Description:*"].tap()
        app.otherElements.containing(.navigationBar, identifier:"Baby's Day").children(matching: .other).element.children(matching: .other).element.children(matching: .table).element.tap()
        
        sleep(4)
        XCTAssert(savePermission.exists)
        savePermission.tap()
        
        sleep(5)
        //  app.alerts["Error"].collectionViews.buttons["OK"].tap()
        XCTAssertFalse( app.alerts["Error"].exists)
        sleep(6)
        
        checkSavedPermission(valueTitlePermissionEdit)
        
    }
    
    func preDelPermission(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewPermission = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 2)
        let tablesQuery=app.tables
        
        let selectCategory = tablesQuery.staticTexts["Activities"]
        
        let valueTitlePermission = "A test two"
        
        let permission =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePermission)
        //  let permissionEdit = permission.buttons["Compose Icon"]
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        
        sleep(4)
        XCTAssert(viewPermission.exists)
        viewPermission.tap()
        
        sleep(5)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        
        //XCTAssert( permission.element.exists)
        // XCTAssert( permissionEdit.exists)
        // permissionEdit.tap()
        if !permission.element.exists {
            
            back.tap()
            let addPermission = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 1)
            
            let permissionTitle = tablesQuery.cells.containing(.staticText, identifier:"Permission Title:*").children(matching: .textField).element(boundBy: 0)
            let permissionDesc = tablesQuery.cells.containing(.staticText, identifier:"Permission Title:*").children(matching: .textView).element
            let selectTitle = tablesQuery.staticTexts["Select Permission Category:*"]
            //let selectCategory = tablesQuery.staticTexts["Well Being"]
            let selectCategory = tablesQuery.staticTexts["Activities"]
            let selectDate = tablesQuery.textFields["Date"]
            let selectDesc = tablesQuery.staticTexts["Permission Description:*"]
            let createPermission =   tablesQuery.buttons["Create Permission"]
            let valueTitlePermission = "A test two"
            let datePickersQuery = app.datePickers
            let valueDescPermission = "A description"
            
            XCTAssert(addPermission.exists)
            addPermission.tap()
            
            sleep(2)
            XCTAssert(selectTitle.exists)
            selectTitle.tap()
            
            sleep(2)
            XCTAssert(selectCategory.exists)
            selectCategory.tap()
            
            sleep(2)
            XCTAssert(permissionTitle.exists)
            permissionTitle.tap()
            permissionTitle.typeText( valueTitlePermission )
            app.buttons["Return"].tap()
            
            sleep(2)
            XCTAssert(selectDate.exists)
            selectDate.tap()
            XCTAssertTrue(datePickersQuery.element.exists)
            //datePickersQuery.pickerWheels["29"].tap()
            //datePickersQuery.pickerWheels["2016"].tap()
            //datePickersQuery.pickerWheels["June"].tap()
            app.toolbars.buttons["Done"].tap()
            
            sleep(2)
            XCTAssert(permissionDesc.exists)
            permissionDesc.tap()
            permissionDesc.typeText( valueDescPermission )
            
            
            sleep(2)
            XCTAssert(selectDesc.exists)
            selectDesc.tap()
            
            sleep(2)
            XCTAssert(createPermission.exists)
            createPermission.tap()
            
            back.tap()
            
            sleep(3)
        } else {
            // back.tap()
            // sleep(2)
            back.tap()
            sleep(3)
        }
        
    }
    
    func test06DelPermission(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewPermission = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 2)
        let tablesQuery=app.tables
        let selectCategory = tablesQuery.staticTexts["Activities"]
        let valueTitlePermission = "A test"
        let valueTitlePermissionTwo = "A test two"
        let permission =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePermission)
        let permissionTwo = tablesQuery.cells.containing(.staticText, identifier:valueTitlePermissionTwo)
        let deleteCategory = permission.buttons["Trash Icon"]
        let deleteCategoryTwo = permissionTwo.buttons["Trash Icon"]
        let warning = app.alerts["Warning!"]
        let delete = warning.collectionViews.buttons["Delete"]
        
        //let cancel = warning.collectionViews.buttons["Cancel"]
        login()
        
        sleep(3)
        
        app.collectionViews.buttons["Permissions"].tap()
        
        sleep(3)
        preDelPermission()
        
        sleep(5)
        XCTAssert(viewPermission.exists)
        viewPermission.tap()
        
        sleep(5)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        
        sleep(5)
        if permission.element.exists {
            
            sleep(10)
            XCTAssert(permission.element.exists)
            sleep(12)
            XCTAssert(deleteCategory.exists)
            deleteCategory.tap()
            
            sleep(6)
            XCTAssert(warning.exists)
            XCTAssert(delete.exists)
            delete.tap()
            sleep(7)
            checkDelPermission(valueTitlePermission)
        } else if permissionTwo.element.exists{
            XCTAssert(permissionTwo.element.exists)
            XCTAssert(deleteCategoryTwo.exists)
            deleteCategoryTwo.tap()
            
            sleep(5)
            XCTAssert(warning.exists)
            XCTAssert(delete.exists)
            delete.tap()
            sleep(5)
            checkDelPermission(valueTitlePermissionTwo)
        }
    }
    
    func checkCreatedGroup(_ group : String){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 1)
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: group)
        
        let existingPermissionGroupsStaticText = tablesQuery.staticTexts["Existing Permission Groups"]
        
        sleep(2)
        viewCategory.tap()
        
        existingPermissionGroupsStaticText.tap()
        existingPermissionGroupsStaticText.swipeUp()
        
        sleep(2)
        XCTAssert(categoryItemCheck.element.exists)
        sleep(5)
        
    }
    func checkSavedGroup(_ group : String){
        
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: group)
        let existingPermissionGroupsStaticText = tablesQuery.staticTexts["Existing Permission Groups"]
        
        sleep(3)
        existingPermissionGroupsStaticText.tap()
        existingPermissionGroupsStaticText.swipeUp()
        
        sleep(6)
        
        XCTAssert(categoryItemCheck.element.exists)
        
        sleep(5)
    }
    
    func checkDelGroup(_ group : String){
        
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: group)
        let existingPermissionGroupsStaticText = tablesQuery.staticTexts["Existing Permission Groups"]
        
        sleep(2)
        existingPermissionGroupsStaticText.tap()
        existingPermissionGroupsStaticText.swipeUp()
        sleep(3)
        
        XCTAssertFalse(categoryItemCheck.element.exists)
        //  XCTAssert( tablesQuery.staticTexts[categoryEdit].exists)
        
        sleep(5)
    }
    
    func preAddGroup(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 1)
        let tablesQuery = app.tables
        let valueTitleGroup="A test"
        
        let permissionGroup =  tablesQuery.cells.containing(.staticText, identifier:valueTitleGroup)
        
        let permissionGroupTwo =  tablesQuery.cells.containing(.staticText, identifier:"A test two")
        let permissionGroupDelTwo = permissionGroupTwo.buttons["Trash Icon"]
        let permissionGroupDel = permissionGroup.buttons["Trash Icon"]
        let warning = app.alerts["Warning!"]
        let delete = warning.collectionViews.buttons["Delete"]
        //let cancel = warning.collectionViews.buttons["Cancel"]
        
        let existingPermissionGroupsStaticText = tablesQuery.staticTexts["Existing Permission Groups"]
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(4)
        XCTAssert( viewGroup.exists)
        viewGroup.tap()
        
        sleep(2)
        //XCTAssert( permissionGroup.element.exists)
        
        existingPermissionGroupsStaticText.tap()
        existingPermissionGroupsStaticText.swipeUp()
        sleep(3)
        
        
        if permissionGroup.element.exists {
            permissionGroupDel.tap()
            
            sleep(2)
            
            delete.tap()
            
            sleep(2)
            
            back.tap()
        } else if permissionGroupTwo.element.exists {
            permissionGroupDelTwo.tap()
            
            sleep(2)
            
            delete.tap()
            
            sleep(2)
            
            back.tap()
        }
        else {
            back.tap()
            sleep(2)
        }
        
    }
    
    func test07AddGroup(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let addGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 0)
        let tablesQuery=app.tables
        
        let titleGroup = tablesQuery.cells.containing(.staticText, identifier:"Title: *").children(matching: .textField).element
        
        let valueTitleGroup="A test"
        let button = tablesQuery.children(matching: .cell).element(boundBy: 8).children(matching: .button).element
        let createPermissionGroup = tablesQuery.children(matching: .cell).element(boundBy: 1).buttons["Create Permission Group"]
        login()
        
        sleep(3)
        
        
        app.collectionViews.buttons["Permissions"].tap()
        
        preAddGroup()
        
        sleep(7)
        XCTAssert(addGroup.exists)
        addGroup.tap()
        
        sleep(7)
        XCTAssert(titleGroup.exists)
        titleGroup.tap()
        titleGroup.typeText(valueTitleGroup)
        app.buttons["Return"].tap()
        
        sleep(7)
        XCTAssert(button.exists)
        button.tap()
        
        sleep(7)
        XCTAssert(createPermissionGroup.exists)
        createPermissionGroup.tap()
        
        sleep(5)
        
        checkCreatedGroup(valueTitleGroup)
    }
    
    func preEditGroup(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 1)
        let tablesQuery = app.tables
        let valueTitleGroup="A test"
        
        let permissionGroup =  tablesQuery.cells.containing(.staticText, identifier:valueTitleGroup)
        let permissionGrp = tablesQuery.cells.containing(.staticText, identifier:"A test two")
        
        let addGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 0)
        
        let titleGroup = tablesQuery.cells.containing(.staticText, identifier:"Title: *").children(matching: .textField).element
        
        let button = tablesQuery.children(matching: .cell).element(boundBy: 8).children(matching: .button).element
        let createPermissionGroup = tablesQuery.children(matching: .cell).element(boundBy: 1).buttons["Create Permission Group"]
        
        let existingPermissionGroupsStaticText = tablesQuery.staticTexts["Existing Permission Groups"]
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        XCTAssert( viewGroup.exists)
        viewGroup.tap()
        
        sleep(2)
        //XCTAssert( permissionGroup.element.exists)
        
        existingPermissionGroupsStaticText.tap()
        existingPermissionGroupsStaticText.swipeUp()
        sleep(3)
        
        if permissionGrp.element.exists{
            
            let permissionGroupDel = permissionGrp.buttons["Trash Icon"]
            let warning = app.alerts["Warning!"]
            let delete = warning.collectionViews.buttons["Delete"]
            permissionGroupDel.tap()
            
            sleep(2)
            
            delete.tap()
            
            sleep(6)
        }
        
        
        if !permissionGroup.element.exists {
            
            back.tap()
            sleep(3)
            addGroup.tap()
            
            sleep(5)
            XCTAssert(titleGroup.exists)
            titleGroup.tap()
            titleGroup.typeText(valueTitleGroup)
            app.buttons["Return"].tap()
            
            sleep(2)
            XCTAssert(button.exists)
            button.tap()
            
            sleep(2)
            XCTAssert(createPermissionGroup.exists)
            createPermissionGroup.tap()
            
            sleep(5)
            
            
        } else {
            back.tap()
            
            sleep(2)
        }
    }
    func test08EditGroup(){
        
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 1)
        let tablesQuery=app.tables
        
        let titleGroup = tablesQuery.cells.containing(.staticText, identifier:"Title: *").children(matching: .textField).element
        
        let selectAllPermissionThisCat = tablesQuery.children(matching: .cell).element(boundBy: 8).children(matching: .button).element
        
        let savePermissionGroup = tablesQuery.children(matching: .cell).element(boundBy: 1).buttons["Save Permission Group"]
        let valueTitleGroup="A test"
        let valueTitleGroupEdit = "A test two"
        let permissionGroup =  tablesQuery.cells.containing(.staticText, identifier:valueTitleGroup)
        let permissionGroupEdit = permissionGroup.buttons["Compose Icon"]
        
        let subGroup =  tablesQuery.staticTexts["Illnesses Accidents and Incidents"]
        let deleteKey = app.keys["delete"]
        
        let existingPermissionGroupsStaticText = tablesQuery.staticTexts["Existing Permission Groups"]
        login()
        
        sleep(3)
        
        
        app.collectionViews.buttons["Permissions"].tap()
        
        
        sleep(3)
        preEditGroup()
        sleep(6)
        XCTAssert( viewGroup.exists)
        viewGroup.tap()
        
        sleep(6)
        XCTAssert( permissionGroup.element.exists)
        
        sleep(5)
        existingPermissionGroupsStaticText.tap()
        existingPermissionGroupsStaticText.swipeUp()
        sleep(5)
        
        
        XCTAssert( permissionGroupEdit.exists)
        permissionGroupEdit.tap()
        
        
        sleep(6)
        XCTAssert( titleGroup.exists)
        titleGroup.tap()
        
        let countTitle = (titleGroup.value as! String).characters.count
        for _ in 0 ..< countTitle {
            deleteKey.tap()
        }
        titleGroup.typeText( valueTitleGroupEdit )
        app.buttons["Return"].tap()
        
        sleep(2)
        XCTAssert(selectAllPermissionThisCat.exists)
        selectAllPermissionThisCat.tap()
        
        
        sleep(2)
        XCTAssert(subGroup.exists)
        subGroup.tap()
        
        sleep(2)
        XCTAssert(savePermissionGroup.exists)
        savePermissionGroup.tap()
        
        sleep(5)
        checkSavedGroup(valueTitleGroupEdit)
        
    }
    
    func preDelGroup(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 1)
        let tablesQuery = app.tables
        let valueTitleGroup="A test two"
        
        let permissionGroup =  tablesQuery.cells.containing(.staticText, identifier:valueTitleGroup)
        
        
        let addGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 0)
        
        let titleGroup = tablesQuery.cells.containing(.staticText, identifier:"Title: *").children(matching: .textField).element
        
        let button = tablesQuery.children(matching: .cell).element(boundBy: 8).children(matching: .button).element
        let createPermissionGroup = tablesQuery.children(matching: .cell).element(boundBy: 1).buttons["Create Permission Group"]
        
        let existingPermissionGroupsStaticText = tablesQuery.staticTexts["Existing Permission Groups"]
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(5)
        XCTAssert( viewGroup.exists)
        viewGroup.tap()
        
        sleep(2)
        //XCTAssert( permissionGroup.element.exists)
        
        existingPermissionGroupsStaticText.tap()
        existingPermissionGroupsStaticText.swipeUp()
        sleep(3)
        
        
        if !permissionGroup.element.exists {
            
            back.tap()
            addGroup.tap()
            
            sleep(5)
            XCTAssert(titleGroup.exists)
            titleGroup.tap()
            titleGroup.typeText(valueTitleGroup)
            app.buttons["Return"].tap()
            
            sleep(5)
            XCTAssert(button.exists)
            button.tap()
            
            sleep(5)
            XCTAssert(createPermissionGroup.exists)
            createPermissionGroup.tap()
            
            sleep(5)
            //  back.tap()
            
            
        } else {
            back.tap()
            sleep(2)
        }
        
    }
    
    
    func test09DelGroup(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 1)
        let tablesQuery = app.tables
        let valueTitleGroup="A test"
        
        let permissionGroup =  tablesQuery.cells.containing(.staticText, identifier:valueTitleGroup)
        let permissionGroupDel = permissionGroup.buttons["Trash Icon"]
        
        
        let valueTitleGroupTwo="A test two"
        
        let permissionGroupTwo =  tablesQuery.cells.containing(.staticText, identifier:valueTitleGroupTwo)
        let permissionGroupDelTwo = permissionGroupTwo.buttons["Trash Icon"]
        
        let warning = app.alerts["Warning!"]
        let delete = warning.collectionViews.buttons["Delete"]
        //let cancel = warning.collectionViews.buttons["Cancel"]
        
        let existingPermissionGroupsStaticText = tablesQuery.staticTexts["Existing Permission Groups"]
        login()
        
        sleep(3)
        
        app.collectionViews.buttons["Permissions"].tap()
        sleep(3)
        preDelGroup()
        sleep(6)
        XCTAssert( viewGroup.exists)
        viewGroup.tap()
        
        sleep(6)
        existingPermissionGroupsStaticText.tap()
        existingPermissionGroupsStaticText.swipeUp()
        
        sleep(6)
        if permissionGroupTwo.element.exists {
            sleep(5)
            XCTAssert( permissionGroupTwo.element.exists)
            XCTAssert( permissionGroupDelTwo.exists)
            permissionGroupDelTwo.tap()
            
            
            sleep(6)
            XCTAssert(warning.exists)
            XCTAssert(delete.exists)
            delete.tap()
            
            sleep(6)
            
            checkDelGroup(valueTitleGroupTwo)
        } else if permissionGroup.element.exists {
            XCTAssert( permissionGroup.element.exists)
            XCTAssert( permissionGroupDel.exists)
            permissionGroupDel.tap()
            
            
            sleep(6)
            XCTAssert(warning.exists)
            XCTAssert(delete.exists)
            delete.tap()
            
            sleep(6)
            
            checkDelGroup(valueTitleGroup)
        }
    }
    
    func checkCreatedAssign(){
        let tablesQuery = app.tables
        
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Permissions"]
        
        let viewAssing = selectChild.buttons["expandButton"]
        
        // let assignItem = tablesQuery.childrenMatchingType(.Cell).elementBoundByIndex(10)
        
        sleep(2)
        
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        
        sleep(2)
        viewAssing.tap()
        
        
    }
    
    func preAssignCreate(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Permissions"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let existItem = selectChild.children(matching: .button).element(boundBy: 2)
        let viewAssing = selectChild.buttons["expandButton"]
        
        // let assignItem = tablesQuery.buttons["deleteButton"]
        
        
        let itemDel = tablesQuery.buttons["deleteButton"]
        //assignItem.buttons["Trash Icon"]
        
        let msgWarning = app.alerts["Warning!"]
        let warningDelete = msgWarning.collectionViews.buttons["Delete"]
        let back = app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(4)
        
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        sleep(4)
        XCTAssertTrue(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        if existItem.exists {
            
            sleep(2)
            XCTAssertTrue(viewAssing.exists)
            viewAssing.tap()
            
            sleep(2)
            XCTAssertTrue(itemDel.exists)
            itemDel.tap()
            
            sleep(3)
            XCTAssertTrue(msgWarning.exists)
            XCTAssertTrue(warningDelete.exists)
            warningDelete.tap()
            
            sleep(3)
            back.tap()
            
        } else {
            back.tap()
        }
        
    }
    
    
    func test10AssingCreate(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let managePermissionsStaticText =   tablesQuery.staticTexts["Manage Permissions"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let newAssing = selectChild.buttons["addButton"]
        
        let selectCategory = tablesQuery.children(matching: .cell).element(boundBy: 10).children(matching: .button).element
        
        let assingSelected = tablesQuery.buttons["Assign Selected"]
        
        let elem =  tablesQuery.children(matching: .cell).element(boundBy: 12)
        login()
        
        sleep(3)
        
        app.collectionViews.buttons["Permissions"].tap()
        
        sleep(3)
        preAssignCreate()
        sleep(8)
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        sleep(8)
        XCTAssert( newAssing.exists)
        newAssing.tap()
        
        sleep(8)
        XCTAssert( managePermissionsStaticText.exists)
        managePermissionsStaticText.tap()
        managePermissionsStaticText.swipeUp()
        
        sleep(8)
        XCTAssert( selectCategory.exists)
        selectCategory.tap()
        
        sleep(4)
        XCTAssert(elem.exists)
        elem.tap()
        elem.swipeDown()
        
        
        sleep(8)
        XCTAssert( assingSelected.exists)
        assingSelected.tap()
        
        sleep(8)
        checkCreatedAssign()
    }
    
    func preAssignEdit(){
        
    }
    func test11AssingEdit(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Permissions"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        
        let itemEdit = tablesQuery.buttons["editButton"]
        
        let managePermissionsStaticText = tablesQuery.staticTexts["Manage Permissions"]
        
        let categorySafeguarding = tablesQuery.otherElements["Safeguarding Children"]
        let selectAllCategory = tablesQuery.cells.containing(.staticText, identifier:"Select All Permissions in this Category ").children(matching: .button).element
        
        let btnAssignSelected = tablesQuery.children(matching: .cell).element(boundBy: 7).buttons["Assign Selected"]
        
        
        login()
        
        sleep(3)
        
        app.collectionViews.buttons["Permissions"].tap()
        
        sleep(5)
        preAssignEdit()
        
        sleep(5)
        XCTAssert(assing.exists)
        assing.tap()
        
        sleep(2)
        XCTAssert(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        
        sleep(2)
        XCTAssert(viewAssing.exists)
        viewAssing.tap()
        
        sleep(2)
        XCTAssert(itemEdit.exists)
        itemEdit.tap()
        
        sleep(4)
        waitFor(managePermissionsStaticText, seconds: 5)
        XCTAssert(managePermissionsStaticText.exists)
        managePermissionsStaticText.tap()
        managePermissionsStaticText.swipeUp()
        
        sleep(2)
        XCTAssert(categorySafeguarding.exists)
        categorySafeguarding.tap()
        selectAllCategory.tap()
        
        sleep(2)
        sleep(2)
        XCTAssert(btnAssignSelected.exists)
        btnAssignSelected.tap()
        
        sleep(5)
        
        sleep(5)
    }
    
    
    func ckeckAssignDelete(){
        
        let tablesQuery = app.tables
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        let viewAssing = selectChild.buttons["expandButton"]
        
        XCTAssertFalse(viewAssing.exists)
        
        sleep(5)
        
    }
    
    
    func preAssignDetele() {
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Permissions"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let existItem = selectChild.children(matching: .button).element(boundBy: 2)
        
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        sleep(4)
        XCTAssertTrue(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        if !existItem.exists {
            
            let newAssing = selectChild.buttons["addButton"]
            let managePermissionsStaticText =   tablesQuery.staticTexts["Manage Permissions"]
            let assingSelected = tablesQuery.children(matching: .cell).element(boundBy: 1).buttons["Assign Selected"]
            
            let elem =  tablesQuery.children(matching: .cell).element(boundBy: 12)
            let selectCategory = tablesQuery.children(matching: .cell).element(boundBy: 10).children(matching: .button).element
            sleep(2)
            XCTAssert( newAssing.exists)
            newAssing.tap()
            
            sleep(5)
            XCTAssert( managePermissionsStaticText.exists)
            managePermissionsStaticText.tap()
            managePermissionsStaticText.swipeUp()
            
            sleep(5)
            XCTAssert( selectCategory.exists)
            selectCategory.tap()
            
            sleep(5)
            XCTAssert(elem.exists)
            elem.tap()
            elem.swipeDown()
            
            
            sleep(7)
            XCTAssert( assingSelected.exists)
            assingSelected.tap()
            
            sleep(5)
            back.tap()
            
            sleep(3)
            
        } else {
            back.tap()
        }
    }
    
    
    func test14AssignDelete(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Permissions"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        
        
        let itemDel = tablesQuery.buttons["deleteButton"]
        
        let msgWarning = app.alerts["Warning!"]
        let warningDelete = msgWarning.collectionViews.buttons["Delete"]
        
        login()
        
        sleep(3)
        
        app.collectionViews.buttons["Permissions"].tap()
        
        sleep(2)
        preAssignDetele()
        sleep(5)
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        sleep(4)
        XCTAssertTrue(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        
        sleep(2)
        XCTAssertTrue(viewAssing.exists)
        viewAssing.tap()
        
        sleep(2)
        XCTAssertTrue(itemDel.exists)
        itemDel.tap()
        
        sleep(3)
        XCTAssertTrue(msgWarning.exists)
        XCTAssertTrue(warningDelete.exists)
        warningDelete.tap()
        
        sleep(6)
        
        ckeckAssignDelete()
    }
    
    
    func checkAddedSign(){
        
        
        
        let tablesQuery = app.tables
        
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Permissions"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.children(matching: .button).element(boundBy: 1)
        
        let itemSign = tablesQuery.buttons["signGreenButton"]
        
        sleep(8)
        XCTAssertTrue(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        
        sleep(8)
        XCTAssertTrue(viewAssing.exists)
        viewAssing.tap()
        
        sleep(7)
        XCTAssertTrue(itemSign.exists)
        
    }
    
    
    func checkDeletSign(){
        
        
        
        let tablesQuery = app.tables
        
        
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Permissions"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        
        let itemSign = tablesQuery.buttons["signRedButton"]
        
        sleep(5)
        XCTAssertTrue(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        
        sleep(2)
        XCTAssertTrue(viewAssing.exists)
        viewAssing.tap()
        
        sleep(2)
        XCTAssertTrue(itemSign.exists)
        
    }
    func preAssignDeleteSign(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Permissions"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        
        let itemSign = tablesQuery.buttons["signGreenButton"]
        
        let managePermissionsStaticText = tablesQuery.staticTexts["Manage Permissions"]
        
        let text = tablesQuery.staticTexts["If you have finished assigning Permissions to Android, tap the Assign Selected button above to save their Permissions."]
        
        let saveButton = app.buttons["Save"]
        
        let sign1Field = tablesQuery.textFields["signature1TextField"]
        let textParent1 = "parent one"
        let sign1 = tablesQuery.buttons["sign1Button"]
        
        let image = app.images["signatureImageView"]
        
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        let itemSignRed = tablesQuery.buttons["signRedButton"]
        let deleteKey = app.keys["delete"]
        
        
        
        let existItem = selectChild.children(matching: .button).element(boundBy: 2)
        
        
        sleep(6)
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        
        
        
        if !existItem.exists {
            
            let newAssing = selectChild.buttons["addButton"]
            let managePermissionsStaticText =   tablesQuery.staticTexts["Manage Permissions"]
            //let assingSelected = tablesQuery.childrenMatchingType(.Cell).elementBoundByIndex(1).buttons["Assign Selected"]
            let assingSelected = tablesQuery.buttons["Assign Selected"]
            
            
            
            let elem =  tablesQuery.children(matching: .cell).element(boundBy: 12)
            let selectCategory = tablesQuery.children(matching: .cell).element(boundBy: 10).children(matching: .button).element
            sleep(12)
            XCTAssert( newAssing.exists)
            newAssing.tap()
            
            sleep(7)
            XCTAssert( managePermissionsStaticText.exists)
            managePermissionsStaticText.tap()
            managePermissionsStaticText.swipeUp()
            
            sleep(7)
            XCTAssert( selectCategory.exists)
            selectCategory.tap()
            
            sleep(5)
            XCTAssert(elem.exists)
            elem.tap()
            elem.swipeDown()
            
            
            sleep(10)
            XCTAssert( assingSelected.exists)
            assingSelected.tap()
            
            sleep(15)
            
            sleep(2)
            XCTAssertTrue(assignPermissionsStaticText.exists)
            assignPermissionsStaticText.tap()
            assignPermissionsStaticText.swipeUp()
            
            sleep(4)
            XCTAssertTrue(viewAssing.exists)
            viewAssing.tap()
            
            sleep(6)
            XCTAssertTrue(itemSignRed.exists)
            itemSignRed.tap()
            
            
            sleep(5)
            XCTAssert(managePermissionsStaticText.exists)
            managePermissionsStaticText.tap()
            sleep(1)
            managePermissionsStaticText.swipeUp()
            
            sleep(4)
            XCTAssert(text.exists)
            text.tap()
            text.swipeUp()
            
            sleep(3)
            XCTAssertTrue(sign1Field.exists)
            sign1Field.tap()
            
            let countTitle1 = (sign1Field.value as! String).characters.count
            for _ in 0 ..< countTitle1 {
                deleteKey.tap()
            }
            sleep(2)
            sign1Field.typeText(textParent1)
            app.buttons["Return"].tap()
            
            
            
            sleep(3)
            XCTAssertTrue(sign1.exists)
            sign1.tap()
            
            sleep(2)
            
            image.tap()
            image.swipeLeft()
            image.tap()
            
            sleep(1)
            sleep(3)
            XCTAssertTrue(saveButton.exists)
            saveButton.tap()
            
            sleep(3)
            back.tap()
            
            
            sleep(7)
            back.tap()
        } else {
            
            sleep(2)
            XCTAssertTrue(assignPermissionsStaticText.exists)
            assignPermissionsStaticText.tap()
            assignPermissionsStaticText.swipeUp()
            sleep(4)
            XCTAssertTrue(viewAssing.exists)
            viewAssing.tap()
            
            sleep(4)
            if itemSign.exists {
                back.tap()
            } else {
                
                XCTAssertTrue(itemSignRed.exists)
                itemSignRed.tap()
                
                
                sleep(3)
                XCTAssert(managePermissionsStaticText.exists)
                managePermissionsStaticText.tap()
                sleep(1)
                managePermissionsStaticText.swipeUp()
                
                sleep(4)
                XCTAssert(text.exists)
                text.tap()
                text.swipeUp()
                
                sleep(3)
                XCTAssertTrue(sign1Field.exists)
                sign1Field.tap()
                
                let countTitle1 = (sign1Field.value as! String).characters.count
                for _ in 0 ..< countTitle1 {
                    deleteKey.tap()
                }
                sleep(2)
                sign1Field.typeText(textParent1)
                app.buttons["Return"].tap()
                
                
                
                sleep(3)
                XCTAssertTrue(sign1.exists)
                sign1.tap()
                
                sleep(2)
                
                image.tap()
                image.swipeLeft()
                image.tap()
                
                sleep(1)
                sleep(3)
                XCTAssertTrue(saveButton.exists)
                saveButton.tap()
                
                sleep(3)
                back.tap()
                
                
                sleep(7)
                back.tap()
            }
        }
        
        sleep(3)
        
    }
    
    
    
    func test13AssignDeleteSign(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Permissions"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        
        
        let itemSign = tablesQuery.buttons["signGreenButton"]
        
        let managePermissionsStaticText = tablesQuery.staticTexts["View Android's Permissions"]
        
        
        //    let sign1Field = tablesQuery.textFields["signature1TextField"]
        let sign1 = tablesQuery.buttons["sign1Button"]
        let deleteButton = app.buttons["Delete"]
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        
        //    let deleteKey = app.keys["delete"]
        
        login()
        
        sleep(7)
        
        app.collectionViews.buttons["Permissions"].tap()
        
        sleep(3)
        preAssignDeleteSign()
        sleep(9)
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        sleep(2)
        XCTAssertTrue(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        
        sleep(2)
        XCTAssertTrue(viewAssing.exists)
        viewAssing.tap()
        
        sleep(5)
        XCTAssertTrue(itemSign.exists)
        itemSign.tap()
        
        sleep(7)
        XCTAssertTrue(managePermissionsStaticText.exists)
        managePermissionsStaticText.tap()
        managePermissionsStaticText.swipeUp()
        
        
        
        sleep(8)
        XCTAssertTrue(sign1.exists)
        sign1.tap()
        sleep(2)
        XCTAssertTrue(deleteButton.exists)
        deleteButton.tap()
        
        sleep(3)
        //    XCTAssertTrue(sign1Field.exists)
        //  sign1Field.tap()
        sleep(3)
        ///   let countTitle1 = (sign1Field.value as! String).characters.count
        //  for _ in 0 ..< countTitle1 {
        //      deleteKey.tap()
        //  }
        //  sleep(2)
        
        ///  app.buttons["Return"].tap()
        
        
        sleep(3)
        
        back.tap()
        sleep(3)
        checkDeletSign()
    }
    
    
    
    func preAssignAddSign(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Permissions"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        
        let itemSign = tablesQuery.buttons["signRedButton"]
        
        let managePermissionsStaticText = tablesQuery.staticTexts["View Android's Permissions"]
        let sign1 = tablesQuery.buttons["sign1Button"]
        
        let deleteButton = app.buttons["Delete"]
        let itemSignGreen = tablesQuery.buttons["signGreenButton"]
        //  let deleteKey = app.keys["delete"]
        //  let sign1Field = tablesQuery.textFields["signature1TextField"]
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        
        let existItem = selectChild.children(matching: .button).element(boundBy: 2)
        
        
        sleep(15)
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        
        sleep(5)
        
        if !existItem.exists {
            
            let newAssing = selectChild.buttons["addButton"]
            let managePermissionsStaticText =   tablesQuery.staticTexts["Manage Permissions"]
            //let assingSelected = tablesQuery.childrenMatchingType(.Cell).elementBoundByIndex(1).buttons["Assign Selected"]
            let assingSelected = tablesQuery.buttons["Assign Selected"]
            
            
            
            let elem =  tablesQuery.children(matching: .cell).element(boundBy: 12)
            let selectCategory = tablesQuery.children(matching: .cell).element(boundBy: 10).children(matching: .button).element
            sleep(7)
            XCTAssert( newAssing.exists)
            newAssing.tap()
            
            sleep(7)
            XCTAssert( managePermissionsStaticText.exists)
            managePermissionsStaticText.tap()
            managePermissionsStaticText.swipeUp()
            
            sleep(7)
            XCTAssert( selectCategory.exists)
            selectCategory.tap()
            
            sleep(5)
            XCTAssert(elem.exists)
            elem.tap()
            elem.swipeDown()
            
            
            sleep(10)
            XCTAssert( assingSelected.exists)
            assingSelected.tap()
            
            sleep(15)
            
            
            
            sleep(3)
            back.tap()
            sleep(3)
            // back.tap()
            
            
        }else {
            
            sleep(6)
            XCTAssertTrue(viewAssing.exists)
            viewAssing.tap()
            
            sleep(5)
            XCTAssertTrue(assignPermissionsStaticText.exists)
            assignPermissionsStaticText.tap()
            assignPermissionsStaticText.swipeUp()
            
            
            sleep(4)
            if itemSign.exists {
                back.tap()
            } else {
                
                sleep(5)
                XCTAssertTrue(itemSignGreen.exists)
                itemSignGreen.tap()
                
                sleep(5)
                XCTAssert(managePermissionsStaticText.exists)
                managePermissionsStaticText.tap()
                sleep(1)
                managePermissionsStaticText.swipeUp()
                
                sleep(3)
                XCTAssertTrue(sign1.exists)
                sign1.tap()
                sleep(2)
                XCTAssertTrue(deleteButton.exists)
                deleteButton.tap()
                
                
                sleep(3)
                
                //     XCTAssertTrue(sign1Field.exists)
                //   sign1Field.tap()
                //   let countTitle1 = (sign1Field.value as! String).characters.count
                //    for _ in 0 ..< countTitle1 {
                //      deleteKey.tap()
                //  }
                // app.buttons["Return"].tap()
                
                
                sleep(5)
                
                back.tap()
                
                sleep(6)
                back.tap()
            }
        }
        
        sleep(3)
        
        
    }
    
    
    func test12AssignAddSign(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let assing = element.children(matching: .button).matching(identifier: "permissions button 1").element(boundBy: 0)
        
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Permissions"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        let itemSign = tablesQuery.buttons["signRedButton"]
        let managePermissionsStaticText = tablesQuery.staticTexts["Manage Permissions"]
        
        let text = tablesQuery.staticTexts["If you have finished assigning Permissions to Android, tap the Assign Selected button above to save their Permissions."]
        
        let saveButton = app.buttons["Save"]
        
        let sign1Field = tablesQuery.textFields["signature1TextField"]
        let textParent1 = "parent one"
        let sign1 = tablesQuery.buttons["sign1Button"]
        
        let deleteKey = app.keys["delete"]
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        
        
        let image = app.images["signatureImageView"]
        
        
        login()
        
        sleep(5)
        
        app.collectionViews.buttons["Permissions"].tap()
        
        sleep(3)
        preAssignAddSign()
        sleep(6)
        XCTAssert(assing.exists)
        assing.tap()
        
        sleep(5)
        XCTAssert(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        
        sleep(5)
        XCTAssert(viewAssing.exists)
        viewAssing.tap()
        
        sleep(6)
        XCTAssert(itemSign.exists)
        itemSign.tap()
        
        
        sleep(6)
        XCTAssert(managePermissionsStaticText.exists)
        managePermissionsStaticText.tap()
        sleep(1)
        managePermissionsStaticText.swipeUp()
        
        
        
        
        
        sleep(4)
        XCTAssert(text.exists)
        text.tap()
        text.swipeUp()
        
        sleep(3)
        XCTAssertTrue(sign1Field.exists)
        sign1Field.tap()
        
        let countTitle1 = (sign1Field.value as! String).characters.count
        for _ in 0 ..< countTitle1 {
            deleteKey.tap()
        }
        sleep(2)
        sign1Field.typeText(textParent1)
        app.buttons["Return"].tap()
        
        sleep(3)
        XCTAssertTrue(sign1.exists)
        sign1.tap()
        
        sleep(3)
        
        image.tap()
        image.swipeLeft()
        image.tap()
        
        
        sleep(3)
        XCTAssertTrue(saveButton.exists)
        saveButton.tap()
        
        sleep(6)
        
        
        back.tap()
        
        sleep(7)
        
        
        checkAddedSign()
        
    }
    
}
