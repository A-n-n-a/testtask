//
//  UIVideoTests.swift
//  pd2
//


import Foundation
import UIKit
import XCTest

class UIVideoTests: UITestCase {
    func testUploadVideo() {
        let selectVideoSource = app.sheets["Please select video source"]
        let tablesQuery = app.tables
        let uploadVideo = app.tables.cells.containing(.staticText, identifier:"Upload Video").children(matching: .button).element
        let datePickersQuery = app.datePickers
        let videoTakenCellsQuery = tablesQuery.cells.containing(.staticText, identifier:"Video Taken:").children(matching: .textField).element(boundBy: 0)
        let uploadVideos = tablesQuery.cells.containing(.staticText, identifier:"Upload Videos").children(matching: .button).element
        let titleVideo = tablesQuery.cells.containing(.staticText, identifier:"Video Taken:").children(matching: .textField).element(boundBy: 1)
        let descriptionVideo = tablesQuery.cells.containing(.staticText, identifier:"Video Taken:").children(matching: .textView).element
        
        goGalleryChild("videos")
        
        //Select a video from the gallery
        waitFor(uploadVideo, seconds: 5)
        uploadVideo.tap()
        XCTAssert(selectVideoSource.exists)
        selectVideoSource.collectionViews.buttons["Choose video from library"].tap()
        if app.collectionViews.images.count > 0 {
            app.collectionViews.images.element(boundBy: 0).tap()
        }
        app.navigationBars["Camera Roll"].buttons["Done"].tap()
        waitFor(app.navigationBars["Baby's Day"].staticTexts["Upload video"], seconds: 5)
        
        //Input 'Video Taken'
        videoTakenCellsQuery.tap()
        XCTAssert(datePickersQuery.element.exists)
        app.pickerWheels.element(boundBy: 0).adjust(toPickerWheelValue: testDataUploadVideo["month"]!)
        app.pickerWheels.element(boundBy: 1).adjust(toPickerWheelValue: testDataUploadVideo["day"]!)
        app.pickerWheels.element(boundBy: 2).adjust(toPickerWheelValue: testDataUploadVideo["year"]!)
        app.toolbars.buttons["Done"].tap()
        waitFor(videoTakenCellsQuery, seconds: 5)
        
        XCTAssertEqual(videoTakenCellsQuery.value as! String, testDataUploadVideo["date"]!)
        XCTAssertEqual(datePickersQuery.element.exists, false)
        
        //Input 'Title'
        if !tablesQuery.staticTexts["Title"].exists {
            tablesQuery.cells.containing(.staticText, identifier:"Video Taken:").children(matching: .button).element.swipeUp()
        }
        tablesQuery.textFields["Title"].tap()
        XCTAssert(app.keys.element.exists)
        titleVideo.typeText(testDataUploadVideo["title"]!)
        
        tablesQuery.staticTexts["Title:"].tap()
        XCTAssertEqual(app.keys.element.exists, false)
        XCTAssertEqual(titleVideo.value as! String, testDataUploadVideo["title"]!)
        
        //Input 'Description'
        descriptionVideo.tap()
        XCTAssert(app.keys.element.exists)
        descriptionVideo.typeText(testDataUploadVideo["description"]!)
        tablesQuery.staticTexts["Description:"].tap()
        XCTAssertEqual(app.keys.element.exists, false)
        XCTAssertEqual(descriptionVideo.value as! String, testDataUploadVideo["description"]!)
        
        //Select area of learning
        app.tables.buttons["Duplicate to Additional Outcomes"].tap()
        waitFor(tablesQuery.staticTexts["Select the Area of Learning:"], seconds: 5)
        
        XCTAssert(tablesQuery.staticTexts["Select the Area of Learning:"].exists)
        
        //Check area learning
        tablesQuery.staticTexts[testDataUploadVideo["area"]!].tap()
        tablesQuery.staticTexts[testDataUploadVideo["aspect"]!].tap()
        waitFor(tablesQuery.staticTexts[testDataUploadVideo["outcome"]!], seconds: 5)
        
        tablesQuery.staticTexts[testDataUploadVideo["outcome"]!].tap()
        waitFor(tablesQuery.staticTexts[testDataUploadVideo["area"]!], seconds: 5)
        
        XCTAssert(tablesQuery.staticTexts[testDataUploadVideo["area"]!].exists)
        XCTAssert(tablesQuery.staticTexts[testDataUploadVideo["aspect"]!].exists)
        XCTAssert(tablesQuery.staticTexts[testDataUploadVideo["age_outcome"]!].exists)
        
        tablesQuery.staticTexts["Listening and Attention"].swipeUp()
        tablesQuery.buttons["Upload selected video"].tap()
        waitFor(app.tables.cells.containing(.staticText, identifier:"Select Year and Month:").children(matching: .textField).element, seconds: 10)
        
        app.tables.cells.containing(.staticText, identifier:"Select Year and Month:").children(matching: .textField).element.tap()
        
        app.pickerWheels["April, 1 of 1"].tap()
        app.toolbars.buttons["Done"].tap()
        tablesQuery.buttons["Enter Gallery"].tap()
        waitFor(app.navigationBars["Baby's Day"].staticTexts["General Gallery"], seconds: 5)
        XCTAssert(tablesQuery.children(matching: .cell).element(boundBy: 3).children(matching: .button).count > 0)
        
    }
    
    func testViewVideo() {
        let tablesQuery = app.tables
        let selectYear = tablesQuery.cells.containing(.staticText, identifier:"Select Year and Month:").children(matching: .textField).element
        let textView = tablesQuery.children(matching: .cell).element(boundBy: 0).children(matching: .textView).element
        
        goGalleryChild("videos")
        
        selectYear.tap()
        XCTAssert(app.pickerWheels.element.exists)
        app.pickerWheels[testDataViewVideo["selectYearInput"]!].tap()
        app.toolbars.buttons["Done"].tap()
        XCTAssertEqual(app.pickerWheels.element.exists, false)
        XCTAssertEqual(selectYear.value as! String, testDataViewVideo["selectYearValue"]!)
        XCTAssert(tablesQuery.buttons["Enter Gallery"].exists)
        
        tablesQuery.buttons["Enter Gallery"].tap()
        XCTAssert(tablesQuery.staticTexts["Video Galleries"].exists)
        
        var checkText = "\(childInfo["firstName"]!) \(childInfo["lastName"]!)'s General Gallery - \(testDataViewVideo["checkSelectYearValue"]!)" 
        var countCheckText = checkText.characters.count
        var endIndexCheckText = checkText.characters.index(checkText.startIndex, offsetBy: countCheckText-1)
        var range = checkText[checkText.startIndex...endIndexCheckText]
        XCTAssertEqual(checkText, range)
        tablesQuery.children(matching: .cell).element(boundBy: 3).children(matching: .button).element(boundBy: 0).tap()
        XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
        XCTAssert(tablesQuery.staticTexts["\(childInfo["firstName"]!)'s current age: \(childInfo["age"]!) months"].exists)
        tablesQuery.cells.containing(.staticText, identifier: "\(childInfo["firstName"]!)'s current age: \(childInfo["age"]!) months" ).children(matching: .button).element(boundBy: 0).tap()
    }
    
    /*
     At first need set parameters fot 'UIDataTests\dataCommentVideo'.
     */
    func testCommentVideo() {
        let tablesQuery = app.tables
        let selectYear = tablesQuery.cells.containing(.staticText, identifier:"Select Year and Month:").children(matching: .textField).element
        let textView = tablesQuery.children(matching: .cell).element(boundBy: 0).children(matching: .textView).element
        let textComment = tablesQuery.cells.containing(.button, identifier:"Add Comment").children(matching: .textView).element
        let videoButtons = tablesQuery.children(matching: .cell).element(boundBy: 3).children(matching: .button)
        let buttonBack = app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        goGalleryChild("videos")
        
        waitFor(selectYear, seconds: 5)
        selectYear.tap()
        waitFor(app.pickerWheels.element, seconds: 5)
        XCTAssert(app.pickerWheels.element.exists)
        app.pickerWheels[testDataCommentVideo["selectYearInput"]!].tap()
        app.toolbars.buttons["Done"].tap()
        XCTAssertEqual(app.pickerWheels.element.exists, false)
        XCTAssertEqual(selectYear.value as! String, testDataCommentVideo["selectYearValue"]!)
        XCTAssert(tablesQuery.buttons["Enter Gallery"].exists)
        
        tablesQuery.buttons["Enter Gallery"].tap()
        XCTAssert(tablesQuery.staticTexts["Video Galleries"].exists)
        
        var checkText = "\(childInfo["firstName"]!) \(childInfo["lastName"]!)'s General Gallery - \(testDataCommentVideo["checkSelectYearValue"]!)" 
        var checkView = textView.value as! String
        var countCheckText = checkText.characters.count
        var endIndexCheckView = checkView.characters.index(checkView.startIndex, offsetBy: countCheckText-1)
        var range = checkView[checkView.startIndex...endIndexCheckView]
        XCTAssertEqual(checkText, range)
        
        if videoButtons.count > 0 {
            var lastIndex = videoButtons.count - 1
            for index in 0 ... lastIndex {
                videoButtons.element(boundBy: index).tap()
                XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
                if tablesQuery.cells.containing(.staticText, identifier: testDataCommentVideo["title"]!).element.exists  && tablesQuery.cells.containing(.staticText, identifier: testDataCommentVideo["date"]!).element.exists {
                    XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
                    XCTAssert(tablesQuery.staticTexts["\(childInfo["firstName"]!)'s current age: \(childInfo["age"]!) months"].exists)
                    
                    //Input 'Comment'
                    app.tables.buttons["•••"].swipeUp()
                    textComment.tap()
                    XCTAssert(app.keys.element.exists)
                    textComment.typeText(testDataCommentVideo["comment"]!)
                    
                    textComment.swipeDown()
                    tablesQuery.staticTexts["Title:"].tap()
                    tablesQuery.staticTexts["Title:"].swipeUp()
                    
                    sleep(2)
                    XCTAssertEqual(app.keys.element.exists, false)
                    
                    var todaysDate = Date()
                    var dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd.MM.yyyy @ HH:mm"
                    dateFormatter.timeZone = TimeZone(secondsFromGMT: 3600)
                    var DateInFormat = dateFormatter.string(from: todaysDate)
                    var endIndexDate=DateInFormat.characters.index(DateInFormat.startIndex, offsetBy: 17)
                    var rangeDate = DateInFormat[DateInFormat.startIndex...endIndexDate]
                    app.tables.buttons["Add Comment"].tap()
                    app.tables.buttons["•••"].swipeUp()
                    XCTAssert(tablesQuery.staticTexts[rangeDate].exists)
                    XCTAssert(tablesQuery.cells.containing(.staticText, identifier:testDataCommentVideo["comment"]!).staticTexts[infoLogin["user"]!].exists)
                    sleep(2)
                    
                    
                    break
                } else {
                    buttonBack.tap()
                }
            }
        }
        
    }
    
    /*
     At first need set parameters fot 'dataDeleteVideo'.
     */
    
    func testDeleteVideo() {
        let tablesQuery = app.tables
        let selectYear = tablesQuery.cells.containing(.staticText, identifier:"Select Year and Month:").children(matching: .textField).element
        let textView = tablesQuery.children(matching: .cell).element(boundBy: 0).children(matching: .textView).element
        let imageButtons = tablesQuery.children(matching: .cell).element(boundBy: 3).children(matching: .button)
        let buttonBack = app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        let warningAlert = app.alerts["Warning!"]
        
        goGalleryChild("videos")
        
        selectYear.tap()
        XCTAssert(app.pickerWheels.element.exists)
        
        app.pickerWheels[testDataDeleteVideo["selectYearInput"]!].tap()
        app.toolbars.buttons["Done"].tap()
        XCTAssertEqual(app.pickerWheels.element.exists, false)
        XCTAssertEqual(selectYear.value as! String, testDataDeleteVideo["selectYearValue"]!)
        XCTAssert(tablesQuery.buttons["Enter Gallery"].exists)
        
        tablesQuery.buttons["Enter Gallery"].tap()
        XCTAssert(tablesQuery.staticTexts["Video Galleries"].exists)
        
        
        var checkText = "\(childInfo["firstName"]!) \(childInfo["lastName"]!)'s General Gallery - \(testDataDeleteVideo["checkSelectYearValue"]!)" 
        var checkView = textView.value as! String
        var countCheckText = checkText.characters.count
        var endIndexCheckView = checkView.characters.index(checkView.startIndex, offsetBy: countCheckText-1)
        var range = checkView[checkView.startIndex...endIndexCheckView]
        XCTAssertEqual(checkText, range)
        
        tablesQuery.staticTexts["Video Galleries"].swipeUp()
        
        if imageButtons.count > 0 {
            var lastIndex = imageButtons.count - 1
            for index in 0 ... lastIndex {
                imageButtons.element(boundBy: index).tap()
                XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
                if tablesQuery.cells.containing(.staticText, identifier: testDataDeleteVideo["title"]!).element.exists  && tablesQuery.cells.containing(.staticText, identifier: testDataDeleteVideo["date"]!).element.exists {
                    XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
                    XCTAssert(tablesQuery.staticTexts["\(childInfo["firstName"]!)'s current age: \(childInfo["age"]!) months"].exists)
                    
                    
                    //Delete Video
                    tablesQuery.buttons["•••"].tap()
                    XCTAssert(app.sheets.collectionViews.buttons["Delete Video"].exists)
                    app.sheets.collectionViews.buttons["Delete Video"].tap()
                    XCTAssert(warningAlert.staticTexts["Deleting this information from the system can NOT be reversed!"].exists)
                    warningAlert.collectionViews.buttons["Delete"].tap()
                    XCTAssert(app.navigationBars["Baby's Day"].staticTexts["General Gallery"].exists)
                    
                    //Check text 'Empty'
                    if imageButtons.count == 0 {
                        XCTAssertEqual(tablesQuery.staticTexts["Empty"].exists, false)
                    } else {
                        XCTAssert(tablesQuery.staticTexts["Empty"].exists)
                    }
                    
                    break
                } else {
                    buttonBack.tap()
                }
            }
        }
    }
    
    func testEditVideo() {
        let tablesQuery = app.tables
        let selectYear = tablesQuery.cells.containing(.staticText, identifier:"Select Year and Month:").children(matching: .textField).element
        let videoTakenCellsQuery = tablesQuery.cells.containing(.staticText, identifier:"Video Taken:").children(matching: .textField).element(boundBy: 0)
        let datePickersQuery = app.datePickers
        let titleVideo = tablesQuery.cells.containing(.staticText, identifier:"Video Taken:").children(matching: .textField).element(boundBy: 1)
        let descriptionVideo = XCUIApplication().tables.cells.containing(.staticText, identifier:"Video Taken:").children(matching: .textView).element
        let buttonBack = app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        let selectAolCellsQuery = tablesQuery.cells.containing(.staticText, identifier:"Select AOL:")
        
        goGalleryChild("videos")
        
        waitFor(selectYear, seconds: 10)
        selectYear.tap()
        XCTAssert(app.pickerWheels.element.exists)
        app.pickerWheels[testDataEditVideo["selectYearInput"]!].tap()
        
        app.toolbars.buttons["Done"].tap()
        XCTAssertEqual(app.pickerWheels.element.exists, false)
        XCTAssertEqual(selectYear.value as! String, testDataEditVideo["selectYearValue"]!)
        XCTAssert(tablesQuery.buttons["Enter Gallery"].exists)
        
        tablesQuery.buttons["Enter Gallery"].tap()
        XCTAssert(tablesQuery.staticTexts["Video Galleries"].exists)
        
        checkText("\(childInfo["firstName"]!) \(childInfo["lastName"]!)'s General Gallery - \(testDataEditVideo["checkSelectYearValue"]!)", tablesQuery.children(matching: .cell).element(boundBy: 0).children(matching: .textView).element.value as! String)
        
        tablesQuery.children(matching: .cell).element(boundBy: 3).children(matching: .button).element(boundBy: 0).tap()
        XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
        XCTAssert(tablesQuery.staticTexts["\(childInfo["firstName"]!)'s current age: \(childInfo["age"]!) months"].exists)
        
        
        //Edit Video
        tablesQuery.buttons["•••"].tap()
        app.sheets.collectionViews.buttons["Edit Video"].tap()
        XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Edit video"].exists)
        app.navigationBars["Baby's Day"].staticTexts["Edit video"].tap()
        
        //Input 'Video Taken'
        videoTakenCellsQuery.tap()
        XCTAssert(datePickersQuery.element.exists)
        app.pickerWheels.element(boundBy: 0).adjust(toPickerWheelValue: testDataEditVideo["month"]!)
        app.pickerWheels.element(boundBy: 1).adjust(toPickerWheelValue: testDataEditVideo["day"]!)
        app.pickerWheels.element(boundBy: 2).adjust(toPickerWheelValue: testDataEditVideo["year"]!)
        app.toolbars.buttons["Done"].tap()
        waitFor(videoTakenCellsQuery, seconds: 5)
        XCTAssertEqual(videoTakenCellsQuery.value as! String, testDataEditVideo["date"]!)
        XCTAssertEqual(datePickersQuery.element.exists, false)
        
        //Input 'Title'
        tablesQuery.staticTexts["Video Galleries"].swipeUp()
        var originTitle = titleVideo.value!
        titleVideo.tap()
        XCTAssert(app.keys.element.exists)
        for symbol in editTitleVideo {
            app.keys[symbol].tap()
        }
        tablesQuery.staticTexts["Title:"].tap()
        XCTAssertEqual(app.keys.element.exists, false)
        XCTAssertEqual(titleVideo.value as! String, "\(originTitle) " + testDataEditVideo["title"]!)
        
        //Input 'Description'
        var originDescription = descriptionVideo.value!
        descriptionVideo.tap()
        XCTAssert(app.keys.element.exists)
        for symbol in editDescriptionVideo {
            app.keys[symbol].tap()
        }
        tablesQuery.staticTexts["Description:"].tap()
        XCTAssertEqual(app.keys.element.exists, false)
        XCTAssertEqual(descriptionVideo.value as! String, testDataEditVideo["description"]! + " \(originDescription)")
        
        //Select area of learning
        app.tables.buttons["Duplicate to Additional Outcomes"].tap()
        sleep(2)
        XCTAssert(tablesQuery.staticTexts["Select the Area of Learning:"].exists)
        
        //Check area learning
        tablesQuery.staticTexts[testDataEditVideo["area"]!].tap()
        tablesQuery.staticTexts[testDataEditVideo["aspect"]!].tap()
        waitFor(tablesQuery.staticTexts[testDataEditVideo["outcome"]!], seconds: 5)
        tablesQuery.staticTexts[testDataEditVideo["outcome"]!].tap()
        waitFor(tablesQuery.staticTexts[testDataEditVideo["area"]!], seconds: 5)
        XCTAssert(tablesQuery.staticTexts[testDataEditVideo["area"]!].exists)
        waitFor(tablesQuery.staticTexts[testDataEditVideo["aspect"]!], seconds: 5)
        XCTAssert(tablesQuery.staticTexts[testDataEditVideo["aspect"]!].exists)
        XCTAssert(tablesQuery.staticTexts[testDataEditVideo["age_outcome"]!].exists)
        
        tablesQuery.staticTexts["Description:"].swipeUp()
        tablesQuery.staticTexts["Description:"].swipeUp()
        tablesQuery.buttons["Update Video"].tap()
        waitFor(app.navigationBars["Baby's Day"].staticTexts["Comments"], seconds: 10)
        XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
        XCTAssert(tablesQuery.staticTexts["\(testDataEditVideo["date"]!)"].exists)
        sleep(2)
        XCTAssert(tablesQuery.staticTexts["\(originTitle) " + testDataEditVideo["title"]!].exists)
        XCTAssert(tablesQuery.staticTexts[testDataEditVideo["description"]! + " \(originDescription)"].exists)
        
        //Check Progress Gallery
        buttonBack.tap()
        buttonBack.tap()
        
        tablesQuery.cells.containing(.staticText, identifier:"\(childInfo["firstName"]!)").buttons["galleries small btn 3"].tap()
        
        
        waitFor(selectAolCellsQuery.children(matching: .textField).element(boundBy: 0), seconds: 10)
        if selectAolCellsQuery.children(matching: .textField).element(boundBy: 0).value as! String != testDataEditVideo["shortArea"]! {
            //Select short area name is not implemented
            
        } else if selectAolCellsQuery.children(matching: .textField).element(boundBy: 1).value as! String != testDataEditVideo["aspect"] {
            //Select aspect name is not implemented
        }
        tablesQuery.buttons["Enter Gallery"].tap()
        XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Progress Gallery"].exists)
        
        checkText("\(childInfo["firstName"]!) \(childInfo["lastName"]!)'s Progress Gallery - \(testDataEditVideo["shortArea"]!)", tablesQuery.children(matching: .cell).element(boundBy: 0).children(matching: .textView).element.value as! String)
        checkVideo((originTitle as! String + " " + testDataEditVideo["title"]!), testDataEditVideo["date"]!)
    }
}
