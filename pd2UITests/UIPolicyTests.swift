 //
 //  UIPermissionTests.swift
 //  pd2
 //
 //  Created by Admin on 17.06.16.
 //  Copyright © 2016 Admin. All rights reserved.
 //
 
 import Foundation
 import UIKit
 import XCTest
 
 class UIPolicyTests: UITestCase {
    
    
    func checkCreatedCategory(_ category : String){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 1)
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: category)
        
        sleep(2)
        viewCategory.tap()
        sleep(4)
        XCTAssert(categoryItemCheck.element.exists)
        sleep(5)
        
    }
    
    
    
    func checkSavedCategory(_ category : String){
        
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: category)
        
        sleep(7)
        
        XCTAssertTrue(categoryItemCheck.element.exists)
        // XCTAssertFalse(tablesQuery.staticTexts[category].exists)
        sleep(5)
    }
    
    func checkDelCategory(_ category : String){
        
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: category)
        
        sleep(5)
        
        XCTAssertFalse(categoryItemCheck.element.exists)
        
        sleep(5)
    }
    
    func preAddCategory() {
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 1)
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: "A test")
        
        let categoryItemCheckTwo = tablesQuery.cells.containing(.staticText, identifier: "A test two")
        let delCategory = categoryItemCheck.buttons["Trash Icon"]
        let delCategoryTwo = categoryItemCheckTwo.buttons["Trash Icon"]
        let warning = app.alerts["Warning!"]
        let delete = warning.collectionViews.buttons["Delete"]
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(9)
        
        viewCategory.tap()
        sleep(4)
        if categoryItemCheck.element.exists {
            sleep(2)
            delCategory.tap()
            sleep(1)
            delete.tap()
        }
        sleep(8)
        if categoryItemCheckTwo.element.exists {
            sleep(2)
            delCategoryTwo.tap()
            sleep(1)
            delete.tap()
        }
        
        sleep(5)
        
        back.tap()
        
    }
    
    func preEditCategory(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 1)
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: "A test")
        
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(2)
        
        viewCategory.tap()
        sleep(4)
        if categoryItemCheck.element.exists {
            back.tap()
            sleep(3)
        } else {
            sleep(5)
            tablesQuery.children(matching: .cell).element(boundBy: 1).buttons["Add a New Policy Category"].tap()
            
            let tablesQuery=app.tables
            let categoryTitle = tablesQuery.cells.containing(.staticText, identifier:"Category Title: *").children(matching: .textField).element
            let valueTitleCategory =  "A test"
            let uploadImage = tablesQuery.cells.containing(.staticText, identifier:"Upload Image").children(matching: .button).element
            let selectSourcePhoto = app.sheets["Please select photo source"]
            let selectFromLibrery =  selectSourcePhoto.collectionViews.buttons["Choose photo from library"]
            let selectFirstPhoto =   app.collectionViews.element.children(matching: .cell).element(boundBy: 1)
            let createCategory = tablesQuery.buttons["Create Category"]
            
            
            sleep(2)
            
            XCTAssert(categoryTitle.exists)
            categoryTitle.tap()
            categoryTitle.typeText( valueTitleCategory );
            app.buttons["Return"].tap()
            
            XCTAssert(uploadImage.exists)
            uploadImage.tap()
            
            sleep(2)
            XCTAssert(selectFromLibrery.exists)
            selectFromLibrery.tap()
            
            sleep(2)
            XCTAssert( tablesQuery.buttons["Moments"].exists)
            tablesQuery.buttons["Moments"].tap()
            
            sleep(2)
            XCTAssert( selectFirstPhoto.exists)
            selectFirstPhoto.tap()
            
            sleep(2)
            XCTAssert( createCategory.exists)
            createCategory.tap()
            
            sleep(2)
            
            if app.alerts["Error"].exists {
                app.alerts["Error"].collectionViews.buttons["OK"].tap()
            }
            sleep(3)
            
            back.tap()
        }
        
        sleep(5)
        
    }
    
    func test01AddCategory(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let addCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 0)
        let tablesQuery=app.tables
        let categoryTitle = tablesQuery.cells.containing(.staticText, identifier:"Category Title: *").children(matching: .textField).element
        let valueTitleCategory =  "A test"
        let uploadImage = tablesQuery.cells.containing(.staticText, identifier:"Upload Image").children(matching: .button).element
        let selectSourcePhoto = app.sheets["Please select photo source"]
        let selectFromLibrery =  selectSourcePhoto.collectionViews.buttons["Choose photo from library"]
        let selectFirstPhoto =   app.collectionViews.element.children(matching: .cell).element(boundBy: 1)
        let createCategory = tablesQuery.buttons["Create Category"]
        
        login()
        sleep(4)
        
        app.collectionViews.buttons["Policies"].tap()
        
        preAddCategory()
        
        sleep(2)
        XCTAssert(addCategory.exists)
        addCategory.tap()
        
        XCTAssert(categoryTitle.exists)
        categoryTitle.tap()
        categoryTitle.typeText( valueTitleCategory );
        app.buttons["Return"].tap()
        
        XCTAssert(uploadImage.exists)
        uploadImage.tap()
        
        sleep(2)
        XCTAssert(selectFromLibrery.exists)
        selectFromLibrery.tap()
        
        sleep(5)
        XCTAssert( tablesQuery.buttons["Moments"].exists)
        tablesQuery.buttons["Moments"].tap()
        
        sleep(3)
        XCTAssert( selectFirstPhoto.exists)
        selectFirstPhoto.tap()
        
        sleep(2)
        XCTAssert( createCategory.exists)
        createCategory.tap()
        
        sleep(2)
        XCTAssertFalse( app.alerts["Error"].exists)
        
        sleep(3)
        
        checkCreatedCategory(valueTitleCategory)
    }
    
    
    func test02EditCategory(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 1)
        let tablesQuery=app.tables
        let categoryTitle = tablesQuery.cells.containing(.staticText, identifier:"Category Title: *")
        let valueTitleCategoryEdit =  "A test two"
        let uploadImage = tablesQuery.cells.containing(.staticText, identifier:"Replace Image").children(matching: .button).element
        let deleteKey = app.keys["delete"]
        let categoryItem = tablesQuery.cells.containing(.staticText, identifier:"A test")
        let editCategory = categoryItem.buttons["Compose Icon"]
        let selectSourcePhoto = app.sheets["Please select photo source"]
        let selectFromLibrery =  selectSourcePhoto.collectionViews.buttons["Choose photo from library"]
        let selectSecondPhoto =   app.collectionViews.element.children(matching: .cell).element(boundBy: 2)
        let saveCategory =  tablesQuery.buttons["Save Changes"]
        let selectTitle = categoryTitle.children(matching: .textField).element
        
        login()
        
        sleep(4)
        app.collectionViews.buttons["Policies"].tap()
        
        sleep(3)
        preEditCategory()
        
        sleep(4)
        XCTAssert(viewCategory.exists)
        viewCategory.tap()
        
        sleep(8)
        
        XCTAssert(categoryItem.element.exists)
        XCTAssert(editCategory.exists)
        editCategory.tap()
        
        sleep(2)
        selectTitle.tap()
        
        let countTitle = (selectTitle.value as! String).characters.count
        for _ in 0 ..< countTitle {
            deleteKey.tap()
        }
        
        
        selectTitle.typeText( valueTitleCategoryEdit )
        app.buttons["Return"].tap()
        
        sleep(2)
        XCTAssert(uploadImage.exists)
        uploadImage.tap()
        
        sleep(2)
        XCTAssert(selectFromLibrery.exists)
        selectFromLibrery.tap()
        
        sleep(2)
        XCTAssert( tablesQuery.buttons["Moments"].exists)
        tablesQuery.buttons["Moments"].tap()
        
        sleep(2)
        XCTAssert( selectSecondPhoto.exists)
        selectSecondPhoto.tap()
        
        
        sleep(2)
        XCTAssert( saveCategory.exists)
        saveCategory.tap()
        
        XCTAssertFalse( app.alerts["Error"].exists)
        sleep(4)
        
        
    }
    
    func preDelCategory(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 1)
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: "A test")
        
        let categoryItemCheckTwo = tablesQuery.cells.containing(.staticText, identifier: "A test two")
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(6)
        
        viewCategory.tap()
        sleep(4)
        if categoryItemCheck.element.exists {
            back.tap()
            sleep(3)
        } else if categoryItemCheckTwo.element.exists {
            back.tap()
            sleep(3)
        }
        else {
            tablesQuery.children(matching: .cell).element(boundBy: 1).buttons["Add a New Policy Category"].tap()
            
            let tablesQuery=app.tables
            let categoryTitle = tablesQuery.cells.containing(.staticText, identifier:"Category Title: *").children(matching: .textField).element
            let valueTitleCategory =  "A test two"
            let uploadImage = tablesQuery.cells.containing(.staticText, identifier:"Upload Image").children(matching: .button).element
            let selectSourcePhoto = app.sheets["Please select photo source"]
            let selectFromLibrery =  selectSourcePhoto.collectionViews.buttons["Choose photo from library"]
            let selectFirstPhoto =   app.collectionViews.element.children(matching: .cell).element(boundBy: 1)
            let createCategory = tablesQuery.buttons["Create Category"]
            
            
            sleep(15)
            
            XCTAssert(categoryTitle.exists)
            categoryTitle.tap()
            categoryTitle.typeText( valueTitleCategory );
            app.buttons["Return"].tap()
            sleep(4)
            XCTAssert(uploadImage.exists)
            uploadImage.tap()
            
            sleep(5)
            XCTAssert(selectFromLibrery.exists)
            selectFromLibrery.tap()
            
            sleep(5)
            XCTAssert( tablesQuery.buttons["Moments"].exists)
            tablesQuery.buttons["Moments"].tap()
            
            sleep(5)
            XCTAssert( selectFirstPhoto.exists)
            selectFirstPhoto.tap()
            
            sleep(5)
            XCTAssert( createCategory.exists)
            createCategory.tap()
            
            sleep(5)
            
            if app.alerts["Error"].exists {
                app.alerts["Error"].collectionViews.buttons["OK"].tap()
            }
            sleep(3)
            
            back.tap()
        }
        
        sleep(5)
    }
    
    func test03DelCategory(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 8").element(boundBy: 1)
        
        let tablesQuery=app.tables
        
        let nameCategory = "A test"
        let categoryItem = tablesQuery.cells.containing(.staticText, identifier:nameCategory)
        let delCategory = categoryItem.buttons["Trash Icon"]
        
        let nameCategoryTwo = "A test two"
        let categoryItemTwo = tablesQuery.cells.containing(.staticText, identifier:nameCategoryTwo)
        let delCategoryTwo = categoryItemTwo.buttons["Trash Icon"]
        let warning = app.alerts["Warning!"]
        let delete = warning.collectionViews.buttons["Delete"]
        login()
        
        sleep(3)
        
        app.collectionViews.buttons["Policies"].tap()
        
        sleep(5)
        preDelCategory()
        sleep(5)
        
        viewCategory.tap()
        
        
        sleep(5)
        if categoryItem.element.exists {
            XCTAssert(categoryItem.element.exists)
            
            XCTAssert(delCategory.exists)
            delCategory.tap()
            
            
            XCTAssert(warning.exists)
            
            XCTAssert(delete.exists)
            
            
            delete.tap()
            
            sleep(5)
            
            checkDelCategory(nameCategory)
        } else    if categoryItemTwo.element.exists {
            XCTAssert(categoryItemTwo.element.exists)
            
            XCTAssert(delCategoryTwo.exists)
            delCategoryTwo.tap()
            
            
            XCTAssert(warning.exists)
            
            XCTAssert(delete.exists)
            
            
            delete.tap()
            
            sleep(5)
            
            checkDelCategory(nameCategoryTwo)
        }
    }
    
    func checkCreatedPolicy(_ policyName : String){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewPolicy = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 2)
        let tablesQuery = app.tables
        let selectCategory = tablesQuery.staticTexts["Activities"]
        let policy =  tablesQuery.cells.containing(.staticText, identifier : policyName)
        let existingPoliciesStaticText = tablesQuery.staticTexts["Existing Policies"]
        
        sleep(3)
        
        viewPolicy.tap()
        
        sleep(2)
        selectCategory.tap()
        
        sleep(4)
        existingPoliciesStaticText.tap()
        sleep(1)
        existingPoliciesStaticText.swipeUp()
        
        
        sleep(2)
        XCTAssert( policy.element.exists)
        
        sleep(5)
        
    }
    func checkSavedPolicy(_ policyName : String){
        
        
        let tablesQuery = app.tables
        let selectCategory = tablesQuery.staticTexts["Activities"]
        let policy =  tablesQuery.cells.containing(.staticText, identifier : policyName)
        let existingPoliciesStaticText = tablesQuery.staticTexts["Existing Policies"]
        
        
        sleep(4)
        selectCategory.tap()
        
        sleep(4)
        existingPoliciesStaticText.tap()
        sleep(1)
        existingPoliciesStaticText.swipeUp()
        
        sleep(3)
        XCTAssert( policy.element.exists)
        
        sleep(5)
        
    }
    
    func checkDelPolicy(_ policyName : String){
        
        let tablesQuery = app.tables
        let policy =  tablesQuery.cells.containing(.staticText, identifier : policyName)
        let existingPoliciesStaticText = tablesQuery.staticTexts["Existing Policies"]
        
        
        sleep(4)
        existingPoliciesStaticText.tap()
        sleep(1)
        existingPoliciesStaticText.swipeUp()
        
        
        sleep(5)
        XCTAssertFalse( policy.element.exists)
        
        sleep(6)
    }
    
    
    
    func preAddPolicy(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewPolicy = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 2)
        let tablesQuery=app.tables
        
        let selectCategory = tablesQuery.staticTexts["Activities"]
        
        let valueTitlePolicy = "A test"
        
        let policy =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePolicy)
        //  let permissionEdit = permission.buttons["Compose Icon"]
        let valueTitlePolicyTwo = "A test two"
        
        let policyTwo =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePolicyTwo)
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        
        sleep(12)
        XCTAssert(viewPolicy.exists)
        viewPolicy.tap()
        
        sleep(7)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        if policy.element.exists {
            
            let deleteCategory = policy.buttons["Trash Icon"]
            let warning = app.alerts["Warning!"]
            let delete = warning.collectionViews.buttons["Delete"]
            
            sleep(15)
            deleteCategory.tap()
            
            sleep(10)
            
            delete.tap()
            
            sleep(5)
            
            back.tap()
            
            sleep(3)
        }
        else {
            back.tap()
            sleep(2)
        }
        
        sleep(5)
        XCTAssert(viewPolicy.exists)
        viewPolicy.tap()
        
        sleep(4)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        
        if policyTwo.element.exists{
            
            let deleteCategoryTwo = policyTwo.buttons["Trash Icon"]
            let warning = app.alerts["Warning!"]
            let delete = warning.collectionViews.buttons["Delete"]
            
            deleteCategoryTwo.tap()
            
            sleep(4)
            
            delete.tap()
            
            sleep(2)
            
            back.tap()
            
            sleep(3)
        }else {
            back.tap()
            sleep(2)
        }
    }
    
    
    
    func test04AddPolicy(){
        
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let addPolicy = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 1)
        
        let tablesQuery=app.tables
        let policyTitle = tablesQuery.cells.containing(.staticText, identifier:"Policy Title:*").children(matching: .textField).element(boundBy: 0)
        let policyDesc = tablesQuery.cells.containing(.staticText, identifier:"Policy Title:*").children(matching: .textView).element
        let selectTitle = tablesQuery.staticTexts["Select Policy Category:*"]
        let selectCategory = tablesQuery.staticTexts["Activities"]
        let selectDate = tablesQuery.textFields["Date"]
        let selectDesc = tablesQuery.staticTexts["Policy Description:*"]
        let createPolicy =   tablesQuery.buttons["Create Policy"]
        let valueTitlePolicy = "A test"
        let datePickersQuery = app.datePickers
        let valueDescPolicy = "A description"
        
        
        login()
        
        sleep(3)
        
        app.collectionViews.buttons["Policies"].tap()
        
        preAddPolicy()
        sleep(8)
        XCTAssert(addPolicy.exists)
        addPolicy.tap()
        
        sleep(8)
        XCTAssert(selectTitle.exists)
        selectTitle.tap()
        
        sleep(8)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        
        sleep(2)
        XCTAssert(policyTitle.exists)
        policyTitle.tap()
        policyTitle.typeText( valueTitlePolicy )
        app.buttons["Return"].tap()
        
        sleep(2)
        XCTAssert(selectDate.exists)
        selectDate.tap()
        XCTAssertTrue(datePickersQuery.element.exists)
        app.toolbars.buttons["Done"].tap()
        
        sleep(2)
        XCTAssert(policyDesc.exists)
        policyDesc.tap()
        policyDesc.typeText( valueDescPolicy )
        
        
        sleep(2)
        XCTAssert(selectDesc.exists)
        selectDesc.tap()
        
        sleep(2)
        XCTAssert(createPolicy.exists)
        createPolicy.tap()
        
        sleep(5)
        XCTAssertFalse( app.alerts["Error"].exists)
        checkCreatedPolicy(valueTitlePolicy)
        
    }
    
    
    
    
    func preEditPolicy() {
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewPolicy = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 2)
        let tablesQuery=app.tables
        
        let selectCategory = tablesQuery.staticTexts["Activities"]
        
        let valueTitlePolicy = "A test"
        let policy =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePolicy)
        let valueTitlePolicyTwo = "A test two"
        let policyTwo =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePolicyTwo)
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        let existingPoliciesStaticText = tablesQuery.staticTexts["Existing Policies"]
        
        let illnessText =  tablesQuery.staticTexts["Illnesses Accidents and Incidents"]
        let illnessText2 =  tablesQuery.otherElements["Illnesses Accidents and Incidents"]
        sleep(10)
        XCTAssert(viewPolicy.exists)
        viewPolicy.tap()
        
        sleep(10)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        
        sleep(4)
        existingPoliciesStaticText.tap()
        sleep(1)
        existingPoliciesStaticText.swipeUp()
        
        if policyTwo.element.exists {
            let deleteCategoryTwo = policyTwo.buttons["Trash Icon"]
            let warning = app.alerts["Warning!"]
            let delete = warning.collectionViews.buttons["Delete"]
            
            deleteCategoryTwo.tap()
            
            sleep(4)
            
            delete.tap()
            
            sleep(12)
        }
        else {
            existingPoliciesStaticText.tap()
            existingPoliciesStaticText.swipeUp()
            sleep(3)
            if policyTwo.element.exists {
                
                let deleteCategoryTwo = policyTwo.buttons["Trash Icon"]
                let warning = app.alerts["Warning!"]
                let delete = warning.collectionViews.buttons["Delete"]
                
                deleteCategoryTwo.tap()
                
                sleep(4)
                
                delete.tap()
                
                sleep(12)
                
                // back.tap()
            }
            if illnessText.exists {
                illnessText.tap()
                illnessText.swipeDown()
            } else if illnessText2.exists {
                // illnessText2.tap()
                // illnessText2.swipeDown()
            }
            
        }
        
        
        if !policy.element.exists {
            
            back.tap()
            let addPolicy = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 1)
            
            let policyTitle = tablesQuery.cells.containing(.staticText, identifier:"Policy Title:*").children(matching: .textField).element(boundBy: 0)
            let policyDesc = tablesQuery.cells.containing(.staticText, identifier:"Policy Title:*").children(matching: .textView).element
            let selectTitle = tablesQuery.staticTexts["Select Policy Category:*"]
            let selectCategory = tablesQuery.staticTexts["Activities"]
            let selectDate = tablesQuery.textFields["Date"]
            let selectDesc = tablesQuery.staticTexts["Policy Description:*"]
            let createPolicy =   tablesQuery.buttons["Create Policy"]
            let valueTitlePolicy = "A test"
            let datePickersQuery = app.datePickers
            let valueDescPolicy = "A description"
            
            sleep(8)
            XCTAssert(addPolicy.exists)
            addPolicy.tap()
            
            sleep(2)
            XCTAssert(selectTitle.exists)
            selectTitle.tap()
            
            sleep(2)
            XCTAssert(selectCategory.exists)
            selectCategory.tap()
            
            sleep(2)
            XCTAssert(policyTitle.exists)
            policyTitle.tap()
            policyTitle.typeText( valueTitlePolicy )
            app.buttons["Return"].tap()
            
            sleep(5)
            XCTAssert(selectDate.exists)
            selectDate.tap()
            XCTAssertTrue(datePickersQuery.element.exists)
            app.toolbars.buttons["Done"].tap()
            
            sleep(5)
            XCTAssert(policyDesc.exists)
            policyDesc.tap()
            policyDesc.typeText( valueDescPolicy )
            
            
            sleep(5)
            XCTAssert(selectDesc.exists)
            selectDesc.tap()
            
            sleep(5)
            XCTAssert(createPolicy.exists)
            createPolicy.tap()
            
            back.tap()
            
            sleep(5)
        } else {
            back.tap()
            sleep(5)
        }
    }
    
    
    
    func test05EditPolicy(){
        
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewPolicy = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 2)
        let tablesQuery=app.tables
        let policyTitle = tablesQuery.cells.containing(.staticText, identifier:"Policy Title:*").children(matching: .textField).element(boundBy: 0)
        let policyDate = tablesQuery.cells.containing(.staticText, identifier:"Policy Title:*").children(matching: .textField).element(boundBy: 1)
        let policyDesc = tablesQuery.cells.containing(.staticText, identifier:"Policy Title:*").children(matching: .textView).element
        
        
        let selectCategory = tablesQuery.staticTexts["Activities"]
        let savePolicy =   tablesQuery.buttons["Save Changes"]
        
        let valueTitlePolicy = "A test"
        let valueTitlePolicyEdit = "A test two"
        let datePickersQuery = app.datePickers
        let valueDescPolicy = "A description two"
        let deleteKey = app.keys["delete"]
        let policy =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePolicy)
        let policyEdit = policy.buttons["Compose Icon"]
        
        let existingPoliciesStaticText = tablesQuery.staticTexts["Existing Policies"]
        
        
        login()
        
        sleep(3)
        app.collectionViews.buttons["Policies"].tap()
        
        preEditPolicy()
        
        sleep(2)
        XCTAssert(viewPolicy.exists)
        viewPolicy.tap()
        
        sleep(8)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        
        sleep(4)
        existingPoliciesStaticText.tap()
        sleep(1)
        existingPoliciesStaticText.swipeUp()
        
        
        sleep(2)
        XCTAssert( policy.element.exists)
        XCTAssert( policyEdit.exists)
        policyEdit.tap()
        
        sleep(7)
        XCTAssert(policyTitle.exists)
        policyTitle.tap()
        
        let countTitle = (policyTitle.value as! String).characters.count
        for _ in 0 ..< countTitle {
            deleteKey.tap()
        }
        policyTitle.typeText( valueTitlePolicyEdit )
        app.buttons["Return"].tap()
        
        sleep(2)
        XCTAssert(policyDate.exists)
        policyDate.tap()
        XCTAssertTrue(datePickersQuery.element.exists)
        
        app.toolbars.buttons["Done"].tap()
        
        sleep(2)
        XCTAssert(policyDesc.exists)
        policyDesc.tap()
        let countDesc = (policyDesc.value as! String).characters.count
        for _ in 0 ..< countDesc {
            deleteKey.tap()
        }
        
        policyDesc.typeText(valueDescPolicy)
        
        app.otherElements.containing(.navigationBar, identifier:"Baby's Day").children(matching: .other).element.children(matching: .other).element.children(matching: .table).element.tap()
        
        sleep(2)
        XCTAssert(savePolicy.exists)
        savePolicy.tap()
        
        sleep(2)
        
        XCTAssertFalse( app.alerts["Error"].exists)
        sleep(5)
        
        checkSavedPolicy(valueTitlePolicyEdit)
        
    }
    
    func preDelPolicy(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewPolicy = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 2)
        let tablesQuery=app.tables
        
        let selectCategory = tablesQuery.staticTexts["Activities"]
        
        let valueTitlePolicy = "A test two"
        
        let policy =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePolicy)
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        let existingPoliciesStaticText = tablesQuery.staticTexts["Existing Policies"]
        
        
        sleep(2)
        XCTAssert(viewPolicy.exists)
        viewPolicy.tap()
        
        sleep(5)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        
        
        sleep(4)
        existingPoliciesStaticText.tap()
        sleep(1)
        existingPoliciesStaticText.swipeUp()
        
        
        if !policy.element.exists {
            
            back.tap()
            let addPolicy = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 1)
            
            let policyTitle = tablesQuery.cells.containing(.staticText, identifier:"Policy Title:*").children(matching: .textField).element(boundBy: 0)
            let policyDesc = tablesQuery.cells.containing(.staticText, identifier:"Policy Title:*").children(matching: .textView).element
            let selectTitle = tablesQuery.staticTexts["Select Policy Category:*"]
            let selectCategory = tablesQuery.staticTexts["Activities"]
            let selectDate = tablesQuery.textFields["Date"]
            let selectDesc = tablesQuery.staticTexts["Policy Description:*"]
            let createPolicy =   tablesQuery.buttons["Create Policy"]
            let valueTitlePolicy = "A test two"
            let datePickersQuery = app.datePickers
            let valueDescPolicy = "A description"
            
            XCTAssert(addPolicy.exists)
            addPolicy.tap()
            
            sleep(2)
            XCTAssert(selectTitle.exists)
            selectTitle.tap()
            
            sleep(2)
            XCTAssert(selectCategory.exists)
            selectCategory.tap()
            
            sleep(2)
            XCTAssert(policyTitle.exists)
            policyTitle.tap()
            policyTitle.typeText( valueTitlePolicy )
            app.buttons["Return"].tap()
            
            sleep(2)
            XCTAssert(selectDate.exists)
            selectDate.tap()
            XCTAssertTrue(datePickersQuery.element.exists)
            app.toolbars.buttons["Done"].tap()
            
            sleep(2)
            XCTAssert(policyDesc.exists)
            policyDesc.tap()
            policyDesc.typeText( valueDescPolicy )
            
            
            sleep(2)
            XCTAssert(selectDesc.exists)
            selectDesc.tap()
            
            sleep(2)
            XCTAssert(createPolicy.exists)
            createPolicy.tap()
            
            back.tap()
            
            sleep(3)
        } else {
            back.tap()
            sleep(2)
        }
        
    }
    func test06DelPolicy(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewPolicy = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 2)
        let tablesQuery=app.tables
        let selectCategory = tablesQuery.staticTexts["Activities"]
        let valueTitlePolicy = "A test"
        let policy =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePolicy)
        let deleteCategory = policy.buttons["Trash Icon"]
        let valueTitlePolicyTwo = "A test two"
        let policyTwo =  tablesQuery.cells.containing(.staticText, identifier:valueTitlePolicyTwo)
        let deleteCategoryTwo = policyTwo.buttons["Trash Icon"]
        
        let warning = app.alerts["Warning!"]
        let delete = warning.collectionViews.buttons["Delete"]
        let existingPoliciesStaticText = tablesQuery.staticTexts["Existing Policies"]
        
        login()
        sleep(3)
        
        app.collectionViews.buttons["Policies"].tap()
        
        preDelPolicy()
        
        sleep(3)
        XCTAssert(viewPolicy.exists)
        viewPolicy.tap()
        
        sleep(5)
        XCTAssert(selectCategory.exists)
        selectCategory.tap()
        
        sleep(4)
        existingPoliciesStaticText.tap()
        sleep(1)
        existingPoliciesStaticText.swipeUp()
        
        
        //  sleep(4)
        //  existingPoliciesStaticText.tap()
        sleep(1)
        // existingPoliciesStaticText.swipeUp()
        
        
        if policyTwo.element.exists{
            sleep(7)
            XCTAssert(policyTwo.element.exists)
            XCTAssert(deleteCategoryTwo.exists)
            deleteCategoryTwo.tap()
            
            sleep(7)
            XCTAssert(warning.exists)
            XCTAssert(delete.exists)
            delete.tap()
            
            sleep(7)
            checkDelPolicy(valueTitlePolicyTwo)
        } else
            if policy.element.exists {
                
                sleep(7)
                XCTAssert(policy.element.exists)
                XCTAssert(deleteCategory.exists)
                deleteCategory.tap()
                
                sleep(7)
                XCTAssert(warning.exists)
                XCTAssert(delete.exists)
                delete.tap()
                
                sleep(7)
                checkDelPolicy(valueTitlePolicy)
        }
        
        
        
        
    }
    
    func checkCreatedGroup(_ group : String){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let viewCategory = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 1)
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: group)
        
        let existingPermissionGroupsStaticText = tablesQuery.staticTexts["Existing Policy Groups"]
        
        sleep(2)
        viewCategory.tap()
        
        existingPermissionGroupsStaticText.tap()
        existingPermissionGroupsStaticText.swipeUp()
        
        sleep(2)
        XCTAssert(categoryItemCheck.element.exists)
        sleep(5)
        
    }
    func checkSavedGroup(_ group : String){
        
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: group)
        let existingPermissionGroupsStaticText = tablesQuery.staticTexts["Existing Policy Groups"]
        
        sleep(6)
        existingPermissionGroupsStaticText.tap()
        existingPermissionGroupsStaticText.swipeUp()
        
        sleep(3)
        
        XCTAssert(categoryItemCheck.element.exists)
        
        sleep(5)
    }
    
    func checkDelGroup(_ group : String){
        
        let tablesQuery=app.tables
        let categoryItemCheck = tablesQuery.cells.containing(.staticText, identifier: group)
        let existingPolicyGroupsStaticText = tablesQuery.staticTexts["Existing Policy Groups"]
        
        sleep(2)
        existingPolicyGroupsStaticText.tap()
        existingPolicyGroupsStaticText.swipeUp()
        sleep(3)
        
        XCTAssertFalse(categoryItemCheck.element.exists)
        
        sleep(5)
    }
    
    func preAddGroup(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 1)
        let tablesQuery = app.tables
        let valueTitleGroup="A test"
        
        let permissionGroup =  tablesQuery.cells.containing(.staticText, identifier:valueTitleGroup)
        let permissionGroupDel = permissionGroup.buttons["Trash Icon"]
        
        let valueTitleGroupTwo="A test two"
        
        let permissionGroupTwo =  tablesQuery.cells.containing(.staticText, identifier:valueTitleGroupTwo)
        let permissionGroupTwoDel = permissionGroupTwo.buttons["Trash Icon"]
        
        let warning = app.alerts["Warning!"]
        let delete = warning.collectionViews.buttons["Delete"]
        
        let existingPermissionGroupsStaticText = tablesQuery.staticTexts["Existing Policy Groups"]
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(4)
        XCTAssert( viewGroup.exists)
        viewGroup.tap()
        
        sleep(2)
        
        existingPermissionGroupsStaticText.tap()
        existingPermissionGroupsStaticText.swipeUp()
        sleep(3)
        
        
        if permissionGroup.element.exists {
            sleep(6)
            permissionGroupDel.tap()
            
            sleep(5)
            
            delete.tap()
            
            sleep(5)
            
            // back.tap()
        }
        else {
            //  back.tap()
            sleep(2)
        }
        sleep(6)
        
        
        
        if permissionGroupTwo.element.exists {
            sleep(6)
            permissionGroupTwoDel.tap()
            
            sleep(5)
            
            delete.tap()
            
            sleep(5)
            
            // back.tap()
        }
        else {
            
            //  back.tap()
            sleep(2)
        }
        back.tap()
        
    }
    
    func test07AddGroup(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let addGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 0)
        let tablesQuery=app.tables
        
        let titleGroup = tablesQuery.cells.containing(.staticText, identifier:"Title: *").children(matching: .textField).element
        
        
        let valueTitleGroup="A test"
        let button = tablesQuery.children(matching: .cell).element(boundBy: 8).children(matching: .button).element
        let createPermissionGroup = tablesQuery.children(matching: .cell).element(boundBy: 1).buttons["Create Policy Group"]
        login()
        
        sleep(10)
        
        
        app.collectionViews.buttons["Policies"].tap()
        sleep(5)
        preAddGroup()
        
        sleep(10)
        XCTAssert(addGroup.exists)
        addGroup.tap()
        
        sleep(8)
        XCTAssert(titleGroup.exists)
        titleGroup.tap()
        titleGroup.typeText(valueTitleGroup)
        app.buttons["Return"].tap()
        
        sleep(8)
        XCTAssert(button.exists)
        button.tap()
        
        sleep(8)
        XCTAssert(createPermissionGroup.exists)
        createPermissionGroup.tap()
        
        sleep(10)
        
        checkCreatedGroup(valueTitleGroup)
    }
    
    
    func preEditGroup(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 1)
        let tablesQuery = app.tables
        let valueTitleGroup="A test"
        
        let policyGroup =  tablesQuery.cells.containing(.staticText, identifier:valueTitleGroup)
        
        let policyGroupEdit = tablesQuery.cells.containing(.staticText, identifier:"A test two")
        
        let addGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 0)
        
        let titleGroup = tablesQuery.cells.containing(.staticText, identifier:"Title: *").children(matching: .textField).element
        
        let button = tablesQuery.children(matching: .cell).element(boundBy: 8).children(matching: .button).element
        let createPolicyGroup = tablesQuery.children(matching: .cell).element(boundBy: 1).buttons["Create Policy Group"]
        
        let existingPolicyGroupsStaticText = tablesQuery.staticTexts["Existing Policy Groups"]
        let back =   app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(6)
        XCTAssert( viewGroup.exists)
        viewGroup.tap()
        
        sleep(2)
        existingPolicyGroupsStaticText.tap()
        existingPolicyGroupsStaticText.swipeUp()
        sleep(6)
        
        if policyGroupEdit.element.exists {
            
            let policyGroupEditDel = policyGroupEdit.buttons["Trash Icon"]
            let warning = app.alerts["Warning!"]
            let delete = warning.collectionViews.buttons["Delete"]
            
            policyGroupEditDel.tap()
            
            sleep(10)
            
            delete.tap()
            
            
            
            
            
            sleep(7)
            
            // back.tap()
            
            
        }
        
        if !policyGroup.element.exists {
            
            sleep(4)
            back.tap()
            addGroup.tap()
            
            sleep(6)
            XCTAssert(titleGroup.exists)
            titleGroup.tap()
            titleGroup.typeText(valueTitleGroup)
            app.buttons["Return"].tap()
            
            sleep(2)
            XCTAssert(button.exists)
            button.tap()
            
            sleep(2)
            XCTAssert(createPolicyGroup.exists)
            createPolicyGroup.tap()
            
            sleep(7)
            
            // back.tap()
            
        }else {
            
            sleep(5)
            back.tap()
        }
        
        sleep(4)
        //   back.tap()
        
        sleep(3)
    }
    
    
    func test08EditGroup(){
        
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 1)
        let tablesQuery=app.tables
        
        let titleGroup = tablesQuery.cells.containing(.staticText, identifier:"Title: *").children(matching: .textField).element
        
        let selectAllPolicyThisCat = tablesQuery.children(matching: .cell).element(boundBy: 8).children(matching: .button).element
        
        let savePolicyGroup = tablesQuery.children(matching: .cell).element(boundBy: 1).buttons["Save Policy Group"]
        let valueTitleGroup="A test"
        let valueTitleGroupEdit = "A test two"
        let policyGroup =  tablesQuery.cells.containing(.staticText, identifier:valueTitleGroup)
        let policyGroupEdit = policyGroup.buttons["Compose Icon"]
        
        let subGroup =  tablesQuery.staticTexts["Illnesses Accidents and Incidents"]
        
        let deleteKey = app.keys["delete"]
        
        let existingPolicyGroupsStaticText = tablesQuery.staticTexts["Existing Policy Groups"]
        login()
        
        sleep(3)
        
        
        app.collectionViews.buttons["Policies"].tap()
        
        sleep(3)
        preEditGroup()
        sleep(8)
        XCTAssert( viewGroup.exists)
        viewGroup.tap()
        
        sleep(8)
        XCTAssert( policyGroup.element.exists)
        
        existingPolicyGroupsStaticText.tap()
        existingPolicyGroupsStaticText.swipeUp()
        sleep(3)
        
        
        XCTAssert( policyGroupEdit.exists)
        policyGroupEdit.tap()
        
        
        sleep(4)
        XCTAssert( titleGroup.exists)
        titleGroup.tap()
        
        let countTitle = (titleGroup.value as! String).characters.count
        for _ in 0 ..< countTitle {
            deleteKey.tap()
        }
        titleGroup.typeText( valueTitleGroupEdit )
        app.buttons["Return"].tap()
        
        sleep(2)
        XCTAssert(selectAllPolicyThisCat.exists)
        selectAllPolicyThisCat.tap()
        
        
        sleep(2)
        XCTAssert(subGroup.exists)
        subGroup.tap()
        
        XCTAssert(selectAllPolicyThisCat.exists)
        selectAllPolicyThisCat.tap()
        
        
        sleep(2)
        XCTAssert(savePolicyGroup.exists)
        savePolicyGroup.tap()
        
        checkSavedGroup(valueTitleGroupEdit)
        
    }
    
    func preDelGroup(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 1)
        let tablesQuery = app.tables
        let valueTitleGroup="A test two"
        
        let policyGroup =  tablesQuery.cells.containing(.staticText, identifier:valueTitleGroup)
        
        
        let addGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 0)
        
        let titleGroup = tablesQuery.cells.containing(.staticText, identifier:"Title: *").children(matching: .textField).element
        
        let button = tablesQuery.children(matching: .cell).element(boundBy: 8).children(matching: .button).element
        let createPolicyGroup = tablesQuery.children(matching: .cell).element(boundBy: 1).buttons["Create Policy Group"]
        
        let existingPolicyGroupsStaticText = tablesQuery.staticTexts["Existing Policy Groups"]
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(8)
        XCTAssert( viewGroup.exists)
        viewGroup.tap()
        
        sleep(8)
        
        existingPolicyGroupsStaticText.tap()
        existingPolicyGroupsStaticText.swipeUp()
        sleep(8)
        
        
        if !policyGroup.element.exists {
            
            back.tap()
            addGroup.tap()
            
            sleep(6)
            XCTAssert(titleGroup.exists)
            titleGroup.tap()
            titleGroup.typeText(valueTitleGroup)
            app.buttons["Return"].tap()
            
            sleep(2)
            XCTAssert(button.exists)
            button.tap()
            
            sleep(2)
            XCTAssert(createPolicyGroup.exists)
            createPolicyGroup.tap()
            
            sleep(9)
            //  back.tap()
            
            
        } else {
            back.tap()
            sleep(2)
        }
        
    }
    
    
    func test09DelGroup(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let viewGroup = element.children(matching: .button).matching(identifier: "permissions button 9").element(boundBy: 1)
        let tablesQuery = app.tables
        let valueTitleGroup="A test"
        
        let policyGroup =  tablesQuery.cells.containing(.staticText, identifier:valueTitleGroup)
        let policyGroupDel = policyGroup.buttons["Trash Icon"]
        
        let valueTitleGroupTwo="A test two"
        
        let policyGroupTwo =  tablesQuery.cells.containing(.staticText, identifier:valueTitleGroupTwo)
        let policyGroupTwoDel = policyGroupTwo.buttons["Trash Icon"]
        
        let warning = app.alerts["Warning!"]
        let delete = warning.collectionViews.buttons["Delete"]
        
        let existingPolicyGroupsStaticText = tablesQuery.staticTexts["Existing Policy Groups"]
        login()
        
        sleep(3)
        
        app.collectionViews.buttons["Policies"].tap()
        
        sleep(4)
        preDelGroup()
        sleep(4)
        XCTAssert( viewGroup.exists)
        viewGroup.tap()
        
        sleep(3)
        existingPolicyGroupsStaticText.tap()
        existingPolicyGroupsStaticText.swipeUp()
        
        
        sleep(5)
        if policyGroup.element.exists {
            XCTAssert( policyGroup.element.exists)
            XCTAssert( policyGroupDel.exists)
            policyGroupDel.tap()
            
            
            sleep(2)
            XCTAssert(warning.exists)
            XCTAssert(delete.exists)
            delete.tap()
            
            sleep(5)
            
            checkDelGroup(valueTitleGroup)
        } else if policyGroupTwo.element.exists {
            
            XCTAssert( policyGroupTwo.element.exists)
            XCTAssert( policyGroupTwoDel.exists)
            policyGroupTwoDel.tap()
            
            
            sleep(2)
            XCTAssert(warning.exists)
            XCTAssert(delete.exists)
            delete.tap()
            
            sleep(5)
            
            checkDelGroup(valueTitleGroupTwo)
        }
    }
    
    func checkCreatedAssign(){
        let tablesQuery = app.tables
        
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        let assignPoliciesStaticText =   tablesQuery.staticTexts["Assign Policies"]
        
        let viewAssing = selectChild.children(matching: .button).element(boundBy: 1)
        
        sleep(2)
        
        assignPoliciesStaticText.tap()
        assignPoliciesStaticText.swipeUp()
        
        
        sleep(2)
        viewAssing.tap()
        
        
    }
    
    func preAssignCreate(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPoliciesStaticText =   tablesQuery.staticTexts["Assign Policies"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let existItem = selectChild.children(matching: .button).element(boundBy: 2)
        let viewAssing = selectChild.buttons["expandButton"]
        
        let itemDel = tablesQuery.buttons["deleteButton"]
        
        let msgWarning = app.alerts["Warning!"]
        let warningDelete = msgWarning.collectionViews.buttons["Delete"]
        let back = app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(6)
        
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        
        if existItem.exists {
            
            sleep(6)
            XCTAssertTrue(viewAssing.exists)
            viewAssing.tap()
            
            sleep(6)
            XCTAssertTrue(assignPoliciesStaticText.exists)
            assignPoliciesStaticText.tap()
            assignPoliciesStaticText.swipeUp()
            
            
            sleep(6)
            XCTAssertTrue(itemDel.exists)
            itemDel.tap()
            
            sleep(6)
            XCTAssertTrue(msgWarning.exists)
            XCTAssertTrue(warningDelete.exists)
            warningDelete.tap()
            
            sleep(6)
            back.tap()
            
        } else {
            back.tap()
        }
        
    }
    
    func test10AssingCreate(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let managePoliciesStaticText =   tablesQuery.staticTexts["Manage Policies"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let newAssing = selectChild.buttons["addButton"]
        
        let selectCategory = tablesQuery.children(matching: .cell).element(boundBy: 10).children(matching: .button).element
        
        let assingSelected = tablesQuery.children(matching: .cell).element(boundBy: 5).buttons["Assign Selected"]
        
        
        let elem =  tablesQuery.children(matching: .cell).element(boundBy: 12)
        login()
        
        sleep(3)
        
        app.collectionViews.buttons["Policies"].tap()
        
        sleep(3)
        preAssignCreate()
        sleep(10)
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        sleep(10)
        XCTAssert( newAssing.exists)
        newAssing.tap()
        
        sleep(10)
        XCTAssert( managePoliciesStaticText.exists)
        managePoliciesStaticText.tap()
        managePoliciesStaticText.swipeUp()
        
        sleep(10)
        XCTAssert( selectCategory.exists)
        selectCategory.tap()
        
        sleep(8)
        XCTAssert(elem.exists)
        elem.tap()
        elem.swipeDown()
        
        
        sleep(8)
        assingSelected.tap()
        
        sleep(12)
        checkCreatedAssign()
    }
    
    
    func preAssignEdit(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Policies"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let existItem = selectChild.children(matching: .button).element(boundBy: 2)
        
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(4)
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        sleep(4)
        XCTAssertTrue(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        if !existItem.exists {
            
            let newAssing = selectChild.buttons["addButton"]
            
            let managePermissionsStaticText =   tablesQuery.staticTexts["Manage Policies"]
            let assingSelected = tablesQuery.children(matching: .cell).element(boundBy: 5).buttons["Assign Selected"]
            
            let elem =  tablesQuery.children(matching: .cell).element(boundBy: 12)
            let selectCategory = tablesQuery.children(matching: .cell).element(boundBy: 10).children(matching: .button).element
            sleep(2)
            XCTAssert( newAssing.exists)
            newAssing.tap()
            
            sleep(2)
            XCTAssert( managePermissionsStaticText.exists)
            managePermissionsStaticText.tap()
            managePermissionsStaticText.swipeUp()
            
            sleep(2)
            XCTAssert( selectCategory.exists)
            selectCategory.tap()
            
            sleep(2)
            XCTAssert(elem.exists)
            elem.tap()
            elem.swipeDown()
            
            
            sleep(3)
            XCTAssert( assingSelected.exists)
            assingSelected.tap()
            
            sleep(5)
            back.tap()
            
            sleep(3)
            
        } else {
            back.tap()
            sleep(3)
        }
        
    }
    
    
    func test11AssingEdit(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Policies"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        
        let itemEdit = tablesQuery.buttons["editButton"]
        
        let managePermissionsStaticText = tablesQuery.staticTexts["Manage Policies"]
        
        let categorySafeguarding = tablesQuery.otherElements["Safeguarding Children"]
        let selectAllCategory = tablesQuery.cells.containing(.staticText, identifier:"Select All Policies in this Category ").children(matching: .button).element
        let btnAssignSelected = tablesQuery.children(matching: .cell).element(boundBy: 7).buttons["Assign Selected"]
        
        
        login()
        
        sleep(8)
        
        app.collectionViews.buttons["Policies"].tap()
        
        sleep(4)
        preAssignEdit()
        sleep(5)
        XCTAssert(assing.exists)
        assing.tap()
        
        
        
        sleep(8)
        XCTAssert(viewAssing.exists)
        viewAssing.tap()
        
        sleep(6)
        XCTAssert(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        
        sleep(6)
        XCTAssert(itemEdit.exists)
        itemEdit.tap()
        
        sleep(8)
        XCTAssert(managePermissionsStaticText.exists)
        managePermissionsStaticText.tap()
        managePermissionsStaticText.swipeUp()
        
        sleep(2)
        XCTAssert(categorySafeguarding.exists)
        categorySafeguarding.tap()
        selectAllCategory.tap()
        
        
        sleep(3)
        XCTAssert(btnAssignSelected.exists)
        btnAssignSelected.tap()
        
        
        sleep(5)
    }
    
    func ckeckAssignDelete(){
        
        let tablesQuery = app.tables
        
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        let viewAssing = selectChild.children(matching: .button).element(boundBy: 2)
        
        
        sleep(4)
        XCTAssertFalse(viewAssing.exists)
        
        sleep(5)
        
    }
    func preAssignDetele() {
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Policies"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let existItem = selectChild.children(matching: .button).element(boundBy: 2)
        
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        sleep(4)
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        sleep(4)
        XCTAssertTrue(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        if !existItem.exists {
            
            let newAssing = selectChild.buttons["addButton"]
            
            let managePermissionsStaticText =   tablesQuery.staticTexts["Manage Policies"]
            let assingSelected = tablesQuery.children(matching: .cell).element(boundBy: 5).buttons["Assign Selected"]
            
            let elem =  tablesQuery.children(matching: .cell).element(boundBy: 12)
            let selectCategory = tablesQuery.children(matching: .cell).element(boundBy: 10).children(matching: .button).element
            sleep(2)
            XCTAssert( newAssing.exists)
            newAssing.tap()
            
            sleep(2)
            XCTAssert( managePermissionsStaticText.exists)
            managePermissionsStaticText.tap()
            managePermissionsStaticText.swipeUp()
            
            sleep(2)
            XCTAssert( selectCategory.exists)
            selectCategory.tap()
            
            sleep(2)
            XCTAssert(elem.exists)
            elem.tap()
            elem.swipeDown()
            
            
            sleep(3)
            XCTAssert( assingSelected.exists)
            assingSelected.tap()
            
            sleep(5)
            back.tap()
            
            sleep(3)
            
        } else {
            back.tap()
            sleep(3)
        }
    }
    
    func test14AssignDelete(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Policies"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        
        let itemDel = tablesQuery.buttons["deleteButton"]
        
        let msgWarning = app.alerts["Warning!"]
        let warningDelete = msgWarning.collectionViews.buttons["Delete"]
        
        login()
        
        sleep(3)
        
        app.collectionViews.buttons["Policies"].tap()
        
        sleep(2)
        preAssignDetele()
        sleep(5)
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        
        
        sleep(8)
        XCTAssertTrue(viewAssing.exists)
        viewAssing.tap()
        
        sleep(7)
        XCTAssertTrue(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        sleep(5)
        XCTAssertTrue(itemDel.exists)
        itemDel.tap()
        
        sleep(3)
        XCTAssertTrue(msgWarning.exists)
        XCTAssertTrue(warningDelete.exists)
        warningDelete.tap()
        
        sleep(6)
        
        ckeckAssignDelete()
    }
    
    
    func checkAddedSign(){
        
        
        
        let tablesQuery = app.tables
        
        
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Policies"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        
        let itemSign = tablesQuery.buttons["signGreenButton"]
        
        sleep(6)
        XCTAssertTrue(viewAssing.exists)
        viewAssing.tap()
        
        sleep(6)
        XCTAssertTrue(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        
        
        
        sleep(2)
        XCTAssertTrue(itemSign.exists)
        
    }
    
    
    func checkDeletSign(){
        
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Policies"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        
        let itemSign = tablesQuery.buttons["signRedButton"]
        
        
        
        sleep(2)
        XCTAssertTrue(viewAssing.exists)
        viewAssing.tap()
        
        sleep(5)
        XCTAssertTrue(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        
        sleep(2)
        XCTAssertTrue(itemSign.exists)
        
    }
    
    func preAssignDeleteSign(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Policies"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        
        let itemSign = tablesQuery.buttons["signGreenButton"]
        
        let managePermissionsStaticText = tablesQuery.staticTexts["Manage Policies"]
        
        let text = tablesQuery.staticTexts["If you have finished assigning Policies to Android, tap the Assign Selected button above to save their Policies."]
        let saveButton = app.buttons["Save"]
        
        let sign1Field = tablesQuery.textFields["signature1TextField"]
        let textParent1 = "parent one"
        let sign1 = tablesQuery.buttons["sign1Button"]
        
        let image = app.images["signatureImageView"]
        
        let deleteKey = app.keys["delete"]
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        let itemSignRed = tablesQuery.buttons["signRedButton"]
        
        sleep(9)
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        
        
        sleep(7)
        XCTAssertTrue(viewAssing.exists)
        viewAssing.tap()
        
        sleep(5)
        XCTAssertTrue(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        
        sleep(5)
        if itemSign.exists {
            back.tap()
        } else {
            
            sleep(6)
            XCTAssertTrue(itemSignRed.exists)
            itemSignRed.tap()
            
            
            sleep(6)
            XCTAssert(managePermissionsStaticText.exists)
            managePermissionsStaticText.tap()
            sleep(1)
            managePermissionsStaticText.swipeUp()
            
            sleep(6)
            XCTAssert(text.exists)
            text.tap()
            text.swipeUp()
            
            sleep(4)
            XCTAssertTrue(sign1Field.exists)
            sign1Field.tap()
            
            let countTitle1 = (sign1Field.value as! String).characters.count
            for _ in 0 ..< countTitle1 {
                deleteKey.tap()
            }
            sleep(2)
            sign1Field.typeText(textParent1)
            app.buttons["Return"].tap()
            
            
            sleep(5)
            XCTAssertTrue(sign1.exists)
            sign1.tap()
            
            sleep(2)
            
            image.tap()
            image.swipeLeft()
            image.tap()
            
            sleep(1)
            sleep(3)
            XCTAssertTrue(saveButton.exists)
            saveButton.tap()
            
            
            sleep(10)
            
            back.tap()
            sleep(6)
            
            back.tap()
            sleep(4)
            
        }
        
        sleep(3)
        
    }
    
    
    
    
    func test13AssignDeleteSign(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Policies"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        
        
        let itemSign = tablesQuery.buttons["signGreenButton"]
        
        let managePermissionsStaticText = tablesQuery.staticTexts["View Android's Policies"]
        
        
        let sign1Field = tablesQuery.textFields["signature1TextField"]
        let sign1 = tablesQuery.buttons["sign1Button"]
        let deleteKey = app.keys["delete"]
        
        let deleteButton = app.buttons["Delete"]
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        login()
        
        sleep(3)
        
        app.collectionViews.buttons["Policies"].tap()
        
        sleep(3)
        preAssignDeleteSign()
        sleep(6)
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        
        
        sleep(2)
        XCTAssertTrue(viewAssing.exists)
        viewAssing.tap()
        
        sleep(2)
        XCTAssertTrue(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        sleep(4)
        XCTAssertTrue(itemSign.exists)
        itemSign.tap()
        
        sleep(3)
        XCTAssertTrue(managePermissionsStaticText.exists)
        managePermissionsStaticText.tap()
        managePermissionsStaticText.swipeUp()
        
        
        
        
        sleep(6)
        XCTAssertTrue(sign1.exists)
        sign1.tap()
        sleep(3)
        XCTAssertTrue(deleteButton.exists)
        deleteButton.tap()
        
        sleep(6)
        /*  XCTAssertTrue(sign1Field.exists)
         sign1Field.tap()
         
         let countTitle1 = (sign1Field.value as! String).characters.count
         for _ in 0 ..< countTitle1 {
         deleteKey.tap()
         }
         
         
         app.buttons["Return"].tap()
         */
        
        
        
        sleep(7)
        back.tap()
        
        sleep(4)
        checkDeletSign()
    }
    
    
    
    func preAssignAddSign(){
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let assing = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 0)
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Policies"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.children(matching: .button).element(boundBy: 1)
        
        let itemSign = tablesQuery.buttons["signRedButton"]
        
        let managePoliciesStaticText = tablesQuery.staticTexts["View Android's Policies"]
        let sign1 = tablesQuery.buttons["sign1Button"]
        
        let deleteButton = app.buttons["Delete"]
        let itemSignGreen = tablesQuery.buttons["signGreenButton"]
        let deleteKey = app.keys["delete"]
        let sign1Field = tablesQuery.textFields["signature1TextField"]
        
        
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        
        let existItem = selectChild.children(matching: .button).element(boundBy: 2)
        
        sleep(3)
        XCTAssertTrue(assing.exists)
        assing.tap()
        
        
        if !existItem.exists {
            
            let newAssing = selectChild.buttons["addButton"]
            
            let managePermissionsStaticText =   tablesQuery.staticTexts["Manage Policies"]
            let assingSelected = tablesQuery.children(matching: .cell).element(boundBy: 5).buttons["Assign Selected"]
            
            let elem =  tablesQuery.children(matching: .cell).element(boundBy: 12)
            let selectCategory = tablesQuery.children(matching: .cell).element(boundBy: 10).children(matching: .button).element
            sleep(10)
            XCTAssert( newAssing.exists)
            newAssing.tap()
            
            sleep(5)
            XCTAssert( managePermissionsStaticText.exists)
            managePermissionsStaticText.tap()
            managePermissionsStaticText.swipeUp()
            
            sleep(10)
            XCTAssert( selectCategory.exists)
            selectCategory.tap()
            
            sleep(5)
            XCTAssert(elem.exists)
            elem.tap()
            elem.swipeDown()
            
            
            sleep(5)
            XCTAssert( assingSelected.exists)
            assingSelected.tap()
            
            sleep(10)
            back.tap()
            
            sleep(3)
            
        } else {
            
            sleep(4)
            XCTAssertTrue(viewAssing.exists)
            viewAssing.tap()
            
            sleep(4)
            XCTAssertTrue(assignPermissionsStaticText.exists)
            assignPermissionsStaticText.tap()
            assignPermissionsStaticText.swipeUp()
            
            sleep(4)
            if itemSign.exists {
                back.tap()
            } else {
                
                sleep(5)
                XCTAssertTrue(itemSignGreen.exists)
                itemSignGreen.tap()
                
                sleep(4)
                XCTAssert(managePoliciesStaticText.exists)
                managePoliciesStaticText.tap()
                
                managePoliciesStaticText.swipeUp()
                
                sleep(3)
                XCTAssertTrue(sign1.exists)
                sign1.tap()
                sleep(2)
                XCTAssertTrue(deleteButton.exists)
                deleteButton.tap()
                
                sleep(3)
                
                
                //   XCTAssertTrue(sign1Field.exists)
                //      sign1Field.tap()
                //       let countTitle1 = (sign1Field.value as! String).characters.count
                //       for _ in 0 ..< countTitle1 {
                //            deleteKey.tap()
                //        }
                //       app.buttons["Return"].tap()
                
                
                sleep(7)
                
                back.tap()
                
            }
        }
        
        sleep(3)
        
    }
    
    func test12AssignAddSign(){
        
        let element =   app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        
        let assing = element.children(matching: .button).matching(identifier: "Policies").element(boundBy: 0)
        
        let tablesQuery = app.tables
        
        let assignPermissionsStaticText =   tablesQuery.staticTexts["Assign Policies"]
        
        let child = "Android Test"
        
        let selectChild =  tablesQuery.cells.containing(.staticText, identifier: child)
        
        let viewAssing = selectChild.buttons["expandButton"]
        let itemSign = tablesQuery.buttons["signRedButton"]
        let managePermissionsStaticText = tablesQuery.staticTexts["Manage Policies"]
        
        let text = tablesQuery.staticTexts["If you have finished assigning Policies to Android, tap the Assign Selected button above to save their Policies."]
        
        
        
        let saveButton = app.buttons["Save"]
        
        tablesQuery.staticTexts["Assign Policies"]
        let sign1Field = tablesQuery.textFields["signature1TextField"]
        let textParent1 = "parent one"
        let sign1 = tablesQuery.buttons["sign1Button"]
        let deleteKey = app.keys["delete"]
        
        let image = app.images["signatureImageView"]
        
        let back =  app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        login()
        
        sleep(5)
        
        app.collectionViews.buttons["Policies"].tap()
        
        sleep(3)
        preAssignAddSign()
        sleep(6)
        XCTAssert(assing.exists)
        assing.tap()
        
        
        
        sleep(4)
        XCTAssert(viewAssing.exists)
        viewAssing.tap()
        
        sleep(2)
        XCTAssert(assignPermissionsStaticText.exists)
        assignPermissionsStaticText.tap()
        assignPermissionsStaticText.swipeUp()
        
        
        
        sleep(2)
        XCTAssert(itemSign.exists)
        itemSign.tap()
        
        sleep(3)
        XCTAssert(managePermissionsStaticText.exists)
        managePermissionsStaticText.tap()
        sleep(1)
        managePermissionsStaticText.swipeUp()
        
        sleep(4)
        XCTAssert(text.exists)
        text.tap()
        text.swipeUp()
        
        sleep(4)
        XCTAssertTrue(sign1Field.exists)
        sign1Field.tap()
        
        let countTitle1 = (sign1Field.value as! String).characters.count
        for _ in 0 ..< countTitle1 {
            deleteKey.tap()
        }
        
        sign1Field.typeText(textParent1)
        app.buttons["Return"].tap()
        
        
        sleep(4)
        XCTAssertTrue(sign1.exists)
        sign1.tap()
        
        sleep(4)
        
        image.tap()
        image.swipeLeft()
        image.tap()
        
        sleep(3)
        XCTAssertTrue(saveButton.exists)
        saveButton.tap()
        
        
        sleep(5)
        back.tap()
        sleep(3)
        
        sleep(7)
        
        
        checkAddedSign()
        
    }
    
 }
