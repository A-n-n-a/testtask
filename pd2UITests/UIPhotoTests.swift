//
//  UITests.swift
//  pd2
//

import Foundation
import UIKit
import XCTest


class UIPhotoTests: UITestCase{
    
    func testUploadPhoto() {
        let selectPhotoSource = app.sheets["Please select photo source"]
        let tablesQuery = app.tables
        let datePickersQuery = app.datePickers
        let photoTakenCellsQuery = tablesQuery.cells.containing(.staticText, identifier:"Photo Taken:").children(matching: .textField).element(boundBy: 0)
        let uploadPhotographs = tablesQuery.cells.containing(.staticText, identifier:"Upload Photographs").children(matching: .button).element
        let titlePhoto = tablesQuery.cells.containing(.staticText, identifier:"Photo Taken:").children(matching: .textField).element(boundBy: 1)
        let descriptionPhoto = XCUIApplication().tables.cells.containing(.staticText, identifier:"Photo Taken:").children(matching: .textView).element
        
        goGalleryChild("photographs")
        
        //Select a photo from the gallery
        uploadPhotographs.tap()
        XCTAssert(selectPhotoSource.exists)
        selectPhotoSource.collectionViews.buttons["Choose photo from library"].tap()
        if app.collectionViews.images.count > 0 {
            app.collectionViews.images.element(boundBy: 0).tap()
        }
        app.navigationBars["Camera Roll"].buttons["Done"].tap()
        waitFor(app.navigationBars["Baby's Day"].staticTexts["Upload photographs"], seconds: 5)
        
        //Input 'Photo Taken'
        photoTakenCellsQuery.tap()
        XCTAssert(datePickersQuery.element.exists)
        app.pickerWheels.element(boundBy: 0).adjust(toPickerWheelValue: testDataUploadPhoto["month"]!)
        app.pickerWheels.element(boundBy: 1).adjust(toPickerWheelValue: testDataUploadPhoto["day"]!)
        app.pickerWheels.element(boundBy: 2).adjust(toPickerWheelValue: testDataUploadPhoto["year"]!)
        app.toolbars.buttons["Done"].tap()
        waitFor(photoTakenCellsQuery, seconds: 5)
        
        XCTAssertEqual(photoTakenCellsQuery.value as! String, testDataUploadPhoto["date"]!)
        XCTAssertEqual(datePickersQuery.element.exists, false)
        
        //Input 'Title'
        tablesQuery.textFields["Title"].tap()
        XCTAssert(app.keys.element.exists)
        for symbol in valueTitlePhoto {
            app.keys[symbol].tap()
        }
        tablesQuery.staticTexts["Title:"].tap()
        XCTAssertEqual(app.keys.element.exists, false)
        XCTAssertEqual(titlePhoto.value as! String, testDataUploadPhoto["title"]!)
        
        //Input 'Description'
        descriptionPhoto.tap()
        XCTAssert(app.keys.element.exists)
        for symbol in valueDescriptionPhoto {
            app.keys[symbol].tap()
        }
        tablesQuery.staticTexts["Description:"].tap()
        
        XCTAssertEqual(app.keys.element.exists, false)
        XCTAssertEqual(descriptionPhoto.value as! String, testDataUploadPhoto["description"])
        
        //Select area of learning
        app.tables.buttons["Duplicate to Additional Outcomes"].tap()
        waitFor(tablesQuery.staticTexts["Select the Area of Learning:"], seconds: 5)
        
        XCTAssert(tablesQuery.staticTexts["Select the Area of Learning:"].exists)
        
        //Check area learning
        tablesQuery.staticTexts[testDataUploadPhoto["area"]!].tap()
        tablesQuery.staticTexts[testDataUploadPhoto["aspect"]!].tap()
        waitFor(tablesQuery.staticTexts[testDataUploadPhoto["outcome"]!], seconds: 5)
        
        tablesQuery.staticTexts[testDataUploadPhoto["outcome"]!].tap()
        waitFor(tablesQuery.staticTexts[testDataUploadPhoto["area"]!], seconds: 5)
        
        XCTAssert(tablesQuery.staticTexts[testDataUploadPhoto["area"]!].exists)
        XCTAssert(tablesQuery.staticTexts[testDataUploadPhoto["aspect"]!].exists)
        XCTAssert(tablesQuery.staticTexts[testDataUploadPhoto["age_outcome"]!].exists)
        
        tablesQuery.staticTexts["Listening and Attention"].swipeUp()
        tablesQuery.buttons["Upload selected photographs"].tap()
        waitFor(app.tables.cells.containing(.staticText, identifier:"Select Year and Month:").children(matching: .textField).element, seconds: 10)
        
        app.tables.cells.containing(.staticText, identifier:"Select Year and Month:").children(matching: .textField).element.tap()
        
        app.pickerWheels["April, 3 of 3"].tap()
        app.toolbars.buttons["Done"].tap()
        tablesQuery.buttons["Enter Gallery"].tap()
        waitFor(app.navigationBars["Baby's Day"].staticTexts["General Gallery"], seconds: 5)
        XCTAssert(tablesQuery.children(matching: .cell).element(boundBy: 3).children(matching: .button).count > 0)
    }
    
    func testViewPhoto() {
        let tablesQuery = app.tables
        let selectYear = tablesQuery.cells.containing(.staticText, identifier:"Select Year and Month:").children(matching: .textField).element
        let textView = tablesQuery.children(matching: .cell).element(boundBy: 0).children(matching: .textView).element
        
        goGalleryChild("photographs")
        
        selectYear.tap()
        XCTAssert(app.pickerWheels.element.exists)
        app.pickerWheels[testDataViewPhoto["selectYearInput"]!].tap()
        app.toolbars.buttons["Done"].tap()
        XCTAssertEqual(app.pickerWheels.element.exists, false)
        XCTAssertEqual(selectYear.value as! String, testDataViewPhoto["selectYearValue"]!)
        XCTAssert(tablesQuery.buttons["Enter Gallery"].exists)
        
        tablesQuery.buttons["Enter Gallery"].tap()
        XCTAssert(tablesQuery.staticTexts["Photograph Galleries"].exists)
        
        var checkText = "\(childInfo["firstName"]!) \(childInfo["lastName"]!)'s General Gallery - \(testDataViewPhoto["checkSelectYearValue"]!)" 
        var countCheckText = checkText.characters.count
        var endIndexCheckText = checkText.characters.index(checkText.startIndex, offsetBy: countCheckText-1)
        var range = checkText[checkText.startIndex...endIndexCheckText]
        XCTAssertEqual(checkText, range)
        tablesQuery.children(matching: .cell).element(boundBy: 3).children(matching: .button).element(boundBy: 0).tap()
        XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
        XCTAssert(tablesQuery.staticTexts["\(childInfo["firstName"]!)'s current age: \(childInfo["age"]!) months"].exists)
        tablesQuery.cells.containing(.staticText, identifier: "\(childInfo["firstName"]!)'s current age: \(childInfo["age"]!) months" ).children(matching: .button).element(boundBy: 0).tap()
        sleep(2)
        XCTAssert(app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .scrollView).element.exists)
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .scrollView).element.tap()
        
    }
    
    /*
     At first need set parameters fot 'dataCommentPhoto'.
     */
    func testCommentPhoto() {
        let tablesQuery = app.tables
        let selectYear = tablesQuery.cells.containing(.staticText, identifier:"Select Year and Month:").children(matching: .textField).element
        let textView = tablesQuery.children(matching: .cell).element(boundBy: 0).children(matching: .textView).element
        let textComment = tablesQuery.cells.containing(.button, identifier:"Add Comment").children(matching: .textView).element
        let imageButtons = tablesQuery.children(matching: .cell).element(boundBy: 3).children(matching: .button)
        let buttonBack = app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        goGalleryChild("photographs")
        
        waitFor(selectYear, seconds: 5)
        selectYear.tap()
        waitFor(app.pickerWheels.element, seconds: 5)
        XCTAssert(app.pickerWheels.element.exists)
        app.pickerWheels[testDataCommentPhoto["selectYearInput"]!].tap()
        app.toolbars.buttons["Done"].tap()
        XCTAssertEqual(app.pickerWheels.element.exists, false)
        XCTAssertEqual(selectYear.value as! String, testDataCommentPhoto["selectYearValue"]!)
        XCTAssert(tablesQuery.buttons["Enter Gallery"].exists)
        
        tablesQuery.buttons["Enter Gallery"].tap()
        XCTAssert(tablesQuery.staticTexts["Photograph Galleries"].exists)
        
        var checkText = "\(childInfo["firstName"]!) \(childInfo["lastName"]!)'s General Gallery - \(testDataCommentPhoto["checkSelectYearValue"]!)" 
        var checkView = textView.value as! String
        var countCheckText = checkText.characters.count
        var endIndexCheckView = checkView.characters.index(checkView.startIndex, offsetBy: countCheckText-1)
        var range = checkView[checkView.startIndex...endIndexCheckView]
        XCTAssertEqual(checkText, range)
        
        if imageButtons.count > 0 {
            var lastIndex = imageButtons.count - 1
            for index in 0 ... lastIndex {
                imageButtons.element(boundBy: index).tap()
                XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
                if tablesQuery.cells.containing(.staticText, identifier: testDataCommentPhoto["title"]!).element.exists  && tablesQuery.cells.containing(.staticText, identifier: testDataCommentPhoto["date"]!).element.exists {
                    XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
                    XCTAssert(tablesQuery.staticTexts["\(childInfo["firstName"]!)'s current age: \(childInfo["age"]!) months"].exists)
                    
                    //Input 'Comment'
                    app.tables.buttons["•••"].swipeUp()
                    textComment.tap()
                    XCTAssert(app.keys.element.exists)
                    for symbol in comment {
                        app.keys[symbol].tap()
                    }
                    
                    textComment.swipeDown()
                    tablesQuery.staticTexts["Title:"].tap()
                    tablesQuery.staticTexts["Title:"].swipeUp()
                    
                    sleep(2)
                    XCTAssertEqual(app.keys.element.exists, false)
                    
                    var todaysDate = Date()
                    var dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd.MM.yyyy @ HH:mm"
                    dateFormatter.timeZone = TimeZone(secondsFromGMT: 3600)
                    var DateInFormat = dateFormatter.string(from: todaysDate)
                    var endIndexDate=DateInFormat.characters.index(DateInFormat.startIndex, offsetBy: 17)
                    var rangeDate = DateInFormat[DateInFormat.startIndex...endIndexDate]
                    app.tables.buttons["Add Comment"].tap()
                    app.tables.buttons["•••"].swipeUp()
                    XCTAssert(tablesQuery.staticTexts[rangeDate].exists)
                    XCTAssert(tablesQuery.cells.containing(.staticText, identifier:testDataCommentPhoto["comment"]!).staticTexts[infoLogin["user"]!].exists)
                    sleep(2)
                    
                    
                    break
                } else {
                    buttonBack.tap()
                }
            }
        }
        
    }
    
    func testEditPhoto() {
        let tablesQuery = app.tables
        let selectYear = tablesQuery.cells.containing(.staticText, identifier:"Select Year and Month:").children(matching: .textField).element
        let photoTakenCellsQuery = tablesQuery.cells.containing(.staticText, identifier:"Photo Taken:").children(matching: .textField).element(boundBy: 0)
        let datePickersQuery = app.datePickers
        let titlePhoto = tablesQuery.cells.containing(.staticText, identifier:"Photo Taken:").children(matching: .textField).element(boundBy: 1)
        let descriptionPhoto = XCUIApplication().tables.cells.containing(.staticText, identifier:"Photo Taken:").children(matching: .textView).element
        let buttonBack = app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        let selectAolCellsQuery = tablesQuery.cells.containing(.staticText, identifier:"Select AOL:")
        
        goGalleryChild("photographs")
        
        waitFor(selectYear, seconds: 5)
        selectYear.tap()
        XCTAssert(app.pickerWheels.element.exists)
        app.pickerWheels[testDataEditPhoto["selectYearInput"]!].tap()
        app.toolbars.buttons["Done"].tap()
        XCTAssertEqual(app.pickerWheels.element.exists, false)
        XCTAssertEqual(selectYear.value as! String, testDataEditPhoto["selectYearValue"]!)
        XCTAssert(tablesQuery.buttons["Enter Gallery"].exists)
        
        tablesQuery.buttons["Enter Gallery"].tap()
        XCTAssert(tablesQuery.staticTexts["Photograph Galleries"].exists)
        
        var checkText = "\(childInfo["firstName"]!) \(childInfo["lastName"]!)'s General Gallery - \(testDataEditPhoto["checkSelectYearValue"])" 
        var countCheckText = checkText.characters.count
        var endIndexCheckText = checkText.characters.index(checkText.startIndex, offsetBy: countCheckText-1)
        var range = checkText[checkText.startIndex...endIndexCheckText]
        XCTAssertEqual(checkText, range)
        
        tablesQuery.children(matching: .cell).element(boundBy: 3).children(matching: .button).element(boundBy: 0).tap()
        XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
        XCTAssert(tablesQuery.staticTexts["\(childInfo["firstName"]!)'s current age: \(childInfo["age"]!) months"].exists)
        
        
        //Edit Photo
        tablesQuery.buttons["•••"].tap()
        app.sheets.collectionViews.buttons["Edit Photo"].tap()
        XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Edit photo"].exists)
        app.navigationBars["Baby's Day"].staticTexts["Edit photo"].tap()
        
        //Input 'Photo Taken'
        photoTakenCellsQuery.tap()
        XCTAssert(datePickersQuery.element.exists)
        app.pickerWheels.element(boundBy: 0).adjust(toPickerWheelValue: testDataEditPhoto["month"]!)
        app.pickerWheels.element(boundBy: 1).adjust(toPickerWheelValue: testDataEditPhoto["day"]!)
        app.pickerWheels.element(boundBy: 2).adjust(toPickerWheelValue: testDataEditPhoto["year"]!)
        app.toolbars.buttons["Done"].tap()
        waitFor(photoTakenCellsQuery, seconds: 5)
        XCTAssertEqual(photoTakenCellsQuery.value as! String, testDataEditPhoto["date"]!)
        XCTAssertEqual(datePickersQuery.element.exists, false)
        
        //Input 'Title'
        tablesQuery.staticTexts["Photograph Galleries"].swipeUp()
        var originTitle = titlePhoto.value!
        titlePhoto.tap()
        XCTAssert(app.keys.element.exists)
        for symbol in editTitlePhoto {
            app.keys[symbol].tap()
        }
        tablesQuery.staticTexts["Title:"].tap()
        XCTAssertEqual(app.keys.element.exists, false)
        XCTAssertEqual(titlePhoto.value as! String, "\(originTitle) " + testDataEditPhoto["title"]!)
        
        //Input 'Description'
        var originDescription = descriptionPhoto.value!
        descriptionPhoto.tap()
        XCTAssert(app.keys.element.exists)
        for symbol in editDescriptionPhoto {
            app.keys[symbol].tap()
        }
        tablesQuery.staticTexts["Description:"].tap()
        XCTAssertEqual(app.keys.element.exists, false)
        XCTAssertEqual(descriptionPhoto.value as! String, testDataEditPhoto["description"]! + " \(originDescription)")
        
        //Select area of learning
        app.tables.buttons["Duplicate to Additional Outcomes"].tap()
        sleep(2)
        XCTAssert(tablesQuery.staticTexts["Select the Area of Learning:"].exists)
        
        //Check area learning
        tablesQuery.staticTexts[testDataEditPhoto["area"]!].tap()
        tablesQuery.staticTexts[testDataEditPhoto["aspect"]!].tap()
        waitFor(tablesQuery.staticTexts[testDataEditPhoto["outcome"]!], seconds: 5)
        tablesQuery.staticTexts[testDataEditPhoto["outcome"]!].tap()
        waitFor(tablesQuery.staticTexts[testDataEditPhoto["area"]!], seconds: 5)
        XCTAssert(tablesQuery.staticTexts[testDataEditPhoto["area"]!].exists)
        waitFor(tablesQuery.staticTexts[testDataEditPhoto["aspect"]!], seconds: 5)
        XCTAssert(tablesQuery.staticTexts[testDataEditPhoto["aspect"]!].exists)
        XCTAssert(tablesQuery.staticTexts[testDataEditPhoto["age_outcome"]!].exists)
        
        tablesQuery.staticTexts["Description:"].swipeUp()
        tablesQuery.staticTexts["Description:"].swipeUp()
        tablesQuery.buttons["Update Photo"].tap()
        waitFor(app.navigationBars["Baby's Day"].staticTexts["Comments"], seconds: 10)
        XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
        XCTAssert(tablesQuery.staticTexts["\(testDataEditPhoto["date"]!)"].exists)
        sleep(2)
        XCTAssert(tablesQuery.staticTexts["\(originTitle) " + testDataEditPhoto["title"]!].exists)
        XCTAssert(tablesQuery.staticTexts[testDataEditPhoto["description"]! + " \(originDescription)"].exists)
        
        //Check Progress Gallery
        buttonBack.tap()
        buttonBack.tap()
        
        tablesQuery.cells.containing(.staticText, identifier:"\(childInfo["firstName"]!)").buttons["galleries small btn 3"].tap()
        
        waitFor(selectAolCellsQuery.children(matching: .textField).element(boundBy: 0), seconds: 10)
        if selectAolCellsQuery.children(matching: .textField).element(boundBy: 0).value as! String != testDataEditPhoto["shortArea"]! {
            //Select short area name is not implemented
            
        } else if selectAolCellsQuery.children(matching: .textField).element(boundBy: 1).value as! String != testDataEditPhoto["aspect"] {
            //Select aspect name is not implemented
        }
        tablesQuery.buttons["Enter Gallery"].tap()
        XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Progress Gallery"].exists)
        
        //checkText("\(childInfo["firstName"]!) \(childInfo["lastName"]!)'s Progress Gallery - \(testDataEditPhoto["shortArea"]!)", tablesQuery.childrenMatchingType(.Cell).elementBoundByIndex(0).childrenMatchingType(.TextView).element.value as! String)
        checkVideo((originTitle as! String + " " + testDataEditPhoto["title"]!), testDataEditPhoto["date"]!)
        
    }
    
    func testCheckEditPhoto() {
        let tablesQuery = app.tables
        
        goGalleryChild("photographs")
        
        tablesQuery.cells.containing(.staticText, identifier:"\(childInfo["firstName"]!)").buttons["galleries small btn 3"].tap()
        
        tablesQuery.textFields["Select AOL"].tap()
        sleep(1)
        //app.pickers.elementBoundByIndex(0).swipeUp()
        app.pickerWheels.element.adjust(toPickerWheelValue: "CL")
        app.pickerWheels["MT, 1 of 2"].adjust(toPickerWheelValue: "MT")
        //app.pickers["MT, 1 of 2"].adjustToPickerWheelValue("MT, 1 of 2")
        //app.pickerWheels["MT, 1 of 2"].
        //app.pickerWheels["MT"].adjustToPickerWheelValue("CL")
        app.toolbars.buttons["Done"].tap()
        //app.tables.cells.containingType(.StaticText, identifier:"Select AOL:").childrenMatchingType(.TextField).elementBoundByIndex(0).tap()
        //app.pickers["MT, 1 of 2"].adjustToPickerWheelValue("CL, 2 of 2")
        
        tablesQuery.cells.containing(.staticText, identifier:"Select AOL:").children(matching: .textField).element(boundBy: 1).tap()
        app.pickerWheels["Listening and Attention, 1 of 1"].tap()
        app.toolbars.buttons["Done"].tap()
        tablesQuery.buttons["Enter Gallery"].tap()
        XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Progress Gallery"].exists)
        if app.collectionViews.images.count > 0 {
            app.collectionViews.images.element(boundBy: 0).tap()
        }
        
    }
    
    /*
     At first need set parameters fot 'dataDeletePhoto'.
     */
    func testDeletePhoto() {
        let tablesQuery = app.tables
        let selectYear = tablesQuery.cells.containing(.staticText, identifier:"Select Year and Month:").children(matching: .textField).element
        let textView = tablesQuery.children(matching: .cell).element(boundBy: 0).children(matching: .textView).element
        let imageButtons = tablesQuery.children(matching: .cell).element(boundBy: 3).children(matching: .button)
        let buttonBack = app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
        
        goGalleryChild("photographs")
        
        selectYear.tap()
        XCTAssert(app.pickerWheels.element.exists)
        app.pickerWheels[testDataDeletePhoto["selectYearInput"]!].tap()
        app.toolbars.buttons["Done"].tap()
        XCTAssertEqual(app.pickerWheels.element.exists, false)
        XCTAssertEqual(selectYear.value as! String, testDataDeletePhoto["selectYearValue"]!)
        XCTAssert(tablesQuery.buttons["Enter Gallery"].exists)
        
        tablesQuery.buttons["Enter Gallery"].tap()
        XCTAssert(tablesQuery.staticTexts["Photograph Galleries"].exists)
        
        
        var checkText = "\(childInfo["firstName"]!) \(childInfo["lastName"]!)'s General Gallery - \(testDataDeletePhoto["checkSelectYearValue"]!)" 
        var checkView = textView.value as! String
        var countCheckText = checkText.characters.count
        var endIndexCheckView = checkView.characters.index(checkView.startIndex, offsetBy: countCheckText-1)
        var range = checkView[checkView.startIndex...endIndexCheckView]
        XCTAssertEqual(checkText, range)
        
        tablesQuery.staticTexts["Photograph Galleries"].swipeUp()
        
        if imageButtons.count > 0 {
            var lastIndex = imageButtons.count - 1
            for index in 0 ... lastIndex {
                imageButtons.element(boundBy: index).tap()
                XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
                if tablesQuery.cells.containing(.staticText, identifier: testDataDeletePhoto["title"]!).element.exists  && tablesQuery.cells.containing(.staticText, identifier: testDataDeletePhoto["date"]!).element.exists {
                    XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
                    XCTAssert(tablesQuery.staticTexts["\(childInfo["firstName"]!)'s current age: \(childInfo["age"]!) months"].exists)
                    
                    
                    //Delete Photo
                    tablesQuery.buttons["•••"].tap()
                    XCTAssert(app.sheets.collectionViews.buttons["Delete Photo"].exists)
                    app.sheets.collectionViews.buttons["Delete Photo"].tap()
                    XCTAssert(warningAlert.staticTexts["Warning!"].exists)
                    warningAlert.collectionViews.buttons["Delete"].tap()
                    XCTAssert(app.navigationBars["Baby's Day"].staticTexts["General Gallery"].exists)
                    
                    //Check text 'Empty'
                    if imageButtons.count == 0 {
                        XCTAssertEqual(tablesQuery.staticTexts["Empty"].exists, false)
                    } else {
                        XCTAssert(tablesQuery.staticTexts["Empty"].exists)
                    }
                    
                    break
                } else {
                    buttonBack.tap()
                }
            }
        }
    }
    
    func testAreaLearning() {
        let tablesQuery = app.tables
        let uploadPhotographs = tablesQuery.cells.containing(.staticText, identifier:"Upload Photographs").children(matching: .button).element
        let selectPhotoSource = app.sheets["Please select photo source"]
        let dataAreaLearning = [
            "Communication and Language": [
                "Listening and Attention": [
                    "Birth - 11 Months": [
                        "Turns toward a familiar sound then locates range of sounds with accuracy.",
                        "Listens to, distinguishes and responds to intonations and sounds of voices.",
                        "Reacts in interaction with others by smiling, looking and moving.",
                        "Quietens or alerts to the sound of speech.",
                        "Looks intently at a person talking, but stops responding if speaker turns away.",
                        "Listens to familiar sounds, words, or finger plays.",
                        "Fleeting Attention - not under child's control, new stimuli takes whole attention."
                    ],
                    "8 Months - 20 Months": [
                        "Moves whole bodies to sounds they enjoy, such as music or a regular beat.",
                        "Has a strong exploratory impulse.",
                        "Concentrates intently on an object or activity of own choosing for short periods.",
                        "Pays attention to dominant stimulus - easily distracted by noises or other people talking."
                    ],
                    "16 Months - 26 Months": [
                        "Listens to and enjoys rhythmic patterns in rhymes and stories.",
                        "Enjoys rhymes and demonstrates listening by trying to join in with actions or vocalisations.",
                        "Rigid attention - may appear not to hear."
                    ],
                    "22 Months - 36 Months": [
                        "Listens with interest to the noises adults make when they read stories.",
                        "Recognises and responds to many familiar sounds, e.g. turning to a knock on the door, looking at or going to the door.",
                        "Shows interest in play with sounds, songs and rhymes.",
                        "Single channelled attention. Can shift to a different task if attention fully obtained - using child's name helps focus."
                    ],
                    "30 Months - 50 Months": [
                        "Listens to others one to one or in small groups, when conversation interests them.",
                        "Listens to stories with increasing attention and recall.",
                        "Joins in with repeated refrains and anticipates key events and phrases in rhymes and stories.",
                        "Focusing attention - still listen or do, but can shift own attention.",
                        "Is able to follow directions (if not intently focused on own choice of activity)."
                    ],
                    "40 Months - 60 Months": [
                        "Maintains attention, concentrates and sits quietly during appropriate activity.",
                        "Two-channelled attention - can listen and do for short span."
                    ]
                ]
            ]
        ]
        
        goGalleryChild("photographs")
        
        //Select a photo from the gallery
        uploadPhotographs.tap()
        XCTAssert(selectPhotoSource.exists)
        selectPhotoSource.collectionViews.buttons["Choose photo from library"].tap()
        if app.collectionViews.images.count > 0 {
            app.collectionViews.images.element(boundBy: 0).tap()
        }
        app.navigationBars["Camera Roll"].buttons["Done"].tap()
        waitFor(app.tables.buttons["Duplicate to Additional Outcomes"], seconds: 5)
        app.tables.buttons["Duplicate to Additional Outcomes"].swipeUp()
        app.tables.buttons["Duplicate to Additional Outcomes"].tap()
        waitFor(tablesQuery.staticTexts["Select the Area of Learning:"], seconds: 5)
        XCTAssert(tablesQuery.staticTexts["Select the Area of Learning:"].exists)
        
        //Check area learning
        for (area, aspects) in dataAreaLearning {
            XCTAssert(tablesQuery.staticTexts[area].exists)
            tablesQuery.staticTexts[area].tap()
            for (aspect, outcomes) in aspects {
                XCTAssert(tablesQuery.staticTexts[aspect].exists)
                tablesQuery.staticTexts[aspect].tap()
                for (outcome, ages) in outcomes {
                    if !tablesQuery.staticTexts[outcome].exists {
                        tablesQuery.staticTexts[outcome].swipeUp()
                    }
                    XCTAssert(tablesQuery.staticTexts[outcome].exists)
                    for age in ages {
                        XCTAssert(tablesQuery.staticTexts[age].exists)
                    }
                }
            }
        }
    }
    
    func testGoGalleryChild() {
        let element = app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let photographs = element.children(matching: .button).matching(identifier: "Galleries").element(boundBy: 0)
        let videos = element.children(matching: .button).matching(identifier: "Galleries").element(boundBy: 1)
        let uploadManager = element.children(matching: .button).matching(identifier: "Galleries").element(boundBy: 2)
        let navigatorBarManage = app.navigationBars["Manage"]
        let navigatorBarBabyDay = app.navigationBars["Baby's Day"]
        let tablesQuery = app.tables
        let uploadPhotographs = tablesQuery.cells.containing(.staticText, identifier:"Upload Photographs").children(matching: .button).element
        
        login()
        waitFor(app.collectionViews.buttons["Galleries"], seconds: 5)
        
        //Check Galleries and exists icons: photographs, videos, upload
        app.collectionViews.buttons["Galleries"].tap()
        waitFor(navigatorBarManage.staticTexts["Galleries"], seconds: 5)
        XCTAssert(navigatorBarManage.staticTexts["Galleries"].exists)
        
        XCTAssert(photographs.exists)
        XCTAssert(videos.exists)
        XCTAssert(uploadManager.exists)
        
        
        //Check Photographs
        photographs.tap()
        XCTAssert(navigatorBarBabyDay.staticTexts["Photo Galleries"].exists)
        XCTAssert(app.tables.staticTexts["PHOTOGRAPH GALLERIES  Shown below are the existing children's Photograph Galleries."].exists)
        
        let countChildren: UInt = app.tables.cells.containing(.staticText, identifier: "First Name:").count
        XCTAssert(countChildren > 0)
        
        if countChildren == 0 {
            //createChildren()
        }
        
        app.tables.cells.containing(.staticText, identifier:"First Name:").element(boundBy: 0).buttons["galleries small btn 4"].tap()
        waitFor(tablesQuery.staticTexts["Select Year and Month:"], seconds: 5)
        if tablesQuery.staticTexts["Select Year and Month:"].exists {
            XCTAssert(tablesQuery.staticTexts["Select Year and Month:"].exists)
            XCTAssert(tablesQuery.buttons["Enter Gallery"].exists)
            XCTAssert(uploadPhotographs.exists)
        } else {
            XCTAssert(uploadPhotographs.exists)
        }
    }
    
    func testSearchChild() {
        let searchChild = app.tables.textFields["Search Child"]
        let moreNumbersKey = app.keys["more, numbers"]
        let deleteKey = app.keys["delete"]
        
        //Check Seacrh child
        /*
         searchChild.tap()
         moreNumbersKey.tap()
         app.keys["2"].tap()
         moreNumbersKey.tap()
         app.keys["2"].tap()
         moreNumbersKey.tap()
         app.keys["2"].tap()
         sleep(2)
         let cellsQuery1 = tablesQuery.cells.containingType(.StaticText, identifier:"111")
         let cellsQuery2 = tablesQuery.cells.containingType(.StaticText, identifier:"222")
         XCTAssert(cellsQuery2.staticTexts["111"].exists)
         XCTAssert(cellsQuery2.staticTexts["04/07/2014"].exists)
         XCTAssert(cellsQuery2.staticTexts["20 months"].exists)
         XCTAssertEqual(cellsQuery1.staticTexts["04/02/2016"].exists, false)
         XCTAssertEqual(cellsQuery1.staticTexts["1 month"].exists, false)
         //
         deleteKey.tap()
         deleteKey.tap()
         deleteKey.tap()
         
         moreNumbersKey.tap()
         app.keys["1"].tap()
         moreNumbersKey.tap()
         app.keys["1"].tap()
         moreNumbersKey.tap()
         app.keys["1"].tap()
         sleep(2)
         XCTAssert(cellsQuery1.staticTexts["111"].exists)
         XCTAssert(cellsQuery1.staticTexts["04/02/2016"].exists)
         XCTAssert(cellsQuery1.staticTexts["1 month"].exists)
         deleteKey.tap()
         deleteKey.tap()
         deleteKey.tap()
         */
        
    }
}
