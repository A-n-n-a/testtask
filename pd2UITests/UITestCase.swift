//
//  UITestCase.swift
//  pd2
//

import XCTest

class UITestCase: UIDataTests {
    let app = XCUIApplication()
    let warningAlert = XCUIApplication().alerts["Warning"]
    var res = false
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app.launch()
    }
    
    override func tearDown() {
        super.tearDown()
        app.terminate()
    }
    
    func waitForElementToAppear(_ element: XCUIElement, file: String = #file, line: UInt = #line) {
        let existsPredicate = NSPredicate(format: "exists == 1")
        expectation(for: existsPredicate, evaluatedWith: element, handler: nil)
        
        waitForExpectations(timeout: 10) { (error) -> Void in
            if (error != nil) {
                let message = "Failed to find \(element) after 5 seconds."
                self.recordFailure(withDescription: message, inFile: file, atLine: line, expected: true)
            }
        }
        
    }
    
    func waitFor(_ element:XCUIElement, seconds waitSeconds:Double) {
        let exists = NSPredicate(format: "exists == 1")
        expectation(for: exists, evaluatedWith: element, handler: nil)
        waitForExpectations(timeout: waitSeconds, handler: nil)
    }
    
    func login() {
        let tablesQuery = app.tables
        let loginWithPassword = self.app.tables.staticTexts["Login with password"];
        let userName = tablesQuery.children(matching: .cell).element(boundBy: 2).children(matching: .textField).element
        let password = tablesQuery.children(matching: .cell).element(boundBy: 3).children(matching: .secureTextField).element
        let domain = tablesQuery.children(matching: .cell).element(boundBy: 1).children(matching: .textField).element
        let deleteKey = app.keys["delete"]
        let adminPickerWheel = tablesQuery.pickerWheels["Admin"]
        let skipButton = tablesQuery.buttons["Skip"]
        
        
        sleep(7)
        
        if tablesQuery.staticTexts["Enter Pin"].exists {
            let button = tablesQuery.cells.containing(.staticText, identifier:"1").children(matching: .button).element(boundBy: 0)
            button.tap()
            button.tap()
            button.tap()
            button.tap()
            sleep(5)
            
        }
        else {
            sleep(5)
            if tablesQuery.staticTexts[infoLogin["user"]!].exists {
                tablesQuery.staticTexts[infoLogin["user"]!].tap()
                waitFor(loginWithPassword, seconds: 10)
                XCTAssert(loginWithPassword.exists)
                loginWithPassword.tap()
            } else {
                waitFor(loginWithPassword, seconds: 10)
                waitFor(tablesQuery.staticTexts[infoLogin["user"]!], seconds: 10)
                XCTAssert(loginWithPassword.exists)
                tablesQuery.staticTexts[infoLogin["user"]!].tap()
                waitFor(loginWithPassword, seconds: 5)
                XCTAssert(loginWithPassword.exists)
                loginWithPassword.tap()
            }
            
            waitFor(userName, seconds: 5)
            XCTAssert(userName.exists)
            
            if (userName.value as! String).isEmpty {
                userName.tap()
                if adminPickerWheel.exists {
                    adminPickerWheel.tap()
                } else {
                    userName.tap()
                    userName.typeText(infoLogin["login"]!)
                }
            }
            password.tap()
            password.typeText(infoLogin["password"]!)
            
            if domain.value as! String != infoLogin["domain"]! {
                if domain.value as! String == ".mybabysdays.com" {
                    domain.tap()
                    domain.typeText("jenkins")
                } else {
                    domain.tap()
                    let countDomain = (domain.value as! String).characters.count
                    for _ in 0 ..< countDomain {
                        deleteKey.tap()
                    }
                    domain.typeText(infoLogin["domain"]!)
                }
            }
            
            tablesQuery.buttons["Login"].tap()
            
            if warningAlert.exists && warningAlert.staticTexts["You need to upgrade to the latest version of the app to continue, download it from the app store."].exists {
                warningAlert.buttons["Update"].tap()
            }
            
            
            if skipButton.exists {
                skipButton.tap()
            }
        }
        //tablesQuery.staticTexts["Enter Pin"].tablesQuery.cells.containingType(.StaticText, identifier:"1").childrenMatchingType(.Button).elementBoundByIndex(0)
    }
    
    func goGalleryChild(_ typeGallery: String) {
        let element = app.otherElements.containing(.navigationBar, identifier:"Manage").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let photographs = element.children(matching: .button).matching(identifier: "Galleries").element(boundBy: 0)
        let videos = element.children(matching: .button).matching(identifier: "Galleries").element(boundBy: 1)
        
        login()
        
        sleep(2)
        waitFor(app.collectionViews.buttons["Galleries"], seconds: 10)
        app.collectionViews.buttons["Galleries"].tap()
        
        if typeGallery == "photographs" {
            waitFor(photographs, seconds: 5)
            photographs.tap()
            waitFor(app.navigationBars["Baby's Day"].staticTexts["Photo Galleries"], seconds: 10)
        } else {
            waitFor(videos, seconds: 5)
            videos.tap()
            waitFor(app.navigationBars["Baby's Day"].staticTexts["Video Galleries"], seconds: 10)
        }
        
        
        searchChild(childInfo["firstName"]!)
        if res {
            app.tables.cells.containing(.staticText, identifier:childInfo["firstName"]!).element.buttons["galleries small btn 4"].tap()
        } else {
            print ("Child \(childInfo["firstName"]!) not exists.")
        }
    }
    
    
    func searchChild(_ child: String) {
        let tablesQuery = app.tables
        let fieldSearchChild = tablesQuery.textFields["Search Child"]
        
        //Check Seacrh child
        waitFor(fieldSearchChild, seconds: 10)
        fieldSearchChild.tap()
        if !app.keys.element.exists {
            waitFor(app.keys.element, seconds: 10)
        }
        sleep(2)
        fieldSearchChild.typeText(child)
        sleep(5)
        app.buttons["Return"].tap()
        if app.keys.element.exists {
            app.buttons["Return"].tap()
        }
        
        if tablesQuery.cells.containing(.staticText, identifier:child).element.exists {
            res = true
        } else {
            res = false
        }
    }
    
    func checkText
        (_ check_text: String, _ full_text: String)
        -> () {
            let countCheckText = check_text.characters.count
            let endIndexCheckText = check_text.characters.index(check_text.startIndex, offsetBy: countCheckText-1)
            let range = full_text[check_text.startIndex...endIndexCheckText]
            XCTAssertEqual(check_text, range)
    }
    
    func checkVideo
        (_ title: String, _ date: String)
        -> () {
            let tablesQuery = app.tables
            let buttonBack = app.navigationBars["Baby's Day"].children(matching: .button).element(boundBy: 1)
            var startIndex: UInt = 3
            var checkExistsVideo = false
            
            while tablesQuery.children(matching: .cell).element(boundBy: startIndex).children(matching: .button).count > 0 {
                for index: UInt in 0 ... 2 {
                    tablesQuery.children(matching: .cell).element(boundBy: startIndex).children(matching: .button).element(boundBy: index).tap()
                    if (app.navigationBars["Baby's Day"].staticTexts["Comments"].exists) {
                        if tablesQuery.cells.containing(.staticText, identifier: "\(title)").element.exists  && tablesQuery.cells.containing(.staticText, identifier: date).element.exists {
                            XCTAssert(app.navigationBars["Baby's Day"].staticTexts["Comments"].exists)
                            XCTAssert(tablesQuery.staticTexts["\(childInfo["firstName"]!)'s current age: \(childInfo["age"]!) months"].exists)
                            checkExistsVideo = true
                            break
                        } else {
                            buttonBack.tap()
                        }
                    } else {
                        break
                    }
                }
                startIndex += 1
            }
            
            XCTAssert(checkExistsVideo)
    }
    
    func createChildren() {
        
        let tablesQuery = app.tables
        let cell = tablesQuery.children(matching: .cell).element(boundBy: 1)
        let moreNumbersKey = app.keys["more, numbers"]
        let returnButton = app.buttons["Return"]
        let doneButton = app.toolbars.buttons["Done"]
        let collectionViewsQuery = app.collectionViews
        let startingDateCellsQuery = tablesQuery.cells.containing(.staticText, identifier:"Starting Date:")
        let firstNameChild = startingDateCellsQuery.children(matching: .textField).element(boundBy: 0)
        let lastNameChild = startingDateCellsQuery.children(matching: .textField).element(boundBy: 1)
        
        collectionViewsQuery.buttons["Manage"].tap()
        collectionViewsQuery.buttons["childrenButton"].tap()
        cell.buttons["Add a New Child"].tap()
        firstNameChild.tap()
        firstNameChild.typeText("John")
        lastNameChild.tap()
        lastNameChild.typeText("Travolta")
    }
}
